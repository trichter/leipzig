---
id: "3970"
title: "Workshop: Feminismus & Klimagerechtigkeit"
start: 2023-03-04 12:00
end: 2023-03-04 15:00
address: interim, Demmeringstr. 32, Leipzig
link: https://www.planlos-leipzig.org/events/workshop-feminismus-klimagerechtigkeit/
isCrawled: true
---
Workshop zu Klimagerechtigkeit und Feminismus: One struggle one fight! 

Wann? 04. März.23 - 12:00 Uhr - ca. 15 Uhr

Wo? Interim, Demmeringerstr. 32

Workshop in deutscher Lautsprache - für Snacks und ausreichend Pause ist gesorgt :)

Digger, bagger mich nicht an!

Was hat Klimagerechtigkeit mit Feminismus zu tun?

Sind eigentlich alle Menschen gleich von den Auswirkungen der globalen Klimakrise betroffen? Absolut nicht! Wir sehen, dass vor allem marginalisierte Gruppen wie z.B. FLINTA* [1], BiPoC [2] und Länder im globalen Süden am stärksten von den Folgen der Klimakrise betroffen sind. Woran liegt das? Und was hat das Ganze mit Kapitalismus, Kolonialismus und Patriarchat zu tun?

Die Klimakrise basiert auf einem ungerechten System, und feuert bestehende globale Ungerechtigkeiten immer weiter an. Es ist also nicht verwunderlich, dass vor allem Betroffene in den vordersten Reihen gegen die Klimakrise kämpfen.

Wir meinen: One Struggle One Fight! Der Kampf für eine klimagerechte Welt muss gleichzeitiger auch ein feministischer Kampf sein. Doch inwiefern sind die sozialen Bewegungen miteinander verknüpft? Und wie kann eine konkrete feministische Praxis innerhalb der Klimagerechtigkeitsbewegung aussehen?

Im Rahmen der feministischen Streikwoche möchten wir diese Fragen und mehr gemeinsam mit euch in einem Workshop diskutieren. Um dich für den Workshop anzumelden, schreib eine kurze Mail an leipzig@ende-gelaende.org oder schreib uns auf unseren Social Media Kanälen. Falls du uns verschlüsselt schreiben möchtest, findest du unseren PGP Key hier: https://www.ende-gelaende.org/mitmachen/ortsgruppen/ . Eine Anmeldung ist hilfreich, aber weder verbindlich noch ein Muss, du kannst also auch einfach am 04. März spontan vorbeischauen. Wir freuen uns auf euch!

_______________________________________________________________________

[1]: FLINTA* steht für Frauen, Lesben, Inter, Nichtbinäre, Trans und Agender und alle, die nicht unter den cis- männlichen Begriff fallen.

[2] BiPoC steht für Black, indigenous and People of Color und ist eine politische Selbstbezeichnung.



https://www.planlos-leipzig.org/events/workshop-feminismus-klimagerechtigkeit/