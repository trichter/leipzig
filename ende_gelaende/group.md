---
name: Ende Gelände Leipzig
website: https://www.facebook.com/Ende-Gel%C3%A4nde-Leipzig-286052808874449/
email: leipzig@ende-gelaende.org
scrape:
  source: ical
  options:
    url: https://www.planlos-leipzig.org/events.ics
  filter:
    description: 'ende-gelaende\.org'
---
Die Ende Gelände Ortsgruppe existiert seit 2018 und ist seitdem mit voller Power dabei! 😉 Unsere Themenschwerpunkte sind die Auseinandersetzung und der Kampf für Klimagerechtigkeit.
Neue Gesichter sind bei uns immer willkommen. Wir versuchen den Einstieg in unsere Arbeit so leicht und offen wie möglich zu gestalten. Möglichkeiten gibt es zu genüge - bring dich bei uns ein!
Wir freuen uns auf dich!