---
name: AG Antifa
website: https://facebook.com/agantifa
email: agantifa@safe-mail.net
scrape:
  source: facebook
  options:
    page_id: agantifa
---
Die Arbeitsgruppe Antifaschistische Politik (AG Antifa) ist ein offener Zusammenschluss in und bei der Partei Die Linke.Leipzig, der sich gegen nazistische und diskriminierende Handlungen und Denkmuster  in  Leipzig  engagiert.  Darüber  hinaus  nehmen  wir die  gesamtgesellschaftlichen  Bedingungen  kritisch  in  den  Blick, in denen diese Denkmuster entstehen und wachsen können.