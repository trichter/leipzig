---
id: "3938"
title: Saatgut-Tauschbörse in Taucha
start: 2023-03-04 10:00
end: 2023-03-04 14:00
address: Schloss Taucha, Haugwitzwinkel 1, Taucha
link: https://www.planlos-leipzig.org/events/saatgut-tauschboerse-in-taucha/
isCrawled: true
---


 

Saatgut ist der Reichtum unserer Erde. Sammeln wir die Samen unserer erfolgreich kultivierten Pflanzen aus unseren Gärten, können wir sie erneut aussäen und sicher sein, dass die kleinen Nachkömmlinge gut mit den Boden- und Witterungsverhältnissen klarkommen.

Das gilt aber nur für sogenannte samenfeste Sorten. Im Handel überwiegen inzwischen F1-Hybriden, deren erste Generation gute Eigenschaften aufweist, ihre Nachkommen aber den Eltern nicht gleichen, so dass Saatgut immer wieder neu gekauft werden muss.

Wenn wir unser Saatgut mit anderen teilen, helfen wir nicht nur, die Pflanzenvielfalt zu erhalten, sondern sparen auch noch Geld.

Wir treffen uns am Sonnabend, dem 04. März, von 10 bis 14 Uhr
in der Schloss-Scheune am Haugwitzwinkel 1 in Taucha

Sie können selbst gesammeltes Saatgut von samenfesten Pflanzenarten mitbringen und mitnehmen, was Sie gern einmal ausprobieren möchten. Sie können aber auch gern in unserem reichhaltigen Angebot stöbern und Samen gegen eine kleine Spende erwerben. Wir haben Saatgut aus den eigenen Gärten für verschiedenste Tomaten- und andere Gemüsesorten sowie für einen blühenden Blumengarten da und freuen uns auf einen regen Austausch.

https://klima-initiative-taucha.de/saatgut-tauschboerse-maerz-2023

https://www.planlos-leipzig.org/events/saatgut-tauschboerse-in-taucha/