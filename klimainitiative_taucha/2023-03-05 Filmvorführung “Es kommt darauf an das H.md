---
id: "3939"
title: Filmvorführung “Es kommt darauf an das Hoffen zu lernen.
  Umsiedlungsgeschichten aus dem Braunkohle-Revier Lausitz” mit anschließendem
  Filmgespräch
start: 2023-03-05 14:30
end: 2023-03-05 16:30
address: Kirchstr. 1, Kirchstr. 1, Taucha b. Leipzig
link: https://www.planlos-leipzig.org/events/filmvorfuehrung-es-kommt-darauf-an-das-hoffen-zu-lernen-umsiedlungsgeschichten-aus-dem-braunkohle-revier-lausitz-mit-anschliessendem-filmgespraech/
isCrawled: true
---


 

Nicht nur in Lützerath und im Rheinland reißt der Kohleabbau tiefe Wunden: Auch in unserer Region um Leipzig und im Lausitzer Braunkohle-Revier waren und sind noch immer viele Menschen von Umsiedlungen betroffen. Mit der Aufführung des Film von Anette Dorothea Weber, der im Februar 2023 seine Premiere hatte und seitdem auf Deutschland-Tour ist, und im anschließenden Filmgespräch mit der Regisseurin Annette Dorothea Weber und dem Filmteam wollen wir uns mit diesem Thema befassen.

Aus der Filmbeschreibung:

In der Lausitz liegt das braune Gold tief in der Erde. 130 Dörfer wurden in den letzten Jahrzehnten abgerissen und die Menschen umgesiedelt, um Braunkohle zu fördern. Auch in den Jahren 2021 und 2022 wurde die Kohle weiter abgebaggert und das letzte Dorf, Mühlrose, wird umgesiedelt.

Der Film erzählt die Umsiedlungsgeschichten von Menschen, Friedhöfen, Schafen und Ameisen. Tiefe Wunden und Risse bleiben, wenn sich das Leben durch den Braunkohleabbau radikal verändert. Aber auch Hoffnung auf mehr Wohlstand an einem neuen Ort keimt auf, der hoffentlich zur Heimat wird. So wenig wie Zusammenhalt in einen Umzugswagen passt, so sehr ziehen die Erinnerungen, Bilder und Träume mit um. Wie können die Menschen diese Widersprüche gut aushalten, ihr Da- und Miteinander-Sein gestalten? Auch vom Weggehen und Wiederankommen erzählt der Film in langsamen Bildern. Kann man denn Hoffnung lernen? „Man kann alles lernen, wenn man es nur will.“

Weitere Informationen zum Film und zur Premierentour: https://www.communityartcenter-mannheim.de/film-es-kommt-darauf-an-das-hoffen-zu-lernen/ 

Zeit: Sonntag, 5.3.2023, 14:30 Uhr
Ort: in den ehemaligen Räumlichkeiten der Neuapostolischen Kirche, Kirchstr.

https://klima-initiative-taucha.de/film-es-kommt-darauf-an-das-hoffen-zu-lernen

https://www.planlos-leipzig.org/events/filmvorfuehrung-es-kommt-darauf-an-das-hoffen-zu-lernen-umsiedlungsgeschichten-aus-dem-braunkohle-revier-lausitz-mit-anschliessendem-filmgespraech/