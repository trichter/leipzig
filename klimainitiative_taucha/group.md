---
name: Klima-Initiative Taucha
website: https://klima-initiative-taucha.de
scrape:
  source: ical
  options:
    url: https://www.planlos-leipzig.org/events.ics
  filter:
    description: klima-initiative-taucha.de
---
Wir sind eine offene Gruppe von Menschen, die den Klimaschutz in Taucha voranbringen wollen. Wir wollen für das Thema sensibilisieren, den Austausch befördern und konkrete Aktivitäten initiieren und unterstützen.