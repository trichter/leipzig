---
name: Das Japanische Haus e. V.
website: https://djh-leipzig.de/
email: mail@djh-leipzig.de
address: Eisenbahnstr 113b, 04315 Leipzig
scrape:
  source: facebook
  options:
    page_id: 186268974762040
---
Im Handlungsfeld Demokratie- und Toleranzförderung agiert Das Japanische Haus e.V. (DJH) als ein brückenschlagender Akteur zwischen internationalem Austausch und lokalen Aktionen und Veranstaltungen. Der Verein wurde von Personen mit Migrationshintergrund ins Leben gerufen, um den sozialen Zusammenhalt im Leipziger Osten zu stärken. Seitdem wirkt das DJH als Brückenbauer zwischen den verschiedenen Kulturen.
