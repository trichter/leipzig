---
id: "939777807472950"
title: Heavenphetamine NOM tour vol.1
start: 2023-04-27 21:00
end: 2023-04-28 00:00
locationName: Das Japanische Haus e. V.
address: Eisenbahnstr. 150, Leipzig
link: https://www.facebook.com/events/939777807472950/
image: 336703282_242665721519672_8431564770934974250_n.jpg
isCrawled: true
---
Japanese psychedelic kraut punk band heavenphetamine will play in brand new Das Japanische Haus e.V. ! They will have donation tour for Ukraine with two Ukrainian support musician. Bio of the band and explanation about the tour are below. Check them and ready to dance!

Heavenphetamine is a psychedelic kraut punk band fronted by Hiroki, with his partner Sara on drums and Yuji on guitar. Their music blends hypnotic rhythms, carefully crafted lyrics, and a rich and varied sonic palette to create a uniquely powerful melodic dance music. So far the band has released two EPs and toured all over the world, from New Zealand to the United Kingdom. In the Summer of 2021, Hiroki and Sara moved to Georgia and developed their style, incorporating elements of techno, jazz and experimental music. In December 2021, they performed in Kyiv, hosted by their friend Evgeny, who also provided technical support for the show and later jammed with the band in his studio. Flautist Kira also joined it. Then, after Russia's invasion of Ukraine, Hiroki and Sara made a momentous decision: to donate almost all of their savings to Ukraine and embark on a worldwide tour to help raise more funds for the war-torn country, relying on the kindness of strangers to house and feed them as they travelled across Europe for five months. The tour was an incredible, albeit challenging, experience, during which they performed again in Kyiv, joined by Evgeny and Kira. The four musicians promised to record an album together after the tour, which finished in October 2022, with the band returning to Georgia. Soon after, they journeyed to Kyiv and recorded their finest and most ambitious work to date. This new album will be released in the Spring of 2023, with an accompanying tour of Europe to follow.

https://youtu.be/eAg_9mUyY9U

The aim of this tour is to spread our love of music while raising money for those in need. The band will only spend money on basic necessities such as food and transport. A donation box will be in place at each concert, with all funds going to charitable organisations, or directly to people in desperate need. Evgeny, a Ukrainian, will help determine where and how donations are best allocated. We will announce where and how much money is donated as the tour progresses.

The purpose of this tour is not only money, however. We feel that collaboration between Japanese and Ukrainian artists is a special and worthwhile endeavour. In the past, Japan has invaded other countries, in Asia and elsewhere, justifying its actions on the basis of being a "great empire". Many Japanese citizens believed the government propaganda and supported these wars, such as the invasion of the USA, while others simply didn’t care. Then, two nuclear bombs devastated Japan, and millions of people lost everything. Now, Ukraine is in a catastrophic conflict situation. From our nation's difficult history, we have learned something important about peace, about working towards a brighter future. It’s not easily explained with language, but with music, it can be felt. We hope to communicate this message on our tour.