---
id: "962884395083736"
title: Küche für Alle
start: 2023-03-30 18:00
end: 2023-03-30 23:30
locationName: Das Japanische Haus e. V.
address: Eisenbahnstr. 150, Leipzig
link: https://www.facebook.com/events/962884378417071/
image: 336536291_5936702786426083_3591337488934849335_n.jpg
isCrawled: true
---
Jeder in Leipzig ist willkommen in unserem Haus! Genießt mal bitte unseren Raum und Speisen.
Ab 20 Uhr steht das Essen. Komm einfach mal vorbei.

– – –

Everyone in Leipzig is welcome to our place! Please enjoy our space and foods(Vegan, With donation).  Food serving from 8 p.m. If you are interested in, just come and join us. 

– – –

どなたでも参加できる「ごはんの会」です。20時から食事が始まります。投げ銭制です。気軽に遊びに来てください！

– – –

부담없이 놀러 오세요!
– – –

 请随时访问！

– – –

أهلاً بكم في البيت الياباني نرحب بالجميع هنا في مكانناو نرجو أن تستمتعو بالمكان والطعام , ويبدأ تقديم الطعام الساعة 7 مساءً. نرجو أن تشاركونا!

– – –

– – –

შაბათს გეპატიჟებათ ყველას Das Japanische Haus ვინც ლაიფციგში ცხოვრობს. დატკბით ჩვენი ადგილით და კერძებით. 19:00 საათიდან ერთად დავაგემოვნებთ მზა კერძს.