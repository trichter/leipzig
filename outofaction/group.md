---
name: Out of Action Leipzig
website: https://outofaction.blackblogs.org/?page_id=142
email: outofaction-leipzig@nadir.org
address: Eisenbahnstrasse 125.
---
Out of Action ist eine Gruppe von Aktivist_innen, die über die psychischen Folgen von
Repression und Gewalt im Kontext von linkem politischen Widerstand
informiert. Wir bieten emotionale erste Hilfe für betroffene
Einzelpersonen und Gruppen an und unterstützen einen solidarischen
Umgang miteinander auch durch (Bezugs-)Gruppenworkshops und
Informationsveranstaltungen.
