---
id: "1"
title: Gesprächsangebot
start: '2019-11-04 19:00'
end: '2019-11-04 20:30'
address: Eisenbahnstrasse 125
link: https://outofaction.blackblogs.org/?page_id=142
teaser: "Wir bieten emotionale erste Hilfe für betroffene Einzelpersonen und Gruppen an"
recurring:
  rules:
    - units: 1
      measure: dayOfWeek
    - units: 0
      measure: weekOfMonthByDay
---
_Out of Action_ ist eine Gruppe von Aktivist_innen, die über die psychischen Folgen von
Repression und Gewalt im Kontext von linkem politischen Widerstand
informiert. Wir bieten emotionale erste Hilfe für betroffene
Einzelpersonen und Gruppen an und unterstützen einen solidarischen
Umgang miteinander auch durch (Bezugs-)Gruppenworkshops und
Informationsveranstaltungen.

Leute aus Leipzig und Umgebung können sich per Mail wenden an:
outofaction-leipzig@nadir★org

Fingerprint: `275D 814D 6B59 E18E 13B3 E012 C5D7 02B5 A55A 6BD0` (gerne mit dem auf der Webseite vergleichen!)

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mDMEYm+RiRYJKwYBBAHaRw8BAQdAPHHX2W646aruGAsjJmh8TPCz1/fdv3q1ml1Q
q7ZpDAG0M291dG9mYWN0aW9uLWxlaXB6aWcgPG91dG9mYWN0aW9uLWxlaXB6aWdA
bmFkaXIub3JnPoiWBBMWCgA+FiEEJ12BTWtZ4Y4Ts+ASxdcCtaVaa9AFAmJvkYkC
GwMFCQlmAYAFCwkIBwMFFQoJCAsFFgIDAQACHgECF4AACgkQxdcCtaVaa9B4cQD8
D9hhFplePzukXzTyd62ZePLtLtyWJutVuUpED0euUiYBAJtt2cyvk/EJrB4B+qIv
7sSBPDM+82RSkPzDGOCEBUIPuDgEYm+RiRIKKwYBBAGXVQEFAQEHQGAEMES7lpII
t+gF/DGcjmxPLKV8/Rp6vzb1VrPQqQxjAwEIB4h+BBgWCgAmFiEEJ12BTWtZ4Y4T
s+ASxdcCtaVaa9AFAmJvkYkCGwwFCQlmAYAACgkQxdcCtaVaa9B2BgEAkd7GMxzj
pWJYOsUvAVZPwUv13UrZxjOop08ix9YaYBYBAOBt046fr8xDVe7zjVs1vzmzHoyg
Rq8Plqec/vgj80wH
=pSx1
-----END PGP PUBLIC KEY BLOCK-----
```