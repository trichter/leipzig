---
id: "538662711709456"
title: "Lindenau – Erlebnisse in Kinder und Jugendjahren mit dem Autor: Lothar Kurth"
start: 2023-04-29 15:00
locationName: Lixer, Pörstenerstr. 9, 04229 Leipzig
link: https://www.facebook.com/events/538662711709456/
image: 338162368_767175598362299_996262247683708597_n.jpg
isCrawled: true
---
Livestream: https://www.youtube.com/watch?v=eQeBds4Uk9I

Buchvorstellung mit Kaffee und Kuchen
und dem Autor Lothar Kurth

Erinnerungen sind Teil unseres Lebens. Jeder blickt auf angenehme und betrübliche zurück.Vorsorglich erinnert man sich eher an das Positive und hält das Negative gedeckelt. Aber auch aus Letzterem erwachsen Lernprozesse fürs Leben. Der Autor verbrachte Kindheit und Jugend im Leipziger Stadtteil Lindenau und kehrte ab Ende der 1990er Jahre mehrfach zur Spurensuche zurück. Seine Erinnerungen reflektieren weniger die große Geschichte als vielmehr den Alltag in einem verfallenden, aber immer noch lebendigen Großstadtviertel der 1950er bis 1980er Jahre. Die detailreichen und liebevoll verfassten Geschichten erinnern viele Leser an eigene Erlebnisse. Jüngeren bieten sie Einblicke in den nicht erfahrenen DDR-Alltag.


Veranstalter*innen: Lixer, Verlag Pro Leipzig, Rosa Luxemburg Stiftung