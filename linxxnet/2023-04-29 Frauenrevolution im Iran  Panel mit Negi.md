---
id: "1354877558624637"
title: Frauenrevolution im Iran / Panel mit Negin Behkam (nd) & Setayesh
  Hadizadeh, Shoan Vaisi,Maryam Aras
start: 2023-04-29 16:00
locationName: Interim
address: Demmeringstraße 32, Leipzig
link: https://www.facebook.com/events/1354877558624637/
image: 338155466_1532437773831543_8531771977422108509_n.jpg
isCrawled: true
---
Livestream: https://youtube.com/live/pFh7NXcfArA?feature=share

Seit 44 Jahren unterdrückt die Islamische Republik Iran die eigene Bevölkerung. Ethnische und religiöse Minderheiten, Frauen und queere Menschen sind davon besonderes betroffen. Der Tod der Kurdin Jina Mahsa Amini am 16. September 2022 führte zu einem beispiellosen, landesweiten Aufstand im Iran, wodurch auch die internationale Solidarität erwachte. Wieso war das Land bisher in keiner anderen Frage so geeint, wie in der Frauenfrage der Jin-Jiyan-Azadi-Revolution? 
Moderation: Negin Behkam (nd) und den Gäst*innen:
-Setayesh Hadizadeh, Ak­ti­vis­tin von Feminista.Berlin, Studentin
-Shoan Vaisi, Mitglied im Rat der Stadt Essen ,kurdischer Aktivist, Linker Politiker. 
- Maryam Aras, Künstlerin und Literaturwissenschaftlerin