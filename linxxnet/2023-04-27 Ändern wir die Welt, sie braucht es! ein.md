---
id: "139195102120883"
title: Ändern wir die Welt, sie braucht es! eine marxistisch-feministische
  Ansage mit dem Kollektiv MF3000
start: 2023-04-27 18:00
locationName: Interim
address: Demmeringstraße 32, Leipzig
link: https://www.facebook.com/events/139195102120883/
image: 338170653_4166077666950628_8491097585776307412_n.jpg
isCrawled: true
---
Livestream: https://youtube.com/live/El1UuuoRR3c?feature=share

Was wäre ein Text, den wir selbst als junge Frauen gern gelesen hätten?

Teil des Kollektiv MF 3000 sind Alex Wischnewski, Bettina Gutperl, Cordula Trunk, Ines Schwerdtner, Jen Funke-Kaiser, Kerstin Wolter und Lisa Mangold. Die Autor*innen engagieren sich in unterschiedlichen linken Spektren, von autonomen Gruppen über Partei, Gewerkschaft, Wissenschaft. Sie haben sich zusammengeschlossen für ein gemeinsames politisches Projekt.