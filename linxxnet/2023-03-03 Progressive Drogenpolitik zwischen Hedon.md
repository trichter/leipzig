---
id: "1922953118047135"
title: Progressive Drogenpolitik zwischen Hedonismus und Gesundheitsprävention
start: 2023-03-03 19:00
locationName: Galerie für Zeitgenössische Kunst
address: Karl-Tauchnitz-Straße 9-11, Leipzig
link: https://www.facebook.com/events/1922953118047135/
image: 322099739_561362446028576_9013557143715596222_n.jpg
isCrawled: true
---
Podiumsdiskussion: Progressive Drogenpolitik zwischen Hedonismus und Gesundheitsprävention

Nach langen Jahren einer dogmengetriebenen staatlichen Drogenpolitik mit Fokus auf Kriminalisierung und Abstinenz scheint es nach dem Antritt der neuen Bundesregierung endlich vorsichtige Bemühungen zu geben, einen pragmatischen Umgang mit illegalisierten Substanzen zu finden. Während bei der grundsätzlichen Bejahung einer Legalisierung etwa von Cannabis von der FDP bis zur LINKE fast alle Parteien des demokratischen Spektrums übereinstimmen, gehen die Vorstellungen über die konkrete Ausgestaltung einer progressiven Drogenpolitik hingegen weit auseinander.
 
Einerseits wird als Gegenmodell zur reaktionären Prohibitionslogik häufig auf die Freiheit des Individuums verwiesen, uneingeschränkt Konsumentscheidungen ohne Einmischung der Gesellschaft treffen zu dürfen – eine Haltung, die schnell unter Verdacht gerät, einem blinden Hedonismus neoliberaler Prägung zu folgen.

Im Angesicht der Absurdität der althergebrachten Unterteilung in „harte“ und „weiche“ Drogen sowie der Schädlichkeit auch sozial erwünschter Formen des Konsums insbesondere von Alkohol lässt sich auch ein anderes Konzept progressiver Drogenpolitik formulieren: Dieses setzt Gesundheitsprävention an erste Stelle und nimmt in diesem Zuge eine Neubewertung von Konsumformen anhand ihrer tatsächlichen gesundheitlichen Auswirkungen abseits tradierter sozialer Normen vor.

Unter den Tisch fällt bei aktuellen Diskussionen über eine moderne Drogenpolitik wie so oft die Stigmatisierung derer, die aufgrund von substanzbezogenen Abhängigkeiten medizinische Versorgung oder Substitutionsbehandlungen benötigen. Dieselben gesellschaftlichen Mechanismen, die das Entstehen dysfunktionaler Konsummuster begünstigen und perpetuieren, verursachen die Ausgrenzung von Menschen mit Abhängigkeitserkrankungen, auch in eigentlich solidarischen Communitys.

Wie eine progressive staatliche Drogenpolitik aussehen muss, die das gesundheitliche Wohlergehen Konsumierender an erste Stelle setzt, wie die Stigmatisierung von Menschen mit Substanzabhängigkeiten beendet werden kann und welche Rolle dabei eine solidarische Stadtgesellschaft einnehmen sollte, wollen wir am Freitag, den 03.03.2023 um 19 Uhr in der Galerie für Zeitgenössische Kunst, Karl-Tauchnitz-Straße 9-11, 04107 Leipzig diskutieren mit
Gerda Matzel, Hausärzt:in und suchtmedizinische Grundversorgung in Leipzig
Dr. Sven Speerforck, Oberarzt in der Klinik und Poliklinik für Psychiatrie und Psychotherapie des Universitätsklinikums Leipzig
Annegret Wegner, Sozialtherapeutin und Leiterin der Suchtberatungs- und Behandlungsstelle „Alternative I“ in Leipzig
Veranstalterin: linXXnet in Kooperation mit dem Kommunalpolitischen Forum Sachsen e.V.
Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.