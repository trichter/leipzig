---
id: "183340154524224"
title: '"My Favorite Job"'
start: 2023-05-11 18:00
locationName: linXXnet
address: Brandstr. 15, Leipzig-Connewitz
link: https://www.facebook.com/events/183340154524224/
image: 342157966_1443823846355117_3629307847300321724_n.jpg
isCrawled: true
---
"My Favorite Job"
- Die Freefilmers mit einem Film über Evakuierungen aus den russisch besetzten Gebieten in der Ukraine

Im Frühling 2022 is die ukrainische Stadt Mariupol von russischen Truppen umzingelt. Sie setzen ihre zerstörerischen Waffen gegen die Zivilbevölkerung jeden Tag ein und töten Massen von Menschen. Mitte März 2022 beginnen Freiwillige, Menschen von Mariupol nach Zaporizhzhia zu evakuieren. Nach jeder Tour kommen sie  zusammen um Informationen auszutauschen, sich gegenseitig zu unterstützen und um sich über ihre traumatischen Erfahrungen auszutauschen.

Der Film wurde von den FreeFilmers produziert, einem Kollektiv ukrainischer "underground filmmakers". Am 11. Mai 2023, 18 Uhr, ist Sashko Protyah von den FreeFilmers im linXXnet auf der Brandstraße 15 in Leipzig. Nach dem gut 30 Minuten langen Film wird er für ein Filmgespräch zur Verfügung stehen.

Die Veranstaltung findet auf Englisch statt und wird organisiert vom Europareferat der Rosa Luxemburg Stiftung und dem linXXnet.

++++

In the spring of 2022, the Ukrainian city of Mariupol was surrounded by Russian troops. They were using the weapons of mass destruction against the civilian population every day. In mid-March volunteers started evacuating people from Mariupol to Zaporizhzhia. They gathered after each trip to exchange the information, support each other, and talk about their traumatic experiences.

The movie was produced by the FreeFilmers, a community of Ukrainian "underground filmmakers" On May 11th 2023, 6pm, Sashko Protyah from FreeFilmers will be in linXXnet office on Brandstraße 15 in Leipzig. After screening the 30 minute movie, he will be up for a talk with the audience.

The event will be in English and is organized by the Europe Unit of Rosa Luxemburg Foundation and linXXnet.