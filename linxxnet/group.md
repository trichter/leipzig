---
name: linXXnet
website: https://www.linxxnet.de
email: kontakt@linxxnet.de
address: Brandstr. 15, Leipzig-Connewitz
scrape:
  source: facebook
  options:
    page_id: linXXnet
---
Das linXXnet ist vieles: Nachbarschaftsladen, Anlaufpunkt, Wohnzimmer für unterschiedliche Politgruppen, Plenumsfabrik, Copyshop, Vereinsberatungsstelle, Transpilager, Veranstaltungsort, Verleihstation, Flyerablage, Ausleihstation, Internetcafe, Politreklametafel…

Aber das linXXnet ist noch mehr: Ein Haufen von Leuten, die hier Politik machen, auf unterschiedlichen Ebenen, an unterschiedlichen Orten und mit verschiedenen Themen. Linke Politik in ihrer ganzen Bandbreite wird hier von mehr als 100 Leuten gestaltet, die regelmäßig in einem unserer Büros ein und aus gehen. Denn seit Sommer 2015 ist das linXXnet auch nicht mehr nur an einem Ort. Seit Eröffnung des INTERIM by linXXnet in Lindenau besteht das linXXnet aus zwei Büros, betrieben und belebt von einem Kollektiv von Aktivist_innen.