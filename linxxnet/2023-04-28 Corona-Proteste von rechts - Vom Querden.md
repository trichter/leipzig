---
id: "226563993180403"
title: Corona-Proteste von rechts - Vom Querdenken zur Querfront? mit dem Autor
  Lucius Teidelbaum
start: 2023-04-28 19:00
locationName: Lixer, Pörstenerstr. 9, 04229 Leipzig
link: https://www.facebook.com/events/226563993180403/
image: 338163001_526691752976392_508366796861774199_n.jpg
isCrawled: true
---
Livestream: https://www.youtube.com/watch?v=-CoQe_OSrqY

Was im April 2020 als diffuse Bewegung gegen die Maßnahmen anfing, wurde schnell zur rechtsoffenen Bewegung mit verschwörungsideologischen Unterbau und zum Marktplatz der alternativen Fakten. Im Buch wird die Entwicklung nachgezeichnet, sowie Inhalte der Bewegung und ihre Gefahren aufgezeigt. Moderation Steven HummelSteven Hummel (RLS Sachsen/ChronikLe)


Veranstalter*innen: Lixer, Unrast-Verlag, Antifa Tresen, ChronikLE, Rosa Luxemburg Stiftung Sachsen