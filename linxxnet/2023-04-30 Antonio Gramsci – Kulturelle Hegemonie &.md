---
id: "1194464201459406"
title: Antonio Gramsci – Kulturelle Hegemonie & Stadtteilarbeit mit Uwe
  Hirschfeld Vortrag und Diskussion
start: 2023-04-30 17:00
locationName: Lixer, Pörstenerstr. 9, 04229 Leipzig
link: https://www.facebook.com/events/1194464201459406/
image: 338173636_235684818852507_8965034049269694386_n.jpg
isCrawled: true
---
Livestream: https://www.youtube.com/watch?v=iX4JTjnQqns

Uwe Hirschfeld hat zahlreiche Texte zu dem italienischen Schriftsteller und Philosophen Antonio Gramsci verfasst. Sein Konzept der Hegemonie, wird vor allem in linken Kreisen breit rezipiert. Nach einer kurzen Einführung zu den Grundzügen der Philosophie Gramscis seinem Leben und Wirken, sprechen wir über die Möglichkeiten, wie seine Überlegungen in die aktuelle Praxis linker Stadtteilarbeit eingebunden und weitergedacht werden können.

Veranstalter*innen: Lixer, Rosa Luxemburg Stiftung