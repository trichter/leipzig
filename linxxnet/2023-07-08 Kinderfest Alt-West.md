---
id: "551749510494399"
title: Kinderfest Alt-West
start: 2023-07-08 15:00
end: 2023-07-08 18:00
locationName: Wasserschloss Leutzsch
address: Leipzig
link: https://www.facebook.com/events/551749510494399/
image: 347246855_2180454948826064_6604801894447796422_n.jpg
isCrawled: true
---
Das traditionellen Leutzscher Kinderfest vom Stadtbezirksverband von DIE LINKE. Leipzig Alt-West bietet auch dieses Jahr wieder die Möglichkeit für Spiel und Entspannung für groß und klein. 
Für Abwechslung sorgen unter anderem das Kaos Spielmobil, Kinderschminken, Basteln, eine Tombola und Torwandschießen. Hüpfburg und Popcorn dürfen natürlich nicht fehlen.