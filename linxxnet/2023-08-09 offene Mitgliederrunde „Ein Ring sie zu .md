---
id: "956576282015258"
title: "offene Mitgliederrunde: „Ein Ring sie zu knechten“? Verkehrspolitik in
  Leipzig"
start: 2023-08-09 19:00
locationName: linXXnet
address: Brandstr. 15, Leipzig-Connewitz
link: https://www.facebook.com/events/956576282015258/
image: 343730119_899619181130843_4937069314156429475_n.jpg
isCrawled: true
---
Der Stadtbezirksverband Süd lädt jeden zweiten Mittwoch zur offenen Mitgliederrunde ein. Bereits dieses Jahr steht ganz im Zeichen der Kommunalwahl im Mai 2024. Wenn man der Anzahl der wütenden Leserbriefe in der LVZ Glauben schenken mag (oder der „Fahrradbubble“ bei Twitter) bewegt kaum ein Thema Leipzig so sehr wie Verkehr und Mobilität. Wird also spannend! Zu Gast haben wir unsere Genossin Franziska Riekewald, die verkehrspolitische Sprecherin unserer Stadtratsfraktion ist. 

(Hinweis: Da wir gutes Wetter und Kneipen mögen, ändert sich der Ort manchmal noch kurzfristig in kleinem Umkreis. Infos dann aber am Ursprungsort und in der Telegramgruppe Süd: https://gleft.de/5bg)