---
id: "145790788506561"
title: Frühlingsfest am Herderplatz
start: 2023-06-10 14:00
end: 2023-06-10 18:00
locationName: Herderplatz, 04277 Leipzig, Deutschland
link: https://www.facebook.com/events/145790788506561/
image: 347249955_1400819174035242_5558235851274164349_n.jpg
isCrawled: true
---
Am Samstag, den 10.Juni wollen wir alle recht herzlich zu unserem Frühlingsfest auf den Herderplatz einladen. 

Ab 14 Uhr wird es bei Kaffee und Kuchen,  sowie verschiedenen Infoständen die Möglichkeit geben sich zu informieren, während die Kleinen beim Graffittie Workshop, bei der Fingermalfarbenmeile, mit Popcorn essen oder einfach auf dem Spielplatz beschäftigt sind. 
Und da der größte Antrieb des Kapitalismus, der Konsum und die Neuanschaffung ist, würden wir uns freuen, wenn es Kinder gibt, die ihr altes Spielzeug mitbringen, um es auf unserem antikapitalistischen Kinderflohmarkt miteinander zu tauschen.

Um 15.30 Uhr wollen wir dann bei unserem Nachbarschaftsforum mit Anwohnenden ins Gespräch kommen und über Eindrücke, Kritiken und Wünsche in Bezug auf die massive und martialische Polizeipräsenz und Ausschreitungen der vergangenen Woche in unserem Viertel sprechen.