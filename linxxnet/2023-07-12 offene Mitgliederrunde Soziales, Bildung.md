---
id: "187819320357699"
title: "offene Mitgliederrunde: Soziales, Bildung und Gesundheit – kann man da
  als Stadt überhaupt was tun?"
start: 2023-07-12 19:00
locationName: Frau Krause
address: Simildenstraße 8, Leipzig
link: https://www.facebook.com/events/187819320357699/
image: 343929376_186582297575176_6587994468847311062_n.jpg
isCrawled: true
---
Der Stadtbezirksverband Süd lädt jeden zweiten Mittwoch zur offenen Mitgliederrunde ein. Bereits dieses Jahr steht ganz im Zeichen der Kommunalwahl im Mai 2024. Was wollen wir und warum? Was sind gerade „heiße Eisen“ in unserer Stadt, was wurde schon umgesetzt und was dauert gefühlt tausend Jahre? Diesmal steht das Thema Soziales, Bildung und Gesundheit auf der Agenda. Referent ist Volker Külow. 

(Hinweis: Da wir gutes Wetter und Kneipen mögen, ändert sich der Ort manchmal noch kurzfristig in kleinem Umkreis. Infos dann aber am Ursprungsort und in der Telegramgruppe Süd: https://gleft.de/5bg)