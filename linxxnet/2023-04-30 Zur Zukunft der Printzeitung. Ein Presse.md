---
id: "1665834030542878"
title: Zur Zukunft der Printzeitung. Ein Presseclub mit dem nd
start: 2023-04-30 16:00
locationName: Interim
address: Demmeringstraße 32, Leipzig
link: https://www.facebook.com/events/1665834030542878/
image: 338126219_757952995962757_715684173702203554_n.jpg
isCrawled: true
---
Livestream: https://youtube.com/live/NTRzSYOc4Zk?feature=share

Zur Zukunft der Printzeitung.
Ist sie denn schon tot? Oder gibt es ein Happy End?
Stirb langsam, liebe Print-Tageszeitung! Aber noch gibt es sie. Wer rettet sie und wie? Die Tageszeitung »nd«, früher »Neues Deutschland«, ist dafür Experte: Keine Zeitung hatte so starken Auflagenverlust seit 2000 und keine Zeitung denkt so darüber nach, wie es weitergeht ohne schlechte Laune. Ein Presseclub mit drei nd-lern aus Feuilleton, Online und Geschäftsführung.