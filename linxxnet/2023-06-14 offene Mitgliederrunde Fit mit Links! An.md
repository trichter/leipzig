---
id: "1314618206077021"
title: "offene Mitgliederrunde: Fit mit Links! An der Connewitzer Spitze"
start: 2023-06-14 19:00
locationName: Connewitzer Kreuz
link: https://www.facebook.com/events/1314618206077021/
image: 344019783_624745926175824_6869574692537842744_n.jpg
isCrawled: true
---
Der Stadtbezirksverband Süd lädt jeden zweiten Mittwoch zur offenen Mitgliederrunde ein. Dieses Mal mit einem „Fit mit Links!“-Spezial mit unserer Genossin und ausbildeten Fitnesstrainerin Nancy. Mit ihr wollen wir gemeinsam nachträglich die neuen Sportmöglichkeiten an der Connewitzer Spitze am Connewitzer Kreuz einweihen – die nicht zuletzt unsere Leute im Stadtrat und Stadtbezirksbeirat ermöglicht haben. Der Sportkurs ist für alle geeignet. Mitzubringen sind Getränke, ein Handtuch und wetterfeste Sportkleidung.

Mi. 14. Juni, 19:00 Uhr, Connewitzer Spitze am Kreuz