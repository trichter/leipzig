---
id: "1968475080154854"
title: "„Zugang verwehrt“ Buchvorstellung und Gespräch über Klassismus(kritik)
  mit Francis Seeck "
start: 2023-04-28 18:00
locationName: Interim
address: Demmeringstraße 32, Leipzig
link: https://www.facebook.com/events/1968475080154854/
image: 338881520_922942189122339_3862974639841776151_n.jpg
isCrawled: true
---
Livestream: https://youtube.com/live/r6iDWjfAoN4?feature=share

Menschen werden wegen ihrer Klassenherkunft diskriminiert. Was heißt das – und können wir das ändern? 
In »Zugang verwehrt« schildert Francis Seeck, wie Menschen wegen ihrer Klassenherkunft und Klassenposition in unserer Gesellschaft diskriminiert werden. Seeck, Sozialwissenschaftler*in, Antidiskriminierungstrainer*in und Expert*in für Klassismuskritik, stammt selbst aus »armen Verhältnissen«, die Eltern rutschten nach der Wende in die Langzeitarbeitslosigkeit. Vor diesem Hintergrund und den Erfahrungen als Aktivist*in berichtet Seeck, welche weitreichenden Folgen die Klassenherkunft haben und wie sie sich von der Geburt an auf das weitere Leben auswirken kann – oft sogar über den Tod hinaus.