---
id: "221298620450517"
title: Solidarität mit den Arbeitskämpfen der Beschäftigten des öffentlichen Dienstes!
start: 2023-04-12 18:00
end: 2023-04-12 20:00
locationName: linXXnet
address: Brandstr. 15, Leipzig-Connewitz
link: https://www.facebook.com/events/221298620450517/
image: 338180974_6076666719076325_8770137064447147388_n.jpg
isCrawled: true
---
Die Beschäftigten des öffentlichen Dienstes befinden sich im Arbeitskampf. Erzieher*innen, Beschäftigte der Nahverkehrsbetriebe, der Stadtreinigung, des Jobcenter und vieler Behörden streiken für bessere Entlohnung: 10,5 % mehr und Erhöhung von Azubi- und Praktikant*innengehälter stehen. Das aktuelle Streikgeschehen übertrifft die Vehemenz der Arbeitskämpfe der letzten Jahre und zeugt von großer Solidarität unter den Beschäftigten. Die geforderten Lohnerhöhungen sind aufgrund der immensen Preissteigerungen mehr als verständlich. Die Arbeitgeberseite dagegen bleibt hart und mit ihren Angeboten weit unter den Forderungen.

Was ist der aktuelle Stand der Arbeitskämpfe? Wie geht es weiter? Warum ist das Gerede von einer "Lohn-Preis-Spirale" Unfug? Und wie können wir unterstützen?

Darüber wollen wir am Mittwoch, 12. April 18 Uhr im linXXnet diskutieren. Zu Gast ist Christian Keil, Gewerkschaftssekretär beim verdi-Bezirk Leipzig-Nordsachsen.

Eine Veranstaltung von der LINKEN-Leipzig-Süd & linXXnet