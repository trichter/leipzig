---
id: film-des-monats-in-einem-land-das-es-nicht-mehr-gibt-2023-07-03t000000z
title: Film des Monats - In einem Land, das es nicht mehr gibt
start: 2023-07-03 19:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/film-des-monats-in-einem-land-das-es-nicht-mehr-gibt-2023-07-03t000000z
image: csm_2023-07-03_Plakat_in-einem-land-das-es-nicht-mehr-gibt_Leipzig_a5ff5e4596.jpg
teaser: Film (Saal)
isCrawled: true
---
Spielfilm (Deutschland 2022, 101 Min, Regie: Aelrun Goette) Filmvorführung In der Reihe „Liebe im geteilten Land“ Eintritt freiIm Sommer 1989 fliegt Suzie kurz vor dem Abitur von der Schule. Statt zu studieren, muss sie sich nun als Arbeiterin in der sozialistischen Produktion bewähren. Ein zufälliges Foto öffnet ihr unverhofft die Tür in die glamouröse Welt der Mode. Suzie taucht ein in die schillernde Subkultur Ostberlins. Sie verliebt sich in den rebellischen Fotografen Coyote und erlebt die Freiheit, von der sie immer geträumt hat. Doch alles hat seinen Preis: Was ist es Suzie wert, ihren Traum zu leben?Aelrun Goette, geboren 1966 in Ost-Berlin, ist Regisseurin von Dokumentar- und Spielfilmen. Nach einer Ausbildung zur Krankenschwester jobbte sie als Model bei der Zeitschrift Sibylle, studierte Philosophie an der Humboldt-Universität zu Berlin und später Regie an der Filmhochschule Babelsberg. Sie arbeitete als Schauspielerin und Theaterregisseurin, war in der Serie „Gute Zeiten, schlechte Zeiten“ zu sehen. Ihre Filme beschäftigen sich mit Menschen, hauptsächlich Frauen in psychischen Grenzbereichen. Seit 2019 ist sie Honorarprofessorin für die Studiengänge Schauspiel, Regie und Drehbuch/Dramaturgie an der Filmhochschule Babelsberg. Informationen zur Veranstaltungsfotografie