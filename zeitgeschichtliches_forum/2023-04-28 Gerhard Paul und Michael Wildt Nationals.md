---
id: gerhard-paul-und-michael-wildt-nationalsozialismus-aufstieg-macht-niedergang-nachgeschichte-2023-04-28t000000z
title: "Gerhard Paul und Michael Wildt: Nationalsozialismus. Aufstieg – Macht –
  Niedergang – Nachgeschichte"
start: 2023-04-28 18:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/gerhard-paul-und-michael-wildt-nationalsozialismus-aufstieg-macht-niedergang-nachgeschichte-2023-04-28t000000z
image: csm_2023-04-28_Wildt-Paul_Leipzig_b61b61482d.jpg
teaser: Buchvorstellung (Saal)
isCrawled: true
---
Buchvorstellung Mit den Historikern und Autoren Gerhard Paul und Michael Wildt Moderation: Gesa Ufer (Autorin und Journalistin) In Kooperation mit der Bundeszentrale für politische BildungBuchvorstellungMit den Historikern und Autoren Gerhard Paul und Michael WildtModeration: Gesa Ufer (Journalistin)In Kooperation mit der Bundeszentrale für politische BildungIn dem Buch "Nationalsozialismus. Aufstieg – Macht – Niedergang – Nachgeschichte" schildern Gerhard Paul und Michael Wildt die Geschichte des Nationalsozialismus als Gesellschafts- und Mediengeschichte. Nationalsozialistische Herrschaft, Krieg und Massenmord schufen nicht zuletzt eigene Bild- und Tonwelten. Diese multimediale Dimension greifen die Autoren in der Publikation auf: In einer eigens entwickelten App werden den Leserinnen und Lesern zahlreiche weiterführende Abbildungen, Videos, Audio-Dateien, Texte und Websites in kontextualisierter Form zugänglich gemacht.Gerhard Paul ist Professor i.R. für Geschichte und ihre Didaktik an der Europa-Universität Flensburg. Er hat zahlreiche Publikationen zur NS-Täterforschung sowie zur Medialität und Visualität der Geschichte veröffentlicht.Michael Wildt ist Professor i.R. für Deutsche Geschichte im 20. Jahrhundert mit Schwerpunkt im Nationalsozialismus an der Humboldt-Universität zu Berlin. Er hat zahlreiche Studien zur Geschichte der Gewalt und zum nationalsozialistischen Terror vorgelegt.www.bpb.deInformationen zur Veranstaltungsfotografie