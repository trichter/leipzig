---
id: gegen-die-angst-seid-nicht-stille-das-geheime-tonband-von-pannach-kunert-und-fuchs-07-12-2022
title: Gegen die Angst, seid nicht stille. Das geheime Tonband von Pannach,
  Kunert und Fuchs
start: 2023-04-04 19:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/gegen-die-angst-seid-nicht-stille-das-geheime-tonband-von-pannach-kunert-und-fuchs-07-12-2022
image: csm_12_07_Gegen_die_Angst_Leipzig_48b543970b.jpg
teaser: Lesung (Saal)
isCrawled: true
---
Lese- und Gesprächsabend Mit Doris Liebermann (Autorin, Berlin) und Christian „Kuno“ Kunert (Musiker und Autor, Goslar)Herbst 1976, Leipzig. Die Liedermacher Gerulf Pannach und Christian Kunert sowie der Schriftsteller Jürgen Fuchs nehmen ein geheimes Tonband mit ihren Liedern und Texten auf. Zwei Musiker der im Jahr zuvor verbotenen Band „Renft“ helfen dabei. Trotz Stasi-Überwachung gelingt es, das Band in den Westen zu bringen.   Die Texte von Jürgen Fuchs, die Lieder von Gerulf Pannach und Christian Kunert sind der DDR-Gesellschaft gegenüber kritisch eingestellt. Alle drei gerieten deshalb zunehmend in das Visier des Ministeriums für Staatssicherheit (MfS). Jürgen Fuchs wurde von der Universität Jena relegiert, Gerulf Pannach durfte nicht mehr als freischaffender Songinterpret auftreten. Ein gemeinsames Liederprogramm von Gerulf Pannach und Christian Kunert fand keine Zustimmung der DDR-Behörden.   Nach der Ausbürgerung von Wolf Biermann im November 1976 wurden alle drei verhaftet und in das Stasi-Untersuchungshaftgefängnis Berlin-Hohenschönhausen gebracht. Als Kulminationspunkt ihrer Arbeiten rückte das „Leipziger Tonband“ in den Mittelpunkt der Vernehmungen.  Dank internationaler Solidarität wurden sie nach einem Dreivierteljahr ohne Prozess nach West-Berlin ausgebürgert. Die Ermittlungsverfahren gegen sie wurden mit der Begründung eingestellt, die „Straftat“, als die einzig ihr „Leipziger Tonband“ aufgeführt war, habe keine schädlichen Auswirkungen mehr, der Sozialismus habe sich entwickelt ... Informationen zur Veranstaltungsfotografie