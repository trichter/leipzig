---
id: lange-nacht-der-fussnoten-ein-pecha-kucha-abend-ueber-maenner-klitschen-und-die-ddr-auf-twitter-16-11-2023
title: Lange Nacht der Fußnoten. Ein Pecha-Kucha-Abend über Männer, Klitschen
  und die DDR auf Twitter
start: 2023-11-16 19:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/lange-nacht-der-fussnoten-ein-pecha-kucha-abend-ueber-maenner-klitschen-und-die-ddr-auf-twitter-16-11-2023
image: csm_Plakat_Version1HP_d51a5ecc2e.jpg
teaser: Vortrag und Gespräch (Saal)
isCrawled: true
---
NULL