---
id: blick-hinter-die-kulissen-1000-kleine-dinge-machen-erst-das-leben-schoen-mode-wohnen-und-einkaufen-in-der-ddr-03-11-2023
title: "Blick hinter die Kulissen:  „1000 kleine Dinge machen erst das Leben
  schön.“ Mode, Wohnen und Einkaufen in der DDR"
start: 2023-11-03 16:30
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/blick-hinter-die-kulissen-1000-kleine-dinge-machen-erst-das-leben-schoen-mode-wohnen-und-einkaufen-in-der-ddr-03-11-2023
image: csm_Foto_Jessika_Weisse2_@privatHP_117a357ec3.jpg
teaser: Gespräch (Dauerausstellung)
isCrawled: true
---
NULL