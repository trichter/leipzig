---
id: elisabeth-wellershaus-wo-die-fremde-beginnt-ueber-identitaet-in-der-fragilen-gegenwart-2023-04-29t000000z
title: "Elisabeth Wellershaus: Wo die Fremde beginnt. Über Identität in der
  fragilen Gegenwart"
start: 2023-04-29 19:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/elisabeth-wellershaus-wo-die-fremde-beginnt-ueber-identitaet-in-der-fragilen-gegenwart-2023-04-29t000000z
image: csm_2023-04-28_Wellershaus_Leipzig_b6d95db1bc.jpg
teaser: Buchvorstellung (Saal)
isCrawled: true
---
Moderation: Nina AminIn Kooperation mit dem Verlag C.H. BeckFremdheit ist ein Phänomen, das die Journalistin Elisabeth Wellershaus seit frühester Kindheit aus den Zuschreibungen anderer kennt. In ihrem Buch zeichnet sie nach, wie viel komplexer, allgegenwärtiger und bereichernder sie die Fremde selbst wahrnimmt – und warum sie uns verbindet. Wellershaus ist im bürgerlichen Stadtteil Hamburg-Volksdorf mit ihren weißen Großeltern und ihrer weißen Mutter aufgewachsen. Ihr Vater lebte als Kind auf einer Kakaoplantage in Äquatorialguinea und zog in den 1960er Jahren an die Costa del Sol. Fremdheit hat sie als Schwarze Deutsche zwischen Hamburg, Malaga und den Lebenswelten ihrer Eltern als komplexes Konstrukt kennengelernt. Nach Studienjahren in London lebt sie als Journalistin mit klassischer Kleinfamilie im gentrifizierten Teil des Berliner Stadtteils Pankow. Heute gehört sie zur privilegierten Mittelschicht, einfache Zugehörigkeitsnarrative greifen längst nicht mehr. In ihrem Buch erforscht Wellershaus Kontexte, in denen Fremdheit sich nicht gleich auf den ersten Blick erschließt: in Freundschaften, Arbeitsbeziehungen, Nachbarschaften, der Familie – in unmittelbarer Nähe. Sie erzählt von unentschlossenen Biografien, komplexen Identitäten und verknüpft die Weltwahrnehmungen anderer mit eigenen. Dadurch gelingt es ihr auf besondere Weise, die identitätspolitischen Perspektiven der Gegenwart zu erweitern.www.chbeck.deInformationen zur Veranstaltungsfotografie