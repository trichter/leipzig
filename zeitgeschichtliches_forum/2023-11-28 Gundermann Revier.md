---
id: gundermann-revier-28-11-2023
title: Gundermann Revier
start: 2023-11-28 19:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/gundermann-revier-28-11-2023
image: csm_3_Foto_GUNDERMANN_REVIER_historisch_02HP_e042a3ca6f.jpg
teaser: Filmvorführung und Gespräch (Saal)
isCrawled: true
---
NULL