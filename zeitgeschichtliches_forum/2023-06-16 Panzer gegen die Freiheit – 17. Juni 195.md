---
id: panzer-gegen-die-freiheit-17-juni-1953-2023-06-16t000000z
title: Panzer gegen die Freiheit – 17. Juni 1953
start: 2023-06-16 19:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/panzer-gegen-die-freiheit-17-juni-1953-2023-06-16t000000z
image: csm_2023-06-17_SK_LO_17Juni_Leipzig_2b3717ee66.jpg
teaser: Szenische Lesung (Saal)
isCrawled: true
---
Zum 70. Jahrestag des Volksaufstandes in der DDR Mit Alida Bohnen und Philipp Zemmrich (Theater der Jungen Welt) Musikalische Begleitung: Steffen Greisiger (Piano) In Kooperation mit der Leipziger Autorin Regine MöbiusIn den Tagen um den 17. Juni 1953 kam es in der DDR zu einer Welle von Streiks, Demonstrationen und politischen Protesten. Mehr als eine Million Menschen gingen in Stadt und Land auf die Straße. Mit massivem Einsatz von Militär, Volkspolizei und Staatssicherheit wurde der Aufstand des 17. Juni blutig niedergeschlagen.2023 jährt sich der Volksaufstand zum 70. Mal. Dies ist Anlass, zurückzublicken: Wovon war das Jahr 1953 geprägt? Was waren die individuellen Beweggründe für eine Beteiligung an den Protesten? Was geschah rund um den 17. Juni 1953 in Leipzig?Die szenische Lesung mit Alida Bohnen und Philipp Zemmrich (Theater der Jungen Welt) kombiniert Erinnerungen von Zeitzeuginnen und Zeitzeugen und gibt Einblick in die Tage des Aufstands vor sieben Jahrzehnten.  Informationen zur Veranstaltungsfotografie