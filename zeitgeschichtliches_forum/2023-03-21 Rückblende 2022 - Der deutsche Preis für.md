---
id: rueckblende-2022-der-deutsche-preis-fuer-politische-fotografie-und-karikatur-2023-03-21t000000z
title: Rückblende 2022 - Der deutsche Preis für politische Fotografie und Karikatur
start: 2023-03-21 18:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/rueckblende-2022-der-deutsche-preis-fuer-politische-fotografie-und-karikatur-2023-03-21t000000z
image: csm_21-03-23_Rueckblende_Leipzig_80c7212b6b.jpg
teaser: Ausstellungseröffnung (Foyer)
isCrawled: true
---
Vom 22. März bis zum 23. April 2023 zeigt das Zeitgeschichtliche Forum Leipzig eine Auswahl der Fotografien und Karikaturen der „Rückblende 2022“.„Zeitenwende“ – dieses Wort prägt das Jahr 2022, das überschattet wird vom Angriffskrieg auf die Ukraine. Auf die Einschränkungen der Pandemie folgen neue, nicht erwartete Krisenerfahrungen: Diskussionen um Verteidigungsetats, Energiekrise, Inflation, Unsicherheit. Aber es gibt auch große Solidarität und Hilfsbereitschaft für die aus der Ukraine ankommenden Geflüchteten. Zudem beschäftigen die Debatten um die Folgen des Klimawandels die Menschen in Deutschland. Dürre und Waldbrände, aber auch Protestaktionen sind allgegenwärtig und führen zu Kontroversen. Seit 2002 ist die „Rückblende“ alljährlich zu Gast im Zeitgeschichtlichen Forum Leipzig. Veranstalter des Wettbewerbs sind die Landesvertretung Rheinland-Pfalz und der Bundesverband Digitalpublisher und Zeitungsverleger in Kooperation mit der Bundespressekonferenz.Den Preis in der Kategorie „Bestes Foto 2022“ teilen sich dieses Jahr Bernd Kammerer mit dem Bild einer Solidaritätskundgebung für die Ukraine und Boris Roessler, der eine niedergebrannte Waldfläche fotografierte. Das Duo Achim Greser und Heribert Lenz gewinnt den ersten Preis der Karikaturen mit ihrem Werk „Putin privat“. In den Kategorien „Beste Serie 2022“ und „Das scharfe Sehen“ werden Filip Singer und Florian Gaertner ausgezeichnet.  Informationen zur Veranstaltungsfotografie