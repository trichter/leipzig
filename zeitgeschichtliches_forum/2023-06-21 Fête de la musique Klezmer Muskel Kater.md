---
id: fete-de-la-musique-klezmer-muskel-kater-2023-06-21t000000z
title: "Fête de la musique: Klezmer Muskel Kater"
start: 2023-06-21 16:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/fete-de-la-musique-klezmer-muskel-kater-2023-06-21t000000z
image: csm_2023-06-21_Klezmer-Muskelkater_Leipzig_8520254953.jpg
teaser: Konzert (Fußgängerzone)
isCrawled: true
---
Vier Länder, vier Kulturen, fünf Menschen – verbunden durch die Sprache der Musik. Das Quintett Klezmer Muskel Kater, dessen Mitglieder aus Syrien, Moldawien, Italien und Deutschland stammen, verbinden verschiedene musikalische Genres. Dabei geht es den Musikern in ihren Arrangements um Verbindung, Aufbruch und Entdeckung. Zu hören ist eine Gesamtheit von Tönen und Stimmungen aus persischer Klassik, Klezmermusik und Balkanliedern mit Akkordeon, Klarinette, Tuba, Percussions und Trompete.http://www.klezmermuskelkater.de/ Informationen zur Veranstaltungsfotografie