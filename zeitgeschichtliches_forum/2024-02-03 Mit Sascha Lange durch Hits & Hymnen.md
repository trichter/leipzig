---
id: mit-sascha-lange-durch-hits-hymnen-03-02-2024
title: Mit Sascha Lange durch "Hits & Hymnen"
start: 2024-02-03 16:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/mit-sascha-lange-durch-hits-hymnen-03-02-2024
image: csm_Sascha_Lange_ZFL_@Gert_Mothes_203cd89468.jpg
teaser: Ausstellungsbegleitung (Wechselausstellung)
isCrawled: true
---
NULL