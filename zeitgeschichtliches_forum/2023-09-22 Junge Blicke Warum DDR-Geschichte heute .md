---
id: junge-blicke-warum-ddr-geschichte-heute-autorinnen-und-autoren-inspiriert-22-09-2023
title: "Junge Blicke: Warum DDR-Geschichte heute Autorinnen und Autoren inspiriert"
start: 2023-09-22 19:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/junge-blicke-warum-ddr-geschichte-heute-autorinnen-und-autoren-inspiriert-22-09-2023
image: csm_22.09.2023_Valerie_Schoenian_2_Credit_JohannaWittig_web_3d8af9e519.jpg
teaser: Gespräch (Saal)
isCrawled: true
---
NULL