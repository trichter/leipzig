---
id: frauen-und-arbeitswelt-krisen-kinder-karriere-2023-03-09t000000z
title: Frauen und Arbeitswelt – Krisen, Kinder, Karriere
start: 2023-03-09 16:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/frauen-und-arbeitswelt-krisen-kinder-karriere-2023-03-09t000000z
teaser: Podiumsdiskussion (Saal)
isCrawled: true
---
16.00-17.30 Uhr: Führung durch die Dauerausstellung 18.00-19.00 Uhr: Podiumsgespräche und Austausch im SaalMusik: Nadine Maria Schmidt (Sängerin, Leipzig)Anmeldung für die Begleitung und/oder die Abendveranstaltung bis 6. März 2023 per E-Mail an gleichstellung@leipzig.de erbeten.In Kooperation mit dem Referat für Gleichstellung von Frau und Mann der Stadt Leipzig und dem Deutschen Gewerkschaftsbund Region Leipzig-NordsachsenInformationen zur Veranstaltungsfotografie