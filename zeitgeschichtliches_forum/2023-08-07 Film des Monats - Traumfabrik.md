---
id: film-des-monats-traumfabrik-2023-08-07t000000z
title: Film des Monats - Traumfabrik
start: 2023-08-07 19:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/film-des-monats-traumfabrik-2023-08-07t000000z
image: csm_2023-08-07_Plakat_traumfabrik_Leipzig_912d5f108f.jpg
teaser: Film (Saal)
isCrawled: true
---
Spielfilm (Deutschland 2019, 125 min, Regie: Martin Schreier) Filmvorführung In der Reihe „Liebe im geteilten Land“ Eintritt freiSommer 1961. Emil ist Komparse im DEFA-Studio Babelsberg und verliebt sich während einer deutsch-französischen Koproduktion Hals über Kopf in die französische Tänzerin Milou. Doch das Glück ist von kurzer Dauer, denn sie werden durch den Mauerbau am 13. August 1961 getrennt. Ein Wiedersehen scheint unmöglich, bis Emil einen waghalsigen Plan schmiedet …Martin Schreier, geboren 1980 in Kronberg im Taunus, studierte an der Deutschen Filmhochschule Berlin, später an der Filmakademie Baden-Württemberg, Spielfilmregie. 2019 absolvierte er die „Hollywood Masterclass“ an der UCLA in Los Angeles, USA. Zusammen mit dem Filmproduzenten Tom Zickler drehte er 2016 den Spielfilm „Unsere Zeit ist jetzt“, 2019 folgte „Traumfabrik“, der international zahlreiche Filmfestivals eröffnete. Seit 2021 realisiert Schreier Filmprojekte für die Amazon Studios.Informationen zur Veranstaltungsfotografie