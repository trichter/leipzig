---
id: der-stasi-mythos-ddr-auslandsspionage-und-der-verfassungsschutz-24-10-2023
title: „Der Stasi-Mythos. DDR-Auslandsspionage und der Verfassungsschutz“
start: 2023-10-24 19:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/der-stasi-mythos-ddr-auslandsspionage-und-der-verfassungsschutz-24-10-2023
image: csm_Cover_StasimythosHP_c4a4d21d76.jpg
teaser: Buchvorstellung (Saal)
isCrawled: true
---
NULL