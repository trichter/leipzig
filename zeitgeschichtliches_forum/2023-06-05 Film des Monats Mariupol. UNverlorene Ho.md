---
id: mariupol-unverlorene-hoffnung-2023-06-05t000000z
title: "Film des Monats: Mariupol. UNverlorene Hoffnung"
start: 2023-06-05 19:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/mariupol-unverlorene-hoffnung-2023-06-05t000000z
image: csm_2023_06_05_Mariupol._Unlost_Hope_Leipzig_afecc0b23e.jpg
teaser: Film (Saal)
isCrawled: true
---
Mariupol. UNverlorene Hoffnung Dokumentation (UKR 2022, 52 min, OmdU, Regie: Max Lytvynov) Filmvorführung In der Reihe „Ukraine – Kampf um Unabhängigkeit“   Es ist die Geschichte eines Verbrechens gegen die Menschlichkeit aus der Sicht der Opfer, die es geschafft haben, zu überleben. Drei Frauen und zwei Männer, die im ersten Monat der russischen Invasion in Mariupol lebten, erzählen, was sie gesehen und gefühlt haben, wie Entscheidungen während des Krieges getroffen wurden. Die Tagebücher der Mariupoler Journalistin Nadia Sukhorukova berichten von mehr als hundert Todesfällen in einem Monat. Durch ihre Chronik der Blockade in den sozialen Medien konnten die Menschen sehen und mitfühlen, was sie erlebte. Die Dokumentation zeichnet die Geschichte einer Stadt, in der alles zerstört wurde außer der Hoffnung.   Der Film, produziert von der Organisation Ukrainischer Produzenten (OUP), wird seit dem 24. August 2022 – dem Unabhängigkeitstag der Ukraine – mit Unterstützung des Außenministeriums der Ukraine auf der ganzen Welt gezeigt.Informationen zur Veranstaltungsfotografie