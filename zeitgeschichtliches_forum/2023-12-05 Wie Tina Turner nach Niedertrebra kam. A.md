---
id: wie-tina-turner-nach-niedertrebra-kam-amateurbands-in-der-ddr-05-12-2023
title: Wie Tina Turner nach Niedertrebra kam. Amateurbands in der DDR
start: 2023-12-05 19:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/wie-tina-turner-nach-niedertrebra-kam-amateurbands-in-der-ddr-05-12-2023
image: csm_Foto_MDR_Doku_Amateurbands_Copyright_Anja_Lehmann_fd9c5caceb.jpg
teaser: Filmvorführung und Gespräch (Saal)
isCrawled: true
---
NULL