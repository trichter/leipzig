---
id: tupoka-ogette-und-jetzt-du-tag-fuer-tag-aktiv-gegen-rassismus-2023-04-27t000000z
title: "Tupoka Ogette: Und jetzt du. / Tag für Tag aktiv gegen Rassismus"
start: 2023-04-27 21:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/tupoka-ogette-und-jetzt-du-tag-fuer-tag-aktiv-gegen-rassismus-2023-04-27t000000z
image: csm_2023-04-27_Ogette1_Leipzig_01ccaa31a2.jpg
teaser: Lesung (Saal)
isCrawled: true
---
Lesung und Gespräch Mit der Autorin Tupoka Ogette Moderation: Daniel Schulz (Journalist)Seit vielen Jahren ist die Bestsellerautorin Tupoka Ogette ("Exit Racism. Rassismuskritisch denken lernen", 2017) eine der bekanntesten Vermittlerinnen für Rassismuskritik in deutschsprachigen Raum. Ergänzend zu ihrem im letzten Jahr erschienen Buch "Und jetzt du. Rassismuskritisch leben" legt sie nun das Journal "Tag für Tag aktiv gegen Rassismus" vor. Jeder Monat steht im Zeichen eines Themas: Black History, Weiße Zerbrechlichkeit, Allyship. Und jede Woche warten Denkanstöße, Aufgaben, Porträts bedeutender Persönlichkeiten sowie Lese- oder Podcastempfehlungen. Tupoka Ogette lädt dazu ein, neue Perspektiven einzunehmen und die Welt jeden Tag ein bisschen offener und bunter zu gestalten. Ihre Bücher geben Anregungen, wie ein rassismuskritisches Leben gelingen kann.In Kooperation mit der Penguin Random House Verlagsgruppewww.penguinrandomhouse.deInformationen zur Veranstaltungsfotografie