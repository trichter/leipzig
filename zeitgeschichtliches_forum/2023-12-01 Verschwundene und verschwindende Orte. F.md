---
id: verschwundene-und-verschwindende-orte-folgen-der-braunkohlefoerderung-in-ostdeutschland-01-12-2023
title: Verschwundene und verschwindende Orte. Folgen der Braunkohleförderung in
  Ostdeutschland
start: 2023-12-01 09:30
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/verschwundene-und-verschwindende-orte-folgen-der-braunkohlefoerderung-in-ostdeutschland-01-12-2023
image: csm_Ferropolis_Jana_Schneider_092a328147.jpg
teaser: Tagung (Saal)
isCrawled: true
---
NULL