---
id: rechtsrock-in-sachsen-zwischen-lokalem-widerstand-und-politischer-ohnmacht-11-01-2024
title: Rechtsrock in Sachsen - Zwischen lokalem Widerstand und politischer Ohnmacht
start: 2024-01-11 19:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/rechtsrock-in-sachsen-zwischen-lokalem-widerstand-und-politischer-ohnmacht-11-01-2024
image: csm_Plakat_A2_Rechtsrock_22b6c7b362.jpg
teaser: Diskussion (Saal)
isCrawled: true
---
NULL