---
id: leipziger-buchpreis-zur-europaeischen-verstaendigung-2023-maria-stepanova-im-gespraech-2023-04-27t000000z
title: "Leipziger Buchpreis zur Europäischen Verständigung 2023: Maria Stepanova
  im Gespräch"
start: 2023-04-27 19:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/leipziger-buchpreis-zur-europaeischen-verstaendigung-2023-maria-stepanova-im-gespraech-2023-04-27t000000z
image: csm_2023-04-27_Stepanova_Leipzig_016dbdc38f.jpg
teaser: Lesung (Saal)
isCrawled: true
---
Lesung und GesprächMit der Preisträgerin des Leipziger Buchpreises zur Europäischen Verständigung 2023 Maria Stepanova, der Literaturwissenschaftlerin und Schriftstellerin Ilma Rakusa und der Literaturübersetzerin Olga Radetzkaja.Den Leipziger Buchpreis zur Europäischen Verständigung erhält in diesem Jahr die russische Lyrikerin Maria Stepanova für den Band „Mädchen ohne Kleider“. In der Begründung der Jury heißt es: „… Unablösbar in der Gegenwart und in der russischen Sprache von Alexander Puschkin über Ossip Mandelstam bis Marina Zwetajewa verankert, ist ihr Werk zugleich ein Hallraum der Weltliteratur, in dem Dante, Goethe und Walt Whitman ebenso anwesend sind wie Ezra Pound, Inger Christensen und Anne Carson. Die Gedichtzyklen – so liedhaft wie erzählerisch – führen eindrücklich vor, wie sich in aktuelle Poesie ein waches Geschichtsbewusstsein einschreibt“. Die Jury verweist auch auf den Lyrikband „Der Körper kehrt wieder“ (2020) und den Roman „Nach dem Gedächtnis“ (2020).Die viel gelobte Übertragung aus dem Russischen stammt von Olga Radetzkaja. Die Laudatio zur Preisverleihung hält die Literaturwissenschaftlerin und Schriftstellerin Ilma Rakusa. Maria Stepanova wurde 1972 in Moskau geboren und lebt derzeit im deutschen Exil. In Kooperation mit der Bundeszentrale für politische Bildung, der Leipziger Buchmesse, der Stadt Leipzig und dem Suhrkamp Verlag.Informationen zur Veranstaltungsfotografie