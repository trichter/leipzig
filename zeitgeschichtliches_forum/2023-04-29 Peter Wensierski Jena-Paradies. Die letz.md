---
id: peter-wensierski-jena-paradies-die-letzte-reise-des-matthias-domaschk-2023-04-29t000000z
title: "Peter Wensierski: Jena-Paradies. Die letzte Reise des Matthias Domaschk"
start: 2023-04-29 20:30
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/peter-wensierski-jena-paradies-die-letzte-reise-des-matthias-domaschk-2023-04-29t000000z
image: csm_2023-04-29_Wensierski_Leipzig_e8994ea4df.jpg
teaser: Lesung (Saal)
isCrawled: true
---
Moderation: Matthias Schmidt (MDR)Freitag, 10. April 1981: In Jena steigt der 23-jährige Matthias Domaschk in den Schnellzug nach Berlin. Er will zu einer Geburtstagsfeier. Doch er kommt nie an, denn der vollbesetzte Zug wird in Jüterbog gestoppt, Matthias und drei weitere Jenaer verhaftet. Zwei Tage später ist er tot, nach einem Verhör in der Stasi-Untersuchungshaftanstalt Gera. Was ist damals geschehen? Fesselnd erzählt Peter Wensierski das Drama der letzten Stunden im Leben eines jungen Mannes, der auf der Suche nach sich selbst und einer lebenswerten Gesellschaft ist. Wie Teile eines Puzzles lassen überraschende Rückblenden in sein Leben das Bild einer unangepassten Jugend in einer Diktatur entstehen.www.aufbau-verlage.de/ch-links-verlagInformationen zur Veranstaltungsfotografie