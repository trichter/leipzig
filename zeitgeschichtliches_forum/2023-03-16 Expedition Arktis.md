---
id: expedition-arktis-2023-03-16t000000z
title: Expedition Arktis
start: 2023-03-16 19:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/expedition-arktis-2023-03-16t000000z
teaser: Wissenschaftskino (Saal)
isCrawled: true
---
Dokumentarfilm „Expedition Arktis: Schweizer Forscher am Nordpol“ (Schweiz 2021, 51 Min, Regie: Philipp Grieß und Simon Sacha)Filmvorführung und GesprächMit Prof. Dr. Andreas Macke (Direktor des Leibniz-Instituts für Troposphärenforschung, Leipzig) und den MOSAIC-Expeditionsteilnehmern Dr. Julian Hofer, Christian Pilz, Dr. Ronny Engelmann, Dr. Hannes Griesche sowie Dr. Dietrich Althausen (angefragt) Moderation: Grit Krämer (Redakteurin, MDR) In Kooperation mit dem Leibniz-Institut für Troposphärenforschung und dem Helmholtz-Zentrum für UmweltforschungIn Film und Fernsehen verschwimmen die Grenzen von Fiktion und Realität. Im Rahmen der Film- und Diskussionsreihe WISSENSCHAFTSKINO erörtern Experten Leipziger Wissenschaftseinrichtungen gemeinsam mit dem Publikum, wo die Realität aufhört und Kino anfängt.Informationen zur Veranstaltungsfotografie