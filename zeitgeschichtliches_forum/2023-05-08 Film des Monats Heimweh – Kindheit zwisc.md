---
id: film-des-monats-heimweh-kindheit-zwischen-den-fronten-2023-05-08t000000z
title: "Film des Monats: Heimweh – Kindheit zwischen den Fronten"
start: 2023-05-08 19:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/film-des-monats-heimweh-kindheit-zwischen-den-fronten-2023-05-08t000000z
image: csm_2023-05-05_Film_des_Monat_2023_Quartal_2_Leipzig_91853155ba.jpg
teaser: Film (Saal)
isCrawled: true
---
Dokumentation (DK 2021, 88 Min, OmdU, Regie: Simon Lereng Wilmont) Filmvorführung In der Reihe „Ukraine – Kampf um Unabhängigkeit“   Schon seit 2014 wird in der Ostukraine gekämpft. Insbesondere Kinder sind die Leidtragenden. Im Kinderheim Priyut nahe der Frontlinie zu den Separatistengebieten finden Kinder aus zerrütteten Familien für eine Weile Zuflucht und Geborgenheit – weil sich eine Gruppe engagierter Erzieherinnen unermüdlich für sie einsetzt. Doch auch in dieser bedrückenden Welt gibt es Ressourcen, die unerschöpflich zu sein scheinen: Lächeln, Umarmungen und Fürsorge. Margarita und ihrem Team ist es gelungen, einen magischen Ort in einer umkämpften Region zu schaffen.Simon Lereng Wilmont, geboren 1975 in Kopenhagen, ist ein dänischer Filmregisseur und Kameramann. Er besuchte die Danske Filmskole in Dänemark und machte dort 2009 seinen Abschluss. Sein Debütfilm war „Traveling with Mr. T.“. Sein zweiter Film „Oleg, eine Kindheit im Krieg“ erzählt den Alltag in einem umkämpften Gebiet aus der Perspektive eines zehnjährigen ukrainischen Waisenjungen, der nahe der Front im Donbass lebt. Mit „Heimweh – Kindheit zwischen den Fronten“ beschäftigt sich Wilmont ein zweites Mal mit jungen Menschen in diesem Kriegsgebiet. Der Film wurde bei der Oscarverleihung 2023 als Bester Dokumentarfilm nominiert.Programm der Reihe „Ukraine – Kampf um Unabhängigkeit“Informationen zur Veranstaltungsfotografie