---
id: hits-hymnen-klang-der-zeitgeschichte-28-11-2023
title: Hits & Hymnen. Klang der Zeitgeschichte
start: 2023-11-28 15:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/hits-hymnen-klang-der-zeitgeschichte-28-11-2023
image: csm_Hits_und_Hymnen_@hdg_web_8742520455.jpg
teaser: Fortbildung (Atelier)
isCrawled: true
---
NULL