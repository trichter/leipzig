---
id: mit-sebastian-krumbiegel-durch-hits-hymnen-klang-der-zeitgeschichte-06-12-2023
title: Mit Sebastian Krumbiegel durch „Hits & Hymnen. Klang der Zeitgeschichte“
start: 2023-12-06 16:30
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/mit-sebastian-krumbiegel-durch-hits-hymnen-klang-der-zeitgeschichte-06-12-2023
image: csm_SebastianKrumbiegel2019_CreditEnricoMeyer_83706fae86.jpg
teaser: Ausstellungsbegleitung (Wechselausstellung)
isCrawled: true
---
NULL