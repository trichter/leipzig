---
id: wissenschaftskino-ich-bin-dein-mensch-09-11-2023
title: "ENTFÄLLT! Wissenschaftskino: Ich bin dein Mensch"
start: 2023-11-09 19:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/wissenschaftskino-ich-bin-dein-mensch-09-11-2023
image: csm_3_FOTO_Ich_bin_dein_Mensch_C5A0405__c_Christine_FenzlHP_f4b07bf43f.jpg
teaser: Filmvorführung und Gespräch (Saal)
isCrawled: true
---
NULL