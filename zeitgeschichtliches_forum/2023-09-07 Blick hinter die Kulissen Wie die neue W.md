---
id: blick-hinter-die-kulissen-wie-die-neue-wechselausstellung-hits-und-hymnen-entsteht-07-09-2023
title: 'Blick hinter die Kulissen: Wie die neue Wechselausstellung "Hits und
  Hymnen" entsteht'
start: 2023-09-07 16:30
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/blick-hinter-die-kulissen-wie-die-neue-wechselausstellung-hits-und-hymnen-entsteht-07-09-2023
image: csm_Hits_und_Hymnen_@hdg_web_8742520455.jpg
teaser: Ausstellungsbegleitung ()
isCrawled: true
---
NULL