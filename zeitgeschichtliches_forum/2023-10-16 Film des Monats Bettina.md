---
id: film-des-monats-bettina-16-10-2023
title: "Film des Monats: Bettina"
start: 2023-10-16 19:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/film-des-monats-bettina-16-10-2023
image: csm_Bettina_Foto_Werner_Popp_HP_20eba991ba.jpg
teaser: Film (Saal)
isCrawled: true
---
NULL