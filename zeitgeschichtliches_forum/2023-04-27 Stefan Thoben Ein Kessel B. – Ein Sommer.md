---
id: stefan-thoben-ein-kessel-b-ein-sommer-auf-bitterfelder-wegen-2023-04-27t000000z
title: "Stefan Thoben: Ein Kessel B. – Ein Sommer auf Bitterfelder Wegen"
start: 2023-04-27 17:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/stefan-thoben-ein-kessel-b-ein-sommer-auf-bitterfelder-wegen-2023-04-27t000000z
image: csm_2023-04-27_Thoben_Leipzig_5e7c4595d3.jpg
teaser: Buchvorstellung (Saal)
isCrawled: true
---
Mit Fotopräsentation"Gleich hinter Bitterfeld hört die Welt auf", erfährt Stefan Thoben als Viertklässler bei einer Spendenaktion für Kinder aus jener Stadt in der fernen DDR. Dreißig Jahre später besucht der gebürtige Oldenburger zum ersten Mal die einst "schmutzigste Stadt Europas" und entdeckt auf seiner Suche nach den Kindern von 1991/92 einen Ort, der nach der Wiedervereinigung einen radikalen und beispiellosen Strukturwandel durchgemacht hat. Auf seinen fotografischen Erkundungen entwickelt Stefan Thoben ein Gespür für die vielerorts unsichtbar gewordene Vergangenheit der Chemieregion. "Ein Kessel B." ist das Porträt einer Stadt im Um- und Aufbruch, eine Melange bunter Fotos und persönlicher Momente auf den Spuren von Monika Maron, Clemens Meyer, Klaus Staeck und dem Geist des Bitterfelder Wegs.In Kooperation mit dem Reiffer Verlagwww.verlag-reiffer.de