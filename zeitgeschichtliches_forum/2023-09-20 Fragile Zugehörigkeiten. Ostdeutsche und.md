---
id: fragile-zugehoerigkeiten-ostdeutsche-und-andere-im-vereinigungskontext-praesentation-des-neuen-jahrbuchs-deutsche-einheit-2023-20-09-2023
title: Fragile Zugehörigkeiten. Ostdeutsche und Andere im Vereinigungskontext
  Präsentation des neuen Jahrbuchs Deutsche Einheit 2023
start: 2023-09-20 18:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/fragile-zugehoerigkeiten-ostdeutsche-und-andere-im-vereinigungskontext-praesentation-des-neuen-jahrbuchs-deutsche-einheit-2023-20-09-2023
image: csm_09_20_Jahrbuch_Deutsche_Einheit_cover_web_3113097c9b.jpg
teaser: Buchvorstellung (Atelier)
isCrawled: true
---
NULL