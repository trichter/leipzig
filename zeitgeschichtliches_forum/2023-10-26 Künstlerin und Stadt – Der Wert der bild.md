---
id: kuenstlerin-und-stadt-der-wert-der-bildenden-kunst-26-10-2023
title: Künstler:in und Stadt – Ateliers in Leipzig? Wandel der Stadtlandschaft
  und Auswirkungen auf künstlerische Arbeit
start: 2023-10-26 18:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/kuenstlerin-und-stadt-der-wert-der-bildenden-kunst-26-10-2023
teaser: Podiumsdiskussion (Saal)
isCrawled: true
---
NULL