---
id: film-des-monats-dessau-dancers-08-01-2024
title: "Film des Monats: Dessau Dancers"
start: 2024-01-08 19:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/film-des-monats-dessau-dancers-08-01-2024
image: csm_10_OMEGA_1986_Foto_Herbert_Schulze_effd538bae.jpg
teaser: Film (Saal)
isCrawled: true
---
NULL