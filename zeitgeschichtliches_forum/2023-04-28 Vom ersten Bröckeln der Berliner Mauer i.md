---
id: vom-ersten-broeckeln-der-berliner-mauer-im-burgenland-2023-04-28t000000z
title: Vom ersten Bröckeln der Berliner Mauer im Burgenland
start: 2023-04-28 21:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/vom-ersten-broeckeln-der-berliner-mauer-im-burgenland-2023-04-28t000000z
teaser: Buchmesse (Saal)
isCrawled: true
---
Gastlandauftritt Österreich: Mit den Autorinnen Barbara Zeman, Katharina Tiwald, Karin Ivancsics und dem Autor Gerhard Altmann Moderation: Günter Kaindlstorfer (Autor und Journalist) Musik: Mira PerusichIn Kooperation mit dem Gastland Österreich bei der Leipziger Buchmesse"Bau ab und nimm mit!" lautete einer der Aufrufe der Paneuropa-Bewegung, die im Sommer 1989 per Flugblatt zu einem Picknick nahe der Grenze bei Sopron einluden. Viele Menschen aus der DDR verstanden die Botschaft und reisten in die Grenzregion. Am 19. August drückten schließlich hunderte Angereiste ein altes Holztor in einem Waldstück bei St. Margarethen im Burgenland auf, um in den Westen zu gelangen. Bis zu 600 Personen nutzten die provisorische Grenzöffnung für die erste Massenflucht seit dem Bau der Berliner Mauer 1961. Welche Erinnerungen an damals prägen das heutige Geschichtsbewusstsein? Was ist von der damaligen Euphorie auf beiden Seiten geblieben? Woran liegt es, dass heute das Ziehen von Grenzen wieder populärer zu sein scheint als das Abbauen von Grenzen? Diesen und weiteren Fragen gehen die Autorinnen Karin Ivancsics, Katharina Tiwald und Barbara Zeman sowie der Autor Gerhard Altmann nach. Es moderiert Günter Kaindlstorfer.https://gastland-leipzig23.at/Informationen zur Veranstaltungsfotografie