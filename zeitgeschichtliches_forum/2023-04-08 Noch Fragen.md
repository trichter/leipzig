---
id: noch-fragen-2023-03-04t000000z-6
title: Noch Fragen?
start: 2023-04-08 14:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/noch-fragen-2023-03-04t000000z-6
image: csm_23-02-23_UKRA-Ausstellungseroeffnung_Leipzig_77588cd07a.jpg
teaser: Begleitprogramm zur Ausstellung (Wechselausstellung "Unabhängigkeit!")
isCrawled: true
---
Antworten und Gespräche in der Wechselausstellung "Unabhängigkeit! Fotografien aus der Ukraine 1991–2022" jeden Samstag zwischen 14 und 16 Uhr, auf Deutsch und Ukrainisch   Am 6. Mai im Rahmen der Museumsnacht Leipzig/Halle von 19 bis 22 Uhr