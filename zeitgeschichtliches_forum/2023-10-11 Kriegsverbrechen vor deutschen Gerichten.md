---
id: kriegsverbrechen-vor-deutschen-gerichten-potentiale-und-huerden-des-angewandten-voelkerstrafrechts-11-10-2023
title: Kriegsverbrechen vor deutschen Gerichten. Potentiale und Hürden des
  angewandten Völkerstrafrechts
start: 2023-10-11 18:30
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/kriegsverbrechen-vor-deutschen-gerichten-potentiale-und-huerden-des-angewandten-voelkerstrafrechts-11-10-2023
teaser: Podiumsdiskussion (Saal)
isCrawled: true
---
NULL