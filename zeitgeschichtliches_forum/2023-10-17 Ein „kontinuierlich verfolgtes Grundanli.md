---
id: ein-kontinuierlich-verfolgtes-grundanliegen-senkung-der-saeuglingssterblichkeit-im-geteilten-deutschland-der-1960er-jahre-17-10-2023
title: Ein „kontinuierlich verfolgtes Grundanliegen“. Senkung der
  Säuglingssterblichkeit im geteilten Deutschland der 1960er Jahre
start: 2023-10-17 19:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/ein-kontinuierlich-verfolgtes-grundanliegen-senkung-der-saeuglingssterblichkeit-im-geteilten-deutschland-der-1960er-jahre-17-10-2023
image: csm_Bild_BArch_HP_97312c93d6.jpg
teaser: Vortrag und Gespräch (Forum live)
isCrawled: true
---
NULL