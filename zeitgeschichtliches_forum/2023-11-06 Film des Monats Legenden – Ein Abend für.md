---
id: film-des-monats-legenden-ein-abend-fuer-tamara-danz-06-11-2023
title: "Film des Monats: Legenden – Ein Abend für Tamara Danz"
start: 2023-11-06 19:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/film-des-monats-legenden-ein-abend-fuer-tamara-danz-06-11-2023
image: csm_Tamara_Danz_Bildrechte_MDRHP_2149efa265.jpg
teaser: Film (Saal)
isCrawled: true
---
NULL