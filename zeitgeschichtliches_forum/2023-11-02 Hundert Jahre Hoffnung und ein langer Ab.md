---
id: hundert-jahre-hoffnung-und-ein-langer-abschied-zur-geschichte-der-sozialdemokratie-02-11-2023
title: Hundert Jahre Hoffnung und ein langer Abschied. Zur Geschichte der
  Sozialdemokratie
start: 2023-11-02 19:00
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/hundert-jahre-hoffnung-und-ein-langer-abschied-zur-geschichte-der-sozialdemokratie-02-11-2023
image: csm_Cover_SeebacherHP_353f38559a.jpg
teaser: Buchvorstellung (Saal)
isCrawled: true
---
NULL