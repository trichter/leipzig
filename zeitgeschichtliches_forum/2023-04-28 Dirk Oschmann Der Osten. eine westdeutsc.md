---
id: dirk-oschmann-der-osten-eine-westdeutsche-erfindung-2023-04-28t000000z
title: "Dirk Oschmann: Der Osten. eine westdeutsche Erfindung"
start: 2023-04-28 19:30
address: Grimmaische Straße 6
link: https://www.hdg.de/zeitgeschichtliches-forum/veranstaltungen/dirk-oschmann-der-osten-eine-westdeutsche-erfindung-2023-04-28t000000z
image: csm_2023-04-28_Oschmann_Leipzig_2231b54142.jpg
teaser: Buchvorstellung (Saal)
isCrawled: true
---
Mit dem Autor Dirk OschmannModeration: Tobias Gohlis (Journalist und Literaturkritiker)Gibt es eine "Ost-Identität"? Was macht sie aus? Und welche Rolle spielen die vielfach damit verknüpften Attribute Populismus, mangelndes Demokratieverständnis, Rassismus, Verschwörungsmythen und Armut? Der Leipziger Germanist Dirk Oschmann stellt in seinem Bestseller die These auf, dass der Westen sich über dreißig Jahre nach dem Mauerfall noch immer als Norm definiere und den Osten als Abweichung sehe. Medien, Politik, Wirtschaft und Wissenschaft, so Oschmann, würden von westdeutschen Perspektiven dominiert. Pointiert durchleuchtet der Autor die gesellschaftlichen Folgen dieses Otherings. "Der Osten hat keine Zukunft, solange er nur als Herkunft begriffen wird", so Oschmanns Fazit.In Kooperation mit Ullstein Buchverlagewww.ullstein.deInformationen zur Veranstaltungsfotografie