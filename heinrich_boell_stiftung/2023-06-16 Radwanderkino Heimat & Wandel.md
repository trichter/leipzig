---
id: "153715"
title: Radwanderkino Heimat & Wandel
start: 2023-06-16 21:00
end: 2023-06-16 22:45
link: https://calendar.boell.de/de/node/153715/
isCrawled: true
---
Mit dem Rad sind wir in Leipzigs Stadtteil Schönefeld im Nordosten der Stadt unterwegs. Rund 15 Kilometer geht es kreuz und quer durch den Stadtteil. Wir genießen Fahrradfreude mit Musik und Kurzfilme aus den „Stories of Change“. Popkorn und Getränke rollen mit.

 Die Teilnahme ist kostenfrei im Rahmen der Stadt-Land-Expedition Schönefeld.

**Zeit: Freitag, 16. Juni // 21:00 - 22:45 Uhr

 Start: 04107 Leipzig // Rohrteichstraße**

[Alle Kurzfilme der Stories of Change e.V.](<https://www.stories-of-change.org/>) im Überblick.

Eine Veranstaltung von Leipziggruen in Kooperation mit Stories of Change e.V. und Weiterdenken - Heinrich-Böll-Stiftung Sachsen e.V..

 Die Veranstaltung wird mitfinanziert durch Steuermittel auf Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.