---
id: "154667"
title: ZÜRN | Eine Lesereihe über Wut und Gewalt
start: 2023-09-20 19:00
end: 2023-09-20 20:30
locationName: “Besser Leben“
address: Holbeinstraße 2, Leipzig
link: https://calendar.boell.de/de/node/154667/
isCrawled: true
---
**Zürn**, die neue Leipziger Lesereihe, die sich mit künstlerischen Erkundungen von Wut und Gewalt beschäftigt, geht in die zweite Runde. Diesmal geht es um das „Gesetz der Väter“, das wir alle allzu früh kennen lernen, innerhalb und außerhalb der Familie. In den Texten der Gästinnen Daniela Dröscher und Janin Wölke werden Töchterwut und Mutterrollen verhandelt, Väter, die über weibliche Körper dominieren - oder einfach aus dem Leben ihrer Kinder verschwinden, ohne dafür gesellschaftlich geächtet zu werden.



**Daniela Dröscher** lebt als freie Autorin in Berlin. Ihre einflussreichen autofiktionalen Bücher „Zeige deine Klasse“ (2018) und „Lügen über meine Mutter“ (auf der Shortlist zum Deutschen Buchpreis 2022) befassen sich mit Sexismus und Klassismus vor dem Hintergrund deutscher Geschichte und Gegenwart.



**Janin Wölke** lebt als Autorin und Lehrerin in Leipzig. 2014 erschien ihr Gedichtband „was passiert wirklich, wenn wir stolpern?“. In ihren lyrischen, dramatischen und erzählerischen Texten beschäftigt sie sich auf eindrückliche Weise mit den Themen Kindheit, Wut und Gewalt.



**Moderation und Konzept:**

**Özlem Özgül Dündar **und** Sibylla Vričić Hausmann**



**Zeit:** 20\. September 2023 // 19 Uhr

**Ort:** “Besser Leben“ // Holbeinstraße 2 // Leipzig

In Kooperation mit:

 Weiterdenken - Heinrich Böll Stiftung Sachsen,

 gefördert durch das Kulturamt der Stadt Leipzig