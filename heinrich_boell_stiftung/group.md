---
name: weiterdenken - Heinrich-Böll-Stiftung
website: https://www.weiterdenken.de/de
scrape:
  source: boell
  options:
    filter:
      - stadt:2469
---
Weiterdenken ist eine Einrichtung der politischen Bildung für Erwachsene in Sachsen. Mit Seminaren, Workshops, Vorträgen, Ausstellungen, Veröffentlichungen, Lesungen und künstlerische Annäherungen an politische Themen unterstützen wir in Sachsen, Ideen, Orientierung, Engagement und konkrete Konzepte für die sozialen und ökologischen Lebensgrundlag...