---
id: "154111"
title: Feministisch, intersektional und solidarisch Sachsen zusammen gestalten |
  Kongress in Leipzig
start: 2023-09-15 09:30
end: 2023-09-16 17:00
locationName: Tapetenwerk
address: Lützner Straße 91, Leipzig
link: https://calendar.boell.de/de/node/154111/
isCrawled: true
---
- <span><span dir="ltr">Wie</span><span dir="ltr">¨NBSP;</span><span dir="ltr">bekommen wir mehr</span><span dir="ltr"> </span><span dir="ltr">vielfältige Perspektiven in Sachsen</span><span dir="ltr"> </span><span dir="ltr">–</span><span dir="ltr"> </span><span dir="ltr">in Politik, Verwaltung</span><span dir="ltr"> und zivilgesellschaftlichen Strukturen?</span></span>

- <span><span dir="ltr">Was bedeuten "Empowerment" und "Powersharing" in unserem Alltag?</span></span>

- <span><span dir="ltr">Wie stärken wir uns gegenseitig?</span></span>

- <span><span dir="ltr">Wie partizipieren Frauen* und queere Me</span><span dir="ltr">nschen mit Flucht</span><span dir="ltr">-</span><span dir="ltr"> </span><span dir="ltr">und</span></span>

     <span><span dir="ltr">Migrationsgeschichte und BIPoC (Black Indigenous People of Color) in Sachsen?</span></span>


<!-- -->

<span><span dir="ltr">Herzlich eingeladen sind alle Multiplikatorinnen* und aktiven Frauen*, Trans*</span><span dir="ltr">-</span><span dir="ltr">, Inter*</span><span dir="ltr"> und nicht</span><span dir="ltr"> </span><span dir="ltr">-</span><span dir="ltr"> </span><span dir="ltr">binären Menschen mit unterschiedlichen Positionierungen</span><span dir="ltr">, Ressourcen und</span><span dir="ltr"> Erfahrungen.</span></span>





<span><span dir="ltr">In</span><span dir="ltr"> </span><span dir="ltr">unterschiedlichen</span><span dir="ltr"> </span><span dir="ltr">Formaten</span><span dir="ltr"> </span><span dir="ltr">zum</span><span dir="ltr"> </span><span dir="ltr">Diskutieren,</span><span dir="ltr"> </span><span dir="ltr">Nachdenken</span><span dir="ltr"> </span><span dir="ltr">aber</span><span dir="ltr"> </span><span dir="ltr">auch</span><span dir="ltr"> </span><span dir="ltr">zum</span><span dir="ltr"> Wohlfühlen</span><span dir="ltr"> </span><span dir="ltr">(wie</span><span dir="ltr"> </span><span dir="ltr">Erzählcafé,</span><span dir="ltr"> </span><span dir="ltr">Podium,</span><span dir="ltr"> </span><span dir="ltr">Interaktiver</span><span dir="ltr"> </span><span dir="ltr">Galerie,</span><span dir="ltr"> </span><span dir="ltr">Workshops,</span><span dir="ltr"> </span><span dir="ltr">Musik,</span><span dir="ltr">Bewegung und Entspannung) wollen wir uns kennenlernen, Erfahrungen</span><span dir="ltr"> </span><span dir="ltr">und Wissen</span><span dir="ltr"> teilen, uns vernetzen und uns gegenseitig stärken.</span><span dir="ltr"> </span><span dir="ltr">Wir sind unterschiedlich, wir machen</span><span dir="ltr"> Fehler, aber wir können zusammen viel erreichen: Darüber wollen wir sprechen.</span></span>





<span><span dir="ltr">Begleitet</span><span dir="ltr"> </span><span dir="ltr">wird</span><span dir="ltr"> </span><span dir="ltr">die</span><span dir="ltr"> </span><span dir="ltr">gesamte</span><span dir="ltr"> </span><span dir="ltr">Veranstaltung</span><span dir="ltr"> </span><span dir="ltr">durch</span><span dir="ltr"> </span><span dir="ltr">Dolmetscherinnen*</span><span dir="ltr"> </span><span dir="ltr">in</span><span dir="ltr"> unterschiedlichen S</span><span dir="ltr">prachen je nach Bedarf. Eine Kinderbetreuung wird angeboten.</span><span dir="ltr">Weitere Unterstützung ist möglich.</span></span>





**Zeit:** 15\./16. September 2023 // 9:30 bis 18:30 Uhr und 9:30 bis 17:00 Uhr



**Ort: **Tapetenwerk // Lützner Straße 91 // Leipzig

<span><span dir="ltr">Eine</span><span dir="ltr"> </span><span dir="ltr">Veranstaltung</span><span dir="ltr"> </span><span dir="ltr">des</span><span dir="ltr"> </span><span dir="ltr">Genderkompetenzzentrum</span><span dir="ltr"> </span><span dir="ltr">Sachsen</span><span dir="ltr"> </span><span dir="ltr">(FrauenBildungsHaus</span><span dir="ltr"> Dresden e.V.) in Kooperation mit *sowieso* Frauen für Frauen e.V., AG Asylsuchende</span><span dir="ltr"> S</span><span dir="ltr">ächsische Schweiz Ostzergebirge e.V., Frauentreff Ausländerrat Dresden e.V., MEDEA</span><span dir="ltr"> International</span><span dir="ltr"> </span><span dir="ltr">-</span><span dir="ltr"> </span><span dir="ltr">FMGZ</span><span dir="ltr"> </span><span dir="ltr">MEDEA</span><span dir="ltr"> </span><span dir="ltr">e.V.,</span><span dir="ltr"> </span><span dir="ltr">DaMigra</span><span dir="ltr"> </span><span dir="ltr">e.V.</span><span dir="ltr"> </span><span dir="ltr">-</span><span dir="ltr"> </span><span dir="ltr">Dachverband</span><span dir="ltr"> </span><span dir="ltr">der</span><span dir="ltr"> Migrantinnenorganisationen, konzeptwerk neue ökonomie Leipzig, Haus der Frauen</span><span dir="ltr"> Zwickau e.V.</span><span dir="ltr">, Weiterdenken He</span><span dir="ltr">inrich-Böll-Stiftung Sachsen</span><span dir="ltr"> </span><span dir="ltr">und den Multiplikatorinnen*</span><span dir="ltr"> Carina Flores</span><span dir="ltr">,</span><span dir="ltr"> </span><span dir="ltr">Douha</span><span dir="ltr"> </span><span dir="ltr">Al</span><span dir="ltr">-</span><span dir="ltr">Fayyad</span><span dir="ltr"> </span><span dir="ltr">und Luciana Cristina Marinho Schollmeier.</span></span>