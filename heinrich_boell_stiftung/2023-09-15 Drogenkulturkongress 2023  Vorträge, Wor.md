---
id: "154839"
title: Drogenkulturkongress 2023 | Vorträge, Workshops und Präsentationen rund
  um das Thema Drogen
start: 2023-09-15 10:00
end: 2023-09-17 00:00
locationName: „Klinge22“
address: Klingenstr. 22 in 04229 Leipzig-Plagwitz
link: https://calendar.boell.de/de/node/154839/
isCrawled: true
---
Schnall dich an für einen außergewöhnlichen Rausch der Erkenntnis und Inspiration! Vom 15. bis 17. September 2023 findet in Leipzig der Kongress zur Drogenkultur statt – und du bist mittendrin! Tauche ein in eine psychedelische Fusion aus informativen Vorträgen, aufregenden Workshops, visionärer Kunst und pulsierender Musik, die deine Synapsen zum Funken bringen.

Vergiss das Klischee und her mit der Lebensrealität! Unser Kongress klärt auf, enttabuisiert und bringt Licht ins Dunkel von Drogenmythen. Hier dreht sich alles um Fakten, Offenheit und Verständnis. In einer Atmosphäre frei von Vorurteilen kannst du dich mit dem vielschichtigen Thema „Drogenkultur“ auseinandersetzen und echte Verbindungen knüpfen.

Egal, ob erfahrener Entdecker oder neugieriger Einsteiger, die Drogenkultur 2023 heißt dich herzlich willkommen. Werde Teil einer einzigartigen Community von Gleichgesinnten, die ihre Erfahrungen teilen und voneinander lernen.

Teil des Kongresses ist auch in diesem Jahr wieder die Ausstellung[ "DrogenKultur"](<https://www.boell-thueringen.de/de/drogenkultur-die-ausstellung> "") der HBS Thüringen.



**Veranstaltungsort:** „Klinge22“, Klingenstr. 22 in 04229 Leipzig-Plagwitz

**Tickets **auf der Veranstaltungswebseite: **[https://drogenkultur.de](<https://drogenkultur.de>)**

---

**Aus dem Programm:**

*Vorträge, Workshops u.a. zu den Themen*

● Konsumkompetenz erlernen (Roman Lemke \| Sucht & Ordnung)

● Der stärkste Stoff – Psychedelische Drogen: Waffe, Rauschmittel, Medikament (Norman Ohler)

● Das Silicon-Valley aus dem Geiste des LSD. Zwischen psychedelischen und virtuellen Wirklichkeiten – Eine ideengeschichtliche Netzwerkanalyse (Tom Saborowski)

● How-to: Kriminalisierung und Stigmatisierung beenden (Philine Edbauer \| #MBMC)

● Microdosing – Psychedelika im Alltag (Maximilian Wüsten \| Psychedelische Reise)

● Umgang mit Verschwörungserzählungen und rechtsextremistischen Gedankengut in der psychedelischen Szene (Dirk Netter)

● Einführung in die Kultivierung von Pilzen (Michael Ganslmeier)

● Sexuelle Übergriffe bei Retreats und Zeremonien – Workshop zur Aufklärung und Prävention (MagicFem\*Room \| PSL)

● How not to take Drugs – Oder wie man sicher auf die andere Seite kommt (Nadja Schollenberger \| PSB)



Weitere Infos und Tickets auf der Veranstaltungswebseite: **[https://drogenkultur.de](<https://drogenkultur.de>)**