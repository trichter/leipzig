---
id: "155045"
title: Zürn | Eine Lesereihe über Wut und Gewalt
start: 2023-10-25 19:00
end: 2023-10-25 21:00
locationName: Pöge-Haus
address: Hedwigstraße 20, Leipzig
link: https://calendar.boell.de/de/node/155045/
isCrawled: true
---
Zürn – die neue Leipziger Lesereihe über Wut und Gewalt – widmet sich im Oktober den Fragen: Wie können Krieg und staatliche Repression dichterisch zur Sprache kommen? Wie lassen sich emotionale und körperliche Erschütterungen einfangen, wenn die Alltagssprache verstummen muss oder unwirksam bis verfälschend wird? Welche Rolle spielen die sprachlichen Darstellungen im politischen Diskurs und in der Berichterstattung? Wer kann/darf be-schreiben? Was bewirken persönliche Involviertheit und Nähe-Distanz zum gewaltvollen Geschehen? Wie involviert muss eine\*r sein, um in unserer Kultur trauern zu dürfen? Ist Trauer manchmal ähnlich tabuisiert wie Wut (es für Frauen\* ist)? Wir glauben, dass poetische Sprache eine Sprache ist, die die Wahrheit sucht. Und freuen uns in diesem Sinne sehr auf die Gedichte und Meinungen unserer Gäst\*innen Anja Utler und Uladzimir Liankievic.

**Anja Utler **lebt als Dichterin und Übersetzerin in Leipzig. Aus ihrem aktuellen Gedichtband "Es beginnt. Trauerrefrain" spricht ihre Anteilnahme mit den Ukrainer\*innen, die seit Februar 2022 im Krieg leben müssen. Zugleich ist er eine Suche nach haltbaren politischen und poetologischen Überzeugungen.

**Uladzimir Liankievic **ist ein belarussicher Autor, der in Minsk lebt. Eine Auswahl seiner Gedichte wurden in der Anthologie "VERSschmuggel. Poesie aus Belarus und Deutschland" ins Deutsche übertragen. Sie „fangen wie ein Stimmentheater die rohe Gewalt in einem repressiven Staat ein." (Claudia Kramatschek, SWR2)

**Moderation und Konzept:**

 Özlem Özgül Dündar und Sibylla Vričić Hausmann

ORT: Pöge-Haus, Hedwigstraße 20, Leipzig

In Kooperation mit: Weiterdenken - Heinrich Böll Stiftung Sachsen, gefördert durch das Kulturamt der Stadt Leipzig