---
id: "154067"
title: Misogynie als Massenunterhaltung | Veronika Kracher über
  antifeministische Online-Kampagnen
start: 2023-08-24 19:00
end: 2023-08-24 21:00
locationName: Wohn- und Kulturprojekt B12
address: Braustraße 20, Leipzig (Zugang nicht barrierefrei)
link: https://calendar.boell.de/de/node/154067/
isCrawled: true
---
**Ort: <span>Wohn- und Kulturprojekt B12, <a href="https://www.openstreetmap.org/way/40905481">Braustraße 20, Leipzig</a>¨NBSP;</span>

***<span>(Zugang nicht barrierefrei)</span>

*

**Vortrag: Zur Wirkungsweise und Funktion antifeministischer Hasskampagnen am Beispiel Amber Heard**

Jede öffentlich präsente FLINTA-Person kennt es: Drohnachrichten, Beleidigungen, sexistische und queerfeindliche Angriffe, im schlimmsten Falle Shitstorms oder großflächige, andauernde Kampagnen. Sie sind Ausdruck von Misogynie: der Strafe des Patriarchats, wenn sich FLINTA patriarchalen Anforderungen an Weiblichkeit verweigern. Es ist bezeichnend, dass diese Hasskampagnen besonders oft Feminist\*innen zum Ziel haben. Gleichzeitig dienen diese Angriffe auch immer als Warnung an andere: Wenn sie sich feministisch äußern, werden sie ebenfalls als Feindbild markiert.

Eine der größten misogynen Kampagnen der letzten Jahre traf die Schauspielerin Amber Heard, die von ihrem Ex-Mann Johnny Depp wegen Verleumdung verklagt wurde. Ihr Vergehen? Sie hatte es gewagt, darüber zu schreiben, was einer Frau passiert, die häusliche Gewalt öffentlich thematisiert. Der öffentlich ausgestrahlte Prozess, der im Juni 2022 endete, wurde von einer vor allem über Social Media geführten Angriffs- und Desinformationswelle begleitet, die ihresgleichen sucht. Heard - also eine Frau, die gezwungen war, öffentlich über ihr zugefügte Gewalt zu sprechen - wurde in Millionen Videos, Memes und TikToks global verhöhnt. Die Welt hat schamlos und bösartig daran partizipiert, ein Gewaltopfer zu retraumatisieren und zu demütigen - und somit ein fatales Signal für alle Frauen gesetzt, die sich gegen patriarchale Gewalt zur Wehr setzen. Denn bittere Tatsache ist: was Heard passiert, wiederfährt in einem patriarchalen System, das Täter anstatt Betroffene schützt, ungezählten anderen Opfern häuslicher Gewalt.

Dieser Vortrag analysiert nicht nur die Mechanismen hinter einer Online-Kampagne, sondern setzt sie auch mittels feministischer und sozialpsychologischer Analysen in einen gesamtgesellschaftlichen Kontext und klärt über die Ideologie und Motivation der Angriffe auf: Misogynie.

Ein Vortrag von [Veronika Kracher](<https://www.ventil-verlag.de/autor/1549/veronika-kracher>)

*Eine Veranstaltung des <span>Wohn- und Kulturprojekt B12 in Zusammenarbeit mit Weiterdenken - Heinrich-Böll-Stiftung Sachsen. </span>

*

*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des von den Abgeordneten des Sächsischen Landtags beschlossenen Haushalts.*