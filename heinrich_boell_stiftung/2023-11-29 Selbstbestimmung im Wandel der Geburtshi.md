---
id: "155577"
title: Selbstbestimmung im Wandel der Geburtshilfe  | Vortrag & Diskussion mit
  einer Hebamme
start: 2023-11-29 19:00
end: 2023-11-29 21:00
locationName: Kunterbunte 19
address: Georg-Schwarz-Str. 19, Leipzig
link: https://calendar.boell.de/de/node/155577/
isCrawled: true
---
Im Selbstverständnis der Hebammen, in der praktizierten Geburtshilfe, in Vorbereitungen auf Geburten oder in Ratgebern spielt die Förderung der Selbstbestimmung eine zentrale Rolle. Die Grundsteine wurden bereits in der Frauengesundheitsbewegung der 1960er/70er gelegt. Doch die gegenwärtige klinische Geburtshilfe steht unter verstärkten Einflüssen der Ökonomisierung.

Auch heute wird das Thema selbstbestimmte Geburt in feministischen Kreisen weiter diskutiert und in den Fokus genommen. Doch wo sind die Grenzen? Und wie hat sich die Geburtshilfe im Laufe der Zeit gewandelt?



 Eingeladen ist eine Hebamme, die einen Einblick in ihren beruflichen Alltag sowie über die Hürden der Selbstbestimmung in der Praxis geben wird.

**Zeit:** 29\. November 2023 // 19 Uhr

**Ort:** Kunterbunte 19, Georg-Schwarz-Str. 19, Leipzig



 Die Veranstaltung findet in Kooperation mit dem Jugendphase e.V. und Weiterdenken Heinrich-Böll-Stiftung Sachsen statt.