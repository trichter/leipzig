---
id: "153367"
title: Sachsen rechts unten | Vorstellung der Publikation 2023
start: 2023-05-24 19:30
end: 2023-05-24 21:00
locationName: Saal im Pöge-Haus
address: Hedwigstraße 20, Leipzig (barrierearmer Zugang, barrierearmes WC)
link: https://calendar.boell.de/de/node/153367/
isCrawled: true
---
**Ort: **<span lang="DE" xml:lang="DE"><span><span><strong>Saal im <a href="https://www.pöge-haus.de/de/">Pöge-Haus</a>, Hedwigstraße 20, Leipzig</strong> (</span></span></span>

barrierearmer Zugang, barrierearmes WC)

Mehr zur Veranstaltung erfahren Sie hier ab 3. Mai.

<span><span><span lang="DE" xml:lang="DE"><span>Die Publikation „Sachsen rechts unten 2023“ des Kulturbüro Sachsen liegt auf der Veranstaltung bereit und kann zeitnah hier bestellt werden. Sie ist in Kooperation mit <em>Weiterdenken – Heinrich-Böll-Stiftung Sachsen</em> und der <em>Amadeu Antonio Stiftung </em>entstanden. </span></span></span></span>

Das Kulturbüro Sachsen gibt den Report "Sachsen rechts unten" jährlich heraus.

**Antidiskriminierungsregel**

 Den Veranstaltenden ist ein respektvolles und diskriminierungsfreies Miteinander sehr wichtig. Störungen oder Beleidigungen führen zum Ausschluss aus der Veranstaltung. Die Veranstaltenden behalten sich vor, von ihrem Hausrecht Gebrauch zu machen und Personen, die rechtsextremen Parteien oder Organisationen angehören, der rechtsextremen Szene zuzuordnen sind oder bereits in der Vergangenheit durch rassistische, nationalistische, antisemitische oder sonstige menschenverachtende Äußerungen in Erscheinung getreten sind, den Zutritt zur Veranstaltung zu verwehren oder von dieser auszuschließen.

<span><span><span><span lang="DE" xml:lang="DE"><span>Eine gemeinsame Veranstaltung von <em>Kulturbüro Sachsen e.V.</em> und <em>Weiterdenken – Heinrich-Böll-Stiftung Sachsen.</em></span></span></span></span></span>

<span><span><span><span lang="DE" xml:lang="DE"><span><em>Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des von den Abgeordneten des Sächsischen Landtags beschlossenen Haushalts.</em></span></span></span></span></span>