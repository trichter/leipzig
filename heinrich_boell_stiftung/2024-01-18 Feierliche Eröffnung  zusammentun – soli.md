---
id: "155717"
title: Feierliche Eröffnung | zusammentun – solidarisch gegen rechts
start: 2024-01-18 17:30
end: 2024-01-18 19:00
address: Galerie KUB | Kantstr. 18 | Leipzig
link: https://calendar.boell.de/de/node/155717/
isCrawled: true
---
<span><span><span lang="DE" xml:lang="DE">Rechte Hetze und Gewalt sind alltäglich. Extreme Rechte gewinnen Wahlen und vernetzen sich. Doch viele tun sich zusammen, um dem entgegenzutreten. Um sie geht es in der Ausstellung: Politische Gruppen sprechen über Alltag und Highlights, Scheitern und Angriffe, Hoffnung und Verbündete. Die Ausstellung streift Fragen rund um solidarisches Handeln: </span>Was macht eine Tätigkeit solidarisch und politisch? Was passiert, wenn Menschen sich zusammentun? Woher kommen Hoffnung und Solidarität angesichts rechter Bedrohung und Gewalt? </span></span>

Zur Eröffnung der Ausstellung [**zusammentun – solidarisch gegen rechts**](<www.weiterdenken.de/zusammentun-ausstellung>) sind zu Gast:

- Sinti Union Schleswig-Holstein
- die Kontinuierilchen aus Gera
- Allmende Taucha
- Hannah Eitel, Konzeption der Ausstellung
- Julia Schulze Wessel, Vorständin von Weiterdenken

<!-- -->

<span><span>Die <strong>Sinti Union</strong> berät und empowert Sinti und Roma. <strong>Die Kontinuierlichen</strong> machen Bildungsveranstaltungen. Der Verein <strong>Allmende Taucha</strong> berät Solidarische Landwirtschaften und wurde dafür mehrfach angegriffen. Die drei Gruppen sind grundverschieden; das Engagement gegen rechts ist ihnen gemeinsam. Bei der Vernissage kommen sie miteinander und mit den Gästen ins Gespräch. </span></span>

<span><span>Im Anschluss gibt es Gelegenheit, die Ausstellung anzusehen und mit den Gästen und Macher*innen ins Gespräch zu kommen.</span></span>

**<span lang="DE" xml:lang="DE"><span>Ort: <a href="https://www.galeriekub.de/">Galerie KUB</a> | </span></span>

Kantstr. 18 \| Leipzig

 Öffnungszeiten: Donnerstag-Sonntag 16-19 Uhr**

**[<span><span>Mehr zu Ausstellung</span></span>

](<https://calendar.boell.de/de/event/zusammentun-solidarisch-gegen-rechts>)**

Weitere Veranstaltung:

**<span lang="DE" xml:lang="DE"><span>Lesung: </span></span>

Wo ist die Solidarität? **Judenhass Underground – Antisemitismus in emanzipatorischen Subkulturen und Bewegungen <span lang="DE" xml:lang="DE"><span>am Dienstag, 23. Januar um 19 Uhr (<a href="https://calendar.boell.de/de/event/wo-ist-die-solidaritaet">mehr erfahren</a>)</span></span>

**Antidiskriminierungsregel**

 Den Veranstaltenden ist ein respektvolles und diskriminierungsfreies Miteinander sehr wichtig. Störungen oder Beleidigungen führen zum Ausschluss aus der Veranstaltung. Die Veranstaltenden behalten sich vor, von ihrem Hausrecht Gebrauch zu machen und Personen, die rechtsextremen Parteien oder Organisationen angehören, der rechtsextremen Szene zuzuordnen sind oder bereits in der Vergangenheit durch rassistische, nationalistische, antisemitische oder sonstige menschenverachtende Äußerungen in Erscheinung getreten sind, den Zutritt zur Veranstaltung zu verwehren oder von dieser auszuschließen.

*<span>Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.</span>

*