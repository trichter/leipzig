---
id: "152629"
title: Re*mapping. Eine neue Bewegung. Leipzig | Feministisches
  Vernetzungstreffen und App-Release
start: 2023-03-19 14:00
end: 2023-03-19 22:00
locationName: Conne Island
address: Koburger Str. 3, Leipzig
link: https://calendar.boell.de/de/node/152629/
isCrawled: true
---
**"Eine neue Bewegung: Re\*mapping Leipzig"** ist eine** App**, die es ermöglicht, <span><span>Leipziger feministische Geschichte(n) in multimedialen Spaziergängen zu erkunden. Künstlerisch macht die App historische und gegenwärtige Kämpfe, Aktivitäten und Bewegungen im öffentlichen Raum digital sichtbar.&nbsp;Die Video-Performance-Walks, Hörstücke, Comics und Augmented-Reality-Interventionen in der App sind als experimentelle Kooperationen zwischen den beteiligten Autor:innen, Aktivist:innen, Künstler:innen, Programmierer:innen, Grafiker:innen und Kurator:innen entstanden.&nbsp;</span></span>

Am **19\. März 2023**<span><span> lädt das Büro für kulturelle Übersetzung zum <strong>Launch der WebApp “Eine neue Bewegung: Re*Mapping Leipzig”</strong> und zum Feiern im Conne Island ein! Zusammen mit Künstler*innen und Autor*innen wird die App vorgestellt. Das Programm besteht aus Lesungen und Performances, Auszügen aus Videos und Soundstücken der App. Außerdem wird es eine Gesprächsrunde mit kurzen Berichten von Akteur*innen über ihre feministische/politische/aktivistische/solidarische Arbeit, ihre Errungenschaften, ihre Wünsche, Widersprüche und Kämpfe geben. Im Fokus steht insbesondere die Frage nach der Sichtbarkeit feministischer Politik in ihren queeren und intersektionalen Ausprägungen. Alle Anwesenden sind eingeladen den Austausch durch Wortbeiträge mitzugestalten. Außerdem: Austausch und Vernetzung, Spaß, Essen, Trinken und Tanzen.</span></span>

<span><span>Musikalisch wird der Tag von einem feministischen DJ-Kollektiv begleitet, organisiert von Connie Walker aka CFM, der Musikerin und Künstlerin, die seit 2002 Akteurin des Leipziger Netzwerks propellas/caramba!rec ist.&nbsp;</span></span>

<span><strong><span>Special Guest der Veranstaltung ist das freie Performancekollektiv CHICKS*!&nbsp;</span></strong></span>



**Zeit:** 19\. März 2023 // 14 bis 22 Uhr

**Ort:**<span><span lang="de-DE" xml:lang="de-DE"> Conne Island </span></span>

<span><span lang="de-DE" xml:lang="de-DE">// Koburger Str. 3 // Leipzig</span></span>

<span><span lang="de-DE" xml:lang="de-DE">Die Veranstaltung ist eine Kooperation zwischen dem Büro für kulturelle Übersetzung und Weiterdenken Heinrich-Böll-Stiftung Sachsen e.V.</span></span>