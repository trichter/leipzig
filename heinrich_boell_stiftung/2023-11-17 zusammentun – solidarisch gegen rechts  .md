---
id: "155605"
title: zusammentun – solidarisch gegen rechts | Ausstellung in Leipzig
start: 2024-01-18 16:00
end: 2024-01-28 19:00
address: Galerie KUB | Kantstr. 18 | Leipzig
link: https://calendar.boell.de/de/node/155605/
isCrawled: true
---
*<span>eine Ausstellung darüber, wie wir uns der extremen Rechten stellen: gemeinsam und solidarisch – über Alltag und Highlights, Scheitern und Angriffe, Hoffnung und Verbündete</span>

*

<span><span><span lang="DE" xml:lang="DE">Rechte Hetze und Gewalt sind alltäglich. Extreme Rechte gewinnen Wahlen und vernetzen sich. Doch viele tun sich zusammen, um dem entgegenzutreten. Um sie geht es in der Ausstellung: Politische Gruppen sprechen über Alltag und Highlights, Scheitern und Angriffe, Hoffnung und Verbündete. Die Ausstellung streift Fragen rund um solidarisches Handeln: </span>Was macht eine Tätigkeit solidarisch und politisch? Was passiert, wenn Menschen sich zusammentun? Woher kommen Hoffnung und Solidarität angesichts rechter Bedrohung und Gewalt? </span></span>

<span lang="DE" xml:lang="DE"><span>Smartphone dabei? Die Hörstücke in der Ausstellung sind über viele Apps und im Internet abzurufen, ebenso Videos in Deutscher Gebärdensprache. </span></span>

**<span lang="DE" xml:lang="DE"><span>Ort: <a href="https://www.galeriekub.de/">Galerie KUB</a> | </span></span>

Kantstr. 18 \| Leipzig

 Öffnungszeiten: Donnerstag-Sonntag 16-19 Uhr**

[Mehr über die Ausstellung erfahren](<https://weiterdenken.de/de/zusammentun-ausstellung>)

Eine Ausstellung der Fachstelle *Bildungsallianzen gegen rechte Ideologien* im Verbund der Heinrich-Böll-Stiftungen

## <span lang="DE" xml:lang="DE"><span>Veranstaltungen während der Ausstellung:</span></span>



<span lang="DE" xml:lang="DE"><span><strong>Feierliche Eröffnung</strong> am Donnerstag, 18. Januar um 17.30 Uhr (<a href="https://calendar.boell.de/de/event/feierliche-eroeffnung">mehr erfahren</a>)</span></span>

**<span lang="DE" xml:lang="DE"><span>Lesung: </span></span>

Wo ist die Solidarität? **Judenhass Underground – Antisemitismus in emanzipatorischen Subkulturen und Bewegungen <span lang="DE" xml:lang="DE"><span>am Dienstag, 23. Januar um 19 Uhr (<a href="https://calendar.boell.de/de/event/wo-ist-die-solidaritaet">mehr erfahren</a>)</span></span>

**Antidiskriminierungsregel**

 Den Veranstaltenden ist ein respektvolles und diskriminierungsfreies Miteinander sehr wichtig. Störungen oder Beleidigungen führen zum Ausschluss aus der Veranstaltung. Die Veranstaltenden behalten sich vor, von ihrem Hausrecht Gebrauch zu machen und Personen, die rechtsextremen Parteien oder Organisationen angehören, der rechtsextremen Szene zuzuordnen sind oder bereits in der Vergangenheit durch rassistische, nationalistische, antisemitische oder sonstige menschenverachtende Äußerungen in Erscheinung getreten sind, den Zutritt zur Veranstaltung zu verwehren oder von dieser auszuschließen.

*<span>Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.</span>

*