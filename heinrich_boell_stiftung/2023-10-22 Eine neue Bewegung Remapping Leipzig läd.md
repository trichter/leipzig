---
id: "155293"
title: 'Eine neue Bewegung: Re*mapping Leipzig lädt ein | Vortrag und
  Performance im Rahmen der Ausstellung "Exercising Collective Disobedience"'
start: 2023-10-22 16:00
end: 2023-10-22 19:00
locationName: Kunstraum D21
address: Demmeringstraße 21, Leipzig
link: https://calendar.boell.de/de/node/155293/
isCrawled: true
---
**16 Uhr: Open Door + Empfang Re\*mapping Leipzig**

**16:30 Uhr: "Zwischen Gleichberechtigung, Bevölkerungspolitik und Selbstbestimmung – Zum Recht auf Schwangerschaftsabbruch in der DDR"**

<span><span><span>Der Kampf gegen die Kriminalisierung von Schwangerschaftsabbrüchen und den §218 bildet seit seiner Einführung in Deutschland Ende des 19. Jahrhunderts einen zentralen Gegenstand gesellschaftlichen Protests. Vielfach wird dabei verkannt, dass Schwangerschaftsabbrüche in Deutschland nicht immer verboten waren. So waren Abbrüche bis zur 12. Woche und ohne Angaben von Gründen in der DDR ab 1972 erlaubt. Der Vortrag erörtert, wie es zu dieser Liberalisierung kam, wie sich in der DDR zu Schwangerschaftsabbrüchen positioniert und wie ihre Rechtmäßigkeit im deutschen Wiedervereinigungsprozess verhandelt wurde.</span><strong>Luisa Klatte </strong>ist Kulturwissenschaftlerin und promoviert an der Universität Leipzig zur Rechtmäßigkeit von Schwangerschaftsabbrüchen in (post-)sozialistischen Gesellschaften am Beispiel der DDR und Polen.&nbsp;</span></span>

**<span><span>17:30: Performance "YCT529" </span></span>

**

<span>Die mystische Performance YCT529 zelebriert die weibliche Menstruation und schlägt dabei eine Brücke zwischen den vier Jahreszeiten und den vier Zyklusphasen. Die Künstlerin thematisiert aber auch die Ambivalenz hormoneller Verhütungsmethoden zwischen Freiheit durch die Emanzipation der eigenen reproduktiven Funktionen und organisierten Unterdrückung der weiblichen Lust. Die Performance gilt als Repräsentation der eigenen Wahrnehmungen der Performerinnen und fügt sich aus sound-installativen Momenten und Text zusammen.<strong>Performance: </strong>Maria Winkler<strong>Sound:</strong> Gabriel Wärfel, Maria Winkler<strong>Performer*innen: </strong>Marie Charlotte Elsner, Maria Winkler</span>

<span><span><strong>Zeit:</strong> 22. Oktober 2023<strong>Ort:</strong> Kunstraum D21 // Demmeringstraße 21 // Leipzig</span></span>

**<span><span>Die Veranstaltung findet im Rahmen der Ausstellung </span></span>

[“<span>Exercising Collective Disobedience</span>

”](<https://www.d21-leipzig.de/en/event/exercising-collective-disobedience/>) statt und ist eine Kooperation zwischen Bükü - Büro für kulturelle Übersetzungen und Weiterdenken Heinrich-Böll-Stiftung Sachsen.**