---
id: "154903"
title: Eröffnung des Informations- und Kulturzentrums der Roma und Sinti in
  Sachsen (IKS)
start: 2023-09-28 17:00
end: 2023-09-28 20:00
link: https://calendar.boell.de/de/node/154903/
isCrawled: true
---
Liebe Netzwerker\*innen, Freund\*innen und Interessierte,

<span><span><span lang="de-DE" xml:lang="de-DE">das Fachnetzwerk Antiziganismus/Antiromaismus eröffnet am 28. September 2023</span></span><span>¨NBSP;</span><span>das <span lang="de-DE" xml:lang="de-DE">Informations- und Kulturzentrum der Roma und Sinti in Sachsen. Das IKS<span> </span>ist der erste offizielle Begegnungs- und Kennenlernort zwischen Roma und Sinti und allen anderen Menschen in Sachsen. Ein Ort der Vernetzung, der Informationen und der Weiterbildung.</span></span><span><span lang="de-DE" xml:lang="de-DE"> Unter einem Dach werden das Fachnetzwerk, unser Kooperationspartner Romano Sumnal und die</span></span><span><span lang="de-DE" xml:lang="de-DE"> </span></span><span><span lang="de-DE" xml:lang="de-DE">Melde- und Informationsstelle Antiziganismus Sachsen<span> (MIA) </span> Veranstaltungen, Ausstellungen und dauerhafte Informationsangebote </span></span></span>

anbieten und regelmäßig ansprechbar sein.

Wann? 28. September 2023, 17 Uhr

Wo? Karl-Liebknecht-Straße 54, 04275 Leipzig

wir freuen uns auf Euch/Sie!