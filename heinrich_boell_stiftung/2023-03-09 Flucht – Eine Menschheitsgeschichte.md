---
id: "152465"
title: Flucht – Eine Menschheitsgeschichte
start: 2023-03-09 19:00
end: 2023-03-09 21:00
locationName: Museum der bildenden Künste Leipzig
address: Katharinenstraße 10, 04109 Leipzig
link: https://calendar.boell.de/de/node/152465/
isCrawled: true
---
<span>In den Augen von Historiker*innen war das 20. Jahrhundert gekennzeichnet durch Fluchtbewegungen. Auch aktuell müssen Menschen ihre Heimat verlassen. Krieg und Not nehmen im jetzigen Jahrhundert kein Ende. Wenn auch die Fluchtauslöser verschieden sind, ähneln sich die Erfahrungen des Auf-der-Flucht-Seins: die Strapazen, das Gefühl des Fremdbestimmtseins bis hin zu der Frage, wann man eigentlich angekommen ist. Der Historiker Andreas Kossert nimmt die Perspektive der Betroffenen auf, lässt in individuellen Erlebnissen die strukturellen Gemeinsamkeiten sichtbar werden. Die Schriftstellerin Khrystyna Kozlovska musste die Ukraine verlassen und lebt nun in Leipzig. Ihre Gedichte verarbeiten Erfahrungen auf literarische Weise. Im Gespräch mit beiden und dem Publikum soll es darum gehen, wie sich Willkommenskultur stärken lässt.</span>

<span>Moderation: Anna Kaleri</span>

, Lauter Leise e.V.

Andreas Kossert, ist promovierter Historiker, arbeitetete am Deutschen Historischen Institut in Warschau und hat verschiedene Sachbücher zum Thema Flucht veröffentlicht.<span> Zuletzt erschien 2020 im Siedler Verlag sein mehrfach prämiertes Buch <a href="https://www.penguinrandomhouse.de/Rezensionen/464363.rhd?gadsnetwork=x&amp;gclid=EAIaIQobChMIlfvbi5SD_QIVDQCLCh23lAphEAAYASAAEgI3s_D_BwE">»Flucht – Eine Menschheitsgeschichte«</a> .</span>

**TERMIN: Donnerstag, 9.3.2023, 19.00 Uhr

 ORT: Museum der bildenden Künste Leipzig, <span>Katharinenstraße 10, 04109 Leipzig</span>

**

<span>Das Gespräch ist eine Veranstaltung des <a href="https://www.lauter-leise.de/">Lauter Leise e.V. Kunst und Demokratie in Sachsen</a> in Kooperation mit Weiterdenken- Heinrich-Böll-Stiftung Sachsen e.V.</span>