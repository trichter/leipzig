---
id: "155715"
title: Wo ist die Solidarität? | Judenhass Underground – Antisemitismus in
  emanzipatorischen Subkulturen und Bewegungen
start: 2024-01-23 19:00
end: 2024-01-23 20:30
link: https://calendar.boell.de/de/node/155715/
isCrawled: true
---
**<span lang="DE" xml:lang="DE"><span>Dienstag, 23. Januar um 19 Uhr in der Galerie KUB º Kantstraße 18 º Leipzig</span></span>

**

<span><span>Niemand will Antisemit sein. Erst recht nicht in Bewegungen mit einem progressiven, emanzipatorischen Selbstbild. Judenhass geht aber auch underground – ob Rapper gegen Rothschilds, DJs for Palestine oder Punks Against Apartheid. BDS, die Boykottkampagne gegen den jüdischen Staat, will nahezu jedes Anliegen kapern, von Klassenkampf bis Klimagerechtigkeit. Altbekannte Mythen tauchen in alternativer Form wieder auf, bei Pride-Demos, auf der documenta oder beim Gedenken an rassistischen Terror. Und viele Jüdinnen*Juden fragen sich, wo ihr Platz in solchen Szenen sein soll. </span></span>

<span><span><span lang="DE" xml:lang="DE"><span>Die Autorin&nbsp;Anastasia Tikhomirova&nbsp;und die Herausgeber&nbsp;Nicholas Potter und&nbsp;Stefan Lauer sprechen über (fehlende) Solidarität mit Jüdinnen*Juden und über Möglichkeiten solidarischer Kritik.</span></span></span></span>

[<span><span>Zum Buch</span></span>

](<https://www.hentrichhentrich.de/buch-judenhass-underground.html>)<span><span> "Judenhass Underground"</span></span>

- <span><a href="https://taz.de/Anastasia-Tikhomirova/!a72804/">Anastasia Tikhomirova</a> ist freie Journalistin, Kulturwissenschaftlerin und Moderatorin. Sie ist Alumna des Marion-Gräfin-Dönhoff Stipendiums der Internationalen Journalistenprogramme 2021, welches sie bei der Novaya Gazeta in Moskau absolvierte. Das Medium Magazin wählte sie 2023 zu den top 30 bis 30 Journalist:innen des Landes. Außerdem macht sie ihren Master in Osteuropastudien und interdisziplinärer Antisemitismusforschung in Berlin.</span>

- [Nicholas Potter](<http://nicholaspotter.net/>) <span><span><span><span>ist britisch-deutscher Journalist und arbeitet bei der Amadeu Antonio Stiftung in Berlin. Er schreibt für diverse Medien wie die taz, Jungle World, Belltower.News und Jüdische Allgemeine über die extreme Rechte, Antisemitismus, Rassismus, Subkulturen, Bewegungen und mehr. Zuvor war er Theaterredakteur beim Exberliner Magazine. Er studierte am King’s College London und der Humboldt-Universität zu Berlin.</span></span></span></span>

- [Stefan Lauer](<https://www.belltower.news/author/stefan-lauer/>) <span><span><span><span>ist Redakteur bei Belltower.News, der journalistischen Plattform der Amadeu Antonio Stiftung, und beschäftigt sich – auch als Referent der Stiftung – mit Antisemitismus, Rassismus und dem rechten Rand. Zwischen 2009 und 2017 arbeitete er als Senior Editor für VICE Deutschland und berichtete über Rechtsextremismus, Verschwörungserzählungen und LGBTQ*-Themen.</span></span></span></span>


<!-- -->

**Die Lesung findet statt im Rahmen der [Ausstellung "zusammentun – solidarisch gegen rechts"](<https://calendar.boell.de/de/event/zusammentun-solidarisch-gegen-rechts>). Sie ist vom 18. bis 28. Januar in der Galerie KUB zu sehen, immer donnerstags bis sonntags 16-19 Uhr**

<span><span><span lang="DE" xml:lang="DE">Rechte Hetze und Gewalt sind alltäglich. Extreme Rechte gewinnen Wahlen und vernetzen sich. Doch viele tun sich zusammen, um dem entgegenzutreten. Um sie geht es in der Ausstellung: Politische Gruppen sprechen über Alltag und Highlights, Scheitern und Angriffe, Hoffnung und Verbündete. Die Ausstellung streift Fragen rund um solidarisches Handeln: </span>Was macht eine Tätigkeit solidarisch und politisch? Was passiert, wenn Menschen sich zusammentun? Woher kommen Hoffnung und Solidarität angesichts rechter Bedrohung und Gewalt? </span></span>

**Weitere Veranstaltung: [Feierliche Eröffnung am 18. Januar](<https://calendar.boell.de/de/event/feierliche-eroeffnung>)**

**Antidiskriminierungsregel**

 Den Veranstaltenden ist ein respektvolles und diskriminierungsfreies Miteinander sehr wichtig. Störungen oder Beleidigungen führen zum Ausschluss aus der Veranstaltung. Die Veranstaltenden behalten sich vor, von ihrem Hausrecht Gebrauch zu machen und Personen, die rechtsextremen Parteien oder Organisationen angehören, der rechtsextremen Szene zuzuordnen sind oder bereits in der Vergangenheit durch rassistische, nationalistische, antisemitische oder sonstige menschenverachtende Äußerungen in Erscheinung getreten sind, den Zutritt zur Veranstaltung zu verwehren oder von dieser auszuschließen.

*<span>Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.</span>

*