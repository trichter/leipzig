---
id: "155029"
title: Klimaküche – Tunesisch kochen | Kochkurs und Information
start: 2023-10-05 18:00
end: 2023-10-05 20:30
locationName: Café im Haus der Demokratie, Bernhard-Göring-Str. 152
address: 04277 Leipzig
link: https://calendar.boell.de/de/node/155029/
isCrawled: true
---
Die Insel Kerkennah liegt vor Sfax (Tunesien) im Mittelmeer. Dattelpalmen, blaues Meer, Schafe, Fischerboote, Sandstrände findet man dort. Doch die Idylle bröckelt: Die Menschen auf Kerkennah kämpfen um den Erhalt der Traditionen und der Insel. Mit wunderschönen Bildern zeigen wir, wie fragil die Zukunft der Insel im Mittelmeer ist. Beim gemeinsamen Kochen von tunesischer Küche wollen wir ins Gespräch kommen. Wir laden ein darüber zu diskutieren, wie wir global zusammen rücken können, um die Herausforderungen des Klimawandels und der Konsumgesellschaft zu meistern.

**ZEIT: Donnerstag, 5.10.2023 // 18 Uhr

 ORT: Café im Haus der Demokratie, Bernhard-Göring-Str. 152 // 04277 Leipzig**

 Um eine Anmeldung bis 2.10.23 wird gebeten.

 (Mail: ernte.dank@bundleipzig.de, Telefon: 0341 / 989 91 050)

Ein Veranstaltungsangebot des BUND Leipzig in Kooperation mit Weiterdenken - Heinrich-Böll-Stiftung Sachsen e.V.