---
id: "152493"
title: SARA MARDINI - GEGEN DEN STROM | Filmpremiere und Gespräch
start: 2023-03-22 19:00
end: 2023-03-22 21:00
locationName: Passage Kinos Leipzig
address: Hainstraße 19a, Leipzig
link: https://calendar.boell.de/de/node/152493/
isCrawled: true
---
2015 wird **Sara Mardini**, syrische Leistungsschwimmerin, zur Heldin. Auf der Flucht über das Mittelmeer versagt der Motor des überfüllten Schlauchbootes. Es droht zu kentern. Gemeinsam mit ihrer Schwester Yusra springt sie ins Wasser, zieht das Boot über drei Stunden schwimmend an Land und rettet alle Geflüchteten vor dem Ertrinken. Die Geschichte macht auf der ganzen Welt Schlagzeilen. Netflix verfilmt sie.

Sara bekommt Asyl in Deutschland, kehrt jedoch nach Lesbos zurück, um Geflüchteten zu helfen. Doch ihre humanitäre Arbeit führt 2018 zu ihrer Verhaftung. Sie wird einer Reihe von schweren Straftaten beschuldigt – darunter Beihilfe zur illegalen Einreise, Geldwäsche, Betrug und Mitgliedschaft in einer kriminellen Vereinigung. Während ihre Schwester bei den Olympischen Spielen schwimmt, wartet Sara gemeinsam mit 24 anderen Aktivist\*innen auf einen Prozess, der sie für 25 Jahre ins Gefängnis bringen könnte.

Über vier Jahre hat die Filmemacherin **Charly Wai Feldman** Saras Kampf um Gerechtigkeit und um eine neue Zukunft in Berlin begleitet. Der Film schlägt <span>eine Brücke zum Netflix-Hit “THE SWIMMERS” </span>

und erzählt die Geschichte weiter. Nachdem die Aufmerksamkeit um Saras Person abgeflacht ist, muss diese sich nämlich mit den zermürbenden Mühlen der griechischen Justiz herumschlagen und versuchen, sich ein neues Leben im Schwebezustand aufzubauen.

Die im Januar begonnene Gerichtsverhandlung von Aktivist\*innen wie Sara Mardini und Séan Binder gibt dem Film eine besondere Brisanz und Aktualität. Sie wirft ein Schlaglicht auf den Versuch, Solidarität mit Geflüchteten zu kriminalisieren.

<span><strong>Gäste:</strong></span>



<span>Sara Mardini // Protagonistin</span>



<span>Seán Binder // Protagonist</span>



<span>Charly Feldman // Regie</span>



<span>Anna Dziembowska // Bildgestaltung</span>



<span>Zamarin Wahdat // Producerin</span>

<span><strong>Zeit:</strong> 22. März 2023 // 19 Uhr</span>

<span><strong>Ort:</strong> Passage Kinos Leipzig // Hainstraße 19a // Leipzig</span>

<span><strong>Eintritt ist frei.</strong></span>

<span>Die Veranstaltung ist eine Kooperation zwischen Amnesty International Deutschland e.V., dem Sächsischen Flüchtlingsrat e.V. und Weiterdenken Heinrich-Böll-Stiftung Sachsen e.V.</span>