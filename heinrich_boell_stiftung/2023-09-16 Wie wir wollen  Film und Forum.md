---
id: "154775"
title: Wie wir wollen | Film und Forum
start: 2023-09-16 16:00
end: 2023-09-16 20:00
locationName: D21 Kunstraum
address: Demmeringstraße 21, Leipzig
link: https://calendar.boell.de/de/node/154775/
isCrawled: true
---
Mit der Intention, die gesellschaftliche Tabuisierung von Abtreibung zu brechen, zeigt das dokumentarisch-filmische Projekt "Wie wir wollen", produziert vom Kollektiv Kinokas, Interviews von 50 Personen, die sich für den Abbruch einer ungewollten Schwangerschaft entschieden haben. Der Film hinterfragt kritisch, was es bedeutet, in diesem sensiblen Thema eine freie Entscheidung zu treffen. Im Nachgang kommen wir mit Expert\*innen und Aktivist\*innen ins Gespräch.

**Gespräch mit:** Gosia von der polnischen Initiative Ciocia Basia, Dr. Joris Gregor (Paar- und Sexualtherapie, Queer und Gender Studies) und Marieke Bea von Doctors for Choice. Das Gespräch wird von der im Themenbereich erfahrenen Laura Clarissa Loew moderiert.

**Zeit:** 16\. September 2023 // 16 Uhr

**Ort:** D21 Kunstraum // Demmeringstraße 21 // Leipzig

Die Veranstaltung ist eine Kooperation zwischen D21 Kunstraum und Weiterdenken Heinrich-Böll-Stiftung Sachsen e.V.