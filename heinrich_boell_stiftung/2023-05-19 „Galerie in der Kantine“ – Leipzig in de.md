---
id: "153517"
title: "„Galerie in der Kantine“ – Leipzig in den Jahren 1928 – 1932 |
  Ausstellungseröffnung - Arbeiter:innenfotografie von Albert Hennig "
start: 2023-05-19 18:00
end: 2023-05-19 22:00
locationName: Schnellbuffet Süd
address: Karl-Liebknecht-Str.  139, 04275 Leipzig
link: https://calendar.boell.de/de/node/153517/
isCrawled: true
---
<span lang="DE-DE" xml:lang="DE-DE"><span>Armut</span><span>, </span><span>Hunger, </span><span>Gewalt – Leipzig am Vorabend des Nationalsozialismus </span><span>ist</span><span> geprägt von</span><span>¨NBSP;</span><span>Arbeitslosigkeit </span><span>und politischer </span><span>Unruhe</span><span>. </span><span>Im Jahr 1932 </span><span>befindet</span><span> sich die</span><span> </span><span>Weltwirtschaftskrise auf einem Höhepunkt. Der Weimarer Staat versucht die Auswirkungen der Krise durch Notstandsverordnungen und eine strikte Sparpolitik zu überwinden. </span><span>Der </span><span>Leipziger </span></span>

<span lang="DE-DE" xml:lang="DE-DE"><span>Oberbürgermeister Carl Goerdeler verkündet, „dass die Wirtschaftslage ein Diktator sei, dem wir alle uns zu fügen“ </span><span>hätten</span><span>. Doch nicht alle trifft die Krise gleichermaßen</span><span>:</span><span> </span><span>Vor allem für</span></span>

<span lang="DE-DE" xml:lang="DE-DE"><span> Millionen</span><span> </span><span>Arbeiter:inne</span><span>n</span><span>, arme oder </span><span>von Armut bedrohte</span><span> Bevölkerung</span><span>sgruppen </span><span>bedeutet </span><span>sie</span><span> </span><span>Hunger</span><span>, </span><span>Not</span><span> und Erschöpfung.</span></span>

<span></span>

<span lang="DE-DE" xml:lang="DE-DE"><span>Durch die</span><span> Fotografien Albert Hennigs, 1907 </span><span>in </span><span>Leipzig </span><span>Klein-</span><span>Zschocher</span><span> geboren, wird der Alltag im Leipzig dieser Zeit greifbar. </span><span>Seine</span><span> Fotografien</span><span> </span><span>dokumentieren</span><span> </span><span>die soziale Wirklichkeit dieser Zeit und sind zugleich mehr als </span><span>der Befund sozialer Problemlagen</span><span>. S</span><span>ie </span><span>machen</span><span> die </span><span>Unruhe und Erschöpfung</span><span> jener </span><span>Menschen</span><span> spürbar</span><span>, die in der o</span><span>ffiziellen </span><span>Leipziger </span><span>Stadtgeschichtsschreibung </span><span>gestaltlos bl</span><span>ei</span><span>ben</span><span>: Arbeitslose in einer langen Schlange vor dem Arbeitsamt in der Leipziger Südvorstadt, </span><span>arme </span><span>Frauen und Männer, die </span><span>Abfälle</span><span> vor der Leipziger Markthalle aufsammeln, eine Speiseanstalt in der </span><span>I</span><span>nnenstadt, in der Bedürftige schweigend essen.&nbsp;</span></span>

<span></span>

<span lang="DE-DE" xml:lang="DE-DE"><span>Die </span><span>Ausstellung </span><span>ist zugleich eine Einladung an historisch Interessierte die Leipziger Alltags- und Sozialgeschichte der ersten Hälfte des 20. Jahrhunderts zu erkunden.</span><span> </span><span>Die Geschichtswerkstatt</span><span> </span><span>leipzig</span><span>|</span><span>unten</span><span> steht</span><span> Interessierte</span><span>n</span> offen, die die <span>Stadtgeschichte erschließen möchte</span><span>n</span><span>.</span><span></span></span>

<span></span>

<span lang="DE-DE" xml:lang="DE-DE"><span>Ort: Schnellbuffet Süd, Karl-Liebknecht-Str. </span></span>

139, 04275 Leipzig

Instagram: leipzig\_unten