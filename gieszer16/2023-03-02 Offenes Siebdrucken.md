---
id: "3969"
title: Offenes Siebdrucken
start: 2023-03-02 15:00
end: 2023-03-02 18:00
address: G16 (Innenhof), Gießerstraße 16, Leipzig
link: https://www.planlos-leipzig.org/events/offenes-siebdrucken-5/
isCrawled: true
---
Wir laden euch zum Offenen Siebdrucken ein:

In unserer Werkstatt im G16-Innenhof hinten rechts. Die Werkstatt ist nicht rollstuhlgerecht, rauchfrei, eingeheizt.

Es gibt Motive/ Farben/ einige Klamotten aus dem Umsonstladen und Snacks vor Ort. Der Umsonstladen hat aber auch offen zu der Zeit oder ihr bringt eigene Klamotten mit. Das Drucken ist gegen Spende.

 

Wir sind hier über Mail erreichbar: unterdrvck@riseup.net


https://gieszer16.blackblogs.org/unterprojekte/unterdrvck/

https://www.planlos-leipzig.org/events/offenes-siebdrucken-5/