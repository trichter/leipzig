---
name: G16
website: https://gieszer16.blackblogs.org
scrape:
  source: ical
  options:
    url: https://www.planlos-leipzig.org/events.ics
  filter:
    description: gieszer16.blackblogs.org
---
