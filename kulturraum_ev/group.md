---
name: Kulturraum e.V. - KreV
website: http://www.krev-leipzig.org/
email: kulturraum@ifz.me
scrape:
  source: facebook
  options:
    page_id: KulturraumEV
---
Der Kulturraum e.V. (KreV) ist eine kultur-politische Gruppe in Leipzig. Wir sind ein unabhängiger Verein, fühlen uns dem IFZ seit seiner Gründung verbunden und führen dort unsere Veranstaltungen durch.