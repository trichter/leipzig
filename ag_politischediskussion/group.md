---
name: AG Politische Diskussion
website: https://www.facebook.com/AGPolitischeDiskussion/
scrape:
  source: facebook
  options:
    page_id: 164200407266494
---
Die Ag Politische Diskussion erklärt sich die in der Gesellschaft herrschenden Zwecke, wer von ihnen profitiert und wer die Schäden trägt.