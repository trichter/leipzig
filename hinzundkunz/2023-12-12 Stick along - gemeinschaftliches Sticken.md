---
id: 6802-1702396800-1702407600
title: Stick along - gemeinschaftliches Sticken
start: 2023-12-12 16:00
end: 2023-12-12 19:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/stick-along-gemeinschaftliches-sticken/
isCrawled: true
---
Heute bei uns im krimZkrams: Gemeinsames Sticken, voneinander lernen und inspirieren lassen. Du hast eigene Ideen? Dann komme gern vorbei! 
Wir freuen uns über deine Anmeldung: unter workshop@kunzstoffe.de | 0163 4846916 | WhatsApp (krimZkrams | kunZstoffe) oder im Laden (Kontaktdaten nicht vergessen!) 
—————————————————————————————————————————————————— 
Liebe Leute, 
für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung. 
Wir wünschen euch viel Spaß. Euer kunZstoffe-Team