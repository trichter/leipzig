---
id: 6751-1699696800-1699718400
title: Aktionstag für alle in der Stadtbibliothek
start: 2023-11-11 10:00
end: 2023-11-11 16:00
address: stadtbibliothek leipzig, Wilhelm-Leuschner-Platz 10-11, 04107 Leipzig,
  Deutschland
link: https://kunzstoffe.de/event/aktionstag-fuer-alle-in-der-stadtbibliothek/
isCrawled: true
---
Infos folgen