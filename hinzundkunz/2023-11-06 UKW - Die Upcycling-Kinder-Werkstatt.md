---
id: 6753-1699288200-1699293600
title: UKW - Die Upcycling-Kinder-Werkstatt
start: 2023-11-06 16:30
end: 2023-11-06 18:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/ukw-die-upcycling-kinder-werkstatt/
isCrawled: true
---
Infos folgen