---
id: 6460-1683824400-1683829800
title: Let's vectorize! - Geometrische Gefäße
start: 2023-05-11 17:00
end: 2023-05-11 18:30
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/lets-vectorize-geometrische-gefaesse/
isCrawled: true
---
Anmeldung erwünscht: unter workshop@kunzstoffe.de | 0163 4846916 | WhatsApp (krimZkrams | kunZstoffe| oder im Laden (Kontaktdaten nicht vergessen!) 
—————————————————————————————————————————————————— 
Liebe Leute, 
für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung. 
Wir wünschen euch viel Spaß. Euer kunZstoffe-Team