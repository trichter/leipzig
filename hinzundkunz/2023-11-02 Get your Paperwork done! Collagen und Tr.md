---
id: 6724-1698940800-1698948000
title: Get your Paperwork done! Collagen und Traumbilder aus Papier.
start: 2023-11-02 16:00
end: 2023-11-02 18:00
link: https://kunzstoffe.de/event/get-your-paperwork-done-collagen-und-traumbilder-aus-papier/
isCrawled: true
---
Na ihr,
habt ihr Lust bestehende Dinge aus ihren Rahmen zu reißen und wieder neu zusammenzusetzen? Sprudelt sie über eure Kreativität und sucht sie nach Möglichkeiten sich auszudrücken? Dann seid ihr bei unserem Collagen-Workshop papierrichtig. Gemeinsam tauchen wir ein, in eine Welt aus Papier, schwimmen durch Fetzen der Vergangenheit  und finden originelle Motive um dein Traumbild zusammenzusetzen. 

Was erwartet euch? 

verschiedenste Materialien sammeln, schneiden, kleben und kombinieren
Keine Vorkenntnisse erforderlich – ob ihr schon Erfahrung habt oder absolute Anfänger seid, hier ist jeder willkommen!
Lasst eurer Fantasie freien Lauf und bringt eure eigenen Magazine, Fotos oder Fundstücke mit, die ihr in eure Collagen einbauen möchtet.


im krimZkrams Leipzig, Georg-Schwarz-Straße 7  
Meldet euch am besten an, das schafft Planungssicherheit. Schickt dazu eine Mail an workshop@kunzstoffe. 
_______________________________________________________________________________________________________ 
Liebe Leute,
für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung.