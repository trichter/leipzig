---
id: 6708-1697558400-1697565600
title: Lust auf Kaffee? Upcycling mit Kaffeetüten
start: 2023-10-17 16:00
end: 2023-10-17 18:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/lust-auf-kaffee-upcycling-mit-kaffeetueten/
isCrawled: true
---
Habt ihr Lust eure Kaffeebeutel-Kreativität entfesseln und ein bisschen Gold und Glitzer in den Herbst zu bringen? 🌱♻️ 
Dann ist unser kleiner Workshop zum Thema Upcycling aus Kaffeetüten genau das richtige. Gemeinsam probieren wir aus und entdecken, wie man aus gebrauchten Kaffeetüten echte Schätze zaubern kann. Eurer Fantasie sind hierbei keine Grenzen gesetzt! Wir freuen uns auf zwei inspirierende Stunden voller Kreativität und Nachhaltigkeit. ☕💚“ 
im krimZkrams Leipzig, Georg-Schwarz-Straße 7  
_______________________________________________________________________________________________________ 
Liebe Leute, 
für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung.