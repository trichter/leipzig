---
id: 6433-1687192200-1687199400
title: UKW - UpcyclingKinderWerkstatt *Mobile aus Plastikdeckeln*
start: 2023-06-19 16:30
end: 2023-06-19 18:30
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/ukw-upcyclingkinderwerkstatt-oesterliches-upcycling/
isCrawled: true
---
In der UpcyclingKinderWerkstatt (UKW) kannst du mit den zahlreichen kunterbunten Materialien experimentieren und verschiedene Werkzeuge kennenlernen. Jeden 1. und 3. Montag im Monat zeigen wir euch eine neue Projektidee. Die UKW findet von 16:30 bis 18:30 Uhr statt und ist für Kinder im Alter von 7-12 Jahren gedacht. 

HEUTE: kunterbuntes Mobile aus kunterbunten Plastikdeckeln
 

!!Clevere KidZ melden sich an. Dazu reicht auch eine kurze Mail an workshop@kunzstoffe, eine spontane Nachricht über what’s App (krimZkrams | kunZstoffe) oder eine SMS an 0163 4846916!! 

—————————————————————————————————————————————————— 
Liebe Leute, 
für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung. 
Wir wünschen euch viel Spaß. Euer kunZstoffe-Team