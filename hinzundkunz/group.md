---
name:  kunZstoffe e.V.
website: https://kunzstoffe.de/
email: buero@kunzstoffe.de
address: Georg-Schwarz-Str. 7, 04177 Leipzig
scrape:
    source: ical
    options:
        url: https://kunzstoffe.de/event/?ical=1
    exclude: []
---
Das hinZundkunZ, kurz huk, ist ein Raum für Kunst, Kultur und alles was da so rein passt.