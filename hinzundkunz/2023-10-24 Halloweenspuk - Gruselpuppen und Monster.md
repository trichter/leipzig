---
id: 6713-1698163200-1698170400
title: Halloweenspuk - Gruselpuppen und Monsterfreund:innen basteln
start: 2023-10-24 16:00
end: 2023-10-24 18:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/halloweenspuk-gruselpuppen-und-monsterfreundinnen-basteln/
isCrawled: true
---
Wolltet ihr schon immer das Fürchten lernen und Spaß dabei haben? Dann kommt zu unserem schaurig-schönen Workshop passenden zu Halloween. Gemeinsam spuken wir durch das krimZkrams und erschauen was wir für tolle Materialien finden. Aus denen basteln wir gruselige Monsterfreund:innen zum Kuscheln oder zu Deko für deine Halloweenparty. 
Komm vorbei und lass uns gemeinsam kreativ werden! Keine gruseligen Vorkenntnisse sind nötig, nur gute Laune und Lust auf Spaß. 
Wir freuen uns auf dich! 🎃👻👹 
im krimZkrams Leipzig, Georg-Schwarz-Straße 7  
_______________________________________________________________________________________________________ 
Liebe Leute, 
für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung.