---
id: 6755-1699977600-1699988400
title: Druckwerkstatt - Monoprint mit Gelplatten
start: 2023-11-14 16:00
end: 2023-11-14 19:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/druckwerkstatt-monoprint-mit-gelplatten-2/
isCrawled: true
---
Infos folgen