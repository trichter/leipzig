---
id: 6575-1690466400-1690473600
title: "Sommerferien(S)pass: Paper Work - bunter Sommerschmuck..."
start: 2023-07-27 14:00
end: 2023-07-27 16:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/sommerferienspass-paper-work-bunter-sommerschmuck/
isCrawled: true
---
… aus Papierperlen 
Heute wird es richtig farbenfroh und schmückend, denn der Nachmittag steht ganz im Sinne eures Sommerschmucks. Werdet mit uns zum Perlendreher und schnappt euch verschiedene Papierarten aus unserer Sammlung, welche zu schade zum Wegwerfen sind. Dann wird kräftig gerollt, gedreht sowie geklebt und zu Ringen, Armbänder, Ketten, Haarbändern und anderem Schmückendes aufgefädelt 
Angebot für Kinder und Jugendliche ab 7 Jahre geeignet 
Kosten: Materialkostenbeitrag auf Spendenbasis (Für Informationen zum Ferienpass hier entlang) 
Es ist keine Anmeldung notwendig. Kommt einfach vorbei!!