---
id: 6363-1678287600-1678298400
title: Simply the Rest - Teppichknüpfen aus Stoffresten
start: 2023-03-08 15:00
end: 2023-03-08 18:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/simply-the-rest-teppichknuepfen-aus-stoffresten/
isCrawled: true
---
Anmeldung erwünscht: unter workshop@kunzstoffe.de oder im Laden (Kontaktdaten nicht vergessen!) 
—————————————————————————————————————————————————— 
Liebe Leute, 
für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung. 
Wir wünschen euch viel Spaß. Euer kunZstoffe-Team