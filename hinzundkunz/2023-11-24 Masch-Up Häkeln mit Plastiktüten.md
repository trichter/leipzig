---
id: 6779-1700841600-1700852400
title: "Masch-Up: Häkeln mit Plastiktüten"
start: 2023-11-24 16:00
end: 2023-11-24 19:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/masch-up-haekeln-mit-plastiktueten/
isCrawled: true
---
Dieses Angebot findet im Rahmen der Europäischen Woche für Abfallvermeidung statt. 
Heute wird passend zum diesjähriges Thema der Aktionswoche „Verpackungen“  mit Plastiktüten gehäkelt… denn nicht nur Wolle eignet sich für Häkelprojekte. 
Start ist 16 Uhr und du kannst jederzeit dazukommen. 
—————————————————————————————————————————————————— 
Liebe Leute, 
für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung.