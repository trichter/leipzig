---
id: 6792-1702917000-1702922400
title: UKW - UpcyclingKinderWerkstatt
start: 2023-12-18 16:30
end: 2023-12-18 18:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/ukw-upcyclingkinderwerkstatt-10/
isCrawled: true
---
Du hast Lust auf kreatives Funkensprühen und möchtest gern was für die Umwelt tun? Dann komme zur UpcyclingKinderWerkstatt ins krimZkrams. Ganz nach dem Motto „nicht verschwenden – wiederverwenden“ hauchen wir beim Upcycling alten oder nicht mehr gebrauchten Dingen ein neues Leben ein. Beim gemeinsamen Werkeln, Tüfteln und Auszuprobieren kannst du mit zahlreichen kunterbunten Materialien experimentieren und verschiedene Werkzeuge kennenlernen. Es werden neue schöne und nützliche Dinge entstehen und ganz nebenbei vermeiden wir Müll. 

Jeden 1. und 3. Montag im Monat zeigen wir euch eine neue Projektidee. Die UKW findet von 16:30 bis 18:00 Uhr statt und ist für Kinder im Alter von 7-12 Jahren gedacht. 
!!Clevere KidZ melden sich an!! Wir würden uns, wenn ihr uns eine kurze Mail an workshop@kunzstoffe, eine spontane Nachricht über what’s App (krimZkrams | kunZstoffe) oder eine SMS an 0163 4846916 schickt!! Danke! 
—————————————————————————————————————————————————— 
Liebe Leute, 
für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung.