---
id: 6573-1689688800-1689696000
title: "Sommerferien(S)pass: Monstermäßig stark..."
start: 2023-07-18 14:00
end: 2023-07-18 16:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/sommerferienspass-monstermaessig-stark/
isCrawled: true
---
… Anhänger aus Holzresten 
Alle Monsterfans aufgepasst! Heute könnt ihr euch einen monstermäßigen Begleiter gestalten. Aus Holzresten entstehen Anhänger für z.B. Schlüssel, Taschen und Rücksäcke die unterwegs zum richtigen Hingucker werden. 
Angebot für Kinder und Jugendliche ab 7 Jahre geeignet 
Kosten: Materialkostenbeitrag auf Spendenbasis (Für Informationen zum Ferienpass hier entlang)
 
Es ist keine Anmeldung notwendig. Kommt einfach vorbei!!