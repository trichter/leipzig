---
id: 6465-1683208800-1683219600
title: Lavendeldruck (im Theater der jungen Welt)
start: 2023-05-04 14:00
end: 2023-05-04 17:00
address: Theater der Jungen Welt, Lindenauer Markt 21, Leipzig, 04177
link: https://kunzstoffe.de/event/lavendeldruck/
isCrawled: true
---
Heute sind wir auch im Theater der jungen Welt als Angebot zum PLAY & CONNECT zu Gast und bieten Lavendeldruck an.