---
id: 6371-1678894200-1678899600
title: Mini Kidz *Rühre rühre Löffelstiel. Werkeln mit Salzteig*
start: 2023-03-15 15:30
end: 2023-03-15 17:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/mini-kidz-ruehre-ruehre-loeffelstiel-werkeln-mit-salzteig/
isCrawled: true
---
Immer im dritten Mittwoch im Monat öffnen wir unsere Werkstatt für Eltern mit ihren Kinder bis zu 6 Jahren. 
HEUTE: kreatives Formen und Gestalten mit Salzteig >> z.B. Anhänger für den Osterstrauß 
Ihr sucht ein Angebot für ältere Kinder? Dann schaut doch einmal bei der UKW – UpcyclingKinderWerkstatt vorbei. 
Anmeldung erwünscht: unter workshop@kunzstoffe.de oder im Laden (Kontaktdaten nicht vergessen!) 
————————————————————————————————————————————————————- 
Liebe Leute, 
für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung. 
Wir wünschen euch viel Spaß. Euer kunZstoffe-Team