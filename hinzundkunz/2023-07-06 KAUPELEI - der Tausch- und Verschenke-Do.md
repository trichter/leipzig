---
id: 6558-1688661000-1688668200
title: KAUPELEI - der Tausch- und Verschenke-Donnerstag
start: 2023-07-06 16:30
end: 2023-07-06 18:30
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/kaupelei-der-tausch-und-verschenke-donnerstag-3/
isCrawled: true
---
…. kommt vorbei zum Tauschen und Verschenken von Allerlei angesammelten Dingen, die zu schade sind zum Wegwerfen. 
…. jeden ersten und dritten Donnerstag vor dem Laden oder bei schlechten Wetter im Durchgang zum Hof