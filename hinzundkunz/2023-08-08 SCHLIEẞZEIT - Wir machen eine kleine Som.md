---
id: 6564-1691452800-1692316800
title: SCHLIEẞZEIT - Wir machen eine kleine Sommerpause
start: 2023-08-08 00:00
end: 2023-08-18 00:00
link: https://kunzstoffe.de/event/schlie%c3%9fzeit-wir-machen-eine-kleine-sommerpause/
isCrawled: true
---
Vom 08. – 18. August bleiben unsere Türen und Tore der Materialsammlung geschlossen. Wir machen eine kleine Sommerpause. Ab dem 22.08. haben wir wieder geöffnet.