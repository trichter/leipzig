---
id: 6542-1689696000-1689703200
title: Gebrauchskunst aus Papierschnipsel | Teil 1
start: 2023-07-18 16:00
end: 2023-07-18 18:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/gebrauchskunst-aus-papierschnipsel-teil-1/
isCrawled: true
---
Zweiteiliges Angebot: 2. Teil am 01.08. | 16-18 Uhr 
  
  
  
Anmeldung erwünscht: unter workshop@kunzstoffe.de | 0163 4846916 | WhatsApp (krimZkrams | kunZstoffe) oder im Laden (Kontaktdaten nicht vergessen!) 
—————————————————————————————————————————————————— 
Liebe Leute, 
für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung. 
Wir wünschen euch viel Spaß. Euer kunZstoffe-Team