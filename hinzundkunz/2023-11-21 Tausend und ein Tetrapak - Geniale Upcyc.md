---
id: 6771-1700582400-1700593200
title: Tausend und ein Tetrapak - Geniale Upcycling-Ideen
start: 2023-11-21 16:00
end: 2023-11-21 19:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/tausend-und-ein-tetrapak-geniale-upcycling-ideen/
isCrawled: true
---
Dieses Angebot findet im Rahmen der Europäischen Woche für Abfallvermeidung statt. 
Heute gibt es passend zum diesjähriges Thema der Aktionswoche „Verpackungen“ ein vielfältiges DIY für Schönes und Praktisches aus Getränkekartons . Heute schon einen geleert? 
Start ist 16 Uhr und du kannst jederzeit dazukommen. 
—————————————————————————————————————————————————— 
Liebe Leute, 
für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung.