---
id: 6757-1700236800-1700247600
title: Neue Taschen aus alten Tüten - Bügeln und Nähen
start: 2023-11-17 16:00
end: 2023-11-17 19:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/neue-taschen-aus-alten-tueten-buegeln-und-naehen/
isCrawled: true
---
Dieses Angebot findet im Rahmen der Europäischen Woche für Abfallvermeidung statt. 
Heute zeigen wir euch, passend zum diesjähriges Thema der Aktionswoche „Verpackungen“,  wie aus Plastikbeuteln und Tüten neue stylische Taschen entstehen. 
—————————————————————————————————————————————————— 
Liebe Leute, 
für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung.