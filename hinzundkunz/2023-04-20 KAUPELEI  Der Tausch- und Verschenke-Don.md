---
id: 6446-1682008200-1682015400
title: KAUPELEI | Der Tausch- und Verschenke-Donnerstag
start: 2023-04-20 16:30
end: 2023-04-20 18:30
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/kaupelei-der-tausch-und-verschenke-donnerstag/
isCrawled: true
---
…. kommt vorbei zum Tauschen und Verschenken von Allerlei angesammelten Dingen, die zu schade sind zum Wegwerfen. 
…. jeden ersten und dritten Donnerstag vor dem Laden oder bei schlechten Wetter im Durchgang zum Hof