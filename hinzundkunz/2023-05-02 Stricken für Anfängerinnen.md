---
id: 6453-1683043200-1683054000
title: Stricken für Anfänger:innen
start: 2023-05-02 16:00
end: 2023-05-02 19:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/stricken-fuer-anfaengerinnen-2/
isCrawled: true
---
Liebe Leute, 
für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung. 
Wir wünschen euch viel Spaß. Euer kunZstoffe-Team 
_______________________________________________________________________________________________________ 
Anmeldung erwünscht! 
Möglich unter workshop@kunZstoffe.de | 0163 4846916 | WhatsApp (krimZkrams | kunZstoffe)