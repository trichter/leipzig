---
id: 6757-1697731200-1697742000
title: Neue Taschen aus alten Tüten - Bügeln und Nähen
start: 2023-10-19 16:00
end: 2023-10-19 19:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/neue-taschen-aus-alten-tueten-buegeln-und-naehen/
isCrawled: true
---
Infos folgen