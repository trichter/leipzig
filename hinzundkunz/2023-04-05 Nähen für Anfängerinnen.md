---
id: 6354-1680706800-1680717600
title: Nähen für Anfänger:innen
start: 2023-04-05 15:00
end: 2023-04-05 18:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/naehen-fuer-anfaengerinnen-10/
isCrawled: true
---
Nähen für Anfänger:innen ist ein regelmäßiger Kurs welcher immer am ersten Mittwoch im Monat stattfindet. Wir stellen euch die Nähmaschinen bereit , ihr bringt eure Ideen oder Projekte mit, schnappt euch das passende Material aus der Sammlung (oder ihr bringt welche mit) und bei Unterstützungsbedarf sind wir für euch mit Rat und Tat da. 
nächsten Termine:  05.04. | 03.05. | 07.06. 
Anmeldung erwünscht: unter workshop@kunzstoffe.de oder im Laden (Kontaktdaten nicht vergessen!) 
—————————————————————————————————————————————————— 
Liebe Leute, 
für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung. 
Wir wünschen euch viel Spaß. Euer kunZstoffe-Team