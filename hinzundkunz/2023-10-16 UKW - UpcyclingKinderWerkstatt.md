---
id: 6670-1697473800-1697479200
title: UKW - UpcyclingKinderWerkstatt
start: 2023-10-16 16:30
end: 2023-10-16 18:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/ukw-upcyclingkinderwerkstatt-8/
isCrawled: true
---
In der UpcyclingKinderWerkstatt kannst du mit den zahlreichen kunterbunten Materialien experimentieren und verschiedene Werkzeuge kennenlernen. Jeden 1. und 3. Montag im Monat zeigen wir euch eine neue Projektidee. Die UKW findet von 16:30 bis 18:00 Uhr statt und ist für Kinder im Alter von 7-12 Jahren gedacht. 
!!Clevere KidZ melden sich an!! Wir würden uns, wenn ihr uns eine kurze Mail an workshop@kunzstoffe, eine spontane Nachricht über what’s App (krimZkrams | kunZstoffe) oder eine SMS an 0163 4846916 schickt!! Danke! 
—————————————————————————————————————————————————— 
Liebe Leute, 
für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung. 
Wir wünschen euch viel Spaß. Euer kunZstoffe-Team