---
id: 6774-1700758800-1700766000
title: Stempel und Co - Geschenkpapier to go
start: 2023-11-23 17:00
end: 2023-11-23 19:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/stempel-und-co-geschenkpapier-to-go/
isCrawled: true
---
Dieses Angebot findet im Rahmen der Europäischen Woche für Abfallvermeidung statt. Heute gibt es passend zum diesjähriges Thema der Aktionswoche „Verpackungen“ ein kunterbuntes Stempeln für selbst gemachtes Geschenkpapier. Es muss nicht immer neu gekauft sein ;O) 
Start ist 17 Uhr und du kannst jederzeit dazukommen. 
—————————————————————————————————————————————————— 
Liebe Leute, 
für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung. 
Wir wünschen euch viel Spaß. Euer kunZstoffe-Team