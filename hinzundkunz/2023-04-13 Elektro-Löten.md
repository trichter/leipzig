---
id: 6395-1681398000-1681405200
title: Elektro-Löten
start: 2023-04-13 15:00
end: 2023-04-13 17:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/elektro-loeten/
isCrawled: true
---
Heute gibt es eine kleine Einführung im „Elektrokabel und Anschlüsse selbst löten sowie reparieren“. Ihr könnt variabel im Zeitraum von 15-17 Uhr einsteigen. Spätester Beginn ist 16:15 Uhr da die Kleine Einführung  ungefähr 45 Minuten dauert. 

Wir bitten um eine verbindliche Anmeldung. Spontanes Vorbeikommen ist aber auch möglich und ggf. mit einer kleinen Wartezeit verbunden, in der ihr euch in unserer Sammlung genüsslich umschauen könnt ;O) 

—————————————————————————————————————————————————— 
Liebe Leute, 
für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung. 
Wir wünschen euch viel Spaß. Euer kunZstoffe-Team