---
id: 6783-1701356400-1701370800
title: TeeZeit - Der gemütliche Generationsaustausch...
start: 2023-11-30 15:00
end: 2023-11-30 19:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/teezeit-der-gemuetliche-generationsaustausch/
isCrawled: true
---
… bei Kaffee und Gebäck. 
Jeden letzten Donnerstag im Monat findet bei uns ein offener Treff statt, bei dem sich ausgetauscht werden kann, was aus abgegebenen Sachen Neues entstanden ist oder über Techniken beraten wird. Unsere Idee ist es, dass in gemütlicher Runde(Material-)Spendene auf Upcycling-Ideen treffen und neue Kreationen gemeinsam erschaffen werden.  Kommt zum Schnacken, Geben, Machen vorbei! 
—————————————————————————————————————————————————— 
Liebe Leute, 
für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung.