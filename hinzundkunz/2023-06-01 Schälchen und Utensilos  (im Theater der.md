---
id: 6465-1685628000-1685638800
title: Schälchen und Utensilos  (im Theater der jungen Welt)
start: 2023-06-01 14:00
end: 2023-06-01 17:00
address: Theater der Jungen Welt, Lindenauer Markt 21, Leipzig, 04177
link: https://kunzstoffe.de/event/lavendeldruck/
isCrawled: true
---
Heute sind wir auch im Theater der jungen Welt als Angebot zum PLAY & CONNECT zu Gast und bieten Schälchen und Utensilos aus einer bunten Materialvielfalt an.