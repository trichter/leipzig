---
id: 6663-1695141000-1695146400
title: Print it real good
start: 2023-09-19 16:30
end: 2023-09-19 18:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/print-it-real-good/
isCrawled: true
---
… Drucken mit Restmaterialien 
  
Anmeldung erwünscht: unter workshop@kunzstoffe.de | 0163 4846916 | WhatsApp (krimZkrams | kunZstoffe) oder im Laden (Kontaktdaten nicht vergessen!) 
—————————————————————————————————————————————————— 
Liebe Leute, 
für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung. 
Wir wünschen euch viel Spaß. Euer kunZstoffe-Team