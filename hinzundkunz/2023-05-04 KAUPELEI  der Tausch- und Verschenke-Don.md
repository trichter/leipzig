---
id: 6449-1683217800-1683225000
title: KAUPELEI | der Tausch- und Verschenke-Donnerstag
start: 2023-05-04 16:30
end: 2023-05-04 18:30
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/kaupelei-der-tausch-und-verschenke-donnerstag-2/
isCrawled: true
---
…. kommt vorbei zum Tauschen und Verschenken von Allerlei angesammelten Dingen, die zu schade sind zum Wegwerfen. 
…. jeden ersten und dritten Donnerstag vor dem Laden oder bei schlechten Wetter im Durchgang zum Hof