---
id: 6446-1685637000-1685644200
title: KAUPELEI | Der Tausch- und Verschenke-Donnerstag
start: 2023-06-01 16:30
end: 2023-06-01 18:30
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/kaupelei-der-tausch-und-verschenke-donnerstag/
isCrawled: true
---
…. kommt vorbei zum Tauschen und Verschenken von Allerlei angesammelten Dingen, die zu schade sind zum Wegwerfen. 
…. jeden ersten und dritten Donnerstag vor dem Laden oder bei schlechten Wetter im Durchgang zum Hof