---
id: 6703-1700150400-1700161200
title: Nähtreff im Osten
start: 2023-11-16 16:00
end: 2023-11-16 19:00
address: Seniorenbüro Ost INGE & WALTHER, Eisenbahnstraße 66, Leipzig, 04316
link: https://kunzstoffe.de/event/naehtreff-im-osten-4/
isCrawled: true
---
Liebe Alle,
seid ihr Näh-Enthusiasten:innen, Neulinge oder habt ein Nähprojekt unterm Bett rumliegen an das ihr euch nicht alleine rantraut? Dann husch husch, kommt zu unserem Nähtreff ins INGE & WALTHER und lasst uns gemeinschaftlich NähErfahrungen austauschen, von einander lernen und jede Menge Nähspaß haben. 
alle Termin: 19.10. + 02.11. + 09.11. + 16.11. + 23.11., jeweils 16-19 Uhr 
_______________________________________________________________________________________________________ 
Liebe Leute, 
für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung.