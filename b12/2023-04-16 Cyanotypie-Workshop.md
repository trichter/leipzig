---
id: "137321522625817"
title: Cyanotypie-Workshop
start: 2023-04-16 11:00
end: 2023-04-16 17:00
locationName: Wohn- und Kulturprojekt B12
address: Braustraße 20, Leipzig
link: https://www.facebook.com/events/137320915959211/
image: 339021028_188457337292930_1625784519407898220_n.jpg
isCrawled: true
---
Wie im letzten Jahr gibt es bald an zwei Wochenenden die Möglichkeit, in den Räumen der B12 mit der Frühlingssonne und Berliner Blau zu experimentieren.
Cyanotypie oder „Lichtmalerei“ ist ein Druck- bzw. Belichtungsverfahren, das vielfältige Möglichkeiten bietet, Formen und Figuren auf verschiedensten Untergründen zu fixieren.
Es kann für erfahrene Künstlerinnen ebenso spaßig sein wie für interessierte Laien.
Im Workshop wird in die Geschichte und Funktionsweise der Cyanotypie eingeführt, mit den notwendigen Chemikalien eine lichtempfindliche Lösung angemischt und mit unterschiedlichen Belichtungsmöglichkeiten experimentiert.
Drucke auf Papier sind ebenso denkbar wie auf Holz oder Stoff (auch zB Beutel, Klamotten usw).
Die Workshop-Leiterin ist autodidaktische Grafikerin und arbeitet seit 15 Jahren mit verschiedensten Methoden des Hoch- und Tiefdrucks.
Die Kurse finden am 15., 16. April sowie am 20., 21. Mai jeweils von 11-17 Uhr statt. Jeder Tag ist ein eigenständiger Termin, es muss sich also nicht für ein ganzes Wochenende angemeldet werden (das geht aber natürlich auch)! Jeder Bei Bedarf kann alles auch zusätzlich auf Englisch erzählt werden.
Die Teilnahme am Kurs ist kostenlos bzw. gegen Soli-Spende an das Projekt B12, allerdings fallen Materialkosten von ca. 20€/Tag/Person an. Chemikalien, Dunkelkammer und Belichtungsraum, Papier, etwas Stoff, Holz und vieles mehr, das benötigt wird, können dadurch gestellt werden. Falls es sich jemand gar nicht leisten kann, könnt ihr auch eine Mail schreiben und dann schauen wir, ob es Spielraum gibt (je nachdem, wie viele Personen sich für einen Termin schon angemeldet haben).
Die Teilnehmerinnenzahl ist beschränkt, deshalb bitten wir um rechtzeitige und verbindliche Anmeldung via Mail an cyanotypie@riseup.net. Anmeldeschluss für den ersten Termin ist der 6. April, für den 2. der 12. Mai.