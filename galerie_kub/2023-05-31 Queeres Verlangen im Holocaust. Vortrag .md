---
id: "268660612251121"
title: Queeres Verlangen im Holocaust. Vortrag und Diskussion mit Dr. Anna
  Hájková (University of Warwick)
start: 2023-05-31 18:00
locationName: Galerie KUB
address: Kantstr. 18, Leipzig-Connewitz
link: https://www.facebook.com/events/268660612251121/
image: 344751374_550278877035379_1172061893441329168_n.jpg
isCrawled: true
---
Vortrag und Diskussion: Queeres Verlangen im Holocaust - Eine Geschichte zwischen romantischer Liebe und Nötigung. Mit Dr. Anna Hájková (University of Warwick, UK)

Moderation: Martin C. Winter (Universität Leipzig)

In ihrem Vortrag bietet Anna Hájková, die Pionierin der queeren Holocaustgeschichte, eine Einführung in das Thema. Wie sah das gleichgeschlechtliche Verlangen der von den Nazis verfolgten Jüdinnen und Juden in Konzentrationslagern, Ghettos und Verstecken aus, und was bedeutete es für die Menschen? Warum reagierten die Lagergesellschaft sowie die Überlebenden nach dem Krieg so oft mit homophoben Vorurteilen?
Anhand beispielhafter Geschichten, wie denen von Anne Frank, Margot Heumann, oder der einer Leipziger KZ-Aufseherin, die Gefangene zu erzwungenen „Beziehungen” nötigte, reflektiert Hájková über eine komplexe und lange verschwiegene Geschichte, die neue Perspektiven auf die Handlungsmacht der Verfolgten, die Bedeutung des Stigmas bis heute und die emanzipatorische Kraft queerer Geschichte wirft.

TW: Der Vortrag wird sexualisierte Gewalt thematisieren.


Dr. Anna Hájková ist associate professor an der University of Warwick. Sie ist die Autorin von The Last Ghetto. An Everyday History of Theresienstadt (2020) und Menschen ohne Geschichte sind Staub. Homophobie und Holocaust (2021).

Dr. Martin Clemens Winter ist wissenschaftlicher Mitarbeiter und Alfred Landecker Lecturer am Historischen Seminar der Universität Leipzig. Er forscht derzeit zu »Unternehmenskultur, Zwangsarbeit und Judenmord beim Leipziger Rüstungskonzern HASAG«.