---
id: "519173823682205"
title: Fluch der guten Tat
start: 2023-03-09 19:00
locationName: Galerie KUB
address: Kantstr. 18, Leipzig-Connewitz
link: https://www.facebook.com/events/519173823682205/
image: 329850246_757940835355195_3604400711539029413_n.jpg
isCrawled: true
---
Die documenta15 ist noch in frischer Erinnerung. Ebenso die zum Teil wenig fruchtbaren Debatten: Regelmäßig folgte auf die Skandalisierung antisemitischer Gehalte diverser Exponate die teils schroffe Zurückweisung dieser Kritik. Jenseits der Diskussionen um einzelne Ausstellungsstücke stellte der Kunsttheoretiker Bazon Brock in verschiedenen Beiträgen grundsätzlichere Fragen zur letztjährigen Ausgabe der weltweit bedeutenden Kunstschau: Hatten wir es dort überhaupt mit Kunst zu tun? Was ist der Unterschied zwischen Kunst und Kultur? Sollten wir uns der vom Künstlerkollektiv ruangrupa kuratierten Veranstaltung kultursensibel-relativistisch annähern? Oder gilt es vielmehr, die zumindest historisch im Westen geprägten Konzepte von individueller Autorschaft und autonomem Kunstwerk zu verteidigen? Warum mündet umgekehrt die Berufung auf Kultur in Angriffe auf die Kunst, wie wir sie von autoritären Staaten kennen? Über diese und weitere Fragen möchten wir mit Bazon Brock ins Gespräch kommen.
 
Bazon Brock ist emeritierter Professor für Ästhetik und Kunstvermittlung und langjähriger Mitwirkender und kritischer Begleiter der dxocumenta in Kassel. Er war Teil der Fluxus-Bewegung und beschäftigt sich seit vielen Jahren praktisch und theoretisch mit Fragen der Kunst und ihrer Aufhebung. Brocks Einlassungen zur documenta15 sind in dem Band Kürzeste Besucherschule d15 (Köln 2022) zu finden und können in einem Interview mit dem Deutschlandfunk nachgehört werden.

Kürzeste Besucherschule d15: https://bit.ly/3Wy8KxD

Deutschland Funk Interview: https://bit.ly/3R0FYVh
 
Veranstaltet von Gruppe Minus und Roter Salon im Conne Island.