---
name: Galerie KUB
website:  https://www.galeriekub.de/
email: kontakt@galeriekub.de
scrape:
  source: facebook
  options:
    page_id: galerie.kub
---
Die galerie KUB versteht sich als Forum für zeitbasierte Kunst und politische Kultur. 