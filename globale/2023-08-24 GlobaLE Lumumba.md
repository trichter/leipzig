---
id: "810279440500943"
title: "GlobaLE: Lumumba"
start: 2023-08-24 20:00
locationName: Caracan
address: Neue Linie 20, Leipzig
link: https://www.facebook.com/events/810279440500943/
image: 363285880_780690977396285_4279038088381134107_n.jpg
isCrawled: true
---
Do. 24. August | 20.00 Uhr | Caracan im Auwald (Neue Linie 20), Connewitz

Lumumba (Spielfilm, Frankreich/Belgien/Haiti/Dtl. 2000, Regie: Raoul Peck, original mit deutschen Untertiteln)

Der Film erzählt die Geschichte der antikolonialen Befreiungsbewegung im Kongo. Patrice Lumumbas Bestrebungen nach nationaler Souveränität und dem Ende der kolonialen Vorherrschaft auf politischer, wirtschaftlicher und kultureller Ebene waren den westlichen Imperialisten ein Dorn im Auge. Die politischen Intrigen gegen den jungen ersten Premierminister des unabhängig gewordenen Kongo gipfelten 1961 in dessen Ermordung. Sein Kampf für die Unabhängigkeit diente als Inspiration für antikoloniale Bewegungen nicht nur in
Afrika, sondern weltweit. Die Geschichte ist ein Schlüssel für das Verständnis der politischen Wirren und der wirtschaftlichen Interessen im heutigen Kongo. 

Anschließend Gespräch u.a. mit Dr. Théophile Vissiennon.

Eintritt frei!

Mehr unter: https://www.globale-leipzig.de/?page_id=1467