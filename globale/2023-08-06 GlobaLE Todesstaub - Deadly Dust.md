---
id: "598554082433648"
title: "GlobaLE: Todesstaub - Deadly Dust"
start: 2023-08-06 21:00
locationName: Clara-Zetkin-Park
address: Leipzig
link: https://www.facebook.com/events/598554082433648/
image: 363353252_780661560732560_6566406326845353402_n.jpg
isCrawled: true
---
So. 06. August | 21.00 Uhr | Clara-Zetkin-Park, Wiese zwischen Sachsenbrücke und Musikpavillon, Zentrum Süd

Todesstaub - Deadly Dust (D. 2007, Regie: Frieder Wagner, original mit deutschen Untertiteln)

Uran-Munition wird trotz völkerrechtlicher Ächtung in zahlreichen heutigen Kriegen eingesetzt. Diese Munition durchdringt einen feindlichen Panzer wie ein Messer die Butter. Dabei verbrennt das radioaktive Uran, das zudem hochgiftig ist und eine Halbwertszeit von 4,5 Milliarden Jahren hat, zu winzigsten Nanopartikeln. Eingeatmet können sie tödliche Krebstumore verursachen und den genetischen Code aller Lebewesen für viele Generationen deformieren. In den betroffenen Ländern sind dadurch ganze Regionen unbewohnbar geworden. Der Film begleitet den deutschen Tropenarzt und Epidemiologen Dr. Siegwart-Horst Günther und seine amerikanischen Kollegen bei ihren Untersuchungen im Kosovo, in Bosnien und im Irak. 

- Veranstaltung anlässlich des internationalen Hiroshima und Nagasaki Gedenktages. 78 Jahre nach den Atombombenabwürfen der USA auf Hiroshima und Nagasaki am 6. und 9. August 1945 lagern heute noch immer über 16.000 Atomsprengköpfe auf
unserem Planeten mit einer Zerstörungsgewalt, die 900.000 Mal so groß ist wie die der Hiroshima-Bombe. Gemeinsame Veranstaltung der GlobaLE und dem Leipziger Antikriegsforum. -

Eintritt frei. 

Mehr: https://www.globale-leipzig.de/?page_id=1467