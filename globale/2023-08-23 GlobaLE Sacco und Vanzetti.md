---
id: "274942838479683"
title: "GlobaLE: Sacco und Vanzetti"
start: 2023-08-23 20:00
locationName: Schönauer Park
address: Leipzig
link: https://www.facebook.com/events/274942838479683/
image: 363064827_780682007397182_4215730620498408861_n.jpg
isCrawled: true
---
Mi. 23. August | 20.00 Uhr | Schönauer Park, Haltestelle Parkallee (im Rahmen des Schönauer Parkfests), Grünau

Sacco und Vanzetti (USA 2006, Regie: Peter Miller, original mit deutschen Untertiteln)

Erzählt wird die Geschichte der beiden italienischen Migranten und Anarchisten Nicola Sacco und Bartolomeo Vanzetti, die 1920 in den USA wegen Mordes angeklagt und – nach einem voreingenommenen und fragwürdigen von politischen und rassistischen Vorurteilen geprägten Prozess – am 23. August 1927 in Boston hingerichtet wurden. Ihr Leidensweg wurde zum Symbol für den bigotten und intoleranten Umgang mit Migranten und Dissidenten in den USA. Millionen Menschen auf dem gesamten Globus protestierten seinerzeit für Ihre Sache und gegen Klassenjustiz und Rassismus. Heute, rund 100 Jahre später, in Zeiten, in denen bürgerliche Freiheiten und Rechte von Menschen
aufgrund ihrer sozialen Herkunft und Abstammung nach wie vor immer wieder unter Beschuss sind, wirkt ihre Geschichte noch immer nach.

Gespräch u.a. mit Ika Audano.

Eintritt frei!

Mehr unter: https://www.globale-leipzig.de/?page_id=1467