---
id: "229043866762578"
title: "GlobaLE: El Entusiasmo"
start: 2023-08-08 21:00
locationName: Richard-Wagner-Hain
link: https://www.facebook.com/events/229043866762578/
image: 365674830_794666142665435_3423121399041566129_n.jpg
isCrawled: true
---
Di. 08. August | 21.00 Uhr | Richard-Wagner-Hain, Wiese am Elsterbecken, Zentrum West

El Entusiasmo (Spanien 2018, Regie: Luis E. Herrero, spanisch/katalanisch mit deutschen Untertiteln)

1975 starb Francisco Franco. Sein Tod machte in Spanien den Weg frei für eine aufbegehrende Jugend, die vieles nachzuholen hatte. Aber auch die exilierten Kämpfer aus dem Spanischen Bürgerkrieg kehrten zurück. In dieser Phase der sogenannten Transición, dem Übergang von der Diktatur zur bürgerlichen Demokratie, schien alles möglich – selbst der Traum, die Revolution von 1936 zu beenden. Der Film legt den Fokus auf einen der erbittertsten Gegner Francos: die Anarchisten und Syndikalisten. Nachdem ihre Organisation, die Gewerkschaft Confederación Nacional del Trabajodie (CNT) kurzzeitig wieder zu einer Massenbewegung wurde, zerrieb sie sich allerdings auch wieder fast ebenso schnell. 

Im Anschluss Gespräch mit der Aktivistin Sara Méndez und dem Zeitzeugen Felipe Orobón, der während der Transición in der Gewerkschaft CNT aktiv war.

Eintritt frei. 

Mehr unter: https://www.globale-leipzig.de/?page_id=1467