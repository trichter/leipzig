---
id: "1008849307202178"
title: "GlobaLE: Eat The Rich"
start: 2023-08-18 20:00
locationName: Caracan
address: Neue Linie 20, Leipzig
link: https://www.facebook.com/events/1008849307202178/
image: 363279208_780677800730936_1969429515420698308_n.jpg
isCrawled: true
---
Fr. 18. August | 20.00 Uhr | Caracan im Auwald (Neue Linie 20), Connewitz

Eat The Rich (Spielfilm, GB 1987, Regie: Peter Richardson, original mit deutschen Untertiteln)

Eine explosive Satire über die Rebellion einer Gruppe junger Menschen gegen die Unterdrückung durch die reiche Oberschicht. Mit anarchischem Humor und bissigen Dialogen entlarvt der Film die Absurdität der Klassengesellschaft und die Gier der Reichen. Ein Film, der zum Lachen bringt, während er gleichzeitig die Ungerechtigkeiten unserer Gesellschaft aufs Korn nimmt und zum Nachdenken anregt.

An dem Abend gibt es Leckeres vom Grill! ;)

Eintritt frei!

Mehr unter: https://www.globale-leipzig.de/?page_id=1467