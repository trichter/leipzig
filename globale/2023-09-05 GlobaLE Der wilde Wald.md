---
id: "620865996521886"
title: "GlobaLE: Der wilde Wald"
start: 2023-09-05 20:00
locationName: Botanischer Garten der Universität Leipzig
link: https://www.facebook.com/events/620865996521886/
image: 363417733_781597230638993_5926616800637911492_n.jpg
isCrawled: true
---
Di. 05. September | 20.00 Uhr | Botanischer Garten (Eingang Johannisallee), Zentrum Süd-Ost

Der wilde Wald (D 2021, Regie: Lisa Eder, deutsch)

„Natur Natur sein lassen“ lautet die Philosophie des Nationalparks Bayerischer Wald. Trotz eines massiven Widerstands ist diese Vision zu einem bahnbrechenden Vorzeigeprojekt geworden. Weil der Mensch nicht in die Natur eingreift, wächst aus den einstigen
Wirtschaftswäldern ein Urwald heran, ein einzigartiges Ökosystem und ein Refugium der Artenvielfalt. Menschen aus aller Welt kommen hierher. Sie suchen Antworten auf die Frage, warum wir mehr wilde Natur brauchen und was wir von ihr lernen können, um Wälder in Zeiten des Klimawandels auch für künftige Generationen zu bewahren. 

+ Gespräch mit Almut Gaisbauer (BUND Sachsen, u.a. Projektleitung "Rettungsnetz Wildkatze") und Dorett Bothmann vom Botanischen Garten Leipzig.

Eintritt frei!

Mehr unter: https://www.globale-leipzig.de/?page_id=1467