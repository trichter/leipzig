---
id: "1312901665978267"
title: "GlobaLE: Nachtredaktion - Lenin, Leipzig und die Iskra"
start: 2023-10-06 18:00
locationName: Felsenkeller Leipzig
address: Karl-Heine-Straße 32, Leipzig
link: https://www.facebook.com/events/1312901665978267/
image: 363332025_781660433966006_6822331204520800706_n.jpg
isCrawled: true
---
Fr. 06. Oktober | 18.00 Uhr | Felsenkeller (Karl-Heine-Str. 32), Plagwitz, "GlobaLE" und "Rosa's Salon" laden ein:

Nachtredaktion - Lenin, Leipzig und die Iskra (DDR 1973, Fernsehspiel von Otto Bonhoff, Regie Ursula Bonhoff, deutsch)

In der Druckerei Rauh und Pohle in Probstheida bei Leipzig arbeitet am Ende des Jahres 1900 eine geheime Nachtredaktion, um den Druck der ersten „Iskra“, die Zeitung russischer Revolutionäre, vorzubereiten. Teilnehmer der Sitzung ist auch ein gewisser Herr Meyer alias Uljanow, nach dem die russische und die Leipziger Geheimpolizei hektisch fahndet - erfolglos, wie sich zeigen wird.

Einführung und Diskussion: Dr. Detlef Kannapin (Berlin)

Eintritt frei!

Mehr unter: https://www.globale-leipzig.de/?page_id=1467