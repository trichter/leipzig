---
id: "151620237953759"
title: "GlobaLE: Die Abenteuer des Werner Holt"
start: 2023-09-07 20:30
locationName: Passage Kinos Leipzig
link: https://www.facebook.com/events/151620237953759/
image: 363326765_783372917128091_6496953388463277482_n.jpg
isCrawled: true
---
Do. 07. September | 20:30 Uhr | Passage-Kinos (Hainstraße 19a).

Die Abenteuer des Werner Holt (Spielfilm, Deutsche Demokratische Republik 1965, Regie: Joachim Kunert, Dieter Noll, deutsch)

Der Film erzählt die Geschichte zweier Freunde, die im Zweiten Weltkrieg auf unterschiedlichen Seiten kämpfen. Der rebellische Werner Holt und der disziplinierte Gilbert Wolzow sind begeisterte Mitglieder der Hitlerjugend und werden als Soldaten an die Front geschickt. Doch schnell werden sie mit der Grausamkeit des Krieges und der Sinnlosigkeit ihres Kampfes konfrontiert. Der Film zeigt, wie die beiden Freunde trotz ihrer unterschiedlichen Überzeugungen zusammenhalten und sich gegen das unmenschliche System auflehnen. Der Film spielt in der Zeit des Zweiten Weltkriegs, aber seine Botschaften und Themen sind zeitlos....

Eintritt frei!

Mehr unter: https://www.globale-leipzig.de/?page_id=1467