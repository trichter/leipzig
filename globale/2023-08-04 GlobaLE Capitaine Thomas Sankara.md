---
id: "193549810359713"
title: "GlobaLE: Capitaine Thomas Sankara"
start: 2023-08-04 21:00
locationName: Clara-Zetkin-Park
address: Leipzig
link: https://www.facebook.com/events/193549810359713/
image: 363350819_780659417399441_127611040828332251_n.jpg
isCrawled: true
---
Fr. 04. August | 21.00 Uhr | Clara-Zetkin-Park, Wiese zwischen Sachsenbrücke und Musikpavillon, Zentrum Süd

Captaine Thomas Sankara (Schweiz 2012, Regie: Christophe Cupelin, original mit deutschen Untertiteln)

Burkina Faso belegt heute wieder einen der allerletzten Plätze des weltweiten Human Development Index. Dabei versorgte das Land sich noch 1987 vollständig selbst mit Lebensmitteln und Waren des täglichen Bedarfs. In den 80ern stieg die Alphabetisierungsrate  stetig, Seuchen wurden mittels Volksimpfungen bekämpft und die Gleichberechtigung der Frau galt als grundlegende Bedingung für das Funktionieren der Gesellschaft. All diese Erfolge sind aufs Engste mit dem Namen Thomas Sankara verbunden, dem jungen Präsidenten, der nach der Revolution am 4.8.1983 an die Macht kam. Sankaras gewaltsamer Tod 1987 hat sehr zu seinem Mythos beigetragen. Heute versinnbildlicht er die Hoffnung eines ganzen Kontinents. 
+ Gespräch mit Hamado Dipama vom Arbeitskreis Panafrikanismus e.V.

Eintritt frei. 

Mehr auf: https://www.globale-leipzig.de/?page_id=1467