---
id: "247369494855752"
title: "GlobaLE: Du und mancher Kamerad"
start: 2023-08-30 20:00
locationName: Clara-Zetkin-Park
link: https://www.facebook.com/events/247369494855752/
image: 362612152_780707647394618_6922160447958365637_n.jpg
isCrawled: true
---
Mi. 30. August | 20.00 Uhr | Clara-Zetkin-Park, Wiese zwischen Sachsenbrücke und Musikpavillon, Zentrum Süd

Du und mancher Kamerad (Deutsche Demokratische Republik 1956, Regie: Annelie Thorndike, Andrew Thorndike, deutsch)

Der Film setzt sich mit den Ursachen und den politischen Hintergründen beider bisheriger Weltkriege und dem deutschen Streben nach der Weltherrschaft auseinander. Mit einer Vielzahl von historischen Filmdokumenten und Wochenschauen aus internationalen Archiven sowie nachgedrehten Szenen, werden mehrere Jahrzehnte deutscher und internationaler Geschichte beleuchtet.

Eintritt frei!

Mehr unter: https://www.globale-leipzig.de/?page_id=1467