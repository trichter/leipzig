---
id: "861161295516726"
title: "GlobaLE: Kobalt - die dunkle Seite der Energiewende"
start: 2023-09-06 20:00
locationName: Clara-Zetkin-Park
address: Ferdinand-Lassalle-/Friedrich-Ebert-/Karl-Tauchnitz-Straße, Leipzig
link: https://www.facebook.com/events/861161295516726/
image: 363385124_780714064060643_2415656371906015202_n.jpg
isCrawled: true
---
Mi. 06. September | 20.00 Uhr | Clara-Zetkin-Park, Wiese zwischen Sachsenbrücke und Musikpavillon, Zentrum Süd

Kobalt – die dunkle Seite der Energiewende (ARTE, Frankreich, Belgien 2022, Regie: Quentin Noirfalisse, Arnaud Zajtman, deutsch)

Damit Autofahren trotz Klimakrise weiter möglich bleibt und zugleich der CO2-Fußabdruck verringert wird, setzen Automobilindustrie und EU-Kommission auf Elektroautos. Doch in deren Batterien wird, zumindest bei einer der beiden marktführenden Technologien, ein äußerst problematischer Rohstoff verbaut: Kobalt. Der Film zeigt die dunkle Seite der Kobaltgewinnung. Die Kinderarbeit im Kleinbergbau ist nur
eines von vielen Problemen. Der gesamte Sektor ist von Korruption zersetzt. Böden werden verseucht, Gesundheit und Leben der
Menschen aufs Spiel gesetzt. Angesichts der gigantischen Probleme, die der begehrte Rohstoff mit sich bringt, sucht die EU nach anderen Wegen, um an Kobalt kommen. Und stellt eine unbequeme Frage: Sollten die Bergwerke in Europa wieder geöffnet werden? 

Anschließend Gespräch u.a. mit Julius Neu (Referent für Rohstoffpolitik, Wirtschaft und Menschenrechte bei INKOTA).

Eintritt frei!

Mehr unter: https://www.globale-leipzig.de/?page_id=1467