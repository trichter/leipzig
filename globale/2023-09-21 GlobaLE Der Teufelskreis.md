---
id: "856849402524248"
title: "GlobaLE: Der Teufelskreis"
start: 2023-09-21 20:00
locationName: Simsonplatz, 04107 Leipzig, Deutschland
link: https://www.facebook.com/events/856849402524248/
image: 363363044_781642137301169_6049253696358182140_n.jpg
isCrawled: true
---
Do. 21. September | 20.00 Uhr | ehemaliger Dimitroffplatz (heute Simsonplatz) vor dem Bundesverwaltungsgericht

Der Teufelskreis (Spielfilm, Deutsche Demokratische Republik 1956, Regie: Carl Ballhaus, deutsch)

Während der Hellseher Hanussen das Brennen eines großen Gebäudes prophezeit, werden von Nazi-Funktionären die letzten Absprachen zu einem Verbrechen getroffen, das der KPD zugeschoben werden soll: der Reichstagsbrand am 27. Februar 1933. Die konstruierte Anschuldigung dient den Faschisten als Grund für Massenverhaftungen von Kommunisten und anderen. Der propagandistisch aufgezogene Gerichtsprozess verkehrt sich allerdings ins Gegenteil... 
Veranstaltung anlässlich des 90sten Jahrestages des Beginns des Leipziger Reichstagsbrandprozesses am 21.09.1933 gegen Georgi Dimitroff und andere...

Gespräch mit dem Journalist und Buchautor Uwe Soukup und dem Historiker Dr. Volker Külow.

Eintritt frei!

Mehr unter: https://www.globale-leipzig.de/?page_id=1467