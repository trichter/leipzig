---
id: "733196718610187"
title: "GlobaLE: Not Just Your Picture"
start: 2023-08-22 20:00
locationName: Schönauer Park
link: https://www.facebook.com/events/733196718610187/
image: 362956475_780679617397421_4960552537457380686_n.jpg
isCrawled: true
---
Di. 22. August | 20.00 Uhr | Schönauer Park, Haltestelle Parkallee (im Rahmen des Schönauer Parkfests), Grünau

Not Just Your Picture (D. 2020, Regie: Anne Paq and Dror Dayan, deutsch)

Der Dokumentarfilm erzählt die Geschichte zweier junger deutsch-palästinensischer Geschwister, Ramsy und Layla Kilani, deren Leben
eines frühen Morgens im Sommer 2014 zerstört wurde, als ihr Vater Ibrahim zusammen mit seiner zweiten Frau und ihren fünf kleinen
Kindern durch einen israelischen Luftangriff während der Angriffe auf Gaza getötet wurden. Die beiden geraten in einen beschleunigten Prozess der Politisierung und Wiederentdeckung ihrer palästinensischen Wurzeln und kämpfen darum, Gerechtigkeit für ihre Familie zu finden. Gleichzeitig versuchen sie, eine politische Realität zu verstehen, die solche Gräueltaten nicht nur zulässt, sondern sogar versucht, jede Kritik daran zum Schweigen zu bringen. 

+ Gespräch mit Ramsy Kilani, einem der Protagonisten des Films.

Eintritt frei!

Mehr unter: https://www.globale-leipzig.de/?page_id=1467