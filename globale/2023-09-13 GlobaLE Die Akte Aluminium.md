---
id: "938644133887914"
title: "GlobaLE: Die Akte Aluminium"
start: 2023-09-13 20:00
locationName: Clara-Zetkin-Park
address: Ferdinand-Lassalle-/Friedrich-Ebert-/Karl-Tauchnitz-Straße, Leipzig
link: https://www.facebook.com/events/938644133887914/
image: 363365101_781607140638002_2502739059879313652_n.jpg
isCrawled: true
---
Mi. 13. September | 20.00 Uhr | Clara-Zetkin-Park, Wiese zwischen Sachsenbrücke und Musikpavillon, Zentrum Süd

Die Akte Aluminium (Österreich, BRD 2013, Regie: Bert Ehgartner, deutsch)

Leicht und rostfrei macht Aluminium unser Leben zweifellos einfacher. Noch vor 120 Jahren unbekannt weist das Metall heute in der industriellen Verarbeitung eine rasante Wachstumskurve auf. Man verbaut es in Fahrzeugen, verpackt Lebensmittel darin und verwendet es in der Pharmaindustrie. Aluminium ist heute weltweit das am zweithäufigsten verwendete Material. Die Herstellung ist jedoch extrem aufwendig und unökologisch: Mit ihr sind Verwüstungen beim Bauxit-Abbau, toxische Schlacken und extremer Stromverbrauch bei der Schmelze verbunden. Verarbeitet in Deos und Arznei steht es nach wie vor im Verdacht Auslöser verschiedenster Krankheiten zu sein. 

Anschließend Gespräch mit einer Vertreterin des Umweltbundesamtes (angefragt), einem Vertreter des Bundesinstituts für Risikobewertung (angefragt) und weiteren Gästen.

Eintritt frei!

Mehr unter: https://www.globale-leipzig.de/?page_id=1467