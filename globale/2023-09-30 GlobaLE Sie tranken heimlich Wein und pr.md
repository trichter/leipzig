---
id: "133304356483373"
title: 'GlobaLE: "Sie tranken heimlich Wein und predigten öffentlich Wasser" -
  Szenische Lesung'
start: 2023-09-30 20:00
locationName: Neues Schauspiel Leipzig
address: Lützner Str. 29, Leipzig
link: https://www.facebook.com/events/133304356483373/
image: 363326746_781650943966955_4642308099508243866_n.jpg
isCrawled: true
---
Sa. 30. September | 20.00 Uhr | Neues Schauspiel Leipzig (Lützner Straße 29), Lindenau

„Sie tranken heimlich Wein und predigten öffentlich Wasser“ (Szenische Lesung mit unbekannten, kapitalismuskritischen Texten Heinrich Heines)

Heine, in Deutschland verfolgt und zensiert, kritisierte im Pariser Exil mit essayistischer Verve und Präzision den Mitte des 19. Jahrhunderts in Frankreich und England entfesselten Kapitalismus: Die luxuriösen Konsumtempel, eine Begegnung mit dem damals reichsten Bankier, die Geldgier der Aktionäre, die stille Verzweiflung der Armen, die verbitterten revolutionären Arbeiter in den Eisenbahnfabriken, die von inszenierten Virtuosen wie Franz Liszt beherrschte Kulturszene – und nicht zuletzt die „dressierte Mittelmäßigkeit der Politik“. Heines Résumé: „Die heutige Gesellschaft verteidigt sich nur aus platter Notwendigkeit, ohne Glauben an das Recht, ja ohne Selbstachtung.“ Was könnte aktueller sein? 
Sprecherin: Carmen Sziller, Conférencier: Dr. Werner Rügemer.

Eintritt frei!

Mehr unter: https://www.globale-leipzig.de/?page_id=1467