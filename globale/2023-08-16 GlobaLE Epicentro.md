---
id: "2231076070423415"
title: "GlobaLE: Epicentro"
start: 2023-08-16 20:00
locationName: Richard-Wagner-Hain
link: https://www.facebook.com/events/2231076070423415/
image: 363388992_780673344064715_3623503556967236112_n.jpg
isCrawled: true
---
Mi. 16. August | 20.00 Uhr | Richard-Wagner-Hain, Wiese am Elsterbecken, Zentrum West

Epicentro (Kuba, Österreich 2021, Regie: Hubert Sauper, original mit deutschen Untertiteln)

Der Dokumentarfilm ist ein eindringliches Porträt des „utopischen“ Kuba und seiner unnachgiebigen Bevölkerung. Ausgehend von der
Explosion des amerikanischen Schlachtschiffes USS Maine 1898 im Hafen von Havanna, welche zum Mitauslöser des Krieges zwischen
den USA und Spanien wurde, unternimmt der Film eine Reise durch Geschichte und Gegenwart Kubas.

Gespräch u.a. mit Aktivisten der Kampagne Unblock Cuba. 

Eintritt frei.

Mehr unter: https://www.globale-leipzig.de/?page_id=1467