---
id: "512283647720054"
title: "GlobaLE: The Illusion Of Abundance"
start: 2023-09-20 20:00
locationName: Clara-Zetkin-Park
address: Ferdinand-Lassalle-/Friedrich-Ebert-/Karl-Tauchnitz-Straße, Leipzig
link: https://www.facebook.com/events/512283647720054/
image: 363359737_781611477304235_8138109194872492997_n.jpg
isCrawled: true
---
Mi. 20. September | 20.00 Uhr | Clara-Zetkin-Park, Wiese zwischen Sachsenbrücke und Musikpavillon, Zentrum Süd

The Illusion Of Abundance (Belgien, Peru, Honduras, Brasilien 2023, Regie: Matthieu Lietaert, Erika Gonzalez Ramirez, dt. UT)

Drei Frauen, drei Länder, drei Kämpfe: Erika Gonzales erzählt die Geschichten von mutigen Aktivistinnen in Peru, Honduras und Brasilien im Kampf gegen die globale Umweltzerstörung. Mit beeindruckenden Bildern und einem scharfen Blick für größere Zusammenhänge, wobei auch die Rolle von europäischen Firmen nicht ausgespart wird, begleiten wir Bertha, Carolina und Maxima. Sie erheben unnachgiebig ihre Stimmen, wenn transnationale Konzerne im Namen des Profits den Verlust von Umwelt und Menschenleben bewusst in Kauf nehmen.

Im Anschluss Gespräch mit Silvia Bodemer von FIAN und Koordinatorin der Kampagne Bergbau Peru.

Eintritt frei!

Mehr unter: https://www.globale-leipzig.de/?page_id=1467