---
id: "659700346217304"
title: "GlobaLE: Ernst Busch - Sänger, Schauspieler, Kommunist"
start: 2023-08-11 21:00
locationName: Richard-Wagner-Platz
address: Leipzig
link: https://www.facebook.com/events/659700346217304/
image: 363300163_780670934064956_4180182944175043422_n.jpg
isCrawled: true
---
Fr. 11. August | 21.00 Uhr | Richard-Wagner-Hain, Wiese am Elsterbecken, Zentrum West

Ernst Busch - Themenabend (Auszüge aus „1935 oder das Faß der Pandora“ und „In Spanien“, DDR 1981/1982 Regie: K.Wolf u.a., dt.)

Der Film kommentiert die Gesänge von Ernst Busch rund um die Ereignisse im Jahr 1935: mit der Einführung der Wehrpflicht, des
Arbeitsdienstes, dem Beginn der militärischen Aufrüstung und schließlich mit der Verabschiedung der Nürnberger Rassengesetze werden die Grundsteine für den Zweiten Weltkrieg und für die Massenvernichtung der jüdischen Bevölkerung in Deutschland gelegt. Im zweiten Teil des Abends wird es um Buschs Zeit in Spanien während des Bürgerkrieges 1936 - 1939 gehen. Busch sang unter anderem für die Internationalen Brigaden und die Soldaten der republikanischen Armee.

Gespräch mit Leuten vom Leipziger Arbeiterliederchor (ALLE).

Eintritt frei.

Mehr unter: https://www.globale-leipzig.de/?page_id=1467