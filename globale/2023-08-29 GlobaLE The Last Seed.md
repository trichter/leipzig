---
id: "146628538418176"
title: "GlobaLE: The Last Seed"
start: 2023-08-29 20:00
locationName: Caracan
address: Neue Linie 20, Leipzig
link: https://www.facebook.com/events/146628538418176/
image: 362628531_780705327394850_3428507267535998367_n.jpg
isCrawled: true
---
Di. 29. August | 20.00 Uhr | Caracan im Auwald (Neue Linie 20), Connewitz

The Last Seed (Südafrika, Tansania, D 2023, Regie: Andréa Gema, original mit deutschen Untertiteln)

Der Dokumentarfilm zeigt, wie das Erbe und die Zukunft der afrikanischen Landwirtschaft bedroht sind. Im Mittelpunkt des Films stehen Auseinandersetzungen um die Kontrolle von Saatgut. Afrikanische Experten und kleinbäuerliche Erzeuger zeigen, wie sie versuchen das Saatgut vor den zerstörerischen Praktiken der Konzerne zu retten und die Ernährungssysteme zu transformieren. Bauern und Bäuerinnen aus dem Senegal, Südafrika und Tansania berichten über die Nachhaltigkeit und Anpassungsfähigkeit ihrer landwirtschaftlichen Praktiken und teilen Weisheiten, die es wert sind, entdeckt zu werden. 

+ Gespräch mit Refiloe Joala (Institute for Poverty, Land and Agrarian Studies) sowie dem Büro der Rosa-Luxemburg-Stiftung in Johannisburg und Jürgen Meier vom Forum Umwelt und Entwicklung.

Eintritt frei!

Mehr unter: https://www.globale-leipzig.de/?page_id=1467