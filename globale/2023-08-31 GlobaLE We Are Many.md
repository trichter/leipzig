---
id: "1246630002681468"
title: "GlobaLE: We Are Many"
start: 2023-08-31 20:00
locationName: Clara-Zetkin-Park
address: Ferdinand-Lassalle-/Friedrich-Ebert-/Karl-Tauchnitz-Straße, Leipzig
link: https://www.facebook.com/events/1246630002681468/
image: 363355594_780709464061103_6874900474758499253_n.jpg
isCrawled: true
---
Do. 31. August | 20.00 Uhr | Clara-Zetkin-Park, Wiese zwischen Sachsenbrücke und Musikpavillon, Zentrum Süd

We Are Many (GB 2014, Regie: Amir Amirani, original mit deutschen Untertiteln)

Die US-Regierung hatte den Sturz des irakischen Premierministers Saddam Hussein seit 2001 erwogen. Im Jahr 2003 fallen militärische
Truppen unter Führung der USA in den Irak ein, mit dem Ziel, Saddam Hussein zu stürzen und so zu verhindern, dass dieser an
Massenvernichtungswaffen gelangen könne. Doch die Beweislage zur offiziellen Begründung des Krieges lässt in den Augen vieler zu
wünschen übrig. Daher formiert sich weltweit großer Widerstand gegen die kriegerische Intervention, welche am 15.Februar 2003 mit einer riesigen, globalen Demonstration, an der über 15 Millionen Menschen teilnehmen, ihren Höhepunkt erreicht. Den Krieg verhindern konnte der Protest dennoch nicht. 

Anschließend Gespräch u.a. mit dem Publizisten Peter Wahl (u.a. Vorstand bei Weltwirtschaft, Ökologie & Entwicklung, WEED).

Eintritt frei!

Mehr unter: https://www.globale-leipzig.de/?page_id=1467