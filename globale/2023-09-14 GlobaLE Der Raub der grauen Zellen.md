---
id: "295800226317022"
title: "GlobaLE: Der Raub der grauen Zellen"
start: 2023-09-14 20:00
locationName: Clara-Zetkin-Park
address: Ferdinand-Lassalle-/Friedrich-Ebert-/Karl-Tauchnitz-Straße, Leipzig
link: https://www.facebook.com/events/295800226317022/
image: 363385184_781609437304439_4222177330653468836_n.jpg
isCrawled: true
---
Do. 14. September | 20.00 Uhr | Clara-Zetkin-Park, Wiese zwischen Sachsenbrücke und Musikpavillon, Zentrum Süd

Der Raub der grauen Zellen (Argentinien, D 2023, Regie: Gaby Weber, deutsch & spanisch mit deutschen Untertiteln)

Die Ampelkoalition sucht Fachkräfte unter anderem in Argentinien, wo viele eine europäische Abstammung und eine helle Hautfarbe haben. Doch im „Globalen Süden“ sorgt das teilweise für Empörung wegen Ausbeutung und dem Abwerben der eigenen Fachkräfte, dem sogenannten „Braindrain“. Die öffentlichen Universitäten kennen keine Studiengebühren, - auch nicht für Ausländer. Sie werden aus Steuermitteln finanziert und wenn die gut ausgebildeten Fachkräfte nach ihrem Abschluss in den Norden emigrieren, droht dem Land eine weitere Verschärfung der Krise. Imperialismus wie zu kolonialen Zeiten, heißt es in Buenos Aires über die Pläne der deutschen Bundesregierung.

Im Anschluss Gespräch mit der Journalistin und Filmemacherin Gaby Weber und Rodrigo Zori Comba (Ser Humanos e.V.).

Eintritt frei!

Mehr unter: https://www.globale-leipzig.de/?page_id=1467