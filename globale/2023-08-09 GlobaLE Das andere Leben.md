---
id: "310188041432486"
title: "GlobaLE: Das andere Leben"
start: 2023-08-09 21:00
locationName: Richard-Wagner-Hain
link: https://www.facebook.com/events/310188041432486/
image: 363321658_780666410732075_1645264517115057129_n.jpg
isCrawled: true
---
Mi. 09. August | 21.00 Uhr | Richard-Wagner-Hain, Wiese am Elsterbecken, Zentrum West

Das andere Leben (D 2020, Regie: KO-Filmkooperative Berlin/Jena/Leipzig, deutsch mit englischen Untertiteln)

Die DDR war als sozialistischer Staat die real existierende Alternative zu Armut, Vereinzelung und Krieg. Kaum war die DDR von der BRD
angeschlossen worden, führte Deutschland wieder Krieg. Dem Krieg gegen Jugoslawien folgten der Afghanistankrieg sowie weitere
Bundeswehreinsätze in Mali, Kongo, Somalia etc. 2022 rief Kanzler Scholz mit der „Zeitenwende“ ein massives Aufrüstungsprogramm der Bundeswehr aus. Wir wollen in der Diskussion darüber sprechen, wie das Ende der DDR mit dem deutschen Kriegskurs zusammenhängt und welche Perspektiven sich für den Kampf gegen Krieg, Aufrüstung und Sozialabbau ergeben. Gespräch u.a. mit den Filmemacher/innen der Kommunistischen Organisation.

Eintritt frei!

Mehr unter: https://www.globale-leipzig.de/?page_id=1467