---
id: "628101185768272"
title: "GlobaLE: Kuhle Wampe oder Wem gehört die Welt?"
start: 2023-08-25 20:00
locationName: BasaMo Insel
link: https://www.facebook.com/events/628101185768272/
image: 363286999_780703137395069_6828673122857673213_n.jpg
isCrawled: true
---
Fr. 25. August | 20.00 Uhr | BasaMo Insel (Odermannstraße 8), Lindenau

Kuhle Wampe oder: Wem gehört die Welt? (Spielfilm, Dtl. 1932, Regie: Slatan Dudow, deutsch)

Berlin 1931. Der Film erzählt die Geschichte der Arbeiterfamilie Bönicke, die während der Weltwirtschaftskrise aus ihrer Wohnung vertrieben wird und in die Gartenkolonie »Kuhle Wampe« im Osten Berlins zieht. Bertolt Brechts Handschrift, der zusammen mit Ernst Ottwald für das Drehbuch verantwortlich war, ist deutlich zu erkennen. Der Film gilt als einer der ersten Filme des proletarischen Kinos und wurde zur Fertigstellung zunächst verboten, mit Kürzungen schließlich freigegeben und 1933 verboten die Faschisten den Film endgültig. Heute gilt er als ein Meilenstein des politischen Kinos. 

Vor dem Film Vorstellung des Vereins Akuhanya e.V. - sauberes Wasser für Mosambik durch Jerry Fulau.

Eintritt frei!

Mehr unter: https://www.globale-leipzig.de/?page_id=1467