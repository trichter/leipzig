---
name: Kritisches Lehramt Leipzig
website: https://kritischelehrerinnenleipzig.wordpress.com/
email: krile-leipzig@inventati.org
scrape:
  source: facebook
  options:
    page_id: KriLeLeipzig
---
Die kritischen Lehrer*innen Leipzig 9st eine Initiative, bei der sich Studierende des Lehramts aller Fächer und Richtungen zusammensetzen und alles, was im Spannungsfeld Schule – Lernen – Lehramtsstudium liegt, kritisch diskutieren, reflektieren und konstruktiv angehen.