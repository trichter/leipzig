---
name: Allmende Taucha
website: https://www.allmendeverein.de
email: info@allmendeverein.de
scrape:
  source: facebook
  options:
    page_id: allmendetaucha
---
Verein für #Ernährungssouveränität und #Agrarwende aus #Taucha bei #Leipzig. 