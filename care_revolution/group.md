---
name: Care Revolution
website: https://care-revolution.org/regionale-vernetzungen/leipzig/
scrape:
  source: facebook
  options:
    page_id: 1802999273348887
# TODO: crawl from https://care-revolution.org/termine/
---
Die Leipziger Regionalgruppe ist Teil des Netzwerks Care Revolution, das sich kritisch mit dem gesellschaftlichen Wert von Sorgearbeit auseinandersetzt.