---
id: "228081656769034"
title: "WORKSHOP: Kleine Landartprojekte im Garten"
start: 2023-07-08 16:00
end: 2023-07-08 18:30
locationName: Stadtgarten H17
address: Hähnelstraße 17, Leipzig
link: https://www.facebook.com/events/228081656769034/
image: 357150664_657098423128882_5999877162727380332_n.jpg
isCrawled: true
---
Vergängliche Kunst mit der Natur in der Natur.

Was im Wald geht, funktioniert im Miniaturformat auch in der grünen Insel Stadtgarten H17 – mitten in der Stadt! Lass dich mit kreativem Blick in die grüne Oase sinken und entschlüpfe dem Alltag beim Gestalten. Erfahre wie fantasievolle Beschäftigung mit Natur Kraft geben kann für die täglichen kleinen und großen Herausforderungen – des Lebens und des gesellschaftlichen Wandels.

Mit Naturpädagogin Henrike Müller von Eulenblick Naturzeit.
Mehr: https://eulenblick-naturzeit.de/

Nur mit Voranmeldung: h17-bildung@freiraumsyndikat.de

Die Teilnahme ist kostenlos. Der Stadtgarten H17 freut sich jedoch über Spenden.