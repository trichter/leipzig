---
id: "1742084919580467"
title: " Familienworkshop: Trickfilm mit Naturmaterialien selber machen"
start: 2023-05-20 15:00
end: 2023-05-20 18:30
locationName: Stadtgarten H17
address: Hähnelstraße 17, Leipzig
link: https://www.facebook.com/events/1742084919580467/
image: 343982280_763046615314009_1351611181292512836_n.jpg
isCrawled: true
---
Einen Trickfilm erstellen ist Kinderleicht!

In diesem Workshop lernt ihr, wie ihr euch selbst eine Trickfilmbox bauen könnt und wie ihr mit Materialien aus unserem schönen Nachbarschaftsgarten H17, mit einfachen Mitteln einen Trickfilm mit Naturmaterialien erstellen könnt.

Alles, was ihr mitbringen müsst, ist ein Smartphone. Wir arbeiten mit der kostenlosen Stop-Motion-App, welche sowohl auf Android und iOS-Smartphones funktioniert.

Was bedeutet Familienworkshop?
Gemeint ist hier, dass minderjährige Kinder in Begleitung von einer volljährigen Person kommen müssen… egal was ihr für euch als Familie definiert!

Du besitzt kein Smartphone?
Keine Sorge! Am Workshoptag könnt ihr die Filme auch mit einem Tablet produzieren, welches wir zur Verfügung stellen.

Verbindliche Anmeldung unter
h17-bildung@freiraumsyndikat.de

Die Anzahl der Teilnehmenden ist begrenzt. Eine Anmeldung verpflichtet zur Teilnahme. Die Teilnahme am Workshop ist kostenlos, wir bitten lediglich um einen Unkostenbeitrag von 5 Euro. ❤️