---
id: "168910472823288"
title: THEMENABEND:Ernährung in Balance - mit Vortrag zur „Planetary Health Diet"
start: 2023-06-23 19:00
end: 2023-06-23 22:00
locationName: Stadtgarten H17
address: Hähnelstraße 17, Leipzig
link: https://www.facebook.com/events/168910472823288/
image: 349063037_646690853460629_3609709094479993425_n.jpg
isCrawled: true
---
Die im Garten aufgebaute Ausstellung „Zukunft(s)Essen“ zu gesunder Ernährung im Klimawandel ehmen wir zum Anlass für einen ganzen Themenabend. Schon ab 18 Uhr ist die Ausstellung für BesucherInnen geöffnet.
Unter dem Motto Ernährung in Balance laden wir ab 19 Uhr Interessierte zu einem gemeinsamen Abendbüfett in den Gemeinschaftsgarten ein.
20 Uhr startet ein Vortrag zur „Planetary Health Diet“:

Die Zusammenhänge zwischen Ernährung, Gesundheit und der Gesundheit des Planeten Erde sind auf globaler Ebene von enormer Bedeutung. Die Planetary Health Diet ist eine Grundlage für einen Speiseplan sowie einer Ernährungsstrategie, um die Gesundheit des Menschen und der Erde gleichermaßen zu schützen.

Der Fachvortrag beginnt mit einem unterhaltsamen Überblick zur Entwicklung unseres heutigen Ernährungsstils. Im Anschluss daran werden die Auswirkungen unserer Ernährungssysteme auf Mensch und Planeten näher beleuchtet. Das vorgestellte Konzept der Planetary Health Diet sowie Lösungsansätze für diese globale Krise sollen Raum für Austausch, Diskussionen und Vernetzung bieten.

Referentin Eva Huttner ist Diplom Ökotrophologin und spezialisiert auf Ernährungs- und Umweltbildung an Schulen; auf Netzwerkarbeit für eine ökologische, klima- und sozial gerechte Nahrungsproduktion sowie -verteilung und auf die Entwicklung von Gesundheitsprogrammen im Bereich Bluthochdruck und Adipositas.