---
id: "604851284912936"
title: Küfa & Filmabend mit VegUtopia e.V.
start: 2023-04-28 18:30
locationName: Stadtgarten H17
address: Hähnelstraße 17, Leipzig
link: https://www.facebook.com/events/604851284912936/
image: 342889931_942428466947124_4466160484677377286_n.jpg
isCrawled: true
---
Die älteste Solawi Ostdeutschlands feiert ihr 11-jähriges Bestehen im Stadtgarten H17. Kommt vorbei und feiert mit.

Am 28.04 veranstaltet Vegutopia e.V. ein Küfa ab 18:30 Uhr. 20 Uhr läuft der Film „Cabbage, potatoes and other demons“ (Original mit engl. Untertitel: kleinbäuerlicher
Gemüseanbau in Rumänien).

Außerdem orgnisiert Vegutopia e.V. eine kleine Samentauschbörse und stellt das Projekt vor.

Wir freuen uns auf euch!