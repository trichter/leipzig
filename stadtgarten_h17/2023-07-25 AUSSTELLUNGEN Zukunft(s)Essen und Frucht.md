---
id: "184064904424233"
title: "AUSSTELLUNGEN: Zukunft(s)Essen und Fruchtbare Erde"
start: 2023-07-25 15:30
end: 2023-07-25 18:30
locationName: Stadtgarten H17
address: Hähnelstraße 17, Leipzig
link: https://www.facebook.com/events/184064897757567/
image: 357735562_659663992872325_3371403241609456355_n.jpg
isCrawled: true
---
Klimawandel und Landwirtschaft sind sehr eng miteinander verknüpft: Die heute verbreitetste Form der Landwirtschaft verursacht den Klimawandel zu großen Teilem mit. 

Erfahre mehr über die Zusammenhänge zwischen heutiger Land- und Lebensmittelwirtschaft mit dem Klimwandel. Unsere Ausstellungen zu Zukunft(s)Essen und Fruchtbarer Erde zeigen euch die Probleme und auch Lösungen auf.

Kommt vorbei!