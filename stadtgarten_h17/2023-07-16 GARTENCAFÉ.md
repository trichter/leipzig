---
id: "831576711871026"
title: GARTENCAFÉ
start: 2023-07-16 15:00
end: 2023-07-16 18:00
locationName: Stadtgarten H17
address: Hähnelstraße 17, Leipzig
link: https://www.facebook.com/events/831576711871026/
image: 357137112_657099829795408_5765631416613528137_n.jpg
isCrawled: true
---
Für die einen ein Kaffeekränzchen für die anderen das schönste Katerfrühstück Leipzigs. 😉

Kommt genießt Kaffee, Kuchen und Limo und tauscht euch übers gemeinschaftliche Gärtnern aus. Auch unsere gepflanzten Ausstellungen können besucht werden.