---
id: "5950915941701372"
title: "WORKSHOP: Nachhaltiges Gärtnern – Wasser sparen"
start: 2023-06-17 14:00
end: 2023-06-17 17:00
locationName: Stadtgarten H17
address: Hähnelstraße 17, Leipzig
link: https://www.facebook.com/events/5950915941701372/
image: 350519124_133220156393486_1678792390756367239_n.jpg
isCrawled: true
---
Hitzerekorde und langanhaltenden Trockenheit machen nicht nur uns Menschen zu schaffen. Auch Pflanzen und Tiere leiden darunter. In diesem Workshop geht es um die Notwendigkeit wassersparenden und ressourcenschonenden Gärtnerns.

Ein Input-Vortrag gibt einen Überblick über die Grundlagen der Boden- und Pflanzenphysiologie. Davon leiten wir praktische Tipps zum ressourcenschonenden Umgang mit dem Schutzgut Wasser ab und gehen auch auf eine ökologische sinnvolle Bodenbearbeitung ein.

Anschließend versuchen wir uns im Bau von „Ollas“ (spanisch für Töpfe), Gefäßen aus unbehandeltem Ton, die vergraben im Beet als Langzeitwasserspender dienen können.

👉 Der Workshop ist geeignet für alle Interessierten und kostenfrei, 🫰 über Spenden für die Materialkosten freuen wir uns!

👉 Anmeldung unter: h17-bildung@freiraumsyndikat.de