---
name:  Stadtgarten H17
website: https://freiraumsyndikat.de/?p=stadtgarten_h17
email: stadtgarten-h17@freiraumsyndikat.de
address: Hähnelstraße 17, Leipzig 
scrape:
  source: facebook
  options:
    page_id: 1551403855088744
---
Der Stadtgarten H17 ist ein Gemeinschaftsgarten in Leipzig-Lindenau.