---
id: "157419400488492"
title: "WORKSHOP: Pflanzenkohle selber machen"
start: 2023-06-24 13:00
end: 2023-06-24 17:00
locationName: Stadtgarten H17
address: Hähnelstraße 17, Leipzig
link: https://www.facebook.com/events/157419400488492/
image: 348875904_957207018658504_2095533997566245760_n.jpg
isCrawled: true
---
In diesem Workshop lernen wir, was der Boden mit dem Klima(wandel) zu tun hat, und wie wir mithilfe von Pflanzenkohle etwas tun können, um den Klimawandel zu bremsen. Gleichzeitig werden wir uns damit beschäftigen, wie uns Pflanzenkohle dabei helfen kann, in die Kreislaufwirtschaft im (Klein)garten einzusteigen. Dabei spielt auch das Thema Kompost eine große Rolle.

Wir werden vor Ort mithilfe eine Pyrolyseofens Pflanzenkohle aus Gehölzschnitt herstellen, fertigen Pflanzenkohlekompost anschauen und alle Fragen rund um das Thema Terra Preta / Pflanzenkohlekompost klären.

Der Workshop ist geeignet und offen für Alle, die sich mit diesem Thema beschäftigen und Boden gut machen wollen.


👉 Mit einem Referenten der Schreberjugend Berlin.

👉 Die Teilnahme ist kostenlos, über eine 🫶 Spende 🫶 an den Verein Stadtgarten H17 e.V. freuen wir uns.

👉 Vorraussetzung für die Teilnahme ist die verbindliche Anmeldung an: h17-bildung@freiraumsyndikat.de