---
id: "6146013658851529"
title: "Podium: Journalismus in Zeiten der Klimakrise"
start: 2023-06-13 19:00
locationName: Neues Rathaus Leipzig
address: Martin-Luther-Ring 4, Leipzig
link: https://www.facebook.com/events/6146013658851529/
image: 351322602_654508829829753_8822667675231359052_n.jpg
isCrawled: true
---
Unser zweites Podium in diesem Jahr widmen wir der Verantwortung des Journalismus in Zeiten der Klimakrise. Podiumsdiskussion mit Publikumsfragen im Anschluss.

Mit:
· Hannah Suppa (Chefredakteurin LVZ)
· Daniel Schlechter (Redaktion MDR Wissen)
· Carel Mohn (Chefredakteur klimafakten.de)

Moderation:
· Dominic Memmel (S4F Leipzig)

Neues Rathaus Leipzig
Stadtbüro | Burgplatz 1
Eintritt frei