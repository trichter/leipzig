---
name: Scientists 4 Future Leipzig
website: https://s4f-leipzig.de
email: Leipzig@Scientists4Future.org
scrape:
  source: facebook
  options:
    page_id: scientists4futureLeipzig
---
Wir sind eine Untergruppe der Scientists 4 Future Bewegung bestehend aus Wissenschaftslern aus Leipzig und Umgebung. Wir unterstützen die Anliegen der Fridays for Future Bewegung.