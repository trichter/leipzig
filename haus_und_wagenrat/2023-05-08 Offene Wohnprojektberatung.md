---
id: 78642-1683561600-1683567000
title: Offene Wohnprojektberatung
start: 2023-05-08 16:00
end: 2023-05-08 17:30
address: Lützner Straße 39, 04177 Leipzig, Lützner Str. 39, Leipzig, 04177
link: https://www.hwr-leipzig.org/event/offene-wohnprojektberatung-3/
isCrawled: true
---
Wenn ihr euch in eurem Freundeskreis schon lange mit der Idee tragt, ein Hausprojekt zu gründen, wenn du Mitstreiter für deine eigene Initiative suchst; oder wenn ihr als Gruppe überlegt, welches der nächste Schritt ist, dann ist die offene Projektberatung die richtige Anlaufstelle. 
Die Veranstaltung bietet in einer kostenfreien Einstiegsberatung einen ersten Überblick über Herangehensweisen, Rechts- und Organisationsformen… 
Die offene Beratung beginnt am Montag, den 08. Mai um 16 Uhr und findet in der Lützener Straße 39 statt.  
Du kannst an diesem Termin nicht? 
Die offene Beratung findet in der Regel jeden ersten Montag im Monat statt. Da es in Ausnahmefällen zu terminlichen Verschiebungen kommen kann, informiere dich bitte immer vorher noch mal über unsere Website. Wenn du den Termin dort findest, dann findet er statt.