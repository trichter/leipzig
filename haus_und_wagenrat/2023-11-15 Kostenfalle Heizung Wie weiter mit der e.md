---
id: 78694-1700074800-1700082000
title: "Kostenfalle Heizung: Wie weiter mit der energetischen Sanierung im
  Gründerzeitbestand von Wohnprojekten?"
start: 2023-11-15 19:00
end: 2023-11-15 21:00
address: hinZundkunZ, Georg-Schwarz-Straße 9, Leipzig, 04177
link: https://www.hwr-leipzig.org/event/kostenfalle-heizung-wie-weiter-mit-der-energetischen-sanierung-im-gruenderzeitbestand-von-wohnprojekten/
isCrawled: true
---
Sowohl Mietergemeinschaften die überlegen ihr Haus selbst vom Einzeleigentümer zu Erwerben als auch schon bestehende, selbstverwaltete Hausprojekte und Einzeleigentümer stehen aktuell vor dem selben Dilemma: 
„Wie wollen wir 2030 unsere (Gründerzeit-)Häuser bezahlbar und klimabewusst warm bekommen?“ 
Im Juni diesen Jahres kursierten in öffentlichen Stellungnahmen innerhalb des Bundeskabinetts Zahlen zu einer erforderlichen, künftigen CO2-Bepreisung, mit denen die Klimaziele im Wärme- bzw. Gebäudesektor erreicht werden können. Eine Verzehnfachung des CO2 Preises pro kWh wurde auf Basis von Analysen des Mercator-Instituts als für 2030 als erforderlich angesehen, insoweit staatliche Förderanreize diese nicht sozial abfedern würden. 

Was bedeutet das für die Sanierungs- und Finanzierungsplanung von kleineren Hauseigentümer_innen mit einer aktuell für untere bis mittlere Einkommen bezahlbaren Warmmiete in Leipzig?
Wie weg von Kohle und Gas, wenn eine komplette energetische Sanierung nicht seitens aller Mietparteien im Haus gestemmt werden kann?
Wie die Wohnungen warm bekommen, wenn ohne energetische Ertüchtigung Brennstoff und damit die Warmmiete in unsanierten Beständen perspektivisch schlicht zu teuer werden wird?

Diese und weitere Fragen möchten wir mit Ihnen/euch besprechen. 
  
Programm: 

15 min Inputvortrag: „Energetische Sanierungen in selbstverwalteten Hausprojekten – Von veränderten Rahmenbedingungen und schwierigen Aushandlungsprozessen.“ Florian Schartel (Haus- und WagenRat e.V // Konzeptberater im Netzwerk Leipziger Freiheit)
15 min Inputvortrag: „Wärmepumpen zur Grundlast in un- und teilgedämmten Gründerzeitbestand. Erste Erfahrungen und bekannte Hürden“ Corinna Scholz (cowerk architektur // Freie Architektin // Effizienzhaus Planerin und -Baubegleiterin für Wohngebäude (EIPOS))
90 min Austausch in Kleingruppen mit Vertreter_innen mehrerer Hausprojektgruppen, welche sich in unterschiedlichen Phasen der energetischen Sanierung(-splanung) befinden.