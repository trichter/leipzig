---
id: 78687-1699286400-1699293600
title: Offene Wohnprojektberatung
start: 2023-11-06 16:00
end: 2023-11-06 18:00
address: HWR-Laden, Georg-Schwarz-Str. 19, Leipzig
link: https://www.hwr-leipzig.org/event/offene-wohnprojektberatung-8/
isCrawled: true
---
Wenn ihr euch in eurem Freundeskreis schon lange mit der Idee tragt, ein Hausprojekt zu gründen, wenn du Mitstreiter für deine eigene Initiative suchst; oder wenn ihr als Gruppe überlegt, welches der nächste Schritt ist, dann ist die offene Projektberatung die richtige Anlaufstelle. 
Die Veranstaltung bietet in einer kostenfreien Einstiegsberatung einen ersten Überblick über Herangehensweisen, Rechts- und Organisationsformen… 
Die offene Beratung beginnt am Montag, den 6. November um 16 Uhr und findet in der Geor-Schwarz-Straße 19 statt.  
Du kannst an diesem Termin nicht? 
Die offene Beratung findet in der Regel jeden ersten Montag im Monat statt. Da es in Ausnahmefällen zu terminlichen Verschiebungen kommen kann, informiere dich bitte immer vorher noch mal über unsere Website. Wenn du den Termin dort findest, dann findet er statt.