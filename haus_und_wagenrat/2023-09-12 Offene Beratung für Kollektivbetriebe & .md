---
id: 78671-1694538000-1694543400
title: Offene Beratung für Kollektivbetriebe & Kooperativen
start: 2023-09-12 17:00
end: 2023-09-12 18:30
address: HWR-Laden, Georg-Schwarz-Str. 19, Leipzig
link: https://www.hwr-leipzig.org/event/offene-kollektivberatung-fuer-betriebe-kooperativen-2/
isCrawled: true
---
An alle Initiativen, die selbstverwaltet Ihren Lebensunterhalt bestreiten wollen und sich zu diesem Zweck zusammentun: 
Offene Kollektivberatung, 17:00-18:30 in der Georg-Schwarz-Straße 19 
Wir schauen gemeinsam, was euer Anliegen ist und schaffen einen offenen Austausch mit allen. Wir geben einen Überblick, bringen unsere Erfahrungen ein, verweisen euch an sinnvolle weitere Stellen und an ExpertInnen. 
Meldet euch bitte an unter: kollektivberatung [äät] hwr-leipzig.org Wer per E-Mail schon vorab kurz und knapp die mitgebrachten Themen mitteilen möchte, ist hierzu herzlich eingeladen! 
Die Beratung ist für euch kostenlos. Wir freuen uns sehr, wenn Ihr Mitglied des HWRs werdet, um die Struktur langfristig auf tragfähige Beine zu stellen. 
Mehr Infos zu uns Beratenden gibt’s hier.