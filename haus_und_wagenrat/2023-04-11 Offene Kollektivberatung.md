---
id: 78598-1681232400-1681237800
title: Offene Kollektivberatung
start: 2023-04-11 17:00
end: 2023-04-11 18:30
address: HWR-Laden, Georg-Schwarz-Str. 19, Leipzig
link: https://www.hwr-leipzig.org/event/offene-kollektivberatung/
isCrawled: true
---
An alle Initiativen, die selbstverwaltet Ihren Lebensunterhalt bestreiten wollen und zu diesem Zweck sich zusammentun. 
offene Kollektivberatung: 
am Dienstag, den 11.04.2023 in der Georg-Schwarz-Straße 19 
Wir schauen gemeinsam, was euer Anliegen ist und schaffen einen offenen Austausch mit allen. Wir geben einen Überblick, bringen unsere Erfahrungen ein, verweisen euch an sinnvolle weitere Stellen und an ExpertInnen. 
Meldet euch bitte an unter: kollektivberatung [äät] hwr-leipzig.org 
Die Beratung ist für euch kostenlos. Wir freuen uns sehr, wenn Ihr Mitglied des HWRs werdet, um die Struktur langfristig auf tragfähige Beine zu stellen.