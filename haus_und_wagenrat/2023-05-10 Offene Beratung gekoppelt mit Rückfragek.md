---
id: 78642-1683741600-1683750600
title: Offene Beratung gekoppelt mit Rückfragekolloquium
start: 2023-05-10 18:00
end: 2023-05-10 20:30
address: Stadtteilbüro Leipziger Westen, Karl-Heine-Straße 54, Leipzig, Sachsen,
  04229, Deutschland
link: https://www.hwr-leipzig.org/event/offene-wohnprojektberatung-3/
isCrawled: true
---
Wenn ihr euch in eurem Freundeskreis schon lange mit der Idee tragt, ein Hausprojekt zu gründen, wenn du Mitstreiter für deine eigene Initiative suchst; oder wenn ihr als Gruppe überlegt, welches der nächste Schritt ist, dann ist die offene Projektberatung die richtige Anlaufstelle. 
Die Veranstaltung bietet in einer kostenfreien Einstiegsberatung einen ersten Überblick über Herangehensweisen, Rechts- und Organisationsformen… 

Die Offene Beratung ist an dem Tag mit dem Rückfragekolloquium zu dieser Ausschreibung von 2 bebauten Grundstücken der LWB gekoppelt. An dem Verfahren Interessierte können hier Vertreter*innen von LWB, vom Amt für Wohnungsbau und Stadterneuerung (AWS) sowie vom Netzwerk Leipziger Freiheit zur Ausschreibung und den Grundstücken persönlich ansprechen. 

Du kannst an diesem Termin nicht? 
Die offene Beratung findet in der Regel jeden ersten Montag im Monat statt. Da es in Ausnahmefällen zu terminlichen Verschiebungen kommen kann, informiere dich bitte immer vorher noch mal über unsere Website. Wenn du den Termin dort findest, dann findet er statt.