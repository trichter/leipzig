---
id: 78584-1678723200-1678728600
title: Offene Projektberatung
start: 2023-03-13 16:00
end: 2023-03-13 17:30
address: HWR-Laden, Georg-Schwarz-Str. 19, Leipzig
link: https://www.hwr-leipzig.org/event/offene-projektberatung-11/
isCrawled: true
---
Wenn ihr euch in eurem Freundeskreis schon lange mit der Idee tragt, ein Hausprojekt zu gründen, wenn du Mitstreiter für deine eigene Initiative suchst; oder wenn ihr als Gruppe überlegt, welches der nächste Schritt ist, dann ist die offene Projektberatung die richtige Anlaufstelle. 
Die Veranstaltung bietet in einer kostenfreien Einstiegsberatung einen ersten Überblick über Herangehensweisen, Rechts- und Organisationsformen… 
Die offene Beratung beginnt am Montag, den 13. März um 16 Uhr und findet in der Georg-Schwarz-Str. 19 statt.  
Du kannst an diesem Termin nicht? 
Die offene Beratung findet in der Regel jeden ersten Montag im Monat statt. Da es in Ausnahmefällen zu terminlichen Verschiebungen kommen kann, informiere dich bitte immer vorher noch mal über unsere Website. Wenn du den Termin dort findest, dann findet er statt.