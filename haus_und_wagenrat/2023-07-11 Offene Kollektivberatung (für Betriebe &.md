---
id: 78660-1689094800-1689100200
title: Offene Kollektivberatung (für Betriebe & Kooperativen)
start: 2023-07-11 17:00
end: 2023-07-11 18:30
address: HWR-Laden, Georg-Schwarz-Str. 19, Leipzig
link: https://www.hwr-leipzig.org/event/offene-kollektivberatung-4/
isCrawled: true
---
An alle Initiativen, die selbstverwaltet Ihren Lebensunterhalt bestreiten wollen und zu diesem Zweck sich zusammentun: 
Offene Kollektivberatung, am Dienstag, den 11.7.2023, 17:00-18:30 in der Georg-Schwarz-Straße 19 
Wir schauen gemeinsam, was euer Anliegen ist und schaffen einen offenen Austausch mit allen. Wir geben einen Überblick, bringen unsere Erfahrungen ein, verweisen euch an sinnvolle weitere Stellen und an ExpertInnen. 
Meldet euch bitte an unter: kollektivberatung [äät] hwr-leipzig.org Wer per E-Mail schon vorab kurz und knapp die mitgebrachten Themen mitteilen möchte, ist hierzu herzlich eingeladen! 
Die Beratung ist für euch kostenlos. Wir freuen uns sehr, wenn Ihr Mitglied des HWRs werdet, um die Struktur langfristig auf tragfähige Beine zu stellen. 
Mehr Infos zu uns Beratenden gibt’s hier.