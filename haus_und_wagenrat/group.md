---
name:  Haus- und WagenRat e.V.
website: https://www.hwr-leipzig.org/
email: post@hwr-leipzig.org
address: Georg-Schwarz-Str. 19, 04177 Leipzig
scrape:
  source: ical
  options:
    url: https://www.hwr-leipzig.org/veranstaltungen/?ical=1
    # TODO: eventuell kaputt? (gerade keine veranstaltungen vorhanden zum testen)
---
Der Haus- und WagenRat e.V. ist ein Verein für selbstorganisierte Räume in und um Leipzig. Er ist aus der Zusammenarbeit selbstorganisierter Hausprojekte und Wagenplätze hervorgegangen, die sich gemeinsam für eine solidarische Stadt einsetzen.