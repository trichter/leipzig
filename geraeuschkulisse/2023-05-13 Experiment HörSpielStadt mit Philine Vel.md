---
id: "1629384590835675"
title: Experiment HörSpielStadt mit Philine Velhagen
start: 2023-05-13 10:00
end: 2023-05-14 16:30
locationName: Temporarium, Westwerk, Karl-Heine-Straße 87, 04229 Leipzig
link: https://www.facebook.com/events/1629384590835675/
image: 337294824_191174806714996_5754205276201190765_n.jpg
isCrawled: true
---
Beim zweitägigen Workshop mit der freien Kölner Theater- und Hörspielmacherin Philine Velhagen beschäftigen sich die Teilnehmenden mit dem öffentlichen Raum im Leipziger Westen, erlernen Velhagens Methoden für Improvisation mit Laien und entwickeln aus diesen Bausteinen gemeinsam eine Hörspiel-Collage. Zum Abschluss des Wochenendes stellen die Workshop-Teilnehmenden ihre Hörspiel-Collagen der Öffentlichkeit vor. 

ANMELDUNG: https://form.jotform.com/230781992177366

Philine Velhagen, freie Theater- und Hörspielmacherin aus Köln, entwickelt ihre Stücke durch im Realen stattfindende Versuchsanordnungen. So auch „Folge dem Schein“, das 2011 Hörspiel des Monats September wurde.

gefördert von der Stadt Leipzig.



PROGRAMM:


Freitag, 12.05.2023, 20-22 Uhr

„Folge dem Schein“

Hörabend und Gespräch mit Philine Velhagen

Ort: Heiter bis Wolkig, Röckener Straße 44, 04229 Leipzig


 
Samstag, 13.05.2023, 10-18.30 Uhr

Workshop drinnen und draußen

Ort: Temporarium, Westwerk, Karl-Heine-Straße 87, 04229 Leipzig

Tram 14 Plagwitz bis Haltestelle Karl-Heine/ Merseburger Str.

Ein- und Ausstieg nicht barrierefrei

 
Sonntag, 14.05.2023, 9.30 – 16.30 Uhr

Workshop drinnen und draußen

ca. 15 Uhr Werkstattgespräch mit allen Teilnehmenden und allen, die ihr einladet

 Ort: Temporarium, Westwerk, Karl-Heine-Straße 87, 04229 Leipzig