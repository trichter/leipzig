---
id: "707661407813733"
title: Folge dem Schein mit Philine Velhagen
start: 2023-05-12 20:00
locationName: Heiter bis Wolkig, Röckener Straße 44, Leipzig-Plagwitz
link: https://www.facebook.com/events/707661407813733/
image: 339120443_980203033343238_6316341712262080802_n.jpg
isCrawled: true
---
Geld ist pure Abstraktion – ein reines Tauschmittel ohne Inhalt. Was passiert, wenn man dieses Prinzip unterläuft?

Velhagens Hörspiel ist eine Versuchsanordnung: Sie hat beschlossen, sich für drei Tage dem Zufallsprinzip des Geldes zu unterwerfen. In dieser Zeit folgt Velhagen einem markierten Zehn-Euro-Schein auf seinem Weg von Hand zu Hand. Sie wird zum Begleitservice der Banknote – und der Menschen, die gerade in deren Besitz sind. Wohin der Weg des Geldes führt und was für Begegnungen auf diesem Weg liegen, ist offen.

Musik: Gregor Schwellenbach

Realisation: Philine Velhagen

Produktion WDR 2011

Gemeinsam mit der Autorin hören wir gemütlich auf der Wiese und kommen danach ins Gespräch.


gefördert von Stadt Leipzig

Am Samstag & Sonntag gibt Velhagen auch einen Workshop – ein StadtExperiment: https://www.facebook.com/events/1629384590835675