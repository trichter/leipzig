---
id: "280409444608715"
title: Geräuschkulisse Hörspielwiese im Schönauer Park
start: 2023-08-16 17:00
locationName: Schönauer Park
link: https://www.facebook.com/events/280409444608715/
image: 365679915_679109254255015_6419687278992403131_n.jpg
isCrawled: true
---
Mit unserem überdimensionierten Radio ziehen wir durch Grünau und verwandeln einen Nachmittag lang den Schönauer Park in eine Hörspielwiese. Kommt vorbei mit Picknickdecke und Lust am gemeinsamen Lauschen von Hörspielen und Radiofeature. Neben dem Hör-Programm erwarten euch interaktive Radiospiele und kleine Klangüberraschungen. 

Die Teilnahme ist kostenlos.

Treffpunkt: Schönauer Park. Zugang über Garskestraße.


Programm:

17:00 – 18:15
Hörspiel (nicht nur) für Kinder: Hetto und Charlie und der Miezvertrag

„Gibt es was zu Fressen, dann fresse ich das. Kommt mir einer dumm, gebe ich dem was zurück. Läuft mir ne Maus über den Weg, dann fang ich die. So einfach ist es.“ Ja, so einfach ist Hettos Leben bei der Familie Krawczyk. Klar, manchmal können die Menschen sehr anstrengend sein und eng ist es auch in der kleinen Wohnung. Aber sonst gibt es eigentlich keinen Grund zur Beschwerde. Bis der Brief vom Vermieter kommt. Plötzlich ist nichts mehr wie vorher und Hetto muss sich etwas einfallen lassen. Ob ihr Charlie, die von oben bis unten gebürstete Katze aus dem reichen Vorort helfen kann?

Zusammenfassung:
Hetto und Charlie sind zwei Katzen, die aus komplett verschiedenen Gegenden der Stadt kommen. Eines Tages lernen sie sich kennen und werden enge Freundinnen. Hetto bringt Charlie das Mäusefangen bei, und Hetto träumt von einer größeren Wohnung. Als Hetto plötzlich mitbekommt, dass ihre Familie aus der Wohnung raus muss und sie nicht mit kann und bei der Nachbarin mit dem lauten Hund wohnen soll, flüchtet sie und sucht bei Charlie Hilfe. Die wird allerdings gerade aus ihrer Wohnung geworfen, da sie ihrer Familie massenweise Mäuse ins Haus gebracht hat. Gemeinsam müssen Hetto und Charlie nun eine Lösung für ihre schwierige Situation finden.

Autor: Frank Wollberg
Regie, Ton, Technik und Bearbeitung: Tobias Ludwig
Länge: 1:11:15
Dramaturgie: Frank Wollberg, Tobias Ludwig 
Eine Produktion des experimentellen Radios der Bauhaus-Universität Weimar 2021

++++++++

18:15 – 19:00
Aufstand

Die Schere geht immer weiter auseinander. Der Kontostand steigt gnadenlos, und die Butter ist noch viel zu billig. Gar nicht zu reden vom allgemeinen Liebesentzug, von biergetränkten Kaschmirmänteln und vollgespuckten Cabrios. Keine leichte Situation für die Millionäre in unserer Gesellschaft. Niemand kann einen leiden, wenn man Geld hat, aber los wird man es auch nicht. Ausgegrenzt und erniedrigt fristet die Oberklasse ein tristes Leben, doch langsam regt sich Widerstand. Und die mutigsten Exemplare wagen sich sogar vors Mikrofon. 

Länge: 38 Minuten
Skript, Regie, Musik: Tom Heithoff
Mitwirkende: Dominik Stein, Christine und Helmut Winkelvoss, Lorenz Eberle, Tom Heithoff.
Autorenproduktion 2016 

++++++++

19:15 – 19:45
Aber trotzdem, trotzdem, der Vater war mir immer der Vater, Väter und Söhne – Feature zu einem Zitat von Franz Kafka

Söhne sprechen über ihre Väter. Im Interview vier Männer im Alter von 35 bis 50 Jahren aus Ost und West, Peter ist Autor, Wolf ist Therapeut, Bernado Künstler und Thomas Journalist. Sie verbindet nichts miteinander, außer dass sie alle Söhne von Vätern sind, die „Kriegskinder“ oder „Kriegsjugendliche“ waren. Väter und Söhne - ein universelles Thema – zwischen Konflikt und Liebe, Konkurrenz und Loslassen. 

Autor*in: Inés Burdow
Dauer: 30:32 min
Jahr: 2015
Regie: Inés Burdow

+++++++++

19:45 – 20:00

Re:Produktion

Zwei Freundinnen überbrücken unterschiedliche Zeitzonen mit Sprachnachrichten. Kilometer und Stunden voneinander entfernt drehen sie sich um wichtige Fragen und Entscheidungen: Eizellen einfrieren? Wie mit einer ungewollten Schwangerschaft umgehen? Kinder kriegen – oder keine? Und wenn ja… wann und wie?

„Re:Produktion“ hat den Hörspielpreis PiNball 2020 gewonnen. Basierend auf dem Kurzhörspiel haben Stefanie Hiem und Vivien Schütz letztes Jahr für den SWR2 die gleichnamige 10-teilige Serie Re:Produktion produziert und arbeiten aktuell an einer zweiten Staffel. 

Autorinnen: Stefanie Heim und Vivien Schütz 
Dauer: 15 min

++++++++

Das Hörspielprogramm ist Teil des Grünauer Kultursommers 2023. Mehr Infos zum Programm findet ihr unter: https://gruenauer-kultursommer.de/