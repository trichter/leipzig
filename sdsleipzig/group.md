---
name: SDS Leipzig
website: https://sdsleipzig.de
email: sds-leipzig@posteo.de
scrape:
  source: facebook
  options:
    page_id: sdsleipzig
---
Wer wir sind. Was wir tun.
Die Linke.SDS gibt es an über 60 Hochschulen. Wir sind ein sozialistischer und demokratischer Studierendenverband, der an der Universität und darüber hinaus für eine andere Welt kämpft. Mit unserem Namen knüpfen wir bewusst an die Traditionslinie des historischen SDS an, der das bewegungsorientierte Zentrum der Studierendenrevolte 1968 war. 