---
id: "968547477532832"
title: Sound Bath relax and fill
start: 2023-05-09 15:30
locationName: Galerie für Zeitgenössische Kunst Leipzig
address: Karl-Tauchnitz-Str. 9-11, Leipzig
link: https://www.facebook.com/events/968547477532832/
image: 345585426_194655933397796_5665410684673039293_n.jpg
isCrawled: true
---
##WE INVITE## 

!!!!We have changes for tomorrow 
instead of Movement & Picture, we invite you to a filling and relaxing session!!!!


Вітаю!
у нас зміни на завтра 
замість Рух&малюнок

запрошуємо на наповнюючу та розслаблюючу зустріч

Звукова ванна

Звукові ванни розслаблюють тіло, заспокоюють і наводять розум більш спокійний стан.  

Звуки, що пронизують тіло, знімають перенапругу м'язів шиї та плечей, а вібрації борються зі стресом та занепокоєнням.