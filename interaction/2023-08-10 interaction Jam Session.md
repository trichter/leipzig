---
id: "1244478659460159"
title: interaction Jam Session
start: 2023-08-10 18:00
end: 2023-08-10 20:00
locationName: Hildegarten Leipzig
link: https://www.facebook.com/events/1244478659460159/
image: 365036094_670437255124294_7587081283580870693_n.jpg
isCrawled: true
---
### WE INVITE ### 
INTERACTION JAM SESSION

Die interaction Jam Session  ist ein offener und diskriminierungsfreier Treff zum Musik machen und -hören.

Alle sind herzlich eingeladen mitzumachen, egal ob mit oder ohne Vorkenntnisse. Wir wollen gemeinsam Instrumente spielen, Musikstile aus unterschiedlichen Ländern ausprobieren, vielleicht auch nur zuhören, uns austauschen und zusammen Spaß haben.

Wenn ihr Instrumente habt, bringt sie gerne mit! Wir haben aber auch einige vor Ort.

Der nächste Termin ist der 10.08.2023 von 18.00 bis 21.00 Uhr.

Adresse:
Haus der Begegnung
Arno-Nitzsche-Str. 37
04277  Leipzig

Bitte beachtet, dass der Genuss von alkoholischen Getränken bei dieser Veranstaltung nicht erwünscht ist.

Die Teilnahme ist kostenlos, aber wir freuen uns über Spenden.

Die Jam Session ist eine gemeinsame Veranstaltung von Wir Sind Paten Leipzig und interaction Leipzig e.V.

—
gefördert vom Kulturamt der Stadt Leipzig.

 


***English version***

### WE INVITE ### 
INTERACTION JAM SESSION

Our jam session  is an open and non-discriminatory meeting for making and listening to music.

Everyone is welcome to participate, whether with or without previous knowledge. We want to play instruments together, try out music styles from different countries, maybe just listen, exchange ideas and have fun  together.

The next date is 10/08/2023 from 6:00pm to 9:00pm.

Address:
Hildegarten
Bürgerbahbhof Plagwitz 
Röckener Str. 44
04229 Leipzig



Please note that the consumption of alcoholic beverages is not welcome at this event.

Participation is free of charge, but we welcome donations.

The Jam Session is a joint event of Wir Sind Paten Leipzig and interaction Leipzig e.V.

—

sponsored by Kulturamt der Stadt Leipzig.