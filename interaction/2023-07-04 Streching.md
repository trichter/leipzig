---
id: "942873246975690"
title: Streching
start: 2023-07-04 15:30
locationName: Galerie für Zeitgenössische Kunst Leipzig
link: https://www.facebook.com/events/942873246975690/
image: 347549684_650877033746983_8419944070235100171_n.jpg
isCrawled: true
---
### WE INVITE ### [українською нижче]

🌟 We invite you to a Stretching Class at the SPACE FOR YOU!🌟

Stretching will help you to feel yourself more flexible, free to move and full of energy 🔥

In the lesson, we will:
✨ Develop flexibility and muscle stretching
✨ Practice reducing tension and feeling relaxed
✨ Improve posture and movements coordination
✨ Relieve pain in the back and joints
✨ Restore energy and lift up your mood.

Let Stretching become your favourite regular ritual for your health and harmony! ✨💆‍♀️💪

Address:
Galerie für Zeitgenössische Kunst
Karl-Tauchnitz-Straße 9-11

Time:
04.07, Tuesday, start at 15:30

Additional information:
✔️The training is suitable for all fitness levels.
✔️You are free to take mats in the SPACE FOR YOU.
✔️Comfortable clothes that do not limit moves are recommended.
✔️Sports shoes are not needed.

The number of participants is limited, registration is required.



🌟Запрошуємо на заняття із Стретчингу в ПРОСТОРІ ДЛЯ ТЕБЕ🌟

Стретчинг допоможе відчути себе більш гнучкими, вільними в рухах на повними енергії 🔥

На занятті ми будемо:
✨ Розвивати гнучкість та розтяжку м'язів
✨ Тренуватись зменшувати напругу і відчувати розслаблення
✨ Покращувати осанку та координацію рухів
✨ Знімати біль у спині та суглобах
✨ Відновлювати енергетику та підіймати настрій.

Нехай Стретчинг стане улюбленим регулярним ритуалом для здоров'я та гармонії! ✨💆‍♀️💪

Адреса:
Galerie für Zeitgenössische Kunst
Karl-Tauchnitz-Straße 9-11

Час:
04.07, вівторок, початок о 15:30

Додаткова інформація:
✔️Заняття адаптоване за навантаженням і підходить для всіх рівнів підготовки.
✔️Килимки для заняття можна взяти в Просторі для тебе.
✔️Рекомендуємо зручний одяг, що не обмежує рухів.
✔️Спортивне взуття не потрібне.

Кількість учасників обмежена, обов‘язкова реєстрація.