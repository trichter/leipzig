---
id: "535875185427903"
title: Yoga Class
start: 2023-06-13 15:30
locationName: Galerie für Zeitgenössische Kunst Leipzig
address: Karl-Tauchnitz-Str. 9-11, Leipzig
link: https://www.facebook.com/events/535875185427903/
image: 347405227_215832797964294_7345283125855099560_n.jpg
isCrawled: true
---
### WE INVITE ### [українською нижче]

🧘‍♀️We invite you to a yoga class in SPACE FOR YOU 🌸

Together with Olga Efimenko, we will warm up, release stress and take care of ourselves💫

Address:
Galerie für Zeitgenössische Kunst
Karl-Tauchnitz-Straße 9-11
Upstairs entrance to the villa

Time:
13.06, Tuesday, start at 15:30

Additional information:
✔️The class is suitable for all levels.
✔️You are free to take mats in the SPACE FOR YOU.
✔️Comfortable clothes that do not limit moves are recommended.
✔️Sports shoes are not needed.

The number of participants is limited, registration is required.

***

🧘‍♀️Запрошуємо на заняття з йоги в ПРОСТОРІ ДЛЯ ТЕБЕ🌸

Разом із Олею Єфименко будемо розминатися, відпускати стрес та піклуватися про себе💫

Адреса:
Galerie für Zeitgenössische Kunst
Karl-Tauchnitz-Straße 9-11
Вхід наверх по сходах у віллу

Час:
13.06, вівторок, початок о 15:30

Додаткова інформація:
✔️Заняття адаптоване за навантаженням і підходить для всіх рівнів підготовки.
✔️Килимки для заняття можна взяти в Просторі для тебе.
✔️Рекомендуємо зручний одяг, що не обмежує рухів.
✔️Спортивне взуття не потрібне.

Кількість учасників обмежена, обов‘язкова реєстрація.