---
id: "1397281570846478"
title: Art meeting for renewal of inner energy
start: 2023-06-06 15:30
locationName: Galerie für Zeitgenössische Kunst Leipzig
address: Karl-Tauchnitz-Str. 9-11, Leipzig
link: https://www.facebook.com/events/1397281570846478/
image: 350525341_1711422039319846_2307314015317979238_n.jpg
isCrawled: true
---
##WE INVITE## [українською нижче]

👩‍🎨We invite you to an art meeting for adults and children to renew the inner energy in SPACE FOR YOU🎨

We all know that it is not easy to react to sad or unexpected news. Art is a great tool for tension and stress relief.
With the help of artistic techniques and communication we will work on the relieving of anxiety and learn how to renew our emotional and energy resources.

Address:
Galerie fur Zeitgenössische Kunst
Karl-Tauchnitz-Strasse 9-11

Time:
06.06, Tuesday, start at 15:30

Additional information:
✔️The meeting is suitable for adults and children from 6 years old.
✔️You will find all the necessary materials in the SPACE FOR YOU.

The number of participants is limited.
When registering, please point the child also as a participant 🧒👧

***

👩‍🎨Запрошуємо на Арт-зустріч для відновлення енергії для дорослих та дітей в ПРОСТОРІ ДЛЯ ТЕБЕ🎨

Ми всі знаємо, як буває складно реагувати на несподівані чи неприємні новини. Малювання - чудовий інструмент для зняття напруги та скидання стресу.
За допомогою художніх технік та спілкування ми будемо працювати над подоланням тривожності та вчитись відновлювати наші емоційний та енергетичний ресурси.

Адреса:
Galerie für Zeitgenössische Kunst
Karl-Tauchnitz-Straße 9-11
Вхід наверх по сходах у віллу

Час:
06.06, вівторок, початок о 15:30

Додаткова інформація:
✔️Заняття підходить для дорослих і дітей від 6-ти років.
✔️Всі необхідні матеріали ви знайдете в Просторі для тебе.

Кількість учасників обмежена.
При реєстрації просимо вказувати дітей, як повноцінних учасників ☺️