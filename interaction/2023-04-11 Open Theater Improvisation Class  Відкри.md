---
id: "741953497526602"
title: Open Theater Improvisation Class // Відкрите заняття з імпровізаційного театру
start: 2023-04-11 15:30
locationName: Galerie für Zeitgenössische Kunst Leipzig
address: Karl-Tauchnitz-Str. 9-11, Leipzig
link: https://www.facebook.com/events/741953497526602/
image: 337258227_945219846632681_4118725476191470109_n.jpg
isCrawled: true
---
### WE INVITE ### [країнською нижче]
Dear everyone,  
We are excited to spend together another week full nice & creative activities in the SPACE FOR YOU!  
This Tuesday (11.04.), we invite you for the Theater Improvisation Class (18+)!  
Participation is free and is meant for the adults. You don't need any experience to take part - just your desire to spend inspiring time together, meet new people and discover something new. Moreover you can be part of a team for an improvisational show at the classes. We are invited to a gallery concert in May and July. This is optional. 
Within the class you will:
 train the skill of being here and now, choosing the best option for actions, communicating, developing creative thinking and speed of decision-making;
 learn how to choose the best action in the moment;
 how to integrate this skill in life - chose own tools for adaptation in constantly changing conditions;
 get motivated, energized and learn how to strengthen our abilities to cope stress of modern reslities;
 met like minded people.
To register - contact us via PM.  Looking forward to seeing you!  
-
Joint Project of the Galerie für Zeitgenössische Kunst Leipzig, interaction Leipzig e.V., @gfzkfuerdich, @gfzk.leipzig, @aanstzj.
Funded by the Social Welfare Office of the City of Leipzig.

***

Привіт усім, 
Наступного тижня ми знову чекаємо на вас та радіємо разом провести насичений, а також креативний час у ПРОСТОРІ ДЛЯ ТЕБЕ!  
У вівторок 11.04 // 15:30-17:30 запрошуємо вас на відкрите заняття з імпровізаційного театру (18+)! 
Участь - безкоштовна. Заняття розраховане на дорослих та не потребує театрального досвіду - лише ваше бажання цікаво провести час, зустріти нових людей та відкрити для себе щось нове.
На занятті ви:
 Отримаєте базове розуміння що таке імпровізація;
 Зрозумієте як вона інтегрується у життя через практичні вправи;
 Оберете свої інструменти для адаптації у постійно змінних умовах;
 Отримаєте дозу мотивації та енергії;
 Дізнаєтесь як посилити свої здібності;
 Приємну творчу атмосферу серед однодумців, які теж незалежно від зовнішніх умов продовжують розвиватись та формувати нове майбутнє.
Для запису, напишіть нам у приватні повідомлення, що бажаєте взяти участь.  Радіємо вам!  
-
Спільний проєкт Галереї сучасного мистецтва міста Ляйпциг, interaction Leipzig e. V., @gfzkfuerdich, @gfzk.leipzig, @aanstzj, @interaction_leipzig.
Проєкт реалізується за фінансової підтримки міста Ляпциг.