---
id: "226763959995924"
title: Move and Draw
start: 2023-05-09 15:30
locationName: Galerie für Zeitgenössische Kunst Leipzig
link: https://www.facebook.com/events/226763959995924/
image: 345187489_775624574066759_3749866728656744942_n.jpg
isCrawled: true
---
##WE INVITE##

💫Move & draw💫

A creative space for parents with children from 6 years old.

A place for creative pleasure, where parents and children can meet in an unusual dimension, full of experiments, discoveries and impressions.

It is an experimental platform for exploring oneself and one's body in a space without frames and templates.

We will explore the relationship between movement and drawing,
and:

 - develop body awareness and stimulate its motor and creative potential;
 - emphasize more on the creative process than on the result;
 - consider the experiment as a working method;
 - to grow confidence in oneself and others through co-creation;
 - to develop productive and lively relationships within the group through deep respect and recognition of the peculiarities of each individual member.

The workshop is based on the Segni mossi art therapy approach.

Please bring comfortable clothes that you are not afraid to be dirty.

Registration is open for 10 families (adult + child).
🤸🏼‍♂️🌸❤️☺️💕



💫Рух & малюнок💫

Простір творчості для батьків з дітьми віком від 6 років.

Місце для творчого задоволення, де батьки та діти можуть зустрітися у незвичайному вимірі, повному експериментів, відкриттів та вражень.

Це експериментальний майданчик для дослідження себе та свого тіла у просторі без рамок та шаблонів.

Ми будемо досліджувати взаємовідносини між рухом і малюнком,
а також:

- розвивати усвідомленість тіла та стимулювати його моторний та творчий потенціал;
- наголошувати більше на творчому процесі, ніж на результаті;
- розглядати експеримент як робочий метод;
- вирощувати впевненість у собі та в інших шляхом співтворчості;
- розвивати продуктивні та живі взаємини всередині групи шляхом глибокої поваги та визнання особливостей кожного окремого учасника.

Воркшоп заснований на арт-терапевтичному підході Segni mossi.

Будь ласка, візьміть з собою зручний одяг, який не шкода забруднити.

Відкрита реєстрація для 10 сімей (дорослий + дитина).
🤸🏼‍♂️🌸❤️☺️💕