---
id: "6354315387962963"
title: Jam Session
start: 2023-06-08 18:00
locationName: Wir sind Paten Leipzig - Supporter
address: Zschochersche Str. 50, Leipzig
link: https://www.facebook.com/events/6354315387962963/
image: 347247044_212531398264795_1282705604648596353_n.jpg
isCrawled: true
---
### WE INVITE ### [ english below ]

👋Es ist wieder so weit👋

Unsere Jam Session 🎺🥁🪘🎻 ist ein offener und diskriminierungsfreier Treff zum Musikmachen und -hören.
Alle sind herzlich eingeladen mitzumachen, egal ob mit oder ohne Vorkenntnisse. Wir wollen gemeinsam Instrumente spielen, Musikstile aus unterschiedlichen Ländern ausprobieren, vielleicht auch nur zuhören, uns austauschen und zusammen Spaß 🤸‍♂️ haben.
Wenn ihr Instrumente habt, bringt sie gerne mit! Wir haben aber auch einige Vor Ort.

Eine gemeinsame Veranstaltung von Wir Sind Paten Leipzig und interaction Leipzig e.V.

❤️‼️Wir treffen uns am Donnerstag den 08.06.2023 zwischen 18 und 20 Uhr, in der Zschocherschenstr. 50, 04229 Leipzig.‼️❤️

Bitte beachtet, dass der Genuss von alkoholischen Getränken bei dieser Veranstaltung nicht erwünscht ist.
--
gefördert vom Kulturamt der Stadt Leipzig.
***

🙌It´s time again🙌

Our jam session 🎺🥁🪘🎻 is an open and non-discriminatory meeting for making and listening to music.
Everyone is welcome to participate, whether with or without previous knowledge. We want to play instruments together, try out music styles from different countries, maybe just listen, exchange ideas and have fun 🤸‍♂️ together.
If you have instruments, please bring them with you! We also have some on site.

A joint event of Wir Sind Paten Leipzig and interaction Leipzig e.V.

❤️‼️We meet on Thursday 08.06.2023 between 6 and 8 pm at Zschocherschestr. 50, 04229 Leipzig.‼️❤️

Please note that the consumption of alcoholic beverages is not welcome at this event.
--
funded by the Cultural Affairs Office of the City of Leipzig