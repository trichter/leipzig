---
id: "1323049574950795"
title: Open Theater Improvisation Class
start: 2023-06-27 15:30
locationName: Galerie für Zeitgenössische Kunst Leipzig
address: Karl-Tauchnitz-Str. 9-11, Leipzig
link: https://www.facebook.com/events/1323049574950795/
image: 353014563_646725350828818_4321359428698283067_n.jpg
isCrawled: true
---
###WE INVITE### [українською нижче]

Dear everyone✨, we are excited to spend together another day full nice&creative activities in the SPACE FOR YOU! 💫 
This time we invite you for the Theater Improvisation Class (18+)! 🎉

Within the class you will:
🔸train the skill of being here and now, choosing the best option for actions, communicating, developing creative thinking and speed of decision-making;
🔹learn how to choose the best action in the moment;
🔸how to integrate this skill in life - chose own tools for adaptation in constantly changing conditions;
🔹get motivated, energized and learn how to strengthen our abilities to cope stress of modern realities;
🔸met like minded people.

Looking forward to seeing you! 🌸

Address:
Galerie fur Zeitgenössische Kunst
Karl-Tauchnitz-Strasse 9-11

Time:
27.06, Tuesday, start at 15:30

Additional information:
✔️ Participation is free and is meant for the adults.
✔️ You don't need any experience to take part - just your desire to spend inspiring time together, meet new people and discover something new.

The number of participants is limited, registration is required.

Joint Project of the Galerie für Zeitgenössische Kunst Leipzig, interaction Leipzig e.V., @gfzkfuerdich, @gfzk.leipzig, @aanstzj.

***

Привіт усім✨, наступного тижня ми знову чекаємо на вас та радіємо часу, який креативно проведемо разом у ПРОСТОРІ ДЛЯ ТЕБЕ! 💫

Запрошуємо вас на відкрите заняття з театральної імпровізації (18+)!🎉

На занятті ви:
🔸Отримаєте ширше розуміння поняття імпровізація
🔹Зрозумієте, як інтегрувати її в життя через практичні вправи
🔸Оберете свої власні інструменти адаптації в сьогоденні, що постійно змінює зовнішні умови
🔹Отримаєте заряд мотивації та енергії
🔸Дізнаєтесь, як розвинути свої здібності
🔹Станете частиною приємної творчої атмосфери серед однодумців, які теж незалежно від обставин продовжують формувати своє майбутнє.

Радіємо зустрічі з вами! 🌸

Кількість учасників обмежена, обов‘язкова реєстрація.

Адреса:
Galerie für Zeitgenössische Kunst
Karl-Tauchnitz-Straße 9-11
Вхід наверх по сходах у віллу

Час:
27.06, вівторок, початок о 15:30

Додаткова інформація:
✔️Заняття безкоштовне та розраховане на дорослих
✔️Участь не потребує театрального досвіду - лише ваше бажання цікаво провести час, зустріти нових людей та відкрити для себе щось нове.

Кількість учасників обмежена, обов‘язкова реєстрація.