---
id: "6371555276190951"
title: Відкрите заняття з німецької // Open German Lesson
start: 2023-03-21 15:30
end: 2023-03-21 17:30
locationName: Galerie für Zeitgenössische Kunst Leipzig
address: Karl-Tauchnitz-Str. 9-11, Leipzig
link: https://www.facebook.com/events/6371555276190951/
image: 336312033_767417481674998_8062361139813892432_n.jpg
isCrawled: true
---
### WE INVITE ### [українською нижче]

We invite everyone to learn German together at the Open Lesson at the SPACE FOR YOU! 
At the meeting, we will discover first words and phrases in German that might be helpful in everyday situations. The lessons are for beginners and will be adjusted to the interests and needs of the participants. Tasty drinks, cake, and snacks will be there for us to make our German learning experience most cozy and enjoyable. Looking forward to meeting you!
The participation is free and open for everyone. The lessons will be held several times per month and not require regular presence. 
We will be glad to greet you with children. There will be everything for drawing and Lego, as well as space to play around.
-
Joint Project of the Galerie für Zeitgenössische Kunst Leipzig, interaction Leipzig e.V., @gfzkfuerdich, @gfzk.leipzig, @claudiaandriichuk, @aanstzj. @interaction_leipzig 

***

Запрошуємо усіх бажаючих вивчати разом німецьку мову до відкритого заняття у ПРОСТОРІ ДЛЯ ТЕБЕ!
Під час зустрічі вивчатимемо перші слова та вирази для спілкування у різних щоденних ситуаціях. Заняття розраховані на початковий рівень та будуть адаптуватися до інтересів учасників та учасниць. Ну, і звичайно ми підготуємо для вас запашні напої та смаколики, аби зробити наш навчальний процес максимально затишним. Радіємо зустрічі!
Участь безкоштовна, довільна та відкрита для усіх бажаючих.
Заняття не потребують реєстрації та проходитимуть декілька разів на місяць.
Ми будемо раді вітати вас із дітками. На місці буде усе для малювання, а також лего.
-
Спільний проєкт Галереї сучасного мистецтва міста Ляйпциг, interaction Leipzig e. V., @gfzkfuerdich, @gfzk.leipzig, @claudiaandriichuk , @aanstzj, @interaction_leipzig.