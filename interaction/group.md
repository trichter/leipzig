---
name: interaction Leipzig
website: https://interaction-leipzig.de
email: kontakt@interaction-leipzig.de
scrape:
  source: facebook
  options:
    page_id: interaction.leipzig
---
interaction Leipzig ist Plattform und Aushandlungsort, an dem sich Menschen mit und ohne Flucht- oder Migrationsbiografie, neue und alte Leipziger*innen einbringen, engagieren, interagieren und gemeinsam neue Perspektiven eröffnen können. Bei interaction Leipzig können Menschen jeden Hintergrundes Workshops, Veranstaltungen oder simple Begegnungen initiieren und dafür Interessierte gewinnen.
Gleichzeitig suchen wir mit Initiativprojekten über künstlerische Zugänge nach neuen kulturellen Praxisformen, in denen Menschen mit und ohne Flucht- oder Migrationsbiografie gleichberechtigt mitgestalten können.