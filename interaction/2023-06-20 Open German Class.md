---
id: "178486841603449"
title: Open German Class
start: 2023-06-20 15:30
locationName: Galerie für Zeitgenössische Kunst Leipzig
address: Karl-Tauchnitz-Str. 9-11, Leipzig
link: https://www.facebook.com/events/178486841603449/
image: 347386814_641529391348414_5672035573648893597_n.jpg
isCrawled: true
---
### WE INVITE ### [українською нижче]

We invite everyone to learn German together at the Open Lesson at the SPACE FOR YOU!✨

At the meeting, we will discover first words and phrases in German that might be helpful in everyday situations. The lessons are for beginners and will be adjusted to the interests and needs of the participants. Tasty drinks, cake, and snacks will be there for us to make our German learning experience most cozy and enjoyable. Looking forward to meeting you!🤩

Address:
Galerie fur Zeitgenössische Kunst
Karl-Tauchnitz-Strasse 9-11

Time:
20.06, Tuesday, start at 15:30

Additional information:
✔️ The participation is free and open for everyone.
✔️ We will be glad to greet you with children. There will be everything for drawing and Lego, as well as space to play around.🌸

***

Запрошуємо усіх бажаючих вивчати німецьку мову на відкрите заняття у ПРОСТОРІ ДЛЯ ТЕБЕ!✨

Під час зустрічі ми вивчатимемо перші слова та вирази для спілкування у різних щоденних ситуаціях. Заняття розраховані на початковий рівень та будуть адаптуватися до інтересів учасників та учасниць. І, звичайно, ми підготуємо для вас запашні напої та смаколики, аби зробити навчальний процес максимально затишним. Будемо раді зустрічі!🤩

Адреса:
Galerie für Zeitgenössische Kunst
Karl-Tauchnitz-Straße 9-11
Вхід наверх по сходах у віллу

Час:
20.06, вівторок, початок о 15:30

Додаткова інформація:
✔️Участь безкоштовна та відкрита для усіх бажаючих без реєстрації
✔️Ми будемо раді вітати вас із дітками. На місці буде усе для малювання, а також лего.🌸