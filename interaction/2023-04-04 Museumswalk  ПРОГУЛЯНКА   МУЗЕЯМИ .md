---
id: "781043540105362"
title: "Museumswalk // ПРОГУЛЯНКА   МУЗЕЯМИ "
start: 2023-04-04 15:30
locationName: Galerie für Zeitgenössische Kunst Leipzig
address: Karl-Tauchnitz-Str. 9-11, Leipzig
link: https://www.facebook.com/events/781043540105362/
image: 336664878_256884493520489_2312451688347366383_n.jpg
isCrawled: true
---
### WE INVITE ### [english below] [українською нижче]

Nächste Woche wollen wir mit dem Raum für Dich, den Raum wechseln ;)

In der Galerie für zeitgenössische Kunst wurde im März die Ausstellung “Looking for a new Foundation” eröffnet. Es stellen die Künstler*innen Alexis Blake, Cihan Çakmak, Vajiko Chachkhiani, Pennie Key, Tatjana Stürmer und Leyla Yenirce aus. Die Arbeiten, Fotografien, Performances, Installationen und mehr beschäftigen sich mit dem Körper. 
Wie verändert sich der Körper? Wie verformt er sich? Wie wird der Körper durch das Außen eingeschränkt? 

Wir möchten am Dienstag 04.04.2023, zur gewohnten Zeit zwischen 15:30 und 17 Uhr, in die Ausstellung gehen. Treffpunkt ist vor dem Museum. Nachdem wir die Ausstellung gesehen haben, gehen wir für einen spielerischen Austausch in den gewohnten Raum, den Raum für Dich. Keine Deutschkenntnisse erforderlich. Jede Altersgruppe ist willkommen. Keine Kosten. Anmeldung nicht erforderlich.

***

Next week we are going to another room of the museum.

The exhibition “Looking for a new Foundation” opened in March in GfZK. The artists Alexis Blake, Cihan Çakmak, Vajiko Chachkhiani, Pennie Key, Tatjana Stürmer and Leyla Yenirce are exhibiting.Their works, photographs, performances, installations and more deal with the body. How does the body change? How does it deform? How is the body restricted by the outside?

We would like to go with you to the exhibition on Tuesday 04.04.2023. We meet at the usual time between 3:30 p.m. and 5 p.m. Meeting point is in front of the museum. After seeing the exhibition, we go to the usual room, the Room for You, for a playful exchange. No knowledge of German required. Any age group is welcome. No cost. No registration necessary. 

***

Наступного тижня пропонуємо вам змінити простір зустрічі учасників проєкту ПРОСТОРУ ДЛЯ ТЕБЕ! ;)

В Галереї сучасного мистецтва, яка допомагає нам із заходами, в березні розпочалась виставка «У пошуках нового фундаменту». У виставці беруть участь такі майстри як: Алексіс Блейк, Джіхан Чакмак, Ваджіко Чачхіані, Пенні Кі, Тетяна Стюрмер та Лейла Єнірце. Роботи художників, фотографії, перформанси та інсталяції розкривають тему людського тіла. Як воно змінюється? Як деформується? Які обмеження для нього створює зовнішнє середовище?

Ми запрошуємо вас на виставку у вівторок, 4 квітня 2023 року, у звичайний час з 15:30 до 17:00. Місце зустрічі -  перед входом до Галереї. Оглянувши виставку, ми повернемось до знайомої нам кімнати для обміну враженнями в ігровій формі. Знання німецької мови не обов'язкове. Вітаються учасники будь-яких вікових груп. Вхід безкоштовний.
Реєстрація не потрібна.