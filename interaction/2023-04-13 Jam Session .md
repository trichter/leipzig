---
id: "551065753800457"
title: "Jam Session "
start: 2023-04-13 18:00
locationName: interaction Leipzig
link: https://www.facebook.com/events/551065753800457/
image: 340793884_945312999995701_6509044692905287798_n.jpg
isCrawled: true
---
### WE INVITE ### [ english below / الترجمة إلى اللغة العربية في الاسفل ]
Unsere Jam Session ist ein offener und diskriminierungsfreier Treff zum Musikmachen und -hören.
Alle sind herzlich eingeladen mitzumachen, egal ob mit oder ohne Vorkenntnisse. Wir wollen gemeinsam Instrumente spielen, Musikstile aus unterschiedlichen Ländern ausprobieren, vielleicht auch nur zuhören, uns austauschen und zusammen Spaß haben.
Wenn ihr Instrumente habt, bringt sie gerne mit! Wir haben aber auch einige Vor Ort.
Eine gemeinsame Veranstaltung von Wir Sind Paten Leipzig und interaction Leipzig e.V.
Wir treffen uns am 13.4.2023 zwischen 18 und 20 Uhr, in der Arno-Nitzsche-Straße 37, Leipzig.
Bitte beachtet, dass der Genuss von alkoholischen Getränken bei dieser Veranstaltung nicht erwünscht ist.
--
gefördert vom Kulturamt der Stadt Leipzig.
***
Our jam session is an open and non-discriminatory meeting for making and listening to music.
Everyone is welcome to participate, whether with or without previous knowledge. We want to play instruments together, try out music styles from different countries, maybe just listen, exchange ideas and have fun together.
If you have instruments, please bring them with you! We also have some on site.
A joint event of Wir Sind Paten Leipzig and interaction Leipzig e.V.
We meet on 13.04.2023 between 6 and 8 pm at Arno-Nitzsche-Straße 37, Leipzig.
Please note that the consumption of alcoholic beverages is not welcome at this event.
--
funded by the Cultural Affairs Office of the City of Leipzig
***
بالموسيقي تتواصل الناس بغض النظر عن اصلهم و لغتهم أو ثقافتهم. و تحت هذا الشعار نعقد جلستنا الموسيقية كإجتماع مفتوح للجميع لعزف و ارتجال الموسيقى وأيضا للإستماع إليها.
نتشرف بدعوة جميع المهتمين للإشتراك معنا في هذه الجلسة الموسيقية ولا يهم إذا كانت لديكم خبرة / تجربة سابقة.
نود العزف و الغناء سوياً و تجربة الأساليب الموسيقية من الثقافات المختلفة و الحديث و تبادل الأفكار عن هذه الأساليب.
إذا كانت لديك آلة موسيقية يرجى إحضارها معك و لدينا أيضا بعض الآلات الموسيقية يمكن للجميع إستخدامها.
هذا الحدث منظم بالعمل المشترك من قبل Wir Sind Paten Leipzig و Interaction Leipzig e.V.
الرجاء أخذ العلم، أن تناول المشروبات الكحولية 
ممنوع في هذا الحدث.
الفعالية ممولة من مكتب الرعاية الاجتماعية في مدينة لايبزيغ.