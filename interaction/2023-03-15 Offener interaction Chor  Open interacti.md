---
id: "948755622777469"
title: Offener interaction Chor // Open interaction Choir
start: 2023-03-15 19:00
end: 2023-03-15 20:30
locationName: Garage Ost / Hermann-Liebmann-Str. 67, Leipzig
link: https://www.facebook.com/events/948755592777472/
image: 321804933_741986857594856_2309860989796202184_n.jpg
isCrawled: true
---
### INTERACTION CHOR ###

[ english below / الترجمة إلى اللغة العربية في الاسفل ]

Wir laden euch herzlich ein zur offenen Probe des interaction Chors!

Unsere Proben sind offen für alle, die mitsingen wollen. 

Wir singen, worauf wir gemeinsam Lust haben. Mehrsprachig und mehrstimmig. Lieder aus verschiedenen Ländern und Genres.

Vorkenntnisse sind nicht nötig. Die wichtigste Voraussetzung ist Begeisterung für das gemeinsame Singen und die Bereitschaft, regelmäßig den Chor zu besuchen.

Die Probe wird geleitet von Viktoriia und Ian.

Es gibt ausreichend Platz im Proberaum, um Abstände einzuhalten. 

----
Gefördert vom Kulturamt Leipzig und dem Freistaat Sachsen.
Sei Teil der musikalischen interaction Facebook-Gruppe: http://bit.ly/1RBryJF

+++

### INTERACTION CHOIR ###

We invite you to the open rehearsal of the interaction choir!

Our rehearsals are free and open for everyone willing to enjoy singing together. 

We sing whatever we feel like singing together. Multilingual and polyphonic. Songs from different countries and genres. 

Previous experience is not necessary. The only requirement is your desire to sing together and willingness to attend  regularly.

The rehearsals are hold by local musicians: Viktoriia und Ian.

There is enough space in the rehearsal room to keep distances. 

----
Funded by the Cultural Office of Leipzig and the Free State of Saxony.
Be part of the musical interaction Facebook group: http://bit.ly/1RBryJF

+++

### مجموعة كورال إنترآكشن ###

أخيراً و بعد طول إنتظار تستطيع مجموعة كورال إنترأكشن بالقاء مجدداً و بشكل دوري لعدة مرات.
نحن نغني فقط ما نشعر بالرغبة في القيام به معًا، أغاني من مختلف البلدان والأنواع وبلغات متعددة. المعرفة السابقة ليست ضرورية. كل المطلوب هو الحماس للغناء مع مجموعة والاستعداد لزيارة الكورال بانتظام.
تقود الجوقة فيكتوريا ميدفيدكو من أوكرانيا (عازفة البيانو والمغنية).
توجد مساحة كافية في غرفة التدريب للحفاظ على المسافات اللازمة بين الأشخاص. نحن نلتزم بشكل كامل بقوانين الوقاية من فيروس كوفيد ١٩.

---
كن جزءًا من مجموعة التفاعل الموسيقي على Facebook: http://bit.ly/1RBryJF