---
id: "234168822762396"
title: "Funk & Soul OPEN AIR "
start: 2023-07-08 14:00
end: 2023-07-08 22:00
locationName: heiter bis wolkig
address: Röckener Straße 44, Leipzig
link: https://www.facebook.com/events/234168822762396/
image: 358354695_2536946103156697_164276717611147550_n.jpg
isCrawled: true
---
Macht euch frisch für den 8.7. und das erste Funk & Soul Open Air aufm Platz. @ig.funkundsoul präsentieren bei uns nen sonnigen Tag mit kühlen Drinks und Groove Beats. Außerdem nackige Füße im Sand, geschmeidige Hüften, Liebe für Musik und Mensch, strahlende Augen bis Grünau, fliegende Herzchen...ihr wisst schon. 

16 DJ Crews.
Das wird heiß. 
Hell yeah. IT´S A GROOVY THING. 

Sexy Lineup hier und jetzt:

14:00 Lt. Dan + C'est Le Mongrel (ThinkLoud)

14:30 Bottrop Boy 

15:00 Nowrockizm + Fonk Diggins (BACK'N'FORTH)
#nowrockizm #fonk_diggins

15:30 Yaya Wiggle (Back - Shack - Track)

16:00 Duktus
#duktus75

16:30 Da Wiesel (Funkiest Man in Town/ Most Wanted Soundsystem)

17:00 Hipbone

17:30 Carool
#carool_dwyl

18:00 Soulution Disko Soundsystem
#soulutiondisko

18:30 Mr. Olsen & Senior Kiez (Polyester Club)
#polyesterclub #djseniorkiez

19:00 John Reed (IG Funk & Soul)
#ig.funkundsoul #reedflavor

19:30 Peanut Vendor (Soul Surgery / Soul Magic)

20:00 Koop@Soundsystem Oststrasse (All Styles - All Smiles)
#soundsystemoststrasse 

20:30 Pete Rogers (Dynamite Soul)

21:00 Cal Pas (Stue Notes)
#calpas74

21:30 Shakin Casi (Jester Wild/ Train Done Gone)
#shakincasi