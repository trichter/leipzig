---
id: "228170633237210"
title: 5 Jahre Heiter Open Air
start: 2023-05-06 14:00
end: 2023-05-06 22:00
locationName: heiter bis wolkig
address: Röckener Straße 44, Leipzig
link: https://www.facebook.com/events/228170633237210/
image: 343672013_622412659759477_5417881395379755917_n.jpg
isCrawled: true
---
So! 
Quizfrage des Monats: 
Wie lange gibts das Heiter?
EY SEIT MAI 2018! 
Unser Baby wird am 6.5. fast auf den Tag genau fünf Jahre alt. 
Fünf ❤️ 
Eröffnung war am 05.05.2018. Wir wussten damals alle nicht, ob das was wird und was das dann überhaupt wird und jetzt pulsiert das Heiter bis Wolkig im Sommer wie ein Herz.

Fünf Jahre Hände und Köpfe und Herzen und Schweiß und Leidenschaft und Liebe, so viel Liebe für diesen Ort. Auf diesem Platz, in diesem Holz, in jeder kleinen Ecke und in dieser ganzen frischen Meeresluft hier draußen stecken Abermillionen Liter des feinsten Herzblutes vieler vieler vieler lieber Menschen, die sich einfach verliebt haben, geblieben, gegangen und wiedergekommen sind. 

Der Platz lebt durch die großen, kleinen und mittelgroßen Menschen, die hier seit fünf Jahren ständig neues Herzblut rantragen, ob als Team oder Gäste. Ziemlich egal, wir gehören eh alle zusammen, weil dieser Ort uns verbindet. 

Mit dem ersten OPEN AIR der Saison 23 wollen wir danke sagen für 5 Jahre Heiter bis Wolkig. 
Achtung Tränchen Trigger. 

Also bitte kommt am 06.05.2023, 14:00-22:00 Uhr. 
…zu gegebenem Anlass mit mindestens 5 Freunden und pünktlich, weils ja immer schnell dolle voll wird!
Lineup gibts natürlich stabil von der @futurefabrik & friends. Das wird erwartungsgemäß wie immer hart gut. 

Wir freuen uns so dermaßen und erwarten euch zahlreich. 
Das wird magisch, noch magischer als sonst. ❤️

#liebe