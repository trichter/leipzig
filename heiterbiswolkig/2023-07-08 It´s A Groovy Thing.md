---
id: "240230615537784"
title: It´s A Groovy Thing FESTIVAL
start: 2023-07-08 14:00
end: 2023-07-08 22:00
locationName: heiter bis wolkig
link: https://www.facebook.com/events/240230615537784/
image: 357460640_1168511447386908_5385449698091776277_n.jpg
isCrawled: true
---
Hello Soul People, 

was gibt es schöneres im Sommer als in der Sonne zu tanzen, mit einen kühlen Drink in der Hand und zwischen den Zehen Sand? IT‘S FESTIVAL TIME ☀️🌴🥳 

Die IG FUNK & SOUL präsentiert im "Heiter bis wolkig" einen Tag lang die Cremé de la Cremé der Leipziger Organic Groove Szene, die sich gegenseitig die Schallplatten in die Hände reicht.

VINYL ONLY 🔥

"Es gibt also geschmeidige Hüften, Liebe für Musik und Mensch, strahlende Augen bis Grünau, fliegende Herzchen... ihr wisst schon." (Zitat Heiter bis wolkig)

Leckere Snacks für den Hunger zwischendurch gibts natürlich auch.

So bring yourself, your friends and your family.
Wir freuen uns auf Euch 🤗 


Das LINE UP:

14:00 Lt. Dan + C'est Le Mongrel (ThinkLoud)
14:30 Bottrop Boy 
15:00 Nowrockizm + Fonk Diggins (BACK'N'FORTH)
15:30 Yaya Wiggle (Back - Shack - Track)
16:00 Duktus
16:30 Da Wiesel (Funkiest Man in Town)
17:00 Hipbone
17:30 Carool
18:00 Soulution Disko Soundsystem
18:30 Mr. Olsen & Senior Kiez (Polyester Club)
19:00 John Reed (IG Funk & Soul)
19:30 Peanut Vendor (Soul Surgery / Soul Magic)
20:00 Koop@Soundsystem Oststrasse (All Styles - All Smiles)
20:30 Pete Rogers (Dynamite Soul)
21:00 Cal Pas (Stue Notes)
21:30 Shakin Casi (Jester Wild/ Train Done Gone)