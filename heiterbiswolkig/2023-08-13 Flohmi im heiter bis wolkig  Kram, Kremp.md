---
id: "304030488696109"
title: Flohmi im heiter bis wolkig | Kram, Krempel & Klimbim Flohmarkt im August!
start: 2023-08-13 11:00
end: 2023-08-13 18:00
locationName: heiter bis wolkig
address: Röckener Straße 44, Leipzig
link: https://www.facebook.com/events/304030488696109/
image: 358139681_989430805480478_4754542110247449429_n.png
isCrawled: true
---
Hereinspaziert, Hereinspaziert!

Nach dem letzten Flohmi im Juli geht es nun munter und fröhlich weiter. Der Sommer ist da & es wird Zeit mit zauberhaften Menschen das sonnige & unbeschwerte Leben zu genießen. ♥

Seid wieder dabei, wenn sich der wunderschöne Garten des heiter bis wolkig in eine Fundgrube für alte und neue Schätze verwandelt.

Beim bunten Markttreiben zwischen Stadt und Wildnis gibt es Kunst, Klamotten und allerhand Kram, Krempel & Klimbim zu entdecken.

Hier nimmt garantiert jeder sein neues ♥ – Teil mit nach Hause!
Weil Schätze suchen hungrig macht, gibt es leckeres Essen & Getränke an der Bar.

Es erwarten Euch außerdem wilde Spiele für Groß und Klein, Musik & jede Menge tolle Menschen aus der Nachbarschaft.

♥ Der Eintritt ist frei!

Wir freuen uns auf Euch!

Auch dieses Jahr werden wir wieder einen Teil der Einnahmen spenden. ♥ Ein Drittel der Einnahmen werden an das heiter bis wolkig gespendet.

Die Anmeldung für Standbesitzer*innen beginnt jeweils 2 Wochen vor dem eigentlichen Flohmarkttermin per E-mail an:
kramkrempelklimbim@gmail.com

-------

Falls das Wetter nicht mitflowt oder es unmenschlich heiß ist werden wir den Flohmarkt leider absagen.

♥ Stay tuned!