---
id: "1427537384714610"
title: Flowmarkt Kram, Krempel & Klimbim im Mai!
start: 2023-05-14 11:00
end: 2023-05-14 18:00
locationName: heiter bis wolkig
address: Röckener Straße 44, Leipzig
link: https://www.facebook.com/events/1427537384714610/
image: 342788697_555231033316385_6206164836909221079_n.jpg
isCrawled: true
---
Hereinspaziert, Hereinspaziert!

Nach der langen Winterpause geht es nun munter und fröhlich weiter. Nach den gefühlt ewig grauen Monaten wird es Zeit mit zauberhaften Menschen das sonnige & unbeschwerte Leben zu genießen. ♥

Seid wieder dabei, wenn sich der wunderschöne Garten des heiter bis wolkig in eine Fundgrube für alte und neue Schätze verwandelt.

Beim bunten Markttreiben zwischen Stadt und Wildnis gibt es Kunst, Klamotten und allerhand Kram, Krempel & Klimbim zu entdecken.

Hier nimmt garantiert jeder sein neues ♥ – Teil mit nach Hause!
Weil Schätze suchen hungrig macht, gibt es leckeres Essen & Getränke an der Bar.

Es erwarten Euch außerdem wilde Spiele für Groß und Klein, Musik & jede Menge tolle Menschen aus der Nachbarschaft.

♥ Der Eintritt ist frei!

Wir freuen uns auf Euch!

Auch dieses Jahr werden wir wieder einen Teil der Einnahmen spenden. ♥ Ein Drittel der Einnahmen werden an das heiter bis wolkig gespendet. 

-------

Falls das Wetter nicht mitflowt, müssen wir den Flohmarkt leider absagen.

♥ Stay tuned!