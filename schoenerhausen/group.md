---
name: SchönerHausen
website: https://schoenerhausen.org
email: info@schoenerhausen.org
scrape:
  source: facebook
  options:
    page_id: SchoenerHausenLE
---
SchönerHausen ist ein selbstverwaltetes und solidarisches Wohnprojekt von Menschen im Leipziger Osten.

Wir sind eine Gruppe von rund 80 Menschen zwischen 0 und 63 Jahren, die sich vorgenommen hat, selbstorganisiert und gemeinschaftlich vier Häuser am Ende der Eisenbahnstraße in Leipzig zu sanieren, um in ihnen solidarisch zusammenzuleben. 