---
name: KoLa Leipzig
website: https://kolaleipzig.de
email: info@kolaleipzig.de
scrape:
  source: facebook
  options:
    page_id: KoLaLeipzig
---
