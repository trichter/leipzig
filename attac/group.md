---
name: attac Leipzig
website: http://www.attac-leipzig.de/
email: leipzig@attac.de
scrape:
  source: facebook
  options:
    page_id: leipzigattac
---
attac steht für eine ökologische und solidarische Weltwirtschaftsordnung, mit international gleichberechtigter Zusammenarbeit und einer nachhaltigen, umweltgerechten Entwicklung weltweit.
Wir wollen eine Welt mit selbstbestimmter Demokratie, kultureller Vielfalt und Menschenrechten für alle.