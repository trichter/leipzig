---
id: "6348526135169190"
title: Dialektik bei Marx - Wochenendseminar [AUSGEBUCHT!]
start: 2023-04-15 11:00
end: 2023-04-16 17:00
locationName: translib
address: Goetzstraße 7, Leipzig
link: https://www.facebook.com/events/6348526135169190/
image: 330316562_3462737413955504_2826554327383014013_n.jpg
isCrawled: true
---
Von März bis Juni 2023 veranstalten wir drei Wochenendseminare zum Problem der Dialektik bei Hegel, Marx und Adorno.
Durch das Dickicht der Theorie führt Franz, der nach längerer Beschäftigung mit dem Gegenstand eine Idee hat, wie man ihn verständlich machen könnte. Inhalte und Struktur des Seminars entstammen seiner bald erscheinenden Dissertation.
Wir bitten um möglichst frühe Anmeldung unter dialektik@posteo.de

Die Seminare können auch einzeln besucht werden.

***

Dialektik – kaum ein Begriff ist umstrittener, unklarer und schwerer zu verstehen. Zugleich ist eine kritische Theorie der Gesellschaft ohne ihn undenkbar. Deshalb wollen wir uns den Begriff im Verlauf von drei Wochenendseminaren aneignen. Wir möchten gemeinsam den revolutionären Gehalt dialektischen Denkens herausarbeiten. Dabei vergegenwärtigen wir uns die Warnungen der kritischen Theorie vor der Dialektik als Weltanschauung sowie die geschichtlichen Lehren von Arbeiterbewegung und Faschismus.
In der Bestimmung dessen, was Dialektik ist, stehen sich zwei Seiten gegenüber. Auf der einen ist dabei der Pol eines Denkens, das sich unerklärlich, bisweilen fast mystisch an die Gegenstände anschmiegen soll. Ein Denken, dass sich einer festen Bestimmung entzieht. Auf der anderen Seite steht der Pol der formalisierten Dialektikauffassung des Marxismus-Leninismus. Dort wurde Dialektik als ein Werkzeug betrachtet, das angesichts der chaotischen Wirklichkeit eine ordnende Funktion besitzt.
Dazu kommt die dunkle Ahnung, dass es ohne eine intime Kenntnis von Hegel und Marx, am besten noch eines Großteils der Philosophiegeschichte, unmöglich zu sein scheint, zu verstehen, was Dialektik ist.
Durch die drei Seminare soll an die Stelle dieser Überforderung die Rekonstruktion eines Problemzusammenhanges rücken, der zumindest erahnen lässt, was es mit dialektischem Denken auf sich hat.

***

Eine Veranstaltung der translib in Kooperation mit der RLS Sachsen.

Die Idee, die versteinerten Verhältnisse zum Tanzen zu bringen, indem man ihnen die eigene Melodie vorspielt, findet sich schon bei Hegel als Bestimmungsmoment dialektischen
Denkens. 
Der wesentliche Unterschied zwischen Hegel und Marx besteht nun darin, dass bei Hegel diese Melodie das Denken, die Wirklichkeit Formbestimmungen des Denkens sind. Bei Marx ist die Melodie die gesellschaftliche Praxis der Menschen und die Formbestimmungen daher Praxisformen.
Dieser Problemstellung und Veränderung dialektischen Denkens wird sich in diesem zweiten Seminar zu Marx gewidmet. Die Erkenntnis, die in der Reflexion der Kategorien liegt, ist nun
nicht mehr die Erfahrung des in den Gegenständen den Menschen entfremdeten Bewusstseins. Vielmehr scheint auf, dass die Kategorien der politischen Ökonomie Ausdruck
der entfremdeten Arbeit sind und damit nicht ewig gültig, sondern durch die Menschen geschaffen, ihr eigenes gesellschaftliches Produkt. In der Herrschaft der Abstraktionen wie Ware, Geld und Kapital verbirgt sich die Herrschaft des Menschen über den Menschen. 
Die Marxsche Dialektik steht damit im Widerspruch zur Aufklärung: Nicht das Denken ist es, das die menschliche Gesellschaft substantiell bestimmt, sondern der Stoffwechsel mit der Natur,
die gesellschaftlich geteilte Arbeit. Daher ist der bürgerlichen Gesellschaft auch nicht durch Aufklärung beizukommen, sondern die uns beherrschenden Kategorien wie Ware, Geld und Kapital verschwinden erst, wenn ihre Grundlage transformiert wird: die gesellschaftliche
Praxis der Menschen.

***

Bitte vermerkt jeweils im Betreff eurer Anmeldung, an welchem/n Seminar/en ihr teilnehmen wollt.

Die Teilnehmer*innen-Zahl ist pro Seminar auf 20 Personen beschränkt.

Nach Anmeldung bekommt ihr einen Reader für das Seminar eurer Wahl zugeschickt.

Wir freuen uns sehr über alle Anmeldungen und noch ein kleines bisschen mehr über Anmeldungen von Frauen und LGBTQ.

Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.