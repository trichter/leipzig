---
id: "493294506214739"
title: 'Buch-Release & Lesung: "Seuchenjahre" von Maximilian Hauer'
start: 2023-04-29 18:30
locationName: translib
address: Goetzstraße 7, Leipzig
link: https://www.facebook.com/events/493294506214739/
image: 340836212_609227184437696_8741970387235613429_n.jpg
isCrawled: true
---
Im Frühjahr 2020 brach SARS-CoV-2 in den Alltag ein und begann in der Lebenswelt zu zirkulieren. Vertrautes wurde unheimlich, individuelle Routinen und soziale Regeln schlagartig außer Kraft gesetzt. Doch die Pandemie irritiert nicht nur den gewohnten leiblichen Umgang mit anderen Menschen, sondern stellt auch die Ordnung zentraler Kategorien wie Staat und Individuum, Politik und Ökonomie, Natur und Gesellschaft in Frage.

Unser Mitglied Maximilian Hauer hat in den drei zurückliegenden Seuchenjahren zu verschiedenen Aspekten der Pandemie geschrieben und veröffentlicht nun im Mandelbaum Verlag einen Essayband zur Pandemie. Darin kritisiert er verbreitete ideologische Verarbeitungsformen des Geschehens, spürt pandemischen Stimmungslagen nach und formuliert eine materialistische Analyse der Seuche sowie ihrer staatlichen Bewältigungsversuche.

Das Buch wird an dem Abend erstmals öffentlich vorgestellt.  

***

Vollständiger Titel: Maximilian Hauer: Seuchenjahre. Orientierungsversuche im
Ausnahmezustand. Wien: Mandelbaum Verlag 2023.