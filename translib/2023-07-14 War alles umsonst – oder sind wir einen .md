---
id: "6972531076094344"
title: War alles umsonst – oder sind wir einen Schritt weiter? Die Kampagne
  Deutsche Wohnen & Co Enteignen
start: 2023-07-14 19:00
locationName: translib
address: Goetzstraße 7, Leipzig
link: https://www.facebook.com/events/6972531076094344/
image: 347420952_642077214616880_6966798106212280117_n.jpg
isCrawled: true
---
Die Kampagne Deutschen Wohnen & Co Enteignen hat geschafft, was vielen vorher kaum machbar schien: Beim durch sie initiierten Volksentscheid im September 2021 über die Enteignung und Vergesellschaftung privater Wohnungsunternehmen in Berlin stimmten über 57% der Wähler*innen für die Enteignung. Damit wurde der Berliner Senat dazu aufgefordert, alle Maßnahmen einzuleiten, die zur Überführung von Immobilien sowie Grund und Boden in Gemeineigentum zum Zwecke der Vergesellschaftung nach Art. 15 GG erforderlich sind. Passiert ist seither nicht viel – die Politik hat offensichtlich nicht die Absicht, den Volksentscheid tatsächlich umzusetzen. Der Kampagne droht währenddessen der Atem auszugehen. Die weit verbreitete Überzeugung, dass an den Verhältnissen nicht zu rütteln sei, droht darin erneut Bestätigung zu finden. Um dem entgegen zu wirken, ist es notwendig, die politische Praxis zu reflektieren und aus ihr zu lernen. Welches vorläufige Fazit ist also aus der Kampagne zu ziehen – und wie könnte und sollte zukünftig agiert werden?

Wir haben zwei DWE-Aktivist*innen eingeladen, um mit ihnen über Stand, Zukunft und politische Einschätzung der Kampagne ins Gespräch zu kommen. Worum geht es der Kampagne? Wie haben sie gearbeitet und wie versuchen sie aktuell der Verschleppungstaktik des Berliner Senats zu trotzen? Auf welche Grenzen stößt die Forderung nach Enteignung? Muss die Kampagne als gescheitert betrachtet werden? Zuletzt wollen wir auch auf die Frage zu sprechen kommen, was unter den gegenwärtigen Bedingungen Ansatzpunkte wären, um wieder politische Handlungsfähigkeit zu entwickeln – nicht nur, aber auch aus kommunistischer Perspektive. 

In der Diskussion erörtern wir gemeinsam, welche Bedeutung die Kampagne über Berlin hinaus, v.a. für linke Stadtpolitik in Leipzig hat.