---
id: "1237262263859981"
title: Dialektik bei Adorno - Wochenendseminar [AUSGEBUCHT !]
start: 2023-06-24 11:00
end: 2023-06-25 17:00
locationName: translib
address: Goetzstraße 7, Leipzig
link: https://www.facebook.com/events/1237262263859981/
image: 330812011_3429104677379163_2062459630952202998_n.jpg
isCrawled: true
---
Von März bis Juni 2023 veranstalten wir drei Wochenendseminare zum Problem der Dialektik bei Hegel, Marx und Adorno.

Durch das Dickicht der Theorie führt Franz, der nach längerer Beschäftigung mit dem Gegenstand eine Idee hat, wie man ihn verständlich machen könnte.
Inhalte und Struktur des Seminars entstammen seiner bald erscheinenden Dissertation.

Wir bitten um möglichst frühe Anmeldung unter dialektik@posteo.de

Die Seminare können auch einzeln besucht werden.

***

Dialektik – kaum ein Begriff ist umstrittener, unklarer und schwerer zu verstehen. Zugleich ist eine kritische Theorie der Gesellschaft ohne ihn undenkbar. Deshalb wollen wir uns den Begriff im Verlauf von drei Wochenendseminaren aneignen. Wir möchten gemeinsam den revolutionären Gehalt dialektischen Denkens herausarbeiten. Dabei vergegenwärtigen wir uns die Warnungen der kritischen Theorie vor der Dialektik als Weltanschauung sowie die geschichtlichen Lehren von Arbeiterbewegung und Faschismus.
In der Bestimmung dessen, was Dialektik ist, stehen sich zwei Seiten gegenüber. Auf der einen ist dabei der Pol eines Denkens, das sich unerklärlich, bisweilen fast mystisch an die Gegenstände anschmiegen soll. Ein Denken, dass sich einer festen Bestimmung entzieht. Auf der anderen Seite steht der Pol der formalisierten Dialektikauffassung des Marxismus-Leninismus. Dort wurde Dialektik als ein Werkzeug betrachtet, das angesichts der chaotischen Wirklichkeit eine ordnende Funktion besitzt.
Dazu kommt die dunkle Ahnung, dass es ohne eine intime Kenntnis von Hegel und Marx, am besten noch eines Großteils der Philosophiegeschichte, unmöglich zu sein scheint, zu verstehen, was Dialektik ist.
Durch die drei Seminare soll an die Stelle dieser Überforderung die Rekonstruktion eines Problemzusammenhanges rücken, der zumindest erahnen lässt, was es mit dialektischem Denken auf sich hat.

***

Eine Veranstaltung der translib in Kooperation mit der RLS Sachsen.

Dieses Seminar ist der negativen Dialektik Adornos gewidmet. Ausgehend von den
Schrecken des Faschismus und den historischen Niederlagen der Arbeiterbewegung, rekonstruierte er die Eckpunkte, die Dialektik als revolutionäre Theorie denkbar werden lassen.
Die Organisation der chaotischen Mannigfaltigkeit der Wirklichkeit durch das wissenschaftliche System, die Ableitung der Kategorien aus der ihnen zugrundeliegenden Substanz wird Adorno verdächtig. Schon in der Form des begrifflichen Denkens selbst, als
Zusammenfassung des Besonderen und Einzelnem unter dem Allgemeinen, liegt ihm zufolge ein Moment von Herrschaft. Jedes Einzelne wird in dem Moment der sprachlichen Bezeichnung durch den Begriff zu einem Allgemeinen und dadurch unterliegt es als bloßes
Exemplar der Herrschaft des Allgemeinen, politisch gesprochen, der Gesellschaft. Dieser Abstraktionsvorgang, der das Einzelne und Allgemeine zusammenschließt, vollzieht sich
dabei nicht nur im Kopf der Menschen, sondern auch in der Gesellschaft selbst, im Warentausch.
Wenn Dialektik die Theorie der Befreiung, die Algebra der Revolution zu sein beansprucht, dann nur, wenn sie das Einzelne, das Ungleiche und Eigensinnige, also das Nichtidentische
bereits in der Theorie bewahrt. Dies meint, mit dem Begriff gegen den Begriff zu denken. So schließt das dritte Seminar mit einer Selbstkritik der Dialektik.

***

Bitte vermerkt jeweils im Betreff eurer Anmeldung, an welchem/n Seminar/en ihr teilnehmen wollt.

Die Teilnehmer*innen-Zahl ist pro Seminar auf 20 Personen beschränkt.

Nach Anmeldung bekommt ihr einen Reader für das Seminar eurer Wahl zugeschickt.

Wir freuen uns sehr über alle Anmeldungen und noch ein kleines bisschen mehr über Anmeldungen von Frauen und LGBTQ.

Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.