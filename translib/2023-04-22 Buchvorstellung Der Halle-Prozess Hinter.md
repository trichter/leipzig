---
id: "670061378226205"
title: 'Buchvorstellung: "Der Halle-Prozess: Hintergründe und Perspektiven"'
start: 2023-04-22 18:00
locationName: translib
address: Goetzstraße 7, Leipzig
link: https://www.facebook.com/events/670061378226205/
image: 335885994_944393933667905_4498981326961867794_n.jpg
isCrawled: true
---
Buchvorstellung: „Der Halle-Prozess: Hintergründe und Perspektiven“

Am 9. Oktober 2019, dem jüdischen Feiertag Jom Kippur, griff ein Rechtsterrorist erst die Synagoge, dann den nahegelegenen Imbiss Kiez Döner in Halle an. Im Laufe seines Anschlags ermordete er zwei Menschen und verletzte und traumatisierte viele weitere in Halle und Wiedersdorf. Der Gerichtsprozess gegen den Täter im Jahr 2020 wurde durch das Buch „Der Halle-Prozess: Mitschriften“ umfänglich dokumentiert. 
Der Folgeband „Der Halle-Prozess: Hintergründe und Perspektiven“ beleuchtet Aspekte und Hintergründe der Tat und des juristischen Verfahrens. Die Autor_innen fragen nach politischen und gesellschaftlichen Konsequenzen und möglichen Strategien gegen antisemitische, rassistische und frauenfeindliche Gewalt. 

Die Mitherausgeberin Christina Brinkmann gewährt an diesem Abend zunächst gesprächsweise Einblicke in das Buch und den Arbeitsprozess der Herausgeber_innen. 

Anschließend stellt Matthias Peter Lorenz seinen Aufsatz „Killing in the name of. Rechtsterroristische Namenspolitiken und antifaschistische Entnennung“ zur Diskussion. In seinem Beitrag zum Sammelband setzt er sich mit der Forderung Betroffener und Überlebender des Anschlags auseinander, den Namen des Täters nicht zu nennen. Die Nichtnennung des Täternamens interpretiert der Referent unter anderem als feministisches Durchbrechen einer patriarchalen Idee von Weitergabe.  

Nach der Veranstaltung bleibt die Bar geöffnet.

***

Christina Brinkmann ist Kunstwissenschaftlerin und hat gemeinsam mit Valentin Hacken am Podcast „Halle nach dem Anschlag“ für Radio Corax gearbeitet.

Matthias Peter Lorenz arbeitet an den Schnittstellen von Philosophie und politischer Theorie zu Fragen der Handlungsmacht sozialer und politischer Bewegungen. Beruflich wie aktivistisch setzt er sich seit vielen Jahren mit rechtsterroristischer Gewalt und ihren Wirkungsweisen auseinander, unter anderem hat er im Münchner Bündnis gegen Naziterror und Rassismus zu den Morden des NSU und dem Münchner NSU-Prozess gearbeitet.

Moderation: Maximilian Hauer (translib)

*** 

Eine Veranstaltung von translib und der Buchhandlung drift

***

Vollständiger Buchtitel:
Christina Brinkmann, Nils Krüger, Jakob Schreiter (Hgg.), Der Halle-Prozess: Hintergründe und Perspektiven, Leipzig 2022: Spector Books.