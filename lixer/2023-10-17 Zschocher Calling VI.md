---
id: 2023-9-17-zschocher_Calling
title: Zschocher Calling VI
start: 2023-10-17 14:00
end: 2023-10-17 19:00
address: Pörstenerstr. 9
isCrawled: true
---
Das jährliche Stadtteilfest des Lixer e.V.  geht nunmehr in die sechste Runde! Dies wollen wir am 17.09 mit euch feiern. los geht’s um 14 Uhr im Schwartzepark mit Live Acts wie Gründe gegen KindA, Nachtlichter und Kollege Hartmann. Für das leibliche Wohl wird wie immer gesorgt. Obendrauf gibt’s natürlich die neusten Info’s aus der Hood.

Ihr wollt mitmachen? Einen Redebeitrag zur aktuellen Themen halten im Viertel? einen Stand machen um euren Verein/Ini/Gruppe zu präsentieren? Oder einfach an dem Tag mit Nerven und fleißigen Händen zur Hand gehen ? meldet euch gerne unter lixer@riseup.net wir freuen uns auf euch

Zschocher bleibt bunt!