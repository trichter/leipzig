---
id: 2023-4-27-buchvorstellung
title: Buchvorstellung
start: 2023-05-27 19:00
end: 2023-05-27 22:00
address: Pörstenerstr. 9
isCrawled: true
---
Michael Lauter
Buchvorstellung und Gespräch
Von einem der im Zuchthaus Mathematik studierte.

Prof. Hans Lauter (1914 – 2012) war ein antifaschistischer Widerstandskämpfer, SED-Funktionär, Mitglied der PDS und zusammen mit Esther Bejerano Ehrenvorsitzender des VVN – BdA. 1933 wurde er kurzzeitig im Hansahaus in Chemnitz interniert, nach seiner Entlassung war er illegal im Kommunistischen Jugendverband Deutschland in Chemnitz, später in Leipzig aktiv. Für seine Tätigkeit wurde er 1936 von der nationalsozialistischen Justiz wegen Hochverrats zu 10 Jahren Zuchthaus verurteilt. Seine Strafe verbüßte er im Zuchthaus Waldheim und in den berüchtigten Moorlagern im Emsland, vom (über)Leben in diesen Lagern erzählt das bekannte Häftlingslied „Die Moorsoldaten“. Seine letzten Lebensjahre verbrachte er in Leipzig-Grünau. Die Geschichten, die Hans Lauter dort aus seinem Leben erzählt, dokumentierte Michael Lauter in dem Buch „Von einem der im Zuchthaus Mathematik studierte“.
Veranstalter*innen: Lixer, Antifa Tresen, Verlag Osiris-Druck, Rosa Luxemburg Stiftung Sachsen