---
id: 2023-12-16-adventindenhöfen
title: Advent in den Höfen
start: 1970-01-01 01:00
address: Pörstenerstr. 9
isCrawled: true
---
Advent in den Höfen Kleinzschochers – 16. 12. 2023

Dieses Jahr auch im Lixer e.v. 

Kommt ab 14:00 zum weihnachtlichen Basteln vorbei! 

Von 16:00 bis 17:30 werden Weihnachtsgeschichten für Kinder gelesen. 

Auch sonst ist in Zschocher viel los das vollständige Programm findet ihr [hier](https://bi-kleinzschocher.blogspot.com/2023/11/advent-in-den-hofen-kleinzschochers-16.html)