---
id: 2023-10-15-stadtteilrundgang
title: Stadtteilrundgang
start: 2023-11-15 13:00
end: 2023-11-15 16:30
address: Pörstenerstr. 9
isCrawled: true
---
Widerstand und Verfolgung im Leipziger Arbeiter*innenviertel Kleinzschocher

Eine Veranstaltung des Lixer e.V.s in Kooperation mit der Rosa-Luxemburg-Stiftung Sachsen

Treffpunkt: 

Gedenkstein für das "Internationale Antifaschistische Komitee"
Nikolai-Rumjanzew-Strasse 2
04207 Leipzig

Mit dem Einsetzen der Industrialisierung entwickelte sich das Dorf Kleinzschocher in rasender Geschwindigkeit zu einem quirligen Arbeiter*innenviertel. Das Leben der Menschen in Zschocher war geprägt durch Enge, Armut und, wenn vorhanden, eintönige und harte Arbeit in den umliegenden Fabriken. Trotzdem oder genau deswegen, gab es einen hohen Grad an politischer Organisierung unter den Bewohner*innen. Ob im Arbeitersport-Verein, bei politischen Diskussionen in der Jugendgruppe oder im Betrieb. Kleinzschocher war ein stark sozialdemokratisch und kommunistisch geprägter Stadtteil. Mit der Machtübergabe an die Nationalsozialisten setzte eine Welle von Terror und Verfolgung ein. Die Strukturen der Arbeiter*innenbewegung wurden zerschlagen und großflächige Razzien fanden statt, denen viele politisch aktive Arbeiter*innen zum Opfer fielen. Die einzelnen Stationen widmen sich den mutigen Widerstandsversuchen der Bewohner*innen des Stadtteils und erzählen ihre Geschichte. Als Abschluss des Rundgangs zeigt die Geschichtswerkstatt »leipzig|unten« ihre Ausstellung zu Albert Hennig, einem Arbeiterfotografen aus Kleinzschocher, und gibt einen kurzen Einblick in sein bewegtes Leben.

Der Rundgang mit Ausstellungsbesuch dauert ca. 3,5 Stunden.

 Mit Manuel Wagner (AG Zschocher-History, Lixer e.V.), Alexander Rode und  Janika Wersing (Geschichtswerkstatt »leipzig|unten«) und Daniela Schmohl (Historikerin, RLS Sachsen)