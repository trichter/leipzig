---
id: 2023-4-27-michaellauter
title: Michael Lauter
start: 2023-05-27 19:00
end: 2023-05-27 22:00
address: Pörstenerstr. 9
isCrawled: true
---
Buchvorstellung und Gespräch
"Von einem, der im Zuchthaus Mathematik studierte"