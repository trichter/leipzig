---
id: 2023-9-9-photo_2023-09-07_10-36-54
title: Küfa
start: 2023-10-09 19:00
end: 2023-10-09 21:00
address: Pörstenerstr. 9
isCrawled: true
---
Am Sonntag gibt es  vietnamesische Sommerrollen mit Salat und Erdnusssauce (vegan). Da alle Zutaten bei der Zubereitung getrennt bleiben, ist die KüfA auch für Menschen mit Unverträglichkeiten geeignet. Wir sammeln Geld für das widerständige Dorf Pödelwitz im Süden Leipzigs.