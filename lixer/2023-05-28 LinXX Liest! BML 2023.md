---
id: 2023-4-28-Buchmesse_Plakat_A3_split_2_Page_1_znCBfht
title: LinXX Liest! BML 2023
start: 2023-05-28 19:00
end: 2023-05-28 22:00
address: Pörstenerstr. 9
isCrawled: true
---
Lucius Teidelbaum

Buchvorstellung und Gespräch

Corona-Proteste von rechts – Vom Querdenken zur Querfront?
Was im April 2020 als diffuse Bewegung gegen die Maßnahmen anfing, wurde schnell zur rechtsoffenen Bewegung mit verschwörungsideologischen Unterbau und zum Marktplatz der alternativen Fakten. Im Buch wird die Entwicklung nachgezeichnet, sowie Inhalte der Bewegung und ihre Gefahren aufgezeigt.

 Moderation Steven Hummel (RLS Sachsen/chronik.LE)

Die Veranstaltung wird [auf dem YouTube Kanal](www.youtube.com/@lixere.v.7454/streams) des Lixer e.V. online übertragen und ist später noch abrufbar. 

Veranstalter*innen: Lixer, Unrast-Verlag, Antifa Tresen, Rosa Luxemburg Stiftung

Die Veranstaltenden behalten sich vor, von ihrem Hausrecht Gebrauch zu machen und Personen, die rechtsextremen Parteien oder Organisationen angehören, der rechtsextremen Szene
zuzuordnen sind oder bereits in der Vergangenheit durch rassistische, nationalistische,
antisemitische oder sonstige menschenverachtende Äußerungen in Erscheinung getreten
sind, den Zutritt zur Veranstaltung zu verwehren oder von dieser auszuschließen.