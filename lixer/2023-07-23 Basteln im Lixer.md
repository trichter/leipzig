---
id: 2023-6-23-basteln_291N0Cw
title: Basteln im Lixer
start: 2023-07-23 15:00
end: 2023-07-23 15:00
address: Pörstenerstr. 9
isCrawled: true
---
Wir treffen uns einmal im Monat zum Basteln. Ob Groß oder Klein - Alle sind gern gesehen! Ich habe einige Ideen, aber bringt auch gerne selbst welche mit. Eure Sabine.