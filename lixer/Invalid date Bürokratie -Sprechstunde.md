---
id: 2023-12-12-Büro_passend
title: Bürokratie -Sprechstunde
start: 1970-01-01 01:00
address: Pörstenerstr. 9
isCrawled: true
---
Ab Oktober zweiwöchentlich!

Brauchst du Unterstützung beim Ausfüllen von Anträgen? Oder Hilfe beim Formulieren von Ämterschreiben und Widersprüchen? Wir bieten in unserer Bürokratie-Sprechstunde Orientierung im Umgang mit Ämtern sowie dem allgemeinen Bürokratiedschungel. Wir vermitteln gern an Stellen, die sich auf dein Anliegen spezialisiert haben.

Die Bürokratie-Sprechstunde umfasst zum Beispiel Hilfen zu den Themen Jobcenter, Wohnthemen (z.B. Wohngeld, Streitigkeiten mit Vermieter*innen etc.), Kranken-, Renten- und Pflegekasse und einiges mehr.