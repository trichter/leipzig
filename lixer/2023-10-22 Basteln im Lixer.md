---
id: 2023-9-22-basteln_291N0Cw
title: Basteln im Lixer
start: 2023-10-22 15:30
end: 2023-10-22 18:00
address: Pörstenerstr. 9
isCrawled: true
---
Wir treffen uns einmal im Monat zum Basteln. Ob Groß oder Klein - Alle sind gern gesehen! Ich habe einige Ideen, aber bringt auch gerne selbst welche mit. Eure Sabine.