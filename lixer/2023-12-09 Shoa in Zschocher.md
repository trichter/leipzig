---
id: 2023-11-9-signal-2023-10-27-180832_004
title: Shoa in Zschocher
start: 2023-12-09 19:30
end: 2023-12-09 21:30
address: Pörstenerstr. 9
isCrawled: true
---
Am 09.11. 2023 erinnern wir an die Bewohner:innen von Zschocher die während des NS als Juden verfolgt und teils ermordet wurden. Seit 2021 forscht die AG Zschocher History zur Geschichte des Stadtteils während der Weimarer Republik und dem Nationalsozialismus. 

[Hier geht es zum Livestream](https://youtube.com/live/pY88ibd8ZW8?feature=share)

Zum Jahrestag des Novemberpogroms von 1938 wird der aktuelle Stand dieser Recherchen in einem kurzen Vortrag vorgestellt. Im Rahmen des Projekts „Geschichte für alle – Zschocher History online“ entstehen verschieden Audioguides die durch die jüngste Geschichte Zschochers führen und eine Homepage auf der die Biographien von Verfolgten nachvollziehbar gemacht werden. 

Danach sprechen wir mit Angehörigen von rassisch und politisch Verfolgten Personen aus Zschocher:

Jan Deelstras Familiengeschichte ist eng mit Kleinzschocher verknüpft, sein Vater war als Zwangsarbeiter in der Nähe der Meyerschen Häuser eingesetzt. Dort lernte er die Ursula Grothe kennen. Gemeinsam versuchten die beiden die Not der Zwangsarbeitenden in ihrer Umgebung zu lindern. Ursula Grothe kam aus einer kommunistisch Geprägten Familie aus „dem roten Meyersdorf“, sie war an Widerstandshandlungen beteiligt und unterstütze die von den Nazis als jüdisch verfolgte Familie Weißmann. 

Florentine Ziemann erzählt über das Leben ihrer Mutter Ria Hornig, die im Kinderheim Jedija aufwuchs. Das Heim wurde von den Schwestern Petereit betrieben und war von 1933 bis zur Schließung im Jahr 1941 in der Tauchnitz Villa in der Windorferstraße ansässig. Da Ria Hornigs Vater Juden war, wurde sie von den Nazis als sogenannte Halbjüdin eingestuft. Durch die Unterstützung der Geschwister Petereit und engen Freunden wurde sie vor der Willkür der SS und Gestapo bewahrt. 

Moderation: Manuel Wagner

Die Vortragsreihe findet im Rahmen des Projekts "Geschichte für Alle - Erinnerungskultur in Zschocher Online" statt. Diese Vortragsreihe wird gefördert vom Bundesministerium für Familie, Senioren, Frauen und Jugend im Rahmen des Bundesprogramm Demokratie leben sowie vom Freistaat Sachsen und dem Landespräventionsrat Sachsen. Diese Maßnahme wird mitfinanziert mit Steuermitteln auf Grundlage des von den Abgeordneten des Sächsischen Landtags beschlossenen Haushaltes.