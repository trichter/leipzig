---
id: 2023-6-28-photo_2023-06-15_20-45-57
title: Stadtteilrundgang
start: 2023-07-28 18:00
end: 2023-07-28 21:00
address: Pörstenerstr. 9
isCrawled: true
---
Stadtteilrundgang des Lixer e.V. und  DieLINKE Südwest 

Widerstand und Verfolgung während des Nationalsozialismus in Kleinzschocher

Beginn 18:00 Uhr Treffpunkt: Rewe Am Adler

In Zschocher gab es bis zu 170 Menschen die durch die Nazis als jüdische verfolgt wurden. Ein Großteil von ihnen wurde in den Lagern der Nazis systematisch ermordet, nur  wenigen gelang es zu überleben oder  frühzeitig das Land zu verlassen.  Auch versuchter Widerstand gegen das Nazi-Regime wurde brutal verfolgt. Auf unserem Rundgang besuchen wir Ort der Verfolgung und des Widerstands in Kleinzschocher  und beschäftigen uns mit  den  Biographien von Opfer des NS-Regimes  aus Kleinzschocher. Der Rundgang dauert circa 1,5 Stunden.  Nach Ende des Rundgangs lassen wir den Abend im Stadtteilladen Lixer ausklingen.