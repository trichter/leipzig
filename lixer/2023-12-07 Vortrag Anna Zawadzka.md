---
id: 2023-11-7-signal-2023-10-27-180832_002
title: "Vortrag: Anna Zawadzka"
start: 2023-12-07 19:30
end: 2023-12-07 21:30
address: Pörstenerstr. 9
isCrawled: true
---
Am 7. November laden wir zu einem Vortrag von Anna Zawadzka über Antisemitismus im heutigen Polen ein.

Wer nicht vor Ort sein kann findet [hier den Livestream auf Youtube](https://youtube.com/live/TbzWYNVvRwo?feature=share)

In ihrem 2023 veröffentlichten Buch "Mehr als Stereotyp: 'Judeobolschewismus' als polnische Kulturvorlage" analysiert Zawadzka den Stereotyp des Judäobolschewismus, der in Polen nach wie vor sehr lebendig ist und die Wahrnehmung von Juden und Jüdinnen in Polen stark beeinflusst. Die Autorin sammelt Beispiele aus der Akademie, Architektur, Geschichte, Politik, Straße und Kulturinstitutionen, um zu zeigen, wie Antisemitismus in heutigem Polen wirkt und funktioniert. Mit ihren Untersuchungen und Beispielen belegt und dokumentiert sie besorgniserregende Tendenzen weitverbreiteten Antisemitismus in Polen, der eine unsichtbare Bindung darstellt, die alle Polen und Polinnen in einem zunehmenden Nationalismus verknüpft. 

Dr. Anna Zawadzka ist eine Soziologin. Sie arbeitet am Institut für Polnische Slawistik der Polnischen Akademie der Wissenschaften. Sie ist Mitglied des Zentrums für kulturelle und literarische Studien zum Kommunismus sowie des London Centre for the Study of Contemporary Antisemitism . Kürzlich veröffentlichte sie das Buch Więcej niż stereotyp. "Żydokomuna" jako wzór kultury polskiej ("Mehr als Stereotyp: Die 'Judäo-Bolschewismus' als polnisches Kulturmuster").

Moderation: Pawel Matusz


Der Vortrag findet in polnischer Sprache mit deutscher Übersetzung statt. Die Veranstaltung wird [hier live](https://youtube.com/live/TbzWYNVvRwo?feature=share)  übertragen und ist später auf dem Youtube-Kanal des Lixer e.V.s abrufbar.

Die Vortragsreihe findet im Rahmen des Projekts "Geschichte für Alle - Erinnerungskultur in Zschocher Online" statt. Diese Vortragsreihe wird gefördert vom Bundesministerium für Familie, Senioren, Frauen und Jugend im Rahmen des Bundesprogramm Demokratie leben sowie vom Freistaat Sachsen und dem Landespräventionsrat Sachsen. Diese Maßnahme wird mitfinanziert mit Steuermitteln auf Grundlage des von den Abgeordneten des Sächsischen Landtags beschlossenen Haushaltes.