---
id: 2023-4-30-Buchmesse_Plakat_A3_split_2_Page_1
title: LinXX Liest! BML 2023
start: 2023-05-30 17:00
end: 2023-05-30 20:00
address: Pörstenerstr. 9
isCrawled: true
---
Antonio Gramsci

Kulturelle Hegemonie & Stadtteilarbeit mit Uwe Hirschfeld

Uwe Hirschfeld hat zahlreiche Texte zu dem italienischen Schriftsteller und Philosophen Antonio Gramsci verfasst. Sein Konzept der Hegemonie, wird vor allem in linken Kreisen breit rezipiert. Nach einer kurzen Einführung zu den Grundzügen der Philosophie Gramscis, seinem Leben und Wirken, sprechen wir über die Möglichkeiten, wie seine Überlegungen in die aktuelle Praxis linker Stadtteilarbeit eingebunden und weitergedacht werden können.

Die Veranstaltung wird [auf dem YouTube Kanal](www.youtube.com/@lixere.v.7454/streams) des Lixer e.V. online übertragen und ist später noch abrufbar. 

Veranstalter*innen: Lixer, Rosa Luxemburg Stiftung

Die Veranstaltenden behalten sich vor, von ihrem Hausrecht Gebrauch zu machen und Personen, die rechtsextremen Parteien oder Organisationen angehören, der rechtsextremen Szene
zuzuordnen sind oder bereits in der Vergangenheit durch rassistische, nationalistische,
antisemitische oder sonstige menschenverachtende Äußerungen in Erscheinung getreten
sind, den Zutritt zur Veranstaltung zu verwehren oder von dieser auszuschließen.