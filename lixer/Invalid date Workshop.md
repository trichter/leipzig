---
id: 2023-12-5-Workshop_Zschocher_Connection
title: Workshop
start: 1970-01-01 01:00
address: Pörstenerstr. 9
isCrawled: true
---
Umgang mit rechten Störungen bei Veranstaltungen

Im Rahmen der Stadtteilvernetzung "ZschocherConnection" bieten wir einen Workshop zum Thema Sicherheit bei öffentlichen Veranstaltungen an.  Die Teilnahme ist kostenfrei.

Teilnahme nur mit Anmeldung unter : zschocherconnection@posteo.de

Im März dieses Jahres wird die antirassistische Autorin Sarah Vecera bei einer Lesung in der Taborkirche von einem Besucher angefeindet, ihre Lesung massiv gestört. Schlussendlich greift die Security ein. Wie können sich Veranstalter*innen auf solche und ähnliche Vorfälle vorbereiten? Wie kann ein Umgang in der konkreten Situation aussehen? Wie können entsprechende Vorfälle nachbearbeitet werden? Diesen und weiteren Fragen wollen wir uns in einem Workshop gemeinsam nähern. Eigene Erfahrungen und Beispiele können gerne mitgebracht werden.

Steven Hummel ist Politikwissenschaftler. Er arbeitet als Bildungsreferent bei der Rosa Luxemburg Stiftung Sachsen und ist ehrenamtlich bei chronik.LE aktiv. Sein Schwerpunktthema ist die extreme Rechte.

Die Veranstaltung des Lixer e.V. und des ProjektRaumKirche wird finanziert durch eine Förderung des Stadtbezirkbeirats Südwest.