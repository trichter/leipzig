---
id: 2023-4-28-buchvorstellung
title: Buchvorstellung
start: 2023-05-28 19:00
end: 2023-05-28 22:00
address: Pörstenerstr. 9
isCrawled: true
---
Lucius Teidelbaum
Buchvorstellung und Gespräch
Corona-Proteste von rechts – Vom Querdenken zur Querfront?
Was im April 2020 als diffuse Bewegung gegen die Maßnahmen anfing, wurde schnell zur rechtsoffenen Bewegung mit verschwörungsideologischen Unterbau und zum Marktplatz der alternativen Fakten. Im Buch wird die Entwicklung nachgezeichnet, sowie Inhalte der Bewegung und ihre Gefahren aufgezeigt. Moderation Steven Hummel (RLS Sachsen/chronik.LE)
Veranstalter*innen: Lixer, Unrast-Verlag, Antifa Tresen, Rosa Luxemburg Stiftung