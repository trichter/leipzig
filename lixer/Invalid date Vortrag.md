---
id: 2023-12-6-23_12_06_Ländliche_Erinnerungsarbeit_sLAG_1
title: Vortrag
start: 1970-01-01 01:00
address: Pörstenerstr. 9
isCrawled: true
---
Online Übertragung des Vortrag des VVN BdA: 

Ländliche Erinnerungsarbeit im generationellen Wandel - 

Potentiale und Realitäten kleiner NS-Gedenkstätten in strukturell benachteiligten Regionen 

Mit Lea Fischer (Regionalentwicklung (M.Sc.), emanzipatorische Prozesse im Fokus), Moderation: Daniela Schmohl (RLS Sachsen)

Eine Veranstaltung der sächsischen Landesarbeitsgemeinschaft Auseinandersetzung mit dem NS (sLAG) in Kooperation mit der RLS Sachsen und der VVN-BdA Leipzig

Livestream über den Youtube-Kanal der sLAG.

 Im Vortrag werden die Ergebnisse und Schlussfolgerungen einer Studie vorgestellt, die Erinnerungsarbeit an kleinen NS-Gedenkstätten in ländlichen Räumen in den Fokus nimmt. Zentrale Themen sind der generationelle Wandel in den Strukturen, gesellschaftlich-regionale Potentiale sowie spezifische Herausforderungen der strukturell benachteiligten Gedenkorte. Es soll außerdem darum gehen, was politische Entscheidungsträger*innen oder Bildungsorganisationen zur Unterstützung der Prozesse von ländlichen NS-Gedenkstätten tun können. Abschließend wird es ein Gespräch zum Vortrag geben.

In Leipzig besteht die Möglichkeit den Stream gemeinsam zu schauen Beginn: 17:30 Uhr im Lixer e.V. in der Pörstener Str. 9 in 04229 Leipzig.