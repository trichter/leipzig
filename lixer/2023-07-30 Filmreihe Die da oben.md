---
id: 2023-6-30-logo
title: Filmreihe "Die da oben"
start: 2023-07-30 20:00
end: 2023-07-30 22:00
address: Pörstenerstr. 9
isCrawled: true
---
Im Rahmen des Kultursommers 2023 in Kleinzschocher zeigen wir Filme des Medienprojekt Wuppertal

Am 30.06. zeigen wir  Kurzfilme zum Thema Verschwörungsideologien

Kein passendes Gegenargument? 
Ein Interviewfilm über Verschwörungserzählungen, Vertrauen in Medien, Fake-News und Meinungsbildung

In Interviews beschreiben Jugendliche ihre Berührungspunkte mit Verschwörungserzählungen, reflektieren ihren Umgang mit Medien und erklären, wie sich ihre Meinung zusammensetzt. 20 Min.

Fake-News

Marcello Orlik des Anti-Fake-News-Blogs Volksverpetzer stellt die Tätigkeit des Blogs vor. In dem Interviewfilm erläutert er Falschmeldungen, die Verbreitung von Fake-News und spricht über seriösen Journalismus. 20 Min.

Verschwörungsideologien 

Interviewfilm mit Expert*innen über die Entstehung und den Umgang mit Verschwörungsideologien
mit Melanie Hermann (Amadeu-Antonio-Stiftung), Christoph Hövel (#kopfeinschalten), Clemens Hötzel (SABRA Servicestelle für Antidiskriminierungsarbeit Beratung bei Rassismus und Antisemitismus) und Marat Trusov (Mobile Beratung gegen Rechtsextremismus Wuppertal). 32 Min.

Die Veranstaltenden behalten sich vor, von ihrem Hausrecht Gebrauch zu machen und Personen, die rechtsextremen Parteien oder Organisationen angehören, der rechtsextremen Szene
zuzuordnen sind oder bereits in der Vergangenheit durch rassistische, nationalistische,
antisemitische oder sonstige menschenverachtende Äußerungen in Erscheinung getreten
sind, den Zutritt zur Veranstaltung zu verwehren oder von dieser auszuschließen.