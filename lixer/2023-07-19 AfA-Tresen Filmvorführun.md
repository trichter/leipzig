---
id: 2023-6-19-photo_2023-06-09_12-00-28
title: AfA-Tresen Filmabend
start: 2023-07-19 19:00
end: 2023-07-19 21:00
address: Pörstenerstr. 9
isCrawled: true
---
Filmvorführung] "Schwarze Sonne" - Dokumentation von Rüdiger Sünner (1998)

Ein, trotz seines Alters, weiterhin aktueller und kompakter Dokumentarfilm, der die mythologischen Hintergründe des Nationalsozialismus untersucht und die Thesen einiger wesentlicher Vertreter des nordischen Rassenkults analysiert. Die Recherche erstreckt sich sowohl auf die okkulten Quellen der Nazis, auf die „Rassenforschungen“ der SS, als auch auf die Architektur des Dritten Reiches mit „Lichtburgen“ und anderen Orten übersteigerter Ahnengläubigkeit. Zudem schlägt der Film einen Bogen in die Gegenwart, fragt nach Gründen für das aktuelle Interesse an Esoterik, Mystik und Magie. Denn solche Mythen leben in der neonazistischen Szene, in unreflektierten esoterischen Zirkeln und anderen Gruppierungen weiter.

--------------------

Nach der Vorführung bleibt weiterhin Raum zum gemütlichen Plaudern und Austauschen mit nicht-alkoholischen und alkoholischen Getränken.

Die Dokumentation ist in deutscher Sprache.