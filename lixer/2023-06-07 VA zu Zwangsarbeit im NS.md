---
id: 2023-5-7-Raddtour_VVN_BDA
title: VA zu Zwangsarbeit im NS
start: 2023-06-07 12:00
end: 2023-06-07 15:30
address: Pörstenerstr. 9
isCrawled: true
---
Live Übertragung der Fahrradtour auf den Spuren von NS-Zwangsarbeit und Widerstand im Leipziger Westen.


Anlässlich des Jahrestages der Befreiung vom Faschismus m 08.Mai 1945 begeben wir uns auf die Spurensuche im Leipziger Westen. An neun Stationen folgen wir den Spuren von Zwangsarbeiter:innen und Widerstandskämpfer:innen in Plagwitz und Großzschocher.

Der Rundgang beginnt um 12:00 in der Nonnenstraße 21

Wem das Fahrradfahren zu mühevoll ist, der oder die kann den Inhalten per Livestream ab 12:00  im Lixer e.V. folgen.

Die Veranstaltenden behalten sich vor, von ihrem Hausrecht Gebrauch zu machen und Personen, die rechtsextremen Parteien oder Organisationen angehören, der rechtsextremen Szene
zuzuordnen sind oder bereits in der Vergangenheit durch rassistische, nationalistische,
antisemitische oder sonstige menschenverachtende Äußerungen in Erscheinung getreten
sind, den Zutritt zur Veranstaltung zu verwehren oder von dieser auszuschließen.