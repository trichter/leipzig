---
id: 2023-12-17-breflo
title: Spieleflohmarkt & Küfa
start: 1970-01-01 01:00
address: Pörstenerstr. 9
isCrawled: true
---
Du willst deine alten Brettspiele loswerden? Her zu uns! 

Am 17.12.2023 ab 16:00 Uhr veranstaltet die Spieleabend Crew des Lixer e.V. einen kleinen Brettspielflohmarkt im Lixer. Kommt vorbei, Anmeldung nicht notwendig. 

Es gibt natürlich keine Standgebühren. 

Kommerzielle Händler*innen sind nicht erwünscht.


Ab 19.00 Uhr gibt es dann Küfa mit leckerem indischen Essen. 


Wir freuen uns auf euch

Eure Spieleabendcrew