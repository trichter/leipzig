---
id: 2023-4-25-Bureau-gruen
title: Bürokratie-Sprechstunde
start: 2023-05-25 16:00
end: 2023-05-25 19:00
address: Pörstenerstr. 9
isCrawled: true
---
Brauchst du Unterstützung beim Ausfüllen von Anträgen? Oder Hilfe beim Formulieren von Ämterschreiben und Widersprüchen? Wir bieten in unserer Bürokratie-Sprechstunde Orientierung im Umgang mit Ämtern sowie dem allgemeinen Bürokratiedschungel. Wir vermitteln gern an Stellen, die sich auf dein Anliegen spezialisiert haben.

Die Bürokratie-Sprechstunde umfasst zum Beispiel Hilfen zu den Themen Jugendamt, Jobcenter, Wohnthemen (z.B. Wohngeld, Streitigkeiten mit Vermieter*innen etc.), Kranken-, Renten- und Pflegekasse. 

Kontaktiere uns unter <lixer_buerokratie@riseup.net> oder komm einfach vorbei!