---
id: 2023-4-29-Buchmesse_Plakat_A3_split_2_Page_1_fxp9tia
title: LinXX liest! BML 2023
start: 2023-05-29 15:00
end: 2023-05-29 18:30
address: Pörstenerstr. 9
isCrawled: true
---
Lothar Kurth
Buchvorstellung mit Kaffee und Kuchen
Lindenau – Erlebnisse in Kinder- und Jugendjahren

Erinnerungen sind Teil unseres Lebens. Jeder blickt auf angenehme und betrübliche zurück. Vorsorglich erinnert man sich eher an das Positive und hält das Negative gedeckelt. Aber auch aus Letzterem erwachsen Lernprozesse fürs Leben. Der Autor verbrachte Kindheit und Jugend im Leipziger Stadtteil Lindenau und kehrte ab Ende der 1990er Jahre mehrfach zur Spurensuche zurück. Seine Erinnerungen reflektieren weniger die große Geschichte als vielmehr den Alltag in einem verfallenden, aber immer noch lebendigen Großstadtviertel der 1950er bis 1980er Jahre. Die detailreichen und liebevoll verfassten Geschichten erinnern viele Leser an eigene Erlebnisse. Jüngeren bieten sie Einblicke in den nicht erfahrenen DDR-Alltag.

Die Veranstaltung wird [auf dem YouTube Kanal](www.youtube.com/@lixere.v.7454/streams) des Lixer e.V. online übertragen und ist später noch abrufbar. 

Veranstalter*innen: Lixer, Verlag Pro Leipzig