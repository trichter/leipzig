---
id: 2023-4-30-vortragunddiskussion
title: Vortrag und Diskussion
start: 2023-05-30 17:00
end: 2023-05-30 20:00
address: Pörstenerstr. 9
isCrawled: true
---
Antonio Gramsci
Kulturelle Hegemonie & Stadtteilarbeit mit Uwe Hirschfeld

Uwe Hirschfeld hat zahlreiche Texte zu dem italienischen Schriftsteller und Philosophen Antonio Gramsci verfasst. Sein Konzept der Hegemonie, wird vor allem in linken Kreisen breit rezipiert. Nach einer kurzen Einführung zu den Grundzügen der Philosophie Gramscis, seinem Leben und Wirken, sprechen wir über die Möglichkeiten, wie seine Überlegungen in die aktuelle Praxis linker Stadtteilarbeit eingebunden und weitergedacht werden können.
Veranstalter*innen: Lixer, Rosa Luxemburg Stiftung