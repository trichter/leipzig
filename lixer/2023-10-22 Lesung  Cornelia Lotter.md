---
id: 2023-9-22-Der-Himmel-ueber-den-Mauern-eBook-klein-1-606x1024
title: Lesung  Cornelia Lotter
start: 2023-10-22 19:30
end: 2023-10-22 22:00
address: Pörstenerstr. 9
isCrawled: true
---
Die Leipziger Autorin Cornelia Lotter liest aus ihrem Roman "Himnel über den Mauern" über den geschlossenen Jugendwerkhof in Torgau.

Der Geschlossene Jugendwerkhof Torgau (GJWH) war eine Disziplinareinrichtung im System
der Spezialheime der Jugendhilfe in der DDR. Er unterstand direkt dem Ministerium für
Volksbildung. Leiter der Anstalt in Torgau waren von 1964 bis 1968 Günther Lehmann und von
1968 bis 1989 Horst Kretzschmar.

In den geschlossenen Jugendwerkhof wurden laut Anordnung vom 22. April 1965 Insassen von
Jugendwerkhöfen und Spezialkinderheimen im Alter von 14 bis 20 Jahren, welche die
Heimordnung „vorsätzlich schwerwiegend und wiederholt verletzen“, eingewiesen. (wikipedia)
Cornelia Lotters Roman „Der Himmel über den Mauern“ handelt von 2 Geschichten der
Inhaftierung in Torgau: ein Mann im Wehrmachtsgefängnis und ein Mädchen später im
geschlossenen Jugendwerkhof.

Der geschlossene Jugendwerkhof in Torgau war der schlimmste Ort der Jugend“erziehung“ der
DDR. Die Jugendlichen sollten gebrochen und so zu brauchbaren Mitgliedern der Gesellschaft
werden.