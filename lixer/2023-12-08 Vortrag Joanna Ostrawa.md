---
id: 2023-11-8-vortrag:joannaostrawa
title: "Vortrag: Joanna Ostrawa"
start: 2023-12-08 19:30
end: 2023-12-08 19:30
address: Pörstenerstr. 9
isCrawled: true
---
Am 8. November laden wir zu einem Vortrag von Dr. Joanna Ostrowska über die Verfolgung von Homosexuellen zur Zeit des Nationalsozialismus ein. In ihrem neuesten Buch "Jene. Homosexuelle während des Zweiten Weltkriegs", das auch ins Deutsche übersetzt wurde, dokumentiert die Autorin die Biografien von polnischen, deutschen und jüdischen Opfern des berüchtigten Paragraphen 175. Fotos, Briefe, Polizeiarchive, Konzentrationslager, geheime Treffpunkte queerer Personen. Die Autorin analysiert detailliert diese Orte, Erinnerungen und Dokumente in ihrer Forschung und rekonstruiert vergessene Biografien von Opfern des Nationalsozialismus, die noch lange nach dem Krieg Verfolgung ausgesetzt waren.

Dr. Joanna Ostrowska ist Historikerin, Dramatikerin und Filmwissenschaftlerin. Sie beschäftigt sich in ihrer Forschung mit den sogenannten vergessenen Opfern des Holocaust, zuvor hat sie unter anderem über sexuelle Zwangsarbeit zur Zeit des Nationalsozialismus und über Opfer erzwungener Sterilisation in Niederschlesien geschrieben.

Moderation: Pawel Matusz

[Link zum Buch] (https://metropol-verlag.de/produkt/jene-homosexuelle-waehrend-des-zweiten-weltkriegs/)

Die Vortragsreihe findet im Rahmen des Projekts "Geschichte für Alle - Erinerungskultur in Zschocher Online statt. Diese Vortragsreihe wird gefördert vom Bundesministerium für Familie, Senioren, Frauen und Jugend im Rahmen des Bundesprogramm Demokratie leben sowie vom Freistaat Sachsen und dem Landespräventionsrat Sachsen. Diese Maßnahme wird mitfinanziert mit Steuermitteln auf Grundlage des von den Abgeordneten des Sächsischen Landtags beschlossenen Haushaltes.