---
id: 2023-5-5-Tresen_05-23
title: AFA Tresen
start: 2023-06-05 19:30
end: 2023-06-05 19:30
address: Pörstenerstr. 9
isCrawled: true
---
Vortrag: „Nazis und rechte Netzwerke in den deutschen Sicherheitsbehörden.“ am 5. Mai 2023

Die Kampagne EntnazifizierungJetzt stellt ihre Rechercheergebnisse vor.

Seit dem 8. Mai 2020 hat die Kampagne EntnazifizierungJetzt über 850 Skandale mit Nazis und Rassist:innen in den deutschen „Sicherheitsbehörden“ gesammelt. Ihre Ergebnisse hat sie nun in der Broschüre „Fünfundfünfzigtausend Schuss“ zusammengefasst, die sie am 5, Mai in Leipzig vorstellt. Dabei geht es um Zusammenhänge und historische Kontinuitäten, aber auch um konkrete Beispiele. Eins ist klar: Nazis und Rassist:innen in Polizei, Bundeswehr, Verfassungsschutz, Bundesnachrichtendienst und Justiz bilden Netzwerke und nutzen diese Strukturen zur Unterwanderung unserer Gesellschaft. Das Problem ist deutlich größer als von öffentlicher Seite immer behauptet wird.

Die Veranstaltenden behalten sich vor, von ihrem Hausrecht Gebrauch zu machen und Personen, die rechtsextremen Parteien oder Organisationen angehören, der rechtsextremen Szene
zuzuordnen sind oder bereits in der Vergangenheit durch rassistische, nationalistische,
antisemitische oder sonstige menschenverachtende Äußerungen in Erscheinung getreten
sind, den Zutritt zur Veranstaltung zu verwehren oder von dieser auszuschließen.