---
id: 2023-7-28-Lieben_mit
title: Filmreihe " Liebe"
start: 2023-08-28 20:00
end: 2023-08-28 22:00
address: Pörstenerstr. 9
isCrawled: true
---
Im Rahmen des Kultursommers 2023 in Kleinzschocher zeigen wir Filme des Medienprojekt Wuppertal

Am 28.07. zeigen wir zwei Kurzfilme zum Thema Liebe und sexuelle Selbstbestimmung

Lieben mit sexueller Selbstbestimmung 
50 Min..

In den Filmen beschreiben Jugendliche und junge Erwachsene ihre verschiedenen Vorstellungen von einvernehmlichem Sex und klassischen sowie alternativen Beziehungsmodellen, wie z.B. Polygamie, Polyamorie oder offener Beziehung. In Interviews berichten die Jugendlichen über ihren Umgang, ihre Wünsche und ihre Erfahrungen in Bezug auf Sex und Beziehung und wo sie die Grenze zwischen konsensualem Sex und Übergriff sehen. Hierbei geht es auch um die Fragestellungen: Welche eindeutigen Signale gibt es vorm Sex? Wie kann die Beziehung und der Sex positiv zwischen den Sexualpartner*innen verhandelt und ihre gegenseitigen Bedürfnisse für beide Seiten verständlich kommuniziert werden? Wie geht man mit missverständlichen Situationen um?

Einfach Mensch
46 Min.


Der Film begleitet drei junge Menschen in ihrem alltäglichen Umgang mit ihrer Transidentität. In persönlichen Interviews geben sie tiefe Einblicke in ihr Leben und in ihre Gefühlswelten. Dabei schildern sie ihre Ängste, Probleme und Erfahrungen, die sie in der Schule, der Familie und im Freundeskreis im Zusammenhang mit ihrer Transsexualität erlebt haben.