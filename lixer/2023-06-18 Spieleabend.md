---
id: 2023-5-18-uboot_Wwcl3T8
title: Spieleabend
start: 2023-06-18 19:00
end: 2023-06-18 22:00
address: Pörstenerstr. 9
isCrawled: true
---
Mit Plastikfiguren Unheil anrichten

Der Spieleabend im Lixer für komplexe Brettspiele. Jeden zweiten Donnerstag in der geraden Kalenderwoche um 19 uhr.

Spiele sind vorhanden, ihr könnte aber auch gerne ein Spiel eurer Wahl mitbringen und wir entscheiden zusammen. 

Getränke gegen Spende. 


Die Veranstaltenden behalten sich vor, von ihrem Hausrecht Gebrauch zu machen und Personen, die rechtsextremen Parteien oder Organisationen angehören, der rechtsextremen Szene
zuzuordnen sind oder bereits in der Vergangenheit durch rassistische, nationalistische,
antisemitische oder sonstige menschenverachtende Äußerungen in Erscheinung getreten
sind, den Zutritt zur Veranstaltung zu verwehren oder von dieser auszuschließen.