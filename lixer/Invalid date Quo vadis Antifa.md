---
id: 2023-12-7-signal-2023-11-28-134320_002
title: Quo vadis Antifa?
start: 1970-01-01 01:00
address: Pörstenerstr. 9
isCrawled: true
---
Wie weiter mit der antifaschistischen Organisierung in Zschocher? 

Nach zwei Jahren Organisierungs- und Vernetzungsbemühungen in Klein- und Großzschocher wollen wir als Gruppe Bilanz ziehen und die Frage diskutieren: Wie weiter mit antifaschistischer Organisierung in Zschocher?

- Was konnte Zschocher Antifa bisher erreichen?
- Wo waren die Probemstellen?
- Braucht es andere Strategien?
- Wie könnte eine zukünftige Stadtteilantifa für Zschocher aussehen?

Es ist auch eine Einladung an alle interessierten Antifaschist*innen aus dem Kiez und darüber hinaus, die sich besser vernetzen und aktiv werden wollen, an der Diskussion und an dem Austausch teilzunehmen.