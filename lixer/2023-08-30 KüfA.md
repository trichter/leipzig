---
id: 2023-7-30-photo_2022-09-10_11-05-30
title: KüfA
start: 2023-08-30 19:00
end: 2023-08-30 22:00
address: Pörstenerstr. 9
isCrawled: true
---
Mindestens jeden zweiten Sonntag verwöhnt euch das Kochkolletktiv Lixer mit leckerem Essen. 

Kommt ab 19:00 Uhr vorbei um zusammen mit euren Nachbar*innen zu Essen. 

Die Mahlzeiten werden gegen eine Spende herausgegeben. 

Der Gewinn der KüfA wird  an gemeinnützige Organisationen gespendet.