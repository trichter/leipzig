---
id: 2023-9-19-photo_2023-05-04_15-57-13_2
title: Queering Balfolk
start: 2023-10-19 19:30
end: 2023-10-19 23:00
address: Pörstenerstr. 9
isCrawled: true
---
Queering Balfolk im Lixer
Tanzkurse für queers* und FreundInnen,
Line und Mara bieten euch einen Einstiegskurs im Balfolk tanzen an. 
An 9 Terminen könnt ihr eure ersten Tanzschritte machen und Paartänze, Gruppentänze, 
Solotänze und Kettentänze lernen. 
Du brauchst keine feste Tanzperson mitzubringen: 
es reicht dich selbst, eine*n FreundIn oder einen kleinen Haufen mitzubringen 
- fühl dich ganz herzlich eingeladen! 
Der Ablauf:
ab 19:30  Tanzkurs für neueinsteigende Menschen
ab 21:00 Livemusik - Tanzen zum Spaß haben!

Es wird pro Abend um eine kleine Spende gebeten (nur wenn möglich), 
die Hauptkosten werden durch den Stadtbezirksbeirat Südwest finanziert.