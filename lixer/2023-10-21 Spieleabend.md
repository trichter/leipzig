---
id: 2023-9-21-new_york
title: Spieleabend
start: 2023-10-21 19:00
end: 2023-10-21 22:00
address: Pörstenerstr. 9
isCrawled: true
---
Mit Plastikfiguren Unheil anrichten

Der Spieleabend im Lixer für komplexe Brettspiele. Jeden zweiten Donnerstag in der geraden Kalenderwoche um 19 Uhr.

Spiele sind vorhanden, ihr könnte aber auch gerne ein Spiel eurer Wahl mitbringen und wir entscheiden zusammen.

Getränke gegen Spende. Der Laden ist rauchfrei.


Die Veranstaltenden behalten sich vor, von ihrem Hausrecht Gebrauch zu machen und Personen, die rechtsextremen Parteien oder Organisationen angehören, der rechtsextremen Szene
zuzuordnen sind oder bereits in der Vergangenheit durch rassistische, nationalistische,
antisemitische oder sonstige menschenverachtende Äußerungen in Erscheinung getreten
sind, den Zutritt zur Veranstaltung zu verwehren oder von dieser auszuschließen.