---
id: 2023-5-31-winzig_logo_bda_leipzig
title: Vortrag zu frühen KZs
start: 1970-01-01 01:00
address: Pörstenerstr. 9
isCrawled: true
---
Übertragung des Online-Vortrag des VVN-BdA

Die Zerschlagung des Widerstands – Terror gegen links und die Errichtung der frühen Konzentrationslager

Im Frühjahr 1933 war das gesamte Deutsche Reich von Lagern und anderen Haftstätten übersät. Politische Gegner*innen wurden verschleppt, mit Gewalt mundtot gemacht oder ins Exil gezwungen. Die Erinnerung an diese frühe Phase der nazistischen Verfolgung wurde lange vernachlässigt. Wir sprechen über die historischen Hintergründe und aktuelle erinnerungspolitische Auseinandersetzungen.

Mit Maxi Schneider (Moderation und inhaltliche Einführung), Steffen Richter (aktiv bei AKuBiZ e.V. und engagiert für die Erinnerung an das frühe Konzentrationslager Hohnstein) und Daniela Schmohl (VVN-BdA Leipzig, Sprecherin der Sächsischen Landesarbeitsgemeinschaft Auseinandersetzung mit dem Nationalsozialismus, Historikerin und Bildungsreferentin bei der Rosa-Luxemburg-Stiftung Sachsen).