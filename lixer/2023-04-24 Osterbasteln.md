---
id: 2023-3-24-basteln_Suilg44
title: Osterbasteln
start: 2023-04-24 15:00
end: 2023-04-24 18:00
address: Pörstenerstr. 9
isCrawled: true
---
Am 24.3. findet ab 15 Uhr erneut unser Bastelzirkel für Groß und Klein statt. 
Material für Osterbasteleien und mehr sowie Ideen sind vorhanden. Eure Einfälle sind aber auch sehr gern gesehen!