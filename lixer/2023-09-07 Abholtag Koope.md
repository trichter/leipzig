---
id: 2023-8-7-Koope_Inflation
title: Abholtag Koope
start: 2023-09-07 17:00
end: 2023-09-07 19:00
address: Pörstenerstr. 9
isCrawled: true
---
Die Koope - Leipziger Lebensmittelkooperative ist ein Verein in Gründung. Unser Beitrag zu Klima- und Umweltschutz ist es, Einfluss auf die Land- und Ernährungswirtschaft in und um Leipzig zu nehmen. Neben Infoveranstaltungen z.B. auf Stadt(teil)festen, Küfas und Filmabenden organisieren wir gemeinschaftlich die Grundversorgung unsere Mitglieder mit Lebensmitteln und Hygieneprodukten. Wir wollen ökologisch erzeugte Lebensmittel zugänglicher zu machen und eine alternative Wirtschaftsweise ermöglichen. Die Abholung und auch Lagerung der Waren findet in den Räumlichkeiten des Lixers statt. 

Sowohl in den Regalen, als auch auf unserer Mitgliederliste ist noch Platz! 

Wir arbeiten derzeit vor allem daran, eine regionale Versorgungsstruktur und den Verein weiter aufzubauen. Alle AGs freuen sich riesig über Zuwachs!

Mitmachen: Kurze Mail an koope@riseup.net

Weitere Infos unter: https://www.koope.de/