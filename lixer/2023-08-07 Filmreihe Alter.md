---
id: 2023-7-7-filmreihe"alter"
title: Filmreihe "Alter"
start: 2023-08-07 20:00
end: 2023-08-07 22:00
address: Pörstenerstr. 9
isCrawled: true
---
Im Rahmen des Kultursommers 2023 in Kleinzschocher zeigen wir Filme des Medienprojekt Wuppertal

Am 07.07.2023 zeigen wir 2 Kurzfilme  zum Thema "Alter" :

Endlich so leben wie ich will 
37 Min.

Der Film porträtiert fünf Menschen, denen gemeinsam ist, dass sie nicht so leben konnten, wie sie wollten, und aus diesem Grund ihre bisherigen sozialen Bezüge oder ihr Herkunftsland verlassen haben – zum Teil auch vor dem Hintergrund einer diskriminierenden Gesellschaft.


Du gehst und ich bleibe 
45 Min.
Der Film erzählt vom doppelten Abschiednehmen bei einer Demenzerkrankung des Partners im Laufe der Erkrankung und nach dem Tod. Es werden drei Partnerschaften porträtiert, die den Umgang mit der Erkrankung zeigen. Was passiert mit einer Liebesbeziehung, in der der Partner sich aufgrund von Demenz verändert und nicht mehr dieselbe Person ist? Und wie gehen die hinterbliebenen Partner mit dem Tod und dem Verlust um?