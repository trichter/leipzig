---
id: 2023-11-20-antifatresen
title: Antifa Tresen
start: 2023-12-20 19:00
end: 2023-12-20 21:00
address: Pörstenerstr. 9
isCrawled: true
---
20.11. um 19 Uhr @ Lixer

Im Sommer 2019 beim Klimacamp in Pödelwitz, angesichts einer drohenden AfD-Beteiligung bei der damaligen Landtagswahl, gab es den Versuch bei Themen und Bewegungen miteinander zu verbinden. Rückblickend und mit kleinen Anekdoten berichten wir von unseren Erlebnissen und welche Schlüsse aus diesem Prozess gezogen wurden.
Angesichts einer sich weiterhin verschärfenden Klimakrise und hohen Zustimmungsraten zur AfD bleibt das Thema aktuell: Wo liegen die Gemeinsamkeiten? (Wie) können wir unsere Kämpfe sinnvoll verbinden? Welche Perspektiven bietet das für einen linken Aktivismus?

Im Anschluss von der Veranstaltung gibt es noch die Möglichkeit zum gemütlichen Plaudern und Austauschen mit nicht-alkoholischen und alkoholischen Getränken.

Die Veranstaltung wird in deutscher Sprache stattfinden. 
Die Räumlichkeiten sind über eine zweistufige Treppe zu erreichen.
Die Toilette über eine mehrstufige und ist selbst leider nicht barrierefrei.