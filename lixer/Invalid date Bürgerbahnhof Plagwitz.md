---
id: 2023-12-10-bürgerbahnhofplagwitz
title: Bürgerbahnhof Plagwitz
start: 1970-01-01 01:00
address: Pörstenerstr. 9
isCrawled: true
---
Treffen der "Bürgerinitiative Bürgerbahnhof Plagwitz erhalten!"

Am 10.12.2023, 17:30 Uhr laden wir euch ein, den 3. Workshoptermin der Bürgerbeteiligung mit uns auszuwerten. Kommt zahlreich. Wir wenden uns gegen eine Bebauung der nordwestlichen Fläche des ehemaligen Plagwitzer Güterbahnhofs. 

Alle Infos findet ihr unter [hier](https://bahnhof-plagwitz.de/)