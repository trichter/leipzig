---
id: 2023-11-10-signal-2023-10-27-180832_005
title: "Vortrag: Marko Dinić"
start: 2023-12-10 19:30
end: 2023-12-10 21:30
address: Pörstenerstr. 9
isCrawled: true
---
Am 10. November laden wir zu einem Vortrag mit dem Autor Marko Dinić. In seinem Vortrag: „Die Wagen brauchen sie für andere Dinge  – Der Holocaust und die Literatur in den Ländern des ehemaligen Jugoslawiens."  Spricht er über den Umgang mit der Shoa in den Ländern des ehemaligen Jugoslawien:

Hier geht es zum [Livestream](https://youtube.com/live/uPb60AvYTNQ?feature=share)

Das Bombardement Beograds am 6. April 1941 durch die deutsche Luftwaffe markierte den Anfang vom Ende des ersten Jugoslawiens; es bedeutete nicht nur den Eintritt seiner ehemaligen Länder in den Zweiten Weltkrieg, sondern auch den Eintritt in einen blutigen Bürgerkrieg, aus dem die Sozialistische Föderative Republik Jugoslawien hervorgehen wird.  Bereits im Juni 1942 meldete Emanuel Schäfer, Befehlshaber der Sicherheitspolizei in den besetzten Gebieten: „Serbien ist judenfrei!“ Die systematische Verfolgung, Deportierung und Vernichtung der jüdischen Bevölkerung, an deren traurigem Ende das Menschheitsverbrechen der Shoah steht, erreicht hier ihren vorläufigen Höhepunkt. In meinem Vortrag nähere ich mich diesem beinahe vergessenen Kapitel der Vernichtung der jugoslawischen Jüdinnen und Juden mit Mitteln des literarischen Zeugnisses, welches versucht, die Gräueltaten der Nationalsozialisten und Ustascha ungeschönt und unmittelbar einzufangen. Dazwischen sollen in einem freien Vortrag die Eckdaten der Besatzung und Auflösung des Königreichs Jugoslawien Aufschluss über die Bedingungen dieser Vernichtung geben. 

Marko Dinić ist ein in Wien ansässiger Schriftsteller und Publizist. Er studierte Germanistik und Jüdische Kulturgeschichte in Salzburg. 2019 erschien sein viel besprochener Debütroman "Die guten Tage" bei Zsolnay/Hanser."

Moderation: Manuel Wagner

Die Vortragsreihe findet im Rahmen des Projekts "Geschichte für Alle - Erinnerungskultur in Zschocher Online statt. Diese Vortragsreihe wird gefördert vom Bundesministerium für Familie, Senioren, Frauen und Jugend im Rahmen des Bundesprogramm Demokratie leben sowie vom Freistaat Sachsen und dem Landespräventionsrat Sachsen. Diese Maßnahme wird mitfinanziert mit Steuermitteln auf Grundlage des von den Abgeordneten des Sächsischen Landtags beschlossenen Haushaltes.