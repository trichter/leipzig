---
id: "533865848843223"
title: "Vortrag: No means No! – zum Umgang mit sexualisierter Gewalt im Fußball"
start: 2023-03-09 19:00
locationName: Pöge-Haus e.V.
address: Hedwigstraße 20, Leipzig
link: https://www.facebook.com/events/533865848843223/
image: 330924258_699513915250423_9124614800596043524_n.jpg
isCrawled: true
---
No means No! – zum Umgang mit sexualisierter Gewalt im Fußball
Vortrag mit Antje Grabenhorst
Weibliche Fans und Ultras gehören zum Fußball, doch Sexismus ist ebenso wie Fälle sexualisierter Gewalt weiterhin Alltag in den Fußballstadien. Noch immer steckt die Auseinandersetzung mit sexualisierter Gewalt im Fußball in den Kinderschuhen. Erst nach einigen traurigen medienwirksamen Vorfällen ist das Thema etwas aus der Tabuzone herausgekommen – Vorfälle, die lediglich ein Schlaglicht auf den gewaltigen Umfang von Sexismus und sexualisierter Gewalt im Fußball liefern, und dennoch prompt zu bloßen „schrecklichen Einzelfällen“ skandalisiert wurden.
Bei einigen Profivereinen gibt es inzwischen bereits Konzepte und Anlaufstellen für Betroffene von Sexismus und sexualisierter Gewalt im Fußballkontext. Auch wir wollen uns am Donnerstag, dem 09. März, ab 19 Uhr im Pöge-Haus (Hedwigstraße 20, 04315 Leipzig) damit beschäftigen, wie diesem Thema im Stadion und in Fanszenen begegnet werden kann.
Die Referentin Antje Grabenhorst wird die Arbeit des Netzwerks gegen Sexismus und sexualisierte Gewalt und deren Handreichungen vorstellen sowie zentrale Begriffe und wichtige Schritte im Umgang mit Übergriffen erläutern. Ein besonderer Schwerpunkt soll dabei auf der Herausforderung eines selbstregulativen Ansatzes in den Fanszenen liegen.
Antje Grabenhorst ist Koordinatorin von Fan.Tastic Females und Teil der Netzwerke gegen Sexismus und sexualisierte Gewalt sowie F_In – Frauen im Fußball