---
id: "511032887769580"
title: Die Zukunft der Außenpolitik ist feministisch - Lesung Kristina Lunz
start: 2023-04-27 19:30
locationName: Pöge-Haus e.V.
address: Hedwigstraße 20, Leipzig
link: https://www.facebook.com/events/511032887769580/
image: 332200143_551930176940703_8371760925997238486_n.jpg
isCrawled: true
---
Frauen und Minderheiten sind von Kriegen und Krisen besonders betroffen - ihre Sicherheit und Rechte werden als erstes eingeschränkt und als letztes wiederhergestellt. Noch immer dominieren weiße Männer das internationale politische Parkett und erhalten patriarchale Strukturen. Wenn Frauen und Minderheiten an Friedensprozessen beteiligt werden, ist der Frieden stabiler und nachhaltiger - davon ist die Autorin und Aktivistin Kristina Lunz überzeugt. In ihrem Buch "Die Zukunft der Außenpolitik ist feministisch" erläutert sie, was feministische Außenpolitik ist, welche Ziele sie verfolgt und welche patriarchalen Strukturen derzeit die internationale Außenpolitik gestalten. Bei der Autorinnenlesung im Rahmen der Leipziger Buchmessse werden auch aktuelle Konflikte wie der Krieg in der Ukraine und die Protestbewegung im Iran in den Fokus gerückt.

Zur Autorinnenlesung vom Kristina Lunz laden wir Sie herzlich ein! Der Eintritt ist kostenlos. 

Einlass: 19.00 Uhr

Beginn der Lesung: 19.30 Uhr

Kristina Lunz ist Unternehmerin, Autorin und Aktivistin. Als Mitbegründerin und co-CEO baut sie das gemeinnützige Unternehmen Centre for Feminist Foreign Policy mit ihrer Mitgründerin auf. 2019 wurde Kristina Lunz auf die ‘30 under 30’ Liste des Forbes Magazin gesetzt und drei Monate später ebenfalls auf die DACH- 30 under 30 Liste von Forbes. Sie ist eine Atlantik Brücke Young Leader, Ashoka Fellow, BMW Foundation Responsible Leader sowie Mitglied der Advisory Group der Goalkeepers Initiative der Bill und Melinda Gates Foundation zu den Nachhaltigkeitszielen der Vereinten Nationen. Kristina Lunz baute als Beraterin des Auswärtigen Amts das Netzwerk des Außenministers ‘Unidas’ auf und hat eine Vielzahl an Auszeichnungen und Preisen erhalten. Sie studierte Diplomatie, Menschenrechte und Psychologie neben Oxford auch in London und Stanford.