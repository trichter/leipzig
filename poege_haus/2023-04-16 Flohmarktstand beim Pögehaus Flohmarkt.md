---
id: "541884701409280"
title: Flohmarktstand beim Pögehaus Flohmarkt
start: 2023-04-16 11:00
locationName: Pöge-Haus e.V.
address: Hedwigstraße 20, Leipzig
link: https://www.facebook.com/events/541884701409280/
image: 339496674_2009984289345029_781570455850651161_n.jpg
isCrawled: true
---
Ich due Gelegenheit  nutzen mich von einigen Sachen und Kleidungsstücken zu trennen, die zwar noch in bestem Zustand sind aber die trotzdem nicht mehr so gut passen.  Unter anderem ist Kleidung von Inwuwei Creations und Alien Lazer Baby aus Berlin dabei.
Kinderkleidung wird es auch geben und ein paar nützliche Kleinigkeiten sind sicher auch dabei.
Wie kommst du hin:
Mit der Straßenbahnlinie 8 und 1 bis Haltestelle  Eisenbahnstraße/Hermann-Liebmann-Straße.
Bezahlung möglich bar oder via PayPal.