---
id: "217184447689135"
title: Sachsen rechts unten - Die Extreme Rechte und der Klima-Diskurs
start: 2023-05-24 19:30
locationName: Pöge-Haus e.V.
address: Hedwigstraße 20, Leipzig
link: https://www.facebook.com/events/217184447689135/
image: 344791404_793158258869496_3553904816942290939_n.jpg
isCrawled: true
---
Die Probleme des menschengemachten Klimawandels sind in den zentralen Debatten von Politik und Gesellschaft angekommen. Spätestens mit dem Start des neu-rechten Dresdner Magazins “Die Kehre – Zeitschrift für Naturschutz” im Mai 2020 wurde besiegelt: Auch die extreme Rechte muss sich den Debatten über Klimaschutz und Klimawandel stellen. Von einem bis dahin geführten Abwehrkampf ist der Start des neu-rechten Umweltmagazins als Ausdruck einer Neuformierung zu sehen. Das eigene ideologische Selbstverständnis innerhalb der extremen Rechten soll mit ökologischen Fragen verbunden werden. Auch in Sachsen.

Im Rahmen der Veranstaltungsreihe wird die neue Ausgabe der Jahrespublikation „Sachsen rechts unten“ vorgestellt.
Welche unterschiedlichen Positionen, Meinungen und Forderungen bezüglich der Klimafragen finden sich bei der extremen Rechten wieder?

Referent*innen:
Solvejg Höppner, Mobile Beratung Kulturbüro Sachsen e.V.
Anne Gehrmann, Mobile Beratung Kulturbüro Sachsen e.V.
Marco Henning, Mobile Beratung Kulturbüro Sachsen e.V.

Die Referent*innen werden an diesem Abend einen Überblick über die antidemokratischen Ziele, Methoden und Strategien geben, die Rechtsextreme in Sachsen bei der Auseinandersetzung mit Klima- und Umweltschutzfragen verfolgen.

Die Publikation kann an diesem Abend auch kostenfrei vor Ort mitgenommen werden.
Nach einem kurzen Input dient der Abend vor allem dem Austausch und der Diskussion.
Die Teilnahme an den Veranstaltungen ist kostenfrei.

Eine gemeinsame Veranstaltung von Kulturbüro Sachsen e.V. und Weiterdenken – Heinrich-Böll-Stiftung Sachsen.

Ausführliche Infos: https://kulturbuero-sachsen.de/veranstaltungsreihe.../