---
id: "1050518345920415"
title: Das (aller-)letzte Gefecht - Die Linke im (neuen) Kalten Krieg
start: 2023-06-20 19:00
locationName: Pöge-Haus e.V.
address: Hedwigstraße 20, Leipzig
link: https://www.facebook.com/events/1050518345920415/
image: 351166079_1002977177725629_6046195095165626617_n.jpg
isCrawled: true
---
Die Debatten und Verlautbarungen der außerparlamentarischen Linken führen ein “Leben in der Zeitschleife”: ungebrochen tönt aus ihnen das Vokabular des frühen 20. Jahrhunderts. Doch was einmal richtig war, kann sich unter veränderten historischen Bedingungen in sein Gegenteil verkehren. Welche fatalen Folgen die Ignoranz gegenüber diesem Zeitkern von Wahrheit haben kann, hat die deutsche Linke häufig gezeigt, und tut es auch heute noch. Nicht zuletzt in ihrer Feindschaft gegenüber Israel oder dem verharmlosenden Verständnis, das dem russischen Überfall auf die Ukraine entgegen gebracht wird — beides schmückt sich nicht selten mit antiimperialistischen, revolutionären und antifaschistischen Floskeln. 

Die Linke scheint die Krisen des letzten Jahrhunderts weder überstanden noch reflektiert zu haben. Jan Gerber versucht in seinem Buch „Das letzte Gefecht“ diese Reflexion nachzuholen und den Schockzustand, in dem sich die deutsche Linke seit dem Untergang des Ostblocks befindet, historisch herauszuarbeiten. Anhand der drei großen linken Schlagwörter Antifaschismus, Antiimperialismus und Revolution soll der Verfall ehemals fortschrittlicher und emanzipatorischer Ideen kritisch aufgearbeitet werden.

Das Buch erschien erstmals 2015 unter gleichem Titel und ist letztes Jahr in erweiterter Neuauflage beim XS-Verlag erschienen. Im Gespräch mit Jan Gerber möchten wir zur historischen Urteilsbildung über die Geschichte der deutschen Nachkriegs-Linken anregen und auch die Auswirkungen auf aktuelle Auseinandersetzungen, Debatten und Spaltungen rund um den russischen Krieg gegen die Ukraine diskutieren.

Jan Gerber ist seit 2009 wissenschaftlicher Mitarbeiter am Dubnow-Institut, seit 2010 in leitender Funktion. Von ihm sind u. a. erschienen: Karl Marx in Paris. Die Entdeckung des Kommunismus, Ein Prozess in Prag. Das Volk gegen Rudolf Slánský und Genossen. Außerdem ist er Herausgeber der Hallischen Jahrbücher, deren erster Band mit dem Titel Die Untiefen des Postkolonialismus 2021 erschien.