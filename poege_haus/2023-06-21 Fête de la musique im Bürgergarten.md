---
id: "1431937607555694"
title: Fête de la musique im Bürgergarten
start: 2023-06-21 18:00
end: 2023-06-21 21:00
locationName: Pöge-Haus e.V.
link: https://www.facebook.com/events/1431937607555694/
image: 351313710_563857779286138_6748526066695956939_n.jpg
isCrawled: true
---
Die urfranzösische Idee der Fête de la Musique ist weltweit ein ungezwungenes Fest der musikalischen Lebensart. Ganz besonders Amateurmusiker*innen aller Genres, aber natürlich auch Profis sind jedes Jahr am Tag der Sommersonnenwende aufgerufen, ihre Spielfreude auszuleben und überall in der Stadt ihr Publikum zu finden und zu begeistern.

Wir laden euch in den wunderschönen Bürgergarten ein, um den Jugend Pop/Jazz Chor Leipzig und die junge Hip-Hop-Gruppe Big Buddha und die Winkekatzen gemütlich zu entdecken!

Jugendpop- und Jazzchor Leipzig: https://www.youtube.com/watch?v=IbwcSJEExW0

Big Buddha und die Winkekatze: https://www.youtube.com/watch?v=LpfYb9yjlfI

Bürgergarten, Meißnerstr. 50 (direkt neben dem Pöge-Haus)