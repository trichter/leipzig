---
id: "921841092193870"
title: Revolution für das Klima
start: 2023-04-20 19:00
locationName: Pöge-Haus e.V.
address: Hedwigstraße 20, Leipzig
link: https://www.facebook.com/events/921841092193870/
image: 340548245_6252666071421926_2459865918057317829_n.jpg
isCrawled: true
---
Warum wir eine ökosozialistische Alternative brauchen

Mit Christian Zeller (Geograph, Universität Salzburg)

Die Klimakatastrophe findet statt. Die Wirtschaftskrise vermindert die Treibhausgasemissionen, aber nur ungenügend und nicht nachhaltig. Die Erderhitzung bleibt zentrale Herausforderung. Millionen von Menschen müssen ihre Heimat verlassen. Die Menschheit befindet sich in einem beängstigenden Rennen gegen die Zeit. Die Regierungen und die großen Konzerne weigern sich, wirksam gegen die Bedrohungen zu handeln. Profite und Wettbewerbsfähigkeit gehen vor. Umwelt-, Gesundheits- und Wirtschaftskrisen verdichten sich.
Christian Zeller macht deutlich, warum es eine Revolution für das Klima  und das Leben der Menschen braucht und wie diese aussehen kann. Die Produktion, der Verkehr und das Finanzsystem sind grundlegend umzubauen. Die gesellschaftliche Infrastruktur - Gesundheit, Pflege, Sorge und Bildung - sind auszubauen. Hierfür braucht es eine gemeinsame und riesige gesellschaftliche Mobilisierung. Dieses Buch zeigt den Weg in Richtung einer Gesellschaft, die weniger und anders produziert, gerecht teilt und in der die Menschen gemeinsam entscheiden. Das ist eine ökosozialistische Gesellschaft.

Christian Zeller ist Professor für Wirtschaftsgeographie an der Paris Lodron Universität Salzburg. Er engagiert sich in ökosozialistischen Kontexten und ist Autor mehrer Bücher. Zuletzt erschien von ihm “Revolution für das Klima – Warum wir eine ökosozialistische Perspektive brauchen” (2020) und zusammen mit Verena Kreilinger und Winfried Wolf  “Corona, Krise, kapital – Eine soldiarische Alternative in den Zeiten der Pandemie” (2020).

Eine Veranstaltung von Sachsen im Klimawandel, BUND Leipzig, Attac Dresden in Kooperation mit der RLS Sachsen

Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.