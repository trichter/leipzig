---
id: "278361794651481"
title: "Lebenslänglich hinter Gittern: Der Leipziger Zoo – eine moralische
  Unrechtsinstitution"
start: 2023-06-23 19:00
locationName: Pöge-Haus e.V.
address: Hedwigstraße 20, Leipzig
link: https://www.facebook.com/events/278361794651481/
image: 354065758_297269165959798_4984691119179042295_n.jpg
isCrawled: true
---
Der Vortrag gibt einen Überblick über die Geschichte des Zoos, von seinen Anfängen zu Beginn des 19. Jhdts bis zu seiner Gegenwart. Insbesondere geht um die vier argumentativen Säulen, mit denen Zoos ihre Existenz rechtfertigen: die Behauptung, sie leisteten einen wesentlichen Beitrag zum Artenschutz, zu wissenschaftlicher Forschung sowie zu Bildung und Erholung naturentfremdeter Großstädter. Selbstredend geht es auch um die Tiere selbst, die in den Zoos gefangengehalten und zur Schau gestellt werden. Letztlich werden Gedanken vorgestellt, was getan werden kann und muß, die moralische Unrechtsinstitution „Zoo“ einem Ende näherzubringen.

Ein besonderer Blick wird auf die kolonial-rassistischen Stereotype geworfen, die bis heute im Leipziger Zoo gepflogen werden.

Referent: Dr. Colin Goldner, Klinischer Psychologe und Autor zahlreicher Publikationen zu Tierrechtsthemen.