---
id: "527038119480389"
title: Marx für Einsteiger*innen
start: 2023-05-06 10:00
locationName: Pöge-Haus e.V.
address: Hedwigstraße 20, Leipzig
link: https://www.facebook.com/events/527038119480389/
image: 318893732_540121031091322_4320658684278047808_n.jpg
isCrawled: true
---
06.05.2023, 10:00 - 07.05.2023, 17:30 Uhr

Mit Almut Woller (macht Demokratiepädagogik und forscht zu Räten) und Matthias Roeskens (beschäftigt sich mit Politischer Theorie und macht Asylverfahrensberatung)

Vor über 150 Jahren erschien der erste Band “Das Kapital - Kritik der politischen Ökonomie” von Karl Marx. Er blieb seit jeher aktuell. In einem Wochenend-Seminar wollen wir mit einer Gruppe von ca. 15 Personen gemeinsam die zentralen Kategorien entwickeln, die grundlegend für das Verständnis des ersten Bandes sind und gleichzeitig bereits zu einer fundierteren Kapitalismuskritik befähigen. Es ist dabei unwichtig, ob die Teilnehmenden schon einige Kapitel gelesen oder überhaupt kein Vorwissen haben: wer Lust hat, kann mitmachen.

Wir erarbeiten die Inhalte gemeinsam mittels eines 20-seitigen Readers mit Auszügen aus dem Originaltext. Während des Wochenendes gibt es ausreichend Zeit, um die Ausschnitte aus den ersten zehn Kapiteln zu lesen, zu diskutieren und die Erkenntnisse gemeinsam festzuhalten. Außerdem gibt es von uns anschauliche Inputs zu relevanten Inhalten, die keinen Platz im Reader gefunden haben. Wir arbeiten dementsprechend mit vielfältigen Methoden, mitunter stille Lesezeiten, Kleingruppenarbeit, gemeinsame Diskussionen, Visualisierungen und Spiele. 

Wir wünschen uns eine intensive Zusammenarbeit der Teilnehmenden untereinander und mit uns. Wir bemühen uns um eine respektvolle und fehlertolerante Diskussionsatmosphäre. Am Ende des Seminars haben die Teilnehmenden einen guten Überblick über die ersten zehn Kapitel des Kapitals und einen Ausblick auf das was darauffolgt, haben Erfahrung im aktiven Umgang mit dem Text, und haben ein Verständnis davon, was es bedeutet, sich Marx' Werk kritisch zu nähern.

Angedachte Zeiten sind: Samstag und Sonntag jeweils von 10-17.30 Uhr mit je einer Mittagspause und flexiblen kleineren Pausen.

Anmeldung bis 28.04. per mail: anmeldung@rosalux-sachsen.de