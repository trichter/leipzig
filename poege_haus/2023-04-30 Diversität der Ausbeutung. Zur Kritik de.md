---
id: "751238883075772"
title: Diversität der Ausbeutung. Zur Kritik des herrschenden Antirassismus'
  Buchvorstellung & Diskussion
start: 2023-04-30 19:00
locationName: Pöge-Haus e.V.
address: Hedwigstraße 20, Leipzig
link: https://www.facebook.com/events/751238883075772/
image: 339733233_233152355914027_8354684393173494760_n.jpg
isCrawled: true
---
Von Antidiskriminierungsstellen bis zur radikalen Linken wird ein liberaler Rassismusbegriff vertreten, der vor allem auf Repräsentation, Inklusion und Diversität setzt. Wie Klasse und Rasse zusammenhängen, wird aktuell so gut wie nicht diskutiert. Das Buch bietet eine politische Intervention in die aktuelle Debatte um strukturellen und institutionellen Rassismus und präsentiert Alternativen zum liberalen Antirassismus, indem ein marxistischer Rassismusbegriff in Theorie und Praxis vorgestellt wird.