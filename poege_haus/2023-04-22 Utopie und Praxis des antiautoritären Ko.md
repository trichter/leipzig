---
id: "748754743575007"
title: Utopie und Praxis des antiautoritären Kommunismus
start: 2023-04-22 10:00
end: 2023-04-23 16:00
locationName: Pöge-Haus e.V.
address: Hedwigstraße 20, Leipzig
link: https://www.facebook.com/events/748754743575007/
image: 337236790_877743956642824_9123178972597553266_n.jpg
isCrawled: true
---
Mit André Kistner und Sebastian Jordan (Initiative Demokratie Arbeitszeitrechnung), Simon Sutterlütti (Assoziation Kritik Utopie und Transformation, Autor), Simon Schuster (Jurist, Autor)

Mit dem Slogan „System Change, not Climate Change“ hat die Klimabewegung eine Erkenntnis und Forderung auf den Punkt gebracht, die immer mehr Menschen einleuchtet: Der Kapitalismus funktioniert mehr schlecht als recht und fährt den Planeten und “die Wirtschaft” gegen die Wand. Es ist (schon lange) Zeit, darüber nachzudenken, wie wir unsere Gesellschaft strukturieren müssten, damit der Ausstieg aus der kapitalistischen Produktionsweise und dem autoritären Staat, der den Laden gerade noch so zusammenhält, gelingen kann.
Wie sieht dann eine tragfähige Alternative aus? Einerseits kann die politische Linke auf eine sozialistische Tradition zurückgreifen, die so alt ist wie die kapitalistischen Gesellschaften selbst. Andererseits zeigen die Erfahrungen des autoritären Staatssozialismus, wie schnell sich neue Herrschaftsformen unter dem Deckmantel einer besseren Gesellschaftsordnung etablieren können. Im Wissen um diese Gefahren und im Wissen um das viel zu häufige Scheitern der zahlreichen Kommune- und Räte-Experimente, wollen wir genauer auf die Knackpunkte schauen: Welche Mechanismen und auch welche Institutionen braucht eine postkapitalistische Gesellschaft, die sowohl direkt-demokratisch ist als auch eine gesamtgesellschaftliche Steuerung der (Re-)Produktion leisten kann? Wer entscheidet und wer führt aus? Wer produziert was, warum und wie? Was können wir hierbei von den Fehlern des 20. Jahrhunderts, Rätetradition, Commons-Praxis und heutigen Alternativen in Chiapas und Rojava lernen? 

Anmeldung bis 16.04. unter anmeldung@rosalux-sachsen.de

* Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.