---
id: "860510285498246"
title: Tad der Nachbarschaft - Parkfest im Bürgergarten
start: 2023-05-26 14:00
end: 2023-05-26 21:00
locationName: Pöge-Haus e.V.
link: https://www.facebook.com/events/860510285498246/
image: 349071093_903502670739493_1556608202088810109_n.jpg
isCrawled: true
---
TAG DER NACHBARSCHAFT - PARKFEST
Freitag, 26.Mai | 14:00 – 21:00 Uhr
Ort: Bürgergarten Meißnerstr. 50

----english version below-----
In diesem Jahr präsentieren wir Euch zum Tag der Nachbarschaft ein buntes Programm aus Musik, Flohmarkt, Tischtennis, Kinderspiele, Küfa und weitere Mitmachangebote. Wir möchten damit ein Zeichen für mehr Solidarität innerhalb der Nachbarschaft setzen und laden alle herzlich zum gegenseitigen Kennenlernen und Vernetzen ein. Kommt vorbei, bringt Freund*innen und Familie mit und genießt einen fröhlichen Nachmittag im Neustädter Bürgergarten!

In Kooperation mit dem Bürgerverein Neustädter Markt e.V.

-----------

On May 26th is Neighborhood Day. Pöge-Haus and the Neustädter Markt e.V. are happy to invite you to celebrate it together in the Bürgergarten (Meißner Straße 42).
From 14:00 until 21:00 there will a colorful programm with concerts, children activities, table tennis, flea market and of course food & drinks!