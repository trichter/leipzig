---
id: "226608563103698"
title: QUEER LOOKS - Gruppenaustellung mit Workshop
start: 2023-04-18 19:00
end: 2023-04-23 21:30
locationName: Pöge-Haus e.V.
address: Hedwigstraße 20, Leipzig
link: https://www.facebook.com/events/226608563103698/
image: 336177419_579213277496658_2628116786417392093_n.jpg
isCrawled: true
---
18.4. Dienstag Vernissage: 19-22h
19.4. Mittwoch 19-21h
21.4. Freitag Workshop: 14-18h
22.4. Samstag 19-21h
23.4. Sonntag 19-21h

Die Gruppenausstellung „Queer Looks“ vereint multimediale Kunst von Sophie Klock, Eden Jetschmann, Lilian Mauthofer und toni, allesamt nicht-binäre Künstler*innen, die sich der Visualisierung von queeren Lebensentwürfen widmen. Dabei kommen traditionelle und futuristische Ästhetiken zusammen und verweisen auf die reale wie auch fiktive zeitliche Repräsentanz der LGBTQ*-Gemeinschaft. In den Werken werden außerdem unterschiedliche Formen queerer Sichtbarkeit, der sie in der Gesellschaft ausgesetzt sind, kritisch verhandelt und angeeignet. In der Ausstellung werden hauptsächlich analoge Fotografien gezeigt, es wird aber auch verschiedene Videokunst, installative Arbeit und eine Performance zu sehen geben. So bildet sich ein autonomes Werkarchiv des Widerstands, mit dem die LGBTQ*-Gemeinschaft ihren Platz in der Gesellschaft beansprucht.

Toni wird im Rahmen der Ausstellung am Freitag einen Performance-Workshop anbieten, bei dem Anfänger*innen und Interessierte einen Fuß in die Welt der performativen Kunstform setzen können. Der Workshop ist auf Spendenbasis und wird im Saal des Pöge-Hauses stattfinden. Anmeldung gerne hier oder auf Instagram @queer.looks