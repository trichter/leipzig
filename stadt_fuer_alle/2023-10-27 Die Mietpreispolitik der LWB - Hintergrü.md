---
id: 2094-123
title: Die Mietpreispolitik der LWB - Hintergründe, Notwendigkeiten, Konsequenzen
start: 2023-10-27 18:00
end: 2023-10-27 20:00
address: Erich-Schilling-Saal im Volkshaus Karl-Liebknecht-Str. 30/32 5. Etage
  Leipzig  04107
link: https://leipzig-stadtfueralle.de/mc-events/die-mietpreispolitik-der-lwb-hintergruende-notwendigkeiten-konsequenzen/
isCrawled: true
---
Die Mietererhöhungen des kommunalen Wohnungsunternehmens LWB erhitzen die Leipziger Gemüter. Das Unternehmen selbst verteidigt seine jährlich über 6000 Erhöhungen auf Grundlage des qualifizierten Mietspiegels als solidarisches Modell…