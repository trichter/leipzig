---
name: Leipzig - Stadt für Alle
website: http://leipzig-stadtfueralle.de/
email: kontakt@leipzig-stadtfueralle.de
pgp:
    url: http://www.leipzig-stadtfueralle.de/files/stadtfueralle-Leipzig_0x9F3E0419_pub.asc
    fingerprint: FD60 1402 5028 7E3F 4E76 F89A 94E8 0A3D 9F3E 0419
scrape:
    source: ical
    options:
        url: http://leipzig-stadtfueralle.de/feed/my-calendar-google/
    # source: facebook
    # options:
    #     page_id: 392238310834246
---
„Stadt für alle“ setzt sich für eine soziale und demokratische Stadtentwicklung ein.