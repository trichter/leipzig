---
id: "164764496179718"
title: Einstiegsvortrag Pro Choice
start: 2023-04-21 18:00
locationName: Subbotnik
address: Vettersstraße 34a, Chemnitz
link: https://www.facebook.com/events/164764496179718/
isCrawled: true
---
Subbotnik, Chemnitz - 21.4. - 18 Uhr

Pro Choice Leipzig präsentiert in einem Einstiegsvortrag einen Einblick in die Kämpfe der Pro Choice Bewegung. Bereits seit über hundert Jahren fordern Feministinnen in Deutschland die Abschaffung des §218, der Schwangerschaftsabbrüche unter Strafe stellt. Was ist seit dem passiert? Wie sehen die Kämpfe vor der eigenen Haustür, genauer gesagt im Erzgebirge aus? Auch im internationalen Kontext gibt es immer wieder Fortschritte, aber auch herbe Rückschläge im Kampf um körperliche und sexuelle Selbstbestimmung.

Der Vortrag richtet sich an Einsteiger*innen, die noch wenig mit dem Thema vertraut sind und Lust haben, sich damit zu befassen.

Die Veranstaltung findet in Kooperation mit weiterdenken – Heinrich-Böll-Stiftung Sachsen und den Kretas Chemnitz statt.