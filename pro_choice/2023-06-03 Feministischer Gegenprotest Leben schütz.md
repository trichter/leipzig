---
id: "638390838110925"
title: "Feministischer Gegenprotest: Leben schützen - Abtreibung legalisieren!"
start: 2023-06-03 12:00
locationName: Annaberg-Buchholz
link: https://www.facebook.com/events/638390838110925/
image: 341164841_605735898133194_2992897927299523316_n.jpg
isCrawled: true
---
Startpunkt unserer Demo: Parkplatz „Sonderbusstand“ in der Geyersdorfer Str. hinter dem Friedhof (ACHTUNG: Hier kann man entsprechend nicht parken!)
Wir sammeln uns dort ab 13 Uhr. Der erste Schnittpunkt mit der Fundi-Route ist auch sehr nahe in der Geyersdorfer Str. / Lindenstr.
Der Endpunkt ist der untere Bhf. in Annaberg-Buchholz. 

Anreiseinformationen: https://prochoiceleipzig.wordpress.com/2023/05/15/anreise/
letzte Demoinfos + Tipps (inkl. Aktionskarte): https://prochoiceleipzig.wordpress.com/2023/05/29/tipps-und-infos-fur-den-demotag/

WIR WERDEN HIER IM EVENT IMMER AKTUELLE INFOS ZU GENAUEN UHRZEITEN, ANREISE & CO BEREITSTELLEN!

AUFRUF:
Seit über einem Jahrzehnt versammeln sich in Annaberg-Buchholz Abtreibungsgegner*innen zu einem sogenannten "Schweigemarsch für das Leben". Ihr gewählter Startpunkt ist dabei wie immer der Parkplatz der Erzgebirgsklinik, die als letzte im gesamten Landkreis überhaupt noch Abtreibungen durchführt. Ihr vorgeschobener "Lebensschutz" ist dabei besonders zynisch, könnten ihre Positionen doch lebensfeindlicher nicht sein. Eines ihrer Hauptziele ist das vollständige Verbot von Abtreibungen, ohne jede Ausnahme. Genau durch solchen Verbote ausgelöste unsichere Abtreibungen sterben jedes Jahr etwa 39000 Frauen und andere Betroffene. Das sind fast doppelt so viele, wie Annaberg-Buchholz überhaupt Einwohner*innen hat. Unzählige weitere leiden unter gesundheitlichen Folgen oder juristischer Verfolgung. 
Doch das ist längst nicht alles, wofür die Teilnehmenden stehen. Seit jeher nutzen die Redner*innen des Schweigemarschs die Bühne für ihre konservativen und heteronormativen Inhalten und verbreiten weitgehend ungestört ihre frauen- und queerfeindliche, antisemtische und nationalistische Haltung. Mit diesen Positionen stehen die christlichen Fundamentalist*innen inhaltlich Schulter an Schulter mit Parteien wie AfD und CDU und anderen rechten, sowie antifeministischen Strömungen. Diese Überschneidungen zeigen sich sowohl bei den Teilnehmenden, als auch bei der Auswahl der Redner*innen. Diese regressive Querfront direkt vor unserer Haustür können und wollen wir nicht unwidersprochen lassen. 
Wir finden ohnehin: 13 Jahre Schweigemarsch sind mehr als genug! Im Christentum gilt die Zahl 13 nicht umsonst als Unglückszahl - dieses Jahr wollen wir den Fundis ein für alle mal beweisen, wieso das so ist! 

Kommt mit uns am 03.06. nach Annaberg-Buchholz. Seid laut, seid wütend, seid viele.
Informiert euch auf unseren Kanälen zur sachsenweiten Anreise und weiteren wichtigen Infos! 

Und nicht vergessen - kein Gott, kein Staat, kein Patriarchat.