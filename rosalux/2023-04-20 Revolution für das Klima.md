---
id: CL6RK
title: Revolution für das Klima
start: 2023-04-20 19:00
end: 2023-04-20 21:00
locationName: Pöge-Haus
address: "Hedwigstraße 20, 04315 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/CL6RK/
teaser: Warum wir eine ökosozialistische Alternative brauchen
isCrawled: true
---
Mit *Christian Zeller* (Geograph, Paris Lodron Universität Salzburg)

Eine Veranstaltung der RLS Sachsen in Kooperation mit dem Bidungsprojekt Sachsen im Klimawandel, BUND Leipzig und Attac Dresden

Die Klimakatastrophe findet statt. Die Wirtschaftskrise vermindert die Treibhausgasemissionen, aber nur ungenügend und nicht nachhaltig. Die Erderhitzung bleibt zentrale Herausforderung. Millionen von Menschen müssen ihre Heimat verlassen. Die Menschheit befindet sich in einem beängstigenden Rennen gegen die Zeit. Die Regierungen und die großen Konzerne weigern sich, wirksam gegen die Bedrohungen zu handeln. Profite und Wettbewerbsfähigkeit gehen vor. Umwelt-, Gesundheits- und Wirtschaftskrisen verdichten sich.

Christian Zeller macht deutlich, warum es eine Revolution für das Klima und das Leben der Menschen braucht und wie diese aussehen kann. Die Produktion, der Verkehr und das Finanzsystem sind grundlegend umzubauen. Die gesellschaftliche Infrastruktur - Gesundheit, Pflege, Sorge und Bildung - sind auszubauen. Hierfür braucht es eine gemeinsame und riesige gesellschaftliche Mobilisierung. Dieses Buch zeigt den Weg in Richtung einer Gesellschaft, die weniger und anders produziert, gerecht teilt und in der die Menschen gemeinsam entscheiden. Das ist eine ökosozialistische Gesellschaft.

*Christian Zeller* ist Professor für Wirtschaftsgeographie an der Paris Lodron Universität Salzburg. Er engagiert sich in ökosozialistischen Kontexten und ist Autor mehrer Bücher. Zuletzt erschien von ihm “Revolution für das Klima – Warum wir eine ökosozialistische Perspektive brauchen” (2020) und zusammen mit Verena Kreilinger und Winfried Wolf “Corona, Krise, kapital – Eine soldiarische Alternative in den Zeiten der Pandemie” (2020).

*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*