---
id: T5O92
title: Phantombilder. Die Polizei und der verdächtige Fremde.
start: 2023-04-29 16:00
end: 2023-04-29 17:30
locationName: linXXnet
address: "Brandstraße 15, 04277 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/T5O92/
teaser: Buchmesse Leipzig
isCrawled: true
---
Mit *Georgiana Banita* (Universität Bamberg), Moderation: *Sophie Petthus* (Humangeographin)

Eine Veranstaltung des Abgeordnetenbüros linXXnet und der RLS Sachsen

George Floyd, Michael Brown, Breonna Taylor. Oury Jalloh, Achidi John, Christy Schwundeck. Ernst Haase, Hans-Jürgen Rose, Mario Bichtemann. Sie – und viel zu viele andere – sind Opfer von tödlicher Polizeigewalt, in Amerika und in Europa. Sie sind Schwarz, oder migrantisch, oder weichen auf andere Weise von der gesellschaftlichen Norm ab. 

Georgiana Banita zeigt in ihrer kulturhistorischen Annäherung, wie und warum das wirkmächtige Phantombild des potenziell gefährlichen Fremden schon immer Zielscheibe westlicher Polizeiapparate war, ideologisches Fundament eines polizeilichen Generalverdachts vor allem gegenüber Schwarzen Menschen und People of Color. Erste Reformen zeigen: Mit mehr nicht-weißen Polizist\*innen allein ist es nicht getan, denn der Rassismus ist strukturell. Ob es um den Gebrauch von Schusswaffen, Racial Profiling, Rasterfahndung oder KI-gestützte Kriminalitätsprognosen geht, um Abschiebung, Grenz- oder Infektionsschutz: Die Abwehr des (vermeintlich) Fremden ist Logik und Praxis polizeilicher Arbeit. Ein nachhaltiger Mentalitätswandel ist nötig, um die toxische Cop Culture zu überwinden und dringend notwendige Veränderungen für eine neue Polizeikultur zu ermöglichen.

*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*