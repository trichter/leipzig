---
id: 49DDU
title: Feminist*in Sein - Selbsterfahrung Reloaded!
start: 2023-05-06 15:00
end: 2023-05-06 20:00
locationName: Conne Island
address: "Koburger Straße 3, 04277 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/49DDU/
teaser: "REIHE: Skillsharing Leipzig"
isCrawled: true
---
Eine Veranstaltung von Skillsharing Leipzig in Kooperation mit der RLS Sachsen

„Feminism“ – Jogginghosen, GRL PWR – Socken, Leoprint, Glitzer und Haare nicht nur auf dem Kopf – rein von außen betrachtet scheint es viele Feminist\*innen in Leipzig zu geben. Doch was macht eigentlich eine gute Feminist\*in aus?

Zuerst werden wir uns in einem kurzen Vortrag die Geschichte der feministischen Bewegung inklusive ihrer Selbsterfahrungspraxis (sogenannte consciousness raising groups) anschauen. Wir schlagen anschließend den Bogen von der damaligen Praxis der Selbsterfahrungsgruppen hin zu den aktuellen feministischen Bewegungen und der Frage, wie die einzelnen Buchstaben in Flinta\* zusammengekommen sind und was sie bedeuten. Im Anschluss daran probieren wir die Praxis von damals einfach mal aus und schauen, ob wir damit noch irgendetwas anfangen können. Unsere Ergebnisse werden wir auf Gemeinsamkeiten hin untersuchen und gemeinsam diskutieren, ob wir daraus eine aktuelle feministische Theorie basteln können.

Der Vortrag wird ungefähr 30 min dauern, dann haben wir 30 min Zeit für Fragen und Diskussion. Nach einer kurzen Pause fangen wir mit den Selbsterfahrungsgruppen an. Dafür haben wir nochmal zwei Stunden Zeit. Bringt bitte etwas zu schreiben mit.



*In Kooperation mit der Rosa-Luxemburg-Stiftung: Gesellschaftsanalyse und politische Bildung e.V.*