---
id: VIFKA
title: Die Türkei nach den Wahlen
start: 2023-05-15 19:00
end: 2023-05-15 21:00
locationName: Vortragssaal der Universitätsbibliothek Leipzig
address: "Beethovenstraße 6, 04107 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/VIFKA/
isCrawled: true
---
Mit *Markus Dressler* (Professor für Moderne Türkeiforschung, Universität Leipzig),* Lena Sanye Güngör* (Psychologin und Ethikerin, Vorsitzende RLS Thüringen, MdL Thüringen), *Horst Junginger* (Professor für Religionswissenschaft und Religionskritik, Universität Leipzig, Co-Vorsitzender RLS Sachsen), *Dr. phil. Nil Mutluer* (Wissenschaftliche Mitarbeiterin an der Unibersität Leipzig, ehemalige Direktorin des Instituts für Soziologie der Nişantaşı-Universität in Istanbul), *Dr. phil. Günter Seufert* (Leiter des Centrums für angewandte Türkeistudien bei der Stiftung Wissenschaft und Politik in Berlin)

Eine Veranstaltung des Religionswissenschaftlichen Instituts der Universität Leipzig in Kooperation mit der RLS Sachsen

Umfragen zufolge wird es bei den Parlaments- und Präsidentenwahlen in der Türkei am 14. Mai 2023 ein sehr knappes Ergebnis geben. Es könnte sogar sein, dass mit dem sozialdemokratischen Oppositionsführer Kemal Kilicdaroglu ein Alevit das höchste Staatsamt in dem vom sunnitischen Islam bestimmten Land übernimmt.

Wie ist das Ergebnis zu bewerten und was ergibt sich aus ihm für die Politik und Zivilgesellschaft in Deutschland? Diese und andere Fragen diskutieren wir mit unseren Gästen am Tag nach den Wahlen.



*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.

*