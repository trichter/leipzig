---
name: Rosa-Luxemburg-Stiftung Sachsen
website: https://sachsen.rosalux.de
email: info@rosalux-sachsen.de
scrape:
    source: rosalux
    options:
        city: Leipzig
        eventUrl: "https://sachsen.rosalux.de/veranstaltung/es_detail/:id/"
---