---
id: JKN4D
title: Antonio Gramsci – Kulturelle Hegemonie &amp; Stadtteilarbeit
start: 2023-04-30 17:00
end: 2023-04-30 18:30
locationName: Stadtteilladen Lixer e.V.
address: "Pörstener Straße 9, 04229  "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/JKN4D/
teaser: Buchmesse Leipzig
isCrawled: true
---
Mit *Uwe Hirschfeld* (Politologe, Sozialpädagoge)

Eine Veranstaltung des Abgeordnetenbüros linXXnet, des Lixer e.V. und der RLS Sachsen

Antonio Gramscis Konzept der Hegemonie wird vor allem in linken Kreisen breit rezipiert. Nach einer kurzen Einführung zu den Grundzügen der Philosophie Gramscis, seinem Leben und Wirken, sprechen wir über die Möglichkeiten, wie seine Überlegungen in die aktuelle Praxis linker Stadtteilarbeit eingebunden und weitergedacht werden können.

Uwe Hirschfeld ist Politologe und Sozialpädagoge und hat zahlreiche Texte zu dem italienischen Schriftsteller und Philosophen Antonio Gramsci verfasst. Bis 2020 lehrte er an der Evangelischen Hochschule Dresden politische Theorie und Bildung.

*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*