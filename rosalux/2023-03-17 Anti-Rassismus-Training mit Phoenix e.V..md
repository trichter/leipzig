---
id: 2S3RI
title: Anti-Rassismus-Training mit Phoenix e.V.
start: 2023-03-17 10:00
end: 2023-03-19 17:00
locationName: Leipzig
address: "genauer Ort wird nach Anmeldung bekannt gegeben, "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/2S3RI/
isCrawled: true
---
Eine Veranstaltung des A und V e.V. in Kooperation mit der RLS Sachsen

**Die Teilnehmer\*innenzahl ist begrenzt. Anmeldungen bitte an: **[**phoenixworkshop@posteo.de**](<mailto:phoenixworkshop@posteo.de>)

Mit dem 3-tägigen Workshop wollen wir Bewusstsein und Verständnis für gesellschaftliche Bedingungen und die systematische Einschreibung des rassistischen Systems auf institutioneller, aber auch emotional-körperlicher Ebene schaffen.

Das Training hilft, die Eingebundenheit der eigenen Persönlichkeit in rassistische Denk- und Gefühlsmuster zu erkennen und einen Bogen zu schlagen zu der rassistischen Prägung in der Sozialisation. Im Alltag werden diese Prägungen durch Medien, durch Zusammensein in Familie, Beruf und Freund\*innenkreis immer wieder bestätigt. Die politische Alltagskultur verstärkt zudem diese Klischees. Das Training stärkt die eigene Wahrnemung, den Kontakt zum eigenen Ich und stellt letztendlich die Frage: Wie kann ich wirkungsvoll etwas gegen Rassismus unternehmen?

Gemeinsam werden erste Schritte und Möglichkeiten gesucht. Dabei arbeiten die Trainer\*innen mit verschiedenen Medien und Methoden, wie Einzel- und Gruppengespräch, Rollenspiel, Videos und anderem. Die Teilnehmer\*innen sollen dazu angehalten werden, sich nach dem Konzept der Critical Whiteness, des kritischen Weißseins, mit ihrer Rolle im System des Rassismus auseinanderzusetzen. Gesellschaftlich können wir auf Dauer nur etwas verändern, wenn viele Menschen anfangen, das kleine und das große Geflecht des Rassismus zu erkennen und bereit sind, sich zu fragen: Wer bin ich als Weiße\*r und wie fühlt sich das an?



*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*

