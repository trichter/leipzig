---
id: MD4TP
title: "Für eine feministische Organisationskultur "
start: 2023-04-27 18:00
end: 2023-04-27 20:00
locationName: Erich-Zeigner-Haus
address: "Zschochersche Straße 21, 04229 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/MD4TP/
teaser: Erfahrungen aus Schweden
isCrawled: true
---
Wenn wir wollen, dass linke Politik auf die Fragen und Bedürfnisse der Mehrheit der Menschen antwortet, dann müssen ihre Stimmen in unseren Organisationen präsent sein. Davon sind wir heute leider noch weit entfernt. Denn auch linke Gruppen sind keine Inseln, sondern von denselben Machtverhältnissen geprägt, die in der Gesellschaft bestehen. Das hat nicht zuletzt die Debatte um #linkemetoo gezeigt. Und doch haben linke Organisationen den Anspruch, stets einen Schritt voranzugehen.

Es geht um nichts weniger als die Frage, wie politische Organisationen Formen schaffen können, die Frauen und anderen benachteiligten Gruppen eine tatsächlich gleichberechtigte, diskriminierungsfreie Teilhabe ermöglichen, und auf diese Weise selbst demokratischer werden. Der strukturelle Sexismus braucht Antworten auf der strukturellen Ebene. Wie kann das gehen?

Es ist keine einfache Aufgabe und benötigt kontinuierliche Arbeit, sowohl an Strukturen als auch an eingelernten Umgangsweisen. Die schwedische Vänsterpartiet (dt. Linkspartei), hat deshalb das [Handbuch zum innerparteilichen Feminismus ](<https://www.rosalux.de/publikation/id/46514>)entwickelt, das klare Kommunikations- und Verhaltensregeln aufzeigt. Aber wie werden diese Buchstaben zur Realität? In welche Debatten sind sie eingebunden? Welche Erfolge konnten so schon erzielt werden? Welche Herausforderungen stellen sich damit im Alltag?

Darüber sprechen wir mit **Caroline Gustafsson**, Vorsitzende der Frauenorganisation der Vänsterpartiet.

Mit Konsekutivübersetzung Englisch-Deutsch.

Die Veranstaltung ist eine Kooperation der Rosa-Luxemburg-Stiftung und dem Erich-Zeigner-Haus e.V.