---
id: LQHM1
title: Woman* Life Freedom
start: 2023-03-23 16:00
end: 2023-04-01 20:00
locationName: A&amp;O Kunsthalle
address: "Brandenburger Straße 2, 04103 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/LQHM1/
teaser: A Protest Art Intervention
isCrawled: true
---
Eine Veranstaltung von Woman\* Life Freedom Leipzig in Kooperation mit der A&O Kunsthalle und der RLS Sachsen

Die Gruppe Woman\* Life Freedom Leipzig eröffnet, in Zusammenarbeit mit der Rosa-Luxemburg-Stiftung Sachsen und der A&O Kunsthalle, im März 2023 eine Ausstellung zum Thema Protestkunst. Soziale Bewegungen produzieren Werke wie Schilder, Transparente, Plakate und andere gedruckte Materialien, die verwendet werden, um ein bestimmtes Anliegen oder eine bestimmte Botschaft zu vermitteln. Protestkunst kann zum Nachdenken anregen, Dialoge schaffen und Beziehungen herstellen. Unser Ziel ist es, durch die Ausstellung das Bewusstsein für die von Frauen\* geführte Revolution im Iran zu schärfen, indem wir die Werke einiger iranischer Künstler\*innen zeigen, die innerhalb und außerhalb Irans, ihrem Protest Ausdruck verliehen haben.

*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*