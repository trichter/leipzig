---
id: 1I6LG
title: Rich Kids - Von Influencer*innen, Business-Coaches und anderen falschen
  Finanz-Prophet*innen
start: 2023-03-16 19:00
end: 2023-03-16 21:00
locationName: Schaubühne Lindenfels
address: "Karl-Heine-Straße 50, 04299 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/1I6LG/
isCrawled: true
---
Mit *Wolfgang M. Schmitt* (Filmkritiker, Podcaster und Autor) und *Ole Nymoen* (freier Journalist, Podcaster und Autor)

Das Versprechen der Influencer\*innen, egal ob explizit oder implizit propagiert, lautet, dass jede\*r es schaffen kann: Jede\*r kann berühmt, reich und schön werden. Man muss sich nur ordentlich anstrengen. Diese meritokratische Ideologie ist jedoch nicht nur bei jenen verbreitet, die Lippenstifte und Fitness-Drinks anpreisen, längst hat sich eine neue Industrie von Business-Coaches, Profiler\*innen und Investor\*innen-Gurus entwickelt, die auf den sogenannten Sozialen Medien wächst und gedeiht. Illustre und teils sinistere Gestalten werben für Drop-Shipping-Modelle, Glücksspiele, Arbitrage-Gewinne und riskante Immobilien-Deals.

Ole Nymoen und Wolfgang M. Schmitt, die Autoren von „Influencer. Die Ideologie der Werbekörper“, zeichnen in ihrem Vortrag das Phänomen nach und analysieren die ideologischen Implikationen.



*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*

