---
id: Q3F3T
title: Von einem, der im Zuchthaus Mathematik studierte - Gespräche mit Hans
  Lauter am runden Tisch
start: 2023-04-27 19:00
end: 2023-04-27 20:30
locationName: Stadtteilladen Lixer e.V.
address: "Pörstener Straße 9, 04229  "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/Q3F3T/
teaser: Buchmesse Leipzig
isCrawled: true
---
Mit *Michael Lauter* (Diplom-Agrarwissenschaftler, Diplom-Gesellschaftswissenschaftler)

Eine Veranstaltung des Abgeordnetenbüros linXXnet, des Lixer e.V., des Antifa-Tresen und der RLS Sachsen



Prof. Hans Lauter (1914 – 2012) war antifaschistischer Widerstandskämpfer, DDR-Funktionär, Mitglied der PDS und zusammen mit Esther Bejarano Ehrenvorsitzender des VVN - BdA.

1933 wurde er kurzzeitig im KZ Sachsenburg interniert, nach seiner Entlassung übernahm er die illegale Leitung des Kommunistischen Jugendverband Deutschland für den Bezirk Leipzig. Für seine Tätigkeit wurde er 1936 von der nationalsozialistischen Justiz wegen Hochverrats zu 9 Jahren Zuchthaus verurteilt. Seine Strafe verbüßte er im Zuchthaus Zwickau und in den berüchtigten Moorlagern im Emsland, vom (über)Leben in diesen Lagern erzählt das bekannte Häftlingslied. „ Die Moorsoldaten“. Seine letzten Lebensjahre verbrachte er in Leipzig-Grünau. Die Geschichten, die Hans Lauter dort aus seinem Leben erzählt, dokumentierte Michael Lauter in dem Buch „Von einem, der im Zuchthaus Mathematik studierte“.

Michael Lauter ist gelernter Rinderzüchter, Diplom-Agraringenieur und Diplom-Gesellschaftswissenschaftler und freiberuflich als Dozent tätig.



*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*