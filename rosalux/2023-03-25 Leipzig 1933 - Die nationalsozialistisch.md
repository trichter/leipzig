---
id: X8AYV
title: Leipzig 1933 - Die nationalsozialistische Machtdurchsetzung
start: 2023-03-25 11:00
end: 2023-03-25 13:00
locationName: Bundesverwaltungsgericht
address: "Simsonplatz 1, 04107 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/X8AYV/
teaser: Zum 90. Jahrestag der Machtübertragung
isCrawled: true
---
Mit *Daniela Schmohl* (Historikerin, RLS Sachsen)

Eine Veranstaltung der VVN-BdA Leipzig e.V. und der RLS Sachsen

Treffpunkt: Vor dem Bundesverwaltungsgericht, Simsonplatz 1, 04107 Leipzig



Der Stadtrundgang verbindet Orte im Zentrum Leipzigs mit der Machtübertragung an die Nationalsozialisten im Januar 1933 und den darauffolgenden Monaten des Terrors gegen politische Gegner\*innen und jüdische Einwohner\*innen sowie der Machtdurchsetzung der Nationalsozialisten. Von der „Reichs-Nein-Stadt“, über die Bücherverbrennung bis zum Reichstagsbrandprozess werden unterschiedliche Stationen des Jahres 1933 im Stadtbild verortet.

Dauer ca. 2 Stunden



*In Kooperation mit der Rosa-Luxemburg-Stiftung: Gesellschaftsanalyse und politische Bildung e.V.*