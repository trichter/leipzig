---
id: KW5GR
title: Atlas der Migration
start: 2023-03-09 19:00
end: 2023-03-09 21:00
locationName: Kunstraum D21
address: "Demmeringstraße 21, 04177  "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/KW5GR/
isCrawled: true
---
Mit *Johanna Bussemer* (Referatsleiterin Europa, Rosa-Luxemburg-Stiftung), Moderation: *Steven Hummel* (Politikwissenschaftler, RLS Sachsen)

Eine Veranstaltung der RLS Sachsen und des Sächsischen Flüchtlingsrates

Der neue Atlas der Migration 2022 erklärt auf 25 Doppelseiten mit zahlreichen Grafiken Daten und Fakten zu Menschen in Bewegung in Deutschland und weltweit. Aktuelle Beiträge zu Flucht und Klima, dem Krieg in der Ukraine, aber auch zur Kettenmigration am Beispiel von Arbeit in der Pflege und in der Landwirtschaft nähern sich anschaulich der alltäglichen Realität von Migration.

**Johanna Bussemer** ist Leiterin des Referates Europa im Zentrum Internationaler Dialog der Rosa-Luxemburg-Stiftung. Zuvor war die studierte Politologin Referentin für Außenpolitik bei der Fraktion DIE LINKE im Deutschen Bundestag. 2021 erschien ihr gemeinsam mit Katja Kipping verfasstes Buch “Green New Deals als Zukunftspakt. Die Karten neu mischen”.

Die Veranstaltung findet als Begleitprogramm zur [Ausstellung «10 Views on Migration»](<https://sachsen.rosalux.de/veranstaltung/es_detail/81BT4/10-views-on-migration>) statt. Die Ausstellung ist vom 01. bis 11. März im Kunstraum D21 (Demmeringstraße 21, 04177 Leipzig) zu sehen. Öffnungszeiten: Mittwoch-Samstag jeweils 14 bis 19 Uhr.



*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*

