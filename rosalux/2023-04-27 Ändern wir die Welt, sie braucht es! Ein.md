---
id: MJZPD
title: Ändern wir die Welt, sie braucht es! Eine marxistisch-feministische
  Ansage vom Kollektiv MF3000
start: 2023-04-27 18:00
end: 2023-04-27 19:30
locationName: Interim
address: "Demmeringstraße 32, 04177 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/MJZPD/
teaser: Buchmesse Leipzig
isCrawled: true
---
Mit *Kollektiv MF3000*

Eine Veranstaltung des Abgeordnetenbüros linXXnet, des querverlag und der RLS Sachsen

Teil des Kollektiv MF 3000 sind Alex Wischnewski, Bettina Gutperl, Cordula Trunk, Ines Schwerdtner, Jen Funke-Kaiser, Kerstin Wolter und Lisa Mangold. Die Autor\*innen engagieren sich in unterschiedlichen linken Spektren, von autonomen Gruppen über Partei, Gewerkschaft, Wissenschaft. Sie haben sich zusammengeschlossen für ein gemeinsames politisches Projekt.



*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*