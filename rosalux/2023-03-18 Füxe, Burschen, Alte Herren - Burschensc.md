---
id: 44KEH
title: Füxe, Burschen, Alte Herren - Burschenschaften und ihre Rolle in der Rechten
start: 2023-03-18 15:00
end: 2023-03-18 20:00
locationName: Conne Island
address: "Koburger Straße 3, 04277 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/44KEH/
teaser: "REIHE: Skillsharing Leipzig"
isCrawled: true
---
Eine Veranstaltung von Skillsharing Leipzig in Kooperation mit der RLS Sachsen

Sie sehen aus als wären Sie aus einem Filmset für eine Geschichts-Doku ausgebrochen. Aber nicht nur der Style, auch das Weltbild ist mehr als Oldschool. Was ist eigentlich der Unterschied zwischen einer Studentenverbindung und einer Burschenschaft? Sind die heute überhaupt noch wichtig in der Rechten? Auf solche und ähnliche Fragen versuchen wir in diesem Workshop Antworten zu formulieren. Wir wollen uns in netter Atmosphäre mit Texten auseinandersetzen, die einen Einstieg in das Thema erlauben und uns dann gemeinsam weiterführende Gedanken dazu machen. Es wird um die Geschichte der Burschenschaften gehen und um deren größere und kleinere Strukturen. Gemeinsam werden wir uns z.B. mit dem Nationalismus, Antifeminismus, Rassismus und Antisemitismus insbesondere der Deutschen Burschenschaft (DB) beschäftigen um das Weltbild zu durchblicken, welches deren Tradition prägt. Vielleicht können wir uns auch einen Einblick in die Leipziger Verbindungslandschaft erarbeiten.

*In Kooperation mit der Rosa-Luxemburg-Stiftung: Gesellschaftsanalyse und politische Bildung e.V.*

