---
id: ELPU7
title: 100 Jahre Ministerpräsident Zeigner und sein Wirken in Leipzig
start: 2023-03-21 18:00
end: 2023-03-21 20:00
locationName: Erich- Zeigner Haus e.V.
address: "Zschochersche Straße 21, 04229 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/ELPU7/
isCrawled: true
---
Mit *Manfred Hötzel* (Historiker) und *Nils Franke* (Historiker), Moderation: *Henry Lewkowitz* (Erich-Zeigner-Haus e.V.)

Eine Veranstaltung von Arbeit und Leben Sachsen e.V. und dem Erich-Zeigner-Haus e.V. in Kooperation mit der RLS Sachsen

Erich Zeigners demokratiepolitisches Wirken vor allem zur Zeit der Weimarer Republik soll an diesem Abend ausführlich beleuchtet werden. Sein Einfluss innerhalb der Arbeiter\*innenbewegung des Leipziger Westens sowie seine Leistungen bei dem Aufbau der Gewerkschaften stehen im Mittelpunkt der Vorträge von Zeigner-Biograf Dr. Manfred Hötzel und Dr. Nils Franke. Beide stellen die Ergebnisse ihrer Recherchen vor und stellen sich im anschließenden Gespräch auch dem Publikum.



*In Kooperation mit der Rosa-Luxemburg-Stiftung: Gesellschaftsanalyse und politische Bildung e.V.*