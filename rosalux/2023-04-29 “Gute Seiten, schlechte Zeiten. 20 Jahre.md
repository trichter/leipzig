---
id: 95WEI
title: “Gute Seiten, schlechte Zeiten. 20 Jahre MALMOE in Wien”
start: 2023-04-29 21:00
end: 2023-04-29 23:00
locationName: Interim
address: "Demmeringstraße 32, 04177 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/95WEI/
teaser: Buchmesse Leipzig
isCrawled: true
---
Mit *Anahita und Philipp* (MALMOE-Redaktionskollektiv)

Eine Veranstaltung des Abgeordnetenbüros linXXnet, des Vereins für mediale Qualität und Vielfalt und der RLS Sachsen

MALMOE ist eine Zeitung, die seit über 20 Jahren in Wien erscheint und auch in Leipzig erhältlich ist.

In dieser szenischen Lesung stellen Anahita und Philipp vom Redaktionskollektiv ein Best-of aus 100 Ausgaben MALMOE zusammen. Freut euch auf eine bunte Mischung aus Lustigem, Skurrilem und Schönem, aber auch Ekligem, Abzulehnendem und Utopischem.

Um dem ganzen den richtigen Rahmen zu geben, servieren wir den Zuschauer\*innen Weißwein und frisch zubereitete Kaspressknödel.

*\* Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.

*