---
id: 4TNRC
title: Männerfreundschaften - auf der Suche nach Gefühlen
start: 2023-04-15 15:00
end: 2023-04-15 20:00
locationName: Conne Island
address: "Koburger Straße 3, 04277 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/4TNRC/
teaser: "REIHE: Skillsharing Leipzig"
isCrawled: true
---
Eine Veranstaltung von Skillsharing Leipzig in Kooperation mit der RLS Sachsen

Über eigene Probleme, Unsicherheiten und Ängste gefühlvoll und empathisch zu sprechen fällt vielen Boys häufig schwer. Das macht sich auch in den Freundschaften mit und unter ihnen bemerkbar. Gemeinsam wollen wir in diesem Workshop dem nachspüren, warum das eigentlich so ist, was belastend daran ist und natürlich darüber sprechen, was es bräuchte um aus diesen Mustern ein Stück weit auszubrechen.

Der Workshop ist offen für all gender. Der Fokus liegt auf dem Austausch untereinander und gemeinsamen Diskussionen.



<div id="magicdomid70" aria-live="assertive" class="ace-line"><em><span class="author-a-z67zg6z122zz70zxz90zz87zfoz72zld4z70zz71z i">In </span><span class="author-a-z67zg6z122zz70zxz90zz87zfoz72zld4z70zz71z">Kooperation mit der Rosa-Luxemburg-Stiftung: Gesellschaftsanalyse und politische Bildung e.V.</span></em></div>