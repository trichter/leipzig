---
id: YN4YW
title: Utopie und Praxis des antiautoritären Kommunismus
start: 2023-04-22 10:00
end: 2023-04-23 16:00
locationName: Pöge-Haus
address: "Hedwigstraße 20, 04315 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/YN4YW/
teaser: Rätekommunismus, Commonismus, Chiapas &amp; Rojava
isCrawled: true
---
Mit *Almut Woller* (macht Demokratiepädagogik und forscht zu Räten), *André Kistner* und *Sebastian Jordan* (Initiative Demokratie Arbeitszeitrechnung), *Simon Sutterlütti* (Assoziation Kritik Utopie und Transformation, Autor), *Simon Schuster* (Jurist, Autor)

Mit dem Slogan „System Change, not Climate Change“ hat die Klimabewegung eine Erkenntnis und Forderung auf den Punkt gebracht, die immer mehr Menschen einleuchtet: Der Kapitalismus funktioniert mehr schlecht als recht und fährt den Planeten und “die Wirtschaft” gegen die Wand. Es ist (schon lange) Zeit, darüber nachzudenken, wie wir unsere Gesellschaft strukturieren müssten, damit der Ausstieg aus der kapitalistischen Produktionsweise und dem autoritären Staat, der den Laden gerade noch so zusammenhält, gelingen kann.

Wie sieht dann eine tragfähige Alternative aus? Einerseits kann die politische Linke auf eine sozialistische Tradition zurückgreifen, die so alt ist wie die kapitalistischen Gesellschaften selbst. Andererseits zeigen die Erfahrungen des autoritären Staatssozialismus, wie schnell sich neue Herrschaftsformen unter dem Deckmantel einer besseren Gesellschaftsordnung etablieren können. Im Wissen um diese Gefahren und im Wissen um das viel zu häufige Scheitern der zahlreichen Kommune- und Räte-Experimente, wollen wir genauer auf die Knackpunkte schauen: Welche Mechanismen und auch welche Institutionen braucht eine postkapitalistische Gesellschaft, die sowohl direkt-demokratisch ist als auch eine gesamtgesellschaftliche Steuerung der (Re-)Produktion leisten kann? Wer entscheidet und wer führt aus? Wer produziert was, warum und wie? Was können wir hierbei von den Fehlern des 20. Jahrhunderts, Rätetradition, Commons-Praxis und heutigen Alternativen in Chiapas und Rojava lernen? 



Zu Beginn gibt es einen Überblick über die sehr unterschiedlichen Ansichten, wozu Räte eigentlich gut sein sollen, was sie leisten müssen und wie sie institutionell aussehen müssten (Almut Woller, Forschung zu Rätetheorie und Räten in Syrien): H. Arendt, anarchistische und marxistische Ansätze, M. Bookchin und C. Castoriadis stehen dabei im Zentrum. Davon ausgehend werden zwei Modelle detaillierter vorgestellt und diskutiert, die auf einen antiautoritären Kommunismus abzielen, einmal basierend auf Räten und einmal basierend auf Commons:

Zum einen werden André Kistner und Sebastian Jordan aus dem Verein Initiative demokratische Arbeitszeitrechnung (IDA) das rätekommunistische Konzept der Arbeitszeitrechnung vorstellen ([https://arbeitszeit.noblogs.org](<https://arbeitszeit.noblogs.org/>)). IDA entwickelt gerade auch eine Online-Anwendung (App), die es Kollektivbetrieben ermöglichen soll auf Basis der Arbeitszeitrechnung miteinander in wirtschaftlichen Verkehr treten zu lassen. Grundlage dafür ist die Schrift Grundprinzipien der kommunistischen Produktion und Verteilung der Gruppe internationaler Kommunisten (GIK) aus dem Jahre 1930/1935. Auf beides wollen wir in Vortrags- und Diskussionsphasen eingehen.

Zum anderen stellt Simon Sutterlütti von der Assoziation Kritik Utopie und Transformation das Konzept des Commonismus vor (vgl. [https://commonism.us](<https://commonism.us/>)). Sutterlütti ist u.a. Co-Autor des Buches Kapitalismus aufheben, in dem die Idee einer bedürfnisorientierten Beitragsökonomie besprochen wird, die auf der Grundlage von Commons wirtschaftet und ohne Geld und Leistungszwang auskommen soll. Sie kritisieren dabei nicht nur die markt- und kommandowirtschaftlichen Konzepte, sondern auch den Ansatz der GIK, bei dem nach wie vor die eigene Arbeitsleistung den Anteil am Konsum bestimmt. Beide Gruppen stellen ihr Konzept und gegenseitige Kritik vor, um in ein produktives Streitgespräch zu treten, an dem alle Interessierten teilzunehmen herzlich eingeladen sind. 



Am Sonntag bietet der Jurist Simon Schuster (Autor von „Demokratie des gehorchenden Regierens“) aufschlussreiche Einsichten in die Art und Weise, wie die Zapatistas in Chiapas die Gewaltenteilung aufgehoben und ‚Entscheiden‘ und ‚Ausführen‘ anders organisiert haben. Dies ermöglicht es, Marx‘ zentrales Diktum über die Pariser Kommune als „gesetzgebend und vollziehend zugleich“ entlang eines realen Räteexperiments neu zu diskutieren. Angefragt ist ein\*e Genoss\*in aus Rojava, der\*die mit uns auf den Aufbau der Rätestrukturen blickt, auf deren Verhältnis zum parlamentarischen System, sowie auf Strategien zur Zurückdrängung des Marktes und die Angewiesenheit auf Geld. 



Uns geht es um eine produktive Diskussion und darum, nicht aneinander vorbeizureden. Deshalb bereiten die Referent\*innen sich im Vorhinein inhaltlich gemeinsam darauf vor, indem sie Überschneidungen und Streitpunkte ausfindig machen und Kritikpunkte bereits in ihre Vorträge mit einbeziehen. Es wird einen überschaubaren Reader geben, der es Teilnehmenden ermöglicht, sich ebenfalls vorher etwas einzulesen, wenn sie möchten. Über eine rege Teilnahme freuen wir uns sehr.

**Seminarzeiten**

Samstag: 10.00 – 18.00 Uhr

Sonntag: 10.00 – 16.00 Uhr

**Die Plätze sind begrenzt. Anmeldung bis 16.04. unter **[](<mailto:anmeldung@rosalux-sachsen.de>)**[anmeldung@rosalux-sachsen.de](<mailto:anmeldung@rosalux-sachsen.de>)**



[Reader zur Veranstaltung](<https://cms.rosalux.de/fileadmin/ls_sachsen/dokumente/Reader_Utopie_und_Praxis_des_antiautorit%C3%A4ren_Kommunismus.pdf>)



*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*