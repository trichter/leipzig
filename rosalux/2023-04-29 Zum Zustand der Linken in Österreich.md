---
id: 8H2IF
title: Zum Zustand der Linken in Österreich
start: 2023-04-29 19:00
end: 2023-04-29 20:30
locationName: Interim
address: "Demmeringstraße 32, 04177 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/8H2IF/
teaser: Buchmesse Leipzig
isCrawled: true
---
Mit *Walter Famler* (Publizist, Generalsekretär Alte Schmiede/Kunstverein Wien), *Melina Klaus* (Pädagogin, ehemalige Bundessprecherin KPÖ), *Peter Porsch* (Soziolinguist, langjähriger Politiker der LINKEN) und *Barbara Steiner* (Politikwissenschaftlerin, Direktorin von transform!europe); Moderation: *Horst Junginger* (Religionswissenschaftler, RLS Sachsen)

Eine Veranstaltung des Abgeordnetenbüros linXXnet und der RLS Sachsen

Nachdem die Sozialdemokratie in Österreich politisch nach rechts gerückt ist, öffnet sich ein neuer Handlungsspielraum für linke Ideen und Parteien. Doch wie und von wem kann dieser genutzt werden? Dazu gibt es verschiedene Ideen und Projekte, die wir in dieser Podiumsdiskussion ausloten wollen.

*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*