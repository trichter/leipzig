---
id: SQ2VM
title: «Gegen den Strom»
start: 2023-03-22 19:00
end: 2023-03-22 20:30
locationName: Passage Kinos
address: "Hainstraße 19a, 04109 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/SQ2VM/
teaser: Filmpremiere
isCrawled: true
---
Sara Mardini und ihre jüngere Schwester Yusra stammen aus einer Familie von Hochleistungssportler:innen in Syrien. Der Krieg im Land unterbricht den Traum, olympische Schwimmerin zu werden, und zwingt die Schwestern 2015 zur Flucht. Als auf der Flucht über das Mittelmeer von der Türkei nach Griechenland der Motor des überfüllten Schlauchbootes versagt, springen die Schwestern ins Wasser und helfen bei der Rettung aller Geflüchteten indem sie das Boot drei Stunden auf Kurs halten bis sie das rettende Ufer von Lesbos erreichen und damit alle Geflüchteten an Bord retten. Die Geschichte macht auf der ganzen Welt Schlagzeilen. Mit 20 ist Sara berühmt. Danach trennen sich die Wege der Schwestern: Yusra schwimmt bei den Olympischen Spielen, während Sara nach Lesbos zurückkehrt, um sich ehrenamtlich zu engagieren und anderen Geflüchteten zu helfen. Im Jahr 2018 aber wird sie verhaftet und einer Reihe von schweren Straftaten beschuldigt - darunter Beihilfe zur illegalen Einreise (Schleusung), Geldwäsche, Betrug und Mitgliedschaft in einer kriminellen Vereinigung. Nach über drei Monaten in einem Hochsicherheitsgefängnis in Griechenland wird Sara auf Kaution freigelassen und wartet seitdem auf ihre Verhandlung - ihr drohen 20 Jahre Haft. Über vier Jahre hat die Filmemacherin Charly Wai Feldman Saras Kampf um Gerechtigkeit und um eine neue Zukunft in Berlin begleitet.

[Zum Trailer](<https://www.youtube.com/watch?v=C-lj5InsCdA>)

