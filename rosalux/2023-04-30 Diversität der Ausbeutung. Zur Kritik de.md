---
id: G91KG
title: Diversität der Ausbeutung. Zur Kritik des herrschenden Antirassismus
start: 2023-04-30 19:00
end: 2023-04-30 21:00
locationName: Pöge-Haus
address: "Hedwigstraße 20, 04315 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/G91KG/
teaser: Buchmesse Leipzig
isCrawled: true
---
Mit *Bafta Sarbo* (Autorin)

Mit dem Sammelband “Die Diversität der Ausbeutung. Zur Kritik des herrschenden Antirassismus” wollen die Herausgeberinnen Bafta Sarbo und Eleonora Roldán Mendívi in aktuelle Debatten intervenieren und einen marxistischen Rassismusbegriff wieder ins Zentrum der Diskussionen holen. Sie vertreten die These, dass in Deutschland von Antidiskriminierungsstellen bis zur radikalen Linken ein liberaler Rassismusbegriff vertreten wird, der vor allem auf Repräsentation, Inklusion und Diversität setzt. Wie Klasse und Rasse zusammenhängen, wird aktuell so gut wie nicht diskutiert. Dabei gibt es durchaus eine kritisch-marxistische Tradition der Rassismusforschung. Der Band will diesen Fundus heben. Hierzu werden historische und aktuelle Diskussionen aus dem englischsprachigen Raum rezipiert sowie aus deutschsprachigen marxistischen Wissensarchiven aktualisiert. Gleichzeitig bietet das Buch eine politische Intervention in die aktuelle Debatte um strukturellen und institutionellen Rassismus – ob auf dem Arbeitsmarkt oder bei der Polizei – und präsentiert Alternativen zum liberalen Antirassismus, indem ein marxistischer Rassismusbegriff in Theorie und Praxis vorgestellt wird. 



Bafta Sarbo ist Sozialwissenschaftlerin. Sie lebt in Berlin und beschäftigt sich mit marxistischer Gesellschaftskritik, (Anti-)Rassismus, Migration und Polizeigewalt. Politisch ist sie unter anderem aktiv im Vorstand der Initiative Schwarze Menschen in Deutschland.

*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*