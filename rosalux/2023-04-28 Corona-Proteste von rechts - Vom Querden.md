---
id: T9Y4E
title: Corona-Proteste von rechts - Vom Querdenken zur Querfront
start: 2023-04-28 19:00
end: 2023-04-28 21:30
locationName: Stadtteilladen Lixer e.V.
address: "Pörstener Straße 9, 04229  "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/T9Y4E/
teaser: Buchmesse Leipzig
isCrawled: true
---
Mit *Lucius Teidelbaum* (Journalist, Publizist); Moderation: *Steven Hummel* (Politikwissenschaftler, RLS Sachsen)

Eine Veranstaltung des Abgeordnetenbüros linXXnet, des Lixer e.V., des UnRast-Verlages, des Antifa-Tresen, chronik.LE und der RLS Sachsen

Was im April 2020 als diffuse Bewegung gegen die Maßnahmen anfing, wurde schnell zur rechtsoffenen Bewegung mit verschwörungsideologischem Unterbau und zum Marktplatz der alternativen Fakten. Im Buch wird die Entwicklung nachgezeichnet, sowie Inhalte der Bewegung und ihre Gefahren aufgezeigt.

Lucius Teidelbaum hat mehrere Bücher im Unrast-Verlag veröffentlicht und ist Stammautor für das Antifa-Magazin “der rechte rand”.

*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*