---
id: 7ACU1
title: Heimatromantik, Lifestyle und rechte Propaganda
start: 2023-03-28 19:00
end: 2023-03-28 21:00
locationName: Neues Schauspiel Leipzig
address: "Lützner Straße 29, 04177 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/7ACU1/
teaser: Rechte Influencerinnen als Werbekörper der Ideologie
isCrawled: true
---
Mit *Viktoria Rösch* (Soziologin)

Ein Foto vom Feld im Sonnenaufgang, das Foto einer jungen Frau mit blonden Locken im Wald, ein Beitrag mit einer schwangeren Frau auf der Wiese und fertig ist die perfekte Inszenierung rechter Heimatromantik im Instagram-Retrofilter. Die Accounts rechter “Tradwives” verkörpern einen Lifestyle, der eine Ausflucht aus der Hektik des Alltags und dem grundsätzlichen Unbehagen mit der Moderne verspricht. Mit ästhetischen Fotos, vermeintlich unverfänglichen Themen und einer persönlichen Note wird rechte Ideologie ‚instagrammable‘ verpackt. Die Schaffung einer virtuellen Wohlfühlatmosphäre ist jedoch nicht das einzige Betätigungsfeld junger rechter Aktivistinnen. Die rechte Rebellin, die gemeinsam mit ihrer Crew posiert, die junge Intellektuelle und die Aktivistin für rechte Frauenrechte haben ebenso ihren Platz in den sozialen Medien.

Der Vortrag gibt einen Überblick über das Phänomen (neu-)rechter Influencerinnen. Wir schauen uns an, wie auf Instagram und YouTube politische Inhalte mit medialen Selbstdarstellungen verknüpft werden, wie gezielte Einblicke ins ‚Private‘ mit politischen Botschaften verbunden werden und welche ästhetischen wie inhaltlichen Angebote junge Frauen der Neuen Rechten machen.

**Viktoria Rösch** forscht zu den medialen Praktiken rechter Influencerinnen und ist Mitglied im Netzwerk feministische Perspektiven & Interventionen gegen die (extreme) Rechte (FemPi)



*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*