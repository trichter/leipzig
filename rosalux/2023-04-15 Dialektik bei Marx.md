---
id: YB9QP
title: "AUSGEBUCHT: Dialektik bei Marx"
start: 2023-04-15 11:00
end: 2023-04-16 17:00
locationName: translib
address: "Goetzstraße 7, 04177  "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/YB9QP/
isCrawled: true
---
Mit *Franz Heilgendorff* (Philosoph)

Eine Veranstaltung der translib in Kooperation mit der RLS Sachsen

Die Idee, die versteinerten Verhältnisse zum Tanzen zu bringen, indem man ihnen die eigene Melodie vorspielt, findet sich schon bei Hegel als Bestimmungsmoment dialektischenDenkens. Der wesentliche Unterschied zwischen Hegel und Marx besteht nun darin, dass bei Hegel diese Melodie das Denken, die Wirklichkeit Formbestimmungen des Denkens sind. Bei

Für Marx ist die Melodie die gesellschaftliche Praxis der Menschen und die Formbestimmungen daher Praxisformen. Dieser Problemstellung und Veränderung dialektischen Denkens wird sich in diesem Seminar zu Marx gewidmet. Die Erkenntnis, die in der Reflexion der Kategorien liegt, ist nun nicht mehr die Erfahrung des in den Gegenständen den Menschen entfremdeten Bewusstseins. Vielmehr scheint auf, dass die Kategorien der politischen Ökonomie Ausdruck der entfremdeten Arbeit sind und damit nicht ewig gültig, sondern durch die Menschen geschaffen, ihr eigenes gesellschaftliches Produkt In der Herrschaft der Abstraktionen wie Ware, Geld und Kapital verbirgt sich die Herrschaft des Menschen über den Menschen.

Die Marxsche Dialektik steht damit im Widerspruch zur Aufklärung: Nicht das Denken ist es, das die menschliche Gesellschaft substantiell bestimmt, sondern der Stoffwechsel mit der Natur, die gesellschaftlich geteilte Arbeit. Daher ist der bürgerlichen Gesellschaft auch nicht durch Aufklärung beizukommen, sondern die uns beherrschenden Kategorien wie Ware, Geld und Kapital verschwinden erst, wenn ihre Grundlage transformiert wird: die gesellschaftliche Praxis der Menschen.

Ziel des Seminars ist ein grundlegendes Verständnis dessen, was Dialektik ist. Kaum ein Begriff ist umstrittener, unklarer und schwerer zu verstehen. Zugleich ist eine kritische Theorie der Gesellschaft ohne ihn undenkbar. Durch das Dickicht der Theorie führt Franz, der nach längerer Beschäftigung mit dem Gegenstand eine Idee hat, wie man ihn verständlich machen könnte.



Die Teilnehmer\*innenzahl ist auf 20 Personen begrenzt, wir bitten um möglichst frühe Anmeldung unter [dialektik@posteo.de](<mailto:dialektik@posteo.de>)



Wer sich weiter mit dem Thema Dialektik beschäftigen möchte, dem sei auch die Veranstaltung [Dialektik bei Adorno ](<https://sachsen.rosalux.de/veranstaltung/es_detail/B3P1P/dialektik-bei-adorno?cHash=f97061d8ef0baa99806abf5a3368d67b>)am 24./25. Juni ans Herz gelegt.

*

*