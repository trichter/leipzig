---
id: 22006-1696521600-1696528800
title: Klimagerechtigkeit - ein antikolonialer Blick auf Ökologie und Wirtschaft
start: 2023-10-05 16:00
end: 2023-10-05 18:00
address: online
link: https://konzeptwerk-neue-oekonomie.org/termin/klimagerechtigkeit-ein-antikolonialer-blick-auf-oekologie-und-wirtschaft/
isCrawled: true
---
Wer leidet am meisten unter der Klimakrise? Welche Herrschaftsverhältnisse verschlimmern sie? Wer entscheidet, welche Ressourcen wie genutzt werden? Wem gehört eigentlich die Natur? Und was muss sich daran verändern, um eine soziale und ökologische Transformation zu realisieren? 
Die Geschichte des Kapitalismus ist eine Geschichte der privatwirtschaftlichen Aneignung von Ressourcen und Arbeitskraft. Sie ist geprägt von kolonialer Gewalt – Bis heute hat die Weltwirtschaft eine koloniale Struktur. Um Lösungen für unfairen Handel und die Klimakrise zu finden, müssen wir ein Verständis ihrer Ursachen entwickeln.