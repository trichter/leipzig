---
id: 22343-1698771600-1698782400
title: Klimagerechtigkeit im Strukturwandel
start: 2023-10-31 17:00
end: 2023-10-31 20:00
address: online
link: https://konzeptwerk-neue-oekonomie.org/termin/klimagerechtigkeit-im-strukturwandel/
isCrawled: true
---
Die Klimakrise gilt als die globale Herausforderung schlechthin – Hitzewellen, Überschwemmungen und Dürren erschweren bereits jetzt ein gutes Leben auf der Erde. Auch in den Braunkohlegebieten im Osten Deutschlands wird das immer sichtbarere. Doch weder sind alle Menschen gleich verantwortlich für die Klimakrise, noch sind alle gleich davon betroffen. 
In diesem Workshop erkunden wir gemeinsam die verschiedenen Aspekte von Klimagerechtigkeit. Wir diskutieren, wie ein solidarischer und klimagerechter Strukturwandel Welt aussehen könnte und entwickeln gemeinsam praktische Ideen und Ansätze zum Handeln – sowohl für uns als Einzelne als auch als Gesellschaft.