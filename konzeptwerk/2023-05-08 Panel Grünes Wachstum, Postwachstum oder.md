---
id: 21542-1683569700-1683578700
title: "Panel: Grünes Wachstum, Postwachstum oder Ökosozialismus | Public
  Climate School"
start: 2023-05-08 18:15
end: 2023-05-08 20:45
address: Hauptcampus Uni Leipzig, Hörsaal 2, Universitätsstraße 1, Leipzig, Deutschland
link: https://konzeptwerk-neue-oekonomie.org/termin/panel-gruenes-wachstum-postwachstum-oder-oekosozialismus-public-climate-school/
isCrawled: true
---
Die menschengemachte Klimakrise ist fest mit der industriellen Revolution und dem vorherrschenden Kapitalismus verknüpft. Mit welchem Wirtschaftsmodell können wir dieser globalen Krise wirklich entgegenwirken?
Bei dem Panel diskutiert Kai Kuhnhenn vom Konzeptwerk mit Hans Rackwitz (Universität Jena,Soziologie) und Katharina Stolla (Grüne Jugend)