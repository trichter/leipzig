---
id: 22304-1699457400-1699462800
title: Digitalisierung ökologisch und gerecht? Geht das?
start: 2023-11-08 15:30
end: 2023-11-08 17:00
address: Cornelsen Informationszentrum Leipzig, Neumarkt 16-20 (2. OG), Leipzig, 04109
link: https://konzeptwerk-neue-oekonomie.org/termin/digitalisierung-oekologisch-und-gerecht-geht-das/
isCrawled: true
---
Unser Alltag wird immer stärker durch digitale Technik geprägt, die sozialen und ökologischen Auswirkungen jedoch seltener hinterfragt. Schon Kinder und Jugendliche leben in einer Welt, in der „Digitalisierung“ als Prozess alle Lebens- und Arbeitsbereiche betrifft und verändert. Die Perspektive auf Technik als etwas, das demokratisch veränderbar und gestaltbar ist, wird selten eingenommen. 
Themen des Workshops:
• ökologische Auswirkungen der Produktion, Nutzung und Entsorgung digitaler Geräte
• globale Zusammenhänge im Kontext von Digitalisierung
• Handlungsoptionen und Alternativen 
Max Bömelburg vom Konzeptwerk Neue Ökonomie e. V. stellt Bildungsmethoden vor, die kritisches Nachdenken über die Herstellung, Nutzung und Kontrolle digitaler Technik ebenso fördern wie die Selbstermächtigung zu Entscheidungen rund um digitale Technik im Alltag. 
Anmeldung: https://veranstaltungen.cornelsen.de/veranstaltung/5212-DigitalisierungkologischundgerechtGehtdas.html