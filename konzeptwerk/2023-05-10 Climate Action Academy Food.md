---
id: 21132-1683730800-1683738000
title: Climate Action Academy Food
start: 2023-05-10 15:00
end: 2023-05-10 17:00
address: online
link: https://konzeptwerk-neue-oekonomie.org/termin/climate-action-academy-food/
isCrawled: true
---
Die Climate Action Academy: Food systems and dietary habits (CAA-FOOD) ist ein ERASMUS+ gefördertes internationales Online-Fortbildung in Deutschland und Slowenien, das sich auf Lebensmittelsysteme und Ernährungsgewohnheiten konzentriert, um die Zusammenhänge zwischen dem Klimawandel und dem täglichen Leben von Jugendlichen zu verdeutlichen.
Die Live-Sessions vermitteln Ihnen ein theoretisches Verständnis und ermöglichen es Ihnen, das Wissen über die verschiedenen Methoden und Instrumente in Ihre Arbeit mit jungen Menschen einzubringen.
Wer: Diese BNE-Fortbildung richtet sich in erster Linie an Lehrer*innen, Multiplikator*innen und lokaler Organisationen in Deutschland, die mit jungen Menschen arbeiten und ihre Kenntnisse über den Zusammenhang zwischen Klimawandel und Ernährungssystemen weiter qualifizieren möchten. Die Teilnahme an der Trainingseinheit ist kostenlos.