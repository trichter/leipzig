---
id: 21128-1683057600-1683064800
title: Wirtschaft neu ausrichten!
start: 2023-05-02 20:00
end: 2023-05-02 22:00
address: online und Freiburg
link: https://konzeptwerk-neue-oekonomie.org/termin/wirtschaft-neu-ausrichten/
isCrawled: true
---
Am 02. Mai 2023 findet der Buchlaunch des Sammelbandes „Wirtschaft neu ausrichten. Care Initiativen in Deutschland, Österreich und der Schweiz“ statt. Der Booklaunch wird außerdem im Livestream übertragen.
Der Sammelband stellt erstmals die vielseitig aufgestellten Care-Initiativen aus Wissenschaft, Praxis und politischem Aktivismus in Deutschland, Österreich und der Schweiz (DACH-Raum) vor.
Sie stehen für eine breite Suchbewegung, die im Angesicht weltweiter multipler Krisen danach fragt, welche Pfade wir einschlagen müssen, um die massive Abwertung und Ausbeutung der (über-) lebensnotwendigen Care-Arbeit ebenso wie von natürlichen Ressourcen strukturell zu überwinden: Es geht im Kern um eine care-zentrierte Neuausrichtung der Wirtschaft, welche das Fundament einer gelingenden sozial-ökologischen Transformation darstellt.
Hier (vor-)bestellen:
shop.budrich.de/produkt/wirtschaft-neu-ausrichten 
Wir haben einen Beitrag geschrieben: Gemeinsam. Gerecht. Global. Lernen, sorgen und handeln in
postmigrantischen Allianzen: Einsichten in die Care-Schwerpunkte
des Kooperationsprojektes (2021/2022)