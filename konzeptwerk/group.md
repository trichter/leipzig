---
name: Konzeptwerk Neue Ökonomie
website: https://www.konzeptwerk-neue-oekonomie.org/
email: info@knoe.org
address: Klingenstr. 22, 04229 Leipzig
scrape:
  source: ical
  options:
    url: https://konzeptwerk-neue-oekonomie.org/veranstaltungen/?ical=1
  filter:
    address: "([Ll]eipzig|04[1-4][0-9]{2}|04683|04687|04668|0482[1478]|04808|online)"
---
Das Konzeptwerk steht ein für eine Demokratisierung der Gesellschaft, von unten, mit allen. Wir vernetzen Bewegungen und ermächtigen Menschen, politische und gesellschaftliche Prozesse mitzugestalten. 
