---
id: 20816-1679133600-1679142600
title: Utopien für eine Zukunft für Alle
start: 2023-03-18 10:00
end: 2023-03-18 12:30
address: online
link: https://konzeptwerk-neue-oekonomie.org/termin/utopien-fuer-eine-zukunft-fuer-alle/
isCrawled: true
---
Stell dir vor, du wachst auf und es ist das Jahr 2048. Wie sieht es aus? Wie bewegst du dich fort? Was arbeitest du? Und wie ernährst du dich? Geht es dir gut? Und dem Rest der Welt? Die sich zuspitzende Klimakrise und die wachsende soziale Ungleichheit machen deutlich: Ein Weiter wie bisher ist weder möglich noch wünschenswert. Wir brauchen gesellschaftliche Veränderung im großen Stil. Doch wie kann Gesellschaft konkret aussehen, wenn Menschen in all ihrer Vielfalt weltweit wertgeschätzt werden und frei und selbstbestimmt leben können? Und wenn gleichzeitig eine lebenswerte Umwelt erhalten wird? Das Konzeptwerk Neue Ökonomie hat mit zahlreichen Akteur*innen und Expert*innen gesprochen und im Projekt Zukunft für Alle konkrete Visionen für eine
solidarische und nachhaltige Zukunft – und Bildungsmaterialien für eine Auseinandersetzung damit – entwickelt. Im Workshop nähern wir uns mit kreativen Methoden einem solidarischen Morgen, arbeiten an der Utopiefähigkeit und tauschen uns über Möglichkeiten einer Bildung für Globale Gerechtigkeit aus.


Anmeldung:  https://www.komment.at/wp-content/uploads/2021/02/Bundesfachtagung-Globales-Lernen_Programm_2023.pdf