---
id: 22249-1697034600-1697043600
title: Klimakrise - Was ist das und was hat das mit mir zu tun?
start: 2023-10-11 14:30
end: 2023-10-11 17:00
address: Querbeet Mitmachgarten, Schlehenweg 29, Leipzig
link: https://konzeptwerk-neue-oekonomie.org/termin/klimakrise-was-ist-das-und-was-hat-das-mit-mir-zu-tun/
isCrawled: true
---
Wir lernen mit dem Team des Konzeptwerkes für neue Ökonomie e.V. Möglichkeiten kennen sich für das Thema Klimagerechtigkeit durch Bildung zu engagieren. Es soll ein Lernraum entstehen für Austausch und Entwicklung von Aktionen durch anwendbare Bildungsmethoden, um aus der Klimakrise eine soziale Gerechtigkeit zu erlangen. Welche Probleme und Lösungen liegen durch die Idee der Nachhaltigkeit in unseren Händen. 
Anmeldung unter mailto:kontakt@wirrwuchs.de