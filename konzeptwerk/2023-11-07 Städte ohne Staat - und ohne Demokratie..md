---
id: 22346-1699381800-1699389000
title: Städte ohne Staat - und ohne Demokratie. Speakterstour zu den weltweit
  ersten Privatstädten in Honduras
start: 2023-11-07 18:30
end: 2023-11-07 20:30
address: hinZundkunZ, Georg-Schwarz-Straße 9, Leipzig
link: https://konzeptwerk-neue-oekonomie.org/termin/staedte-ohne-staat-und-ohne-demokratie-speakterstour-zu-den-weltweit-ersten-privatstaedten-in-honduras/
isCrawled: true
---
Gespräch über „libertär“ kapitalistische Enklaven, internationale Netzwerke und den Widerstand der sozialen Bewegungen in Honduras gegen die „Zonen für Beschäftigung und Entwicklung“ (ZEDES)
Der Kampf gegen Privatstädte – eine „libertär“ kapitalistische Dystopie, gegen die es breiten Widerstand von unten braucht!
Honduras ist der erste Staat weltweit, der von Unternehmer:innen geführte Privatstädte als Enklaven auf dem eigenen Staatsgebiet zuließ. Hinter dem Konzept der von Unternehmen regierten Enklaven stehen internationale marktradikale („libertäre“) Netzwerke, die Demokratie und Sozialstaat in weiten Teilen ablehnen. Die Enklaven durften sich ihre eigenen Gesetze geben, eigene Gerichte und Sicherheitsdienste einsetzen und sogar eine eigene Citizenship vergeben.
Soziale Bewegungen hatten sich von Anbeginn gegen die Pläne gestellt, Land an unternehmensgeführte Privatstädte abzutreten, gefürchtet wurde von Anfang an die Vertreibung insbesondere indigener Gemeinden und armer Bevölkerungsschichten.
Auch hierzulande brachte die AfD-Fraktion 2021 einen Antrag in den Bundestag ein, die deutsche Entwicklungszusammenarbeit auf das Modell der Privatstädte umzustellen; im Juni 2023 berichtete der Norddeutsche Rundfunk von einer Bürgergenossenschaft im mittelsächsischen Döbeln, die „Parallelstrukturen zum Staat“ anstrebe. 
Zwei Gäste aus Honduras, Venessa Cardenas Woods und Christopher Castillo berichten von Erfolgen und Niederlagen der Widerstandsbewegung in Honduras, informieren über die Hintergründe der Privatstadt-Initiativen und die Erfahrungen einer karibisch-indigenen Gemeinde mit einer Unternehmer-Enklave in unmittelbarer Nachbarschaft.