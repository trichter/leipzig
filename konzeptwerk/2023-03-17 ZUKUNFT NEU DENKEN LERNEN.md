---
id: 20813-1679070600-1679077800
title: ZUKUNFT NEU DENKEN LERNEN
start: 2023-03-17 16:30
end: 2023-03-17 18:30
address: online
link: https://konzeptwerk-neue-oekonomie.org/termin/zukunft-neu-denken-lernen/
isCrawled: true
---
Christoph Sanders diskutiert auf dem Podium „Zukunft neu denken lernen“ auf der Bundesfachtagung Globales Lernen mit. 
Anmeldung: https://www.komment.at/wp-content/uploads/2021/02/Bundesfachtagung-Globales-Lernen_Programm_2023.pdf