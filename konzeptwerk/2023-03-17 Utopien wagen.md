---
id: 20727-1679067000-1679234400
title: Utopien wagen
start: 2023-03-17 15:30
end: 2023-03-19 14:00
address: Maker Space Leipzig, Lindenthaler Straße 61-65, 04155 Leipzig
link: https://konzeptwerk-neue-oekonomie.org/termin/utopien-wagen/
isCrawled: true
---
Stell dir vor, du wachst auf und es ist das Jahr 2048. Wie sieht es aus? Wie bewegst du dich fort? Was arbeitest du? Und wie ernährst du dich? Geht es dir gut? Und dem Rest der Welt? 
Die sich zuspitzende Klimakrise und die wachsende soziale Ungleichheit machen deutlich: Ein Weiter wie bisher ist weder möglich noch wünschenswert. Wir brauchen gesellschaftliche Veränderung im großen Stil. Doch wie kann eine Zukunft konkret aussehen, in der Menschen in all ihrer Vielfalt weltweit wertgeschätzt werden und frei und selbstbestimmt leben können? Und in der gleichzeitig eine lebenswerte Umwelt erhalten wird?
Das Konzeptwerk neue Ökonomie hat mit zahlreichen Akteur*innen und Expert*innen gesprochen und im Projekt Zukunft für Alle konkrete Visionen für eine solidarische und nachhaltige Zukunft entwickelt. 
In diesem Workshop blicken wir gemeinsam auf diese utopischen Visionen und verknüpfen sie mit unseren eigenen Vorstellungen und Erfahrungen. Zusammen erforschen wir Möglichkeiten, Widersprüche, Leerstellen und Konfliktpotenzial unserer erträumten Utopien. Auf der Suche nach Lösungsansätzen für eine gute Zukunft für Alle werden uns folgende Fragen beschäftigen: Inwiefern prägt uns unsere Umgebung und unsere Geschichte in unserem Denken und Träumen? Was davon steht einer gerechten Zukunft für alle im Weg und wie können wir verlernen? 
Der Workshop findet im Rahmen der Erasmus+ geförderten Veranstaltungsreihe „From Dreams to Actions, from Reflection to Engagement – DARE for socio-economic change!“ statt. Im Anschluss an diesen Utopie-Workshop wird es von April bis Juli einen Lesekreis gebe. Zum Abschluss der Projekts organisieren wir den Transformationsworkshop „Von der Utopie zur Realität“. Hier gibt es mehr Infos über das ganze DARE-Projekt. 
Die Teilnahme am Workshop ist kostenlos. Es gibt vegane Verpflegung für alle Teilnehmenden. 
Du hast noch offene Fragen, oder möchtest uns deine Bedürfnisse oder Wünsche mitteilen? Du benötigst für die Workshops einen Schlafplatz in Leipzig? Dann komme gerne auf uns zu. n.peulen@knoe.org
0341/39281686 
Wir freuen uns sehr, dass das Interesse für diesen Workshop so groß ist. Sowohl der Workshop als auch die Warteliste sind mittlerweile jedoch voll und wir haben die Anmeldung wieder geschlossen. Für den Lesekreis und den Transformationsworkshop im September könnt ihr euch aber noch anmelden wenn es soweit ist. Außerdem lohnt sich ein regelmäßiger Blick ins unseren Veranstaltungskalender, da wir immer wieder Workshops und Vorträge zu gesellschaftlichen Utopien geben.