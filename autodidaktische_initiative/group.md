---
name: autodidaktische initiative
website: https://adi-leipzig.net/
email: info@adi-leipzig.net
address: Georg-Schwarz-Straße 19, Leipzig
scrape:
  source: adi
---
Die Autodidaktische Initiative (adi) schafft einen nicht-kommerziellen Raum, in dem Menschen aus
eigenem Antrieb gemeinsam diskutieren, lernen und reflektieren können. Zudem nutzen wir diesen
Raum, um uns zu engagieren und miteinander zu vernetzen