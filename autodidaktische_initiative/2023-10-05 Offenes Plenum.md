---
id: offenes-plenum-2023-10-05
title: Offenes Plenum
start: 2023-10-05 19:00
address: Georg-Schwarz-Straße 19
link: https://adi-leipzig.net/offenes-plenum/
image: Plenum-Header.png
isCrawled: true
---
**Termin:** am 05.10 um 19:00 Uhr* jede gerade Woche am Donnerstag*.

---

---

**Zum offenen Online – Plenum der adi sind alle Interessierten eingeladen!** Hier werden Raum- und Kooperationsanfragen, ADI-Projekte und weitere organisatorische Themen besprochen. Es findet Donnerstags um 19 Uhr in einem zweiwoechigen Turnus statt!

![](<http://adi-leipzig.net/wp-content/uploads/plenum-1-225x300.jpg> =127x169)

Du willst eine Lesekreis oder ähnliches gründen und suchst Räumlichkeiten in denen du deine Idee umsetzen kannst?

Dann komm vorbei, teil deine Idee mit uns und wir schauen gemeinsam, ob die adi der Raum für deine Idee ist.

Wenn Du interessiert bist oder weitere Fragen hast, schreib uns einfach ein Mail an [info@adi-leipzig.net ](<mailto:info@adi-leipzig.net>)