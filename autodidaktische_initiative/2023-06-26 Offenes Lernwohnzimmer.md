---
id: offenes-lernwohnzimmer-2023-06-26
title: Offenes Lernwohnzimmer
start: 2023-06-26 12:00
address: Georg-Schwarz-Straße 19
link: https://adi-leipzig.net/offenes-lernwohnzimmer/
isCrawled: true
---
**Termin:** am 26.06 um 12:00 Uhr* jeden Montag*.

Die Räumlichkeiten der ADI stehen Montags und Dienstag von 12 bis 17Uhr zur Verfügung.