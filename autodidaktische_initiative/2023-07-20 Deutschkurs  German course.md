---
id: deutsch-als-fremdsprache-german-as-foreign-language-2023-07-20
title: Deutschkurs / German course
start: 2023-07-20 17:00
address: Georg-Schwarz-Straße 19
link: https://adi-leipzig.net/deutsch-als-fremdsprache-german-as-foreign-language/
isCrawled: true
---
**Termin:** am 20.07 um 17:00 Uhr* jeden Donnerstag*.







<figure class="wp-block-image size-large is-resized"><img decoding="async" src="http://adi-leipzig.net/wp-content/uploads/icon-3154240_1280-1024x1024.png" alt="" class="wp-image-4227" width="587" height="587" srcset="https://adi-leipzig.net/wp-content/uploads/icon-3154240_1280-1024x1024.png 1024w, https://adi-leipzig.net/wp-content/uploads/icon-3154240_1280-300x300.png 300w, https://adi-leipzig.net/wp-content/uploads/icon-3154240_1280-150x150.png 150w, https://adi-leipzig.net/wp-content/uploads/icon-3154240_1280-768x768.png 768w, https://adi-leipzig.net/wp-content/uploads/icon-3154240_1280.png 1280w" sizes="(max-width: 587px) 100vw, 587px"></figure>

……………………………………………………………………

Selbstorganisierter Deutschkurs in der ADI

Wir treffen uns jeden Donnerstag von 17-19 Uhr zum Deutschlernen. Freiwillige Lehrkräfte bereiten Gesprächsthemen, Sprachspiele, Grammatikerklärungen und Übungen vor. Bringt gerne auch eigene Fragen oder Themen mit, mit denen wir uns beschäftigen können!

Eine Anmeldung ist nicht notwendig. Der Kurs ist für euch kostenfrei. Schaut einfach mal vorbei!

<span style="color: #fcbf81;">Du hast noch Fragen? Melde Dich gern bei uns unter <a style="color: #fcbf81;" href="mailto:daf@adi-leipzig.net">daf@adi-leipzig.net&nbsp;</a></span>

……………………………………………………………………

Self-organised German classes at ADI

We meet up every Thursday from 5-7 pm to study German. Volunteer teachers prepare topics for conversation, language games, grammar explanations, and exercises. You’re of course welcome to bring your own questions and topics, too!

There is no registration required and the course is free of charge. Just drop by!

<span style="color: #fcbf81;">Still have questions? You can contact us at <a style="color: #fcbf81;" href="mailto:daf@adi-leipzig.net">daf@adi-leipzig.net&nbsp;</a>.&nbsp;</span>

<span style="color: #000000;">……………………………………………………………………</span>



Clases de alemán auto-organizativas

Nos reunimos todos los jueves de las 5 a las 7 de la tarde, para estudiar alemán. Profes voluntari@s preparan temas de conversación, juegos de lengua, explicaciones de gramática y ejercicios. ¡Claro que puedes traer tus propios temas y preguntas también!

No hace falta inscribirse, y las clases son gratis. ¡Apúntate!

<span style="color: #fcbf81;">¿Aún tienes preguntas? Nos puedes contactar a <a style="color: #fcbf81;" href="mailto:daf@adi-leipzig.net">daf@adi-leipzig.net&nbsp;</a>.&nbsp;</span>

……………………………………………

Cours d’allemand auto-organisé à l’ADI

Nous nous réunissons tous les jeudis de 17h à 19h pour apprendre l’allemand. Des enseignant\_es bénévoles préparent des sujets de conversation, des jeux de langue, des explications de grammaire et des exercices. Vous êtes également invité\_es à apporter vos propres questions ou sujets que nous pouvons traiter!

Une inscription n’est pas nécessaire. Le cours est gratuit pour vous. Venez simplement nous rejoindre!

<span style="color: #fcbf81;">Vous avez encore des questions? Contactez-nous par mail&nbsp;: <a style="color: #fcbf81;" href="mailto:daf@adi-leipzig.net">daf@adi-leipzig.net</a></span>

<span style="color: #000000;">……………………………………………………………………</span>

Самостоятельно организованный курс немецкого языка в ADI

<div dir="auto"></div>

<div dir="auto">Мы встречаемся каждый четверг с 17-00 до 19-00 часов для изучения немецкого языка. Учителя-добровольцы подготовят темы для бесед, языковых игр, объяснения грамматики и упражнений. Приносите свои вопросы или темы, с которыми мы можем разобраться и решить.</div>

<div dir="auto">Регистрация не требуется.</div>

<div dir="auto">Курс для вас бесплатный.</div>

<div dir="auto"></div>

<div dir="auto">Просто зайдите!</div>

<div dir="auto"></div>

<div dir="auto">Если у вас есть вопросы, то обращайтесь к нам по адресу: <a href="mailto:daf@adi-leipzig.net" target="_blank" rel="noopener">daf@adi-leipzig.net</a></div>

……………………………………………………………………

<pre></pre>

<div class="wp-block-image"><figure class="alignright size-large is-resized"><img decoding="async" loading="lazy" src="http://adi-leipzig.net/wp-content/uploads/image-5.png" alt="" class="wp-image-4688" width="782" height="78" srcset="https://adi-leipzig.net/wp-content/uploads/image-5.png 602w, https://adi-leipzig.net/wp-content/uploads/image-5-300x30.png 300w" sizes="(max-width: 782px) 100vw, 782px"></figure></div>

<div class="wp-block-image"><figure class="alignright size-large is-resized"><img decoding="async" loading="lazy" src="http://adi-leipzig.net/wp-content/uploads/image-6.png" alt="" class="wp-image-4689" width="678" height="238" srcset="https://adi-leipzig.net/wp-content/uploads/image-6.png 602w, https://adi-leipzig.net/wp-content/uploads/image-6-300x106.png 300w" sizes="(max-width: 678px) 100vw, 678px"></figure></div>