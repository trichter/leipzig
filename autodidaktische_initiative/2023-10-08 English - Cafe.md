---
id: english-cafe-2023-10-08
title: English - Cafe
start: 2023-10-08 13:00
address: Georg-Schwarz-Straße 19
link: https://adi-leipzig.net/english-cafe/
isCrawled: true
---
**Termin:** am 08.10 um 13:00 Uhr* jeden Sonntag*.

<div class="wp-block-image"><figure class="aligncenter size-full is-resized"><img decoding="async" fetchpriority="high" src="https://usercontent.one/wp/adi-leipzig.net/wp-content/uploads/English-Cafe.png" alt="" class="wp-image-5446" style="width:333px;height:231px" width="333" height="231" srcset="https://usercontent.one/wp/adi-leipzig.net/wp-content/uploads/English-Cafe.png 444w, https://usercontent.one/wp/adi-leipzig.net/wp-content/uploads/English-Cafe-300x208.png 300w" sizes="(max-width: 333px) 100vw, 333px"></figure></div>



**Am Sonntag den 19.März startet mit dem Englisch-Sprachcafe ein neues Angebot in der ADI:**



---



**English Language-Café in the ADI starts again: 3.9.23**



In our English language-café we want to offer people a chance to practice English conversation and meet people in a cozy living room-atmosphere. Are you a native speaker who would like to help teach, connect with locals, search for tandem-partners, etc.? Come to the English language-cafe at „Autodidaktische Initiative“, Georg-Schwarz-Strasse 19., Lindenau. Starting at the 19th of March, every Sunday from 1 pm till open end.



**We are looking for native English-speakers**!