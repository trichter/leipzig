---
id: open-air-kino-die-ritterinnen-2023-08-25
title: "Open Air Kino: Die Ritterinnen( Achtung! Externer Ort: Eisi 187! )"
start: 2023-08-25 19:00
address: Georg-Schwarz-Straße 19
link: https://adi-leipzig.net/open-air-kino-die-ritterinnen/
isCrawled: true
---
**Termin:** am 25.08 um 19:00 Uhr.

**Ort:** Eisenbahnstrasse 182

<div class="wp-block-image"><figure class="aligncenter size-full"><img decoding="async" fetchpriority="high" width="510" height="333" src="http://adi-leipzig.net/wp-content/uploads/die-riterinnen.jpg" alt="" class="wp-image-5725" srcset="https://adi-leipzig.net/wp-content/uploads/die-riterinnen.jpg 510w, https://adi-leipzig.net/wp-content/uploads/die-riterinnen-300x196.jpg 300w" sizes="(max-width: 510px) 100vw, 510px"></figure></div>

Wir freuen uns euch diese Woche in den Garten und ins Hinterhaus der Eisi 182 einladen zu dürfen.

Ab 19h laden wir euch zum gemeinsamen kühlen Getränk, sowie ab ca. 21h zum Open Air Kino mit Popcorn (bei nassen Wetter, ggf. im Hinterhaus).

Gezeigt wird: „Die Ritterinnen“ (Deutsch; kein Untertitel):

Autobiographische Spiel-Doku-Kombination über eine autonome Frauengruppe in Berlin-Kreuzberg, die am 1. Mai 1987 den Aufstand probt.

U.a. in Kooperation mit der Regisseurin Barbara Teufel und Initiative Hinterhaus // Eisi 182

Wir freuen uns auf eurer Kommen.