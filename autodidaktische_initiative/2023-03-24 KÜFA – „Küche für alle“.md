---
id: kuefa-kueche-fuer-alle-2023-03-24
title: KÜFA – „Küche für alle“
start: 2023-03-24 19:00
address: Georg-Schwarz-Straße 19
link: https://adi-leipzig.net/kuefa-kueche-fuer-alle/
isCrawled: true
---
**Termin:** am 24.03 um 19:00 Uhr* jeden Freitag*.

<span style="font-family: Ubuntu;"><span style="font-size: medium;">Jeden Freitag gibt es in der ADI Essen auf Spenden-Basis. Für alle und vegan! Neben dem gemeinsamen Essen und Trinken soll die KÜFA auch einen Rahmen für Veranstaltungen Vorträgen, Kulturprogramm etc. bieten.</span></span>

---

Liebe Menschen,

 nach langer Pause findet unsere Küfa endlich wieder statt. Ab dem 08. April servieren wir wieder jeden Freitag ab 19 Uhr veganes Essen gegen Spende. Wir bitten alle einen tagesaktuellen negativen Test am Eingang vorzuzeigen. Wir freuen uns auf euch.

---

Hi folx,

 after a long break our „Küfa“-project is finally coming back. Starting at the 8th of April, we will again serve vegan food against donations every Friday at 7pm. We will have to ask all of you to bring a negative test from the same day and show it at the entrance. we‘re looking forward to seeing you.