---
id: flinta-workingspace-2-2023-08-24
title: Flinta* Workingspace
start: 2023-08-24 09:00
address: Georg-Schwarz-Straße 19
link: https://adi-leipzig.net/flinta-workingspace-2/
isCrawled: true
---
**Termin:** am 24.08 um 09:00 Uhr* jeden Donnerstag*.

<div class="wp-block-image"><figure class="aligncenter size-large is-resized"><img decoding="async" fetchpriority="high" src="http://adi-leipzig.net/wp-content/uploads/Flinta-Workspace-Logo-3-896x1024.png" alt="" class="wp-image-5567" width="531" height="607" srcset="https://adi-leipzig.net/wp-content/uploads/Flinta-Workspace-Logo-3-896x1024.png 896w, https://adi-leipzig.net/wp-content/uploads/Flinta-Workspace-Logo-3-263x300.png 263w, https://adi-leipzig.net/wp-content/uploads/Flinta-Workspace-Logo-3-768x877.png 768w, https://adi-leipzig.net/wp-content/uploads/Flinta-Workspace-Logo-3.png 1011w" sizes="(max-width: 531px) 100vw, 531px"></figure></div>

Keinen Bock mehr auf Einzelsitzungen mit deinem Laptop und darauf, stumpf deine Aufgaben abzuarbeiten? Wir auch nicht – jedenfalls nicht alleine!



Ab dem 8. Juni entsteht jeden Donnerstag in der Adi ein FLINTA\* Coworking Space. Von 9:00-16:00 stehen Tische, Sofas und der Boden als Platz zum nebeneinader arbeiten zur Verfügung.

Du entscheidest wann, wie lange und wie regelmäßig. Let’s get shit done oder auch einfach nur den Stundenzettel voll!

FLINTA\* only. Wir wollen einen better Space für FLINTA\* in der Lohnarbeit, uns gegenseitig empowern und austauschen.

Du willst dich über ein ganz bestimmtes Thema im Bereich Lohnarbeit austauschen oder sogar einen Workshop halten? Wir freuen uns auf deinen Input und gemeinsam einen Arbeitsraum zu gestalten!

---

Don’t want to spend any more one-on-one sessions with your laptop and just work through your tasks? Neither do we – at least not alone!



Starting June 8th, a FLINTA\* coworking space will be created every Thursday at Adi. From 9:00-16:00 tables, sofas and the floor will be available as a place to work side by side.

You decide when, how long, and how regularly. Get shit done or just fill up your timesheet!

FLINTA\* only. We want to create a better space for FLINTA\* in paid work, empower each other and exchange ideas.

Do you want to talk about a specific topic in the field of paid work or even hold a workshop? We look forward to your input and shaping a workspace together!