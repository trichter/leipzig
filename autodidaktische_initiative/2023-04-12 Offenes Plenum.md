---
id: offenes-plenum-2023-04-12
title: Offenes Plenum
start: 2023-04-12 17:00
address: Georg-Schwarz-Straße 19
link: https://adi-leipzig.net/offenes-plenum/
image: Plenum-Header.png
isCrawled: true
---
**Termin:** am 12.04 um 17:00 Uhr* jeden Mittwoch*.

---

---

**Zum offenen Online – Plenum der adi sind alle Interessierten eingeladen!** Hier werden Raum- und Kooperationsanfragen, ADI-Projekte und weitere organisatorische Themen besprochen. Es findet jeden Donnerstag um 19 Uhr statt!

![](<http://adi-leipzig.net/wp-content/uploads/plenum-1-225x300.jpg> =127x169)

Du willst eine Lesekreis oder ähnliches gründen und suchst Räumlichkeiten in denen du deine Idee umsetzen kannst?

Dann komm vorbei, teil deine Idee mit uns und wir schauen gemeinsam, ob die adi der Raum für deine Idee ist.

Wenn Du interessiert bist oder weitere Fragen hast, schreib uns einfach ein Mail an [info@adi-leipzig.net ](<mailto:info@adi-leipzig.net>)