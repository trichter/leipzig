---
id: offenes-lernwohnzimmer-2023-06-12
title: Offenes Lernwohnzimmer
start: 2023-06-12 12:00
address: Georg-Schwarz-Straße 19
link: https://adi-leipzig.net/offenes-lernwohnzimmer/
isCrawled: true
---
**Termin:** am 12.06 um 12:00 Uhr* jeden Montag*.