---
id: adhs-west-gruppe-2023-06-27
title: ADHS West Gruppe
start: 2023-06-27 17:00
address: Georg-Schwarz-Straße 19
link: https://adi-leipzig.net/adhs-west-gruppe/
isCrawled: true
---
**Termin:** am 27.06 um 17:00 Uhr* jede gerade Woche am Dienstag*.

Seit dem 27.Juni 2023 trifft sich die ADHS West Gruppe in den Räumlichkeiten der Autodidaktischen Initiative e.V.. Weitere Informationen zur Gruppe und an wen ihr euch wenden könnt bei Interesse folgen.