---
id: 15-oktober-hackbar-in-der-adi-2023-10-15
title: Hackbar in der ADI
start: 2023-10-15 16:00
address: Georg-Schwarz-Straße 19
link: https://adi-leipzig.net/15-oktober-hackbar-in-der-adi/
isCrawled: true
---
**Termin:** am 15.10 um 16:00 Uhr.

<div class="wp-block-image"><figure class="aligncenter size-large is-resized"><img decoding="async" fetchpriority="high" src="https://usercontent.one/wp/adi-leipzig.net/wp-content/uploads/Hack-Bar-15.10-1024x576.png" alt="" class="wp-image-5948" style="width:505px;height:284px" width="505" height="284" srcset="https://usercontent.one/wp/adi-leipzig.net/wp-content/uploads/Hack-Bar-15.10-1024x576.png 1024w, https://usercontent.one/wp/adi-leipzig.net/wp-content/uploads/Hack-Bar-15.10-300x169.png 300w, https://usercontent.one/wp/adi-leipzig.net/wp-content/uploads/Hack-Bar-15.10-768x432.png 768w, https://usercontent.one/wp/adi-leipzig.net/wp-content/uploads/Hack-Bar-15.10-1536x864.png 1536w, https://usercontent.one/wp/adi-leipzig.net/wp-content/uploads/Hack-Bar-15.10.png 1920w" sizes="(max-width: 505px) 100vw, 505px"><figcaption class="wp-element-caption"><strong>Am 15.10.23 um 16Uhr findet die Hackbar in der ADI statt:</strong></figcaption></figure></div>



Die Hackbar ist ein offener Raum den wir (eine kleine Gruppe von Leuten mit unterschiedlichen Skills) seit 2021 in wechselnden Räumen und nun in der ADI Sonntags 16.00 in losen Abständen von ca. 6-8 Wochen veranstalten. Kommt vorbei mit euren Hard- und Softwareproblemen, wenn ihr eure Daten und Kommunikation verschlüsseln oder eure Handys googlefrei machen wollt, wenn ihr Programmier-skills teilen, Computer-Musik und -Kunst entwickeln, diskutieren oder einfach nur mit uns abhängen, Mate trinken, Pizza essen wollt. Wir freuen uns auf euch.