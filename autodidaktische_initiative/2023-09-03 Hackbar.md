---
id: hackbar-2023-09-03
title: Hackbar
start: 2023-09-03 16:00
address: Georg-Schwarz-Straße 19
link: https://adi-leipzig.net/hackbar/
image: Hackbar-1.jpg
isCrawled: true
---
**Termin:** am 03.09 um 16:00 Uhr.

<div class="wp-block-image"><figure class="aligncenter size-full is-resized"><img decoding="async" src="http://adi-leipzig.net/wp-content/uploads/Hackbar-1.jpg" alt="" class="wp-image-5714" style="width:405px;height:405px" width="405" height="405" srcset="https://adi-leipzig.net/wp-content/uploads/Hackbar-1.jpg 500w, https://adi-leipzig.net/wp-content/uploads/Hackbar-1-300x300.jpg 300w, https://adi-leipzig.net/wp-content/uploads/Hackbar-1-150x150.jpg 150w" sizes="(max-width: 405px) 100vw, 405px"></figure></div>

**Am 3.9.23 um 16Uhr findet die Hackbar in der ADI statt:**

Die Hackbar ist ein offener Raum den wir (eine kleine Gruppe von Leuten mit unterschiedlichen Skills) seit 2021 in wechselnden Räumen und nun in der ADI Sonntags 16.00 in losen Abständen von ca. 6-8 Wochen veranstalten. Kommt vorbei mit euren Hard- und Softwareproblemen, wenn ihr eure Daten und Kommunikation verschlüsseln oder eure Handys googlefrei machen wollt, wenn ihr Programmier-skills teilen, Computer-Musik und -Kunst entwickeln, diskutieren oder einfach nur mit uns abhängen, Mate trinken, Pizza essen wollt. Wir freuen uns auf euch.