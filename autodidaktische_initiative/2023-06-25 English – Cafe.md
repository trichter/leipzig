---
id: english-cafe-2023-06-25
title: English – Cafe
start: 2023-06-25 13:00
address: Georg-Schwarz-Straße 19
link: https://adi-leipzig.net/english-cafe/
isCrawled: true
---
**Termin:** am 25.06 um 13:00 Uhr* jeden Sonntag*.

<div class="wp-block-image"><figure class="aligncenter size-full is-resized"><img decoding="async" src="https://adi-leipzig.net/wp-content/uploads/English-Cafe.png" alt="" class="wp-image-5446" width="333" height="231" srcset="https://adi-leipzig.net/wp-content/uploads/English-Cafe.png 444w, https://adi-leipzig.net/wp-content/uploads/English-Cafe-300x208.png 300w" sizes="(max-width: 333px) 100vw, 333px"></figure></div>



**Am Sonntag den 19.März startet mit dem Englisch-Sprachcafe ein neues Angebot in der ADI:**



Wollt ihr Englische Konversation üben mit Muttersprachler*innen? Sucht ihr Hilfe bei Hausarbeiten oder Tandempartner*innen? Müsst ihr euch auf Prüfungen vorbereiten? Das Sprachcafé in der ADI will einen Raum schaffen um in gemütlicher Wohnzimmerathmosphäre alles rund um die englische Sprache zu trainieren und dabei Menschen kennenzulernen. Ab dem 19. März wieder jeden Sonntag, 13 Uhr bis open end. Autodidaktische Initiative, Georg Schwarz-Str. 19. Getränke und Kuchen gegen Spende.



---



**English Language-Café in the ADI starts again**:



In our English language-café we want to offer people a chance to practice English conversation and meet people in a cozy living room-atmosphere. Are you a native speaker who would like to help teach, connect with locals, search for tandem-partners, etc.? Come to the English language-cafe at „Autodidaktische Initiative“, Georg-Schwarz-Strasse 19., Lindenau. Starting at the 19th of March, every Sunday from 1 pm till open end.



**We are looking for native English-speakers**!