---
id: lesekreis-kritische-maennlichkeit%ef%bf%bc-2023-05-01
title: LK – Kritische Männlichkeit
start: 2023-05-01 19:00
address: Georg-Schwarz-Straße 19
link: https://adi-leipzig.net/lesekreis-kritische-maennlichkeit%ef%bf%bc/
isCrawled: true
---
**Termin:** am 01.05 um 19:00 Uhr* jede gerade Woche am Montag*.

<div class="wp-block-image"><figure class="aligncenter size-large is-resized"><img decoding="async" src="http://adi-leipzig.net/wp-content/uploads/Kritische-Maennlichkeit-1024x719.jpeg" alt="" class="wp-image-5340" width="513" height="359" srcset="https://adi-leipzig.net/wp-content/uploads/Kritische-Maennlichkeit-1024x719.jpeg 1024w, https://adi-leipzig.net/wp-content/uploads/Kritische-Maennlichkeit-300x211.jpeg 300w, https://adi-leipzig.net/wp-content/uploads/Kritische-Maennlichkeit-768x539.jpeg 768w, https://adi-leipzig.net/wp-content/uploads/Kritische-Maennlichkeit.jpeg 1280w" sizes="(max-width: 513px) 100vw, 513px"></figure></div>

Im Lesekreis „Kritische Männlichkeit“ geht es um die Themen Feminismus, Patriarchat und Männlichkeitsbilder. Dabei diskutieren wir männliche Sozialisationsmuster sowie alltägliches und strukturelles Verhalten von Männern im Patriarchat. Wir reflektieren erlebtes sowie eigenes Verhalten, hinterfragen unsere Umwelt und erarbeiten wie toxische Männlichkeitsmuster abgebaut werden können.

Dabei diskutieren wir in der Gruppe vorab herum geschickte Texte. Wir sind offen für alle Menschen, egal welchem Geschlecht sie sich zuordnen. Vorwissen wird ebenfalls nicht benötigt, denn wir möchten in einfacher Sprache und ohne viele Hürden ins Gespräch kommen. Wir ordnen uns explizit keiner bestimmten Denkschule oder Richtung zu. Jede\*r ist bei uns willkommen, also schaut gerne vorbei!



#### Termine 2023:

<div class="is-layout-flex wp-container-4 wp-block-columns"><div class="is-layout-flow wp-block-column"><p>09.01. </p><p>23.01.</p><p>06.02.</p><p>20.02.</p></div><div class="is-layout-flow wp-block-column"><p>06.03.</p><p>20.03.</p><p>03.04.</p><p>17.04.</p></div><div class="is-layout-flow wp-block-column"><p>01.05. – entfällt ggf.wg. 1. Mai Feiertag</p><p>15.05.</p><p>29.05.</p><p>12.06.</p></div></div>