---
id: bildungstheorie-bei-antonio-gramsci-2023-10-04
title: Bildungstheorie bei Antonio Gramsci
start: 2023-10-04 18:00
address: Georg-Schwarz-Straße 19
link: https://adi-leipzig.net/bildungstheorie-bei-antonio-gramsci/
isCrawled: true
---
**Termin:** am 04.10 um 18:00 Uhr.

<div class="wp-block-image"><figure class="aligncenter size-large is-resized"><img decoding="async" fetchpriority="high" class="wp-image-5750" style="width: 619px; height: 476px;" src="https://usercontent.one/wp/adi-leipzig.net/wp-content/uploads/Header-1024x788.png" alt="" width="619" height="476"></figure></div>

Der Marxist Antonio Gramsci ist vor allem für seine kritische Gesellschafts- und politische Hegemonietheorie, sowie seine kulturtheoretischen Analysen bekannt. Dabei ist sein Ansatz der politischen Bildung ebenso durch einen pädagogischen Grundgedanken geprägt, der sich durch seine gesamte theoretische und praktische Tätigkeit zieht.



Gramsci verweist im Rahmen seiner Überlegungen zur Hegemonie darauf, dass diese als pädagogisches Verhältnis verstanden werden könne. Damit sind jeglicher Bildungsarbeit politische Positionierungen inhärent. Es bleibt nur die Frage, agiert man im Interesse der Hegemonie der Herrschenden oder kritisiert man diese und entwickelt dazu Alternativen, die demokratischer und solidarischer (und ökologischer, müssen wir heute ergänzen) sind.

Mit dem Konzept des Alltagsverstandes ist es zudem möglich, den Bildungsprozess alternativer Hegemonien in den Praxen der alltäglichen Lebensbewältigung politisch zu verstehen und didaktisch zu unterstützen. Dabei ist es angebracht, sich ergänzend auch auf andere Überlegungen, wie beispielsweise von Paulo Freire, zu beziehen. Der Vortrag wird die Thematik systematisch entfalten und einige handlungsorientierende Thesen für eine kritisch-politische Bildung formulieren.