---
id: lk-das-kapital-band-1-2023-03-21
title: LK – Das Kapital, Band 1
start: 2023-03-21 17:00
address: Georg-Schwarz-Straße 19
link: https://adi-leipzig.net/lk-das-kapital-band-1/
image: Marx-Header-1038x393.png
isCrawled: true
---
**Seit Mitte Mai 2019 trifft sich in der ADI ein Lesekreis zu Karl Marx Hauptwerk „Das Kapital“.**

Im Vordergrund steht selbstredend die gemeinsame Lektüre des 1. Bandes der „Kritik der politischen Ökonomie“, wobei bei Bedarf ergänzende Texte zur Klärung inhaltlicher Fragen und theoretischem Kontext zur Hilfe genommen werden können. Im Sinne der Autodidaktik soll sich der Lesekreis in der Gruppe selbst organisieren.

Das Angebot richtet sich sowohl an Einsteiger\*innen, als auch Interessierte mit Vorkenntnissen. Wir bemühen uns dabei um eine beidseitig fruchtbaren Austausch, während wir eine Atmosphäre einseitiger Belehrung oder der Durchsetzung dogmatischer Deutungshoheiten vermeiden wollen.

***Der Lesekreis befindet sich momentan in der Abschluss-Phase des ersten Bandes. Über die weitere Lektüre der Folge-Bände besteht bisher noch keine konkrete Planung. Gegebenenfalls würden wir natürlich über einen entsprechenden Kurs oder einen erneuten Kurs zum ersten Band informieren.*****

**