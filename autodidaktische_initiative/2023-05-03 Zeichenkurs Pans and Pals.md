---
id: zeichenkurs-pans-and-pals-2023-05-03
title: "Zeichenkurs: Pans and Pals"
start: 2023-05-03 19:00
address: Georg-Schwarz-Straße 19
link: https://adi-leipzig.net/zeichenkurs-pans-and-pals/
isCrawled: true
---
**Termin:** am 03.05 um 19:00 Uhr* jede gerade Woche am Mittwoch*.

<div class="wp-block-image"><figure class="aligncenter size-large is-resized"><img decoding="async" src="https://adi-leipzig.net/wp-content/uploads/ZEICHENKURS-2-722x1024.jpg" alt="" class="wp-image-5475" width="346" height="512"><figcaption class="wp-element-caption"><strong>Ab dem 22.3.23 findet im Zweiwochen-Rhythmus ein autodidaktischer Zeichenkurs in der ADI statt:</strong></figcaption></figure></div>



Dear all people,

there is a new thing taking place at the ADI. It is a drawing meeting for everyone to just come by.

It will be every 2nd Wednesday from the 22.3. onwards. It really is for everyone. We draw comics, doodles and about everything.



---



Hallo alle Menschen,

es gibt etwas neues in der ADI. Es ist ein Zeichentreffen, bei dem jede\*r vorbei kommen kann.

Es findet jeden zweiten Mittwoch ab dem 22.3. statt…

Es ist wirklich für jeden. Wir zeichnen Comics, doodeln oder zeichnen sonst so ziemlich alles, wie gerade die Ideen kommen.



Infos via Telegram:

[https://t.me/+oTE9xqAAAIAIMzly](<https://t.me/+oTE9xqAAAIAIMzly>)