---
id: offenes-lernwohnzimmer-2-2023-07-06
title: Offenes Lernwohnzimmer
start: 2023-07-06 12:00
address: Georg-Schwarz-Straße 19
link: https://adi-leipzig.net/offenes-lernwohnzimmer-2/
isCrawled: true
---
**Termin:** am 06.07 um 12:00 Uhr.

Die Räumlichkeiten der ADI stehen Montags und Dienstag von 12 bis 17Uhr zur Verfügung.