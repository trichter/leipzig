---
id: lk-abolutionismus-2023-05-24
title: LK – Abolitionismus
start: 2023-05-24 19:00
address: Georg-Schwarz-Straße 19
link: https://adi-leipzig.net/lk-abolutionismus/
isCrawled: true
---
**Termin:** am 24.05 um 19:00 Uhr* jede ungerade Woche am Mittwoch*.

<div class="wp-block-image"><figure class="aligncenter size-full is-resized"><img decoding="async" src="http://adi-leipzig.net/wp-content/uploads/Lesekreis-Abolutionismus_adi_sharepic-1.jpg" alt="" class="wp-image-5292" width="339" height="339" srcset="https://adi-leipzig.net/wp-content/uploads/Lesekreis-Abolutionismus_adi_sharepic-1.jpg 500w, https://adi-leipzig.net/wp-content/uploads/Lesekreis-Abolutionismus_adi_sharepic-1-300x300.jpg 300w, https://adi-leipzig.net/wp-content/uploads/Lesekreis-Abolutionismus_adi_sharepic-1-150x150.jpg 150w" sizes="(max-width: 339px) 100vw, 339px"></figure></div>

Abolutionismus ist die Bewegung zur Abschaffung von Gefängnissen, Polizei und strafendem Justizsystem. Diese repressiven Staatsapparate sollen durch Strukturen ersetzt werden, mit denen in selbstorganisierten communities Sicherheit und Gerechtigkeit hergestellt werden kann. Durch die Black Lives Matter Bewegung haben abolutionistische Gruppen und Ansätze vor allem in den USA neuen Auftrieb erhalten.

Sie unterscheiden sich von liberalen Forderungen nach bestimmten Reformen, sondern fechten die Legitimität repressiver Institutionen, Verfahren und Denkweisen an. Obwohl die Diskussion darum schon viel älter ist, erscheint es den meisten Menschen völlig unvorstellbar zu sein, die Staatsgewalt zu beschränken. Und das hat bestimmte Gründe, die wir erforschen wollen.

Anfang des Jahres erschien ein Sammelband, um diese Debatte in den deutschsprachigen Kontext zu importieren. Darin sind zahlreiche sozialwissenschaftliche und aktivistische Beiträgen enthalten, die unterschiedlich voraussetzungsvoll sind. Im Lesekreis werden wir uns gemeinsam Grundlagen des Abolutionismus erschließen und diskutieren.

Die einberufenden Personen sind *weiß* und anderweitig privilegiert. Wir versuchen eine Atmosphäre zu schaffen, in der Rassismus kritisiert wird und in der wir unsere eigenen Positionen hinterfragen und u.a. Perspektiven von People of Color Raum geben können.

Da wir den Reader als Textgrundlage nehmen, wird auch die Diskussion in deutscher Sprache stattfinden.

Wenn ihr Interesse habt, teilzunehmen, meldet euch gern vorher an unter: jonathan@adi-leipzig.net

*Vanessa Thompson / Daniel Loick (Hrsg.), Abolutionismus – Ein Reader, Berlin: Suhrkamp 2022.*