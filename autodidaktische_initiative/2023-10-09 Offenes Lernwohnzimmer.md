---
id: offenes-lernwohnzimmer-2023-10-09
title: Offenes Lernwohnzimmer
start: 2023-10-09 12:00
address: Georg-Schwarz-Straße 19
link: https://adi-leipzig.net/offenes-lernwohnzimmer/
isCrawled: true
---
**Termin:** am 09.10 um 12:00 Uhr* jeden Montag*.

<figure class="wp-block-image size-large"><img decoding="async" fetchpriority="high" width="1024" height="1024" src="http://adi-leipzig.net/wp-content/uploads/Lernwohnzimmer_Instakachel-1024x1024.png" alt="" class="wp-image-5617" srcset="https://usercontent.one/wp/adi-leipzig.net/wp-content/uploads/Lernwohnzimmer_Instakachel-1024x1024.png 1024w, https://usercontent.one/wp/adi-leipzig.net/wp-content/uploads/Lernwohnzimmer_Instakachel-300x300.png 300w, https://usercontent.one/wp/adi-leipzig.net/wp-content/uploads/Lernwohnzimmer_Instakachel-150x150.png 150w, https://usercontent.one/wp/adi-leipzig.net/wp-content/uploads/Lernwohnzimmer_Instakachel-768x768.png 768w, https://usercontent.one/wp/adi-leipzig.net/wp-content/uploads/Lernwohnzimmer_Instakachel.png 1080w" sizes="(max-width: 1024px) 100vw, 1024px"></figure>



Unsere Öffnungszeiten (Mo/Di 12-17.00) sind einen offener Raum zum studieren, arbeiten und verabreden. Kommt gern vorbei wenn ihr nicht allein arbeiten wollt, einen Raum benötigt oder die Option zum sozialen Austausch schätzt. Nutzt die Öffnungszeiten auch gern um unsere Räume für eure Projekte anzufragen und zu besichtigen. Leider sind unsere Räume im Moment nicht barrierefrei – am Eingang ist eine kleine Schwelle zu überwinden, die nicht Rolli-gerechte Toilette ist nur über eine vierstufige Treppe erreichbar.