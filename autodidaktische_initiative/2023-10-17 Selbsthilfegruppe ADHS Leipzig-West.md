---
id: adhs-west-gruppe-2023-10-17
title: Selbsthilfegruppe ADHS Leipzig-West
start: 2023-10-17 17:00
address: Georg-Schwarz-Straße 19
link: https://adi-leipzig.net/adhs-west-gruppe/
isCrawled: true
---
**Termin:** am 17.10 um 17:00 Uhr* jede gerade Woche am Dienstag*.



Wir sind eine selbstorganisierte Selbsthilfegruppe für Menschen mit ADHS im Leipziger Westen, die im Zweiwochentakt – jeweils dienstagsabends in den geraden Wochen – zusammenkommt.

Die Gruppe trifft sich, um sich zu diversen Oberthemen rund um das Leben mit Neurodivergenz auszutauschen. Unser Ziel ist dabei sowohl Psychoedukation, als auch der freie Austausch von persönlichen Erfahrungen und Bewältigungsstrategien.

Um einen sicheren, vertrauensvollen Raum zu schaffen, in dem wir uns langsam kennenlernen und einander öffnen können, laden wir aktuell nur einmal monatlich (jeweils dem vierten Dienstag im Monat) und nach Anmeldung 2-3 neue Interessierte zu einem Kennenlernen mit der Gruppe ein.

Dazu sind alle Betroffenen von ADHS, egal ob diagnostiziert oder (noch) nicht, herzlich willkommen, denen an einem respektvollen und vertraulichen Miteinander gelegen ist.

Melde Dich bei Interesse oder offenen Fragen gern bei uns unter [adhs.leipzig-west@web.de](<mailto:adhs.leipzig-west@web.de>).

Wir freuen uns auf Dich!