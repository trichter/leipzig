---
name: KonsumGlobal Leipzig
website: http://www.globalisiert.de/
email: info@konsumglobal-leipzig.de
scrape:
  source: facebook
  options:
    page_id: 108961192479759
---
Mit KonsumGlobal Leipzig möchten wir Kindern und Jugendlichen die Themen Konsum, Globalisierung und Nachhaltigkeit näher bringen. Wir wollen die KonsumentInnen von morgen schon heute ermuntern, einen kritischen Blick hinter die glitzernde Werbefassade von Alltagsprodukten zu werfen. 