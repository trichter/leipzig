---
id: "155889493871109"
title: "Collective Living: An interview with the co-founders of a 10-year house
  project"
start: 2023-05-14 15:00
end: 2023-05-14 16:00
locationName: Pizza LAB
address: Georg-Schwarz-Straße 10, Leipzig
link: https://www.facebook.com/events/155889493871109/
image: 344760122_168963532775793_8462555275889006496_n.jpg
isCrawled: true
---
If you've ever considered living communally, but you have no idea where to start, this is the event for you. 

Collective living is a solution to many problems. The real estate market puts renters in competition, and the obvious solution is to unionize. Collective ownership of real estate is possible, and much, much less expensive than renting. 

Plus, collective living offers other advantages, such as a sense of belonging, sharing resources, and a ready pool of allies to help one confront the random challenges of life. 

So, why does it sound like a pipe dream to most of us?

Watch a live interview with the co-founders of the Schwarz10 house, a 10-year-old project house in Leipzig, and get an insight into the ups and downs of collective living.

ABOUT US: Schwarz10 is a house project founded in 2012. Every Sunday at 15:00 in Pizza Lab (our non-profit vegan pizzeria), we host live talks and interviews on a topic related to alternatives to capitalism, from understanding the financial system to cultivating awareness to building a network of allies.