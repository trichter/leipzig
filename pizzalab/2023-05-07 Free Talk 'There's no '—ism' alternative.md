---
id: "253701473857327"
title: "Free Talk: 'There's no '—ism' alternative to capitalism'"
start: 2023-05-07 15:00
end: 2023-05-07 16:00
locationName: Pizza LAB
address: Georg-Schwarz-Straße 10, Leipzig
link: https://www.facebook.com/events/253701473857327/
image: 344537532_576197554334937_8702254321413734182_n.jpg
isCrawled: true
---
The problem with considering a reasonable alternative to capitalism is that most people try to find another ism to subscribe to. But the solution isn't an ism. We invite you to a presentation on some work-arounds to capitalism – the good news is that the precedents exist and have been tested.

ABOUT US: Schwarz10 is a house project founded in 2012. Every Sunday at 15:00 in Pizza Lab (our non-profit vegan pizzeria), we host live talks and interviews on a topic related to alternatives to capitalism, from understanding the financial system to cultivating awareness to building a network of allies.