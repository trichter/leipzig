---
name: Pizza LAB
website: https://www.pizzalab.de/
email: kontakt@pizzalab.de
scrape:
  source: facebook
  options:
    page_id: pizzalableipzig
---
Pizza Lab is a non-profit project. A team of volunteers coming from Leipzig and every corner of the world is responsible for all the magic you see at the Lab. The profits of every month are donated to local projects that focus on sustainability, ecology, vegan nutrition and animal rights, or are used to organize events that contribute to improve ou...