---
id: "771842221110602"
title: "Systems Awareness: The Final Trip (free talk)"
start: 2023-05-21 15:00
end: 2023-05-21 16:00
locationName: Pizza LAB
link: https://www.facebook.com/events/771842221110602/
image: 344864154_124941387244464_4045932726017104261_n.jpg
isCrawled: true
---
Psychedelics enlarge one's perspective. One may take in a broader view than before, and take more 'alien' perspectives into account. From this broader perspective, one can understand how one is part of a larger system, and how multiple systems work together.

But maintaining a broader perspective is challenging, and the mind tends to default to what is morally “right” and “wrong” in order to decide how to act. There is a tendency to gravitate toward short-term and individual-based solutions, such as focusing on our own survival, which can lead to long-term problems.

We are at an impass.

Arguably, the future of humanity rests on our ability to perceive and understand in broader contexts. Is this happening because more people have access to education than ever before? No. Perspective-ranging is not to be taught.

Yet, it is to be hinted at. Join us, on 21 May at 15:00 in the Pizza Lab, for a lively exchange of experience.

ABOUT US: Schwarz10 is a house project founded in 2012. Every Sunday at 15:00 in Pizza Lab (our non-profit vegan pizzeria), we host live talks and interviews on a topic related to alternatives to capitalism, from understanding the financial system to cultivating awareness to building a network of allies.