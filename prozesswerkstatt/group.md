---
name: ProzessWerkstatt
website: https://www.prozesswerkstatt-leipzig.org
email: kontakt@prozesswerkstatt-leipzig.org
scrape:
  source: prozesswerkstatt
---
Die Prozesswerkstatt ist ein emanzipatorisches Bildungs- und Beratungskollektiv.

Wir haben das Ziel, die Fähigkeit zu kooperativer Kommunikation und konstruktiver Konfliktbearbeitung von Initiativen, Vereinen, Gruppen und Teams zu verbessern, die für eine sozial und ökologisch gerechtere Gesellschaft eintreten. Um dieses Ziel zu erreichen, bieten wir Workshops und Gruppenprozessbegleitung an. Außerdem erforschen, sammeln und entwickeln wir Methoden und Inhalte, die Gruppen und Teams dabei unterstützen Konflikte gemeinsam Konflikte zu bearbeiten und konstruktiv zusammen zu arbeiten.