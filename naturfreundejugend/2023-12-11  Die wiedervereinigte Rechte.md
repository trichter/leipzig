---
id: 20231125T2123Z-1700947382.8209-EO-1225-1
title: " Die wiedervereinigte Rechte"
start: 2023-12-11 19:00
end: 2023-12-11 21:00
address: Ort auf Anfrage
link: https://naturfreundejugend-leipzig.de/events/event/die-wiedervereinigte-rechte/
isCrawled: true
---
Seit einigen Jahren wird gesellschaftlich intensiver über die als Baseballschlägerjahre bezeichneten 90er gesprochen. Die damalige Gewaltwelle entstand jedoch weder im luftleeren Raum, noch sind die West- und Ostdeutschen Entlastungsnarrative allzu glaubhaft. Der Vortrag geht verschiedenen Strängen nach, um die Gewaltwelle der 1990er anhand der Kontinuität rechter Gewalt in Ost- und Westdeutschland seit den 1980ern zu kontextualisieren, einen genaueren Blick auf rechte Diskurse bei den Leipziger Montagsdemos zu werfen, die Frage nach der Anpassung rechter Parteien an die neue Situation zu stellen und nocheinmal genauer auf die Gewalt der 90er zu Blicken.… weiterlesen  “Die wiedervereinigte Rechte”