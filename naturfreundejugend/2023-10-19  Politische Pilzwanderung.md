---
id: 20230926T1323Z-1695734583.8026-EO-1219-1
title: " Politische Pilzwanderung"
start: 2023-10-19 14:00
end: 2023-10-19 16:00
link: https://naturfreundejugend-leipzig.de/events/event/politische-pilzwanderung/
isCrawled: true
---
Welche Pilze lassen sich in einer Bergbaufolgelandschaft finden und welche Rolle spielen Pilze in Kultur und Politik?
Das wollen wir gemeinsam im Rahmen einer kleinen Wanderung durch die Neue Harth herausfinden.
Festes Schuhwerk und bei geeigneter Wetterlage (d.h. ausreichend feucht / warm) Sammelutensilien (Messer/Korb) nicht vergessen!
Treff ist 14 Uhr am Bahnhof Markkleeberg-Gaschwitz https://osm.org/go/0MD0p8tHZ–?m=… weiterlesen  “Politische Pilzwanderung”