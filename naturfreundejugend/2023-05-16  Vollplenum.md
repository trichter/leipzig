---
id: 20230515T1100Z-1684148422.2563-EO-1204-1
title: " Vollplenum"
start: 2023-05-16 19:00
end: 2023-05-16 21:00
link: https://naturfreundejugend-leipzig.de/events/event/vollplenum-9/
isCrawled: true
---
Auch 2023 wieder: Alle zwei Wochen Orgaplenum für alle. Neu dabei? Hier ist dein Startpunkt bei uns mit zu machen, schreib‘ uns einfach eine Mail.
Jedes erste Plenum im Monat mit anschließendem Müßiggang.