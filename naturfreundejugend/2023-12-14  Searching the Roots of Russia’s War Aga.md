---
id: 20231125T2123Z-1700947382.8342-EO-1223-1
title: " Searching the Roots of Russia’s War Against Ukraine in the Post-Soviet
  Nineties"
start: 2023-12-14 18:00
end: 2023-12-14 21:00
address: Meuterei
link: https://naturfreundejugend-leipzig.de/events/event/post-soviet-nineties/
isCrawled: true
---
For a scholar who works with the topics of present day eastern-european history, one particular decade is often more attractive than the others. It is the nineties that seem to have so many mysteries and clues, puzzles and answers, a time period which is clearly already a History yet something always real and present.… weiterlesen  “Searching the Roots of Russia’s War Against Ukraine in the Post-Soviet Nineties”