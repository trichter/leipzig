---
id: 20230808T1101Z-1691492467.2145-EO-1211-1
title: " Veranstaltung: Die Gewalt der Vereinigung"
start: 2023-08-23 19:00
end: 2023-08-23 21:00
link: https://naturfreundejugend-leipzig.de/events/event/veranstaltung-die-gewalt-der-vereinigung/
isCrawled: true
---
In der Nacht des 2. Oktober 1990 feierten Millionen von Deutschen die Wiedervereinigung. Gleichzeitig griff ein rechter Mob vor allem im Osten Deutschlands Heime von Vertragsarbeiter*innen und besetzte Häuser an. 
So schritten in Leipzig am 2. Oktober ca. 150 Neonazis durch die Innenstadt. Sie überfielen Passant*innen und griffen das soziokulturelle Zentrum „Die Villa“ an.… weiterlesen  “Veranstaltung: Die Gewalt der Vereinigung”