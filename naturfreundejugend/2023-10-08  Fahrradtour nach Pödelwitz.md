---
id: 20230926T1300Z-1695733222.9286-EO-1217-1
title: " Fahrradtour nach Pödelwitz"
start: 2023-10-08 11:00
end: 2023-10-08 18:00
address: Connewitzer Kreuz
link: https://naturfreundejugend-leipzig.de/events/event/fahrradtour-nach-poedelwitz/
isCrawled: true
---
System Change, sozial-ökologische Transformation, gelebte Utopie… Alle reden davon, aber wie können erste Schritte dazu konkret aussehen?
Auf einer Fahrradtour nach Pödelwitz wollen wir uns mit euch und den Bewohner*Innen von Pödelwitz darüber austauschen.
Gemeinsam starten wir um 11 Uhr am Connewitzer Kreuz und fahren am Cospudener See und am Tagebau Schleenhain vorbei weiter nach Pödelwitz.… weiterlesen  “Fahrradtour nach Pödelwitz”