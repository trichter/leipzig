---
id: 20230926T1323Z-1695734583.8125-EO-1218-1
title: " Vorwärts und aufwärts – 100 Jahre 9. Hauptversammlung"
start: 2023-10-28 13:00
end: 2023-10-28 22:00
address: Volkshaus
link: https://naturfreundejugend-leipzig.de/events/event/vorwaerts-und-aufwaerts-100-jahre-9-hauptversammlung/
isCrawled: true
---
100 Jahre 9. Hauptversammlung des Touristenvereins Die NaturFreunde (T.V.D.N.) im Leipziger Volkshaus
Zur Veranstaltungsseite auf naturfreunde.de
13 Uhr: Stadtspaziergang auf den Spuren der Arbeiter:innenbewegung mit Daniela Schmohl (RLS)
18 Uhr: Abendveranstaltung und Podiumsdiskussion, mit musikalischer Begleitung von einem Duo des Chors Pir-Moll im Volkshaus
Podiumsteilnehmer*innen:
Monika Kirst (Geschichtsboden Volkshaus Leipzig)
Karl Heinrich Pohl (Historiker)
Dieter Groß (Historiker, NaturFreunde)
Daniela Schmohl (Historikerin, Rosa-Luxemburg-Stiftung)
„Vorwärts und aufwärts!“… weiterlesen  “Vorwärts und aufwärts – 100 Jahre 9. Hauptversammlung”