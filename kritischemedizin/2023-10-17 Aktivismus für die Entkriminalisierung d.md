---
id: 20632-1697565600-1697576400
title: "Aktivismus für die Entkriminalisierung des  Schwangerschaftsabbruchs:
  Filmvorführung und Vortrag mit „Women on Web“"
start: 2023-10-17 18:00
end: 2023-10-17 21:00
address: Hörsaal Augenklinik (Zugang über Liebigstr. 14)
link: https://www.leipzig.kritmed.de/veranstaltung/aktivismus-fuer-die-entkriminalisierung-des-schwangerschaftsabbruchs-filmvorfuehrung-und-vortrag-mit-women-on-web/
isCrawled: true
---
Vorführung des Films „Vessel“ über „Women on Waves“, eine NGO, die von der niederländischen Gynäkologin Rebecca Gomperts gegründet wurde, und ungewollt Schwangeren in Ländern mit restriktiven Abtreibungsgesetzen Zugang zu Schwangerschaftsabbrüchen ermöglicht. Im Anschluss wird es einen Vortrag zu ihrem Engagement und der aktuellen Versorgungslage sowie die Möglichkeit zum Gespräch mit „Women on Web“-Aktivistinnen aus Deutschland geben.