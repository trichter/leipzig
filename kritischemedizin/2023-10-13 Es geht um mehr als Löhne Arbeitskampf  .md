---
id: 20614-1697216400-1697223600
title: "Es geht um mehr als Löhne: Arbeitskampf  für ein besseres Gesundheitssystem"
start: 2023-10-13 17:00
end: 2023-10-13 19:00
address: Seminarraum 202 (Campus Augustusplatz)
link: https://www.leipzig.kritmed.de/veranstaltung/es-geht-um-mehr-als-loehne-arbeitskampf-fuer-ein-besseres-gesundheitssystem/
isCrawled: true
---
Unnötige Operationen, massive Arbeitsverdichtung, chronischer Personalmangel – alles Folgen der Jahrzehntelangen & politisch gewollten Ökonomisierung im Gesundheitswesen. Wie wir das System von unten umkrempeln, zeigt die Streikbewegung von Pflegekräften, die in mehreren Städten erfolgreich für bessere Arbeitsbedingungen gekämpft haben. Welche Chancen bietet gewerkschaftliche Organisierung im Kampf um bedarfsgerechte Gesundheitsversorgung? Was macht den Streik im Krankenhaus besonders? Wie können wir die Erfahrungen für Leipzig nutzen & solche Kämpfe unterstützen?