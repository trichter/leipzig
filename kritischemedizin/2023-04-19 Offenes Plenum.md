---
id: 20602-1681927200-1681932600
title: Offenes Plenum
start: 2023-04-19 18:00
end: 2023-04-19 19:30
address: Carl-Ludwig-Institut SR4
link: https://www.leipzig.kritmed.de/veranstaltung/offenes-plenum-3/
isCrawled: true
---
Ihr seid interessiert an unserer Arbeit oder wollt Teil von KritMed werden? 
Dann kommt zu unserem offenen Plenum! 
Wir stellen unsere AGs vor: 
x fem*med
x Antirassismus/kritisches Weißsein
x Ökonomisierung im Gesundheitswesen 
Wir freuen uns auf alle Interessierten! Eure kritis <3