---
id: 20621-1697648400-1697655600
title: Kapitalismus im Gesundheitswesen - Workshop und Einführung ins DRG-System
start: 2023-10-18 17:00
end: 2023-10-18 19:00
address: Hörsaal 16, Universitätsstraße 3, Leipzig, 04109, Deutschland
link: https://www.leipzig.kritmed.de/veranstaltung/kapitalismus-im-gesundheitswesen-workshop-und-einfuehrung-ins-drg-system/
isCrawled: true
---
Ob ständige Unterbesetzung, zu frühe Entlassungen oder unnötige Operationen – an etlichen Stellen werden im deutschen Gesundheitssystem Profite über adäquate Patient*innenversorgung gestellt. Wir wollen den Missständen auf den Grund gehen und einen Blick auf Entwicklung und Status Quo des deutschen Systems der Krankenhausfinanzierung – des DRG-Systems – werfen. Es gibt einen inhaltlichen Input und wir werden uns anschließend interaktiv die Auswirkungen von Krankenhausfinanzierungsarten auf Akteur*innen wie Pflegekräfte, Patient*innen und Ärzt*innen erarbeiten. Kein Vorwissen erforderlich!