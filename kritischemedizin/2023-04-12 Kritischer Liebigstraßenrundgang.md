---
id: 20581-1681315200-1681318800
title: Kritischer Liebigstraßenrundgang
start: 2023-04-12 16:00
end: 2023-04-12 17:00
address: Fahrradständer gegenüber UKL Hauptgebäude, Liebigstr. 20
link: https://www.leipzig.kritmed.de/veranstaltung/kritischer-liebigstrassenrundgang/
isCrawled: true
---
Geschichte der Uniklinik Leipzig zur NS-Zeit