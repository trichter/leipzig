---
id: 20616-1697554800-1697562000
title: Rechte Einstellungen und Medizinstudium? - Erfahrungen und
  Handlungsmöglichkeiten
start: 2023-10-17 15:00
end: 2023-10-17 17:00
link: https://www.leipzig.kritmed.de/veranstaltung/rechte-einstellungen-und-medizinstudium-erfahrungen-und-handlungsmoeglichkeiten/
isCrawled: true
---
in Zusammenarbeit mit dem Kulturbüro Leipzig e.V. | Voranmeldung an leipzig@kritmed.de 

Dieser Workshop liefert einen kurzen Einblick in die Dimensionen extrem rechter Strukturen, Erscheinungsformen und Positionen. Zudem öffnen wir den Raum, um eigene Erfahrungen im Rahmen des Medizinstudiums mit rechten Äußerungen z.B. von Mitstudierenden, Lehrenden oder Patient*innen auszutauschen und Handlungsmöglichkeiten zu diskutieren. Wir stellen Beratungsstellen in Leipzig und Sachsen sowie Ansprechpartner*innen an der Medizinischen Fakultät vor.