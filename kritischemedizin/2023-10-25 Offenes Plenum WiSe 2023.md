---
id: 20641-1698260400-1698265800
title: Offenes Plenum WiSe 2023
start: 2023-10-25 19:00
end: 2023-10-25 20:30
address: Seminarraum 6, Liebigstr. 27b, Leipzig
link: https://www.leipzig.kritmed.de/veranstaltung/offenes-plenum-wise-2023/
isCrawled: true
---
Wir laden euch herzlich zu unserem offenen Plenum ein! Wir werden uns und unsere AGs in Ruhe vorstellen und alle Fragen und Anmerkungen beantworten. Kommt ganz unverbindlich, wir freuen uns 🙂