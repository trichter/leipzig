---
id: 20577-1682013600-1682019000
title: Vortrag "Hürden in der Gesundheitsversorgung von Menschen ohne Papiere"
start: 2023-04-20 18:00
end: 2023-04-20 19:30
address: Carl-Ludwig-Institut SR4
link: https://www.leipzig.kritmed.de/veranstaltung/vortrag-huerden-in-der-gesundheitsversorgung-von-menschen-ohne-papiere/
isCrawled: true
---
Vortrag von CABL e.V.