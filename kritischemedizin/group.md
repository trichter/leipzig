---
name: Kritische Medizin Leipzig
website: https://www.leipzig.kritmed.de
email: leipzig@kritmed.de
scrape:
  source: ical
  options:
    url: https://www.leipzig.kritmed.de/veranstaltungen/?ical=1
---
Wir sind eine Hochschulgruppe der Universität Leipzig und zusammen bilden wir die Arbeitsgruppe „Kritische Medizin Leipzig“, die Menschen aus dem Gesundheitswesen vereint.
Wir haben uns zur Aufgabe gemacht, sämtliche Prozesse, Abläufe und Strukturen in Bezug auf gesundheitspolitische Fragen kritisch zu beleuchten.