---
id: 20626-1697122800-1697130000
title: Revolution für unsere Gesundheit - aber nicht mit Lauterbachs Reform!
start: 2023-10-12 15:00
end: 2023-10-12 17:00
address: Seminarraum 204 (Campus Augustusplatz), Universitätsstraße 1, Leipzig, 04109
link: https://www.leipzig.kritmed.de/veranstaltung/revolution-fuer-unsere-gesundheit-aber-nicht-mit-lauterbachs-reform/
isCrawled: true
---
„Wir stehen am Vorabend einer Revolution“, damit kündigte Gesundheitsminister Lauterbach die Krankenhausreform großspurig an, doch Spoiler: revolutionär ist die Reform nicht. An der zugrunde liegenden Profitlogik im Gesundheitssystem wird nicht gerüttelt. Im Gegenteil, private Konzerne werden weiter Profite mit unsere Gesundheit und auf Kosten der Beschäftigten machen können.