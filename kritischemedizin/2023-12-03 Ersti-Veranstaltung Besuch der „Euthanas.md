---
id: 20637-1701588600-1701622800
title: "Ersti-Veranstaltung: Besuch der „Euthanasie“-Gedenkstätte Bernburg"
start: 2023-12-03 07:30
end: 2023-12-03 17:00
link: https://www.leipzig.kritmed.de/veranstaltung/ersti-veranstaltung-besuch-der-euthanasie-gedenkstaette-bernburg-2/
isCrawled: true
---
Sonntag, 03.12.23 | 7:30 bis ca. 17 Uhr | Treffpunkt Leipzig Hbf, Gleis 11
 
Dieses Jahr bieten wir wieder einen Besuch der Gedenkstätte für die Opfer der NS-„Euthanasie“ am Klinikum Bernburg an. Die Veranstaltung richtet sich insbesondere an Studierende aus dem ersten Semester, aber natürlich sind auch Studierende aus höheren Semestern herzlich willkommen. Wir reisen gemeinsam mit dem Zug an, Treffpunkt ist um 7:30 Uhr am Hauptbahnhof Leipzig (Gleis 11). Zurück sein werden wir voraussichtlich gegen 17 Uhr. Die Führung ist kostenlos, es fallen ggf. nur die Kosten für das DB-Sachsenticket an (für alle ohne Deutschlandticket, abhängig von der genauen Teilnehmer*innenanzahl 12-15 EUR).  
Rund 14.000 Patientinnen und Patienten aus Heil-und Pflegeanstalten sowie Häftlinge aus den Konzentrationslagern Buchenwald, Flossenbürg, Groß-Rosen, Neuengamme, Ravensbrück und Sachsenhausen starben allein in Bernburg. Im Spätsommer 1943 wurde die „Euthanasie“-Anstalt Bernburg geschlossen. Die baulichen Überreste der Vernichtungsanlage blieben zum Teil erhalten, darunter die Gaskammer (https://gedenkstaette-bernburg.sachsen-anhalt.de/). 
Anmeldung an leipzig@kritmed.de | Maximale Teilnehmer:innenzahl 23 Personen – first come, first serve!