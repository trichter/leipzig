---
id: 352-1698498000
title: Furcht und Hoffnung (Bim o Omid)
start: 2023-10-28 15:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/furcht_und_hoffnung__bim_o_omid_
image: _DSF2386.jpeg
isCrawled: true
---
Aus ihren unbeschwerten Kinderspielen herausgerissen, ergeben sich neun junge Afghan\*innen den Entscheidungen ihrer Eltern. Ihnen bleibt keine Wahl über ihre eigene Zukunft. Von ihrem ganzen bisherigen Leben können sie auf dem Weg nach Europa nur mitnehmen, was in einen Rucksack passt. Doch auch in finsteren Zeiten findet sich Hoffnung. Mit Tanz, Poesie und Theater erzählen die Schauspieler\*innen ihre Geschichten und gewinnen dabei Stück für Stück die Macht über ihre eigenen Narrative zurück.



 زندگ مان را در کوله پشتی هامان گذاشتند و جز این تصمیمی نمیتوانستیم بگیریم، ما کودکانی بی اختیار و آینده ای مه آلود در یک اجرای تئاتر قدرتمند متاثرکننده، نقشگردانان جوان افغان تجربیات شخصی و عمیقخود را به نمایش میگذارند 



Das Stück ist auf Persisch/Dari mit deutschen Übertiteln. 



 Es spielen: **Atifa Gardi, Masumah Haydari, Masumah Mohammadi, Meysam Habibi, Mohammad Reza Wakili, Mohsen Ahmadi, Reyhane Gardi, Zahra Gardi & Zahra Mousawi **



 Regie: **Frishteh Sadati ** 

 Kostüm und Bühnenbild: **Frishteh Sadati** 

 Licht: **Sahar Sharifi** 

 Sound: **Diverse Marwi** 

 Assistenz: **Zainab Mohammadi** 

 Übertitel & Produktion: **Livia Lück** 



Im Anschluss findet ein Gespräch mit dem Publikum statt und der Nachmittag klingt mit Tanz & Musik aus.

**Dauer:** 180min
                                                                    
                                    (mit Pause)