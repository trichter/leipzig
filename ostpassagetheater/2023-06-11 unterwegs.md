---
id: 313-1686506400
title: unterwegs
start: 2023-06-11 20:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/unterwegs
image: unterwegs_copyright_by_Lori_Heart.jpg
isCrawled: true
---
Es geht ums Pissen, das fehlende Leipziger Toilettenkonzept, belastendes Gepäck, Bewegungen durch den öffentlichen Raum, unangenehme Begegnungen und um Orte zum Durchatmen. Unterwegs als FLINTA\* ist das zentrale Motiv der interdisziplinären Performance. Im Schnitt sind die Performerinnen der Company ein Drittel ihrer Zeit unterwegs, davon werden ca. 1000 Stunden im Jahr in öffentlichen Verkehrsmitteln verbracht. Was erleben sie in dieser Zeit? Ist der Weg das Ziel oder bleibt die Company auf dem Rastplatz stecken?



Es spielen: **Clara-Marie Rutjes, Karoline Weber, Lotta Steenbeck, Lori Heart, Marie Batzdorf, Mathilde Olschowsky, Paulina Rübenstahl & Sophie-Marie Hertel**



Künstlerische Leitung: **Lea Hoffmann**



Kamera: **Fa Maszlanka**



Technik: **Joseph Naumann**



Im Anschluss an die Premiere am 09.06. sind alle Gäste zur Premierenfeier eingeladen!



*Die Performance ist Teil des 14. KAOS-Kultursommers, der seit 2010 von der Kulturwerkstatt KAOS – einem Projekt der KINDERVEREINIGUNG Leipzig e.V. – veranstaltet wird. Die Reihe voller Theater, Musik und Familienveranstaltungen wird vom Kulturamt der Stadt Leipzig, der Leipzigstiftung und vielen privaten Unterstützer\*innen gefördert. Die Kulturwerkstatt KAOS ist auf dem malerischen Gelände der Wasserstraße 18 im Leipziger Westen beheimatet. Die Performance „unterwegs“ wird passend zum Namen auf der anderen Seite der Stadt, im Ost-Passage Theater in der Konradstraße 27 (Zugang über die Eisenbahnstraße) aufgeführt.*

**Dauer:** 60min