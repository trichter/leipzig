---
id: 224-1694800800
title: "KLUB-EVENING: Infos, Fudder & Karaoke"
start: 2023-09-15 20:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/klub_evening__infos__fudder___karaoke
image: Pull-Over_copyright_by_OPT.jpg
isCrawled: true
---
Herzlich willkommen zum ersten Klubanend der 6. Spielzeit. Du willst wissen, was das *Ost-Passage Theater* tut und wie es funktioniert? Dann komm zum Kulturmampf und lass es dir in gemütlicher Runde von Mitwirkenden erklären. Keine Spielchen, keine Erwartungen. Mit anschließender Karaoke. Everybody is welcome :)

**Dauer:** 180min