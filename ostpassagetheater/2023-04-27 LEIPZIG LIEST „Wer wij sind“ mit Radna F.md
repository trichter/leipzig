---
id: 292-1682614800
title: "LEIPZIG LIEST: „Wer wij sind“ mit Radna Fabias, Valentijn Hoogenkamp &
  Eva Meijer"
start: 2023-04-27 19:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/leipzig_liest__%E2%80%9Ewer_wij_sind%E2%80%9C_mit_radna_fabias__valentijn_hoogenkamp___eva_meijer
image: Radna_Fabias_+_Valentijn_Hoogenkamp_+_Eva_Meijer_copyright_by_Wouter_le_Duc_+_Lenny_Oosterwijk_+_Irwan_Droog.jpg
isCrawled: true
---
Wer sind wij? Wie sieht unsere Welt aus, kann ich diese Welt selbst gestalten, und wenn ja, wie klingt das, wie fühlt sich das an? Drei niederländische Schriftsteller\*innen finden Antworten auf diese drängenden Fragen. **Radna Fabias’** preisgekrönter Gedichtband “Habitus” *(Elif Verlag)* folgt einer Migrantin zurück in ihr Heimatland, die Antillen, und findet dafür originelle Bilder, ein raues Sprachregister und einen mitreißenden Rhythmus. **Valentijn Hoogenkamp** debütierte mit dem gefeierten Roman “Ich und Louis Claus” *(Atlantik Verlag)*, in dem ein junges Mädchen sich Hals über Kopf in einen Mitschüler verliebt, ihn aus den Augen verliert und nach 18 Jahren wieder trifft. Die Schriftstellerin und Philosophin **Eva Meijer** hat mit “Die Grenzen meiner Sprache” *(Verlag btb)* einen Essay über die heilende Kraft der Sprache geschrieben, der aber auch von der Kunst des Laufens, von Hunden und Katzen und Bäume im Winter erzählt.



**Svenja Gräfen und Kais Harrabi** werden durch diesen schillernden Abend führen. Svenja Gräfen ist selbst Autor\*in, zuletzt erschien der Bestseller “Radikale Selbstfürsorge Jetzt. Eine feministische Perspektive”. Kais Harrabi ist Journalist, Kritiker und Podcaster, der sich vor allem für Literatur, Kunst und Digitales interessiert.

**Dauer:** 180min
                                                                    
                                    (mit Pause)