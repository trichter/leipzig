---
id: 347-1697911200
title: Analyse:Paralyse - Festival für politische Kunst.
start: 2023-10-21 20:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/analyse_paralyse___festival_fuer_politische_kunst_
image: hoch_15x10cm.jpg
isCrawled: true
---
Der *Analyse:Paralyse* Verein veranstaltet auch dieses Jahr – in kleinerem Rahmen, aber in gewohnt qualitativer Zusammensetzung – ihr Festival für politische Kunst im breitgefächerten kulturellen und kreativen Sektor. Simultan zu verschiedenen Workshops, Panels und interaktiven sowie partizipativen Angeboten mit dem Theaterkollektiv *gruppe tag*, der Designerin **Luise Hesse**, der* Krudebude* und vielen anderen Kulturschaffenden, die sich am Wochenende in der *Garage Ost* zusammenfinden, schlägt dafür am Samstagabend im *Ost-Passage Theater* darstellende Kunst in Reinform durch ein Performance - Wrestlingkollektiv auf die Latten. Es wird heiß, es wird politisch, den Rest macht ihr! Der genaue Programmablauf von SA 12:00 Uhr bis SO 22:00 Uhr steht noch nicht fest. Watch out!



**Zur Homepage von *Analyse:Paralyse*:** [https://analyse-paralyse.net/festival/](<https://analyse-paralyse.net/festival/>)