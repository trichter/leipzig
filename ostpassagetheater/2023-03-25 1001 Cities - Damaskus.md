---
id: 276-1679770800
title: 1001 Cities - Damaskus
start: 2023-03-25 20:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/1001_cities___damaskus
image: Four_Acts_for_Syria1_copyright_by_Waref_Abu_Quba.jpg
isCrawled: true
---
Veranstaltungsreihe, die Dich in die Erlebniswelt unterschiedlicher internationaler Städte entführt. Los geht es am 25.03. in Damaskus. Wir laden Dich ein zuzuschauen, zuzuhören, auszuprobieren und zu schmecken. Erinnerungen, Gedanken und Geschichten um Damaskus, die wir bei einem gemeinsamen Essen mit Dir teilen möchten. Wir freuen uns auf einen gemeinsamen Abend, mit Gesprächen am Tisch, einem Kurzfilm von Waref Abu Quba und einem tanzbaren Ende.

**Dauer:** 180min
                                                                    
                                    (mit Pause)