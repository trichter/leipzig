---
id: 224-1686938400
title: "KLUB-EVENING: Infos & Fudder"
start: 2023-06-16 20:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/klub_evening__infos___fudder
image: Pull-Over_copyright_by_OPT.jpg
isCrawled: true
---
OPT-Klub? Was ist das denn bitte? Noch nie davon gehört? Das könnte an einer rezenten Pandemie liegen und daran, dass wir mit Dir/euch den Klub definieren. Also komm zu einer gemütlichen Runde, mit Küfa und Infos. Lern die Arbeitsgruppen kennen, finde heraus, wie Du helfen kannst. Komm zum Klubabend!

**Dauer:** 180min