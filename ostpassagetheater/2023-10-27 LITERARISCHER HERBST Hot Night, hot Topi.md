---
id: 351-1698429600
title: Hot Night, hot Topics – DIE HOT TOPIC HOT NIGHT
start: 2023-10-27 20:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/hot_night__hot_topics_%E2%80%93_die_hot_topic_hot_night
image: HotTopic.jpg.webp
isCrawled: true
---
Eine Lesung, bei der es knistert, prickelt und rauscht: Wir feiern das Release der vierten Ausgabe von *Hot Topic!



 Hot Topic!* ist ein in regelmäßigen Abständen erscheinendes, literarisch anspruchsvolles Heft im Stil des Groschenromans. Außernormative Erotiktexte von verschiedenen Autor:innen werden von wechselnden Künstler:innen illustriert. Sex und Erotik, Feelings, Kink, Desire und fuckable fantasies – was berührt und kitzelt dich und mich? Autor:innen der vierten Ausgabe lesen aus ihren Texten, die scharfsinnig von Tobia König illustriert wurden. Wir freuen uns außerdem auf die Comiclesung von **Lina Ehrentraut** – soziale und emotionale Aspekte zwischenmenschlicher Beziehungen, Sex und Selbstzweifel sind hier die Themen. Nach der Lesung prickelts in den Gläsern! Wir stoßen mit euch an: kühler Sekt, spritzige Gespräche, perlige Stimmung – und feiern den Abend, die Literatur, die LeseLust.



**Und hier lang geht's zur Homepage von * Hot Topic!*:** [https://hottopicheft.com/](<https://hottopicheft.com/>)

**Dauer:** 180min