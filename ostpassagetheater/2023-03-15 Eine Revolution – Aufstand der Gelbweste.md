---
id: 273-1678906800
title: Eine Revolution – Aufstand der Gelbwesten
start: 2023-03-15 20:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/eine_revolution_%E2%80%93_aufstand_der_gelbwesten
image: Eine_Revolution_copyright_by_Drop-Out_Cinema_eG.jpeg
isCrawled: true
---
Der Dokumentarfilm begleitet vier Protagonist:innen der sogenannten „Gelbwestenbewegung“, der Protestbewegung, die nach der Steuererhöhung auf die Spritpreise durch die Regierung von Emmanuel Macron ganz Frankreich überzog. Regisseur Emmanuel Gras versucht sich dabei den Fragen anzunähern: Wer sind diese wütenden Menschen? Was sind ihre Motive und Ziele? Dafür hat er 6 Monate mit Ihnen verbracht. *(Gras, F 2022 – FSK 12 – OmdU)*



Im Anschluss findet ein vom *SDS Leipzig* moderiertes Gespräch zum Thema mit dem Publikum statt.



*„EINE REVOLUTION erscheint zum richtigen Zeitpunkt, einige Monate vor den nächsten Präsidentschaftswahlen, um uns daran zu erinnern, dass nicht nur ein großer Teil der französischen Bevölkerung leidet, sondern dass neue Formen des Protests möglich sind, sofern sie richtig kanalisiert werden. Der Dokumentarfilm von Emmanuel Gras ist ein lebendiges Zeugnis dieses Kampfes, ohne zu polemisieren und auch ohne ihre allzumenschlichen Fahnenträger übermäßig zu idealisieren.“* (Critique Film, FR)



Wir zeigen den Film in der Originalsprache auf Französisch mit deutschen Untertiteln. 



**Mehr Informationen:** [https://dropoutcinema.org/archive/3507/](<https://dropoutcinema.org/archive/3507/>)



**Hier geht's zum Trailer auf *YouTube*:** [https://youtu.be/zOOBYFLYYCY](<https://youtu.be/zOOBYFLYYCY>)

**Dauer:** 150min