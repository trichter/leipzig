---
id: 122-1699552800
title: "Stolpersteine: Familie Weißblüth und Samuel Hundert"
start: 2023-11-09 19:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/stolpersteine__familie_weissblueth_und_samuel_hundert
image: Stolpersteine_für_Familie_Weißblüth_und_Hundert_cc_by_Erich_Zeigner_Haus.jpg
isCrawled: true
---
Am 09. November 2023 jährt sich zum 85. Mal die Nacht der nationalsozialistischen Novemberpogrome im Deutschen Reich. Wir möchten an die faschistische Gräueltat an Familie Weißblüth und Samuel Hundert erinnern. Du bist eingeladen, Dich mit uns um 19:00 Uhr Höhe Eisenbahnstr. 47, gegenüber Zugang zum Parkplatz (ehem. Melchiorstr.) zu versammeln (Stolperstein Nr. 185). Wir werden etwas zur Biografie der Familien erzählen, ihre Stolpersteine putzen und anschließend gemeinsam schweigen.



 Alle Infos zum Stolpersteinputzen in Leipzig: [https://erich-zeigner-haus-ev.de/neunter-november/](<https://erich-zeigner-haus-ev.de/neunter-november/>)

**Dauer:** 20min