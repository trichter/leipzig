---
id: 285-1681059600
title: „Die Linke in China“ – mit Ralf Ruckus
start: 2023-04-09 19:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/%E2%80%9Edie_linke_in_china%E2%80%9C_%E2%80%93_mit_ralf_ruckus
image: Die-Linke-in-China-Umschlag_copyright_by_Mandelbaum_Verlag.jpg
isCrawled: true
---
Weltweit werden in der Linken unterschiedliche Positionen zur *Volksrepublik China* und zur dortigen *Kommunistischen Partei* diskutiert. Manche halten das heutige China noch für sozialistisch, andere erkennen die kapitalistische Ausbeutung und die Unterdrückung sozialer Kämpfe durch das jetzige Regime. Kaum diskutiert werden die linken oppositionellen Bewegungen, die es seit Gründung der Volksrepublik gegeben hat und bis heute gibt. Im Zentrum der revolutionären Umwälzung der 1950er-Jahre stand zwar der Versuch der *Kommunistischen Partei*, ein sozialistisches System aufzubauen. Ihr Sozialismus schuf jedoch neue Klassenspaltungen und in der Folge Wellen sozialer Proteste von Arbeiter:innen, Migrant:innen und Frauen\*. Aus diesen Protesten gingen jeweils linke Gruppen und Bewegungen hervor, die sich gegen das Regime stellten. Diese Dialektik von sozialen Kämpfen und linken Oppositionsbewegungen prägte die Geschichte der Volksrepublik. 



Sie stehen im Zentrum des neuen Buches von** Ralf Ruckus**, das er an diesem Abend vorstellt, und anschließend auch für ein Gespräch mit dem Publikum zur Verfügung steht.



**Zur Homepage des Verlages:** [https://www.mandelbaum.at/buecher/ralf-ruckus/die-linke-in-china/](<https://www.mandelbaum.at/buecher/ralf-ruckus/die-linke-in-china/>)



**Hier geht's zum Blog von Ralf Ruckus:** [https://nqch.org/](<https://nqch.org/>)

**Dauer:** 90min