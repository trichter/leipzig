---
id: 224-1700416800
title: "KLUB-EVENING: Kulturmampf"
start: 2023-11-19 19:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/klub_evening__kulturmampf
image: Pull-Over_copyright_by_OPT.jpg
isCrawled: true
---
OPT-Klub? Was ist das denn bitte? Noch nie davon gehört? Also komm zu einer gemütlichen Runde, mit Kulturmampf und Infos. Lern die AG’s kennen, finde heraus wie du Teil vom OPT werden kannst! Komm zum Klubabend! Everybody is welcome :)

**Dauer:** 180min