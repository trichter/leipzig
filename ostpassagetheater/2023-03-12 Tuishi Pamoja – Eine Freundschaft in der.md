---
id: 271-1678629600
title: Tuishi Pamoja – Eine Freundschaft in der Savanne
start: 2023-03-12 15:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/tuishi_pamoja_%E2%80%93_eine_freundschaft_in_der_savanne
image: tuishi_pamoja_copyright_by_Fidula-Verlag_Holzmeister_GmbH.jpg
isCrawled: true
---
Machen Streifen wirklich doof? Und stimmt es, dass man mit langhalsigen Tieren sowieso nicht vernünftig reden kann? Wichtige Fragen für das Giraffenkind Raffi und das kleine Zebra Zea. Seit Jahren leben die Herden nebeneinander, aber denkt ihr, sie würden miteinander reden? Zum Glück sind da noch die pfiffigen Erdmännchen. Und der Angriff der Löwen, der ausnahmsweise mal was Gutes bewirkt. Eine Geschichte über Vorurteile, Freundschaft und Toleranz.

**Dauer:** 45min