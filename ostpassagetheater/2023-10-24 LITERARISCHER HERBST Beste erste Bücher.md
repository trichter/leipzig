---
id: 348-1698170400
title: "LITERARISCHER HERBST: Beste erste Bücher"
start: 2023-10-24 20:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/literarischer_herbst__beste_erste_buecher
image: 211026_lh_21_ost_passage_13.1600x1200.jpg
isCrawled: true
---
Ein großer Abend der Romandebüts. Aus fünf besonders vielversprechenden Erstlingen wird gelesen. Bei der Auswahl wurde auf eine möglichst große Bandbreite geachtet: Es geht um das rauschhafte München der 80er- Jahre zwischen Hedonismus und Biederkeit, Clubnächte und AIDS (**Lion Christ**: „Sauhund“, *Hanser*), um die erste große Liebe, Unschuld und Verrat in Dresden-Gittersee 1976 (**Charlotte Gneuß**: „Gittersee“, *S. Fischer* – nominiert für den *Deutschen Buchpreis*!), um die besondere Hausgemeinschaft von Aussiedler:innen in einem Hochhaus am Waldrand (**Birgit Mattausch**: „Bis wir Wald werden“, *Klett-Cotta*), um politischen Mut, Verfolgung und die gefährliche Reise einer jungen Frau in die heimliche Metropole der Kurden (**Beliban zu Stolberg**: „Zweistromland“, *Kanon*) und um eine mitreißende Familiengeschichte auf drei Kontinenten zwischen jüdischer Tradition und deutschem „Gedächtnistheater“ (**Dana Vowinckel**: „Gewässer im Ziplock“, *Suhrkamp*). Moderiert wird das Ganze von **Linn Penelope Rieger**. Wer sich für die Stimmen von Morgen interessiert, sollte sich diesen Abend freihalten!



**Zum gesamten Programm des *Literarischen Herbstes 2023* geht's hier lang:** [https://literarischer-herbst.com/programm/](<https://literarischer-herbst.com/programm/>)

**Dauer:** 180min
                                                                    
                                    (mit Pause)