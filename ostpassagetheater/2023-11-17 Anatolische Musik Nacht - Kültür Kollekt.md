---
id: 363-1700247600
title: Anatolische Musik Nacht - Kültür Kollektiv Event
start: 2023-11-17 20:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/anatolische_musik_nacht___kueltuer_kollektiv_event
image: KüKo_cc_Jaco_Salva.jpg
isCrawled: true
---
Das *Kültür Kollektiv Leipzig* bringt die unterschiedlichsten Musikarten aus Anatolien auf die Bühne. Der Abend wird eine Nacht voller Klang und Harmonie zwischen den Kulturen. All die unterschiedlichen Wurzeln führen uns zusammen wenn es heißt: Musik, Vergnügung und Frieden!	



**Zur Homepage vom *Kültür Kollektiv Leipzig*:** [https://www.kuko-leipzig.de/](<https://www.kuko-leipzig.de/>)

**Dauer:** 240min
                                                                    
                                    (mit Pause)