---
id: 183-1677870000
title: Der Lippenstiftkongress
start: 2023-03-03 20:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/der_lippenstiftkongress
image: _RRR6437.jpg
isCrawled: true
---
In einer Welt in der der Lippenstifte zentraler Alltagsgegenstand ist, lädt Sie die mächtige Monopolfirma "Lipphold Industries" herzlich zum alljährlichen Lippenstiftkongress ein. Auf der LipCon 2023 soll nicht nur das farbenfrohe Kosmetikobjekt exploriert, sondern auch persönliche Fragen nach Suche, Identität und Sein in einer Welt der schier unendlichen Reize und Möglichkeiten verhandelt werden. Dabei begleiten wir die junge Journalistin Kleo Klosewitz, die sich mit ihrem besten Freund Schorsch auf den Kongress begibt. Sie ist investigativ unterwegs und will herausfinden, inwieweit das Verschwinden ihrer Schwester mit Lipphold Industries zusammenhängt. Auf dem Weg nach Antworten, die Kleo erst im Außen und dann im Inneren sucht, begegnet sie spannenden Personen, gerät in ungewöhnliche Siutationen und wird einige Male auf die Probe gestellt.



 Der Lippenstiftkongress ist eine abwechslungsreiche, unterhaltende und vierdimensionale Erfahrung, die zum Lachen, Grübeln und Mitfühlen anregen, vor allem aber Lust auf bunte Lippen machen soll.

**Dauer:** 120min