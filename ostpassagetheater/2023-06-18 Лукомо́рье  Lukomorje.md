---
id: 61-1687078800
title: Лукомо́рье // Lukomorje
start: 2023-06-18 11:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/%D0%9B%D1%83%D0%BA%D0%BE%D0%BC%D0%BE%CC%81%D1%80%D1%8C%D0%B5____lukomorje
image: Lukomorje_cc_by_Vektor_Schule.jpg
isCrawled: true
---
Russisch-Ukrainisches Theaterprogramm. An einem mysteriösen Ort tief im Wald passiert Unglaubliches. Hauptfiguren aus einem Märchen treffen andere Figuren aus anderen Märchen. Hier stimmt etwas nicht! Nichts geht nach Plan… Warum treffen sich Baba Jaga und Harry Potter? Wie wirken Äpfel für die ewige Schönheit und kann der schwarze Kater Gedichte lesen? Das erfahren wir auf jeden Fall! Russisches Volksmärchen nach einer Fantasie von Puschkin.



Anmeldung über: **info[ät]vektor-schule.de**

**Dauer:** 90min