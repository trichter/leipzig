---
id: 311-1685984400
title: Kultur ist mehr als nur Unterhaltung -  Offene Gesprächsrunde über linke
  Kulturpolitik mit Kulturbürgermeisterin Dr. Skadi Jennicke
start: 2023-06-05 19:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/kultur_ist_mehr_als_nur_unterhaltung____offene_gespraechsrunde_ueber_linke_kulturpolitik_mit_kulturbuergermeisterin_dr__skadi_jennicke
image: image.png
isCrawled: true
---
Der Stadtbezirksverband *DIE LINKE. Leipzig-Ost* lädt Dich herzlich ein zu einer spannenden Fishbowl- Diskussion mit der Leipziger Kulturbürgermeisterin **Skadi Jennicke** zum Thema “Kulturpolitik von links”. Was sind die Herausforderungen und Chancen für eine linke Kulturpolitik in Leipzig? Wie können wir die kulturelle Vielfalt und Teilhabe fördern? Welche Rolle spielen die freien Kulturschaffenden und die Kultureinrichtungen in den Stadtteilen? Diese und weitere Fragen wollen wir mit Dir und der Kulturbürgermeisterin diskutieren.

**Dauer:** 120min