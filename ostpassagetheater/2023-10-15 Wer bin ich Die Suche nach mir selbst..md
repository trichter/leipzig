---
id: 345-1697382000
title: Wer bin ich? Die Suche nach mir selbst.
start: 2023-10-15 17:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/wer_bin_ich__die_suche_nach_mir_selbst_
image: Zirkomania.jpg
isCrawled: true
---
Bin ich ich selbst oder eine Version, die andere von mir verlangen? Mit diesem Thema sich hat das Jugendensemble von *Zirkomania* auseinandergesetzt und in zwei Wochen eine Show erarbeitet, die dazu anregt, über sich selbst nachzudenken. Wer sind sie? Wer wollen sie sein?

**Dauer:** 60min