---
id: 288-1681477200
title: Zirkus Eisenbahn
start: 2023-04-14 15:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/zirkus_eisenbahn
image: Foto_copyright_by_Zirkomania_e.V..jpg
isCrawled: true
---
Zirkus ist Pädagogik ist Kunst! *Zirkomania* ist ein Zirkusverein aus Leipzig mit über 10-jähriger Geschichte. Zusammen mit dem Talent-Campus der *Volkshochschule Leipzig* hat der Verein mit Kindern & Jugendlichen aus dem Stadtteil die verschiedenen Mittel des Zirkus ausprobiert. Nun präsentieren sie die Ergebnisse der Projektwoche auf der großen Bühne des *Ost-Passage Theaters*. Der pädagogische Prozess zur selbstbestimmten Show steht dabei im Vordergrund!



**Hier geht's zur Homepage von *Zirkomania e.V.*:** [https://zirkomania.de/](<https://zirkomania.de/>)

**Dauer:** 60min