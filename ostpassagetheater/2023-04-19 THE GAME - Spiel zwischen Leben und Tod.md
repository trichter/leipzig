---
id: 290-1681927200
title: THE GAME - Spiel zwischen Leben und Tod
start: 2023-04-19 20:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/the_game___spiel_zwischen_leben_und_tod
image: The_Game_copyright_by_Lautlos_e.V..jpg
isCrawled: true
---
An der bosnisch-kroatischen Grenze dreht sich alles um „THE GAME“, den illegalen Gang über die EU-Außengrenze. An diesem Spiel nehmen nicht nur junge Männer, sondern ganze Familien teil. Manuela und Bernd, zwei Flüchtlingshelfer aus Bayern, möchten helfen und erkennen schnell, dass bei diesem Spiel auch Schlepper, die Polizei, internationale Organisationen und viele weitere Spieler mitmischen. Nach und nach lernen die beiden die Spieler kennen und werden selbst Teil des „GAMES“. *(Federl, BA AT DE 2021)*



**Mehr Informationen zum Film:** [https://www.lautlos-verein.org/the-game.html](<https://www.lautlos-verein.org/the-game.html>)



**Hier geht's zum Trailer auf *YouTube*:** [https://youtu.be/4jhHgjx74uo](<https://youtu.be/4jhHgjx74uo>) 



Wir zeigen den Film in den verschiedenen Originalsprachen mit deutschen Untertiteln.



Im Anschluss berichten *Medical Volunteers International* über ihre Arbeit.



**Hier geht's zur Homepage von *Medical Volunteers International*:** [https://medical-volunteers.org/de/](<https://medical-volunteers.org/de/>)

**Dauer:** 120min