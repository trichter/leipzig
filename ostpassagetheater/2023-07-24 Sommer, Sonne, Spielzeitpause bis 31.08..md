---
id: 1-1690149660
title: Sommer, Sonne, Spielzeitpause bis 31.08.2023
start: 2023-07-24 00:01
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/sommer__sonne__spielzeitpause_bis_31_08_2023
image: OPT-Pressebild11kl-ccbyErik_Melzer.jpg
isCrawled: true
---
Viel Sonne und spannende Erlebnisse wünscht Dir Dein Ost-Passage Theater!

**Dauer:** 56160min