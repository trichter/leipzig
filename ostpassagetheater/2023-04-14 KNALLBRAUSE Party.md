---
id: 289-1681495200
title: KNALLBRAUSE Party
start: 2023-04-14 20:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/knallbrause_party
image: KNALLBRAUSE_Party_copyright_by_Miriam_Heckers.jpg
isCrawled: true
---
Zwei Monate vor ihrer ersten Premiere des Jahres lädt das *Freie Ensemble Jedermensch* zu einer bunten Party ein. Die Gäste erwartet ein Mix aus musikalischer und performativer Bespaßung, Getränke in schönen Farben und die Möglichkeit, die *Ost-Passage-Theater*\-Bühne als Tanzfläche für sich zu entdecken. Ein Besuch wird sich lohnen, zumal exklusiv das erste Festivalticket für das *KNALLBRAUSE Festival 2024* verlost wird.



**Der Programm-Ablauf in Kürze:** 



 20 Uhr: *Görda* (Alternativ Pop / Experimental Songwriter)

 21 Uhr: Drunk Lecture mit *après SHE\**

 22 Uhr: Ziehung Tombola

 bis 2 Uhr: DJs

 bis 3 Uhr: Ausklingen zu Trash



 Weitere Details zum Veranstaltungsverlauf gibt's über die Homepage des Ensembles: [https://www.ensemblejedermensch.de/knallbrause/knallbrause-party](<https://www.ensemblejedermensch.de/knallbrause/knallbrause-party>)

**Dauer:** 420min