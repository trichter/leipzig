---
id: 224-1697994000
title: "KLUB-EVENING: Infos, Cocktails & Karaoke"
start: 2023-10-22 19:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/klub_evening__infos__cocktails___karaoke
image: Pull-Over_copyright_by_OPT.jpg
isCrawled: true
---
Herzlich willkommen zum zweiten Klubabend der 6. Spielzeit. Du willst wissen, was das *Ost-Passage Theater* tut und wie es funktioniert? Dann komm zum Cocktailschlürfen und lass es Dir in gemütlicher Runde von Mitwirkenden erklären. Keine Spielchen, keine Erwartungen. Mit anschließender Karaoke. Everybody is welcome :)

**Dauer:** 180min