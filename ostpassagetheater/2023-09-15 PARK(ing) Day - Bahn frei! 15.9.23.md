---
id: 333-1694779200
title: PARK(ing) Day - Bahn frei! 15.9.23
start: 2023-09-15 14:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/park_ing__day___bahn_frei__15_9_23
image: Lastenrad.jpg
isCrawled: true
---
Am diesjährigen **PARK(ing) Day** FR 15.09. - dem weltweiten Aktionstag für weniger Autos in den Städten 2023 - hat sich ein Aktionsbündnis zusammengefunden, um die Eisenbahnstraße zwischen Hermann-Liebmann-Straße und Torgauer Platz von 14-22 Uhr zu sperren und die Parknischen gemeinsam mit den Nachbar:innen und Anrainenden in dieser Zeit kreativ zu bespielen. *(Die TRAM fährt trotzdem!)* 



Das *Ost-Passage Theater* beteiligt sich selbstredend an der Aktion. Als schaut an unserer Parknische in der Nähe Hermann-Liebmann-Straße vorbei und lasst uns ins Gespräch kommen.

**Dauer:** 480min