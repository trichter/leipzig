---
id: 324-1689357600
title: I WAS TOXIC, TOO
start: 2023-07-14 20:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/i_was_toxic__too
image: TOXIC_copyright_by_Anna_Maria_Koch.jpg
isCrawled: true
---
Als Ada erfährt, dass ihre Ex einen Kurzfilm über die Beziehung veröffentlicht, kann sie das Erlebte nicht länger verdrängen. Mit ihrer WG und dem Einsatz von Puppen und Objekten versucht sie, das Beziehungsdrama neu zu verhandeln, um endlich einen Schlussstrich zu ziehen. 



Im Stück werden verschiedene Perspektiven einer Ex-Beziehung retrospektiv erkundet. Mittels einer künstlerischen Fokussierung auf die Einflüsse patriarchaler Gefüge, westlicher Wertenormen und der popkulturellen Darstellung von „toxic relationships“ wird untersucht, wie eine gemeinsame Verantwortung über das Geschehene sensibel verhandelt werden kann. 



 Die unterschiedlichen Standpunkte werden durch verschiedene Darstellungsformen erfahrbar gemacht. Insbesondere die Arbeit mit Puppen ist eine Premiere für das [*Freie Ensemble Jedermensch*](<https://www.ensemblejedermensch.de/>).



 Es spielen: **Jenny Pohle, Hannah Adam, Frieda Mennert, Lara Clabaugh, Maja Nolte & Manuela Ni**



 Inszenierung: **Anna Maria Koch**

 Dramaturgie: **Alexandra Emig**

 Regieassistenz: **Juliane Vogler**



 Puppenbau: **Henriette Kachel**

 Bühnenbild: **Hannah Klitzke**

 Kostüm: **Sandra Cienkowski**

 Ausstattungsassistenz: **Lux**



 Am FR 14.07. im Anschluss: **Premierenfeier!**

**Dauer:** 80min