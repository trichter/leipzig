---
id: 275-1679680800
title: 2. Offene Bühne Harfe
start: 2023-03-24 19:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/2__offene_buehne_harfe
image: Harfe_copyright_by_Babett_Niclas.jpg
isCrawled: true
---
Trau Dich! Die Offene Bühne Harfe bietet die Möglichkeit sich gegenseitig Stücke oder Songs vorzuspielen, neue Dinge auszuprobieren oder das Auftreten zu üben. Offen für alle Niveaus, jedes Alter und alle Genres (Klassik, Folk, Radiosongs, etc.....) sowie Improvisation. 



Es können vor Ort genutzt werden:



\- eine Doppelpedalharfe (Style 30, L&H)

\- eine Hakenharfe, 34 Saiten (Weißgerber Artemis) mit eingebautem Tonabnehmer

\- ein Amp mit Klinke- und XLR Anschluss (Cube) und Mikrophon für Stimme. 



Außerdem wollen wir die Premiere des *LOHB Orchesters* feiern! Mit allen Harfenspieler\*innen, die Lust haben, werden wir in einem Harfenorchester vorher ab 16 Uhr zwei gemeinsame Stücke erproben und dann auf der Offenen Bühne direkt spielen! Einzige Voraussetzung dafür ist, dass Du ein Instrument mitbringst und im Vorfeld die Noten grob vorbereitest. Jedes Alter und alle Spielniveaus sind herzlich willkommen, es gibt vier unterschiedliche Stimmen zur Auswahl. Eine Anmeldung im Vorfeld an **mail[ät]babettniclas.de** ist für das *LOHB Orchester* notwendig und für die Offene Bühne mit Name+Stück super, aber keine Voraussetzung um mitmachen zu können.

**Dauer:** 120min