---
id: 316-1687716000
title: Las Migrañas
start: 2023-06-25 20:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/las_migra%C3%B1as
image: Las_Migranas-br_copyright_by_Las_Migranas.jpg
isCrawled: true
---
Alles andere als Kopfschmerzen bereitet Las Migrañas mit einer völlig eigenen Kombination von Postpunk und aller Wucht des Sprechgesangs! Direkt aus der Untergrundszene bringen uns „Die Migränen“ ihre ganz eigenen Tonkombinationen aus würzigem Reggaeton, wütendem Punk und Latinx-Kraut mit Synthpop-Elementen. Geordnete Verwirrung, die zur chaotischen Ekstase führt. Ibuprofen ist nicht willkommen! 



\+++ ++ +



*“Escuchar las migrañas es escuchar conjuros, destellos de luz, puntos ciegos y hormigueos en el espíritu. A través de melodías armoniosas y ritmos hipnóticos aparecen visiones sin tiempo, fósiles de ensueño que rompen el suelo lleno de piedras y ventanas. Escuchar las migrañas es escuchar el aura de canciones que ya estaban ahí pero nunca se te habían presentado. Dejarse llevar en un ritual colectivo sin principio ni fin.”* (Ángeles Rojas)



**Hörbeispiel:** [https://lasmigranias.bandcamp.com/track/petr-leo](<https://lasmigranias.bandcamp.com/track/petr-leo>)



***Facebook*\-Seite der Band:** [https://www.facebook.com/lasmigranias/](<https://www.facebook.com/lasmigranias/>)

**Dauer:** 90min