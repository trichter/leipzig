---
id: 269-1678039200
title: Zeit zum Atemholen – Frauenfußball kickt gegen Unterdrückung
start: 2023-03-05 19:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/zeit_zum_atemholen_%E2%80%93_frauenfussball_kickt_gegen_unterdrueckung
image: Zeit_zum_Atemnolen_copyright_by_Werkstattfilm_e.V..jpg
isCrawled: true
---
Der Kurzfilm **„Zeit zum Atemholen - Niloufar Ardalan und der FC Ardalan“** erzählt über den Kampf des iranischen Frauenfußballteams *FC Ardalan* um das Atemholen auf dem Fußballplatz als Ort des Abstands von der Unterdrückung durch Gesellschaft und Familie.



 Niloufar Ardalan, Ex-Kapitänin und Trainerin der iranischen Frauen-Nationalmannschaft, hat Frauen zusammengesucht und betreut einige Teams. Sie ist für viele Frauen ein Symbol für den Kampf, das Streben und das Bemühen Vieler. Die gesamte Ausrüstung, die Spielutensilien und Stadionmiete gehen immer auf ihre eigenen Kosten. Niloufar Ardalan und ihre leidenschaftlichen Fußballerinnen beschreiben ihren Kampf, ihre Erfolge und Niederlagen. Sie berichten von Kopftuchzwang bis Scheidung und Ausreiseverbot. Die Fußballfrauen in diesem Film erzählen über ihre Träume, ihr Streben und ihren Widerstand, weil Fußball für diese Frauen eine Zeit zum Atemholen ist. *(Zahedi, D IR 2022)*



 Die ungewöhnliche Filmproduktion ist eine Zusammenarbeit zwischen dem *FC Ardalan* in Teheran und dem *Gegengerade-Festival* in Oldenburg und gewann die Preise für „best documentary (short)“ und „best social justice“ auf dem internationalen *Filmmaker Life Festival*.



 Im Anschluss an den Film: Diskussion mit dem Filmproduktionsteam aus Oldenburg.

**Dauer:** 100min