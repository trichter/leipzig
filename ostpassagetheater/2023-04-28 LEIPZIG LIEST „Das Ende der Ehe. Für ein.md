---
id: 293-1682704800
title: "LEIPZIG LIEST: „Das Ende der Ehe. Für eine Revolution der Liebe“ mit
  Emilia Roig"
start: 2023-04-28 20:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/leipzig_liest__%E2%80%9Edas_ende_der_ehe__fuer_eine_revolution_der_liebe%E2%80%9C_mit_emilia_roig
image: Emilia_Roig_copyright_by_Susanne_Erler.jpg
isCrawled: true
---
Die Ehe ist in unserer Gesellschaft unantastbar. Trotz ihrer Institutionalisierung wird sie als Inbegriff der Liebe romantisiert und mythisch verklärt. Dabei verschärft eine Heirat für Frauen oft die Ungleichheit, und sie führt zu finanzieller Abhängigkeit. Die Bestsellerautorin Emilia Roig blickt hinter die Fassade eines patriarchalen Konstrukts und weist Wege zu einer Revolution der Liebe. Die Ehe normiert Beziehungen und Familie, kontrolliert Sexualität, den Besitz und die Arbeitskraft. Sie ist eine wichtige Stütze des Kapitalismus und lässt uns in binären Geschlechterrollen verharren. In ihrem mutigen und provokanten Buch ruft **Emilia Roig** daher das Ende der Ehe aus. Sie hinterfragt die Übermacht der Paare und untersucht, ob man Männer lieben und zugleich das Patriarchat stürzen kann. Letztlich wäre eine Abschaffung der Ehe nicht nur für Frauen befreiend, sondern für alle. Denn nur dann können wir Liebe in Freiheit und auf Augenhöhe miteinander neu denken und leben.



Emilia Roig ist promovierte Politikwissenschaftlerin und Gründerin des *Center for Intersectional Justice (CIJ)* in Berlin. Sie unterrichtet an verschiedenen Universitäten und hält Keynotes und Vorträge zu Intersektionalität, Feminismus, Rassismus, Diskriminierung und hat den Bestseller "Why we matter. Das Ende der Unterdrückung" geschrieben. Emilia ist Ashoka Fellow und wurde 2022 zur »Most Influential Woman of the Year« des* Impact of Diversity Award* gewählt.



**Mehr Informationen zur Autorin:** [https://de.wikipedia.org/wiki/Emilia\_Roig](<https://de.wikipedia.org/wiki/Emilia_Roig>)



Das Gespräch mit Emilia Roig moderiert Fatima Khan.



**Fatima Khan**, 1987 in Bhola/Bangladesch geboren und in Köln aufgewachsen, ist Autorin, Künstlerin, Kuratorin und Moderatorin. Sie studierte Antike Sprachen und Kulturen - Klassische Literaturwissenschaft und Germanistik an der Universität zu Köln. 2018 war sie Initiatorin und Mitgründerin der* q[lit]\*clgn*, des ersten feministischen Literaturfestival Deutschlands. Seit 2022 studiert sie Literarisches Schreiben im postgradualen Diplomstudiengang der *Kunsthochschule für Medien Köln*.

**Dauer:** 90min