---
id: 355-1697821200
title: "Die LINKE: Wie weiter?"
start: 2023-10-20 19:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/die_linke__wie_weiter_
image: Gemeinsam_jetzt.jpg
isCrawled: true
---
**Gemeinsam, jetzt! Für eine starke LINKE.** Allen ist bewusst, dass die Lage der Partei *Die LINKE* aktuell keine gute ist. Zwischen all den verschiedenen Meinungen fehlt es oft an einer klaren Linie, viele Menschen wissen nicht mehr, wofür sie steht. Wozu braucht es die Partei und was kann sie für die Bewegung erreichen?



 Diskussionsveranstaltung mit dem Co-Bundesvorsitzenden **Martin Schirdewan**, der Co-Landesvorsitzenden **Susanne Schaper** und der Europawahlkandidatin **Ines Schwerdtner**. Die Moderation des Abends übernimmt der Vorsitzende des Stadtverbandes *Die LINKE* **Adam Bednarsky**.

**Dauer:** 90min