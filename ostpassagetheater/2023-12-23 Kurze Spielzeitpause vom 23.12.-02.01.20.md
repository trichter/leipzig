---
id: 62-1703286060
title: Kurze Spielzeitpause vom 23.12.-02.01.2024
start: 2023-12-23 00:01
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/kurze_spielzeitpause_vom_23_12__02_01_2024
image: photo_2023-11-24_11-38-07.jpg
isCrawled: true
---
Das Team des *Ost-Passage Theaters* wünscht allen besinnliche und erholsame Feiertage! Wir werden die Zeit nutzen, um hier und da ein paar Verbesserungen und Reparaturen am Haus vorzunehmen und 2024 vorzubereiten. In alter Frische sehen wir uns dann im Neuen Jahr wieder. Rutsch gut rein!!!

**Dauer:** 15840min