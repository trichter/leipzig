---
id: 375-1703246400
title: Russische Zivilgesellschaft in Deutschland – Unterstützungsnetzwerk
start: 2023-12-22 13:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/russische_zivilgesellschaft_in_deutschland_%E2%80%93_unterstuetzungsnetzwerk
image: IMG_1884_ccLika_Petrychenko.JPEG
isCrawled: true
---
Vertretende des *Solidarus e.V.* und die Theaterregisseurin **Anya Demidova** sprechen über Möglichkeiten der Finanzierung kreativer Projekte, wie man sich in Deutschland leichter anpassen kann und über Möglichkeiten der aktiven zivilgesellschaftlichen Beteiligung in Deutschland. // На мероприятии представители Solidarus e.V. и режиссерка Аня Демидова расскажут: о возможностях финансирования творческих проектов, о том, как легче адаптироваться в Германии, о возможностях активного гражданского участия в Германии 



 Приглашаем вас на встречу в рамках проекта "Российское гражданское общество - сеть поддержки", организованную правозащитной организацией Solidarus e.V. Мероприятие нацелено в особенности на людей, которые приехали в Германию в течение последних двух лет. В первой части встречи представители организации поделятся ключевыми аспектами адаптации в Германии, помогая разобраться с бюрократическими вопросами и предоставляя информацию о возможностях гражданского участия.



 Затем, специально для тех, кто интересуется творческой сферой, режиссерка Аня Демидова проведет лекцию о финансировании творческих проектов в Германии. 



 В ходе лекции вы узнаете:

 ✔️О фондах и организациях, готовых поддерживать театральные проекты;

 ✔️ Как писать заявки и создавать финансовые планы, которые будут работать;

 ✔️ Где искать информацию о текущих конкурсах и open call'ах;

 ✔️О личном опыте Ани, её секретах и ноу-хау в области театрального фандрайзинга.



 Лекция поможет расширить возможности в реализации творческих проектов и узнать из первых уст о том, каким образом можно привлечь финансирование в Германии, адаптируя свои идеи к требованиям местного рынка культуры и искусства.



 Мероприятие пройдет на русском языке.



**Постановка на учет:** [https://shorturl.at/bfqL1](<https://shorturl.at/bfqL1>)



\+++ ++ +



Wir laden Sie zu einem Treffen im Rahmen des Projekts "Russische Zivilgesellschaft in Deutschland - Ein Unterstützungsnetzwerk" ein, das von der Menschenrechtsorganisation *Solidarus e.V. *organisiert wird. Im ersten Teil des Treffens werden Vertreter der Organisation wichtige Aspekte der Anpassung in Deutschland vorstellen, bei bürokratischen Fragen helfen und über Möglichkeiten der Bürgerbeteiligung informieren. Anschließend wird die Theaterregisseurin **Anya Demidova **einen Vortrag über die Förderung kreativer Projekte in Deutschland halten, der sich speziell an Interessierte aus der Kreativbranche richtet. 



 Während des Vortrags erfahren Sie mehr über:

 ✔️ Stiftungen und Organisationen, die bereit sind, Theaterprojekte zu unterstützen;

 ✔️ Wie man Anträge schreibt und Finanzierungspläne erstellt, die funktionieren;

 ✔️ Wo Sie Informationen über aktuelle Förderwettbewerbe und Ausschreibungen finden;

 ✔️ Anyas persönliche Erfahrungen, Geheimnisse und Know-how im Theaterfundraising.



 Der Vortrag wird dazu beitragen, die Möglichkeiten bei der Realisierung kreativer Projekte zu erweitern und aus erster Hand zu erfahren, wie man in Deutschland Fördermittel einwerben kann, indem man seine Ideen an die Anforderungen des deutschen Kunst- und Kulturmarktes anpasst.



 Die Veranstaltung findet auf Russisch statt.



**Anmeldung:** [https://shorturl.at/bfqL1](<https://shorturl.at/bfqL1>)

**Dauer:** 180min