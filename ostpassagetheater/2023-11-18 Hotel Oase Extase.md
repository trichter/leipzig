---
id: 364-1700334000
title: Hotel Oase Extase
start: 2023-11-18 20:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/hotel_oase_extase
image: photo_2023-09-29_19-57-05.jpg
isCrawled: true
---
**One night only!**



 5-Sterne Wellneszhotel der Extraklasse in traumhafter Leipziger Eisenbahnstraszen-Lage, 6.600 m², 21 Bade-, Schwitz- und Relaxattraktionen...



 Eine zwielichtige Hotelmanagerin sitzt an der Rezeption und stutzt sich den Bart, während sie gemeine Pläne schmiedet. Daneben der verfluchte Page, wartend auf seinen Moment der Rache. Spinnen fallen von der Decke, während die Gäste einchecken...



**\*\***



*Gönnen Sie sich doch einmal eine Auszeit vom Stress des Alltags und erleben Sie das Abenteuer eines Wellnessbesuchs im Hotel OASE EXTASE! Es reizt vor allen Dingen durch seine charismatischen Gäste und das allseits begehrte Hotelbarprogramm. Erleben Sie, wie sechs Performer:innen aus Drag, Physical Theatre, Luftakrobatik und Tanz eine Hotelanlage zum Einsturz bringen, oder doch das Patriarchat?



 Wer wird das Hotel am Ende lebend verlassen?*



 Atemberaubend *(aber wir haben eine Lüftungsanlage)*,

 einzigartig *(auf jeden Fall die Kostüme)*,

 erforschend *(dank unwiderstehlichem Charme)*,

 fesselnd *(mit code word)*,

 genial *(we know)*,

 unvergesslich *(bis zum nächsten Bier)*,

 glamourös *(was wir halt so gefunden haben)*,

 humorvoll *(HAHAHAHA)*,

 und absolut mitreißend *(bitte anschnallen!)*



**Oase Extase** ist ein inszeniertes Hotel, dass ein theatrales Varietéformat durch queerfuturistische Bodies neu belebt. Im Anschluss dürfen alle Gäste der Veranstaltung die gesammelte extatische Energie raustanzen zu dem Dj-Set von **Luki Loop.**



**über die company**

 Sechs einzelne Performer:innen schließen ihre Künste unter dem Namen OASE EXTASE zusammen, um an diesem einen Abend eine extatische Show abzuliefern. Ohne viel Proben und mit null Budget zaubern sie aus dem Nichts einen unvergesslichen Abend. Alle Performer:innen sind queer und wollen mehr Sichtbarkeit für ihre Anliegen und Kämpfe schaffen. *"Wir wollen Zugang zu Theaterhäusern, um unsere Kunst zu zeigen, aber wir können uns nicht im Dschungel der Kulturlandschaft behaupten, dafür fehlt uns leider der passende Uni-Abschluss! Also müssen wir auf andere Wege versuchen Teil dieser Landschaft zu werden und bauen erst einmal ein fettes Hotel darauf, kommt gerne alle vorbei und gönnt euch etwas Klasse."*

 Das ist die erste Veranstaltung unter dem Namen "Hotel Oase Extase", geplant sind weitere Veranstaltungen mit wechselnden Themen bezogenen Titeln. Die Hauptorganisatoren sind **Lokke Wurm** *(Physical Theatre/Clown)* und **Novir Gin** *(Drag Queen)*, ihre eingeladenen Kolleg:innen sind unter anderem **Maxe Knaxe** *(Luftakrobatik)*, **Tafida Galagel/TafiGala** *(Bellydance)* und weitere Überraschungsgäste...

**Dauer:** 180min
                                                                    
                                    (mit Pause)