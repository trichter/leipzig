---
id: 353-1698516000
title: "LITERARISCHER HERBST: Dirk von Lowtzow & Wolfram Lotz – Mutmaßungen über
  die Wirklichkeit"
start: 2023-10-28 20:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/literarischer_herbst__dirk_von_lowtzow___wolfram_lotz_%E2%80%93_mutmassungen_ueber_die_wirklichkeit
image: 211026_lh_21_ost_passage_13.1600x1200.jpg
isCrawled: true
---
**Dirk von Lowtzow & Wolfram Lotz** lesen aus ihren Büchern und reden über den Schwarzwald. Beide wuchsen im Schwarzwald auf, bringen Texte auf die Bühne und haben zuletzt ein Tagebuch veröffentlicht. *Tocotronic*\-Frontmann Dirk von Lowtzow und Theaterautor Wolfram Lotz reden über die Jugend in der Provinz und was daraus für die Kunst folgt, über die Tagebuchform, über das Verhältnis von Text und Musik und über das Glück, in der Kunst nicht alleine zu sein.



**Zum gesamten Programm des *Literarischen Herbstes 2023* geht's hier lang:** [https://literarischer-herbst.com/programm/](<https://literarischer-herbst.com/programm/>)

**Dauer:** 90min