---
id: 307-1685089800
title: Abschlussaufführung der Klasse 4b der Wilhelm Wander Grundschule
start: 2023-05-26 10:30
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/abschlussauffuehrung_der_klasse_4b_der_wilhelm_wander_grundschule
image: Wilhelm-Wanderer.jpg
isCrawled: true
---
Gemeinsam mit der Abschlussklasse 4b der *Wilhelm Wander Grundschule* und ihrem Klassenlehrer Sebastian Clemen haben zwei Theaterpädagog:innen am Haus innerhalb einer Projektwoche eine partizipative Theaterinszenierung erarbeitet. Die Ergebnisse präsentieren sie nun auf der Großen Bühne des *Ost-Passage Theaters*.

**Dauer:** 60min