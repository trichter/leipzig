---
id: 240-1679392800
title: '"Kleine Geschichten" von Leo N. Tolstoi'
start: 2023-03-21 11:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/__kleine_geschichten___von_leo_n__tolstoi
image: 10102018_Ciaccona_0062_Kopie.jpg
isCrawled: true
---
Lew Tolstoi, der sonst selten unter tausend Seiten blieb, hat auch ein paar zauberhafte Miniaturerzählungen für Kinder geschrieben. Mit elementaren Dingen wie Stroh, Wolle, Erde oder einem Brotteig erwecken die Tänzerin Anna Städler und die Schauspielerin Katja Rogner diese Tolstoischen Perlen zum Leben. Mit viel Musik und Humor spielen sie die verschiedenen Figuren - Kinder, eine Großmutter, sogar eine Hündin und einen Vogel - und tauchen ein in deren scheinbar kleine Geschichten.



 Wie es dem Ensemble einzufangen gelingt, von welchem Gefühl oder von welcher Situation aus dem Leben die Episoden eigentlich erzählen - dass es bei der Geschichte mit dem Brot um das Weitergeben und Weitertragen geht, oder wie bei der Hundeszene der Verlust kurz im Raum steht, oder das elementare Gefühl, keine Puppe, keinen Begleiter, keinen Haltepunkt im Leben zu haben -, das sind die eigentliche Treffer des Stücks, sowohl bei vier- oder sechsjährigen Kindern, als auch bei Erwachsenen.



Es spielen: **Anna Städler** *(Tanz)* & **Katja Rogner** *(Schauspiel)*



 Regie: **Stefan Ebeling**

 Produktionsleitung: **Josepha Vogel**



Compagnie *ciacconna clox*: "Mit oft winzigen, dafür aber umso raffinierteren Mitteln schafft ciacconna clox Kindertheater auf hohem Niveau. Im Gegensatz zur alltäglichen Reizüberflutung und Effekthascherei geht es ciacconna clox um unaufdringlich starke und poetische Bilder, die gerade in der Schlichtheit enorme Wirkung entfalten. ciacconna clox mutet den Zuschauern einiges zu, ohne den Kontakt zu ihnen zu verlieren. Kinder werden ernst genommen, in ihrer Fantasie gefordert. Dadurch, dass das Theater sich nicht auf die Sprache beschränkt, sondern alle Sinne anspricht, gerät es zu einem Gesamtkunstwerk, das eine kreative Nähe zu den Kindern und Erwachsenen sucht, ohne sie zu bedrängen." (Aus der Begründung der Jury zum *Leipziger Bewegungskunstpreis 2009*)



Mehr Informationen zur Compagnie*ciacconna clox*: [http://www.ciacconnaclox.de/](<http://www.ciacconnaclox.de/>)

**Dauer:** 45min