---
id: 344-1697202000
title: Zirkus Eisenbahn die Zweite
start: 2023-10-13 15:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/zirkus_eisenbahn_die_zweite
image: Foto.jpg
isCrawled: true
---
Zirkus ist Pädagogik ist Kunst! *Zirkomania* ist ein Zirkusverein aus Leipzig mit über 10-jähriger Geschichte. Zusammen mit dem Talent-Campus der *Volkshochschule Leipzig* hat der Verein mit Kindern & Jugendlichen aus dem Stadtteil die verschiedenen Mittel des Zirkus ausprobiert. Nun präsentieren sie die Ergebnisse der Projektwoche auf der großen Bühne des *Ost-Passage Theaters*. Der pädagogische Prozess zur selbstbestimmten Show steht dabei im Vordergrund!



**Zur Homepage von *Zirkomania*:** [https://zirkomania.de/](<https://zirkomania.de/>)

**Dauer:** 60min