---
id: 362-1700074800
title: "MITTELDEUTSCHE KURZFILMNACHT: Best of Kurzsuechtig 2023"
start: 2023-11-15 20:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/mitteldeutsche_kurzfilmnacht__best_of_kurzsuechtig_2023
image: IMG_4207.jpg
isCrawled: true
---
Sechs Kurzfilme, sechs verschiedene Welten: Am Hallenser Stadtrand, fernab des urbanen Zentrums, ist Wolfgang mit seinem Boot am Fluss gestrandet, um sich hier ein neues Zuhause aufzubauen; Elia spürt in Briefen und Erzählungen den Sehnsüchten und Schicksalen seiner Großeltern zwischen DDR-Militärdienst und dem Alltag zu Hause nach; im Erzgebirge der Nachwendezeit hat Lotta vom Kramladen ihrer Mutter die Nase voll …



 Die *Mitteldeutschen Kurzfilmnächte* zeigen eine Auswahl der besten Kurzfilme aus Sachsen, Sachsen-Anhalt und Thüringen, die in diesem Jahr beim *KURZSUECHTIG Filmfestival* ausgezeichnet wurden.



*Die Kurzfilmnächte widmen sich exklusiv dem Schaffen mitteldeutscher Filmemacherinnen und Filmemacher. Die Filme setzen sich mit Herkunft, der unmittelbaren Lebensrealität junger Menschen, aber auch mit der ostdeutschen Vergangenheit auseinander. Sie richten den Blick auf das Alltägliche wie auf das Groteske gleichermaßen. Aber auch filmkünstlerisch entfalten die Filme ein faszinierendes Spiel der verschiedenen Macharten – von Animation über Dok und Fiktion bis zu Experimental, wir zeigen euch die ganze Bandbreite des hiesigen Filmschaffens.*



**Im Programm:**



**"Wind Whisperer"** – Der Windflüsterer erzählt der Legende nach vom zerbrechlichen Glanz des Lebens. Am Ende des Horizonts wird eine singende Zikade geboren. Ihr Leben ist kurz, aber ihr Zirpen währt ewig. Liebevoller Stop-Motion-Film über Leben und Vergänglichkeit. *(Fernanda Caicedo, 2022, 7 min)*.



**"Stadtrand"** – Der Güterzug schiebt sich dicht vorbei an Häusern und Gärten. Am Horizont ragt der Schornstein des Kraftwerks empor und Strommasten zerschneiden den Himmel. Der Alltag am Stadtrand ist geprägt von Industrie und Gewerbe. Doch für eine Handvoll Menschen bilden die Freiräume dazwischen ihre Heimat. Während Wolfgang mit seinem Boot am Fluss gestrandet ist und hier ein neues Zuhause gefunden hat, wohnen Wilma und Eberhard Schneider schon seit ihrer Kindheit am Rande der Stadt. Einige Häuser weiter meditiert ein Buddhist in einer umgebauten Werkshalle. Manchmal liegt der Geruch der Hundefutterfabrik in der Luft und das Geklapper der Pferdekutsche hallt durch die Straßen. „Stadtrand“ ist das dokumentarische Porträt eines Zwischenraums aus Stadt und Land, Industrie und Natur. *(Conrad Winkler, 2023, 22 min)*.



**"A Goat´s Spell"** – Ein Kind und sein Tag. Eine Eroberung vor dem Frühstück, eine ahnungslose Ziege vor dem Haus, ein paar verheißungsvolle Flugzeuge weit oben im Himmel. Und dann brechen Dinge und Ereignisse auseinander. Scheinbar hängt das mit der Ziege zusammen. Wenn überhaupt irgendetwas nicht nur scheinbar zusammenhängt. *(Gerhard Funk, 2022, 9 min)*.



**"Dieser lange Atem, Beates Laden betreffend"** – Erzgebirge in der Nachwendezeit. Beate startet neu durch und hat in der Garage ihres Bauernhofes einen Laden für Haushaltswaren eröffnet. Der läuft nur mäßig und Vertreter beißen sich dort die Zähne aus, um neue Waren loszuwerden. Auch weil Tochter Lotta, das ganze Geschäft ziemlich bescheuert findet. *(Olaf Held, 2022, 21 min)*.



**"Es ist kälter geworden"** – Elia liest aus Briefen seiner Großeltern. Frank muss in der DDR zum Militärdienst. Täglich schreibt er nach Hause. Das junge Paar sucht Wege mit der Sehnsucht und Einsamkeit umzugehen, aber die Zeit in der Kaserne bleibt verlorene Lebenszeit. Als die Großmutter stirbt suchen Frank und Elia neue Wege mit der Sehnsucht umzugehen. *(Elia Zeißig, 2022, 8 min)*.



**"Die Heimkehr der Spargelstecher"** – Das aus 12 Tableaux Vivants bestehende Filmprojekt „Die Heimkehr der Spargelstecher“ führt durch surreale, humorvolle Assoziationswelten, welche das kollektive Gedächtnis der Deutschen ins Groteske ziehen. Günter Schabowski zitiert am Grill die Maueröffnung und Champagnersozialinnen warten auf den nie kommenden Bus. *(Lara Scherpinski, 2021, 15 min)*.



**Mehr Informationen zum Kurzfilmfestival: ** [https://kurzsuechtig.de/](<https://kurzsuechtig.de/>)

**Dauer:** 90min