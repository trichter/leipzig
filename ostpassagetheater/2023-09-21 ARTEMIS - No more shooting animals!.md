---
id: 335-1695319200
title: ARTEMIS - No more shooting animals!
start: 2023-09-21 20:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/artemis___no_more_shooting_animals_
image: Artemis-Theaterplakat-RZ.jpg
isCrawled: true
---
Musiktheater um ein Feuer. Es brennt, die Welt steht in Flammen, eine Fliege summt eine beinah heilige Melodie. Eigentlich beschwert sie sich über das Wetter. Tragischerweise wird sie überhört. Eine Wohngemeinschaft. Vier dudige Dudes und die Göttin der Jagd. Geschichten von Hamstern, Hunden und Apokalypsen. Ein Science-Action-Träller über die Verwandtschaft der Arten und ihr alltägliches Zusammenleben.



**ACHTUNG:** *Die Veranstaltungen DO 21.09. und FR 22.09. finden outdoor im Club Ostend (Plautstraße 41) statt; nur bei Regen im Ost-Passage Theater, deshalb bitte am Tag der Aufführung nochmals informieren!*



<div class="gallery"><div class="gallery-title">Probenbilder "ARTEMIS - No more shooting animals!"</div><div class="slider" data-dir="Artemis" data-current="0"><div class="slider-left">&lt;</div><div class="slider-image"><img class="slider-img" src="https://ost-passage-theater.de/wp-content/mediathek/impressions/gallery/Artemis/4_.JPG"><img class="slider-img opt-hidden" data-img-src="https://ost-passage-theater.de/wp-content/mediathek/impressions/gallery/Artemis/6_.JPG"><img class="slider-img opt-hidden" data-img-src="https://ost-passage-theater.de/wp-content/mediathek/impressions/gallery/Artemis/9_.JPG"></div><div class="slider-right">&gt;</div></div><div class="credits"><div class="credits-image"></div><div class="credits-gallery">Bilder: _copyright by Leonie Sowa </div></div></div>

**Dauer:** 80min