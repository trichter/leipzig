---
id: 263-1684076400
title: "Löt-Session // Soldering Session #3"
start: 2023-05-14 17:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/loet_session____soldering_session__3
image: Lötsession_3.jpg
isCrawled: true
---
Cable broken but no soldering iron at hand? The *Ost-Passage Theater* helps you! In this guided soldering session we want to give you an understanding of the soldering equipment and give you an insight into the world of technology. Previous knowledge is not necessary. We will provide soldering iron, solder, heat shrink tubing etc. and help you with your project. You can bring your own technical equipment or cables or just try out a little. For a few Euros you can also buy cables and plugs from us and solder your own audio cable (XLR or jack). 



 The session will be led by **Till Wimmer**, freelance technician at our house.



\+++ ++ +



Kabelbruch, aber keinen Lötkolben zur Hand? Das *Ost-Passage Theater* hilft Dir! In dieser angeleiteten Lötsession möchten wir Dir das Lötequipment näher bringen und geben Dir einen Einblick in diese schweißtreibende (aber kinderleichte) Welt der Technik. Vorkenntnisse sind nicht erforderlich. Wir stellen Lötkolben, Lötzinn etc. zur Verfügung und helfen Dir bei Deinem Projekt. Du kannst Dein eigenes Equipment oder Kabel mitbringen oder Dich einfach ein wenig an dem ausprobieren, was wir so da haben. Für wenige Euro kannst Du bei uns auch Kabel & Stecker kaufen und Dir Dein eigenes Audiokabel (XLR oder Klinke) zusammenlöten. 



Geleitet wird die Session von **Till Wimmer**, freiberuflicher Techniker in unserem Hause!

**Dauer:** 180min