---
id: 185-1690120800
title: "Spielzeit-Abgrillen #5"
start: 2023-07-23 16:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/spielzeit_abgrillen__5
image: photo_2023-06-13_17-14-51.jpg
isCrawled: true
---
Ein großartiges Jahr 2022/23 liegt hinter uns! Zum Abschluss der 5. Spielzeit laden wir alle Freunde, Fans und Unterstützer\*innen zum gemeinsamen Spielzeit-Abgrillen. Für Speis (fleisch/vegan) und Trank (alc/non alc) wird gesorgt. Ideal, um den einen oder die andere aus dem Team näher kennen zu lernen und zusammen über visionäre Ideen und progressive Projekte zu spinnen. Everybody is welcome :)

**Dauer:** 240min