---
id: 272-1678543200
title: Lets Sing and Dance Together - Interkultureller Familiennachmittag
start: 2023-03-11 15:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/lets_sing_and_dance_together___interkultureller_familiennachmittag
image: Eröffnung_-_Kinderfest_11.03._-_Ost-Passage_Theater_-_2018_cc_by_Thomas_Grahl.jpg
isCrawled: true
---
*Konrads Eisenbahn e.V. *lädt gemeinsam mit dem *Vektor Schule e.V.* zu einem bunten interkulturellen Nachmittagsprogramm. Neben musikalischen und tänzerischen Darbietungen der verschiedenen Musik- und Tanzgruppen beider Vereine gibt es einen Basar mit Töpferware, Kaffee und Kuchen, Kinderschminken und die berühmte Bastelstraße. Kein Eintritt, bringt Kuchen oder eine Spende mit!

**Dauer:** 120min