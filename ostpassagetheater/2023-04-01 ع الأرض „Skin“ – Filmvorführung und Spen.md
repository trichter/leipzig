---
id: 281-1680372000
title: "ع الأرض: „Skin“ – Filmvorführung und Spendenveranstaltung für vom
  Erdbeben Betroffene in Syrien"
start: 2023-04-01 20:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/%D8%B9_%D8%A7%D9%84%D8%A3%D8%B1%D8%B6__%E2%80%9Eskin%E2%80%9C_%E2%80%93_filmvorfuehrung_und_spendenveranstaltung_fuer_vom_erdbeben_betroffene_in_syrien
image: Skin_copyright_by_Afraa_Batous.jpg
isCrawled: true
---
Vor dem Hintergrund der Syrienkrise seit 2011 überschneiden sich in dem Dokumentarfilm „Skin“ die Schicksale dreier Charaktere. Angeführt von lebensverändernden Ereignissen in Syrien begeben sich Soubhi, Hussein und Afraa (die Regisseurin) auf eine Reise, um sich selbst und die anderen zu entdecken. Ein zutiefst persönlicher Film, der die Erinnerungen der Regisseurin an ihre beiden engsten Freunde Hussein und Soubhi erforscht, die darum kämpfen, ihre Taten in politisch und sozial schwierigen Zeiten in Syrien zu vereinbaren. *(Batous, SY 2015)*



**Afraa Batous** ist 33 Jahre alt. Sie stammt aus Aleppo, lebte einige Zeit in Damaskus und dann in Beirut, bevor sie 2016 nach Deutschland zog. Sie schrieb sich an einer Filmhochschule in Deutschland ein und drehte 2015 ihren Dokumentarfilm.



 Wir zeigen den Film in der Originalsprache auf Arabisch mit englischen Untertiteln. 



 Der Abend wird organisiert von *3L-Ard / on the ground*, einem Selbsthilfenetzwerk in Leipzig lebender Syrer:innen zur Unterstützung der Betroffenen der Erdbebenkatastrophe in Nordsyrien.

**Dauer:** 120min