---
id: 371-1702209600
title: ENDO...METRI...WAS?
start: 2023-12-10 13:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/endo___metri___was_
image: photo_2023-11-22_08-46-08.jpg
isCrawled: true
---
Die Ausstellung soll die Erkrankung „Endometriose“ sichtbar machen und richtet sich sowohl an Betroffene, als auch an Unbeteiligte bzw. die, die nicht wissen, was Endometriose eigentlich ist. Neben Informationstexten auf Englisch, Arabisch und Deutsch liegt der Fokus auf dem bedrückenden Gefühl, welches diese chronische Krankheit auslösen kann, vermittelt durch Kunstwerke, die Raumgestaltung sowie Musik. Das wird schließlich kein Museumsbesuch ;)

**Dauer:** 420min