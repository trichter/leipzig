---
id: 306-1685124000
title: Aşk, Mark ve Ölüm // Liebe, D-Mark und Tod
start: 2023-05-26 20:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/a%C5%9Fk__mark_ve_%C3%96luem____liebe__d_mark_und_tod
image: LOVE,_DEUTSCHMARKS_AND_DEATH_Still_01_Ismet_Topçu_copyright_by_Filmfaust_GmbH.jpg
isCrawled: true
---
Dokumentarfilm über die größte musikalische Untergrund-Bewegung Deutschlands. Mit noch nie gesehenem Archivmaterial erzählt Cem Kaya die 60 Jahre lange Musikgeschichte der türkischen Einwander:innen, ihrer Kinder und Enkelkinder in Deutschland. Anfang der 1960er-Jahre wurden die sogenannten Gastarbeiter:innen aus Anatolien und anderen Gegenden der Türkei von der Bundesrepublik Deutschland angeworben. Von Anfang an gab es etwas, dass sie immer begleitet hat und Bestandteil ihrer Kultur war: ihre Musik, ein Stück Heimat in der Fremde. Über die Jahre entwickelten sich in Deutschland eigenständige musikalische Richtungen, die es in dieser Form im Mutterland nicht gab. "Liebe, D-Mark und Tod" erzählt diese beispiellose Geschichte einer selbständigen Musikkultur. *(Kaya, D 2022 – OmdU)*



**Mehr Informationen:** [https://de.wikipedia.org/wiki/A%C5%9Fk,\_Mark\_ve\_%C3%96l%C3%BCm\_%E2%80%93\_Liebe,\_D-Mark\_und\_Tod](<https://de.wikipedia.org/wiki/A%C5%9Fk,_Mark_ve_%C3%96l%C3%BCm_%E2%80%93_Liebe,_D-Mark_und_Tod>)



**Trailer:** [https://www.askmarkveolum.de/](<https://www.askmarkveolum.de/>)



 Wir zeigen den Film in den Originalsprachen auf Türkisch & Deutsch mit deutschen Untertiteln.



 Im Anschluss gibt es türkische Musik & Snacks aus der Dose. Außerdem werden Spenden gesammelt für *Uluslararası Tüm Engelliler Yaşlılar Kimsesizler Federasyonu (UTEF) (Internationale Föderation aller Behinderten, älteren Menschen und Waisen)* in der Türkei.

**Dauer:** 180min
                                                                    
                                    (mit Pause)