---
id: 331-1694851200
title: On Living II
start: 2023-09-16 10:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/on_living_ii
image: 230821-OnLiving.jpg
isCrawled: true
---
Musikalisch-künstlerische Auseinandersetzung mit der eigenen Identität und der Beziehung zu den Großeltern. Diese Menschen sind uns nah und ähnlich und doch können und konnten sie uns, aufgrund von Scham oder Trauma, manche Dinge nie erzählen. 



Die Workshop-Teilnehmer\*innen, sowie die Musiker\*innen und eine Video- & Fotokünstlerin werden diesen verschütteten persönlichen Geschichten Ausdruck verleihen. Auf den Workshops entwickeln die Teilnehmer\*innen kleine Klanginstallationen, die - verbunden mit einer Reflexion über persönliche Gegenstände – als Konzert und interaktive Ausstellung aufgeführt werden. Das Projekt will das Publikum dazu einladen, mit musikalischer Neugier über eigene familiäre Geschichten zu reflektieren.



Um Anmeldung wird gebeten: **mail@neasa.de**

**Dauer:** 300min