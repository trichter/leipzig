---
id: 347-1697907600
title: "Analyse:Paralyse: Panel und Drestlingshow"
start: 2023-10-21 19:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://ost-passage-theater.de/veranstaltungen/analyse_paralyse__panel_und_drestlingshow
image: photo_2023-09-29_14-05-08.jpg
isCrawled: true
---
**19:00 Uhr PANEL**



 Leipziger Akteure der Kunst und Kulturszene setzen sich mit den Themenschwerpunkten Gemeinschaft und Kollektivarbeit auseinander.



**20:00 Uhr DRESTLINGSHOW**



 "Lasst es euch einführen, in die Welt der PIPIKAKA DRESTLINGSHOW, wo die Fäuste fliegen und die Bäuche wippen. Mit wildem Kampf, schwingenden Hüften, viel Drama - Eine große und glückliche Familie: Daddys und ihre Arschgeburten von Kindern, die Bratinasbrothers, das Onehitwonder und natürlich Deathproof the Damager. Nichts für prüde Gemüter! EURE PIPIDRAGOSTINOS"



*Der Analyse:ParalyseVerein veranstaltet auch dieses Jahr – in kleinerem Rahmen, aber in gewohnt qualitativer Zusammensetzung – ihr Festival für politische Kunst im breitgefächerten kulturellen und kreativen Sektor. Simultan zu verschiedenen Workshops, Panels und interaktiven sowie partizipativen Angeboten mit dem Theaterkollektiv *gruppe tag*, der Designerin **Luise Hesse**, der* Krudebude* und vielen anderen Kulturschaffenden, die sich am Wochenende in der *Garage Ost* zusammenfinden, schlägt dafür am Samstagabend im *Ost-Passage Theater* darstellende Kunst in Reinform durch ein Performance - Wrestlingkollektiv auf die Latten. Es wird heiß, es wird politisch, den Rest macht ihr! Der genaue Programmablauf von SA 12:00 Uhr bis SO 22:00 Uhr steht noch nicht fest. Watch out!* 



**Zur Homepage von *Analyse:Paralyse*:** [https://analyse-paralyse.net/festival/](<https://analyse-paralyse.net/festival/>)

**Dauer:** 90min