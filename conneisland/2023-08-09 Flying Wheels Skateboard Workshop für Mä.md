---
id: "6577"
title: Flying Wheels Skateboard Workshop für Mädchen (und Jungen)
start: 2023-08-09 10:00
end: 2023-08-09 14:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6577.html
isCrawled: true
---
Flying Wheels Skateboard Workshop für Mädchen (und Jungen)

!AUSGEBUCHT!
Skateboard Workshops für Kinder und Jugendliche im Alter von 6 - 12 Jahre.

Ihr lernt von erfahrenen Skateboarder:innen an jeweils drei aufeinanderfolgenden Tagen in drei verschiedenen Skateparks in Leipzig die Grundlagen des Skatens.

Zeitraum:
10. - 12. Juli 2023 
24. - 26. Juli 2023
07. - 09. August 2023
jeweils 10:00 - 12:00 Uhr

Anmeldung unter: anmeldungworkshop@conne-island.de 

Der Workshop ist kostenfrei. Helme, Schützer und Skateboards werden gestellt.

Das Projekt wird gefördert durch das Amt für Jugend und Familie und durch den Urban Souls e.V. unterstützt. Die Workshops finden im Rahmen des Ferienpasses statt. 


http://www.conne-island.de/termin/nr6577.html