---
id: "6517"
title: Früher waren wir mal politisch (organisiert)
start: 2023-04-25 18:30
end: 2023-04-25 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6517.html
isCrawled: true
---
Ü30 Spezial 

Vor ein paar Monaten habt ihr vielleicht eine Demonstration in Leipzig gesehen mit roten Fahnen, einige auf der Demonstration trugen Tücher mit Hammer und Sichel und ihr habt euch gefragt wieso? Ihr habt auf Instagram etwas von den Terfs und Swerfs in Leipzig gelesen und fragt euch was damit gemeint ist? Ihr wart früher vielleicht mal in politischen Kontexten organisiert, seid aber wegen Beruf, Familie oder aus anderen Gründen schon lange raus? Ihr spielt aber mit dem Gedanken vielleicht doch wieder politisch aktiver zu werden, wisst aber nicht wie und wo?

Geplant ist eine Veranstaltung für die Generation Ü30, die mal politisch aktiv gewesen ist und gerne wieder mehr machen möchte. Die Veranstaltung richtet sich nicht an Menschen, die bereits organisiert sind, sondern an jene die es gerne wieder wären. Ebenso ist es nicht gedacht, mögliche Konflikte oder Kritiken an politischen Entwicklungen in der Stadt in diesem Rahmen zu diskutieren. Die Veranstaltung soll ca. 2 Stunden dauern und mit euch zusammen den Fragen nachgehen was mögliche Probleme sind und wie wir sie lösen können.

Solltet ihr wegen mangelnder Kinderbetreuung nicht an der Veranstaltung teilnehmen können, meldet euch gerne per Mail (frueher_politisch@riseup.net) vorher bei uns, wir finden sicherlich einen zweiten Termin, nehmen dies in der Veranstaltung mit auf und werden auch darüber sprechen.

Die Veranstaltung findet Im Café des Conne Island statt. 

http://www.conne-island.de/termin/nr6517.html