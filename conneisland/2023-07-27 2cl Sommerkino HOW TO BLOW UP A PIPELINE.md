---
id: "6547"
title: "2cl Sommerkino: HOW TO BLOW UP A PIPELINE"
start: 2023-07-27 21:00
end: 2023-07-28 04:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6547.html
isCrawled: true
---
HOW TO BLOW UP A PIPELINE

IN KOOPERATION MIT DER CINÉMATHÈQUE
USA 2022, Daniel Goldhaber, 100 Min., OmU
mit Ariela Barer, Kristine Froseth, Lukas Gage, Forrest Goodluck, Sasha Lane, Jayme Lawson

Im Kampf gegen Umweltzerstörung und Profitmaximierung hat eine heterogene Gruppe aus jungen Aktivist*innen genug von symbolischen Aktionen und möchte Konkretes. Präzise planen sie die Vorbereitung und Durchführung eines in vieler Hinsicht hochgefährlichen Sabotageaktes. Trotz unterschiedlicher Motivationen und mancher Zweifel und Ängste, arbeiten alle gemeinsam auf das gleiche Ziel hin, und das ist eine gigantische Ölpipeline in der texanischen Wüste.

Informationen zu diesem Film kamen mit folgender Nachricht an: "Nur um Missverständnisse zu vermeiden: Der Titel ist zwar Programm, aber der Film ist weder ein Aufruf noch eine Anleitung zum Bombenbau." Das mag stimmen. Aber wenn ein Film einen derartigen Hinweis nötig hat, hat das sicher einen Grund. Welch subversive Kraft in HOW TO BLOW UP A PIPELINE steckt, muss jede*r selbst entscheiden. Hochrelevant und unfassbar spannend ist er in jedem Fall.



http://www.conne-island.de/termin/nr6547.html