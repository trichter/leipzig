---
id: "6635"
title: Café Show / MARCH + CF98 + Melonball
start: 2023-11-19 18:00
end: 2023-11-19 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6635.html
isCrawled: true
---
Café Show / MARCH + CF98 + Melonball

MARCH ( Record Release Tour )
CF98
Melonball

http://www.conne-island.de/termin/nr6635.html