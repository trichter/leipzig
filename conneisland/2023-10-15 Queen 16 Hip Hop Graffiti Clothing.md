---
id: "6642"
title: Queen 16 Hip Hop Graffiti Clothing
start: 2023-10-15 13:30
end: 2023-10-15 17:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6642.html
isCrawled: true
---
Queen 16 

13:30 Entry/Registration 
14:30 Cypherselection and Cyphers
15:00 Battletime and Cyphers
20:30 Winner Ceremony

Culture Exchange International BGirl Battle Queen16
Crew Battle (Only Invite)
Cypher Champs (Open for all BBoys and BGirls) 
Graffiti
HipHop 
Clothing

Schaut euch das gesamte Programm an unter: https://www.frauenkultur-leipzig.de/angebote/aktuelle-projekte/queen16/

http://www.conne-island.de/termin/nr6642.html