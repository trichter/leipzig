---
id: "6497"
title: "BeatNightOut #1"
start: 2023-03-24 20:00
end: 2023-03-25 00:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6497.html
isCrawled: true
---
BeatNightOut #1 

BeatNightOut 1
- beatsets, showcases, open jamsession, DJs, - 

Beat showcases by:

Sir Mantis
Yunghesslich
kulmbach
Brous One

Djs: Juicy & Free-kee

In Anlehnung an den einstigen Beatmaker:innen Stammtisch "Overdubclub", wollen wir der hiesigen Beat/Producer und DJ Szene in Leipzig und Umgebung eine Plattform geben.
Der Abend soll ein Ort zum Austausch, für Neuentdeckungen, zum Ausprobieren und Improvisieren sein. Du möchtest gern mal am Showcase teilnehmen und in 15-30 Minuten deine Produktionen zeigen? - melde dich an unter: ben@conne-island.de
Du möchtest an der open jamsession teilnehmen?
- bring dein Equipment mit und klemm dich an!
- moderiert wird der Abend vom ehemaligen Host der Word! Cypher Bazel aka irgendein Frank -


http://www.conne-island.de/termin/nr6497.html