---
id: "6482"
title: "Skill Sharing - Feminist*in-Sein  Selbsterfahrungspraxis Reloaded!"
start: 2023-05-06 14:30
end: 2023-05-06 18:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6482.html
isCrawled: true
---
Skill Sharing - Einführungsworkshops in emanzipative Theorie und Praxis

Feminism - Jogginghosen, GRL PWR  Socken, Leoprint, Glitzer und Haare nicht nur auf dem Kopf  rein von außen betrachtet scheint es viele Feminist*innen in Leipzig zu geben. Doch was macht eigentlich eine gute Feminist*in aus?
Zuerst werden wir uns in einem kurzen Vortrag die Geschichte der feministischen Bewegung inklusive ihrer Selbsterfahrungspraxis (sogenannte Consciousness raising groups) anschauen. Wir schlagen anschließend den Bogen von der damaligen Praxis der Selbsterfahrungsgruppen hin zu den aktuellen feministischen Bewegungen und der Frage, wie die einzelnen Buchstaben in Flinta* zusammengekommen sind und was sie bedeuten. Im Anschluss daran probieren wir die Praxis von damals einfach mal aus und schauen, ob wir damit noch irgendwas etwas anfangen können. Unsere Ergebnisse werden wir auf Gemeinsamkeiten hin untersuchen und gemeinsam diskutieren, ob wir daraus eine aktuelle feministische Theorie basteln können.
Der Vortrag wird ungefähr 30 min dauern, dann haben wir 30 min Zeit für Fragen und Diskussion. Nach einer kurzen Pause fangen wir mit den Selbsterfahrungsgruppen an. Dafür haben wir nochmal zwei Stunden Zeit. Bringt bitte etwas zu schreiben mit.

Die Reihe findet in Kooperation mit der Rosa Luxemburg Stiftung Sachsen statt.

http://www.conne-island.de/termin/nr6482.html