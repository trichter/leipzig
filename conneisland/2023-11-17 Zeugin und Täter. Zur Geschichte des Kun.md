---
id: "6682"
title: "Zeugin und Täter. Zur Geschichte des Kunsthauses Tacheles in Berlin.
  Buchvorstellung mit Su Tiqqun"
start: 2023-11-17 19:00
end: 2023-11-17 23:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6682.html
isCrawled: true
---
Zeugin und Täter. Zur Geschichte des Kunsthauses Tacheles in Berlin. Buchvorstellung mit Su Tiqqun

Als Kunsthaus schrieb das Tacheles eine bemerkenswerte Geschichte. Es war Echokammer der 80er-Jahre Kunst, Programmkino, Spielstätte, Techno-Labor, Möglichkeitsraum und begehrtes Schmuddelkind des Berlin-Tourismus. Es überwältigte seine Macher und Besucher gleichermaßen, hat sich als Gesamtkunstwerk in eine Legende gehüllt, die in Kürze noch einmal aufgewärmt werden wird, wenn das Areal am Tacheles als nagelneues Luxusquartier an der Friedrichstraße die Schlagzeilen füttert.

Su Tiqqun wuchs in einer prosperierenden Industriestadt in Thüringen auf. Der Vater, Bergmann untertage, die Mutter Textilgutachterin übertage. Su's ganzheitliche Erziehung wurde auf die Verbesserung und Rettung der Welt geeicht. Sie studierte daher Deutsch und Geschichte auf Lehramt in Leipzig, wurde mit 24 Jahren allseits observierter Rebell, arbeitete als Lehrerin auf dem Land und in der Stadt, erst staatlich, dann kirchlich bis 1990, sie hat die ersten sieben Jahre des Tacheles studiert und ist danach von der öffentlichen Bühne verschwunden.

Die Veranstaltung findet im Saal des Conne Island statt.

http://www.conne-island.de/termin/nr6682.html