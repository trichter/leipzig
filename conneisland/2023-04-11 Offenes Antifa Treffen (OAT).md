---
id: "6516"
title: Offenes Antifa Treffen (OAT)
start: 2023-04-11 18:30
end: 2023-04-11 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6516.html
isCrawled: true
---
OAT im April: First of May  Judgement Day

First of May  Judgement Day So war mal der Titel zu einem 1. Mai in Leipzig (hier: https://www.nadir.org/nadir/initiativ/bgr/pages/index2.htm). Wir wollen in die Vergangenheit schauen was es so am 1. Mai in Leipzig gab, wie sich dieser so entwickelt hat aus einer linken Perspektive und was für dieses Jahr schon bekannt ist. Für dieses OAT gehen wir gleich zu Beginn ins Cafe im Conne Island. Wenn ihr möchtet, können wir zur Thematik auch einen Film schauen oder er läuft einfach nebenbei, während sich bei einem Getränk ausgetauscht werden kann.

Was ist das Offene Antifa Treffen?

Es ist wichtig, sich zu vernetzen und Strukturen aufzubauen, um den Widerstand zu organisieren  nicht nur gegen Rassismus und Faschismus, sondern auch gegen Nationalismus, Sexismus, Antisemitismus, Homosexuellenfeindlichkeit und die kapitalistische Gesamtscheiße.

Du willst endlich was dagegen tun, wusstest bisher aber nicht wo, wie und mit wem? Dann komm zum Offenen Antifa Treffen (OAT).

http://www.conne-island.de/termin/nr6516.html