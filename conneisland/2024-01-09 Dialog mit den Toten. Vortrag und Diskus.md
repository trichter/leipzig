---
id: "6685"
title: Dialog mit den Toten. Vortrag und Diskussion mit Frank Raddatz zur
  Dramatik von Heiner Müller
start: 2024-01-09 19:00
end: 2024-01-09 23:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6685.html
isCrawled: true
---
Dialog mit den Toten. Vortrag und Diskussion mit Frank Raddatz zur Dramatik von Heiner Müller

Heiner Müller hat wie kein anderer Dramatiker die Auswirkungen von 1989 in der Öffentlichkeit diskutiert, weil er seine Tätigkeit des Stücke Schreibens verstellt sah. Die Bedingungen für ein neues Theater, eine Neue Kunst müssten freigesprengt werden, aus der beklemmenden Enge der postsozialistischen Welt. Dafür solle man die Toten literarisch ausgraben, damit das Unabgegoltene abgegolten werden kann und die gemarterten Geschlechter zu ihrem Recht kommen. Was aber das heißt und welchem Verhältnis die Kunst damit zur Gesellschaft gestellt wird, darüber wollen wir mit Frank Raddatz diskutieren, der selbst mit Müller noch diskutieren konnte  den wir also auszugraben versuchen werden.

Frank Raddatz ist Dramatiker und hat zu Heiner Müller promoviert, in den 80er und 90er Jahren hat er mehrere öffentliche Veranstaltungen mit Müller über die postsowjetische Zeit und den Verlust der Utopie durchgeführt.

Die Veranstaltung findet in Kooperation mit dem Literaturhaus Leipzig im Haus des Buches statt.

http://www.conne-island.de/termin/nr6685.html