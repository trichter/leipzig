---
id: "6622"
title: Die Hand ist ein einsamer Jäger
start: 2023-09-27 19:00
end: 2023-09-27 23:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6622.html
isCrawled: true
---
Die Hand ist ein einsamer Jäger

Frauenkörper, sie dienen als Anschauungsmaterialien, Ausstellungsobjekte, Projektionsflächen und Kampfplätze. Oft werden sie in zu enge Räume und Kleider gezwängt. Prinzessin Selda, pixelgewordene Fantasieoberfläche, rekelt sich willenlos. Das Hungermädchen wartet auf sein Verschwinden. Eine Rebellin probiert die Masken der Weiblichkeit, während sich eine andere fragt, wem eigentlich die Hand gehört, die sich mit ihrem Bein befasst.

Eine Performerin verwandelt den fast leeren Bühnenraum in ein Experimentierfeld der Körper. Dabei schlüpft sie in unterschiedliche Rollen und Situationen, parodiert das was wir  dem weiblichen oder männlichen Geschlecht als naturgegeben zuschreiben und in welche Zwänge wir uns da durch begeben. Sei es die richtige Epilation im Schambereich, das Schlankheitsgebot oder immer und jederzeit trinkfest und potent zu sein.

Eine Übung in der Dekonstruktion der Geschlechter, vermischt mit trashigem Gender-Hacking und poppigen Einflüssen.

Schauspiel: Johanna Franke
Regie: Kim Ehinger

Triggerwarnung: Die Aufführung behandelt Formen sexualisierter Gewalt

Aufführungsrechte liegen beim S. Fischer Verlag
____________________

VITA:

Johanna Franke wurde 1990 in Leipzig geboren. Dort und in Übersee probierte sie diverseste Jobs (DHL), Theaterkeller (Cammerspiele Leipzig) und Studiengänge aus, bis sie 2012 an der Hochschule für Musik und Darstellende Kunst in Frankfurt am Main landet, wo sie den Preis als jahrgangsbeste Schauspielerin des dritten Ausbildungsjahres erhält.
Neben dem Studium spielte sie Desiree in einer Adaption von Bruckners KRANKHEIT DER JUGEND, den Luka in Maxim Gorkis Nachtasyl am Schauspiel Frankfurt sowie Martel in Rose Bernd am Schauspielhaus Bochum. Andere spannende Erfahrungen in der Studienzeit sammelte sie beispielsweise beim Performancedreh zur Ausstellung von Mike Bouchet und Paul McCarthy in der Kunsthalle Portikus und ihrer One-Woman-Performance Hyperion an den Landungsbrücken Frankfurt und als Sängerin einer im Keim ersticken Postpunkband. 2016 beginnt sie als festes Ensemblemitglied am Theater Osnabrück. Hier spielte sie u. a. in der Uraufführung Die unbekannte Stadt von Anis Hamdoun, die Feuerschutzbeauftragte in Jan Philipp Stanges Stückentwicklung ins Blaue und die Rolle der Isa in Bilder deiner großen Liebe. Seit der Spielzeit 2019/20 ist Johanna Franke festes Ensemblemitglied am Theater Plauen-Zwickau. Hervorzuheben ist hierbei u.a. die Rolle der Katja in der Theateradaption von Aus dem Nichts (Regie: Sebastian Sommer). Auch war sie neben ihrem Theaterschaffen das erste Mal am Dreh einer Webserie beteiligt ( 2 Minuten, Regie: Lisa Miller).

Kim Ehinger wurde 1991 in Konstanz geboren und studierte Regie an der Akademie für darstellende Kunst in Ulm. Darauf folgten mehrere Regieassistenzen u.a. am Theater Heidelberg unter Brit Bartkowiak, am Theater Konstanz unter Neil LaBute und am Theater der Keller in Köln unter Steffen Jäger. 2015 wurde sie mit ihrem Stück Wie es weitergeht für das Interplay Europe  Festival for young Playwrights nach Bregenz eingeladen. 2017 war sie Teilnehmerin der Dramatiker*innenbörse in Nenzing, in deren Rahmen eine szenische Lesung ihres Textes Die Vaterlosen stattfand. Desweitern inszenierte sie ihren Text Das erstgeborene Land in der Brotfabrik Berlin, sowie am Theaterdiscounter Berlin. Sie hospitierte an der Schaubühne am Lehniner Platz in Berlin unter Thomas Ostermeier.
Von 2018 bis 2021 war Kim Ehinger am Theater Osnabrück als Regieassistentin engagiert. Dort inszenierte sie 2019 Und jetzt: die Welt! oder Es sagt mir nichts, das sogenannte Draußen von Sibylle Berg, welches sie 2020 in Folge der Pandemie zu einer Webserie für den Onlinespielplan des Theaters weiterentwickelte.
Kim Ehinger lebt als freie Regisseurin und Autorin in Berlin.

http://www.conne-island.de/termin/nr6622.html