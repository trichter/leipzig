---
id: "6596"
title: Karaoke auf dem Freisitz
start: 2023-06-16 19:00
end: 2023-06-16 23:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6596.html
isCrawled: true
---
Karaoke auf dem Freisitz

Karaoke + Fassbier + Food

http://www.conne-island.de/termin/nr6596.html