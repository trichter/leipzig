---
id: "6593"
title: Bowl Night Skate Session
start: 2023-07-07 18:30
end: 2023-07-07 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6593.html
isCrawled: true
---
Rahmenprogramm der Deutschen Skateboard Meisterschaft

mit Drinks und BBQ.

Ab 20 Uhr werfen Aero, Bambule und Beyond ihre neusten Skatevideoproduktionen auf die Leinwand. 

Alle Infos und das gesamte Programm findet ihr unter  www.heizhaus-leipzig.de


http://www.conne-island.de/termin/nr6593.html