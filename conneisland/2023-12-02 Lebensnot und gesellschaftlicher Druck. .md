---
id: "6687"
title: Lebensnot und gesellschaftlicher Druck. Seminar mit Christine Kirchhoff
start: 2023-12-02 10:00
end: 2023-12-02 14:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6687.html
isCrawled: true
---
Lebensnot und gesellschaftlicher Druck. Seminar mit Christine Kirchhoff

Als ein Produkt ständigen Opferns der Triebbefriedigung schreitet Kultur nicht linear fort oder ist vererbbar, sondern vielmehr ein Prozess der Aneignung und muss ständig und von jedem Einzelnen wiederholt werden. Sigmund Freud hat dies als Lebensnot gefasst und darin einen Naturzwang gesehen. Vorausgesetzt ist darin der Mangel und daher der Zwang das Überleben zu sichern. Christine Kirchhoff argumentiert in Tradition der Kritischen Theorie dafür, das Konzept der Lebensnot in einer kritischen Wendung als gesellschaftlichen Druck zu diskutieren. Darin wird die Unvernunft der kapitalistischen Gesellschaft offenbar und die Lebensnot vom Schicksal zum kritischen Begriff, der vor der Kultur nicht haltmacht.

Christine Kirchhoff lehrt an der International Psychoanalytic University in Berlin und ist Autorin zahlreicher Bücher über Psychoanalyse und Gesellschaftskritik.

Die Veranstaltung findet im Infoladen statt. Es kann nur eine begrenzte Anzahl von Leuten teilnehmen, darum bitte eine Mail an KulturistKitt[at]conne-island.de

http://www.conne-island.de/termin/nr6687.html