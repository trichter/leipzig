---
id: "6541"
title: "Ü30-Spezial: Früher waren wir mal politisch (organisiert)"
start: 2023-05-30 17:30
end: 2023-05-30 21:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6541.html
isCrawled: true
---
Ü30-Spezial: Früher waren wir mal politisch (organisiert) Teil 2

Bei einem gelungenen gemeinsamen Auftakt sind wir ins Gespräch gekommen über viele unterschiedliche Aspekte rund um das Thema Antifa im Alter, die wir sicherlich nur anschneiden konnten. Darauf aufbauend gibt es einen zweiten Termin, der sich gewünscht wurde und bei dem wir die angeschnittenenen Punkte vertiefen wollen und uns weiter vernetzen möchten. Für all jene, die verhindert waren und trotzdem noch interessiert sind: es ist problemlos möglich bei Teil 2 neu einzusteigen. Um unterschiedliche Erwartungen an den nächsten Abend aufgreifen zu können und im Sinne des DIY, schickt doch gerne Punkte die ihr vertiefen möchtet oder eure Ideen via Mail an: frueher_politisch@riseup.net

Solltet ihr mit euren Kids kommen wollen und eine Betreuung benötigen, meldet euch bitte auch gerne vorab per Mail.

http://www.conne-island.de/termin/nr6541.html