---
id: "6711"
title: Knocked Loose
start: 2024-02-29 19:00
end: 2024-02-29 23:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6711.html
isCrawled: true
---
Knocked Loose

Knocked Loose 
+ Special Guest

Tickets sind ab dem 11.12.2023 um 12:00 Uhr auf folgender Seite erhältlich!

https://www.mawi-concert.de/index.php?menus_id=2&solo=1&id=27200&ve=236164

http://www.conne-island.de/termin/nr6711.html