---
id: "6628"
title: "Sound System Sessions #2"
start: 2023-10-07 21:00
end: 2023-10-08 04:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6628.html
isCrawled: true
---
Sound System Sessions #2

music
lectures
vibes

http://www.conne-island.de/termin/nr6628.html