---
id: "6651"
title: "Gruppo d´Azione, Ordnung untergraben  Chaos schaffen"
start: 2023-10-14 19:00
end: 2023-10-14 23:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6651.html
isCrawled: true
---
Gruppo d´Azione, Ordnung untergraben  Chaos schaffen

Lesung und Diskussion mit den beiden Autoren und dem Herausgeber

Ordnung untergraben  Chaos schaffen ist ausnahmsweise mal kein Werk über die Kurven der italienischen Meister oder eine der vielen, berühmten Curva Nords oder Suds des Stiefels, sondern ein eher unentdeckter Fleck mit ganz eigener Ultras-Geschichte seit dem Jahr 1974. Es geht um die Gruppo d´Azione aus Ferrara, Ultras von SPAL, die von 1986 bis 1992 aktiv war und damals die vorderste Reihe der Curva Ovest gebildet hat.

Das Buch ist 2015 im Original erschienen und von Fillippo Landini und Alessandro Casolari verfasst worden, der eine Mitglied, der andere Mitbegründer und Anführer der Gruppo dAzione. Zusammen haben sie ihre eigene Geschichte und die Geschichte einer ganzen Generation von Jugendlichen zu dieser Zeit und in vergleichbar großen, italienischen Städten aufgeschrieben. Einer Generation auf der Suche nach Action und sich selbst. Auf 288 Seiten und in 68 anekdotenreichen Kapiteln in denen vor allem die Auswärtsspiele, die Plätze der eigenen Stadt und die Ausflüge in fremde Länder, zu Stränden oder auf Konzerten beleuchtet werden, wird man mitten in die Lebensrealität der 80er/90er Jahre in Ferrara geholt. Eine spannende, aber eher unbeleuchtete Zeitspanne, die in mancher Hinsicht vielleicht auch einige Parallelen mit der aktuellen Phase der Ultras hierzulande aufweist  und deren Erzählung nun endlich auch auf Deutsch erschienen ist.

 
Eine Veranstaltung vom Verlag Erlebnis Fußball und vom Fanprojekt Leipzig.

https://erlebnis-fussball.de/

http://www.conne-island.de/termin/nr6651.html