---
id: "6521"
title: Skill Sharing - Kritik der individualisierten Konsumkritik
start: 2023-06-10 14:30
end: 2023-06-10 18:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6521.html
isCrawled: true
---
Gibt es ein richtiges Konsumieren im Falschen?

In einem Vortrag mit optionalem Workshop und Diskussion beschäftigen wir uns mit der Konsumkritik, also der Annahme das man ausschließlich oder am besten über einen anderen Konsum von anderen Waren die Welt verändern könnte. Vielen Menschen, meist aus privilegierten Milieus, macht nicht nur der Klimawandel Angst. Auch die gefühlte Leere eines Lebens als Arbeitskraftbehälter und bloßer Konsument sinnloser Warenberge sorgt für Unbehagen. Im ganz bewussten Konsum von vermeintlich besonders fairen, ökologischen und dabei meist auch teuren Dingen, macht man sich selbst zum Aushängeschild einer geistig moralischen Wende.

Doch nach welchen Prinzipien funktioniert eigentlich unsere Ökonomie, Produktion und Konsumption? Ein Blick auf das Verhältnis von Natur und Gesellschaft sowie die Grundmechanik der kapitalistischen Ökonomie zeigt uns, dass der Kapitalismus eben keine Konsumptionsweise sondern eine Produktionsweise ist. Konsum oder die Befriedigung gesellschaftlicher Bedürfnisse ist nicht Zweck des Kapitalismus, sondern die Produktion um ihrer Selbst willen. Diese Produktionsweise die nur die Vermehrung von Profit zum Ziel hat, unterliegt einem selbstzerstörerischem Wachstumszwang. Doch was können wir dagegen tun? Konsumkritik läuft falsch, wenn sie an die Stelle von gesellschaftlicher Kapitalismuskritik gesetzt wird: Falsch ist nicht die Frage danach, was man persönlich für gesellschaftliche Veränderungen tun kann, sondern die Reduktion darauf!

Die Reihe findet in Kooperation mit der Rosa Luxemburg Stiftung Sachsen und der Naturfreunde Jugend Leipzig statt.


http://www.conne-island.de/termin/nr6521.html