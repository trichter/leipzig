---
id: "6690"
title: Sportpark Leutzsch / Über 100 Jahre Alfred-Kunze-Sportpark
start: 2023-12-06 18:00
end: 2023-12-06 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6690.html
isCrawled: true
---
Sportpark Leutzsch / Über 100 Jahre Alfred-Kunze-Sportpark

Der Alfred-Kunze-Sportpark in Leipzig ist ein Flickenteppich aus maroden Stehplaetzen und einer Sitzplatztribuene mit wurmstichigem Dach. Mit anderen Worten: ein wunderschoenes Stadion. (Christoph Biermann in der 11 Freunde)
 
Da ist Geschichte dran, verstehste, da haben wir die Bonzen geschlagen, da waren wir die Groeßten, das lernste nicht in der Schule, Junge, Geschichte ist Geschichte.
(Clemens Meyer in seinem Roman Als wir traeumten)
 
 
 
Seit über 100 Jahren liegt im Westen der Stadt Leipzig der Alfred-Kunze-Sportpark. Mit seiner einzigartigen Lage am Rande von Leipzig-Leutzsch, zwischen Arbeitersiedlungen, Villenviertel und Landschaftsschutzgebiet wurde der Sportpark nicht nur eines der beruehmt-beruechtigtsten Fußballstadien des Landes, sondern ebenso zu einem Raum fankultureller, politischer und sozialer Aushandlungen. In zwei Baenden, mit insgesamt 784 Seiten, illustriert von rund 1100 Bildern, Bauplaenen, Archivalien und Zeichnungen erzählen 12 Autorinnen und Autoren dessen Geschichte.
 
Im Rahmen der Buchvorstellung soll nicht nur die Geschichte des Sportparks als Bauwerk oder als reine Sportstaette erzaehlt werden. Vielmehr wird das Stadion als soziokultureller Raum, geprägt von dem Stadtteil, der ihn umgibt und den Akteurinnen und Akteuren, die ihn erfahren, erlebt und erschaffen haben, im Mittelpunkt stehen. Die Lesung zum Buch begleitet Fans literarisch auf dem Weg zum Stadion, ehe ausführlich die historisch-politische Entwicklung des Stadtteils Leutzsch im Spiegel der Stadtgeschichte dargestellt wird. Sie zeigt auf, wie sich die Fankultur im Stadion entwickelte und erstmals welchen Platz Frauen auf den Raengen und im Verein hatten und haben und wie sie sich diesen bis heute immer wieder erkaempfen muessen.
 
 
Mittwoch, 6. Dezember 2023
Einlass 18 Uhr, Beginn 19 Uhr

http://www.conne-island.de/termin/nr6690.html