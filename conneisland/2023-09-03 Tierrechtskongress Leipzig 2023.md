---
id: "6606"
title: Tierrechtskongress Leipzig 2023
start: 2023-09-03 12:00
end: 2023-09-03 16:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6606.html
isCrawled: true
---
Tierrechtskongress Leipzig 2023

Am Wochenende vom 2. bis 3. September 2023 findet im Conne Island der zweite Leipziger Tierrechtskongress statt. Organisiert wird der Kongress wieder von Vegan in Leipzig, einem Kooperationsprojekt Leipziger Tierrechts- und Tierbefreiungsgruppen.

Dabei setzen sich erneut zahlreiche Referent*innen inhaltlich und kritisch mit verschiedenen Aspekten der Tierrechtsarbeit auseinander. Dieses Mal wird es Vorträge zu Themen wie Tierschutzrecht, Speziesismus in Alltag und Bildung sowie auch zu Diversity und Antidiskriminierung geben. Erstmalig schließen wir am ersten Tag die Runde mit einem Panel Talk zu Feminismus in der veganen Szene.

Nach intensiver Auseinandersetzung mit den Rückmeldungen und Wünschen zum ersten Leipziger Tierrechtskongress 2021, wird der Kongress in der zweiten Runde etwas entzerrter gestaltet sein und mehr Zeit für informellen Austausch sowie Anschlussdiskussionen mit den Referierenden bieten. Das Ziel bleibt weiterhin, vegan lebende Menschen und allgemein interessierte Personen zu vernetzen und gemeinsame Schnittmengen unterschiedlicher Gruppen und Bewegungen zu finden.

Sowohl Samstag als auch Sonntag beginnt der Kongress um 12 Uhr mit dem ersten Vortrag. Zwischen den täglich 5 Vorträgen wird es viel Pausenzeit geben, die zum Beispiel im Garten bei Kaffee, Kuchen und anderen veganen Leckereien der Feinkost Meißner verbracht werden können. Alle Vorträge werden auch per Lautsprecher im Garten zu hören sein. Im Anschluss an das offizielle Programm freuen sich die Veranstalter*innen auf regen Austausch mit allen Teilnehmenden und Interessierten im Conne Island.

Im Außenbereich werden wieder viele örtliche Gruppen und Organisationen an Informationsständen über sich und ihre Arbeit im Tierrechtsbereich aufklären. Auch eigens bedruckte T-Shirts und andere Textilien werden zu erwerben sein.

Eine Anmeldung zum Kongress ist nicht notwendig, auch sind derzeit keine Maßnahmen im Rahmen eines Hygienekonzeptes gegen das Corona-Virus zu erwarten. Der Eintritt bleibt kostenlos, Spenden sind freiwillig.

http://www.conne-island.de/termin/nr6606.html