---
id: "6648"
title: Lesung "Punk as F*ck" + Vortrag "Sexismus im Punk"
start: 2023-11-03 19:00
end: 2023-11-03 23:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6648.html
isCrawled: true
---
Lesung "Punk as F*ck" + Vortrag "Sexismus im Punk"

Das Akronym FLINTA steht für Frauen, Lesben, Intersexuelle-, Nicht-Binäre-, Transgender- und Agender-Personen. Der Begriff dient der Sichtbarmachung von Geschlechtsidentitäten und eint somit alle Menschen, die von patriarchalen Strukturen betroffen sind  auch und gerade in der Punkszene. 

Der Veranstaltungsbeitrag ist nicht nur eine Lesung mit Auszügen aus dem Buch "PUNK as F*CK - die Szene aus FLINTA Perspektive", welches Anfang September 2022 beim Ventil Verlag erschien, sondern vor allem ein Vortrag zu den Themen Punk und Feminismus und zur punktoo- Bewegung, welche die Grundlage für die Arbeit am Buch war.

50 Autor*innen berichten in diesem Sammelband davon, was sie als FLINTA in der Szene erlebten bzw. erleben mussten. Gleichzeitig gehen sie darauf ein, was ihnen Punk bedeutet und warum es sich aus ihrer Sicht für diese Subkultur zu kämpfen lohnt. 50 unterschiedliche Erfahrungsberichte, die einen unmittelbaren Einblick in den Alltag von FLINTA verschiedenster biografischer Hintergründe gewähren. 

http://www.conne-island.de/termin/nr6648.html