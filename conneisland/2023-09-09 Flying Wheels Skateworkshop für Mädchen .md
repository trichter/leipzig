---
id: "6586"
title: Flying Wheels Skateworkshop für Mädchen und Frauen
start: 2023-09-09 12:00
end: 2023-09-09 16:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6586.html
isCrawled: true
---
Flying Wheels Skateworkshop für Mädchen und Frauen (Tag 1)

Mit Unterstützung ausgebildeter Workshopleiterinnen können sowohl Anfängerinnen als auch fortgeschrittenen Skaterinnen den Conne Island Skatepark entdecken und ihre Skills ausbauen. Der Workshop richtet sich vor allem an die Altersgruppe zwischen 12 -27 Jahren. 

Der zweitägige Workshop findet Samstag und Sonntag jeweils von 12:00 - 14:00 Uhr statt.

Der Workshop ist kostenfrei. Skateboards, Helme und Schützer werden gestellt.

Die Anmeldung erfolgt unter: anmeldungworkshop@conne-island.de

Das Projekt wird gefördert durch das Amt für Jugend und Familie und durch den Urban Souls e.V. unterstützt.

http://www.conne-island.de/termin/nr6586.html