---
id: "6543"
title: "Anita Prestes: Olga Benario- eine biografische Annäherung"
start: 2023-05-12 17:00
end: 2023-05-12 21:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6543.html
isCrawled: true
---
Anita Prestes: Olga Benario- eine biografische Annäherung

Die Lesung findet in der feministischen Bibliothek MONALiesA statt (Bernhard Göring-Straße 152 04277 Leipzig)

Die brasilianische Historikerin Anita Leocadia Prestes kommt nach Leipzig, um ihr neues Buch  eine biographische Annäherung an das Leben ihrer Mutter Olga Benario Prestes  vorzustellen. Anhand neu zugänglicher Informationen aus dem Gestapo-Archiv schildert Anita Prestes, wie ihre Mutter, die Kommunistin Olga Benario, für ihre Ideale kämpfte. In dem Gespräch wollen wir zudem auf verschiedene Stationen ihres Lebens eingehen. Was bedeutet ihre Geschichte für antifaschistischen Widerstand in Gegenwart und Zukunft?

1936, vor 87 Jahren, wurde Anita Prestes als Tochter der Kommunistin und Antifaschistin Olga Benario Prestes und dem brasilianischen Revolutionär Luiz Carlos Prestes in Nazihaft in Berlin-Friedrichshain geboren. Mit der Campanha Prestes, einer internationalen Solidaritätskampagne für die Befreiung der politischen Gefangenen Olga Benario Prestes, ihrer Tochter Anita Leocadia und ihrer Genossin Elise Ewert erkämpften Anitas Großmutter und Tante väterlicherseits die Freilassung von Anita mit 14 Monaten. Bei ihnen wuchs Anita im mexikanischen Exil auf, bis sie 1945 nach Brasilien zurückkehrten. Olga Benario wurde ins KZ Ravensbrück verlegt, zu Zwangsarbeit verpflichtet und 1942 von den Nazis in Bernburg ermordet.

Anita interessierte sich für die Geschichte ihrer Mutter, die sie nie richtig kennengelernt hatte. Sie wurde selbst aktive Kommunistin und ging während der brasilianischen Diktatur ins sowjetische Exil. Sie studierte Chemie und später Geschichte und widmete ihr Interesse auch als Historikerin dem Leben ihrer revolutionären Eltern und dem Kampf gegen den Faschismus. Die Fähigkeit zum Widerstand und Standhaftigkeit von Olga Benario ist das, was sie besonders an ihrer Mutter beeindruckt.

Das Gespräch findet auf Deutsch und Portugiesisch statt und wird verdolmetscht.
Eine Veranstaltung der Feministischen Bibliothek MONAliesA, der Gedenkstätte für Zwangsarbeit Leipzig, dem Conne Island, der Rosa-Luxemburg-Stiftung und dem Verbrecher Verlag. Diese Veranstaltung wird mit Mitteln aus dem Bundesprogramm Demokratie leben finanziert.


http://www.conne-island.de/termin/nr6543.html