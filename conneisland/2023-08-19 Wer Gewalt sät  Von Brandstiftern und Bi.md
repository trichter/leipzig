---
id: "6630"
title: "Wer Gewalt sät  Von Brandstiftern und Biedermännern"
start: 2023-08-19 16:30
end: 2023-08-19 20:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6630.html
isCrawled: true
---
Wer Gewalt sät  Von Brandstiftern und Biedermännern

Vom 22. bis zum 26. August 1992 fand das rassistische Pogrom von Rostock-Lichtenhagen statt, im letzten Jahr gab es am 27. August 2022 eine Gedenkdemonstration in Rostock, an der sich auch Antifaschist*innen aus Leipzig beteiligten. Am Vorabend dieser Demonstration wurde ein Brandanschlag in Leipzig auf die Gemeinschaftsunterkunft für Geflüchtete in der Liliensteinstraße in Grünau verübt, zu diesem Zeitpunkt lebten 180 Menschen in dem Gebäude. Bis heute wurde dieser Versuch eines rechten Brandanschlags auf ein Wohnhaus nicht aufgeklärt.

Ab den 21. August 2015 tobte der rassistische Mob im sächsischen Heidenau und ab den 26. August 2018 in Chemnitz. All diesen Ereignissen ist gemeinsam, dass nicht nur offene Rassist*Innen und Neonazis auf der Straße waren, die versuchten Menschen zu verletzten und zu töten, sondern auch die so genannten Biedermänner diese Situationen möglich machten und forcierten.

Wer gegen die Nazis kämpft, der kann sich auf den Staat überhaupt nicht verlassen. Esther Bejarano

Ein Satz, der seit vielen Jahren zitiert wird, aber was für Schlüsse und Konsequenzen werden daraus eigentlich gezogen? Was folgt gerade für Antifaschist*innen in Sachsen aus dieser Erkenntnis und den rassistischen Angriffen und Anschlägen der vergangenen Jahre?

https://www.rassismus-toetet-leipzig.org/

die Veranstaltung findet im Café statt

http://www.conne-island.de/termin/nr6630.html