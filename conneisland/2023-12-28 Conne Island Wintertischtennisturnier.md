---
id: "6705"
title: Conne Island Wintertischtennisturnier
start: 2023-12-28 12:00
end: 2023-12-28 16:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6705.html
isCrawled: true
---
Conne Island Wintertischtennisturnier

Anmeldung ab 12:00
Einspielen 12.00 - 13.00
Start: 13:00
Startgebühr: 5
Mixed Teams only!

Nach 3 Jahren Pause endlich wieder ein Winter Tischtennisturnier im Conne Island. Gemeinsam mit unseren Freund*innen von Roter Stern Leipzig veranstalten wir den Post X-mas Sportklassiker. Neben Getränken und Musik wird es auch wieder einiges zu gewinnen geben. Der Spaß am Spiel und Austausch stehen bei diesem Turnier im Vordergrund! Deshalb haben wir uns eine schöne Überraschung überlegt. Wir freuen uns auf einen entspannten Nachmittag mit euch!

Indoor - Djing & Hangout

http://www.conne-island.de/termin/nr6705.html