---
id: "6671"
title: "Phantastische Gesellschaft: Falsche Juden und imaginierte
  Familiengeschichten zur NS-Verfolgung"
start: 2023-11-29 18:00
end: 2023-11-29 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6671.html
isCrawled: true
---
Phantastische Gesellschaft: Falsche Juden und imaginierte Familiengeschichten zur NS-Verfolgung

Phantastische Gesellschaft: Falsche Juden und imaginierte Familiengeschichten zur NS-Verfolgung

Auf dem Panel: 

Clemens Böckmann und Johannes Spohr, Herausgeber des Sammelbands Phantastische Gesellschaft 

Miklós Klaus Rózsa, Photograph aus Zürich und einer der Interviewpartner des Buches

Moderation: Alexandra Bandl

Es diskutieren Miklos Rosza, Alexandra Bandl und die Herausgeber des Buches »Phantastische Gesellschaft« über jüdische Identität in Deutschland und das Phänomen, dass einige christliche Deutsche erfundene Geschichten über jüdische Verfolgte für politische Zwecke nutzen. Das Buch untersucht die Motive, gesellschaftliche Rollen und Auswirkungen dieses Verhaltens auf Erinnerungskultur und Zeitzeugenschaft sowie die Grenze zwischen Fakten und Fiktion.

Unterstützt von der Amadeu-Antonio-Stiftung.

Sammelband Phantastische Gesellschaft https://neofelis-verlag.de/verlagsprogramm/wissenschaft/politik-debatte/1048/phantastische-gesellschaft

http://www.conne-island.de/termin/nr6671.html