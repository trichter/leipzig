---
id: "6502"
title: Das Imperiale Russland und seine Huldigung durch linke Dogmatiker:innen
start: 2023-05-03 18:30
end: 2023-05-03 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6502.html
isCrawled: true
---
Vortrag von Anastasia Tikhomirova

Der Vertrag wird erst 19:30 beginnen, nicht wie ursprünglich geplant 19:00.

Am 24. Februar 2022 marschierte die russische Armee in die Ukraine ein. Der Krieg, der im Osten der Ukraine schon seit 2014 nicht enden will, hat sich damit zu einem schonungslosen Angriffskrieg gegen die gesamte Ukraine und ihre Bürger:innen ausgeweitet. In Sachen Angriffskrieg besitzt Russland bereits einige Erfahrung: Die Tschetschenienkriege 1994 und 1999, der Georgienkrieg 2008 und der Militäreinsatz in Syrien 2015 dienten als Blaupause für die Invasion in der Ukraine. Doch auch heute noch gelingt es zahlreichen westlichen linkspolitischen Akteur:innen nicht, die Beweggründe für diesen Krieg richtig zu erfassen und ihn als das zu begreifen, was er ist: Eine besonders aggressive Form des russischen Imperialismus und Neokolonialismus. Stattdessen wird über Putins mentalen Gesundheitszustand, Nazis in der Ukraine oder die Osterweiterung der NATO diskutiert, der die vermeintlich antiimperialistische westliche Deutung dieses Krieges eine Mitschuld einräumen will. Dabei wäre für eine konsequent antiimperialistische Haltung gerade jetzt eine Auseinandersetzung mit der russischen Geschichte und den kolonialen Kontinuitäten darin vor, während und nach dem Bestehen der Sowjetunion, unabdingbar, um die Frage nach den ideologischen Hintergründen des Angriffskriegs beantworten zu können.

Anastasia Tikhomirova ist freie Journalistin, Kulturwissenschaftlerin und Moderatorin. Sie ist Alumna des Marion-Gräfin-Dönhoff Stipendiums der Internationalen Journalistenprogramme 2021, welches sie bei der Novaya Gazeta in Moskau absolvierte. Außerdem macht sie ihren Master in Osteuropastudien und interdisziplinärer Antisemitismusforschung in Berlin.


http://www.conne-island.de/termin/nr6502.html