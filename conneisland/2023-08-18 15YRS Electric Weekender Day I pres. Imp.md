---
id: "6612"
title: 15YRS Electric Weekender Day I pres. Import/Export
start: 2023-08-18 20:00
end: 2023-08-19 00:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6612.html
isCrawled: true
---
15 YRS ELECTRIC WEEKNENDER DAY I pres. Import/Export

We're celebrating 15 years of the notorious Electric Weekender with a bunch of high class artists.

15 YRS ELECTRIC WEEKNENDER DAY I pres. Import/Export

Line-Up Friday:
Albina 
Alto Bloom (live)
DJ Balduin (live)
Heather Karing (live)
Wilted Woman (live) 

http://www.conne-island.de/termin/nr6612.html