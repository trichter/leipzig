---
id: "6452"
title: Halftime
start: 2023-05-10 18:00
end: 2023-05-10 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6452.html
isCrawled: true
---
Halftime

Der Einlass ist begrenzt. Daher haben wir einen Telegram Channel eingerichtet, über den wir über einen Einlassstop informieren. Wenn das der Fall ist, vermeidet bitte lange Schlangen und kommt später wieder. Wir sagen bescheid, wenn wieder Platz ist!
https://t.me/joinchat/ME7JEF1Gs4Y3Mjhi

x no dogs allowed x 

http://www.conne-island.de/termin/nr6452.html