---
id: "6532"
title: Spendensammlung für die Erdbebenregion in der Türkei und Syrien
start: 2023-05-14 15:00
end: 2023-05-14 19:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6532.html
isCrawled: true
---
Soli-Aktionen

Spendensammlung für die Erdbebenregion in der Türkei und Syrien

Open Air ab 15 Uhr: Essen & Getränke, Aktivitäten für Kinder, Live Musik (DJ Rahmlet, Viola-Loopdeck (Cenk Ergüner), Saz (Mert Güney)). 

Konzerte ab 19 Uhr: KüKo Sounds, Element of Elephants, DJ Rahmlet

Fortlaufend: Live-Informationen zu den Wahlergebnissen in der Türkei

Die gesammelten Spenden gehen an die Platform Kunstaktivitäten für Kinder (https://www.instagram.com/cocuksanatalaniplatformu/). Diese NGO organisiert Workshops mit künstlerischen Aktivitäten für Kinder in der vom Erdbeben betroffenen Region in der Türkei, um ihre psychosoziale Situation zu verbessern.

Eine freiwillige Organisation von Kültür Kollektiv Leipzig (https://www.kuko-leipzig.de/) zusammen mit Conne Island.


http://www.conne-island.de/termin/nr6532.html