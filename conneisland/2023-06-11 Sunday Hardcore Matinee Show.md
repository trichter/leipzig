---
id: "6510"
title: Sunday Hardcore Matinee Show
start: 2023-06-11 15:00
end: 2023-06-11 19:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6510.html
isCrawled: true
---
Sunday Hardcore Matinee Show

INDOOR Matinnee Show

One King Down
Shutdown
Swoon
No Turning Back
Worst Doubt

http://www.conne-island.de/termin/nr6510.html