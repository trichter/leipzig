---
id: "6614"
title: 15YRS Electric Weekender Day III
start: 2023-08-20 11:00
end: 2023-08-20 15:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6614.html
isCrawled: true
---
15YRS ELECTRIC WEEKENDER DAY III

We're celebrating 15 years of the notorious Electric Weekender with a bunch of high class artists.

Line-Up Sunday:
Catnip (live)
DJ DVB
Hannie Phi & Jewelry
Luise
Rosa Red & EnelRAM

http://www.conne-island.de/termin/nr6614.html