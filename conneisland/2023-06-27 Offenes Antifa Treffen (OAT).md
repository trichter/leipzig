---
id: "6600"
title: Offenes Antifa Treffen (OAT)
start: 2023-06-27 18:30
end: 2023-06-27 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6600.html
isCrawled: true
---
Offenes Antifa Treffen (OAT)

"Nur gemeinsam kommen wir da durch!"

Harte Wochen waren das, daher soll es im Juni keinen Vortrag geben, sondern Raum für Austausch und Fragen, die euch beschäftigen und die wir vielleicht kollektiv beantworten können. Vielleicht habt ihr auch Ideen wie Menschen unterstützt werden können, die von der Repression der letzten Wochen betroffen waren und sucht weitere Menschen, die das gerne mit euch zusammen umsetzen würden. Nur gemeinsam kommen wir gegen die Angriffe auf Antifaschist*innen an.

Wenn ihr krank seid, bleibt bitte zu Hause.

Was ist das Offene Antifa Treffen?

Es ist wichtig, sich zu vernetzen und Strukturen aufzubauen, um den Widerstand zu organisieren  nicht nur gegen Rassismus und Faschismus, sondern auch gegen Nationalismus, Sexismus, Antisemitismus, Homosexuellenfeindlichkeit und die kapitalistische Gesamtscheiße.

Du willst endlich was dagegen tun, wusstest bisher aber nicht wo, wie und mit wem? Dann komm zum Offenen Antifa Treffen (OAT).


http://www.conne-island.de/termin/nr6600.html