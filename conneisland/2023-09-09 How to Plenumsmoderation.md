---
id: "6624"
title: "VERSCHOBEN AUF TBA How to: Plenumsmoderation"
start: 2023-09-09 15:00
end: 2023-09-09 19:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6624.html
isCrawled: true
---
VERSCHOBEN AUF TBA: FANTIFA - How to: Plenumsmoderation

VERSCHOBEN AUF TBA

Das Bündnistreffen war mal wieder ultra langwierig und am Ende hat niemand gecheckt was eigentlich entschieden wurde. Die letzte Zoom-Konverenz war mega durcheinander, weil alle zu besprechenden Punkte sich vermischt haben und alles gleichzeitig diskutiert wurde. Beim Plenum halten die Typen immer super lange Redebeiträge und alle anderen sind genervt.

Plena sind ein wesentlicher Teil von politischer Arbeit und trotzdem oft kräftezehrend und vor allem lang! Sie sind unser Diskussionsforum, unser wichtigstes Planungsinstrument und eine Gelegenheit, Positionen und Strategien auszutauschen.

Natürlich es gibt kein Zaubermittel wie alle Plena harmonisch und erfolgreich werden. Aber eine strukturierte Moderation kann doch zumindest bei allen oben beschriebenen Situationen helfen. Ein gutes Plenum erledigt nicht nur die anstehenden Aufgaben, sondern es beteiligt alle und hat Mittel, zu Konsens und Entscheidungen zu führen. Gute Moderation kann helfen, alle diese Dinge geschehen zu lassen. Es braucht die Moderation als Methode, Treffen und Plena zu gestalten und eure Gruppe zufrieden mit der  Entscheidungsfindung und dem Treffen zu machen.

Wir wollen mit euch überlegen: was wir von Moderation erwarten, wie verschiedene Plenas moderiert werden und welche Methoden es gibt Gruppengespräche zu moderieren und gemeinsam zu Entscheidungen zu treffen.


http://www.conne-island.de/termin/nr6624.html