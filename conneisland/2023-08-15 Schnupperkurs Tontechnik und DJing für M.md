---
id: "6609"
title: Schnupperkurs Tontechnik und DJing für Mädchen
start: 2023-08-15 10:00
end: 2023-08-15 14:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6609.html
isCrawled: true
---
Schnupperkurs Tontechnik und DJing für Mädchen

Di, 15.8.
10 - 15 Uhr
Schnupperkurs Tontechnik und DJing im Conne Island
Workshop für Mädchen zwischen 12 und 16 Jahren

Ort: Conne Island Saal
Mitbringen: USB-Stick mit eigener Musik
Teilnehmer*innen: 8
Anmeldung an: anna@conne-island.de


Im Conne Island finden Konzerte und Tanzveranstaltungen verschiedenster Genre wie Hip Hop, Pop, House, Punk und Hardcore statt. Bei jeder Veranstaltung muss die Anlage betreut werden, oder draußen auf dem Freisitz erst Boxen, Mischpult, Monitore und DJ-Technik aufgebaut werden. Im Workshop zeigen wir Dir, wie eine kleine Aktiv-PA (Public Address-Beschallungsanlage) verkabelt wird und welches Mischpult für eine Band, eine Lesung oder ein DJ-Set gebraucht wird. Außerdem könnt ihr an digitalen CDJs die Technik des auflegens ausprobieren. DJs verwenden diese Player, um ihre Tracks und Songs übergangslos ineinander zu mischen.

Für den DJ-Part könnt ihr eigene Musik mitbringen (.mp3 auf einem USB-Stick oder CDs).

Marlene und Anna, die den Workshop halten, veranstalten selbst im Conne Island und in anderen Spielstätten Konzerte und Tanzveranstaltungen und legen als DJ-Team auf.

-
Barrierefreiheit
Wenn Du begleitende Unterstützung benötigst, melde Dich bitte bei uns. Wir können deinen Besuch vor Ort gemeinsam planen.

http://www.conne-island.de/termin/nr6609.html