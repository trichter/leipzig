---
id: "6681"
title: Kultur als Theater um die Subvention oder Subversion. Eine Reprise der Begriffe
start: 2023-11-10 19:00
end: 2023-11-10 23:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6681.html
isCrawled: true
---
Kultur als Theater um die Subvention oder Subversion. Eine Reprise der Begriffe

Kultur ist gut  Kulturgut; und doch ist jedes dieser guten Güter niemals ein Dokument der Kultur, ohne zugleich ein solches der Barbarei zu sein. Das aber meint nicht die Kulturministerin. Als Gut ist Kultur immer auch Zeichen des Triumphs über das am Boden - über die Unterlegenen. Weil ihre Arbeit sich angeeignet wird, obwohl sie abgeschafft gehört, trägt die Kultur die Barbarei mit sich.

Ist und bleibt Kultur der Kitt, dann versöhnt sie mit barbarischen Verhältnissen für die auch eine Claudia Roth steht, statt für eine Versöhnung einzustehen, die nur im Abseits von beiden zu kultivieren wäre.

Die Veranstaltung findet im Infoladen statt.

http://www.conne-island.de/termin/nr6681.html