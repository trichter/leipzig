---
id: "6610"
title: 3D-Objekte und kleine Animationen bauen - Blender Kennenlernen
start: 2023-10-06 13:00
end: 2023-10-06 17:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6610.html
isCrawled: true
---
3D-Objekte und kleine Animationen bauen - Blender Kennenlernen. Workshop für Mädchen

Do, 24.8.
15 - 18 Uhr
3D-Objekte und kleine Animationen bauen - Blender Kennenlernen
Workshop für Mädchen zwischen 14 und 18 Jahren

Ort: Backstage Conne Island
Mitbringen: Laptop, Maus (kann auch geliehen werden)
Teilnehmer*innen: 6
Anmeldung an: anna@conne-island.de

Blender ist eine 3D-Modellierungs-Software, in der man Bilder in 3D oder auch Animationen kreieren kann. Blender gehört zu den 3D-Softwares, die von in Berufszweigen professionell genutzt wird - von der Innenarchitektur über Grafikdesign bis zum Industriedesign oder im Animationsfilm. Dabei ist die Software kostenlos und open source.

In diesem Workshop lernt ihr die ersten Schritte in dieser komplexen Software. Ihr modelliert ein eigenes Objekt, legt die Farben und Texturen der Oberfläche fest, und nehmt es als Bilddatei oder als kurzes Video mit nach Hause.

Für den Workshop benötigt ihr einen Laptop mit 32 GB RAM oder mehr und eine externe Maus. Falls ihr euch unsicher seid, ob euer Laptop geeignet ist, oder ihr keinen mitbringen könnt, schreibt uns gerne an, wir finden eine Lösung bzw organisieren genügend Laptops für den Workshop.

Den Workshop halten Marlene und Anna. Marlene hat Informatik studiert und arbeitet gerade mit künstlicher Intelligenz, vor dem Studium hat sie eine Kunstschule besucht. Anna arbeitet an der Kunsthochschule Burg Giebichenstein in Halle und hat sich in diesem Kontext die Funktionen in Blender mit Video-Tutorials selbst beigebracht.

-
Barrierefreiheit
Wenn Du begleitende Unterstützung benötigst, melde Dich bitte bei uns. Wir können deinen Besuch vor Ort gemeinsam planen.

http://www.conne-island.de/termin/nr6610.html