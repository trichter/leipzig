---
id: "6557"
title: "2cl Sommerkino und Jüdische Wochen 2023: FRÜHLING FÜR HITLER / THE PRODUCERS"
start: 2023-06-28 21:00
end: 2023-06-29 04:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6557.html
isCrawled: true
---
FRÜHLING FÜR HITLER / THE PRODUCERS

Mit Einfürung zum Film

USA 1968, Mel Brooks, 87 Min., OmU
mit Zero Mostel, Gene Wilder

Einfach das schlechteste Musical aller Zeiten.


http://www.conne-island.de/termin/nr6557.html