---
id: "6481"
title: "Skill Sharing - Männerfreundschaften  auf der Suche nach Gefühlen"
start: 2023-04-15 14:30
end: 2023-04-15 18:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6481.html
isCrawled: true
---
Skill Sharing - Einführungsworkshops in emanzipative Theorie und Praxis

Über eigene Probleme, Unsicherheiten und Ängste gefühlvoll und empathisch zu sprechen fällt vielen Boys häufig schwer.
Das macht sich auch in den Freundschaften mit und unter ihnen bemerkbar.
Gemeinsam wollen wir in diesem Workshop dem nachspüren, warum das eigentlich so ist,
was belastend daran ist und natürlich darüber sprechen, was es bräuchte um aus diesen Mustern ein Stück weit auszubrechen.
Der Workshop ist offen für all gender. Der Fokus liegt auf dem Austausch untereinander und gemeinsamen Diskussionen. 

Die Reihe findet in Kooperation mit der Rosa Luxemburg Stiftung Sachsen statt.

http://www.conne-island.de/termin/nr6481.html