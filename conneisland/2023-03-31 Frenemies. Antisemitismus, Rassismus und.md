---
id: "6484"
title: Frenemies. Antisemitismus, Rassismus und ihre Kritiker*innen.
start: 2023-03-31 18:30
end: 2023-03-31 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6484.html
isCrawled: true
---
Buchvorstellung und Diskussion mit Sina Arnold, Doris Liebscher und Ronya Othmann

Eigentlich könnte doch alles ganz einfach sein, oder? Antisemitismus und Rassismus sind beides menschenfeindliche Einstellungen, die von allen bekämpft werden müssen. Die Kritik dieser Ideologien müsste deshalb stets zusammen geleistet werden. In der Praxis kommt es jedoch immer wieder zu Unvereinbarkeiten und handfesten Auseinandersetzungen, mit wechselseitigen Ausschlüssen, Relativierungen, Beschuldigungen und einem Klima des Argwohns.

Das Buch Frenemies. Antisemitismus, Rassismus und ihre Kritiker*innen, herausgegeben von Meron Mendel, Saba-Nur Cheema und Sina Arnold und 2022 erschienen im Verbrecher Verlag, versammelt kurze Texte von Forscher*innen, Bildungspraktiker*innen, Aktivist*innen, die jeweils als Antworten zu naiven Fragen dargestellt werden  in Form eines FAQ. Was unterscheidet Antisemitismus und Rassismus? Gibt es Verbindungen zwischen Nationalsozialismus und Kolonialismus? Ist BDS antisemitisch? Sind Jüdinnen und Juden weiß? Wer wird durch die Bezeichnung antimuslimischer Rassismus außer Acht gelassen? Geht die Justiz härter gegen Antisemitismus als gegen Rassismus vor? Wie werden diese Debatten in anderen Ländern geführt? Der Anspruch des Buches ist es, einen niedrigschwelligen Einstieg in ein komplexes und konfliktreiches Themenfeld zu liefern. Die Schwerpunkte liegen auf Antisemitismus, antimuslimischem und anti-Schwarzem Rassismus.

Die Autorinnen Ronya Othmann und Doris Liebscher und die Herausgeberin Sina Arnold stellen das Buch und einige der Texte vor und diskutieren gemeinsam über Scheitern und Perspektiven linker Kämpfe gegen Rassismus und Antisemitismus.

http://www.conne-island.de/termin/nr6484.html