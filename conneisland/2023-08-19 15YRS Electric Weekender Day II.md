---
id: "6613"
title: 15YRS Electric Weekender Day II
start: 2023-08-19 23:00
end: 2023-08-20 06:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6613.html
isCrawled: true
---
15YRS ELECTRIC WEEKENDER DAY II

We're celebrating 15 years of the notorious Electric Weekender with a bunch of high class artists.

Line-Up Saturday:
akkro
Aril Brikha
Evan Baggs
Jlululu
Naitwa
Perm & HAL
S.ra
96kbps & Easy Miner 


http://www.conne-island.de/termin/nr6613.html