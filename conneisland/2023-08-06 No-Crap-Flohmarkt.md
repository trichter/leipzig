---
id: "6588"
title: No-Crap-Flohmarkt
start: 2023-09-24 13:00
end: 2023-09-24 17:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6588.html
isCrawled: true
---
No-Crap-Flohmarkt

Nachholtermin: No-Crap-Flohmarkt auf dem Freisitz des Conne Island.

Bei Musik und Sonnenschein habt ihr die Möglichkeit eure alten Lieblingsstücke loszuwerden und Neue mit nach Hause zu nehmen.

Der No-Crap-Flohmarkt versteht sich als alternativ und unkommerziell. Statt Trödel und Nazi-Devotionalien soll es um Sub- und Popkultur gehen. Durchsucht eure Plattensammlungen und Fanzines, eure Kleider- und Schuhschränke und verbringt mit uns einen schönen Nachmittag beim Stöbern und Tauschen.



http://www.conne-island.de/termin/nr6588.html