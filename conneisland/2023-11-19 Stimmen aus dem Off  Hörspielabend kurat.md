---
id: "6683"
title: "Stimmen aus dem Off?  Hörspielabend kuratiert von Lukas Holfeld"
start: 2023-11-19 19:00
end: 2023-11-19 23:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6683.html
isCrawled: true
---
Stimmen aus dem Off?  Hörspielabend kuratiert von Lukas Holfeld

In Form von Streifzügen durch Hörspiele und Stücke wollen wir uns der Kultur als Dialog mit den Toten nähern und den konstruktiven Defätismus kultivieren.

Lukas Holfeld betreibt den Blog Das Ärgernis, gibt das Magazin Kunst Spektakel und Revolution heraus und macht seit vielen Jahren Radio. Bei Radio Lotte, Radio Corax und dem FSK erscheint seine monatliche Sendung WutpilgerStreifzüge.

Die Veranstaltung findet im Infoladen statt.

http://www.conne-island.de/termin/nr6683.html