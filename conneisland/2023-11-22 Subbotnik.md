---
id: "6668"
title: Subbotnik
start: 2023-11-22 10:00
end: 2023-11-22 14:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6668.html
isCrawled: true
---
Subbotnik

Keep your Island clean and your hands dirty!

Für Verpflegung ist gesorgt.



http://www.conne-island.de/termin/nr6668.html