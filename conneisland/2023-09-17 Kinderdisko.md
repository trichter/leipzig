---
id: "6620"
title: Kinderdisko
start: 2023-09-17 14:00
end: 2023-09-17 18:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6620.html
isCrawled: true
---
Kinderdisko

Ballons, Waffeln, Limo, Schminken. 


DJs:
Die Gang (DJ .Punkt. and fam)
Miami Schnüller Soundsystem


Bring your parents!
no afterhour, cause the kids are alright


http://www.conne-island.de/termin/nr6620.html