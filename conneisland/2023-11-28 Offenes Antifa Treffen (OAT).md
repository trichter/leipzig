---
id: "6693"
title: Offenes Antifa Treffen (OAT)
start: 2023-11-28 19:00
end: 2023-11-28 23:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6693.html
isCrawled: true
---
Offenes Antifa Treffen (OAT)

OAT im November: Aufstand der Anständigen

Im Jahr 2000 wurde von Bundeskanzler der Aufstand der Anständigen in Deutschland ausgerufen. Viel wurde in den letzten Jahren dazu geschrieben, so hieß es bereits 2002 im AIB: Der »Aufstand der Anständigen« traf die Antifa-Bewegung weitgehend unvorbereitet und in einer Situation, in der viele Gruppen und EinzelaktivistInnen mit der alltäglichen Arbeit schon überfordert waren. Als unmittelbare Reaktion auf den »anständigen Sommer« zeichneten sich mehrere Tendenzen ab. (https://antifainfoblatt.de/aib56/antifa-bewegung)

Wir wollen uns beim OAT damit beschäftigen, gab es diesen "Aufstand" eigentlich? Was wird darunter verstanden und wo ist dieser "Aufstand der Anständigen" eigentlich in den letzten Jahren zu sehen und zu erleben?

Was ist das Offene Antifa Treffen?

Es ist wichtig, sich zu vernetzen und Strukturen aufzubauen, um den Widerstand zu organisieren  nicht nur gegen Rassismus und Faschismus, sondern auch gegen Nationalismus, Sexismus, Antisemitismus, Homosexuellenfeindlichkeit und die kapitalistische Gesamtscheiße.

Du willst endlich was dagegen tun, wusstest bisher aber nicht wo, wie und mit wem? Dann komm zum Offenen Antifa Treffen (OAT).

Nächstes OAT am 28. November 2023 um 19 Uhr im Conne Island im Vorderhaus (Koburger Str. 3, 04277 Leipzig)  Wenn ihr krank seid, bleibt bitte zu Hause.

http://www.conne-island.de/termin/nr6693.html