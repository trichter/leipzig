---
id: "6467"
title: 'Buchvorstellung und Gespräch: "FEMI(NI)ZIDE. Kollektiv patriarchale
  Gewalt bekämpfen"'
start: 2023-04-29 19:00
end: 2023-04-29 23:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6467.html
isCrawled: true
---
FEMI(NI)ZIDE. Kollektiv patriarchale Gewalt bekämpfen

Das Autor*innen-Kollektiv BIWI KEFEMPOM (bis wir keinen einzigen Femi(ni)zid mehr politisieren müssen) sind Judith Goetz, Cari Maier, Kyra Schmied und Marcela Torres Heredia aus Wien.Im Gespräch mit Bettina Wilpert stellen sie ihr Buch vor, außerdem wird ein*e Vertreter*in von KeineMehr Leipzig das Podium ergänzen und über die lokale Perspektive auf Femi(ni)zide berichten. 
Seit Sommer 2020 lässt die feministische Vernetzung »Claim the Space« in Wien keinen Femi(ni)zid mehr unbeantwortet und fordert damit kontinuierlich eine öffentliche Auseinandersetzung ein. Als Teil davon und anknüpfend an feministische Kämpfe in Lateinamerika und der Karibik diskutiert das österreichische Autor*innenkollektiv die Analysen von Femiziden und Feminiziden für den deutschsprachigen Raum. Dabei dient Femi(ni)zid als politischer Begriff der Benennung und Bekämpfung eines breiten Kontinuums patriarchaler Gewalt gegen Frauen, Lesben, inter, nichtbinäre, trans und agender Personen (FLINTA). Das Buch thematisiert die strukturellen und intersektionalen Gewaltverhältnisse, die den Morden zugrunde liegen. Die Autor* innen nehmen Bezug auf historische und transnationale Protest- und Erinnerungsformen sowie in diesem Kontext angestoßene Debatten und diskutierte Begriffe wie Femi(ni)zid-Suizid oder Transizid. Somit werden Möglichkeiten eines kollektiven, solidarischen Kampfes gegen patriarchale Gewalt  nicht trotz, sondern aufbauend auf unterschiedlichen Erfahrungen  ausgelotet.


http://www.conne-island.de/termin/nr6467.html