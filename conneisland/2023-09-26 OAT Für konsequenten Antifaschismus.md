---
id: "6649"
title: "OAT: Für konsequenten Antifaschismus"
start: 2023-09-26 19:00
end: 2023-09-26 23:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6649.html
isCrawled: true
---
OAT: Für konsequenten Antifaschismus

Nach einem allgemeinen Überblick wollen wir darüber ins Gespräch kommen, was in den vergangenen Jahren unter antifaschistische Intervention, konsequenten Antifaschismus, Feuerwehrpolitik und aktuell nach der antifaschistischen Demonstration in Zschocher in Leipzig diskutiert wurde und wird. Dabei scheint es hauptsächlich in der aktuellen Diskussion um das Auftreten und die Außenwirkung von antifaschistischen Demonstrationen zu gehen, doch ist das überhaupt eine Auseinandersetzung die zu führen lohnt oder geht es eigentlich um etwas anderes und viel mehr?

Was ist das Offene Antifa Treffen?

Es ist wichtig, sich zu vernetzen und Strukturen aufzubauen, um den Widerstand zu organisieren  nicht nur gegen Rassismus und Faschismus, sondern auch gegen Nationalismus, Sexismus, Antisemitismus, Homosexuellenfeindlichkeit und die kapitalistische Gesamtscheiße.

Du willst endlich was dagegen tun, wusstest bisher aber nicht wo, wie und mit wem? Dann komm zum Offenen Antifa Treffen (OAT).

Nächstes OAT am 26. September 2023 um 19 Uhr im Conne Island im Vorderhaus (Koburger Str. 3, 04277 Leipzig)   Wenn ihr krank seid, bleibt bitte zu Hause.

http://www.conne-island.de/termin/nr6649.html