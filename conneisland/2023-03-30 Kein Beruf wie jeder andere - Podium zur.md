---
id: "6485"
title: '"Kein Beruf wie jeder andere" - Podium zur Prostitutionskritik'
start: 2023-03-30 18:00
end: 2023-03-30 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6485.html
isCrawled: true
---
"Kein Beruf wie jeder andere" - Podium zur Prostitutionskritik

Wir müssen reden! Und zwar über Prostitution und ihre verheerenden Folgen. Das Narrativ der befreiten, empowering, feministischen Sexwork ist realitätsfern und dient eher den Freiern und Zuhältern als den betroffenen Frauen.

Wir wollen darüber sprechen, wie es eine Gesellschaft prägt, dass Frauenkörper käuflich sind und was Deutschlands liberale Gesetzgebung mit Zwangsprostitution und dem Bordell Europas zu tun hat.

Wenn Frauen zum Objekt degradiert werden und Sex gekauft wird, gilt es gerade als Linke, genauer hinzuschauen  wie feministisch ist das? Wollen und können wir das ertragen? Was ist zu tun?

Warum ist Prostitution eben kein Beruf wie jeder andere?

Wir laden ein zum Podiumsgespräch am 30.03. im Conne Island. Mit dabei sind eine Aussteigerin aus der Prostitution, eine Sozialarbeiterin von Karo e.V. und Sozialwissenschaftlerin und Soziologin Manuela Schon.

Konzept und inhaltliche Gestaltung: 
Alina Unverzagt, Jugendbotschafterin für TERRE DES FEMMES e.V.

http://www.conne-island.de/termin/nr6485.html