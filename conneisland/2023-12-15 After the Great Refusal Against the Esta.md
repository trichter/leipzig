---
id: "6684"
title: "After the Great Refusal: Against the Established Taste. Vortrag und
  Diskussion mit Mikkel Bolt Rasmussen (Englisch)"
start: 2023-12-15 19:00
end: 2023-12-15 23:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6684.html
isCrawled: true
---
After the Great Refusal: Against the Established Taste. Vortrag und Diskussion mit Mikkel Bolt Rasmussen (Englisch)

In seinem Band After the Great Refusal schreibt Mikkel Bolt Rasmussen über Dada, den Surrealismus und die Situationisten; über die Kunstavantgarde, ihre vielen Tode und das Verhältnis der Gegenwartskunst zu ihr - über die Politisierung der Kunst und die Ästhetisierung der Politik in Form von Faschismus und Kulturindustrie. Bei uns wird er das Buch vorstellen und seine Thesen zur Diskussion stellen. Thesen über den Doppelcharakter der Kunst heute, den Versuch radikaler Kunst in Form eines langen Marsches durch die Institutionen zu wirken und der Imagination der Zukunft aus der Vergangenheit innerhalb der Avantgarde heute.

Mikkel Bolt Rasmussen ist Kunsthistoriker und unterrichtet Cultural Studies an der Universität in Kopenhagen. Er veröffentlicht Artikel und Bücher zur Avantgarde, moderner politischer Philosophie und revolutionärer Tradition.

Die Veranstaltung findet im Infoladen statt.

http://www.conne-island.de/termin/nr6684.html