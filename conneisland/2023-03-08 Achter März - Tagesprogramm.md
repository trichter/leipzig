---
id: "6493"
title: Achter März - Tagesprogramm
start: 2023-03-08 09:30
end: 2023-03-08 13:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6493.html
isCrawled: true
---
Achter März - Tagesprogramm

Ab 9:30 Uhr - 14 Uhr Kinderbetreuung im Conne Island / kostenlos
Anmeldung unter: anmeldungworkshop@conne-island.de 

In Zusammenarbeit mit der Sportetage- Süd bieten wir Schnupper- Sportkurse für Frauen an:
10:00 Uhr -11:30 Uhr Kickboxen
12.30Uhr - 13.30 Uhr Mobility Flow
In der SportetageSüd, Simildenstraße 20, Leipzig
Teilnahme ist kostenlos
Anmeldung unter: 
anmeldungworkshop@conne-island.de

Ab 16 Uhr Workshop in Zusammenarbeit mit der MONALiesA:
"Einführung in die feministische Ideologiekritik" im Conne Island - Teilnahme kostenlos

17 Uhr - 21 Uhr Küfa im Conne Island

Ab 20 Uhr: Konzert mit Shitney Beers + Tigeryouth im Conne Island 
VVK: 12 AK: 15  

http://www.conne-island.de/termin/nr6493.html