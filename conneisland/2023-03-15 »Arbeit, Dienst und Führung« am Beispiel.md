---
id: "6476"
title: "»Arbeit, Dienst und Führung« am Beispiel des Leipziger Rüstungskonzerns
  HASAG. "
start: 2023-03-15 18:30
end: 2023-03-15 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6476.html
isCrawled: true
---
Buchvorstellung und Gespräch mit Nikolas Lelle und Martin Clemens Winter 

Die Deutschen und ihre Arbeit. Eine lange Geschichte eines überhöhenden Selbstbildes. Eine lange Geschichte des Antisemitismus, die der Nationalsozialismus noch einmal radikalisierte. Deutsch sollte eine Arbeit sein, die der Volksgemeinschaft diente. Unter Verweis auf »deutsche Arbeit« begründete der Nationalsozialismus nicht nur sein antisemitisches Selbstbild, sondern auch Praktiken der Verfolgung und Vernichtung. In »Arbeit, Dienst und Führung. Der Nationalsozialismus und sein Erbe« (erschienen im Verbrecher Verlag) rekonstruiert Nikolas Lelle diese Geschichte und analysiert dieses Selbstbild. Das Buch wird in der Abendveranstaltung vorgestellt und mit aktuellen Forschungen zu einem lokalen Beispiel verbunden: Die Leipziger Hugo Schneider AG (HASAG) war einer der größten Rüstungsproduzenten im nationalsozialistischen Deutschland und galt als »NS-Musterbetrieb«. Zugleich wurden in den Zwangsarbeits- und Konzentrationslagern der Firma zehntausende Menschen aus ganz Europa brutal ausgebeutet und nach »Arbeitsfähigkeit« selektiert. Tausende Jüdinnen und Juden starben in den Lagern der HASAG im von den Deutschen besetzten Polen.Lesung und Diskussion widmen sich diesem Zusammenhang von Inklusion und Exklusion durch »deutsche Arbeit« und der Verbindung von »Betriebsgemeinschaft«, Zwangsarbeit und Vernichtung im Nationalsozialismus.

Eine Veranstaltung der Gedenkstätte für Zwangsarbeit Leipzig in Kooperation mit der Universität Leipzig. Unterstützt von der Alfred Landecker Foundation.


Dr. Nikolas Lelle arbeitet seit 2020 bei der Amadeu Antonio Stiftung als Projektleiter der Bildungs- und Aktionswochen gegen Antisemitismus. Zuvor promovierte er  nach einem Studium der Philosophie und Soziologie in Frankfurt am Main und Mainz  an der Humboldt Universität zu Berlin in der Sozialphilosophie. 

Dr. Martin Clemens Winter ist wissenschaftlicher Mitarbeiter und Alfred Landecker Lecturer am Historischen Seminar der Universität Leipzig. Er forscht derzeit zu »Unternehmenskultur, Zwangsarbeit und Judenmord beim Leipziger Rüstungskonzern HASAG«.

http://www.conne-island.de/termin/nr6476.html