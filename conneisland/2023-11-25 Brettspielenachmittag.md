---
id: "6658"
title: Brettspielenachmittag
start: 2023-11-25 15:00
end: 2023-11-25 19:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6658.html
isCrawled: true
---
Brettspielenachmittag

Mensch soll ja den Tag nicht vor dem Abend loben, aber vielleicht vor dem Nachmittag? Der Spieleabend verwandelt sich in diesem Jahr zum Spielenachmittag!

Bringt gern eure Lieblingsspiele mit oder sucht euch was aus der großen Auswahl an Brett- und Kartenspielen raus.

Der Brettspielenachmittag findet immer am letzten Samstag im Monat statt. (außer Dezember)

http://www.conne-island.de/termin/nr6658.html