---
id: "6555"
title: "2cl Sommerkino: LIEBE, D-MARK UND TOD / A&#350;K, MARK VE ÖLÜM"
start: 2023-07-16 21:00
end: 2023-07-17 04:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6555.html
isCrawled: true
---
LIEBE, D-MARK UND TOD / AŞK, MARK VE ÖLÜM

BRD 2022, Cem Kaya, Dokumentarfilm, 96 Min., OmU
mit Ismet Topçu, Yüksel Ergin, Metin Türköz, Cavidan Ünal, Hatay Engin, Cem Karaca, Boe B., Dede Deli, Derya Yildirim

Anfang der 1960er Jahre wurden die sogenannten Gastarbeiter*innen aus Anatolien und anderen Gegenden der Türkei von der BRD angeworben. Von Anfang an gab es etwas, dass sie dabei immer begleitet hat: ihre Musik  ein Stück Heimat in der Fremde. Über die Jahre entwickelten sich eigenständige musikalische Richtungen, die es in dieser Form auch in der Türkei nicht gab.

Cem Kayas Film ist ein mitreißender Ritt durch Jahrzehnte einer Musikkultur in ihren bewegendsten und schillerndsten Ausprägungen, die die (post-)migrantische und die deutsche Popkultur nachhaltig beeinflussten: als Ausdruck von Sehnsucht und Protest, Lebensfreude und Selbstbehauptung. Ein Film, der uns in Archivaufnahmen und Interviews viele grandiose Musiker*innen entdecken lässt und sprichwörtlich bewegt. Bei Künstler*innen, wie der Nachtigall von Köln Yüksel Özkasap, Mayestero Asik Metin Türköz, dem Duo Derdiyoklar (Liebe Gabi), Cem Karaca und die Kanaken (Mein Freund, der Deutsche) und vielen anderen, kann kaum ein Kinositz still bleiben.


http://www.conne-island.de/termin/nr6555.html