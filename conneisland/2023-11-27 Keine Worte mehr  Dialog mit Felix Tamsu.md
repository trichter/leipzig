---
id: "6695"
title: "\"Keine Worte mehr\"  Dialog mit Felix Tamsut"
start: 2023-11-27 19:30
end: 2023-11-27 23:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6695.html
isCrawled: true
---
"Keine Worte mehr"  Dialog mit Felix Tamsut

Über den 7. Oktober 2023 und seine Folgen auf die israelische Gesellschaft und Fußball-Fanszenen

Am 7. Oktober 2023 überfiel die Hamas und ihre Unterstützer Israel. Mehr als 1400 Menschen wurden ermordet, über 4500 verwundet und mehr als 200 Menschen wurden als Geiseln in den Gaza-Streifen verschleppt. Es handelt sich um den schwersten Angriff auf jüdisches Leben seit 1945. Soweit die objektive Faktenlage.

Vor diesem Hintergrund haben wir den in Israel geborenen Journalisten Felix Tamsut eingeladen, um über die Auswirkungen des Tages auf die israelische Gesellschaft und die Fußball-Fanszenen in Israel, aber auch in Deutschland, zu sprechen. Wie hat er den Tag persönlich erlebt und wie gestaltet sich die öffentliche Wahrnehmung in der israelischen Presse? Welche Diskurse werden innerhalb der israelischen Gesellschaft geführt und findet eine Diskursverschiebung gerade im Umgang und der Sicht auf die Palästinenser*innen statt?

Da die Kernthemen der journalistischen Arbeit auf Fankultur und der Verbindung zwischen Politik und Fußball liegen, möchten wir mit Felix auch über die Wahrnehmung und den Umgang des Tages innerhalb der israelischen Fanszenen sprechen. Nach dem Angriff wurde der Spielbetrieb der israelischen Liegen zunächst eingestellt. Gab es dennoch wahrnehmbare Stellungnahmen relevanter Fangruppen? Und inwieweit wurden die Solidaritätsbekundungen deutscher Fankurven in Israel wahrgenommen?

Wir möchten den Vortrag nutzen, um Spenden für die Organisation ERAN (Emotional First Aid Services) in Israel zu sammeln. Israels größtem Sorgentelefon wurde die Subventionierung durch die eigene Regierung gestrichen, sodass sie unter der aktuellen Arbeitsbelastung auf jede Unterstützung angewiesen sind.

Eine Veranstaltung der Georg Schwarz Brigade und des Fanprojekts Leipzig.

http://www.conne-island.de/termin/nr6695.html