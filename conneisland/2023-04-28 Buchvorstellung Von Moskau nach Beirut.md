---
id: "6492"
title: 'Buchvorstellung: "Von Moskau nach Beirut"'
start: 2023-04-28 18:30
end: 2023-04-28 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6492.html
isCrawled: true
---
Buchvorstellung mit den Herausgeber:innen Miriam Mettler und Alex Carstiuc

Von Moskau nach Beirut stellt eine politische Intervention für Israel und gegen die modernen Formen des Antisemitismus dar.


Im Sommer 2022 jährte sich der Libanonkrieg zum 40. Mal: 1982 rief Israels Libanon-Offensive heftige Reaktionen in der westlichen Öffentlichkeit hervor, die damals noch nicht zum Standardrepertoire der Berichterstattung gehörten. In den Massenmedien wurde der jüdische Staat  von Léon Poliakov in dieser Schrift als Jude unter den Staaten bezeichnet  des Völkermords an der palästinensischen Bevölkerung bezichtigt und die Israel angekreideten Verbrechen mit denen der Nazis gleichgesetzt. Während in der arabischen Welt und den meisten sozialistischen Staaten diese Gleichsetzung bereits seit Israels Staatsgründung im Jahr 1948 an der Tagesordnung war, bedurfte es in der westlichen Welt, wie Léon Poliakov anhand eindrücklicher Beispiele und Quellen nachweist, einer längeren Entwicklung, um diese Form antisemitischer Desinformation für sich zu entdecken und zu popularisieren.

Poliakov war diese Neuerung Anlass für seinen 1983 auf Französisch publizierten Essay De Moscou à Beyrouth. Essai sur la désinformation, der nun zum ersten Mal in deutscher Sprache erscheint. Hier analysiert er die antisemitische Propaganda und die damit einhergehenden judenfeindlichen Exzesse, die sich im Zuge der israelischen Intervention im Libanonkrieg bahnbrachen. Um zu beantworten, wie es so weit kommen konnte, zeichnet er die Entwicklung des Antisemitismus im 20. Jahrhundert nach, insbesondere die Transformation, die dieser in der Sowjetunion erfuhr, und schildert die zentrale Rolle, die die stalinistische Propaganda hierbei spielte.

Er beschreibt die Radikalisierung des arabischen Antisemitismus durch die Protokolle der Weisen von Zion, die als Schlüsseldokument des modernen Antisemitismus betrachtet werden können. Diese Propagandaschrift leistete der Projektion einer jüdischen Weltverschwörung Vorschub und ermöglichte es so, die Juden zu den neuen Nazis zu erklären. Ein Wahn, der als wesentliche Ursache der vermeintlichen Unlösbarkeit des Konflikts zwischen der arabischen Welt und Israel betrachtet werden kann. Eine Tatsache, von der diejenigen, die Israel unter Verweis auf das Völkerrecht zur Mäßigung auffordern, bis heute geflissentlich absehen.

Insbesondere in Zentraleuropa bedurfte es des antiimperialistischen und antizionistischen Turns der 68er-Bewegung, um die einstigen Sympathien für den jungen jüdischen Staat in die Vorstellung vom berufspalästinensischen Unterdrückten als revolutionärem Subjekt zu verschieben. Zwei Wendepunkte sind für den Autor dabei zentral: 1967, als im Zuge des Sechstagekriegs das Bild des verfolgten Juden durch das des Siegers und Unterdrückers ersetzt wurde; und der Mai 1968, als ein Teil der Jugend, von den revolutionären Kämpfen der Dritten Welt berauscht, die PLO romantisierte und auf den gleichen Sockel hob wie den Vietcong. Poliakov widmet sich insbesondere den ideologischen Brüchen in den 1970er Jahren, den sich wandelnden Formen des Antisemitismus in der arabischen Welt und der politischen Linken. Er zeigt die Macht der sowjetischen und arabischen Propaganda auf, die weltweit auf vielfältige Weise verbreitet wurde, um Israel international zu kompromittieren und es  wie Poliakov konstatiert  zum »Juden der Nationen« zu machen.

Von Moskau nach Beirut stellt eine politische Intervention für Israel und gegen die modernen Formen des Antisemitismus dar. Der Essay kann gleichwohl als Fortsetzung von Poliakovs Schrift Vom Antizionismus zum Antisemitismus (1967, Calmann-Lévy; 1992, ça ira) begriffen werden. Hatte er dort bereits unmittelbar nach dem Sechstagekrieg den Antisemitismus im Gewand des Antizionismus erkannt, so weist Poliakov in dieser Schrift nach, dass im Sommer 1982 die antiisraelische Propaganda zu einer Aufhebung aller Schranken und Tabus führte, die den Antisemitismus seit der Shoah noch irgendwie eingehegt hatten.

Vita

Léon Poliakov, 1910 in St. Petersburg geboren und 1997 in Orsay gestorben, floh mit seiner Familie nach der Oktoberrevolution 1917 über Berlin nach Paris. Während der deutschen Besatzung Frankreichs überlebte er die Shoah im Untergrund und war in der Résistance an Judenrettungen beteiligt. Als autodidaktischer Historiker publizierte er 1951 mit »Vom Hass zum Genozid« die erste analytische Studie über die Shoah, die erst 2021 auf Deutsch (Edition Tiamat) veröffentlicht wurde, während sie in Frankreich, ebenso wie seine achtbändige »Geschichte des Antisemitismus« zum Standardwerk avancierte. Weitere Schriften: »Der arische Mythos«, »Das Dritte Reich und die Juden« (gem. mit Joseph Wulff), »Das Dritte Reich und seine Diener«, »Das Dritte Reich und seine Denker«, »Vom Antizionismus zum Antisemitismus«, »St. Petersburg  Berlin  Paris. Memoiren eines Davongekommenen«.

Vortragende

Miriam Mettler und Alex Carstiuc (Berlin), Herausgeberin und Herausgeber der Schrift Von Moskau nach Beirut von Léon Poliakov (Dezember 2022, 224 Seiten, ca ira-Veralg)



http://www.conne-island.de/termin/nr6492.html