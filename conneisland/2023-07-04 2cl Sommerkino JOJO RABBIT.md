---
id: "6558"
title: "2cl Sommerkino: JOJO RABBIT"
start: 2023-07-04 21:00
end: 2023-07-05 04:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6558.html
isCrawled: true
---
JOJO RABBIT

USA 2019, Taika Waititi, 108 Min., OmU
mit Roman Griffin Davis, Taika Waititi, Scarlett Johansson, Sam Rockwell

Deutschland während des Zweiten Weltkrieges: Der kleine Jojo Betzler ist ein überzeugter Nazijunge. Gerade erst hat er im Ferienlager der Hitlerjungend gelernt, wie man Granaten richtig wirft und Bücher verbrennt. Sein doofer imaginärer Freund Adolf steht ihm bei etwaigen Zweifeln mit Rat zur Seite. Aber als Jojo herausfindet, dass seine liebevolle alleinerziehende Mutter Rosie ein jüdisches Mädchen versteckt, geraten seine Überzeugungen ins Wanken.

Regisseur Taika Waititi, Sohn eines Maori, ließ es sich nicht nehmen, selbst Hitler in seinem Film zu spielen -- weil den das ziemlich sicher zur Weißglut getrieben hätte. Mit seinem aus Werken wie 5 ZIMMER KÜCHE SARG und den letzten THOR-Filmen bekannten Humor und knallbunten Farben hinterfragt er die klassische filmische Aufarbeitung des Dritten Reichs und adaptiert das Thema für ein heutiges Publikum neu. Seine starbesetzte Komödie rüttelt an den Grenzen des guten Geschmacks und rechnet dabei mit Ignoranz, Verblendung, und Mitläufertum ab.


http://www.conne-island.de/termin/nr6558.html