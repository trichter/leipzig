---
id: "6713"
title: OAT "Autos als Waffe"
start: 2023-12-19 19:00
end: 2023-12-19 23:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6713.html
isCrawled: true
---
OAT Autos als Waffe  Tatort Henstedt-Ulzburg

Am 17. Oktober 2020 machte der rechte Täter Melvin S. am Rande einer Parteiveranstaltung der Alternative für Deutschland (AfD) im Bürgerhaus Henstedt-Ulzburg mit seinem Pick-Up-Wagen gezielt Jagd auf antifaschistische Gegendemonstrant*innen. Er nahm dabei den Tod von Menschen billigend in Kauf und verletzte vier Personen zum Teil schwer.

Am 2. Juli 2023 begann vor dem Landgericht Kiel der Prozess gegen den Täter. Wahrscheinlich fällt am 21.12.2023 das Urteil, wir wollen uns darüber austauschen was bisher vom Prozess bekannt, sowie über Autos als Waffe von Rechten sprechen. Denn auch in Leipzig kam es zu einer Tat, die wohl ohne juristische Folgen für den Täter bleiben wird.

Was ist das Offene Antifa Treffen?

Es ist wichtig, sich zu vernetzen und Strukturen aufzubauen, um den Widerstand zu organisieren  nicht nur gegen Rassismus und Faschismus, sondern auch gegen Nationalismus, Sexismus, Antisemitismus, Homosexuellenfeindlichkeit und die kapitalistische Gesamtscheiße. 

Du willst endlich was dagegen tun, wusstest bisher aber nicht wo, wie und mit wem? Dann komm zum Offenen Antifa Treffen (OAT).

Nächstes OAT am 19. Dezember 2023 um 19 Uhr im Conne Island im Vorderhaus (Koburger Str. 3, 04277 Leipzig)   Wenn ihr krank seid,
bleibt bitte zu Hause.


http://www.conne-island.de/termin/nr6713.html