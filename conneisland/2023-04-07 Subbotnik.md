---
id: "6515"
title: Subbotnik
start: 2023-04-07 11:00
end: 2023-04-07 15:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6515.html
isCrawled: true
---
Subbotnik

Hey liebe Island Sympatisant:innen,

Die Sommersaison steht vor der Tür und der Freisitz wartet auf hungrige und durstige Mäuler. Damit die Saison einwandfrei starten kann planen wir am 07.04. 11:00 Uhr (diesen Freitag) nochmal einen Subbotnik. Wir haben beim Letzten Einsatz dank der tatkräftigen Unterstützung von euch viel geschafft, dennoch gibt es noch einige dreckige und unordentliche Stellen zu beseitigen. Es wird wieder ein ausgiebiges Frühstück und Mittagessen für euch geben damit wir gut gestärkt die letzen Aufgaben angehen können. 

Bis Freitag fällt uns bestimmt noch die eine oder andere schöne Aufgabe ein. 

Wir sehen uns Freitag in aller Frische und bei pumpender Mucke.

Eure Island  Hausmeister Gang / Facility Management


Subbotnik

http://www.conne-island.de/termin/nr6515.html