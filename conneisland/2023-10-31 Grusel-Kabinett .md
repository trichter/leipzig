---
id: "6675"
title: "Grusel-Kabinett "
start: 2023-10-31 19:00
end: 2023-10-31 23:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6675.html
isCrawled: true
---
Grusel-Kabinett  Antifaschist*innen berichten: Wie (über) leben unter einer faschistischen Regierung?

Es ist schaurig: Jeden Tag eine Umfrage darüber wie viele Menschen sich in Kaltland eine Regierung mit Faschisten und Neonazis wünschen. Mit den Landtagswahlen im nächsten Jahr in Sachsen, Thüringen und Brandenburg gibt es die reale Möglichkeit, dass bereits im nächsten Jahr diese gruselige Vorstellung Realität werden kann  Gerade vor dem Hintergrund der Ergebnisse der Wahlen in Hessen und Bayern in diesem Monat und den Ergebnissen der parlamentarischen Rechten und der Faschisten. Um sich auf diese mögliche Horrornacht am 1. September 2024 vorzubereiten, wurden Menschen für ein Podium eingeladen, die darüber berichten können wie das Leben unter faschistischen Regierungen sich verändert hat und wie Antifaschist*innen darauf reagiert haben. Im Anschluss nach dem Podium wollen wir euch jedoch nicht geschockt und entmutigt nach Hause gehen lassen, sondern noch etwas zusammen bleiben, bei Musik und Getränken weiter sprechen, vielleicht etwas tanzen und einen schönen Abend zusammen verbringen. Verkleidet euch also gerne, es ist Halloween!

Die Halloween-Veranstaltung wird unterstützt und gestaltet von:

Rassismus tötet!  Leipzig, Offenes Antifa Treffen,
Ladenschlussbündnis Leipzig und Endfame (Engagierte Demokrat*innen für
die Amerikanisierung Europas)


http://www.conne-island.de/termin/nr6675.html