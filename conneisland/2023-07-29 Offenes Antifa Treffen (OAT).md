---
id: "6608"
title: Offenes Antifa Treffen (OAT)
start: 2023-07-29 18:00
end: 2023-07-29 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6608.html
isCrawled: true
---
Offenes Antifa Treffen (OAT)

Vortrag zur Prozessbegleitung zum Femizid an Besma A. in Göttingen 2021-23

Fast 2 Jahre lang wurde am Landgericht Göttingen der Femizid an Besma A. verhandelt. Besma A. wurde im April 2020 von ihrem Ehemann schlafend im Wohnzimmer erschossen. Diese zwei Jahre lang hat die AG Prozessbegleitung jeden Prozesstag beobachtet, das Prozessgeschehen analysiert und kritisiert und immer wieder Gerechtigkeit für Besma gefordert. Nun ist der Täter für den Mord an Besma A. verurteilt worden.

Neben viel Wut über das Prozessgeschehen bleibt die Frage  wurde für Besma A. Gerechtigkeit geschaffen? Die AG Prozessbegleitung in ihrem Vortrag über ihre Beobachtungen, Erfahrungen und Erkenntnisse von den letzten Monaten des Prozesses. Sie zieht aber auch ein Fazit über den Prozess und ihre eigene Arbeit als Prozessbegleitung. Es referiert die AG Prozessbegleitung zum Femizid an Besma A.

Was ist das Offene Antifa Treffen?

Es ist wichtig, sich zu vernetzen und Strukturen aufzubauen, um den Widerstand zu organisieren  nicht nur gegen Rassismus und Faschismus, sondern auch gegen Nationalismus, Sexismus, Antisemitismus, Homosexuellenfeindlichkeit und die kapitalistische Gesamtscheiße.

Du willst endlich was dagegen tun, wusstest bisher aber nicht wo, wie und mit wem? Dann komm zum Offenen Antifa Treffen (OAT).


http://www.conne-island.de/termin/nr6608.html