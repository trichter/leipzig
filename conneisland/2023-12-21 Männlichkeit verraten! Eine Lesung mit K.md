---
id: "6701"
title: "VERSCHOBEN AUF TBA: Männlichkeit verraten! Eine Lesung mit Kim Posster"
start: 2023-12-21 19:00
end: 2023-12-21 23:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6701.html
isCrawled: true
---
VERSCHOBEN AUF TBA: Männlichkeit verraten! Eine Lesung mit Kim Posster

----Aufgrund des gesundheitlichen Zustands von Kim müssen wir den Vortrag leider auf einen unbekannten Zeitpunkt verschieben----

Wie lässt sich die Männerfrage emanzipatorisch beantworten?
Das Verhältnis von Männlichkeit, Männern und Feminismus wird so intensiv diskutiert, wie seit fast 20 Jahren nicht mehr. Konzepte wie Profeminismus und die Geschichte der antisexistischen Männerbewegung werden wiederentdeckt und sollen neu belebt werden. Besonders das Schlagwort kritische Männlichkeit versammelt neue Ansätze und Gruppen, die das Verhältnis von Männlichkeit und feministischer Kritik bestimmen und praktisch angehen wollen. Was eigentlich Grund zur Hoffnung geben sollte, stellt sich bei genauerem Hinsehen aber oft als bloße Fortsetzung der Katastrophe heraus.
Denn die neu entflammte Debatte und Praxis zur Kritik an Männlichkeit wird von popfeministischer Lebensberatung, dem innerlichen Moralismus des Privilegiencheckens und dem verzweifelten Versuch dominiert, cis Männern feministische Kritik irgendwie schmackhaft zu machen. Vor allem das, was unter dem Label kritische Männlichkeit geschieht, ist nicht viel mehr als ein hoch individualisiertes Programm zur Resouveränisierung verunsicherter (cis) Männer. Feministische Kritik wird sich dafür im schlechtestmöglichen Sinne einverleibt, damit ihre Konsequenzen weiter ausgesessen und unterlaufen werden können  nur diesmal mit dem korrekten Vokabular und einer profeministischen Pseudo-Praxis.
Männlichkeit verraten! bricht mit allen Versuchen der einhegenden Versöhnlichkeit und geht in die Konfrontation. Der provokante Essay ist das Ergebnis von über fünf Jahren Frust, Enttäuschung und analytischer Wut über (eigene) Männlichkeit; darüber, wie sie in der Linken herrscht und wie gerade der neue Profeminismus auf sie eingeht. Er verbindet dafür Beobachtung und Polemik, Analyse und Intervention, Theorie und Praxis  in der Hoffnung auf eine organisierte Männlichkeitskritik, die Männlichkeit weder erkunden noch stärken will. Stattdessen soll sie organisiert und institutionalisiert zum konkreten Problem gemacht werden, zu dem die real existierenden Männer ein bewusstes und politisches Verhältnis einnehmen müssen.

http://www.conne-island.de/termin/nr6701.html