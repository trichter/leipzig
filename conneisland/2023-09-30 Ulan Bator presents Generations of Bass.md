---
id: "6629"
title: Ulan Bator presents Generations of Bass
start: 2023-09-30 21:00
end: 2023-10-01 04:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6629.html
isCrawled: true
---
Ulan Bator presents Generations of Bass

^foxic^ (Tanzen mit Pflanzen, Push Gang)
Cuepric (Bass Focus) 
Relict (Flexout, diascope) 
Base, Derrick, Reckless & Tabeat (Ulan Bator)

more tba

powered by Bassculture Audio Soundsystem


Ulan Bator nimmt euch mit durch eine Nacht voller Jungle and Drum'n'Bass Breaks und Bässen. Angelegt die Galaxie in ihrer ganzen Weite zu erkunden, seien es smooth'e Breaks im 140er Tempo zu Beginn hin zu 160er, Liquid, frischen Jungle und Drum'n'Bass Sounds spiegelt sich dieses auch in der Bandbreite der DJs wieder, ein Mix aus diversen Leipziger Bass Generationen. Angetrieben durch das Bassculture Audio Soundsystem steht alles bereit für eine rasante Fahrt auf tieffrequenten Wellen!



http://www.conne-island.de/termin/nr6629.html