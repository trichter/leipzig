---
id: "6549"
title: "2cl Sommerkino: COCAINE BEAR"
start: 2023-07-12 21:00
end: 2023-07-13 04:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6549.html
isCrawled: true
---
COCAINE BEAR

USA 2023, Elizabeth Banks, 95 Min., OmU
mit Keri Russell, Ray Liotta, Alden Ehrenreich, Margo Martindale, Jesse Tyler Ferguson

Basierend auf einer wirklich absolut wahren Geschichte erzählt COCAINE BEAR von einem Schwarzbär, der eine in den Wäldern verloren gegangene große Menge Kokain findet und den Trip seines Lebens hat -- leider mit blutigen Folgen für alle, die ihm zu nahe kommen!

Als schlechter Film im besten Sinne, ist COCAINE BEAR trashiger Spaß für alle, die ihr Hirn mal abschalten wollen, ohne es gleich einem Bär zum Fraß vorzuwerfen. Mit 80's Musik und Ästhetik, jeder Menge Kunstblut und einer Gruppe engagierter Schauspieler*innen, deren Gesichter man alle irgendwie kennt, nimmt sich die Splatter-Komödie selbst nie zu ernst. Die Regisseurin von PITCH PERFECT 2 und der Drehbuchautor von THE BABYSITTER: KILLER QUEEN sorgen dafür. Der echte Bär ist übrigens gleich nach seiner Kokserei einfach gestorben, ohne wild durch den Wald zu toben. Aber die wahre Version der Geschichte ist heute Schnee von gestern.

http://www.conne-island.de/termin/nr6549.html