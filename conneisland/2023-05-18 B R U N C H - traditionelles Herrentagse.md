---
id: "6567"
title: "B R U N C H - traditionelles Herrentagsevent "
start: 2023-05-18 14:00
end: 2023-05-18 18:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6567.html
isCrawled: true
---
Benefiz für feministische Initiativen!

Torten und Kuchen
Piccolo und Kaffee
Musik und Chillen

Traditionelles Herrentagsevent von AFBL und fantifa

Tradition ohne Männertagsgedöns, fun for everyone und tender to all gender!

Come and join the fun!

http://www.conne-island.de/termin/nr6567.html