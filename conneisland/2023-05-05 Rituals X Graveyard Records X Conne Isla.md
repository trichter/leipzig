---
id: "6507"
title: "Rituals X Graveyard Records X Conne Island "
start: 2023-05-05 20:00
end: 2023-05-06 00:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6507.html
isCrawled: true
---
Rituals X Graveyard Records X Conne Island 

Die Rituals x Graveyard Records Crew freut sich über eine sweete Colab mit dem Conne Island.
An diesem Abend haben wir Alehlokapi & Band, David Novell und nspktklr eingeladen uns mit Zeit und Attitüde Morsezeichen in unsere Herzen zu senden.Wenn der Beat angeht, wissen wir, dass wir richtig sind. Für alle Artists gilt: Sie folgen ihremeigenen Flow, ihre Texte sind punchlines from the heart, kritisch-witty, radikal-romantisch und sie verschenken das darin versteckte Glück an uns.Hier verschmilzt feinster Neo Soul mit Pop, On-Point-Rap küsst bassy Punchlines & wavy Synthpop umarmt Galore Vocals.Daher sehr verehrtes Publikum, liebe Goal Getter, Showrunner, Romancer & Girlbosses, besuchen Sie eine Show voller Magie, eine Nacht mit Slo-Mo gegen FOMO, wo wir alle God*esses* sind.
Line-up
Alehlokapi & Band (Leipzig, live)
David Novell (Leipzig, live)
nspktklr (Leipzig, live)


////////English:
RITUALS x Graveyard Records crew is happy to announce a sweet colab with beloved Conne Island.
This evening we invited Alehlokapi & Band, David Novell and nspktklr & they will send Morse codes into our hearts with time and attitude.When the beat comes, you know you are in the right place. All artists follow their own flow, their lyrics are punchlines from the heart, critical-witty, radical-romantic and they give away the happiness hidden in them to us.Finest neo-soul fuses with pop, on-point rap kisses bassy punchlines & wavy synthpop embraces galore vocals.Therefore dear audience, dear Goal Getters, Showrunners, Romancers & Girlbosses, attend ashow full of magic, a night of Slo-Mo vs FOMO, where we are all God*esses*.

Line-up
Alehlokapi & Band (Leipzig, live)
David Novell (Leipzig, live)
nspktklr (Leipzig, live)


http://www.conne-island.de/termin/nr6507.html