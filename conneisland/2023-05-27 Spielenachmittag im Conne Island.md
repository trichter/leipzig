---
id: "6534"
title: Spielenachmittag im Conne Island
start: 2023-05-27 15:00
end: 2023-05-27 19:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6534.html
isCrawled: true
---
Brettspielenachmittag

Mensch soll ja den Tag nicht vor dem Abend loben, aber vielleicht vor dem Nachmittag?
Der Spieleabend verwandelt sich in diesem Jahr zum Spielenachmittag!

Bringt gern eure Lieblingsspiele mit oder sucht euch was aus der großen Auswahl an Brett- und Kartenspielen raus.

(findet im Café statt, ihr könnt euch bei gutem Wetter aber auch auf den Freisitz setzen)

http://www.conne-island.de/termin/nr6534.html