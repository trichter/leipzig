---
id: "6505"
title: Offenes Antifa Treffen (OAT)
start: 2023-03-21 18:30
end: 2023-03-21 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6505.html
isCrawled: true
---
OAT im März: Antifaschismus in der Defensive?

Neonazis sind in Sachsen dauerhaft auf den Straßen aktiv, sei es bei den noch klassischen Aufmärschen am Montag oder jetzt wieder vermehrt gegen Unterkünfte für Geflüchtete. Zusätzlich gibt es eine starke Repression gegen antifaschistische Strukturen und wahrscheinlich wird auch das Antifa Ost  Verfahren in Sachsen im März oder Anfang April enden.

Antifaschismus in der Defensive? Alles scheiße? Schauen wir uns das doch genauer an und tauschen uns darüber aus. Zudem könnt ihr die neue Broschüre Fünfundfünzigtausend Schuss.* von der Kampagne Entnazifizierung jetzt abgreifen, die in diesem Monat erschienen ist und veröffentlicht wurde.

Was ist das Offene Antifa Treffen?

Es ist wichtig, sich zu vernetzen und Strukturen aufzubauen, um den Widerstand zu organisieren  nicht nur gegen Rassismus und Faschismus, sondern auch gegen Nationalismus, Sexismus, Antisemitismus, Homosexuellenfeindlichkeit und die kapitalistische Gesamtscheiße.

Du willst endlich was dagegen tun, wusstest bisher aber nicht wo, wie und mit wem? Dann komm zum Offenen Antifa Treffen (OAT). Wir treffen uns im Vorderhaus des Conne Island.

http://www.conne-island.de/termin/nr6505.html