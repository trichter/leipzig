---
id: "6553"
title: "2cl Sommerkino: VICTORIA"
start: 2023-07-25 21:00
end: 2023-07-26 04:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6553.html
isCrawled: true
---
VICTORIA

IN KOOPERATION MIT DER CINÉMATHÈQUE
BRD 2014, Sebastian Schipper, 136 Min., OmeU
mit Laia Costa, Frederick Lau, Franz Rogowski, Burak Yigit, Max Mauff

Eine Stunde noch, dann neigt sich auch diese Nacht in Berlin wieder dem Ende zu. Vor einem Club lernt Victoria aus Madrid die vier Berliner Sonne, Boxer, Blinker und Fuß kennen. Der Funke zwischen ihr und Sonne springt sofort über, aber Zeit füreinander haben die beiden nicht. Sonne und seine Kumpels haben noch etwas vor. Um eine Schuld zu begleichen, haben sie sich auf eine krumme Sache eingelassen und Victoria soll als Fahrerin einspringen. Was für sie wie ein großes Abenteuer beginnt, entwickelt sich erst zu einem euphorischen Tanz und dann schnell zum Albtraum. Während der Tag langsam anbricht, geht es für Victoria und Sonne auf einmal um Alles oder Nichts.

Gedreht in einer einzigen Einstellung, ohne Schnitt und Pause folgt die Kamera den Protagonist*innen von VICTORIA vom Club im Keller bis auf Hochhausdächer, durch eine treibende Nacht bis ins Morgengrauen. Nicht nur deshalb wurde Sebastian Schippers Berlinfilm zum sagenumwobenen Kult. Dreimal wurde alles komplett durchgespielt, zweimal war der Regisseur nicht zufrieden. Wäre der dritte Versuch wieder nichts geworden, wäre der Film angeblich nie veröffentlicht worden. Stattdessen wurde VICTORIA zu einem riesigen Kinoerfolg, der bis heute nichts von seiner Sogwirkung verloren hat.


http://www.conne-island.de/termin/nr6553.html