---
id: "6580"
title: 30th Little Sista Skate Cup
start: 2023-08-27 10:00
end: 2023-08-27 14:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6580.html
isCrawled: true
---
30th Little Sista Skate Cup

30th Little Sista Bowlcup dreißigster Little Sista Skateboard Contest Das ließt sich so einfach runter. Dahinter steckt jedoch einiges mehr als nur der älteste, jährlich durchweg stattfindende Skatecontest der Welt. Mittlerweile sind einige Sieger:innen immerhin wesentlich jünger als der Contest selbst aber das wird wohl bei Prestige-trächtigen Contest immer der Fall sein ab einem gewissen Zeitpunkt. Viel mehr schade ist aber, dass der Hintergrund dieses Contest gar nicht mehr so bekannt ist bei Skateboarder:innen unter 30 Jahren.
Little Sista war nämlich der Skateboardshop von Torsten Goofy Schubert im Conne Island; dem Platz, an dem der Contest seit jeher als Austragungsort dient. Man könnte sagen, dass Conne Island, der Little Sista Skateshop und der Skatecontest sind quasi das Epizentrum Ostdeutschen Skateboardings.

Und was hatten wir im Osten?
Genau!
Nüscht!
Un was hammor draus jemacht?!
Das kann noch nicht abschließend beantwortet werden, denn am Ende simmer ja noch nüsch! Wir haben ja nun am 26. und 27.08.2023 das Beste noch vor uns! Nämlich den runden Geburtstag!

Und da geht´s ab. Alle, die schon mal im Conne Island oder speziell beim Little Sista Bowl Cup (LSBC) waren (Bowlcup übrigens erst seit dem 20igsten LSBC, als der Bowl damals neu eingeweiht wurde) wissen, das Conne Island hat einen anderen Flair Es ist nicht nur ein Skatepark, sondern ein Ort, der zum Verweilen einlädt! Skaten, Getränke, Snacks, Kultur, Schatten, eine gute DIY-Skateboarder:innen-Szene Alles mit einer gewissen Hingabe und Liebe erstellt. Der Contest nie dafür gemacht, heraus zu finden, wer der beste Skateboarder bzw. die beste Skateboarderin ist, sondern dafür gedacht, Skateboarding zu zelebrieren und eine gemeinsame gute Zeit zu haben.
Diesen Geist, den Goofy, alias Torsten Schubert, schon 1991 ins Conne Island gebracht hat, pflegen die Organisator:innen gern weiterzugeben und am Leben zu erhalten.
JEDOCH lassen wir uns dieses Jahr nicht lumpen, ein großartiges Event zu veranstalten und aufzudrehen! Heck Yeah!
Deshalb gibt es dieses Jahr nicht nur insgesamt 2.500 Preisgeld, viele Sachpreise, einen Jam mit Tricks for Ca$h sondern auch an beiden Tagen die vermutliche beste Contestmoderation EU-West  und insgesamt 6 Bands an beiden Tagen. Ach ja! Alle deine Freunde werden auch da sein und sich fragen wo du steckst!
Also kommt in Scharren am 26./27.08. ins Conne Island in Leipzig. Wir freuen uns darauf, mit dir ein fettes Wochenende verbringen zu können und die Idee eines Familientreffens von Skateboarder:innen von woher-auch-immer weiterführen zu dürfen!

http://www.conne-island.de/termin/nr6580.html