---
id: "6544"
title: "2cl Sommerkino: TAXI ZUM KLO"
start: 2023-06-08 20:30
end: 2023-06-09 00:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6544.html
isCrawled: true
---
Eröffnung des 2cl Sommerkino mit Einführung

BRD 1980, Frank Ripploh, 95 Min., OmeU mit Frank Ripploh, Bernd Broaderup, Gitta Lederer

Ich mag Männer, bin 30 Jahre alt, von Beruf Lehrer, sagt Frank, der abends auch schon mal als Peggy ausgeht. Im Kino lernt er Bernd kennen und nimmt ihn mit nach Hause. Von da an sind die beiden ein Paar. Frank genießt das Zusammensein mit Bernd, aber er will auch weiter seine Freiheiten jenseits des trauten Heims: Treffen mit anderen Männern, anonymen Sex in Parks und auf öffentlichen Toiletten, Abenteuer und Exzess. Bernd hingegen wünscht sich eine monogame Partnerschaft und ein ruhiges Leben auf dem Bauernhof.

Bei seiner Erstveröffentlichung im Jahr 1980 löste TAXI ZUM KLO in den Kinos einen Skandal aus. Und manch explizite Szene zwischen glory hole und Proktologie könnte auch heute noch für WTF-Momente im Publikum sorgen. Dennoch gilt der schamlos schwule Liebesfilm von Frank Ripploh, dessen offener Umgang mit der eigenen Sexualität ihn seine Stelle als Lehrer kostete, heute als einer der großen Klassiker des queeren Kinos. Eine erotische, absurd komische Reise in die Schwulenszene der 80er, und ein ungemein witziges und berührendes, durch und durch unabhängiges Stück queerer Kinogeschichte.

Mit einer Einführung von Jan Künemund.

http://www.conne-island.de/termin/nr6544.html