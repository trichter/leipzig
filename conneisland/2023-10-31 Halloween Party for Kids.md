---
id: "6674"
title: Halloween Party for Kids
start: 2023-10-31 17:00
end: 2023-10-31 21:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6674.html
isCrawled: true
---
Halloween Party

Mit Kinderprogramm, Kostümwettbewerb (18 Uhr), Essen und Trinken und vielen Süßigkeiten.


http://www.conne-island.de/termin/nr6674.html