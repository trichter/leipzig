---
id: "6563"
title: "2cl Sommerkino: EVERYTHING EVERYWHERE ALL AT ONCE"
start: 2023-07-20 21:00
end: 2023-07-21 04:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6563.html
isCrawled: true
---
EVERYTHING EVERYWHERE ALL AT ONCE

IN KOOPERATION MIT DER CINÉMATHÈQUE
USA 2022, Daniel Scheinert, Daniel Kwan, 139 Min., OmU
mit Michelle Yeoh, Jamie Lee Curtis, Ke Huy Quan, Stephanie Hsu, Jenny Slate, James Hong

Waschsalonbesitzerin Evelyn Wang geht im Chaos ihres Alltags unter und auch der Gang zum Finanzamt ist unausweichlich. Doch während sie mit ihrer Familie bei der Steuerprüferin vorspricht, wird ihr Universum komplett durcheinandergewirbelt: Sie entdeckt, dass das Multiversum real ist und sie auf die Fähigkeiten und das Leben anderer Versionen ihrer selbst zugreifen kann.

Über den vielleicht größten Überraschungserfolg und Publikumsliebling der letzten Jahre muss nicht mehr viel gesagt werden. Ein Film, der voller Ideen strotzt und mit seiner unglaublichen Fülle an Einfällen fast schon zu überwältigen droht. Dass ausgerechnet dieser nerdige Film, der außerdem Asian und Asian American Schauspieler*innen in den Fokus rückt, zu einem derartigen Erfolg wird, hätten nur wenige geglaubt. Somit ist er auch ein Plädoyer für Chancen, Möglichkeiten und die Liebe zur Fantasie, die jeden noch so tristen Gang zum Finanzamt mit glubschäugigen Steinen, Wurstfingern und ganz besonderen Bageln erhellen kann.

http://www.conne-island.de/termin/nr6563.html