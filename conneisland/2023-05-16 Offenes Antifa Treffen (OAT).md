---
id: "6542"
title: Offenes Antifa Treffen (OAT)
start: 2023-05-16 18:30
end: 2023-05-16 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6542.html
isCrawled: true
---
Offenes Antifa Treffen (OAT)

Medien titelten bundesweit LKA kündigt Offensive gegen Linksextremisten an, Hintergrund war ein Interview mit dem LKA Sachsen in der Sächsischen Zeitung im April diesen Jahres, es drehte sich hauptsächlich um das Antifa-Ost Verfahren, welches vielleicht im Mai oder im Juni in Dresden zu Ende gehen soll. Die Opferberatung RAA führt in der eigenen Statistik 2923 rechte Angriffe im Zeitraum 2012  2022 mit 4274 Betroffenen dieser Gewalt. Im selben Zeitraum werden 585 Angriffe in der Stadt Leipzig aufgeführt. Nach Recherchen von ZEIT und  Tagesspiegel wurden in Sachsen seit 1990 mindestens 17 Menschen von Neonazis ermordet, hinzu kommen 8 Verdachtsfälle. In der Stadt Leipzig wurden 7 Menschen ermordet, hinzu kommen 3 Verdachtsfälle. Schlagzeilen wie LKA Sachsen kündigt Offensive gegen Rechtsextreme an, fanden sich bei Suchmaschinen nicht.

Wir haben Menschen von der Kampagne Wir sind alle LinX angefragt uns ihre Eindrücke zum Antifa-Ost-Verfahren zu schildern und wie sie die Offensive vom LKA und die Repression gegen Antifaschist*innen in Sachsen wahrnehmen.

Was ist das Offene Antifa Treffen?

Es ist wichtig, sich zu vernetzen und Strukturen aufzubauen, um den Widerstand zu organisieren  nicht nur gegen Rassismus und Faschismus, sondern auch gegen Nationalismus, Sexismus, Antisemitismus, Homosexuellenfeindlichkeit und die kapitalistische Gesamtscheiße.

Du willst endlich was dagegen tun, wusstest bisher aber nicht wo, wie und mit wem? Dann komm zum Offenen Antifa Treffen (OAT).


http://www.conne-island.de/termin/nr6542.html