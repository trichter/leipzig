---
id: "6446"
title: "Re*mapping Leipzig: Feministisches Vernetzungstreffen und App-Release"
start: 2023-03-19 14:00
end: 2023-03-19 18:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6446.html
isCrawled: true
---
Re*mapping Leipzig: Feministisches Vernetzungstreffen und App-Release

Am Sonntag, den 19. März 2023 laden wir dazu ein, den Launch der WebApp Eine neue Bewegung: Re*Mapping Leipzig im Conne Island mit uns zu feiern! Für das Fest bereitet das Projektteam ein ganztägiges Programm mit Sektempfang und Kaffee, Inhalten und Unterhaltung, mit Talks und Sounds vor. Zusammen mit Künstler*innen und Autor*innen wollen wir Euch unsere App vorstellen, uns austauschen und vernetzen, zusammen Spaß haben, trinken, essen und tanzen. 

Die entstandene App Eine neue Bewegung: Re*Mapping Leipzig ermöglicht Leipziger feministische Geschichte(n) in multimedialen Spaziergängen zu erkunden. Künstlerisch macht die App historische und gegenwärtige Kämpfe, Aktivitäten und Bewegungen im öffentlichen Raum digital sichtbar.

Special Guest der Veranstaltung ist das freie Performancekollektiv CHICKS*!

Eine neue Bewegung: Re*Mapping Leipzig: Paula Ábalos (Animation), Patrizia Bieri (Tanz), Anne Brüssau (Stimme), Lina Ehrentraut (Comic), Marlene Freter (Sound), Mandy Gehrt (Performance), Julia Gerke (Pop Up Struktur), Alexandra Ivanciu (Foto), Olivia Golde (Text), Anne Hofmann, Melina Weissenborn, Katharina Zimmerhackl (Aktion), Anja Kaiser (Intervention), Kantefilm (Video), Maria Kobylenko (Animation), Carolin Krahl (Text), Johanna Krümpelbeck (Vermittlung), Koschka Linkerhand (Text), Lauretta van der Merwe (Stimme), Theresa Münnich und Leni Pohl (Sound), Felix von Oppeln-Bronikowski (3D), Jasmina Rezig (Sound), Rebecca Maria Salentin (Sprecherin), Anna Schimkat (Sound), Ann C. Schomburg (Augmented Reality), Yvi Strüwing (Stimme), Connie Walker aka CFM (Sound), Elisa Überschär (Stimme), Angelika Waniek (Performance), die Anwesenheit und viele mehr!

Kuratorisches Team: Julia Kurz, Kati Liebert, Ewa Meister, Johanna Ralser, Kristina Semenova, Olga Vostretsova

Grafik & Programmierung: Studio Tabassomi, Christin Ursprung, Kristin Fritsch

Das Programm wird in Zusammenarbeit mit Conne Island und der Stiftung Weiterdenken organisiert.

Unsere Unterstützerinnen waren und sind: Stadt Leipzig, Kulturstiftung des Freistaates Sachsen, Programm Neustart Kultur des Kunstfonds

http://www.conne-island.de/termin/nr6446.html