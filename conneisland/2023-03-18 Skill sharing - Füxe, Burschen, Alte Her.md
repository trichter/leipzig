---
id: "6480"
title: "Skill sharing - \"Füxe, Burschen, Alte Herren  Burschenschaften und
  ihre Rolle in der Rechten\""
start: 2023-03-18 14:30
end: 2023-03-18 18:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6480.html
isCrawled: true
---
Skill Sharing - Einführungsworkshops in emanzipative Theorie und Praxis

Sie sehen aus als wären Sie aus einem Filmset für eine Geschichts-Doku ausgebrochen. Aber nicht nur der Style, auch das Weltbild ist mehr als Oldschool.
Was ist eigentlich der Unterschied zwischen einer Studentenverbindung und einer Burschenschaft? Sind die heute überhaupt noch wichtig in der Rechten? Auf solche und ähnliche Fragen versuchen wir in diesem Workshop Antworten zu formulieren. Wir wollen uns in netter Atmosphäre mit Texten auseinandersetzen, die einen Einstieg in das Thema erlauben und uns gemeinsam weiterführende Gedanken dazu machen. Es wird um die Geschichte der Burschenschaften gehen und um deren größere und kleinere Strukturen. 
Gemeinsam werden wir uns z.B. mit dem Nationalismus, Antifeminismus, Rassismus und Antisemitismus insbesondere der Deutschen Burschenschaft (DB) beschäftigen um das Weltbild zu durchblicken welches deren Tradition prägt. Vielleicht können wir uns auch einen Einblick in die Leipziger Verbindungslandschaft erarbeiten. 

Die Reihe findet in Kooperation mit der Rosa Luxemburg Stiftung Sachsen statt.

http://www.conne-island.de/termin/nr6480.html