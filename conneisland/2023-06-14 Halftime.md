---
id: "6595"
title: Halftime
start: 2023-06-14 18:00
end: 2023-06-14 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6595.html
isCrawled: true
---
Halftime

Conne Island's best Wednesday hangaround! 

Der Einlass ist begrenzt. Daher haben wir einen Telegram Channel eingerichtet, über den wir über einen Einlassstop informieren. Wenn das der Fall ist, vermeidet bitte lange Schlangen und kommt später wieder. Wir sagen bescheid, wenn wieder Platz ist!
https://t.me/joinchat/ME7JEF1Gs4Y3Mjhi

x no dogs allowed x 

http://www.conne-island.de/termin/nr6595.html