---
id: "6665"
title: Vortrag zum Thema "Graffiti und Männlichkeit"
start: 2023-10-11 18:00
end: 2023-10-11 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6665.html
isCrawled: true
---
Vortrag zum Thema "Graffiti und Männlichkeit"

1. Teil:
BREAK UP YOUR GRAFF BOYS CLUB
Aussteigerprogramm für Einsteiger
Vortrag und Diskussion mit Christoph May
Institut für Kritische Männlichkeitsforschung / Detox Masculinity Institute
Was genau ist eigentlich toxische Männlichkeit in der Graffiti-Szene? Bin ich selbst toxisch? Bin
ich wirklich ein guter Partner in Crime, Vater und Freund? Wie männlich sind meine Crew, meine
Styles, meine Musik und meine Graffiti-Filme, -Instas und -TikToks? Wie männlich mein
Doppelleben im Job, meine Beziehungen, mein Sex? Und ist mein Bomber-Blick auf die Welt
tatsächlich so frei und grenzenlos, wie ich glaube?

2. Teil:
Die Geschichte von einer, die auszog das Fürchten zu lernen
Vortrag mit Susanne Fitzek, Fachlehrerin, lebt seit 20 Jahren in Berlin. Sie ist Herausgeberin von
Sammlung 1 Berlin und dem Playboys-Kalender zusammen mit Chika und seit diesem Jahr
erstmalig Teil der Orga beim Hypergraphia-Festival in Potsdam. In ihren Projekten setzt sie sich
vor allem mit der Rolle von FLINTA* Personen und der Inszenierung von Männlichkeit, in der
Berliner Graffitiszene auseinander.

http://www.conne-island.de/termin/nr6665.html