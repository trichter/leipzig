---
id: "6566"
title: "2cl Sommerkino: ALL THE BEAUTY AND THE BLOODSHED"
start: 2023-07-22 21:00
end: 2023-07-23 04:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6566.html
isCrawled: true
---
ALL THE BEAUTY AND THE BLOODSHED

IN KOOPERATION MIT DER CINÉMATHÈQUE
USA 2022, Laura Poitras, 117 Min., OmU
mit Nan Goldin

Nan Goldin hatte bereits viele Leben und hat sie alle überlebt: Auf der Flucht aus dem konservativen Elternhaus tauchte sie in den 1980er Jahren tief in die queere Subkultur New Yorks ein. Ihre intimen Fotografien von Freund*innen machten sie zum Star der modernen Kunstwelt. Zum ersten Mal gab sie damit auch Menschen ein Gesicht, die von der Gesellschaft ausgeschlossen und verachtet wurden. Doch wenige Jahre später starben viele von Nans engsten Freund*innen an AIDS. Sie überlebte schmerzmittelabhängig. Heute ist Nan Goldin auch explizit politische Aktivistin im Kampf gegen die Milliardärsfamilie Sackler, die skrupellos Anstoß für die Opioidkrise in den USA gab  und zu den weltweit größten Kunstmäzenen zählt, von denen nicht zuletzt auch Künstler*innen wie Nan Goldin selbst abhängig sind.

Nan Goldins Leben und Werk zeugen von unglaublicher Stärke und Empathie. Laura Poitras (CITIZENFOUR) portraitiert die Künstlerin in einem Film, der, wie Nan Goldins Biographie selbst, geprägt ist von einer komplexen Verknüpfung gleich mehrerer hochrelevanter Diskurse unserer Zeit. Gleichzeitig ist der Film ein zutiefst berührendes Werk voller Schönheit und Kraft.

http://www.conne-island.de/termin/nr6566.html