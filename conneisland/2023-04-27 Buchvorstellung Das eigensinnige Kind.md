---
id: "6500"
title: 'Buchvorstellung: "Das eigensinnige Kind"'
start: 2023-04-27 18:30
end: 2023-04-27 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6500.html
isCrawled: true
---
Wolfram Ette, Karin Nungeßer

"Es war einmal ein Kind eigensinnig und that nicht, was seine Mutter haben wollte. Darum hatte der liebe Gott kein Wohlgefallen an ihm und ließ es krank werden und kein Arzt konnte ihm helfen und in kurzem lag es auf dem Todtenbettchen. Als es nun ins Grab versenkt war und Erde über es hingedeckt, so kam auf einmal sein Aermchen wieder hervor und reichte in die Höhe, und wenn sie es hineinlegten und frische Erde darüber thaten, so half das nicht, es kam immer wieder heraus. Da mußte die Mutter selbst zum Grabe gehn und mit der Ruthe aufs Aermchen schlagen und wie sie das gethan hatte, zog es sich hinein und hatte nun erst Ruhe unter der Erde."

Lesung und Diskussion

Das Märchen vom eigensinnigen Kind ist kurz und schrecklich, und illustriert mit seltener Brutalität, was mit Kindern geschieht, die nicht tun, was ihre Mütter haben wollen. Damit ist es  so die beiden Literaturwissenschaftler*innen Wolfram Ette und Karin Nungeßer  ein sehr deutsches Märchen. Ausgehend vom Grimmschen Text erkunden sie, was Eigensinn ist und welche Konsequenzen seine Unterdrückung hat.
Dabei geht es auch um die Frage historischer Kontinuitäten und transgenerationaler Weitergaben. Welche Spuren zeitigen der Nationalsozialismus und die Erziehungsratgeber von Johanna Haarer bis heute? Hat die Neue Rechte etwas mit unterdrücktem Eigensinn zu tun? Welche Fantasien treiben sie an? Welche Rolle spielt die Angst in der Attraktionskraft dieser und anderer sozialer Bewegungen und welche Rolle der Mangel? Lassen sich die destruktiven gesellschaftlichen Dynamiken des zugeschriebenen und unterdrückten, des ausgemerzten und verdrängten, des entstellten, ignorierten, parodierten, ungelebten, nicht totzukriegenden Eigensinns durchbrechen  und wenn ja, wie?

http://www.conne-island.de/termin/nr6500.html