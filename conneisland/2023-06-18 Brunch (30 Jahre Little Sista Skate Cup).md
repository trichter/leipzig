---
id: "6590"
title: Brunch (30 Jahre Little Sista Skate Cup)
start: 2023-06-18 10:00
end: 2023-06-18 14:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6590.html
isCrawled: true
---
Brunch (30 Jahre Little Sista Skate Cup)

http://www.conne-island.de/termin/nr6590.html