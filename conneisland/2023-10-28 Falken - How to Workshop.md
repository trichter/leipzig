---
id: "6631"
title: Skillsharing - Workshops als Teil emanzipatorischer Bildungsarbeit
start: 2023-10-28 15:00
end: 2023-10-28 19:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6631.html
isCrawled: true
---
Skillsharing - Workshops als Teil emanzipatorischer Bildungsarbeit

3 Stunden 
10 Teilnehmende

In Kooperation mit den Falken Leipzig

Bildung und Selbstbildung spielen eine entscheidende Rolle für eine emanzipatorische, linke Bewegung. Sie ermöglichen uns eine systematische Auseinandersetzung mit der Gesellschaft in der wir leben und auf die wir versuchen einzuwirken.
Seit einiger Zeit erfahren dabei Workshops als spezifische Bildungsveranstaltungen immer mehr Aufmerksamkeit in linken Gruppen und Projekten. Von ihnen erhoffen sich viele eine Alternative zu den voraussetzungsvollen und oft eintönig erscheinenden Lesekreis- und Vortragsveranstaltungen.
Für uns Falken, als sozialistischen Kinder- und Jugendverband, sind Lernen, Wissensvermittlung und gemeinsame kritische Gesellschaftsanalyse wichtige Bestandteile und Voraussetzungen für unsere politische Organisierung und Arbeit. Dafür nutzen auch wir Workshops, um Kindern, Jugendlichen und jungen Erwachsenen von 7 bis 27 Jahren, alternative und gesellschaftskritische Lern-, Bildungs- und Organisierungsräume zu schaffen. Hier können wir unsere Interessen diskutieren und so unsere Strukturen und politischen Perspektiven an diesen ausrichten.
So haben wir über die letzten Jahren einige Konzepte und Methoden entwickelt, erprobt und immer wieder verändert, die wir gerne mit euch teilen möchten. Dafür werden wir anfangs kurz in die politische Bildungsarbeit der Falken einführen, um daraus mit euch lern- und bildungstheoretische Grundsätze und Herausforderungen für die Gestaltung von Workshops abzuleiten. Darauf aufbauend wollen wir euch die Möglichkeit geben eigene Ideen, Interessen und erste Konzepte für Workshops zu entwickeln und mit uns zu diskutieren, sowie konkrete Methoden für diese zu erproben.
Für die Teilnahme braucht ihr keine Vorkenntnisse. Wenn ihr schon Ideen, Interessen oder Konzepte habt, könnt ihr diese selbstverständlich gerne mitbringen.

Eine Anmeldung ist nicht nötig.

http://www.conne-island.de/termin/nr6631.html