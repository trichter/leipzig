---
name: BUNDjugend Leipzig
website: https://www.bund-leipzig.de/bundjugend/
email: bundjugend@bund-leipzig.de
scrape:
  source: facebook
  options:
    page_id: bundjugendleipzig
---
Die BUNDjugend ist die Jugendorganisation im Bund für Umwelt und Naturschutz Deutschland e.V. (BUND). In Leipzig setzen wir uns für unsere Stadtnatur und eine nachhaltige Stadtentwicklung ein.