---
name: Primsa Leipzig
website: https://www.il-leipzig.org/
email: prisma@inventati.org
scrape:
  source: ical
  options:
    url: https://www.planlos-leipzig.org/events.ics
  filter:
    description: '/prismaleipzig/'
---
Seit Oktober 2012 gibt es in Leipzig die Gruppe Prisma. Wir haben alle unterschiedliche politische Hintergründe und Erfahrungen. Uns eint, dass wir uns organisieren wollen, um eine linksradikale Politik zu entwickeln, die offen für neue Mitstreiter_innen ist und Bündnisse mit anderen linken Kräften sucht. Ak­tu­ell sind un­se­re Ar­beits­schwer­punk­te Ras­sis­mus, Queer­fe­mi­nis­mus und So­zia­le Kämp­fe.