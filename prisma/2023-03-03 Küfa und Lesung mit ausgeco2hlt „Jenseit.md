---
id: "3903"
title: "Küfa und Lesung mit ausgeco2hlt: „Jenseits von Hoffnung und Zweifel“"
start: 2023-03-03 18:00
end: 2023-03-03 22:00
address: Tipi @ Westwerk, Karl-Heine-Straße 87, Leipzig
link: https://www.planlos-leipzig.org/events/kuefa-und-lesung-mit-ausgeco2hlt-jenseits-von-hoffnung-und-zweifel/
isCrawled: true
---
KüFa und Lesung „Jenseits von Hoffnung und Zweifel“ am 03.03. im Tipi, Westwerk.
Start Küfa 18h, Lesung 20 Uhr

Auf was können wir noch hoffen? Und was bleibt, wenn Hoffnung nicht mehr
trägt? Wenn dich das umtreibt, komm am 03.03. zur Lesung aus Buch von AusgeCO2hlt
„Jenseits von Hoffnung und Zweifel. Gedanken zum Widerstand in der
Klimakrise.“
https://www.unrast-verlag.de/index.php/neuerscheinungen/jenseits-von-hoffnung-und-zweifel-detail

AusgeCo2hlt, das sind rund ein gutes Dutzend Menschen, die mit viel Zeit
und Herz in der Bewegung für Klimagerechtigkeit aktiv sind, vor allem
rund um das Rheinische Braunkohlerevier. Sie schauen auf Arbeitsweisen
in der Klimabewegung und analysieren ihre großen Narrative - die
Apokalypse, die Utopie, ‚Unite behind the science‘. Sie prüfen, was
ihnen Kraft gibt, politisch aktiv zu sein und was ihnen Energie raubt.
Überall schwingt die Frage mit, wie wir von einer Arbeitsweise
wegkommen, die einem chronischem Ausnahmezustand ähnelt, hin zu einem
widerständigem Leben, in dem wir langfristig zu Hause sein können.
Die Autor*innen freuen sich, mit euch darüber ins Gespräch zu kommen: am
03.03.2023 um 20 Uhr im Tipi, Westwerk.

„Oft ist davon die Rede, dass wir das Klima ›retten‹ müssen. Das klingt
so, als könnten wir den Zustand der Welt irgendwann in eine sichere
Schublade legen, dann Feierabend machen und Minigolf spielen gehen. Doch
es wird nicht den einen mythischen Tag geben, an dem die Welt zum
Paradies wird. Was nicht heißt, dass wir nicht gewinnen können. Wir
können Zerstörung aufhalten, Lebensverhältnisse verbessern. Doch der
Kampf um Klimagerechtigkeit wird nie vorbei sein.“

https://www.instagram.com/prismaleipzig/

https://www.planlos-leipzig.org/events/kuefa-und-lesung-mit-ausgeco2hlt-jenseits-von-hoffnung-und-zweifel/