---
id: apfelpressen-und-streuobstralley
title: Apfelpressen und Streuobstralley
start: 2023-10-08 10:00
end: 2023-10-08 14:00
address: Streuobstwiese, Stahmelner Straße, 04159 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/apfelpressen-und-streuobstralley/
image: Apfelbaum__Jana_Burmeister__weboptimiert.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Beim jährlichen Apfelpressen auf der Streuobstwiese gibt es wieder viel zu entdecken für Klein und Groß, vom Apfel zum Saft und vieles mehr.</p><p>Um Anmeldung über ernte.dank@bundleipzig.de wird gebeten!&nbsp;<b>Kostenlos</b></p></div></div>

<!--TYPO3SEARCH_end-->