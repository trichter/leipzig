---
id: mehrweg-stammtisch-gastronomie-und-lebensmittelbetriebe-von-mockau-1
title: Mehrweg-Stammtisch Gastronomie- und Lebensmittelbetriebe von Mockau
start: 2023-05-03 14:00
address: Die Villa Leipzig (Mockau), Essener Straße 100, 04357 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/mehrweg-stammtisch-gastronomie-und-lebensmittelbetriebe-von-mockau-1/
image: csm_IMG_20230331_133021_85368d7136.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Die Mehrwegangebotspflicht ist da! Wer eine Gastronomie in der Leipziger Innenstadt, in Mockau oder entlang der Georg-Schumann-Straße betreibt, kann sich im Rahmen der „Allerlei-to-go“-Kampagne durch den BUND Leipzig unterstützen lassen. Angeboten werden</p><ul class="rte-unordered-list"><p>¨NBSP;</p><li>Status-Checks</li><li>Beratungen auf Augenhöhe</li><li>Vernetzung mit weiteren Gastronomien der Umgebung sowie</li><li>Prämienanreize</li> <p></p></ul><p>Das Ziel des "Mehrweg-Stammtisches" soll es sein, die in Mockau ansässigen Gastronomie- und Lebensmittelbetriebe über die Kampagne "Allerlei to go" zu informieren sowie eine Plattform für den Austausch und die Vernetzung untereinander zu bieten. So kann der "Alternative Mehrweg" gemeinsam begegnet werden!</p><h2 class="rte-heading rte-heading--h3">Mehr Informationen</h2><div class="rte-highlight rte-highlight--greenLight"><p>Eine verbindliche Anmeldung ist erforderlich an <a href="#" title="Mehrweg-Stammtisch Mockau" data-mailto-token="nbjmup+hbtuspopnjfAcvoe.mfjqajh/ef" data-mailto-vector="1">gastronomie(at)bund-leipzig.de</a></p><p>Weitere Informationen zu "Allerlei to go" sowie zu Möglichkeiten der Kontaktaufnahme unter <a href="/mehrweg/">www.bund-leipzig.de/mehrweg</a></p></div></div></div>

<!--TYPO3SEARCH_end-->