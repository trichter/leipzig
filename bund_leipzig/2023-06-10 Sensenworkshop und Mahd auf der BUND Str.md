---
id: sensenworkshop-1
title: Sensenworkshop und Mahd auf der BUND Streuobstwiese, 8 Uhr
start: 2023-06-10 08:15
address: Stahmelner Str. 37, 04159 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/sensenworkshop-1/
image: 2017-5-20_Heumahd_Sensenkurs_1B8A6009_Susanne_Wagner__weboptimiert.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Die Mahd auf der BUND-Streuobstwiese in Wahren steht an!</p><p>Für alle, die selbst einmal zur Sense greifen möchten, bietet der BUND Leipzig einen Sensenworkshop an, bei dem das fachgerechte Mähen mittels Sense und das Dengeln der Sensen von Thomas Thiel erläutert und vorgeführt wird. Verpflegung für ein anschließendes Picknick oder auch vorhandene Sensen dürfen gern mitgebracht werden. Festes Schuhwerk wird empfohlen.</p><p></p><p><b>Die Teilnahme ist kostenlos. Eine Anmeldung ist erforderlich.</b></p><p>Wir freuen uns über helfende Hände!</p><p><b><a href="https://www.google.de/maps/place/51%C2%B022'25.1%22N+12%C2%B018'52.8%22E/@51.4228981,12.2223291,28288m/data=!3m1!1e3!4m5!3m4!1s0x0:0x0!8m2!3d51.3736389!4d12.3146667" target="_blank" rel="noreferrer">Wegbeschreibung:</a></b></p><p>An Öffnungstagen ist die Wiese an der Stahmelner Straße, stadtauswärts auf der linken Straßenseite, zu erreichen. Hinter einer Steinmauer mit einem großen Tor ohne Straßennummer liegt sie versteckt.¨NBSP;<b>Tram:</b> Linie 11 (Richtung Schkeuditz), Haltestelle Pittlerstraße.</p><p>zu finden mit den Koordinaten: 51°22'25.1"N 12°18'52.8"E</p><h2 class="rte-heading rte-heading--h3">Mehr Informationen</h2><div class="rte-highlight rte-highlight--greenLight"><p>Für die Teilnahme ist eine <b>Anmeldung erforderlich</b> unter: <b><a href="#" data-mailto-token="nbjmup+bonfmefoAcvoe.mfjqajh/ef" data-mailto-vector="1">anmelden(at)bund-leipzig.de</a> </b>oder <b>0341 / 9899 1050</b></p><p><a href="#" title="ANMELDUNG" class="rte-anchor rte-anchor--button rte-anchor--button__btnGreen" data-mailto-token="nbjmup+bonfmefoAcvoe.mfjqajh/ef" data-mailto-vector="1">ANMELDUNG</a></p><p><a href="https://www.bund-leipzig.de/themen-und-projekte/naturschutz/streuobstwiese-wahren/" target="_top">Mehr zu unserer Streuobswiese erfahren!</a></p><p><a href="https://www.bund-leipzig.de/ueber-uns/arbeitskreise/arbeitskreis-streuobst/" target="_top">Zur Seite des AK Streuobst.</a></p></div></div></div>

<!--TYPO3SEARCH_end-->