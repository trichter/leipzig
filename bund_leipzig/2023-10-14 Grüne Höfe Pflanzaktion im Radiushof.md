---
id: gruene-hoefe-pflanzaktion-im-radiushof
title: '"Grüne Höfe" Pflanzaktion im Radiushof'
start: 2023-10-14 14:00
end: 2023-10-14 17:00
address: Wohnanlage "Radiushof", Credéstraße 9
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/gruene-hoefe-pflanzaktion-im-radiushof/
image: csm_Kiju_Gruenau_Vorher_Nachher_1_Nicole_Bruehl_7fe4faa0e6.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>In Kooperation mit der <b>Vereinigten Leipziger Wohnungsgenossenschaft eG </b>und der dort ansässigen Mieterschaft wird im Radiushof eine Wildblumenwiese angelegt. Das Projekt "Grüne Höfe" unterstützt dabei und lädt ausdrücklich weitere Interessierte zum Ideen holen und Mitmachen ein!</p><p>Wer schon immer mal wissen wollte, wie genau eine Wildblumenwiese angelegt wird und welche Schritte hier zu beachten sind, darf gerne vorbeikommen und ggf. direkt mit anpacken. Wie immer gilt: Bitte achtet darauf, Gartenkleidung anzuziehen! Es wird geeggt, gewalzt und Limonade getrunken.</p><p>Achtung: Ihr wollt euren Hof auch fachkundig begrünen und dafür finanziell gefördert werden? Der Endspurt läuft beim <a href="/themen-und-projekte/natur-und-artenschutz/gruene-hoefe/">Projekt "Grüne Höfe"</a> - jetzt noch anmelden und bis Ende November&nbsp;den Hof gestalten!</p><h2 class="rte-heading rte-heading--h3">Mehr Informationen</h2><div class="rte-highlight rte-highlight--greenLight"><p>Eine Anmeldung wird erbeten unter <a href="#" data-mailto-token="nbjmup+lpnnvojlbujpo.hsvfofipfgfAcvoe.mfjqajh/ef" data-mailto-vector="1">kommunikation-gruenehoefe(at)bund-leipzig.de</a></p><p>Die Pflanzaktion ist kostenfrei. Familien sind herzlich willkommen.</p></div></div></div>

<!--TYPO3SEARCH_end-->