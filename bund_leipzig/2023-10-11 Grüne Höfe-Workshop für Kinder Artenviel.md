---
id: gruene-hoefe-workshop-fuer-kinder-artenvielfalt-vor-der-haustuer
title: '"Grüne Höfe"-Workshop für Kinder: Artenvielfalt vor der Haustür'
start: 2023-10-11 15:00
address: Bauspielplatz Mockau „Fuxbau“, Essener Str. 51, 04357 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/gruene-hoefe-workshop-fuer-kinder-artenvielfalt-vor-der-haustuer/
image: csm_Igel_mit_Haus_und_Logo_Juliane_Weickert__8e02612f06.png
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p class="MsoPlainText">Zur Vorbereitung auf die kalte Jahreszeit wollen wir in den Herbstferien gemeinsam auf dem Bauspielplatz eine <b>Igelburg bauen</b>, die den Tieren einen ungestörten Unterschlupf bietet. Dafür nutzen wir viele natürliche Materialien (Laub, Steine, Äste, Holz). Ganz nebenbei werden altersgerechte Informationen über den Lebensraum und die Bedeutung von Igeln und weiteren Hofbewohnern vermittelt.</p><p class="MsoPlainText">Außerdem dürfen sich alle Kinder zur Vorfreude auf den Frühling <b>Gefäße mit Frühjahrszwiebeln</b> bepflanzen. Pünktlich zum Frühlingsbeginn werden diese sprießen und können dann von den Kindern in ihren Höfen angepflanzt werden. So fördern die Kinder auf spielerische und selbstwirksame Weise die Artenvielfalt vor ihrer Haustür.</p><p>Der Workshop richtet sich an Kinder der 1. - 6. Klasse, Eltern und Geschwisterkinder sind ebenfalls willkommen. Ein pünktliches Erscheinen um 15.00 ist keine Voraussetzung, das Angebot besteht fortlaufend bis ca. 18.00 Uhr.</p><p>Geheimtipp für alle Erwachsenen: Nutzt die Chance der nun anstehenden letzten "Grüne Höfe"-Pflanzperiode, meldet euch mit eurem Hof an und lasst euch mit bis zu 470 € pro Hof bezuschussen! Weitere Informationen unter <a href="http://www.bund-leipzig.de/gruenehoefe" target="_blank">www.bund-leipzig.de/gruenehoefe</a></p><h2 class="rte-heading rte-heading--h3">Mehr Informationen</h2><div class="rte-highlight rte-highlight--greenLight"><p>Eine Anmeldung an <a href="#" data-mailto-token="nbjmup+lpnnvojlbujpo.hsvfofipfgfAcvoe.mfjqajh/ef" data-mailto-vector="1">kommunikation-gruenehoefe(at)bund-leipzig.de</a> erleichtert uns das Einschätzen, ist aber nicht zwingend erforderlich. Das Angebot ist kostenfrei.</p></div></div></div>

<!--TYPO3SEARCH_end-->