---
id: bier-von-hier
title: Bier von hier
start: 2023-10-06 16:30
address: Plagwitzer Brauerei, Klingenstraße 22, 04229 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/bier-von-hier/
image: csm_beer-3378136_1280_29e146a118.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Lokal gebrautes Bier – warum ist das wichtig und wertvoll? Die Plagwitzer Brauerei stellt sich und ihr Bier vor. Wie klimafreundlich und lecker regionales Bier ist, wollen wir mit euch sowohl theoretisch als auch praktisch erfahren, indem wir bei kalten Bier einem Vortrag zum CO2-Fussabruck unserer Lebensmittel lauschen</p><p>Um Anmeldung über ernte.dank@bundleipzig.de wird gebeten!</p><p>Unkosten für Bier</p></div></div>

<!--TYPO3SEARCH_end-->