---
id: gruene-hoefe-workshop-wie-wird-mein-innenhof-ein-hotspot-der-artenvielfalt
title: '"Grüne Höfe"-Workshop Herbst-Edition: Wie wird mein Innenhof ein Hotspot
  der Artenvielfalt?'
start: 2023-09-23 15:00
end: 2023-09-23 18:00
address: Nachbarschaftsgärten e. V., Josephstraße 27
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/gruene-hoefe-workshop-wie-wird-mein-innenhof-ein-hotspot-der-artenvielfalt/
image: csm_IMG_20230324_150015_102ca9c68b.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Gemeinsam mit Philipp Drosky von <a href="http://www.naturgarten-leipzig.de" target="_blank" rel="noreferrer">Naturgarten Leipzig</a> treffen wir uns heute im Nachbarschaftsgarten in der Josephstraße. Auch hier wollen wir nach den Prinzipien des naturnahen Gärtnerns ein paar Rückzugsorte für die stadtgestressten Insekten schaffen! Wir klären, worauf im Herbst bei der Pflanzung und auch allgemein besonders geachtet werden muss und legen direkt zusammen los.</p><p>Bitte achtet darauf, Gartenkleidung anzuziehen!</p><p>Achtung: Ihr wollt euren Hof auch fachkundig begrünen und dafür finanziell gefördert werden? Der Endspurt läuft beim <a href="/themen-und-projekte/natur-und-artenschutz/gruene-hoefe/">Projekt "Grüne Höfe"</a> - jetzt noch anmelden und bis November&nbsp;den Hof gestalten!</p><h2 class="rte-heading rte-heading--h3">Mehr Informationen</h2><div class="rte-highlight rte-highlight--greenLight"><p>Eine Anmeldung wird erbeten unter <a href="#" data-mailto-token="nbjmup+lpnnvojlbujpo.hsvfofipfgfAcvoe.mfjqajh/ef" data-mailto-vector="1">kommunikation-gruenehoefe(at)bund-leipzig.de</a></p><p>Der Workshop ist kostenfrei und es gibt Limonade! Familien sind herzlich willkommen.</p></div></div></div>

<!--TYPO3SEARCH_end-->