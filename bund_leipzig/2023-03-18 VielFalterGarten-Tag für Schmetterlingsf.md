---
id: vielfaltergarten-tag-fuer-schmetterlingsfans-14-uhr
title: VielFalterGarten-Tag für Schmetterlingsfans, 14 Uhr
start: 2023-03-18 14:17
address: im Hörsaal 115 in der Johannisallee 21, 04103 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/vielfaltergarten-tag-fuer-schmetterlingsfans-14-uhr/
image: SLIDER-Sommer-Schmetterling1__Gabriele_Rada_.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Save the Date! Unsere Teilnehmenden-Konferenz steht wieder an – diesmal in Präsenz!</p><p>Über 400 Teilnehmende beobachteten im Jahr 2022 fleißig Tagfalter in und um Leipzig. Die Ergebnisse, die wir aus den Zählungen erhalten haben, stellen wir während der VielFalterGarten-Konferenz vor. Welche Tagfalter wurden 2022 gesichtet? Können Rückschlüsse und Zusammenhänge aus den Zählungen der Jahre zuvor gezogen werden? Was können wir im Jahr 2023 weiterhin für mehr Biodiversität in Leipzig und darüber hinaus tun? Solche und weitere Fragen möchten wir vor Ort mit euch beantworten und diskutieren. Außerdem warten weitere tolle Programmpunkte auf euch:</p><p><b>14-15 Uhr:</b> Präsentationen zum Projekt VielFalterGarten und bisheriger Projekt-Ergebnisse; Kinderbetreuung vor Ort ist möglich</p><p><b>15.15 Uhr:</b> Gemeinsamer Weg zum Botanischen Garten der Universität Leipzig (Linnéstr. 1, 04103 Leipzig)</p><p><b>Ab 15.30 Uhr:</b> Vielseitiges Programm in den Gewächshäusern, mit:</p><ul class="rte-unordered-list"><p>¨NBSP;</p><li>Führungen durch die Gewächshäuser</li><li>Kinderführungen durch das Schmetterlingshaus</li><li>Themenstände zum Informieren und Ausprobieren</li><li>Spielen und Basteln für Kinder</li><li>Austausch bei Kleinen Leckereien</li> <p></p></ul><p>Die Veranstaltung ist kostenfrei. Bitte meldet euch vorher an unter <a href="https://www.vielfaltergarten.de/veranstaltungen/anmeldung@vielfaltergarten.de" target="_blank" rel="noreferrer">anmeldung(at)vielfaltergarten.de </a></p><p>Bei weiteren Fragen kontaktiert uns gern: <a href="#" data-mailto-token="nbjmup+jogpAwjfmgbmufshbsufo/ef" data-mailto-vector="1">info(at)vielfaltergarten.de</a></p></div></div>

<!--TYPO3SEARCH_end-->