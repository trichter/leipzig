---
id: exkursion-naturfotografie
title: Fotoexkursion durch den Auwald
start: 2023-04-22 10:12
address: "Treffpunkt: Galopprennbahn Scheibenholz, Rennbahnweg 2 A, 04107 Leipzig"
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/exkursion-naturfotografie/
image: csm_AKFuF_Mascha__5488cd701a.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Der Arbeitskreis Foto und Film lädt ein zu einer gemeinsamen Fotoexkursion durch den Auwald. Die Exkursion ist eine super Gelegenheit mit uns ins Gespräch zu kommen und den AK Foto und Film kennenzulernen.&nbsp;Jeder, der Spaß am Fotografieren und/oder einem Naturspaziergang hat, ist herzlich eingeladen!</p><p>Wir freuen uns!&nbsp;</p><h2 class="rte-heading rte-heading--h3">Mehr Informationen</h2><div class="rte-highlight rte-highlight--greenLight"><p><b>Neue Mitstreiter*innen</b>&nbsp;sind immer&nbsp;<b>herzlich willkommen</b>! Kommen Sie einfach zum Treffpunkt und lernen Sie uns kennen.&nbsp;</p><p>Mehr Informationen zum Arbeitskreis Foto und Film sind <a href="https://www.bund-leipzig.de/ueber-uns/arbeitskreise/arbeitskreis-foto-und-film/" target="_blank">hier</a> zu finden.&nbsp;</p></div></div></div>

<!--TYPO3SEARCH_end-->