---
id: workshop-wie-wird-mein-garten-zum-schmetterlingsmagnet
title: AUSGEBUCHT! Workshop “Wie wird mein Garten zum Schmetterlingsmagnet?”
start: 2023-05-07 11:00
end: 2023-05-07 13:00
address: Kleingartenverein Abendsonne e.V., Küchenholzallee 2b, 04249 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/workshop-wie-wird-mein-garten-zum-schmetterlingsmagnet/
image: workshop-gaertnern-1170x694.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Kleingärtner*innen aufgepasst! Wir bieten auch in diesem Jahr wieder Workshops zum Thema “Naturnahes Gärtnern im Kleingarten” an. In unserem Workshop geben wir Tipps und Anleitungen, die euch das naturnahe Gärtnern näher bringen. Was ist in einem Kleingarten möglich und was muss beachtet werden? Diese und weitere Fragen werden wir im Rahmen des Workshops beantworten.</p><p></p><p><b>Alle Plätze sind vergeben! </b></p></div></div>

<!--TYPO3SEARCH_end-->