---
id: gruene-hoefe-workshop-zur-anlage-einer-blumenwiese
title: '"Grüne Höfe"-Workshop zur Anlage einer Wildblumenwiese'
start: 2023-03-24 14:30
address: Kinder- & Jugendtreff Leipzig-Grünau e. V., Heilbronner Straße 16,
  04209 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/gruene-hoefe-workshop-zur-anlage-einer-blumenwiese/
image: csm_230213_Illu_Gruenau_e92578c7d3.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Endlich beginnen unsere "Draußen"-Workshops! Heute geht es gleich ganz praktisch an die Anlage einer Wildblumenwiese gemeinsam mit dem am Projekt teilnehmenden Kinder- und Jugendtreff Grünau. Die Wildblumenwiese ist eine absolute win-win-situation für Mensch und Tier: Einmal eingerichtet und ausgesät, macht sie viel weniger Arbeit als konventionelle Rasenflächen. Gleichzeitig bereichert sie den Speiseplan der dort lebenden Arten immens und fördert so auch die Artenvielfalt.</p><p>Wir möchten unsere Workshop-Teilnehmenden herzlich dazu einladen, bei den folgenden Schritten selbst mit anzupacken:</p><ul class="rte-unordered-list"><p>¨NBSP;</p><li>Vorbereitung des Saatbetts (Achtung, hier wird gegraben!)</li><li>Aussaat des Saatguts</li><li>Anstoßen mit Limo</li> <p></p></ul><p>Bitte denkt an festes Schuhwerk und Gartenkleidung!</p><p>Der Workshop richtet sich an die Teilnehmenden unseres Projekts und alle, die es noch werden wollen. Mehr Infos zum Projekt: <a href="https://www.bund-leipzig.de/gruenehoefe" target="_blank">www.bund-leipzig.de/gruenehoefe</a></p><h2 class="rte-heading rte-heading--h3">Mehr Informationen</h2><div class="rte-highlight rte-highlight--greenLight"><p>Um Anmeldung wird gebeten unter <a href="#" data-mailto-token="nbjmup+lpnnvojlbujpo.hsvfofipfgfAcvoe.mfjqajh/ef" data-mailto-vector="1">kommunikation-gruenehoefe(at)bund-leipzig.de</a>.</p></div></div></div>

<!--TYPO3SEARCH_end-->