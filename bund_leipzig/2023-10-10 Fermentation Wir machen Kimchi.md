---
id: fermentation-wir-machen-kimchi
title: "Fermentation: Wir machen Kimchi"
start: 2023-10-10 18:00
address: Cafe - Haus der Demokratie, Bernhard-Göring-Straße 152, 04277 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/fermentation-wir-machen-kimchi/
image: _c_Philip.Knoll_Einfach_Unverpackt_Leipzig_9_webseite.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Kaffee, Brot, Käse, Kimchi – Was haben diese Lebensmittel gemeinsam? Richtig, sie sind fermentiert. Wir klären auf, was das eigentlich ist und stellen selbst Kimchi her.</p><p><b>Bitte ein großes Schraubglas mitbringen.</b></p><p>Um Anmeldung über ernte.dank@bundleipzig.de wird gebeten!&nbsp;<b>Kostenlos.</b></p></div></div>

<!--TYPO3SEARCH_end-->