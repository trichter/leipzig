---
id: praktische-tipps-zur-umsetzung-der-mehrwegsangebotspflicht
title: Praktische Tipps zur Umsetzung der Mehrwegsangebotspflicht
start: 2023-03-02 10:00
end: 2023-03-02 12:00
address: IHK zu Leipzig, Goerdelerring 5, 04109 Leipzig, Raum 605
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/praktische-tipps-zur-umsetzung-der-mehrwegsangebotspflicht/
image: csm_2022-06-07_Tupperdose_Glas_Suffiziente_Gastronomie__c_ThomasPuschmann__6__fc6bd34fea.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Das Verzehrverhalten der Gäste hat sich in den vergangenen Jahren stark verändert. Der „schnelle Snack“ zwischendurch und der Kaffee to-go bestimmen unser Straßenbild. Die dazugehörigen Einwegverpackungen für Speisen und Getränke sorgen für volle Mülleimer und stellen eine unnötige Verschwendung von Ressourcen dar.</p><p>Auch deshalb besteht seit dem 1. Januar 2023 eine Mehrwegangebotspflicht für Speisen und Getränke zum Mitnehmen.</p><p>Die Umsetzung dieser Pflicht nach § 33 und&nbsp;34 Verpackungsgesetz wirft bei den Gastronomen noch einige Fragen auf. In der gemeinsamen Informationsveranstaltung von IHK zu Leipzig, DEHOGA Sachsen Regionalstelle Leipzig und BUND Regionalgruppe Leipzig&nbsp;möchten wir Ihnen Antworten auf Ihre Fragen und praktische Umsetzungstipps geben.</p><p></p><p>Die Teilnahme ist kostenlos. Anmeldung erforderlich unter&nbsp;<a href="https://www.leipzig.ihk.de/veranstaltung/mehrweg-die-pflicht-als-chance-nutzen/" target="_blank" rel="noreferrer">https://www.leipzig.ihk.de/veranstaltung/mehrweg-die-pflicht-als-chance-nutzen/</a></p></div></div>

<!--TYPO3SEARCH_end-->