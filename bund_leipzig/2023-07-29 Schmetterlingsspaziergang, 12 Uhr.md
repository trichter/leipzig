---
id: schmetterlingsspaziergang
title: Schmetterlingsspaziergang, 12 Uhr
start: 2023-08-05 12:00
address: Gemeinschaftsgarten Grünau, Miltitzer Allee 2, WK 8 Lausen-Grünau 04207 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/schmetterlingsspaziergang/
image: schmetterlingsspaziergang.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Am 5. August geht es mit der Volkshochschule Leipzig auf die Suche nach Schmetterlingen! Dabei könnt ihr spannendes über die bunten Insekten lernen, warum wir sie brauchen und wie ihr helfen könnt sie zu erhalten! Natürlich halten wir währenddessen auch immer Ausschau nach den Tagfaltern und mit etwas Glück könnt ihr Admiral und Co. ganz aus der Nähe betrachten!</p><p>Der Spaziergang ist wie immer kostenlos und für die ganze Familie geeignet!</p><p></p><p>Die Teilnehmerzahl ist begrenzt. Daher bitten wir um Anmeldung:&nbsp;<a href="https://www.vhs-leipzig.de/p/-495-C-C181G24K" target="_blank" data-id="https://www.vhs-leipzig.de/p/-495-C-C181G24K" data-type="URL" rel="noreferrer">hier anmelden.</a></p><p>Bei weiteren Fragen kontaktiert uns gern:&nbsp;<a href="#" data-mailto-token="nbjmup+jogpAwjfmgbmufshbsufo/ef" data-mailto-vector="1">info(at)vielfaltergarten.de</a></p></div></div>

<!--TYPO3SEARCH_end-->