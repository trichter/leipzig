---
id: gruene-hoefe-workshop-zur-hof-gestaltungsplanung
title: '"Grüne Höfe"-Workshop zur Hof-Gestaltungsplanung'
start: 2023-03-11 15:00
end: 2023-03-11 18:00
address: Pöge-Haus, Hedwigstraße 20, 04315 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/gruene-hoefe-workshop-zur-hof-gestaltungsplanung/
image: csm_221114_Illu_Workshop_bbd6ff7c11.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Manchmal ist es so schwer, anzufangen!</p><p>Das wissen wir vom Projekt "Grüne Höfe" natürlich und bieten deshalb Starthilfe in Form dieses Workshops. Gemeinsam wollen wir Schritt für Schritt durch die relevanten Fragen der Hof-Gestaltung nach ökologischen Gesichtspunkten gehen und im Austausch miteinander Möglichkeiten der Begrünung erarbeiten. Dabei werden uns u. a. folgende Fragen beschäftigen:</p><ul class="rte-unordered-list"><p>¨NBSP;</p><li>Was gibt es bereits im Hof (z. B. Strukturen, Arten)? Was davon ist verzichtbar oder gar invasiv?</li><li>Wie finde ich die passenden Gestaltungsmaßnahmen für meinen Hof?</li><li>Zu welchem Zeitpunkt macht eine Umsetzung der Maßnahmen Sinn?</li> <p></p></ul><p>Wir möchten unsere Workshop-Teilnehmenden bitten, Bildmaterial ihrer Höfe mitzubringen - so können wir leichter in den Planungsteil einsteigen.</p><p>Der Workshop richtet sich an die Teilnehmenden unseres Projekts und alle, die es noch werden wollen. Mehr Infos zum Projekt: <a href="https://www.bund-leipzig.de/gruenehoefe" target="_blank">www.bund-leipzig.de/gruenehoefe</a></p><h2 class="rte-heading rte-heading--h3">Mehr Informationen</h2><div class="rte-highlight rte-highlight--greenLight"><p>Um Anmeldung wird gebeten unter <a href="#" data-mailto-token="nbjmup+lpnnvojlbujpo.hsvfofipfgfAcvoe.mfjqajh/ef" data-mailto-vector="1">kommunikation-gruenehoefe(at)bund-leipzig.de</a>. Die Teilnahmezahl ist auf 15 Plätze beschränkt, es wird eine Warteliste geben.</p></div></div></div>

<!--TYPO3SEARCH_end-->