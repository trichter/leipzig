---
id: schmetterlingsfreundlicher-garten
title: Schmetterlingsfreundlicher Garten
start: 2023-10-07 10:13
address: KGV “An der Krähenhütte”, Dösner Straße, Treff Amselweg
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/schmetterlingsfreundlicher-garten/
image: csm_Schmetterling__Guy_Peer__-_Kopie_9dc81fe47f.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Wie kommen die Schmetterlinge in unsere Gärten zurück und was können wir dafür tun? Im Rahmen des VielfalterGarten-Projektes laden wir Sie in einen “schmetterlingsfreundlichen Garten” ein und zeigen wie es gehen kann.</p><p>Um Anmeldung über ernte.dank@bundleipzig.de wird gebeten!</p></div></div>

<!--TYPO3SEARCH_end-->