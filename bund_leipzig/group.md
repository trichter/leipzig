---
name: BUND Leipzig
website: https://www.bund-leipzig.de
scrape:
  source: bund
  options:
    base: https://www.bund-leipzig.de/service/termine-bund-leipzig/
    categoryIds:
        - 12 # Exkursion
        - 17 # Vortrag
        - 18 # Workshop
---
Im BUND Leipzig setzen wir uns seit 2009 für Umwelt- und Naturschutz in Leipzig um Umgebung ein. Als Regionalgruppe des Bund für Umwelt und Naturschutz Deutschland sind wir Teil des mit über 500.000 Unterstützer*innen größten und wichtigsten gemeinnützigen Naturschutzverbands Deutschlands.