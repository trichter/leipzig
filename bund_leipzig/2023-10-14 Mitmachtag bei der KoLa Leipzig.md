---
id: mitmachtag-bei-der-kola-leipzig
title: Mitmachtag bei der KoLa Leipzig
start: 2023-10-14 10:16
address: KoLa Leipzig, Engelsdorfer Str. 99, 04425 Taucha
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/mitmachtag-bei-der-kola-leipzig/
image: csm_KoLa-Leipzig-_Guy_Peer_48be5bbaa6.jpeg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Nach einer kurzen Ackerführung bei der solidarischen Landwirtschaft, KoLa, geht es weiter mit der gemeinsamen Sellerie-Ernte.</p></div></div>

<!--TYPO3SEARCH_end-->