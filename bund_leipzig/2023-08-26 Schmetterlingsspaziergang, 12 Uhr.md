---
id: schmetterlingsspaziergang-1
title: Schmetterlingsspaziergang, 12 Uhr
start: 2023-08-26 12:00
address: Grüner Bogen Paunsdorf, Heiterblickallee 72, 04329 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/schmetterlingsspaziergang-1/
image: schmetterlingsspaziergang.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Am 26. August laden wir euch mit der Volkshochschule Leipzig zu einem gemütlichen Spaziergang durch den Grünen Bogen in Paunsdorf ein! Dabei machen wir uns auf die Suche nach heimischen Tagfaltern und lernen, wieso wir sie brauchen und wie wir sie erhalten können. Mit ein bisschen Glück beobachten wir dabei auch Kaisermantel und Co. ganz aus der Nähe!</p><p>Der Spaziergang ist wie immer kostenlos und für die ganze Familie geeignet!</p><p>Die Teilnehmerzahl ist begrenzt. Wir bitten daher um Anmeldung:&nbsp;<a href="https://www.vhs-leipzig.de/p/gruenau/politik-gesellschaft-umwelt/schmetterlingsspaziergang-gruener-bogen-paunsdorf-495-C-C181P00K" target="_blank" data-id="https://www.vhs-leipzig.de/p/gruenau/politik-gesellschaft-umwelt/schmetterlingsspaziergang-gruener-bogen-paunsdorf-495-C-C181P00K" data-type="URL" rel="noreferrer">hier anmelden.</a></p><p>Bei weiteren Fragen kontaktiert uns gern:&nbsp;<a href="#" data-mailto-token="nbjmup+jogpAwjfmgbmufshbsufo/ef" data-mailto-vector="1">info(at)vielfaltergarten.de</a></p><p></p></div></div>

<!--TYPO3SEARCH_end-->