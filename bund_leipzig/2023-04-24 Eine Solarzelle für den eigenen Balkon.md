---
id: eine-solarzelle-fuer-den-eigenen-balkon-19-uhr
title: Eine Solarzelle für den eigenen Balkon
start: 2023-04-24 19:30
end: 2023-04-24 21:30
address: Café im Haus der Demokratie, Bernhard-Göring-Str. 152, 04277 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/eine-solarzelle-fuer-den-eigenen-balkon-19-uhr/
image: solar-system-ge2253cd5e_1920_httpspixabay.comphotossolar-system-roof-power-generation-2939560_.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Der Arbeitskreis <a href="/ueber-uns/arbeitskreise/arbeitskreis-klima-und-energie/" target="_blank">Klima und Energie</a> lädt zu einer Infoveranstaltung über Steckersolarzellen. Lohnt sich sich eine Steckersolarzelle auch für meinen Balkon? Was muss ich bei der Installation beachten? Und fördert die Stadt Leipzig eine Anschaffung? All diese Fragen beantwortet Christian Neuperger am 24. April um 19:30 Uhr im Café vom Haus der Demokratie. Seien Sie dabei und werden auch Teil der Energiewende!</p><h2 class="rte-heading rte-heading--h3">Mehr Informationen</h2><div class="rte-highlight rte-highlight--greenLight"><p>Um Anmeldung wird gebeten unter anmelden(at)bund-leipzig.de</p></div></div></div>

<!--TYPO3SEARCH_end-->