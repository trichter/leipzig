---
id: infoabend-nord-trifft-sued-gemeinsam-mit-eine-welt-leipzig-ev
title: Infoabend „Nord-trifft-Süd“ - gemeinsam mit Eine Welt Leipzig e.V.
start: 2023-09-16 18:30
address: Stadtbüro Leipzig, Burgplatz 1, 04109 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/infoabend-nord-trifft-sued-gemeinsam-mit-eine-welt-leipzig-ev/
image: csm_EW_Nord_trifft_sued_El_Puente_Michael_Sommer_9133333bc4.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Im Rahmen der bundesweiten Fairen Woche unter dem Motto „Fair und kein Grad mehr“ laden wir und Eine Welt Leipzig e.V. zu einem Infoabend mit lokalen Gästen aus dem Bereich Klimaschutz und Biodiversität sowie indischen Gästen der Fairhandelsorganisation „Last Forest“ ein. Last Forest stellt dabei sein Fairhandelsprojekt vor und geht auf Herausforderungen durch den Klimawandel ein. Katrin vom BUND Leipzig, Wildbienenkennerin und -liebhaberin, wird über das Thema Wildbienen und ihre Bedeutung für die Biodiversität im Ökosystem Stadt sprechen. Freuen Sie sich auf einen informativen Abend!</p><p>Der Eintritt ist frei.</p><p><a href="https://einewelt-leipzig.de/event/nord-trifft-sued/" target="_blank" rel="noreferrer">Weitere Informationen finden Sie hier.</a></p></div></div>

<!--TYPO3SEARCH_end-->