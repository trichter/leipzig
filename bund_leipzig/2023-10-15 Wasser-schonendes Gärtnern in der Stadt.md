---
id: wasser-schonendes-gaertnern-in-der-stadt
title: Wasser-schonendes Gärtnern in der Stadt
start: 2023-10-15 14:00
address: VAGaBUND Lene, Lene-Voigt-Park, 04317 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/wasser-schonendes-gaertnern-in-der-stadt/
image: csm_2019-10-20_VAGaBUND-Lene_Jana-Burmeister_DSCN9840_50b26c925b.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Bei einem kleinen Workshop liefert das Team des VaGaBUND Lene euch Impulse, damit euer Balkon im Sommer nicht auf dem Trockenen sitzen bleibt.</p></div></div>

<!--TYPO3SEARCH_end-->