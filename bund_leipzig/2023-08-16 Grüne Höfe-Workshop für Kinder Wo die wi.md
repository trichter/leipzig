---
id: gruene-hoefe-workshop-fuer-kinder-wo-die-wilden-bienen-wohnen
title: '"Grüne Höfe"-Workshop für Kinder: Wo die wilden Bienen wohnen'
start: 2023-08-16 15:00
address: Hildegarten im Bürgerbahnhof Plagwitz, gegenüber vom Bauspielplatz
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/gruene-hoefe-workshop-fuer-kinder-wo-die-wilden-bienen-wohnen/
image: csm_Biene_mit_Haus_und_Logo_e0fece4f74.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Wildbienen und Honigbienen haben einiges gemeinsam - und doch unterscheiden sie sich stark.</p><p>Während die Honigbiene häufig in menschgemachten Behausungen lebt und Staaten bildet, summt die Wildbiene einzeln herum und nutzt jeden noch so kleinen Winkel als Nistplatz. Da diese Winkel immer mehr verschwinden, wollen wir im Workshop <b>Nisthilfen bauen</b> und sie so unterstützen. Und weil ein Dach über dem Kopf nicht reicht, sondern auch für Wasser gesorgt sein muss, bauen wir außerdem <b>Insektentränken</b>, die auch anderen Kleinstlebewesen den Sommer erleichtern. Die Bauwerke sollen natürlich unbedingt mit in den heimischen Hof genommen werden!</p><p>Ganz nebenbei werden altersgerechte Informationen über den Lebensraum und die Bedeutung von Wildbienen und weiteren Hofbewohnern vermittelt.</p><p>Der Workshop richtet sich an Kinder der 1. - 6. Klasse, Eltern und Geschwisterkinder sind ebenfalls willkommen. Ein pünktliches Erscheinen um 15.00 ist keine Voraussetzung, das Angebot besteht fortlaufend bis ca. 18.00 Uhr.</p><p>Geheimtipp für alle Erwachsenen: Nutzt die Chance der bald anstehenden letzten "Grüne Höfe"-Pflanzperiode, meldet euch mit eurem Hof an und lasst euch mit bis zu 470 € pro Hof bezuschussen! Weitere Informationen unter <a href="http://www.bund-leipzig.de/gruenehoefe" target="_blank">www.bund-leipzig.de/gruenehoefe</a></p><h2 class="rte-heading rte-heading--h3">Mehr Informationen</h2><div class="rte-highlight rte-highlight--greenLight"><p>Eine Anmeldung an <a href="#" data-mailto-token="nbjmup+lpnnvojlbujpo.hsvfofipfgfAcvoe.mfjqajh/ef" data-mailto-vector="1">kommunikation-gruenehoefe(at)bund-leipzig.de</a> erleichtert uns das Einschätzen, ist aber nicht zwingend erforderlich. Das Angebot ist kostenfrei.</p></div></div></div>

<!--TYPO3SEARCH_end-->