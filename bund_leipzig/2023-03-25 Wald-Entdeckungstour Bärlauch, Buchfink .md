---
id: wald-entdeckungstour-baerlauch-buchfink-und-begegnung
title: "Wald-Entdeckungstour: Bärlauch, Buchfink und Begegnung"
start: 2023-03-25 11:00
address: Sachsenbrücke, Anton-Bruckner-Allee 50, 04107 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/wald-entdeckungstour-baerlauch-buchfink-und-begegnung/
image: csm_baerlauch_6b2077818e.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p class="MsoPlainText">Es ist faszinierend, was die Natur uns schenkt. Es gibt mehr als Bärlauch, ganz ohne Preis-Etikett. Der Frühjahrswald bietet Orte der Entspannung und der Naturschauspiele. Wenn Singvögel und andere Waldbewohner aktiv werden, die Bäume aber noch kein Blätterdach tragen, können wir in aller Ruhe entdecken. Der AK Postwachstum lädt ein, gemeinsam durch den Nonnenwald zu spazieren, ihn zu erkunden und uns dabei über das Geschenkte auszutauschen.&nbsp;</p><p class="MsoPlainText">Kommen Sie einfach vorbei! Wir freuen uns auf Sie.&nbsp;</p><p class="MsoPlainText">Treffpunkt: Beim Glücksbaum an der Sachsenbrücke, Clara-Zetkin-Park</p><h2 class="rte-heading rte-heading--h3">Mehr Informationen</h2><div class="rte-highlight rte-highlight--greenLight"><p>Weitere Informationen zum Arbeitskreis Postwachstum sind <a href="/ueber-uns/arbeitskreise/arbeitskreis-postwachstum/">hier</a> zu finden.</p></div></div></div>

<!--TYPO3SEARCH_end-->