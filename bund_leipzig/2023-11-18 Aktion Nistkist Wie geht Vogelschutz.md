---
id: aktion-nistkist-wie-geht-vogelschutz
title: "Aktion Nistkist: Wie geht Vogelschutz?"
start: 2023-11-18 10:16
address: Gelände von Columbus Junior in der Oststraße 181 b (Stötteritzer Wäldchen)
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/aktion-nistkist-wie-geht-vogelschutz/
image: csm_Vogelhaus__Gunnar_Warnecke__d153e66324.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Ein Tag zum Thema Vogelschutz für die ganze Familie! Werdet selbst aktiv und baut euer eigenes Vogelhaus! Wir stellen Materialien und Werkzeuge zur Verfügung und einen Experten. Lernt mit ihm mehr über unsere gefiederten Freunde und ihre Lebensräume. Für kleine Stärkungen ist gesorgt! Auch die jüngsten Vogelfreunde sind herzlich willkommen. Bitte beachtet, dass kleine Kinder in Begleitung von Erwachsenen sein sollten.</p><p></p><p>Freunde und Bekannte sind gern gesehen, bringt jede/n mit, der/die sich interessiert bei uns mitzumachen!</p><p>P.S.: Wer bei der organisation von diesem Projekt mitwirken möchte, kann gerne am 06.11.2023 um 18:00 Uhr im Heimatverein Berggut Holzhausen; Zuckelhausener Ring 17; 04288 Leipzig dazustoßen.</p></div></div>

<!--TYPO3SEARCH_end-->