---
id: klimaschutz-und-geschlechtergerechtigkeit
title: Klimaschutz und Geschlechtergerechtigkeit
start: 2023-10-26 16:00
end: 2023-10-26 18:00
address: Festsaal Neues Rathaus, Martin-Luther-Ring 4 - 6, 04109 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/klimaschutz-und-geschlechtergerechtigkeit/
image: csm_VA_Klimaschutz_und_Geschlechtergerechtigkeit_Sharepic_e68e0c0d07.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Am 30. Oktober 2019 beschloss der Leipziger Stadtrat die Ausrufung des Klimanotstandes. Infolgedessen entstand ein Sofortmaßnahmenprogramm für Leipzig. Mit dem zentralen Instrument des Leipziger Energie- und Klimaschutzprogramms 2030 (EKSP) will Leipzig den Weg zur klimaneutralen Stadt realisieren.</p><p>In keinem Strategiepapier sind Überlegungen zu finden, wie unterschiedlich der Klimawandel Frauen und Männer betrifft und wie unterschiedlich sie in Entscheidungsprozessen involviert sind, aber geschlechtergerechter Klimaschutz ist wichtig. An diesem Abend werden Expertinnen und Experten aus Wissenschaft, Kommunalpolitik und Verwaltung über dessen Umsetzung in Leipzig diskutieren.</p><p>Eingeladen sind Ulrike Röhr (GenderCC – Women for Climate Justice), Ariane Pflaum (Leiterin des Referats nachhaltige Entwicklung und Klimaschutz der Stadt Leipzig) und Sophia Kraft (Senior Business Analyst bei European Energy Exchange).</p><p>Nach den drei Inputs der Referentinnen freuen wir uns auf eine gemeinsame Diskussion mit Ihnen.</p><p></p><p>Um Anmeldung zur Veranstaltung an <a href="#" data-mailto-token="nbjmup+tvtboo/ibfofmAmfjqajh/ef" data-mailto-vector="1">susann.haenel(at)leipzig.de</a> wird gebeten.</p></div></div>

<!--TYPO3SEARCH_end-->