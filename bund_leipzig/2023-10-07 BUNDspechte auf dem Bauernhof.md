---
id: bundspechte-auf-dem-bauernhof
title: BUNDspechte auf dem Bauernhof
start: 2023-10-07 15:00
address: Dölitzer Wassermühle, Vollhardtstr. 16, 04279 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/bundspechte-auf-dem-bauernhof/
image: csm_Schafe__Julia_Becher__fc59f40c76.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Die Kindergruppe BUNDSpechte entdecken gemeinsam einen Bauernhof. Die Erlebnistour reicht von Schafen, Bienen, Hühnern und Katzen, bis hin zu Küchenkräutern. Wie wird aus Wolle ein Faden? Was machen die Bienen im Herbst? Wie bereiten sich die Tiere auf den nahenden Winter vor?</p><p>Um Anmeldung über ernte.dank@bundleipzig.de wird gebeten!</p></div></div>

<!--TYPO3SEARCH_end-->