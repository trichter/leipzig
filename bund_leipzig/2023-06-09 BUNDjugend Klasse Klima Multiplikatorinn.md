---
id: klasse-klima-workshop
title: "BUNDjugend: Klasse Klima Multiplikator*innen Workshop"
start: 2023-06-09 09:06
end: 2023-06-11 20:10
address: Café im Haus der Demokratie, Bernhard-Göring-Str. 152, 04277 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/klasse-klima-workshop/
image: klasse-klima-logo-2.png
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Du möchtest Workshops zu den Themen Klimawandel, Klimaschutz oder Klimagerechtigkeit gestalten? Vom 09. bis zum 11. Juni bietet die BUNDjugend Leipzig eine Multiplikator*innen Schulung für junge Menschen zwischen 18 und 27 Jahren an. Ziel des Workshopwochenendes ist es, die nötigen Methoden und Inhalte zu erlernen, um eigene Projekttage und Workshops an Schulen durchführen zu können.</p><p>Zusammen mit 19 weiteren Teilnehmer*innen wirst du einen tieferen Einblick in Klimabildungskonzepte bekommen, deine pädagogischen Kompetenzen schärfen und dein Wissen zum Thema Klimaschutz auffrischen und erweitern. Außerdem werden wir das Material für Klasse Klima direkt ausprobieren, damit du erfährst, wie es sich anfühlt, Multiplikator*in zu sein.Themenschwerpunkte sind Klimagerechtigkeit, Klimawandel, Klimaschutz, Umweltpsychologie und Projektmethoden.</p><h2 class="rte-heading rte-heading--h3">Mehr Informationen</h2><div class="rte-highlight rte-highlight--greenLight"><p>Die Anmeldung erfolgt über das <a href="/workshop-klasse-klima/">Anmeldeformular</a>.</p><p>Fragen zum Workshop können an umweltpaedagogik(at)bund-leipzig.de gestellt werden.</p></div></div></div>

<!--TYPO3SEARCH_end-->