---
id: bundjugend-online-workshop-mit-farn-ev-die-extreme-rechte-zwischen-klimawandelleugnung-und-klimanationalismus
title: 'BUNDjugend: Online Workshop mit FARN e.V. "Die extreme Rechte zwischen
  Klimawandelleugnung und Klimanationalismus"'
start: 2023-10-24 19:00
address: "Internet: Link wird durchgegeben"
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/bundjugend-online-workshop-mit-farn-ev-die-extreme-rechte-zwischen-klimawandelleugnung-und-klimanationalismus/
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p>Extrem rechte Akteur*innen bestreiten entweder den anthropogen verursachten Klimawandel oder sie sehen die Ursache für die Klimakatastrophe im Bevölkerungswachstum des globalen Südens. Je nach Adressat*innen bedienen sich diese Akteur*innen unterschiedlicher Strategien. In einem online Workshop mit dem FARN e.V. wollen wir (extrem) rechte Akteur*innen und deren Positionen im Themenfeld Klima- und Energiepolitik kennenlernen und eine solidarische Gegenperspektive der Klimagerechtigkeit aufzeigen.</p><p>Interessierte Menschen bitten wir um eine Anmeldung über die E-Mail-Adresse<b> bundjugend@bund-leipzig.de</b>.</p></div></div>

<!--TYPO3SEARCH_end-->