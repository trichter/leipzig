---
id: infoveranstaltung-gruene-hoefe-alles-zu-anmeldung-und-teilnahme-am-projekt
title: Infoveranstaltung "Grüne Höfe" - Alles zu Anmeldung und Teilnahme am Projekt
start: 2023-03-29 17:00
end: 2023-03-29 19:00
address: galerie KUB, Kantstraße 18, 04275 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/infoveranstaltung-gruene-hoefe-alles-zu-anmeldung-und-teilnahme-am-projekt/
image: csm_220824_Gruene_Hoefe_Logo_52ecc21785.png
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Klimatische Veränderungen und bauliche Verdichtung machen der&nbsp;Stadtnatur das Leben schwer. Indem wir unsere Höfe und Grünflächen ökologisch aufwerten und naturnah umstrukturieren, können wir einen Teil dazu beitragen, die einheimische Tier- und Pflanzenvielfalt zu fördern und ausgleichend auf das Stadtklima einzuwirken. Es entstehen wertvolle und nachbarschaftliche Orte für Mensch und Tier. Räume der Erholung und Naturerfahrung für die einen und essenzielle Räume zum Leben für die anderen. Diesen Entstehungsprozess zu begleiten und zu unterstützen, ist das Ziel des Projektes "Grüne Höfe" von der BUND Regionalgruppe Leipzig.</p><p>In dieser Informationsveranstaltung erfahren Interessierte, wie sie Teil des Projektes werden, was es zu beachten gilt und welche Angebote wir als Projektteam machen können.</p><p>Das Projekt "Grüne Höfe" richtet sich sowohl an Haus- und Mietgemeinschaften als auch an Hauseigentümer*innen und Wohnungsbaugenossenschaften. Mehr Informationen zum Projekt: <a href="http://www.bund-leipzig.de/gruenehoefe" target="_blank">www.bund-leipzig.de/gruenehoefe</a></p><p>Machen wir uns gemeinsam für die Stadtnatur stark!</p><h2 class="rte-heading rte-heading--h3">Mehr Informationen</h2><div class="rte-highlight rte-highlight--greenLight"><p>Um Anmeldung wird gebeten an <a href="#" data-mailto-token="nbjmup+lpnnvojlbujpo.hsvfofipfgfAcvoe.mfjqajh/ef" data-mailto-vector="1">kommunikation-gruenehoefe(at)bund-leipzig.de</a></p></div></div></div>

<!--TYPO3SEARCH_end-->