---
id: gruene-hoefe-pflanzaktion-mit-dem-internationalen-bund-ib
title: '"Grüne Höfe" Pflanzaktion mit dem Internationalen Bund (IB)'
start: 2023-10-25 13:00
end: 2023-10-25 15:00
address: Gräfestraße 23, Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/gruene-hoefe-pflanzaktion-mit-dem-internationalen-bund-ib/
image: csm_Kiju_Gruenau_Vorher_Nachher_1_Nicole_Bruehl_7fe4faa0e6.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Der <b>Internationale Bund (IB) Freier Träger der Jugend-, Sozial- und Bildungsarbeit e.V. </b>wertet seinen Vereinsitz innerhalb eines Teamtags ökologisch auf. Wir unterstützen dabei und laden ausdrücklich weitere Interessierte zum Ideen holen und Mitmachen ein!</p><p>Zusammen wollen wir eine Wildblumenwiese anlegen (viele Hände, schnelles Ende) und ein paar Stauden sowie Gräser an schattigen Standorten platzieren. Ganz nebenbei erläutern wir die Grundprinzipien der naturnahen Hofgestaltung. Wie immer gilt: Bitte achtet darauf, Gartenkleidung anzuziehen!</p><p>Achtung: Ihr wollt euren Hof auch fachkundig begrünen und dafür finanziell gefördert werden? Der Endspurt läuft beim <a href="/themen-und-projekte/natur-und-artenschutz/gruene-hoefe/">Projekt "Grüne Höfe"</a> - jetzt noch anmelden und bis Ende November&nbsp;den Hof gestalten!</p><h2 class="rte-heading rte-heading--h3">Mehr Informationen</h2><div class="rte-highlight rte-highlight--greenLight"><p>Eine Anmeldung wird erbeten unter <a href="#" data-mailto-token="nbjmup+lpnnvojlbujpo.hsvfofipfgfAcvoe.mfjqajh/ef" data-mailto-vector="1">kommunikation-gruenehoefe(at)bund-leipzig.de</a></p><p>Die Pflanzaktion ist kostenfrei.</p></div></div></div>

<!--TYPO3SEARCH_end-->