---
id: klimagerechtigkeit
title: Klimagerechtigkeit
start: 2023-10-13 17:00
address: Café im Haus der Demokratie, Bernhard-Göring-Str. 152, 04277 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/klimagerechtigkeit/
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p>Klimagerechtigkeit, was ist das eigentlich? Was für eine Verantwortung hat der globale Norden beim Klimakollaps? Was für Folgen hat es für eine marginalisierte Gruppe, wenn Hitzeperioden und Überschwemmungen ganze Ernteperioden zerstören? In einem Workshop wollen wir gemeinsam diesen Fragen nachgehen.</p></div></div>

<!--TYPO3SEARCH_end-->