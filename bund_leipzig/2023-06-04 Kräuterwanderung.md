---
id: kraeuterwanderung
title: Kräuterwanderung
start: 2023-06-04 12:00
end: 2023-06-04 18:00
address: Eingangsbereich Sommerbad, Kleinzschocher – Küchenholzallee 75, 04229 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/kraeuterwanderung/
image: Wildkraeuter-Leipzig.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Liebe Menschen,</p><p>der BUND Sachsen nimmt Sie mit auf eine Entdeckungsreise in die Welt der Wildkräuter und Wurzeln! Am Sonntag, den 04. Juni, wollen wir mit Ihnen zusammenkommen und uns austauschen! Wir gehen auf Kräuterwanderung mit anschließendem Workshop. Wir sammeln einen kraftvollen Kräuterstrauß und erfahren um die Verwendungsmöglichkeiten der Pflanzen u. a. für die Haut, in Salben, Ölen und anderen Essenzen. Für Daheim bereiten wir ein duftendes Kräuterparfüm zu. Referent ist&nbsp;<a href="https://wildekraeuterey.de/index.php/ueber-mich" target="_blank" rel="noreferrer">André Freymann</a>, ein Baumfreund, Kräuterspezialist und Heilpflanzenkundler.</p><p>Bitte bringen Sie Sammelbehältnisse, ggf. kleine Pflanzschaufel, Sitzunterlage, wetterangepasste Kleidung, eigene Getränke, ein 100-150ml Schraubdeckelglas, ein Schneidebrett und ein scharfes Schneidemesser mit. Für ein leckeres Picknick um 12 Uhr wird gesorgt. Die Kräuterwanderung geht dann um 14 Uhr los und wird zwei Stunden dauern.</p><p>Wir freuen uns auf Sie!</p><p><b>Melden Sie sich <a href="https://www.bund-sachsen.de/bundakademie/anmeldung-bund-sachsen-veranstaltungen/" target="_blank">hier </a>an</b></p><h2 class="rte-heading rte-heading--h3">Mehr Informationen</h2><div class="rte-highlight rte-highlight--greenLight"><p>Bitte melden Sie sich mit diesem <a href="https://www.bund-sachsen.de/mitmachen/unterstuetzung-fuer-bund-gruppen/anmeldung-bund-akademie/" target="_blank">Formular</a> an. Die Veranstaltung kostet 15€. Für BUND-Mitglieder ist sie kostenlos.&nbsp;</p></div></div></div>

<!--TYPO3SEARCH_end-->