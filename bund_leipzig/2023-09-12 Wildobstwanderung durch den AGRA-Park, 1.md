---
id: wildobstwanderung-durch-den-agra-park-16-uhr
title: Wildobstwanderung durch den AGRA-Park, 16 Uhr
start: 2023-09-12 16:00
address: Dölitzer Wassermühle, Vollhardtstr. 16, 04279 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/wildobstwanderung-durch-den-agra-park-16-uhr/
image: csm_Obstkorb_2f2c50f69b.jpg
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p>Wildobstwanderung durch den AGRA-Park mit&nbsp;Katrin Erben und Urte Grauwinkel</p><p>Wir entdecken Beeren, Nüsse und Eßbares an Bäumen und Sträuchern am Wegesrand. Dazu gibt es Wissenswertes und Geschichte rund um die Pflanzen und den Park. Anschließend findet eine Verkostung mit Wildobstspezialitäten statt.</p></div></div>

<!--TYPO3SEARCH_end-->