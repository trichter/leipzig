---
id: kindergruppe-bundspechte-1530-uhr-2
title: +++fällt aus!+++Kindergruppe BUNDspechte, 15 Uhr
start: 2023-12-16 15:00
end: 2023-12-16 17:00
address: Stötteritzer Wäldchen – Kletterspielplatz, Oberdorfstraße
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/kindergruppe-bundspechte-1530-uhr-2/
image: IMG_5535_Antonia_Kern_..JPG
isCrawled: true
---
<!--TYPO3SEARCH_begin-->

<div class="das-hier-ist-column-main"><div id="c347"><p></p><p class="western">***english version below***</p><p class="western">Was ist eigentlich ein Wald? Was macht ihn so besonders? Welche Tiere und Pflanzen finden wir hier? Und was bedeutet er für uns Menschen?</p><p>Die <a href="/kindergruppe-bundspechte/" target="_top">BUNDspechte</a> sind Kinder im Alter von 5 bis 10 Jahren, die sich im Stadtwald bewegen. Gemeinsam begeben sie sich auf spannende Entdeckungsreisen durch den Wald und lernen Pflanzen, Tiere und Waldmythen kennen. Der <a href="/arbeitskreis-umweltpaedagogik/">Arbeitskreis Umweltpädagogik</a> freut sich insbesondere auch über Kinder mit Fluchterfahrung, deren Eltern und Vertrauenspersonen gerne mitmachen können. Eine Verdolmetschung kann leider nicht garantiert werden.</p><p><b>Treffpunkt: </b>Stötteritzer Wäldchen – Kletterspielplatz, Oberdorfstraße</p><p><b>Anmeldung </b>erforderlich bei umweltpaedagogik(at)bund-leipzig.de bis Freitags 15 Uhr</p><p>Es wird um eine Spende gebeten.</p><p>***english version***</p><p>What is a forest? What makes it so special? What animals and plants can we find here? And what does it mean for us humans?</p><p>The BUNDspechte is a group for children between the ages of 5 and 10, who spend time in the city's forest. Together they go on exciting journeys of discovery through the forest and learn about plants, animals and forest myths. The Environmental Education Working Group is especially happy to welcome children with refugee experience, whose parents and confidants are welcome to join in. Unfortunately a translation cannot be guaranteed.</p><p><b>Meeting place: </b>Stötteritzer Wäldchen – Kletterspielplatz, Oberdorfstraße</p><p><b>Registration </b>required at umweltpaedagogik(at)bund-leipzig.de until Friday 3pm</p><p>A donation is kindly requested.</p></div></div>

<!--TYPO3SEARCH_end-->