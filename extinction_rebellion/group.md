---
name: Extinction Rebellion Leipzig
website: https://extinctionrebellion.de/og/leipzig/

scrape:
    source: facebook
    options:
        page_id: 349843872526521
---
Wir organisieren gewaltfreie Aktionen des zivilen Ungehorsams, weil unsere Regierungen bis jetzt nicht genug gegen Klimakrise und Artensterben tun.
