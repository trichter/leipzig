---
id: "4295"
title: Empowerment für tina* Personen
start: 2023-06-10 12:00
end: 2023-06-10 15:00
address: Index, Breite Straße 1, Leipzig
link: https://www.planlos-leipzig.org/events/empowerment-fuer-tina-personen/
isCrawled: true
---
Im Rahmen der Queering Defaults Pride 2023

+ mit Anmeldung/ with registration +
+ English below + 

Im Empowerment-Workshop wollen wir einen geschützten Raum schaffen, den wir gemeinsam gestalten und nach unseren Bedürfnissen mit Inhalten füllen. Nach einer anfänglichen Erwartungsabfrage gibt es verschiedene Möglichkeiten für die Teilnehmenden sich mit Gefühlen, Erfahrungen und Bedürfnissen als marginalisierte Personen auseinanderzusetzen. Mit einer Kreativ-Methode wollen wir uns dann mit den vielen schönen Aspekten unserer Identität beschäftigen.
Der Workshop richtet sich an inter* Personen und Personen, die sich nicht oder nur teilweise mit dem Geschlecht identifizieren, dass ihnen bei der Geburt zugewiesen wurde oder dieses hinterfragen.

Bei Interesse kannst Du Dich über empowerment_ws@posteo.de anmelden.
Gib bitte Deinen Namen (und ggf. Pronomen) an und ob du bestimmte Bedürfnisse hast. Freddie meldet sich dann mit weiteren Infos bei dir. Wenn Du Dich fragst, ob Du in diesem Workshop richtig bist, kannst Du Dich ebenso an Freddie wenden. Für den Workshop gibt es 15 Plätze.
Freddie (keins/er) ist seit einer Weile neben Studium und Lohnarbeit als politischer Bildnerin tätig. Sein Fokus liegt auf queeren Themen, der Sensibilisierung Nicht-Betroffener und dem Empowerment Betroffener. Freddie ist queer und trans*, weiß und able-bodied.
Empowerment for tina* Persons

ENG:
In this Empowerment workshop we want to create a safer space that we shape together according to our needs.
After talking about our expectations for the day there are different options for the participants to reflect on emotions, experiences and needs as a marginalized person.
We also want to look at the many beautiful aspects of our identity with a creative method.
The workshop is aimed at inter* people and people who do not or only partially identify with the gender they were assigned at birth or who are questioning it.

If you are interested send an e-mail to empowerment_ws@posteo.de.
Please include your name (and pronouns if wanted) and if you have any specific needs. Freddie will contact you with more information afterwards. If you are wondering whether this workshop is for you, you can also contact Freddie. The workshop space is limited to 15 people.
Freddie (they/he) has been working in civic education for some time now while studying at uni and doing wagework. He focusses on queer topics, the sensitizing of non-affected and the empowerment of affected people. Freddie is queer and trans*, white and able-bodied.

Sprachen/Languages:
Deutsch und Englisch simultan
German and English simultaniously

Barrieren/ Accessibility:
Raum und Toiletten Rollstuhl zugänglich
Room and toilets wheelchair accessible

https://queeringdefaults.noblogs.org/pride-2023/

https://www.planlos-leipzig.org/events/empowerment-fuer-tina-personen/