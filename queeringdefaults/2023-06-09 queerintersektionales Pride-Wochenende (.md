---
id: "3963"
title: queerintersektionales Pride-Wochenende (Demo)
start: 2023-06-09 00:00
end: 2023-06-12 00:00
link: https://www.planlos-leipzig.org/events/queerintersektionales-pride-wochenende-demo/
isCrawled: true
---
english below / français en bas

Im Rahmen der vierten Pride der queer-intersektionalen Gruppe Queering Defaults wird am zweiten Juniwochenende 2023 wieder die radikale Pride-Demo in Leipzig stattfinden. Wir sind fleißig am vorbereiten und es ist gut möglich, dass euch am Demo-Wochenende noch weitere Veranstaltungen erwarten! Weitere Infos werden wir veröffentlichen, sobald die Planung weiter fortgeschritten ist. But save the dates!

2.-4.6.2023 - Workshops, Vorträge, Panel, Hang-Outs ...

9.-11.6.2023 - Demo-Wochenende, ggf. mit weiterem Programm

So, 11.6.2023 - radikale intersektionale Pride-Demo

 

eng

As part of the fourth Pride of the queer-intersectional group Queering Defaults, the annual radical Pride Demonstration will again take place in Leipzig on the second weekend of June 2023. We are busy preparing and it is quite possible that you can expect more events during the demo weekend! We will publish more information as soon as the planning is further advanced. But save the dates!

2-4.6.2023 - Workshops, lectures, panel, hang-outs …

9-11.6.2023 - weekend of the demonstration, possibly with further programme.

Sun, 11.6.2023 - radical intersectional Pride demonstration

 

fr

Dans le cadre de la quatrième Pride du groupe queer intersectionnel Queering Defaults, la manif radical du Pride aura à nouveau lieu à Leipzig le deuxième week-end de juin 2023. Nous préparons activement les événements et il est fort possible que d'autres ateliers ou soirées vous attendent pendant le week-end de la manifestation ! Nous publierons plus d'informations dès que la planification sera plus avancée. Mais réservez les dates !

2-4.6.2023 - Ateliers, soirées, discussion/débats, hang-outs ...

9-11.6.2023 - Week-end de la manifestation, éventuellement avec plus de programme.

Di, 11.6.2023 - manif intersectionnelle radicale de la Pride

https://queeringdefaults.noblogs.org/

https://www.planlos-leipzig.org/events/queerintersektionales-pride-wochenende-demo/