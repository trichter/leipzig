---
id: "4336"
title: Intersektionale Pride-Demo
start: 2023-06-11 13:00
end: 2023-06-11 13:00
address: Augustusplatz, Augustusplatz, Leipzig
link: https://www.planlos-leipzig.org/events/intersektionale-pride-demo/
isCrawled: true
---
The future is intersectional – queer movements
(Die Zukunft ist intersektional – Queere Bewegungen)
Wir laden euch ein zur 4. intersektionalen Pride-Demo von Queering Defaults!
Nehmt euch mit uns die Straße. Wir wollen eine politische und empowernde Pride-Demo. Wir wollen sichtbar sein, laut werden und uns nichts vorschreiben lassen. Wir wollen gemeinsam glücklich, traurig, hoffnungsvoll oder wütend sein…
Hier sind ein paar erste Infos – weitere Details zur Route, Zeiten usw. kommen in den nächsten Tagen!
Wir freuen uns auf euch!

Wann?
Sonntag, 11.6.2023, ab 13 Uhr

Wo?
Start: Augustusplatz
Ende: Wiese im Clara-Zetkin-Park

Was?
Intersektionale Demonstration mit Musik und Redebeiträgen auf den Zwischenkundgebungen. Am Ende gibt es Performances und gemeinsames Ausklingen lassen im Park.

Barrieren?
Die ganze Route ist ebenerdig befahrbar. Wir achten darauf, dass wir langsam laufen.
Es wird mind. ein Auto geben, in dem Menschen mitfahren können, wenn sie eine Pause brauchen (befahrbar mit Rampe).
Es gibt einen Lautsprecherwagen im vorderen Drittel, über den Musik abgespielt wird während der Demo.
Es wird Deutsche Gebärdensprache (DGS)-Dolmetschung geben. Die Moderation und Ansagen auf der Demo sind auf Deutsch und Englisch.
Alle Beiträge sind auf Deutsch oder Englisch. Online können die Redebeiträge in mehreren Sprachen live mitgelesen werden.

Mehr Infos: @queeringdefaults (Insta) /
queeringdefaults.noblogs.org

https://queeringdefaults.noblogs.org/post/2023/05/24/demo-info-2023/

https://www.planlos-leipzig.org/events/intersektionale-pride-demo/