---
name:  Queering Defaults
website: https://queeringdefaults.noblogs.org/
email: qd-contact@riseup.net
scrape:
  source: ical
  options:
    url: https://www.planlos-leipzig.org/events.ics
  filter:
    description: queeringdefaults.noblogs.org
---
Wir geben Intersektionalität eine queere Perspektive.
Wir sind eine unabhängige selbstorganisierte Gruppe, die queerpolitische Arbeit
leistet, zum Beispiel in Form von Workshops, Panels, Demonstrationen und über
Social Media.
Wir wollen Normstrukturen in Frage stellen, widersprüchliche Positionen aushalten
lernen und produktive Auseinandersetzungen führen, die in solidarische Praxen
münden sollen.
Wir wollen ein inklusiver Safer Space sein.
Unsere Ziele und unser Selbstverständnis sind dabei eine Momentaufnahme, Teil
eines ständigen Reflexionsprozesses und offen für Weiterentwicklung.