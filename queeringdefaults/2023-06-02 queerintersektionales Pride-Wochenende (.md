---
id: "3962"
title: queerintersektionales Pride-Wochenende (WS, Vorträge...)
start: 2023-06-02 00:00
end: 2023-06-05 00:00
link: https://www.planlos-leipzig.org/events/queerintersektionales-pride-wochenende-ws-vortraege/
isCrawled: true
---
english below / français en bas

Im Rahmen der vierten Pride der queer-intersektionalen Gruppe Queering Defaults werden am ersten Juniwochenende 2023 wieder verschiedene Workshops und Vorträge in Leipzig stattfinden. Auch in diesem Jahr werden queere Themen aus einem intersektionalen Blickwinkel betrachtet. Weitere Informationen zu konkreten Inhalten, Zeiten und Orten werden wir veröffentlichen, sobald die Planung weiter fortgeschritten ist. But save the dates!

2.-4.6.2023 - Workshops, Vorträge, Panel, Hang-Outs ...

9.-11.6.2023 - Demo-Wochenende, ggf. mit weiterem Programm

So, 11.6.2023 - radikale intersektionale Pride-Demo

 

eng

As part of the fourth Pride of the queer-intersectional group Queering Defaults, various workshops and lectures will again take place in Leipzig on the first weekend of June 2023. As every year, queer topics will be looked at with an intersectional view. We will publish more information on concrete contents, times and locations as soon as the planning is further advanced. But save the dates!

2-4.6.2023 - Workshops, lectures, panel, hang-outs ...

9-11.6.2023 - weekend of the demonstration, possibly with further programme.

Sun, 11.6.2023 - radical intersectional Pride demonstration

 

fr

Dans le cadre de la quatrième Pride du groupe queer intersectionnel Queering Defaults, différents ateliers et conférences auront à nouveau lieu à Leipzig le premier week-end de juin 2023. Cette année encore, les thèmes queer seront abordé.e.s sous avec un regard intersectionnel. Nous publierons plus d'informations sur le contenu concret, les horaires et les lieux dès que la planification sera plus avancée. Mais réservez les dates !

2-4.6.2023 - Ateliers, soirées, discussion/débats, hang-outs ...

9-11.6.2023 - Week-end de la manifestation, éventuellement avec plus de programme.

Di, 11.6.2023 - manif intersectionnelle radicale de la Pride

https://queeringdefaults.noblogs.org/

https://www.planlos-leipzig.org/events/queerintersektionales-pride-wochenende-ws-vortraege/