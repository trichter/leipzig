---
name: FAU Leipzig
website: https://leipzig.fau.org/
email: kontakt.leipzig@fau.org
scrape:
  source: ical
  options:
    url: https://leipzig.fau.org/events.ics
---