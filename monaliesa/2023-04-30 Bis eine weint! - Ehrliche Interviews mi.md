---
id: "882821533017031"
title: Bis eine* weint! - Ehrliche Interviews mit Müttern
start: 2023-04-30 15:00
locationName: MONAliesA
address: Bernhard-Göring-Straße 152, Leipzig-Connewitz
link: https://www.facebook.com/events/882821533017031/
image: 338161156_790481475674954_4982066144224997_n.jpg
isCrawled: true
---
Bis eine* weint! - Ehrliche Interviews mit Müttern zu Gleichberechtigung, Care-Arbeit und Rollenbildern

Die Autorinnen Nicole Noller und Natalie Stanczak geben Müttern* eine Plattform und machen sichtbar, welchen Herausforderungen sie gegenüberstehen. Sie zeigen in ihrem Buch unterschiedliche Lebensrealitäten, ohne zu bewerten. 17 verschiedene Mütter, die ihren eigenen Weg gehen und uns mitnehmen in ihren Alltag, uns inspirieren und anregen, den Status Quo zu hinterfragen, die Forderungen stellen und ihren täglichen Struggle nicht verstecken. Egal, ob Muslima, queer, Mom of Color, Hauptverdienerin, DJane, Hebamme oder Vollzeit-Mutter – sie alle haben etwas zu sagen und sie alle wollen wir hören!

Die Lesung mit Gespräch findet im Rahmen von Leipzig liest statt.
Der Eintritt ist kostenlos.