---
id: "869178540939973"
title: "Sarah Diehl: Die Freiheit, allein zu sein (Lesung + Gespräch)"
start: 2023-04-28 18:00
locationName: MONAliesA
address: Bernhard-Göring-Straße 152, Leipzig-Connewitz
link: https://www.facebook.com/events/869178540939973/
image: 335932454_723786959530422_7697806596433118940_n.jpg
isCrawled: true
---
Im heutigen Strudel aus Karriere- und Familienplanung werden wir daran gehindert, ein gutes Verhältnis zum Alleinsein zu entwickeln. Dabei brauchen wir es unbedingt, um uns äußeren Erwartungshaltungen zu entziehen und uns mit uns selbst komplett zu fühlen. Zentral ist für Sarah Diehl, dass Alleinsein nicht nur als elementarer Teil der Selbstfürsorge essentiell und absolut positiv ist, sondern dass es als wesentliche Triebfeder für Veränderung auch für das gesellschaftliche Miteinander Bedeutung hat.

Diehl blickt auf die Bedeutung des Alleinseins innerhalb der Familie oder Partnerschaft, in der kreativen und politischen Arbeit, wie in der Natur oder auf Reisen und ermutigt, das Alleinsein immer wieder bewusst zu suchen, um keine Selbstzensur der eigenen Bedürfnisse für die Leistungsgesellschaft oder für Familien- oder Liebesideale zu betreiben.

Sarah Diehl lebt als Autorin und Aktivistin in Berlin und fühlt sich in der Politik ebenso zu Hause wie im Literarischen. Sie engagiert sich seit 15 Jahren im Bereich der internationalen reproduktiven Rechte, ist Mitbegründerin der Organisation Ciocia Basia und gibt Seminare zum Thema "Will ich Kinder?“. Letzte Veröffentlichungen: “Die Uhr, die nicht tickt” und “Die Freiheit, allein zu sein”.

Eine Kooperation der Buchhandlung drift und der MONAliesA – Feministische Bibliothek und Archiv.

Im Rahmen von Leipzig liest. 

Für die Veranstaltung gibt es eine Maximalbegrenzung der Teilnehmer*innenanzahl. Platzreservierungen sind nicht möglich. Danke für euer Verständnis! 

Über Spenden freuen wir uns. Spendenempfehlung: 2–10 EUR