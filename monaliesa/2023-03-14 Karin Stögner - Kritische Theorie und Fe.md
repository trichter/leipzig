---
id: "6039429976134882"
title: Karin Stögner - Kritische Theorie und Feminismus
start: 2023-03-14 19:30
locationName: MONAliesA
address: Bernhard-Göring-Straße 152, Leipzig-Connewitz
link: https://www.facebook.com/events/6039429976134882/
image: 326527945_618817823411433_7454066789980474296_n.jpg
isCrawled: true
---
Karin Stögner widmet sich in ihrem Vortrag dem Verhältnis von Kritischer Theorie und Feminismus und geht auf die jeweiligen Rezeptionshintergründe ein. Zudem skizziert sie Fragestellungen einer aktuellen feministischen Kritischen Theorie und bezieht sich dabei insbesondere auf Intersektionalität, Identitätspolitik und Autoritarismus heute.

Für die Veranstaltung gilt eine Maximalbegrenzung der Teilnehmendenanzahl. Danke für euer Verständnis und Mitwirken!
Über Spenden freuen wir uns. Spendenempfehlung: 2–10 EUR