---
id: "3503566813260807"
title: "Bis eine* weint - Lesung und Gespräch "
start: 2023-04-30 15:00
locationName: MONAliesA
address: Bernhard-Göring-Straße 152, Leipzig-Connewitz
link: https://www.facebook.com/events/3503566813260807/
image: 336752430_211825148103975_7110770234280165980_n.jpg
isCrawled: true
---
Bis eine* weint! - Lesung und Gespräch 
über Mutterschaft, strukturelle Ungleichheiten und eine gerechtere Ausgestaltung von Fürsorge

Autorinnen Nicole Noller und Natalie Stanczack im Gespräch mit der Bundestagsabgeordneten Nadja Sthamer

Wann: 30. April 2023, 15 Uhr 
Wo: MONAliesA – Feministische Bibliothek und Archiv Leipzig

Eintritt frei!

Rückfragen und unverbindliche Anmeldung per Mail an leipzig@nadja-sthamer.de 

---
Im Rahmen von „Leipzig liest“ lädt Nadja Sthamer, MdB, die Autorinnen Nicole Noller und Natalie Stanczak des Leipziger Verlags Palomaa Publishing dazu ein, über ihr Buch „Bis eine* weint! -Ehrliche Interviews mit Müttern zu Gleichberechtigung, Care-Arbeit und Rollenbildern" ins Gespräch zu kommen.

Gemeinsam werfen sie einen Blick auf unterschiedliche Lebensrealitäten von Müttern* und laden anschließend zur Diskussion über die Vereinbarung von Familie und Beruf, gesellschaftlichen Erwartungen und die Überwindung struktureller Ungleichheiten ein. 
Dabei widmen sie sich insbesondere der Frage, welche politischen Rahmenbedingungen geschaffen werden müssen, um eine gerechtere Ausgestaltung von Sorgearbeit zu ermöglichen. 

„Bis eine* weint“ ist kein Rosa-Happy-Mama*-Buch. Es ist ein ehrliches Manifest. Ein Manifest für die Vielfalt im Leben von Müttern*. Ein Manifest für die Liebe zwischen Eltern und Kindern. Ein Manifest für ehrliche Gespräche über die ganze Fülle des Mutter*seins. Es zeigt 17 verschiedene Mütter, die ihren eigenen Weg gehen und uns mitnehmen in ihren Alltag, uns inspirieren und anregen, den Status Quo zu hinterfragen, die Forderungen stellen und ihren täglichen Struggle nicht verstecken. Egal, ob Muslima, queer, Mom of Color, Hauptverdienerin, DJane, Hebamme oder Vollzeit-Mutter – sie alle haben etwas zu sagen und sie alle wollen wir hören! 
---