---
id: vMVwRvQh
title: "Wirklichkeit [endlich] begreifen: Frauen* entsprechend ehren, Jetzt! "
start: 2024-01-18 17:00
end: 2024-01-18 20:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
ERÖFFNUNG EINER GANZ BESONDEREN AUSSTELLUNG

## **12 Ehrenbürgerinnen *in spe*** 

*für 2024, 2025, 2026, 2027, 2028, 2029 …* *… auch als Vorschläge & Inputs für ein sich demokratisch progressiv weiterentwickelndes Leipzig*

**Seit 1832** hat die Stadt Leipzig insgesamt **90x das Ehrenbürger:innen-Recht verliehen** – **89x Männer, 1x, ein einziges an eine Frau**. Als eine der höchsten städtischen Auszeichnungen spiegeln sich hier auch gesellschaftliche Macht-/Strukturen der jeweiligen Zeit – und das patriarchale Gesellschaftsstrukturen in Leipzig bis heute, bis ins 21. Jahrhundert noch immer existieren.

Diese seit über 190 Jahren so deutliche städtische Nichtachtung der unzähligen hervorragenden Leistungen von Frauen\* ist nicht nur erschütternd & empörend, sondern ist eine Nichtachtung unserer demokratischen Rechtsstaatlichkeit. Leipzig als eine Stadt, in der seit Anbeginn städtischer Existenz Frauen\* öffentlich sehr aktiv tätig waren & sind – in einer Stadt, in der Frauen gesellschaftlich vieles mit-/ angeschoben haben & „am Laufen halten“ – und sich „in herausragender Weise“ für das städtische Gemeinwesen verdient gemacht haben – muss dieser Missstand endlich korrigiert werden.

Die AG Frauen\*Projekte Leipzig ist in diesem Kontext aktiv geworden: Von April 2023 bis März 2024 wurden/werden monatlich Vorschläge für Ehrenbürgerinnen\* bei der Stadt Leipzig eingereicht. Und um dieses starke Ungleichgewicht öffentlicher Würdigung – die Leistungen von Frauen\* betreffend – noch deutlicher sichtbar zu machen, wurde/wird zu jeder aktuellen Einreichung eine weitere Frau\* posthum vorgeschlagen, die diese Ehrung der Stadt Leipzig ebenfalls verdient gehabt hätte. [https://agfrauenprojekte-leipzig.com/ehrenbuergerinnen-der-stadt-leipzig/](<https://agfrauenprojekte-leipzig.com/ehrenbuergerinnen-der-stadt-leipzig/>)

## In dieser Ausstellung werden alle für die Ehrenbürgerinnen-Würde [ *in spe & posthum* ] vorgeschlagenen 24 Frauen vorgestellt. 

Zur Ausstellungseröffnung sind alle Interessierten ganz herzlich eingeladen.