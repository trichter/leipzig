---
id: kCo0JXJ4
title: Frauen* als Role-Models in Informatik, Naturwissenschaft & Kunst
start: 2023-06-22 19:00
end: 2023-06-22 20:30
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
AUSSTELLUNG TWO IN ONE (2 in 1):

Der Berufs-Anteil von Frauen\* im MINT-Bereich (Mathematik, Informatik, Naturwissenschaft, Technik) liegt in Deutschland bei unter 20%. Auch in Politik, Kunst und anderen Bereichen der Gesellschaft sind immer noch weit weniger als 50% Frauen präsent, obwohl sie die Hälfte der Bevölkerung bilden.

## Diese Doppelausstellung porträtiert auf sehr unterschiedliche Weise Frauen\* als Role-Models in Naturwissenschaft & Kunst…

**Frauen in Naturwissenschaft & Kunst**

 CONSTANZE GUHR, Autorin / Illustratorin und Künstlerin, Berlin

 Zeichnungen \| Aquarell, Brushpen, Fineliner

Ihre eigenen Themen sind Kreativität, Nachhaltigkeit und Empowerment… sie begeistert sich für wunderbare und kreative Frauen aus Naturwissenschaft, Politik und Kunst. Entstanden ist eine Porträtreihe zu Frauen, die sie faszinieren oder inspirieren aufgrund ihres Handelns, ihrer Aussagen. Viele dieser Frauen hatten ein bewegtes Leben. waren mutig, haben Neues gewagt und entdeckt oder sich gegen Widerstände durchgesetzt. Entstanden ist so eine Porträtreihe zu Künstlerinnen und Politikerinnen u.a. Mascha Kaleko, Rosa Luxemburg, Marie Curie, Hanna Arendt, Frida Kahlo, Hildegard von Bingen, Michelle Obama, Björk oder Vandana Shiva…

CONSTANZE GUHR arbeitet seit 2004 als selbstständige Illustratorin, beschäftigt sich mit Frauenpower; liebt es, ihr Wissen weiterzu-geben und ist immer neugierig auf neue Themen, die sie in Bücher verwandeln kann.

**Frauen in der Informatik**

 der AG LINK & SIGMA STERN

Diese beiden studentischen Gruppen haben sich zusammengeschlossen, um mit ein paar Vorurteilen aufzuräumen und FLINTA\*-Persönlichkeiten vorzustellen, welche die Welt der Informationstechnik maßgeblich verändert haben. Vieles davon findet tagtägliche Verwendung und ist aus unserer Welt nicht mehr wegzudenken. Ihr Anspruch: „Wir wollen wir FLINTA\* in der IT-Branche in ihrem Sein und Arbeit stärken und ermutigen, sich nicht von den patriarchischen Gesellschaftsstrukturen abschrecken zu lassen, sondern einander zu stützen und Raum zur eigenen Entfaltung zu geben.“

Kooperation aus den studentischen Gruppen SIGMA\* & AG-LINK. Ihr Anspruch: „Wir stehen für die Sichtbarkeit von FLINTA\* in der IT-Branche ein. Gemeinsam wollen wir FLINTA\* in ihrem Sein und Arbeit stärken und ermutigen, sich nicht von den patriarchischen Gesell-schaftsstrukturen abschrecken zu lassen, sondern zu stützen und Raum zur eigenen Entfaltung zu geben.“

 Eintritt: frei

*Ausstellungsdauer: 22.06.2023 – 22.08.2023*