---
id: z70jiYmO
title: Kräuter und Schönheit
start: 2023-05-12 15:00
end: 2023-05-12 17:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
Im Rahmen der Veranstaltungsreihe

 TEE & INTERKULTURELLES GESPRÄCH

Mit PEGGY BURIAN, Soziologin, Märchenerzählerin

Welche Kräuter sind besonders nützlich für Frauen? Und welche Kräuter werden in welchem Land besonders gern genutzt? Von Ringelblumenblüten für unterschiedlichste Hautbesonderheiten bis zu Gond Katira, dass das Haar voll und glänzend erscheinen lässt. 

 Lasst uns zusammen die Welt der Kräuter entdecken, uns austauschen und unsere Schönheitsgeheimnisse miteinander teilen.

Diese Veranstaltung findet statt im Bereich der Politischen Bildung der Volkshochschule Leipzig in Kooperation mit Frauenkultur – und ist ein Angebot der Begegnung. Bei einer Tasse Tee kommen Frauen aus verschiedenen Ländern zu ausgewählten Themen miteinander ins Gespräch. Anmeldung erwünscht. \| Eintritt: frei

## Ort: FiA \| Konradstr. 62, 04315 Leipzig