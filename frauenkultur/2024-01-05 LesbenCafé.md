---
id: qYEftjwk
title: Lesben*Café
start: 2024-01-05 19:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
Das monatliche Lesben\*Café ist ein Ort, um sich in freundlicher Atmosphäre kennenzulernen, auszutauschen, Gemeinschaft zu pflegen und auch um gemeinsam lesbische\* Projekte und Unternehmungen zu planen; ein Ort positiver Energie und Kraft, der immer wieder neu gestaltet werden kann.

Das Café findet ***freitags*** einmal Monat statt. Geplant ist der 1./2. Freitag des Monates. Wegen *Unplanbarem* siehe bitte unter [Programm](</programm/> "Programm [6]")

Wer gerne gemeinsam in den Abend starten möchte, ist herzlich eingeladen um 19 Uhr zu einer “Kennenlern-Willkommensrunde” dazu zu kommen!