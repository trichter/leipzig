---
id: 0Ns2Amf1
title: "Vergessen, verdrängt, verunmöglicht. Repräsentation, Rezeption und Werke
  von FLINTA*-Philosophinnen* "
start: 2024-01-14 16:00
end: 2024-01-14 15:30
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
VORTRAG *im Rahmen der Ausstellung Philosophi\*A*

In der Philosophiegeschichte sind FLINTA\* (Frauen, Lesben, intergeschlechtliche, non-binäre, trans\* und agender und weitere Personen) nicht nur unterrepräsentiert, sondern ihre Werke wurden nicht ausreichend beachtet, gewürdigt und zitiert und somit außerhalb des sog. Kanons gedrängt. In manchen Fällen wurden ihre Arbeiten sogar bewusst zerstört oder gestohlen. Auch ein Blick auf das wissenschaft-liche Personal im Studienfach Philosophie an Universitäten in Europa zeigt, dass dieses größtenteils aus weißen cis-Männern besteht. Feministische Philosophie wird dabei überwiegend nicht regulär in Lehrveranstaltungen integriert, sondern als ein gesondertes Thema behandelt, was es nach wie vor selten in die Vorlesungsverzeichnisse schafft.

In dem Vortrag werden einer intersektionalen Perspektive die Hintergründe und der aktuelle Stand zur Verdrängung von FLINTA\* in der Philosophie(geschichte) beleuchtet. Zudem wird die Bedeutung feministischer Philosophie für die (De)konstruktion von Männlichkeits- und Weiblichkeitskonzepten erörtert und nicht zuletzt wird auch auf die in der Ausstellung gewürdigten Philosoph\*innen eingegangen.

CAROLIN EIRICH hat nach zwei Ausbildungen (Fremdsprachenkorrespondenz und Heilerziehungspflege) an der Humboldt-Universität zu Berlin und an der University of Sydney Kulturwissenschaften und Gender Studies studiert. Anschließend hat sie im Bereich kulturelle und politische Bildung und als Lehrkraft für Deutsch als Fremd- und Zweitsprache in Geflüchteten-, Integrations- und Elternkursen gear-beitet. Seit 2020 lehr Carolin Eirich am Zentrum für transdisziplinäre Geschlechterstudien (ZtG) der HU Berlin.