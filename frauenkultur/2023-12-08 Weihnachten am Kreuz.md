---
id: p1xhNJ4H
title: Weihnachten am Kreuz
start: 2023-12-08 16:00
end: 2023-12-08 22:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
ERÖFFNUNG WEIHNACHTSMARKT

**…geöffnet bis zum 17.12.2023**

Randvoll mit Köstlichem, Schönem, Einzigartigem und wundervoll Duftendem in den Hallen und auf den Freiflächen der Kulturfabrik. 

 Wir freuen uns auf alle, die Lust haben, auch bei unserem Spendenbasar vorbeizuschauen!