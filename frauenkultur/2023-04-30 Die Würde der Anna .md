---
id: bYkHZSIc
title: "Die Würde der Anna "
start: 2023-04-30 11:00
end: 2023-04-30 12:30
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
SZENISCHE LESUNG 

 zur Buchmesse 2023 *Leipzig liest* in der FraKu

Mit ANGELIKA SCHLÜTER

Was macht ein Leben aus? Wann ist es glücklich; wann gelungen?

 Angelika Schlüter nimmt die Hörer:innen mit in eine Zeit, in der diese Fragen so nicht gestellt wurden. Die Lebensumstände diktierten den Alltag, den es in Krieg und Frieden zu bestehen galt. Sie erzählt aus dem Leben ihrer Urgroßmutter Anna (1867 – 1947), die zeitlebens trotz widriger Umstände ihre Würde nie verlor. Begleitend zu dem erzählenden Text, schuf Angelika Schlüter in mehrjähriger Arbeit aus hunderten von Einzelteilen eine Wandcollage aus Fotos, Zeichnungen, Briefen, Objekten und Sammelgut aus assoziativen Materialien.

 Text und Bildwelt sind auf eindrucksvolle, ästhetische Weise miteinander verwoben… und werden in dieser Lesung – unterlegt mit Musikeinspielungen – vorgestellt.

ANGELIKA SCHLÜTER arbeitet in den Bereichen Film, Hörspiel, Installation und Skulptur. Viele ihrer Arbeiten entstanden, inspiriert durch den jeweiligen Ort in verschiedenen Ländern Europas, u.a. in Rumänien, Polen, Ungarn, Frankreich und Spanien. 

 Eintritt: nach Selbsteinschätzung