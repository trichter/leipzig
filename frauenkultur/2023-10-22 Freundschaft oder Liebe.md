---
id: RB7uSoj5
title: Freundschaft oder Liebe
start: 2023-10-22 13:00
end: 2023-10-22 15:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
LESUNG & GESPRÄCH innerhalb des 30. LeLe\*Tre’s

Die Geschichte der Beziehungen zweier Ikonen der ukrainischen Moderne: Lesja Ukrajinka und Olga Kobyljanska

 mit der Autorin NATALJA SHARANDAK

 In Berlin, wo die an Knochen-Tuberkulose leidende Dichterin Lesja Ukrajinka 1899 in einer Privatklinik behandelt wurde – liest sie die Erzählungen von der Schriftstellerin Olga Kobyljanska, die einen großen Eindruck auf sie machen. Und sie entscheidet sich, einen Brief an Olga zu schreiben.

Lesja Ukrajinka und Olga Kobyljanska waren die wichtigsten Vertreterinnen der Moderne in der ukrainischen Literatur – und Schwestern im Geiste. Ihre Werke sind von feministischen Ideen geprägt – auch in ihren privaten Leben vernachlässigten die beiden Frauen die patriarchalischen Konventionen und die Institution der Ehe. Zwischen den beiden Frauen begann eine Korrespondenz, die sich zu einer sehr engen Freundschaft entwickelte – über 14 Jahre lang bis zum Tod von Lesja. In ihren Briefwechseln haben sie sich über ihre ge-sellschaftliche Positionen als Frau und Autorin – und auch mit Zärtlichkeiten ausgetauscht.

Fast ein Jahrhundert lang haben die Biografen mit Vehemenz bestritten, dass die Beziehung der beiden Künstlerinnen mehr als nur eine Freundschaft gewesen sein könnte bzw. gewesen ist. In einer Gesellschaft, wo die gleichgeschlechtliche Liebe verachtet wurde, schien es unmöglich, dass die größte ukrainische Dichterin Lesja Ukrajinka eine Frau liebte. Mit der Entwicklung der LGBTIQA\*-Bewegung in der Ukraine werden ihre Stimmen immer leiser – und lauter werden die Stimmen, die versuchen über dieses Thema öffentlich zu diskutie-ren. War es nur Freundschaft oder Liebe? Auf der Grundlage von Briefen, Tagebüchern und einigen Werken der beiden Autorinnen, die die Verliebtheit einer Frau in eine Frau thematisieren – wird versucht, diese Frage zu beantworten. Mit zahlreichen dokumentarischen Fotografien. \| Eintritt: nach Selbsteinschätzung

**Das gesamte Programm des 30. LeLe\*Tre’s ->[siehe bitte hier](</angebote/aktuelle-projekte/leletre-leipziger-lesbentreffen/>)**