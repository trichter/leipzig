---
id: 0DxYgL3l
title: Uferfrauen – Lesbisches L(i)eben in der DDR
start: 2023-07-13 19:00
end: 2023-07-13 21:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
FILM & GESPRÄCH im Rahmen des Leipziger CSD 2023

 Mit der Regisseurin und Botschafterin des Leipziger CSD 2023 

 BARBARA WALLBRAUN

Filmscreening des preisgekrönten Dokumentarfilmes – ausgezeichnet u.a. mit dem Debütpreis des Queerscopeverbands und dem Publikumspreis der Lesbisch-schwulen Filmtage Hamburg.

Der Film portraitiert sechs lesbische Frauen, die in der DDR gelebt haben. Die Protagonistinnen erzählen offen und berührend ihre Geschichte(n) darüber, wie es sich als lesbische Frau in der DDR lebte: Christiane aus Berlin, Carola aus Dresden, Pat aus Mecklen-burg-Vorpommern; Elke und das Langzeit-Paar Sabine und Gisela aus Sachsen-Anhalt. Sie lassen das Publikum an ihrem damaligen Lebensalltag teilhaben, an ihrem Kampf um Selbstbestimmung, der ersten Liebe, unkonventioneller Familienplanung sowie Konﬂikten mit der SED und dem Gesetz.

> „Ein Film der Mut machen will, für sich selbst und ein gleichberechtigtes, freies Leben einzustehen.“ (NDR Kulturjournal)

Im Statement der Leipziger Filmemacherin als CSD-Botschafterin heißt es u.a.:

> “Unser Blick sollte sich nicht nur nach vorn richten, sondern auch die Vergangenheit berücksichtigen. Vergesst beim Blick in die queere Zukunft nicht die Älteren mit ihren Geschichten und Erfahrungen, von denen wir profitieren. Lasst uns in einen Dialog der Generationen kommen!“

Einen Grundstein dafür legt Sie mit den „Uferfrauen“ mit berührendem Empowerment.

 Eintritt: nach Selbsteinschätzung

*Foto: Betty Pabst*