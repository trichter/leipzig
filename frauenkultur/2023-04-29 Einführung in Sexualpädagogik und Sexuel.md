---
id: mAWPDYP2
title: "Einführung in Sexualpädagogik und Sexuelle Bildung. "
start: 2023-04-29 18:00
end: 2023-04-29 19:30
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
LESUNG & GESPRÄCH \| W. Kohlhammer 

 zur Buchmesse 2023 *Leipzig liest* in der FraKu

Basisbuch für Studium und Weiterbildung

 mit Prof. Dr. HEINZ-JÜRGEN VOSS

Eine „Sexualpädagogik der Vielfalt“ um 1930? Die Schülerinnenzeitung „Bienenkorb-Gazette“ (1967) als Auftakt einer aktuellen Sexualpädagogik in der BRD und Westberlin? … bis hin zu aktuellen Entwicklungen.

Das neue (Lehr-)Buch gibt Aufschluss. Sexualpädagogik muss in Teilen neu gedacht werden. Es lohnt sich, genau auf Entwicklungen zu sehen; auch darauf, wie Denkweisen der „Geschlechterwandlung & -mischung“ in den Traditionen der Sexualpädagogik tlw. ausgelöscht wurden. Auch nach 1945 griff man sie nicht wieder auf – weder in der BRD/Westberlin, noch in der DDR. Geschlechtliche/sexuelle Entwicklung, die Selbstbestimmung akzeptiert, gewährleistet und Grenzen achtet, wäre schon eher möglich gewesen. Doch erst mit aktuellen Entwicklungen kommen wir hier an. Heute stehen die Förderung von geschlechtlicher und sexueller Selbstbestimmung im Vordergrund – entsprechende sexualpädagogische Konzepte werden, in Verbindung mit Schutzkonzepten, vorangebracht. Zugleich gibt es zunehmend Angebote für Erwachsene, gelingende Sexualität im Blick zu behalten, am eigenen sexuellen Wohlergehen, aber auch an Sexualproblemen zu arbeiten. Dieser neue Einführungsband liefert einen aktuellen, zeitgemäßen Überblick.

Prof. Dr. HEINZ-JÜRGEN VOSS, Professur für Sexualwissenschaft und sexuelle Bildung am Fachbereich Soziale Arbeit. Medien. Kultur der Hochschule Merseburg 

 Eintritt: frei