---
id: 4wcTVPfT
title: "Digitale Gewalt: Wen betrifft sie und wie geht Politik damit um?"
start: 2023-12-07 19:00
end: 2023-12-07 20:30
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
VORTRAG & DISKUSSION

 mit ANNE ROTH

Häusliche und geschlechtsspezifische Gewalt finden durch die Digitalisierung unseres Alltags zunehmend auch ‘online’ statt. Social Media, Messenger Apps und Smart-Homes bieten neue Angriffsflächen für Täter und stellen damit eine Gefahr für Betroffene dar. Zu häufig wird diese Gewalt nicht ernst genommen oder als ein rein individuelles Problem verkannt. Wie die aktuelle Lage aussieht und was politische Lösungen sein könnten, erläutert Anne Roth in ihrem Vortrag.

ANNE ROTH ist Referentin für Netzpolitik der Linksfraktion im Bundestag. Sie beschäftigt sich seit vielen Jahren mit digitaler Gewalt und darüber hinaus mit Überwachung, Sicherheitsbehörden und feministischer Netzpolitik.

 Eintritt: nach Selbsteinschätzung