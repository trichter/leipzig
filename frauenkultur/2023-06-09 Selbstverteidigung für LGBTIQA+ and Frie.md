---
id: lb5kdKgU
title: Leider abgesagt! Selbstverteidigung für LGBTIQA+ and Friends! Bodenkampf
  Spezial!
start: 2023-06-09 17:30
end: 2023-06-09 19:30
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
## **Leider abgesagt!!!** Der WORKSHOP mit DENISE (she/they) und CHARLIE (alle Pronomen)

Wir wollen mit euch gemeinsam entdecken, wozu euer Körper fähig ist und wie ihr euch und andere effektiv selbst behaupten und schützen könnt. Wir haben beide jahrelange Erfahrung in unterschiedlichen Kampfsportarten, wir sind weiß und queer. Dich erwarten 2 Stunden Auseinandersetzung mit Körpersprache, Selbstbehauptung und Techniken, um dich in Situationen wehrhaft zu machen und dir ein sicheres Gefühl zu geben. Dieses Mal wollen wir viel am Boden arbeiten, und so verschiedenen Situationen durchspielen.

Wer kann teilnehmen? Alle Menschen ab 16 Jahren, die sich als queer, LGBTIQA\*+ oder Friends/Allies begreifen. Unsere Workshops sind offen für alle Gender, Alter, Erfahrungslevel, Traumatisierungen, körperlichen Gegebenheiten, races und classes. Du brauchst nichts, außer Kleidung; in der du dich gut bewegen kannst und vielleicht etwas zu trinken. Wir starten immer mit einer Vorstellungsrunde und sprechen Erfahrungen, Wünsche und Tabus durch. Dann erarbeiten wir gemeinsam verschiedene Techniken, arbeiten mit Alltagsgegenständen und fokussieren uns dann auf den Boden und Verteidigung in liegender Position. Am Ende freuen wir uns über eine Feedbackrunde und einen gemeinsamen Abschluss des Abends. Wenn du Assistenzbedarf hast, trainiert deine Assistenzgeber:in kostenlos mit dir mit. Kontaktiere uns gerne, wenn du Fragen hast. Anmeldung bitte bis zum\* 07.06.2023\* 

 Teilnahmebeitrag nach Selbsteinschätzung: Für zwei Stunden Workshop bitte zwischen 20,- und 30,- Euro.