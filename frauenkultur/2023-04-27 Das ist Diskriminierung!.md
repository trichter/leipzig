---
id: bRetYTvJ
title: NEU!  OpferMacht. Klartext reden über sexualisierte Gewalt.
start: 2023-04-27 17:00
end: 2023-04-27 18:15
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
## … nach der krankheitsbedingten Absage von Yara Hofbauer

LESUNG & DISKUSSION \| Unrast-Verlag

 zur Buchmesse 2023 *Leipzig liest* in der FraKu

Mit NORA KELLNER

Aufgerüttelt durch persönliche Erfahrungen zeigt die Autorin, wie sich sexualisierte Gewalt – die alltäglich und in allen Gesellschaftsbereichen unseres Lebens verankert ist – auf das Leben von FLINTA auswirkt. Nüchtern und sachlich beschreibt die Sozialwissenschaftlerin, wie es sich anfühlt, wenn eine sich wehrt und vor Gericht zieht und wie schmerzhaft und belastend die Bewertung der Tat durch außenstehende Menschen ist. In der Analyse der eigenen Geschichte nimmt Nora Kellner immer wieder Bezug auf gesellschaftliche Debatten und konkrete Beispiele, die dokumentieren, wie sexualisierte Gewalt aufrechterhalten und reproduziert wird. Sie wirft einen Blick auf die zutiefst sexistischen Strukturen im Polizeiapparat und wundert sich darüber, warum die linken Kräfte – wenn sie es ernst meinen mit ihrer Verurteilung von sexualisierter Gewalt – ihrer Verantwortung in diesem Diskurs nicht gerecht werden. In einer Gesellschaft und Kultur, in der Betroffene systematisch zum Schweigen gebracht werden, räumt die Autorin mit einer Vielzahl von Mythen rund um das Thema auf und pocht darauf, dass FLINTA niemals selbst schuld sind.

Das Buch ist gleichzeitig Plädoyer gegen die Verharmlosung sexualisierter Gewalterfahrung in einer patriarchalen Gesellschaft, gegen rassistische Auslegungen der Strukturen sexualisierter Gewalt und Empowerment bzw. Identifikation für betroffene FLINTA.

NORA KELLNER (sie/ihr), 1998 in Weiden in der Oberpfalz, Bayern, geboren, hat einen Bachelor in Politikwissenschaft und absolviert zurzeit ein Masterstudium der Gender & Queer Studies in Köln. Sie ist eines der zahllosen Opfer von sexualisierter Gewalt. Ihre eignen Erfahrungen bewegten sie dazu, das Buch “OpferMacht. Klartext reden über sexualisierte Gewalt” zu veröffentlichen, das 2023 im Unrast Verlag erscheint/erschienen ist. Mit diesem macht sie gesellschaftliche Dynamiken sichtbar, die im Umgang mit sexualisierter Gewalt immer noch wirkmächtig sind.

 Eintritt: frei