---
id: Bdop2lbc
title: Weltmädchentag
start: 2023-10-11 13:30
end: 2023-10-11 19:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
OFFENER AKTIONSTAG

Wir laden alle Mädchen\* ab 9 Jahren und alle jungen Frauen\* zum diesjährigen Aktionstag anlässlich des Weltmädchentags am 11. Oktober ein. Es wird verschiedene Workshops, Musik und Essen geben. 

 Der Aktionstag wird veranstaltet vom AK Mädchen\* Leipzig. Zahlreiche Vertretende aus Einrichtungen der Kinder- und Jugendhilfe bilden dieses Fachgremium, das schon seit vielen Jahren eine politische Lobby für Mädchen\*/junge FLINTA\* schafft.

**Ort: Gelände des CVJM Leipzig e.V., Schönefelder Allee 23A, 04347 Leipzig**