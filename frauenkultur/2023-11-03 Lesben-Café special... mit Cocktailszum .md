---
id: PQgKbnue
title: "Lesben*-Café "
start: 2023-11-03 19:00
end: 2023-11-03 22:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
OFFENER TREFF

Das Lesben\*Café ist ein Ort, um sich in freundlicher Atmosphäre kennenzulernen, auszutauschen und um gemeinsam Projekte und Unternehmungen zu planen; ein Ort positiver Energie und Kraft, der immer wieder neugestaltet werden kann.