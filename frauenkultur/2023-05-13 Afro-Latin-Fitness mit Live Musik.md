---
id: L3E1cNYu
title: Afro-Latin-Fitness mit Live Musik
start: 2023-05-13 10:00
end: 2023-05-13 12:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
## TANZ-WORKSHOP für Frauen\*

mit CLAUDIA CASTAÑEDA; Leiterin der Tanz-Performancegruppe “Madamtamtam”

Der Empowerment-Workshop richtet sich an Frauen, besonders an Frauen mit Migrationsgeschichte, die sich gerne bewegen möchten zu westafrikanischer Tanz- und Trommelmusik. Sie sind alle herzlich eingeladen, mit Hilfe des Tanzes Stress abzubauen – und sich von Energie und Lebensfreude anstecken zu lassen. Anmeldung erwünscht. Nächster Termin: **20\.05.2023** 

 Eintritt gegen Spende