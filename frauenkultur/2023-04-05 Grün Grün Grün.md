---
id: ip9veJHC
title: Grün Grün Grün
start: 2023-04-05 15:00
end: 2023-04-05 17:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
## GARTENEINSATZ auf dem FraKu-Grünstück

Der gemeinsame Garteneinsatz für alle grünbegeisterten Nutzerinnen\* der Grünfläche am Haus der Frauenkultur. 

 Es darf gehackt, geharkt, gegraben und gepflanzt werden… plus Gratisgetränk… :)