---
id: qRtVhGvf
title: Offener Lesekreis zu Leben und Werk von Charlotte Wolff
start: 2023-12-05 17:00
end: 2023-12-05 19:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
OFFENER LESEKREIS

Die jüdische Ärztin Charlotte Wolff (geb. 1897 in Riesenburg/Prabuty / gest. 1986 in London) war Sexualwissenschaftlerin und Schriftstellerin – und sie veröffentlichte grundlegende Werke zur weiblichen Homosexualität. Alle Interessierten können gern dazukommen.