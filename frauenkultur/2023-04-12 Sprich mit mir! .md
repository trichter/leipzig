---
id: rFciczt0
title: "Sprich mit mir! "
start: 2023-04-12 15:00
end: 2023-04-12 17:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
SPRACHTANDEM

 Ein Projekt der Frauenkultur Leipzig und des Museums der Bildenden Künste Leipzig.

Interessierte Frauen können an jedem 2. Mittwoch im Monat das Museum der Bildenden Künste besuchen und im Sprachtandem dabei Sprache neu lernen. Eine Deutsch-Sprechende und eine Deutsch-Lernende Frau besuchen gemeinsam das Museum – und erzählen einander, was sie sehen. Sie entdecken zusammen Bilder – und lernen gemeinsam Deutsch oder auch eine andere Sprache…

Für Frauen und Mädchen ab 14 Jahren.Die Teilnahme ist kostenlos.

 Interessierte Frauen und Mädchen melden sich bitte in der Frauenkultur Leipzig \| Tel. 0341 – 213 00 30 oder im Museum der Bildenden Künste Leipzig \| Tel. 0341 – 216 999 41, kirsten.huwig@leipzig.de