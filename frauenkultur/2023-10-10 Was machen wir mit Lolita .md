---
id: Xe5Bzb8N
title: "Was machen wir mit Lolita? "
start: 2023-10-10 17:00
end: 2023-10-10 18:30
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
LESUNG & GESPRÄCH

**Argumente und Kämpfe um Frauen und Kultur**

 mit der Autorin LAURA FREIXAS.

Öffentliche Veranstaltung auf Spanisch des Lesekreises Femigradas zum Tag der Schriftstellerinnen. Die Lesung findet auf Spanisch statt. 

 Eintritt nach Selbsteinschätzung. 

*Eine Veranstaltung von Femigradas*

 ———————————————————————————

 LECTURA & CHARLA COLOQUIO

 ¿Qué hacemos con Lolita? Argumentos y atallas en torno a las mujeres y la kultura

 con la autora LAURA FREIXAS. Evento en español con un pequeño donativo.