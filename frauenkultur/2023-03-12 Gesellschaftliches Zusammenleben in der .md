---
id: 3b8CbGoO
title: Gesellschaftliches Zusammenleben in der Frühgeschichte
start: 2023-03-12 15:00
end: 2023-03-12 17:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
## … über das Leben von Menschen in der Zeit der Bandkeramik, die vor 7.000 Jahren auch in Rückmarsdorf ein Zuhause hatten.

**VORTRAG MIT ERZÄHL-CAFÉ** für alle interessierten Menschen 

 … innerhalb des Projektes „Text, Bild & Stein: Sie erzählen uns unsere Geschichte. Rückmarsdorf 2023“

Mit CHRISTINE RIETZKE, Frauenkultur Leipzig

 seit 1991 Seminarleitungen zu Themen politischer Bildung

In Rückmarsdorf wurde menschliche Sesshaftigkeit ab 5.000 v.u.Z. nachgewiesen, in der Zeit der sogenannten „Glockenbecher Kultur“. Eine der bekanntesten Forscherinnen zu dieser Phase war die litauische Archäologin Mariia Gimbutas. 1950 wurde sie als Archäologie-Dozentin an die Harvard-Universität berufen. Sie entwickelte die These, nach der im Bereich zwischen Kaukasus, Wolga und Ural schon früh Pferde domestiziert wurden. Die dadurch entstandene hohe Mobilität erlaubte den Menschen nach klimatischen Veränderungen im Kaukasus nach neuen Weide- und Lebensgebieten Richtung Westen zu suchen.

Gimbutas und andere Forscher:innen gehen anhand unterschiedlichster Funde davon aus, dass in „Alteuropa“ im Neolithikum zwischen den Geschlechtern ausgewogene (gynandric) matrilineare Gesellschaftsformen existierten. Durch die Ausbreitung der indoeuropäischen Gruppen zwischen 4.300 und 2.800 v.u.Z. kam es zu gesellschaftlichen Veränderung in den alt-europäischen Agrarkulturen; es wurden Prozesse eingeleitet, die zur Etablierung aristokratisch zu nennender Oberschichten führte. Diese Eroberung und Veränderung Europas findet sich archäologisch in der Glockenbecher-Kultur wieder… auch in Rückmarsdorf.

Die Ideen dieser Forschungen vorzustellen, ist Anliegen dieses Vortrages. Denn tangiert werden unmittelbar auch aktuelle Herausforderun-gen unserer Zeit wie z.B. klimatische Veränderungen mit dadurch einsetzenden Wander-/Migrationsbewegungen bis hin zu Kriegen und Flucht. Und auch, dass ein gerechtes Zusammenleben aller Geschlechter – als Garant für Frieden – unabdingbar faire sozio-ökonomische Verteilungen der Ressourcen braucht. Der Eintritt ist frei.

***Zum Projekt***

## Text, Bild & Stein: Sie erzählen uns unsere Geschichte. Rückmarsdorf 2023 

… ein Gemeinschaftsprojekt des Soziokulturellen Zentrums Frauenkultur Leipzig und des Heimatsvereins Rückmarsdorf

 … mit zahlreichen Veranstaltungen: Vorträgen, Workshops und Kreativangeboten

 … im Themenjahr des Dezernats Kultur: „**Leipzig. Die ganze Stadt als Bühne**“ – mit Schwerpunkt auf den Leipziger Ortschaften

 … um Orte neu zu entdecken, sichtbarer zu machen und um Gemeinschaft und ein gutes Miteinander zu stärken