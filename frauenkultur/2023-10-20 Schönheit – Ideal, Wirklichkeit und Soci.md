---
id: oPE7OhTz
title: Schönheit – Ideal, Wirklichkeit und Social Media
start: 2023-10-20 15:00
end: 2023-10-20 17:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
TEE & INTERKULTURELLES GESPRÄCH

Diese Veranstaltungsreihe ist ein Angebot der soziokulturellen Begegnungen im Leipziger Osten. Bei einer Tasse Tee kommen Frauen aus verschiedenen Ländern zu ausgewählten Themen miteinander ins Gespräch.

Heute: **„Schönheit – Ideal, Wirklichkeit und Social Media“**

 Die Lücke zwischen dem realen “echten” Aussehen und den geposteten Bildern auf social media wächst überall auf der Welt. Dies möchten wir uns gemeinsam anschauen, Möglichkeiten und Gefahren ausloten und uns über das vielseitige Thema austauschen.

Diese Veranstaltung der Volkshochschule Leipzig im Bereich der Politischen Bildung findet statt in Kooperation mit dem Soziokulturellen Zentrum Frauenkultur 

 Anmeldung erwünscht. \| Eintritt:. frei

**Ort: FiA, Konradstr. 62, 04315 Leipzig**