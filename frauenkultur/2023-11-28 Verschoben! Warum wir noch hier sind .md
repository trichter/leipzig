---
id: c5ZJYw8H
title: "Verschoben! Warum wir noch hier sind "
start: 2023-11-28 19:00
end: 2023-11-28 20:30
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
## Update: Wegen akuter Erkrankung der Autorin musste die Lesung auf den **28\.11.2023** verschoben werden!

LESUNG aus Anlass des Internationalen Tages gegen Gewalt an Frauen

 Mit der Autorin MARLEN PELNY

> Dieser Roman ist eine sprachlich kraftvolle Auflehnung: gegen Ungerechtigkeit, die tötet. Gegen die Gewalt, der wir täglich begegnen und die wir zu überleben versuchen.

Auf dem Tisch liegt ein Fotoalbum. Darin: Fotos von Etty als Baby; mit vier Jahren im Schwimmbad; mit elf beim Mini-Golf; mit vierzehn vor der Haustür, kurz vor ihrem gewaltsamen Tod. Die Gefahr, der Frauen und Mädchen in dieser Welt ausgesetzt sind, ist nun in die unmittelbare Nähe der Erzählerin gerückt. Etty war die Tochter ihrer besten Freundin Heide. Von nun an unterliegt ihre Welt einer zweiten Zählzeit – da, wo Ettys Leben endete, fängt für sie ein anderes an. Was bleibt, sind Fotos, Erinnerungen und so viele Fragen: Wie weiterleben? Wie jeden Tag aufstehen? Wie sich weiterhin in der Wohnung aufhalten, in der Etty zuhause war? Wie ihr Lachen, ihre frechen Antworten, ihre feinen Gesichtszüge erinnern, ohne zu zerbrechen? Der eigentlich unmögliche Versuch, das Geschehene zu verstehen, wird zum Versuch, zu funktionieren… in einem Schwebezustand, aufgeladen mit Liebe, unterfüttert mit Hilflosigkeit.

Marlen Pelny beleuchtet die Geschichte eines Femizids aus der Perspektive der Hinterbliebenen. Sie zeigt, was es bedeutet, zurückzu-bleiben. Wenn einer Mutter zwei Tage Sonderurlaub zur Trauer zugestanden werden. Wenn Ordner voller Bürokratie abgearbeitet werden müssen – ganz obenauf Geburts- und Sterbeurkunde, mit denen sich nun ein ganzes Leben zusammenfassen lässt. Aber auch: Wie es sich anfühlt, wenn die eigene Stadt, in der man sich nicht nur zuhause sondern auch frei gefühlt hat, plötzlich zur Gefahrenzone wird.

MARLEN PELNY plakatierte deutsche Städte mit Lyrik und veröffentlichte Gedichtbände „Auftakt“ (2007) \| „Wir müssen nur noch die Tiere erschlagen“ (2013). Ihre klare Poesie durchströmt ihr Romandebüt „Liebe / Liebe“. 2022 ausgezeichnet mit dem Klopstock-Förderpreis.

Eintritt gegen Spende 

 [für Aufklärungstafeln zu Femiziden in Deutschland im Rahmen des Sechsten Leipziger Frauen\*/ FLIN-TA\*Festivals am 04.05.2024]