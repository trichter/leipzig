---
id: 1urUUiwL
title: "Nordland 2061 – Gleichheit "
start: 2023-04-26 21:00
end: 2023-04-26 22:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
LESUNG \| acabus-Verlag

 zur Buchmesse 2023 *Leipzig liest* in der FraKu

Mit GABRIELE ALBERS, Hamburg

Die Bundesrepublik ist Geschichte. Die nördlichen Bundesländer haben sich schon vor Jahren zu „Nordland“ zusammengeschlossen, einem Staat, in dem allein das Geld regiert. In diesem Land, in dem Mädchen nichts wert und Frauen das Eigentum ihrer Männer sind, kämpft Lillith für Freiheit und Gleichheit. Bo, ihr wichtigster Verbündeter in diesem Kampf, sitzt im Gefängnis – ihretwegen. Als es Lillith endlich gelingt, ihn zu befreien, ist der Preis höher als gedacht. Viel höher. Gleichzeitig ist die Chance für einen echten Wandel im Land zum Greifen nah. Aber hat Lillith dafür die richtigen Mitstreiter:innen an ihrer Seite?

GABRIELE ALBERS, Journalistin und Autorin, Volkswirtin, analysiert sie seit Jahren das Zusammenspiel von Wirtschaft und Politik. Ihre Themen: gefährliche Entwicklungen unserer Zeit und deren Folgen in einer fiktiven Zukunft. Ihre persönliche Zeitreisemaschine würde ins Jahr 2500 gehen…? \| Eintritt: frei