---
id: nImlX3Os
title: Kostenlose Rechtsberatung rund um das Thema „Grad der Behinderung“
start: 2023-07-04 15:30
end: 2023-07-04 16:30
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
RECHTSBERATUNG. Neues Angebot 1x im Monat

 mit der Rechtsanwältin ANNA VORWERG

Bei bestehenden Krankheiten und Behinderungen an der Teilhabe im Alltags- oder Erwerbsleben kann es sinnvoll sein, beim Versorgungsamt die Feststellung eines sogenannten „Grades der Behinderung“ zu beantragen. In der kostenfreien Beratungsstunde möchte Rechtsanwältin Anna Vorwerg Unsicherheiten bei der Antragsstellung nehmen und hilfreiche Tipps geben. Eine Anmeldung ist bitte bis **03\.07.2023** erforderlich! Nächster Termin am **05\.09.2023**

## Jeden 1. Dienstag im Monat 15:30-16:30 Uhr