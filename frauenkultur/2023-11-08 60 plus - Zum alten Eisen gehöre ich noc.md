---
id: xk6IjCzE
title: "60 plus - Zum alten Eisen gehöre ich noch lange nicht! "
start: 2023-11-08 11:00
end: 2023-11-08 13:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
FRAUENSTAMMTISCH

 II. Frauenstammtisch für aktive (Un)Ruheständlerinnen

 Im beruflichen (Vor)Ruhestand angekommen, sind die alltäglichen Arbeiten meist überschaubar. Natürlich, größere Renovierungs- und Putz-Aktionen oder Gartenarbeiten halten einen schon auf Trab – und Angst wegen “des Rastens und Rostens” ist völlig unbegründet.

Aber manchmal gibt es immer wieder mal einen Moment, in dem es gut wäre, mit anderen Menschen im Gespräch zu sein oder gemeinsam etwas zu tun. Die eigene Meinung öffentlich zu machen, ist ein wichtiger Teil unseres gesellschaftlichen Lebens. Wir mischen uns ein, mischen mit und können nur so – wenn auch nur ein Stückchen – Einfluss nehmen auf das, was in unserer Stadt passiert oder eben nicht. Und dafür ist der “rückenstärkende Austausch” in einer Gruppe hervorragend geeignet.

 Leitung: Edda Minkus