---
id: 532uk72U
title: Queen 16 – Female Urban Festival
start: 2023-10-14 12:00
end: 2023-10-14 22:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
Seit einigen Jahren findet auch im Breaking eine immer stärker werdende emanzipatorische Bewegung statt, mit der Mädchen\* und junge Frauen\* versuchen, die Szene zu erobern – wobei sie mehr Mühe und Zeit aufbringen müssen, um sich in dieser „Männerdomäne“ durchzusetzen und aufgrund dessen auch wesentlich häufiger und schneller wieder aufhören. Der kulturelle Output und die selbstbestimmte Repräsentation sind wichtige Funktionen in der Entwicklung für junge Mädchen\* und Frauen\* – gerade in einer so stark wachsenden und immer mehr gesellschaftlich anerkannten Szene wie der urbanen Kultur. Das Queen 16 Festival setzt den Fokus auf junge Mädchen\* & Frauen\* – und gibt denen eine Bühne, die sonst eher weniger am Kulturbetrieb teilnehmen. Eine weiterer Festivalsaspekt liegt auf der wichtigen Schnittstelle zwischen den Bereichen des künstlerischen Erlernens einer Kunstform, um diese anschließend in einem öffentlichen Raum umzusetzen. Dieser Prozess wird durch eine gesellschaftskritische Auseinandersetzung mit internationalen Austausch unterstützt, um damit ein demokratisches und vielfältiges Zusammenleben zu gestalten.

**Das Leitbild des „Queen 16 – Female Urban Festivals“**

 Ein offenes, zugewandtes und freundliches Miteinander von jungen Künstlerninnen\* aus vielen Ländern zu erleben. Eine Initiative von Frauen\* für Frauen\* um Eigenverantwortung zu übernehmen.

> „Nur wer sichtbar ist – findet auch statt.” Tijen Onaran

Die Akteur:innen des Festivals betonen, dass die Inklusion beider/aller Geschlechter wichtig ist. Das gesamte Wochenende sind Repräsentanten:innen der Szene dazu eingeladen, ihre Sichtweisen zu teilen. Der Anliegen des Queen 16 – Female Urban Festival: Stärkung & Empowerment von Mädchen\* und Frauen\*, sich mittels Kunst auszudrücken, ins aktive Handeln zu kommen und ihre kulturellen und sozialen Kompetenzen zu präsentieren, sich untereinander zu vernetzen und voneinander zu lernen.

Es werden 12 internationale Tänzerinnen\* eingeladen, welche eine gute Reputation in der Breaking Szene weltweit aufweisen und auch als Role-Models fungieren. Die internationalen Tänzerinnen\* weisen eine gute Erfahrungsgrundlage auf und sind aufgrund dessen Expertinnen\* auf den Gebieten der künstlerisch-technischen sowie emotionalen Ebene. Es gibt bereits einige wenige Orte, die als „Save Space“ für Frauen\* dienen und innerhalb denen sich Frauen\* in geschützten Räumen ausprobieren oder bei sensiblen Fragen in Austausch treten können. Diese sind aber eher die Ausnahmen. Mithilfe des Queen 16 Festivals sollen diese Räume weiter gefördert und etabliert werden.

##  Die Highlights 

13\.10.2023	Frauenkultur Leipzig e.V. – **Welcome-Day**: Panel Diskussion \| Vorträge 

 14\.10.2023	Gym, Hammerstr. 11, Leipzig – **Knowledge-Day**: Workshops Footwork, Creativity, Flow, Creativity 

 15\.10.2023	Conne Island – **Culture-Day**: Internationales B-Girl-Battle, Cypher-Battle

[**Ausführliche Infos: -> siehe bitte hier**](</angebote/aktuelle-projekte/queen16/>)