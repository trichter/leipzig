---
id: vymWbCQR
title: "Lateinamerikanisches Klavier-Konzert. "
start: 2023-11-12 16:00
end: 2023-11-12 17:30
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
KONZERT 

 Kompositionen aus Chile, Kuba und Brasilien

Mit NATALIA ALVAREZ MALEBRÁN & MATILDE MÉNDEZ ZENTENO (Chile)

Ein mitreißendes Programm mit Kompositionen von Violeta Parra, Dr. Svetlana Kotova, Vicente Bianchi, und anderen, lädt ein zu einer Reise durch die Landschaften und Rhythmen Lateinamerikas. Eine besondere Überraschung bietet ein vierhändiges Duett mit traditio-neller chilenischer Musik.

NATALIA ALVAREZ MALEBRÁN (Chile) wurde 1990 in der Hafenstadt Coquimbo geboren. 2009 trat sie in die Universität von La Serena ein, wo sie Klavier bei Horacio Tardito und später bei Ariadna Colli studierte. In ihrem Heimatland beteiligt sie sich an kulturellen Förder-projekten und trat in verschiedenen Regionen und Städten Chiles als Solistin und begleitende Pianistin auf so u.a.für „Coro de Cámara Universidad de La Serena“ und „Cantares del Elqui“. Auf der Suche nach neuen Lebenserfahrungen zog sie 2019 nach Leipzig. Ab 2021 ist sie Mitglied des Internationalen Frauenchors Leipzig.

MATILDE MÉNDEZ ZENTENO (Chile, \*1997) Santiago de Chile. Ihr Grundstudium schloss sie unter der Anleitung von Dr. Svetlana Kotova an der Universität von Chile ab. Derzeit lebt sie in Leipzig mit der Motivation, historische Tastaturen zu studieren.

Eintritt: nach Selbsteinschätzung