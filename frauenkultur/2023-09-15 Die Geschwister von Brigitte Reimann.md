---
id: 6O6Y6jZV
title: Die Geschwister von Brigitte Reimann
start: 2023-09-15 18:00
end: 2023-09-15 19:30
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
FANLESUNG zur Neuausgabe Die Geschwister

 Mit der Schauspielerin ELISA UEBERSCHÄR

> „[…]	Wie kam das jetzt aber alles?
> 
>  Die Geschwister, ja, Reimann hat die 1963 geschrieben, spielen tut das ganze ja 1960.Da gabs eben 63 ne Ausgabe, später nochmal 69 und jetzt 2023 – zum großen Jubiläumsjahr von unserer Brigitte!
> 
>  In Hoyerswerda, 2022, beim Aufräumen im Haus der ehemaligen Begegnungsstätte, gibt’s jetzt nicht mehr in Hoy, Begegnung mit Rei-mann findet jetzt in der Bibliothek statt, haben die eine Kiste im Keller gefunden. Und Gott sei Dank: nicht einfach weggeschmissen mit dem restlichen Schutt, ne ne, die haben die Kiste aufgemacht. Und finden da drinnen die ersten 5 Kapitel des Originalmanuskripts der Geschwister. Die Kiste muss die Reimann ja damals schon vergessen haben, 68! 68 ist die Reimann ja weg nach Neubrandenburg, nachdem ganzen privaten Wahnsinn mit Pietschmann, und dem Jon und dem ganzen Betrug, ja ja ja. Hat sie die Kiste da wohl vergessen. Naja, auf jeden Fall gutes Timing würde ich sagen, oder? Findest du ne Kiste 22 mit dem Originalmanuskript und 23 ist Reimannjahr! Das ist doch klasse.

> Ja, und Reimann wird ja auch gerade wiederentdeckt, selbst in Großbritannien – hier: ist die Ausgabe der Geschwister auf Englisch. Der ganze Osten wird ja gerade wiederentdeckt, ne. Ich hab Reimann ja schon 2017/2018 entdeckt. Hab ich meine erste Lesung konzipiert, Franziska Linkerhand auf 90minuten gekürzt und richtig viel recherchiert. Bin ich rumgefahren, hab den Ludwig Reimann getroffen, in Hamburg, Hamburg Altona. Sehr nett gewesen. Dann war ich in Berlin, habe die Amsterdam Freundin getroffen. Und alles gelesen, das ist klar. Also ich hab die Reimann schon vor einigen Jahren entdeckt und das wusste der Aufbau Verlag natürlich und die haben mich da jetzt empfohlen, hierher und dann les ich noch in Magdeburg und Halle und na klar in Hoy und aber auch in Chemnitz, Leipzig und auch Salzwedel. Ja, so war das, deswegen sind wir jetzt hier.“

Bild und Text ©Elisa Ueberschär