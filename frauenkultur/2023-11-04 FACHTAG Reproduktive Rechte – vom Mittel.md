---
id: s7CDO8ne
title: "FACHTAG: Reproduktive Rechte – vom Mittelalter/Frühe Neuzeit bis in die
  Gegenwart"
start: 2023-11-04 10:00
end: 2023-11-04 17:30
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
FACHTAG des AK Aufarbeitung Hexenverfolgung Leipzig & der Frauenkultur Leipzig

10\.00 Uhr BEGRÜSSUNG & EINFÜHRUNG

**Über die Ursprünge des §218 und den Beginn der Einschränkung reproduktiver Rechte von Frauen**

 CHRISTINE RIETZKE, Vorstand Frauenkultur Leipzig, Mitgründerin des AK Aufarbeitung Hexenverfolgung

10\.30 Uhr VORTRAG

**Abtreibungsprozesse vor dem Leipziger Stadtgericht in der Frühen Neuzeit**

 MADELEINE APITZSCH, Historikerin & Germanistin; lebt und forscht in Leipzig u. a. zu Hexenverfolgungen in Kursachsen sowie zur Kriminalitätsgeschichte der Stadt Leipzig in der Frühen Neuzeit.

11\.50 Uhr VORTRAG

**Zu Geburt und Geburtshilfe aus Sicht der Gebärenden und aus Sicht der Hebemütter** 

 Prof. Dr. EVA LABOUVIE, Historikerin

 Die Historikerin Eva Labouvie beschäftigt sich seit den 1980er Jahren mit Geschlechterforschung – u.a. mit Geburt, Geburtshilfe, Mutterschaft und mit Reproduktion von der Frühen Neuzeit bis ins Heute (16. bis 21. Jahrhundert). In diesem Vortrag betrachtet sie die Gebärkulturen des 16. bis 19. Jahrhunderts – und auch die Entwicklung des Standes der Hebammen von relativ autark handelnden, von Frauenkollektiven gewählten Personen bis hin zu ihrer Kontrolle, Reglementierung und Professionalisierung durch Mediziner und Behörden ab dem 19. Jahrhundert.

13\.30 Uhr VORTRAG

**Zwischen Fürsorge und Disziplinierung. Zur Lebenssituation unehelich Schwangerer im Leipziger Lazarett.**

 Prof. Dr. phil. ELKE SCHLENKRICH, Historikerin

 Im Zentrum des Vortrages stehen in existenzielle Not geratene und von Obdachlosigkeit bedrohte unehelich Schwangere, die vor ihrer Niederkunft im Lazarett Zuflucht suchten. Dort wurde ihnen einerseits (medizinische) Versorgung zuteil, andererseits sollten sie sich hier als „Objekte“ für medizinische Untersuchungen zur Verfügung stellen.

14\.30 Uhr THEMATISCHER INPUT

**Mit Pflanzen verhüten. Über die Wiederentdeckung von Möglichkeiten selbstbestimmter Geburtenregelung**

 PEGGY BURIAN, Soziologin

15\.30 Uhr THEMATISCHER INPUT & DISKUSSION

**Medizin mit Heilpraktiken der Ahn:innen dekolonialisieren**

 CRISTIANA PARISI, Lehrerin zu dekolonialer Praxis 

 Cristiana Parisi wird ihren Vortrag in portugiesisch halten – mit deutscher Übersetzung.

16\.15 Uhr KURZFILM & GESPRÄCH

**Getty Abortions**

 von und mit FRANZIS KABISCH, künstlerische Forscherin, Filmemacherin und Autorin 

*2023 auf dem DOK Leipzig ausgezeichnet mit der Goldenen Taube*

 Wie sehen Abtreibungen aus? Was für Bilder prägen unsere Sicht darauf? Und woher kommen sie? Der Desktop-Video-Essay „getty abortions“ untersucht, wie deutschsprachige Medien das Thema Abtreibung illustrieren und klickt sich dabei durch Stockfoto-Datenbanken, BRAVO-Girl-Zeitschriften und private Dokumente einer echten Abtreibungserfahrung. Er springt von den frühen 2000ern ins späte 19. Jahrhundert, befragt feministische Wissensschätze und chattet mit fiktiven Figuren – eine Frage bleibt jedoch offen: Warum schaut eigentlich niemand in die Kamera?

16\.45 Uhr THEMATISCHER INPUT

**Schwangerschaftsabbruch – unser Recht, unsere Entscheidung! Hier und weltweit!** 

 pro choice [leipzig] Bündnis für sexuelle Selbstbestimmung

Anmeldung ist bitte bis zum **02\. 11. 2023** erforderlich – unter hallo@frauenkultur-leipzig.de oder 0341 – 2130030

**Ausführliche Informationen:** [**\-> bitte hier**](</media/fachtag_hx2023fin_web_1.pdf>)