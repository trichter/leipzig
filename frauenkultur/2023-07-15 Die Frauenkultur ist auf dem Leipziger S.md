---
id: NK1CFdlT
title: "Die Frauenkultur ist auf dem Leipziger Straßenfest des CSD "
start: 2023-07-15 11:30
end: 2023-07-15 18:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
LEIPZIGER CSD-STRASSENFEST & DEMO

## Das CSD-Motto 2023: The future is queer!

11\.30 Uhr beginnt die Kundgebung auf dem Augustusplatz. Die Demo starte 13 Uhr und endet 16 Uhr wieder auf dem Augustusplatz. Dann geht es dort weiter mit vielen spannenden Bühnen-Acts bis 20 Uhr.