---
id: 1m8bo6Yj
title: Frauen-Berufe und Frauen-Alltag im 13. bis 15. Jahrhundert in Mitteleuropa
start: 2023-04-23 15:00
end: 2023-04-23 17:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
## … und auch zur Zeit der Hexenverfolgungen in der Frühen Neuzeit, sowie über deren Folgen, die bis in die Gegenwart hineinreichen.

**VORTRAG MIT ERZÄHL-CAFÉ** für alle interessierten Menschen 

 … innerhalb des Projektes „Text, Bild & Stein: Sie erzählen uns unsere Geschichte. Rückmarsdorf 2023“

 Mit CHRISTINE RIETZKE, Frauenkultur Leipzig

Der Alltag der Menschen im Mittelalter war bis zum 14. Jh. in Mittel- und Westeuropa stark durch den jeweiligen gesellschaftlichen Stand bestimmt. In der Oberschicht wurden Ehen meist geschlossen, um Besitz oder Titel zu vererben. Für das ‚einfache Volk‘ war eine Eheschließung nicht üblich. Es gab zahlreiche Frauen, die allein lebten und sich ihren Unterhalt selbst erarbeiteten. Frauen waren in mehr als 200 bekannten Berufen tätig. Sie konnten als Mägde oder Tagelöhnerin auf dem Bau arbeiten (bis zu 30% der auf dem Bau Arbeitenden waren Frauen, die schon damals geringer entlohnt wurden). Frauen waren unterwegs z.B. als Händlerinnen, Gauklerinnen oder als Marketenderinnen, die kriegerische Heere begleiteten. In Frauenklöstern oder religiösen Laien-Schwesternschaften (wie z.B. den Beginen) erwarben Frauen Wissen und gaben es weiter. Handwerkerinnen organisierten sich in zunftähnlichen Gruppen (vor allem im Textilgewerbe); sie arbeiteten im Metall und Holzhandwerk. Die Verarbeitung von Milch und Käse sowie Backen und Brauen waren Tätigkeiten, die im frühen Mittelalter fast ausschließlich von Frauen betrieben wurden. Und der erste „verbriefte Müller“ in Leipzig war eine Müllerin, die bis 1392 die Mühle in Gohlis betrieb…

Fast alles daran änderte sich mit der Zeit der Hexenverfolgungen (1450 – 1750). In diesem multimedialen Vortrag werden neue Perspektiven auf die Rolle von Frauen im gesellschaftlichen Gefüge in der damaligen Zeit aufgezeigt – auch um *zu fragen*, wie sich diese bis heute entwickelt hat.

***Zum Projekt***

## Text, Bild & Stein: Sie erzählen uns unsere Geschichte. Rückmarsdorf 2023 

… ein Gemeinschaftsprojekt des Soziokulturellen Zentrums Frauenkultur Leipzig und des Heimatsvereins Rückmarsdorf

 … mit zahlreichen Veranstaltungen: Vorträgen, Workshops und Kreativangeboten

 … im Themenjahr des Dezernats Kultur: „**Leipzig. Die ganze Stadt als Bühne**“ – mit Schwerpunkt auf den Leipziger Ortschaften

 … um Orte neu zu entdecken, sichtbarer zu machen und um Gemeinschaft und ein gutes Miteinander zu stärken

**Ort: Ortsteilzentrum Rückmarsdorf, Ehrenberger Straße 5a**