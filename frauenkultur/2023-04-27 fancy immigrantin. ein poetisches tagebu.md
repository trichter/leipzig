---
id: vQZ8N3gg
title: "fancy immigrantin. ein poetisches tagebuch. "
start: 2023-04-27 21:00
end: 2023-04-27 22:15
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
21\.00 Uhr 

 LESUNG & GESPRÄCH \| w\_orten & meer

 zur Buchmesse 2023 *Leipzig liest* in der FraKu

Mit HATICE AÇIKGÖZ

Die Autorin schreibt in ihrem Buch sehr persönlich direkt aus dem Herzen einer Generation. Kindheits- und Jugenderinnerungen werden aufgearbeitet und der Blick wird gelenkt auf Scham und Schmerz, die durch eine rassistische Gesellschaft ausgelöst wurden.

Hatice Açıkgöz, veröffentlichte bisher Texte u.a. in „Literarische Diverse“. Ihr Buch ein oktopus hat drei herzen erschien 2022 bei SUKULTUR. fancy immigrantin wird bereits erschienene und noch nie veröffentlichte Texte enthalten, sowie Illustrationen von Irem Kurt. 

 Eintritt: nach Selbsteinschätzung