---
id: 0X34qoFa
title: Entdecke die Welt
start: 2023-07-24 10:00
end: 2023-07-24 16:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
## 23\. MÄDCHEN-SOMMER-ERLEBNIS-CAMP für Mädchen\* zwischen acht und 16 Jahren	

Gemeinsam wollen wir für vier Tage auf eine kleine Entdeckungsreise gehen, um das Nahe und das Ferne zu entdecken und kennenzu-lernen. Neben dem Störmthaler See und dem Botanischen Garten in Großpößna, wollen wir auch Musik und Essen aus dir vielleicht noch unbekannten Ländern erkunden.

## WO: Botanischer Garten Großpösna

Anmeldung unbedingt unter 0341-2130030 oder [hallo@frauenkultur-leipzig.de](<mailto:hallo@frauenkultur-leipzig.de>) bis **21\.07.2023** erforderlich