---
id: jilDlLig
title: Die Frauenkultur ist auf dem CONNEWITZER STRASSENFEST
start: 2023-05-14 11:00
end: 2023-05-14 18:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
Wir freuen uns, Euch am Stand der FraKu “Hallo zu sagen” …

*Info über den “genaueren Standort” folgen.*

## Ort des CONNEWITZER STRASSENFESTs ist wie immer: auf der *Selnecker*