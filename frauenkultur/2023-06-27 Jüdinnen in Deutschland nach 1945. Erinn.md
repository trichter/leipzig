---
id: gY6SArx5
title: Jüdinnen in Deutschland nach 1945. Erinnerungen, Brüche, Perspektiven
start: 2023-06-27 19:00
end: 2023-06-27 20:30
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
VORTRAG & DISKUSSION im Rahmen der Jüdischen Woche

 Eine Reihe im Deutschland Archiv der Bundeszentrale für politische Bildung \| [www.deutschlandarchiv.de](<https://www.deutschlandarchiv.de>)

 Mit SHARON ADLER, Berlin

## Facetten jüdischer Frauenidentitäten – Beiträge, Interviews und Porträts.

In dieser Reihe, die weibliches jüdisches Leben in Deutschland nach 1945 vorstellt, gibt Mitherausgeberin Sharon Adler einen Überblick über beispielhaft wegweisende jüdische Protagonistinnen und deren Arbeitsfelder sowie Frauenorganisationen im 20. Jahrhundert. Der Rückblick schlägt den Bogen über die Nachkriegszeit und die Situation der Überlebenden bis zum gesellschaftspolitischen Engage-ment und den Forschungs-, Film- und Ausstellungsprojekten jüdischer Frauen in der Gegenwart.

Sharon Adler, Publizistin und Fotografin, ist Gründerin und Herausgeberin des Online-Magazins für Frauen „AVIVA-Berlin“ (gegründet 2000) sowie seit 2013 ehrenamtlich tätig im Vorstand der Stiftung ZURÜCKGEBEN. Stiftung zur Förderung Jüdischer Frauen in Kunst und Wissenschaft (gegründet 1994). Mehr über das Online- und Buchprojekt „Jüdinnen in Deutschland nach 1945“ unter: [www.bpb.de/themen/deutschlandarchiv/318092/juedinnen-in-deutschland-nach-1945](<https://www.bpb.de/themen/deutschlandarchiv/318092/juedinnen-in-deutschland-nach-1945>).

 Eintritt: nach Selbsteinschätzung