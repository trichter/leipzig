---
id: jqzEu1s0
title: Roads – zwischen Düsseldorf und New Orleans
start: 2023-09-14 18:00
end: 2023-09-14 21:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
FILM & GESPRÄCH

**Mit der Filmemacherin JESSICA JACOBY**

Jessica Jacoby erzählt in „Roads – zwischen Düsseldorf und New Orleans“, dem ersten Teil ihres dreiteiligen Dokumentarfilms ihre Familiengeschichte: Ihre Großeltern väterlicherseits Arthur und Ella Jacoby (geborene Rosenthal) wurden 1941 nach Minsk deportiert und ermordet während Jessicas Vater Klaus in den USA vergeblich versuchte ihnen die Flucht zu ermöglichen.

In Auseinandersetzung mit ihrer Familiengeschichte und um das Schicksal ihrer jüdischen Großeltern vor dem Vergessen zu bewahren, trug sie Briefe und Tagebuchaufzeichnungen zusammen und interviewte überlebende Familienangehörige und ihre Nachkommen. Ihre Recherchen führten sie in die USA, Portugal und nach Südafrika – wo ihre Tante lebte, eine starke Frau und Vorbild, die sie zu dem Filmprojekt inspiriert hatte. Ihr Film folgt ihrem zentralen Anliegen: die Erinnerung an die Verbrechen der Nationalsozialisten wachzuhal-ten. Dies noch einmal umso mehr nach dem Attentat auf die Synagoge von Halle, das deutlich machte, wie aktiv die nationalsozialisti-sche Szene in Deutschland gegenwärtig ist – aber auch, wie wenig der Verfassungsschutz dagegen unternimmt. Der erschreckende Zuwachs von AFD-Wähler:innen und damit die Normalisierung von NS-Gedankengut in allen Teilen der Gesellschaft unterstreicht die Notwendigkeit um die Demokratie in Deutschland und Europa zu kämpfen einmal mehr.

JESSICA JACOBY, geb. 1954 in Frankfurt am Main, ist Historikerin, Filmjournalistin, Dokumentarfilm Autorin, Ausstellungsmacherin in Museen, Bildungsreferentin in Frauenprojekten, Interpretin jiddischer Lieder und Mitherausgeberin des Buchs „Nach der Shoa geboren. Jüdische Frauen in Deutschland” (1994). 1984 gründete sie in West-Berlin mit anderen Frauen den „Schabbeskreis”, eine lesbisch-feministische politische Gruppe, die sich bis 1989 aktiv für die Präsenz und Wahrnehmung jüdischer Frauen in der neuen Frauenbewegung und mit Antisemitismus in feministischen Zusammenhängen auseinandersetzte.

 Eintritt: nach Selbsteinschätzung