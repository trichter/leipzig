---
id: LxMISxsl
title: Heute ist Sommerfest!
start: 2023-06-04 15:00
end: 2023-06-04 18:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
## SOMMERFEST DER KULTURFABRIK LEIPZIG

Große und kleine Menschen sind herzlich eingeladen zum diesjährigen Sommerfest, das nach dreijähriger Pause, endlich wieder statt-finden kann. Es gibt fantastische Mitmach-Spiele, ein Open-Air-Sommerprogramm mit viel Musik, u.a. vom Chor „Singen bei uns“ und der Singer-/Songwriterin Anne Unger-Weise. Auch auf der kleinen Wiese an der Frauenkultur wird es spannend: Hier habt ihr viel Spaß an einem ganz besonderen Spiele-Parcours. Und alle mit Appetit auf echt leckeren Kuchen sind hier total richtig, denn der Kuchenbasar des Sommerfestes findet im Café der Frauenkultur statt! Also, wir sehen uns… Wir freuen uns auf euch… 

 Eintritt: frei

## Das Sommerfest wird von der gesamten Kulturfabrik mit den Vereinen Cammerspiele, Frauenkultur, HALLE 5 und WERK 2 veranstaltet.