---
id: qCs5Y1er
title: LO.Fe – Leipziger Osten Fest
start: 2023-06-17 16:00
end: 2023-06-17 20:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
Informations- und Begegnungsfest von und für Menschen aus aller Welt

## FraKu, MiO & FiA sind wieder mit Info’s und interaktiven Angeboten dabei …

Ort: Stadtteilpark Rabet, Konradstraße 26