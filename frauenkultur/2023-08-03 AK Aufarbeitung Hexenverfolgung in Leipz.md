---
id: 2pwqQqjV
title: "AK Aufarbeitung Hexenverfolgung in Leipzig | Sachsen. "
start: 2023-08-03 16:00
end: 2023-08-03 18:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
OFFENES ARBEITSTREFFEN

*Anliegen des Arbeitskreises:*

 Eine öffentliche Diskussion über das Benennen von nachweisbar begangenem Unrecht trägt auch die Möglichkeit in sich, gesellschaftliche Zusammenhänge damals wie heute klarer zu erkennen… und auch Formen der Rehabilitation von unschuldig ermordeten Frauen und Männern zu finden, selbst wenn dies viele hundert Jahre später stattfindet.

*Aufarbeitung der Hexenverfolgung in Leipzig/Sachsen – ein Arbeitskreis, ein Ausstellungsprojekt in progress…* 

 Alle Informationen zum AK [\-> hier](<https://www.hexenprozesse-leipzig.de/>)

**Alle Interessierten sind willkommen.** 

 Eintritt: frei