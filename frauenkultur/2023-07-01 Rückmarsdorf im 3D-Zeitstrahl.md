---
id: DUUwLYhs
title: Rückmarsdorf im 3D-Zeitstrahl
start: 2023-07-01 11:00
end: 2023-07-01 17:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
KREATIVE MIT-MACH-AKTION

 Mit den Künstlerinnen CLAUDIA BERNIG, KERSTIN KRIEG & Team FraKu

*Ein dreidimensionaler Zeitstrahl zur Rückmarsdorfer Geschichte, zum Leben hier über mehrere Jahrhunderte – und natürlich zum Leben in Rückmarsdorf im Heute … und mit einem kleinen Blick in die Zukunft.*

***Die Idee dieser Mitmach-Aktion:***

 Der Rückmarsdorfer Zeitstrahl soll vermitteln, was Menschen in Rückmarsdorf über Jahrhunderte so alles vollbracht haben… und noch vollbringen werden. Dafür werden bestimmte Ereignisse in Rückmarsdorf sehr kreativ gestaltet. Da es in der Rückmarsdorfer Geschichte so viele beeindruckende Ereignisse gibt, werden leider nicht alle im Zeitstrahl dargestellt werden können, sondern stellvertretend einzelne ausgewählte.

Wie zum Beispiel: **die erste urkundliche Erwähnung von Rückmarsdorf im Jahr 1285**, der Bau der **Schule am Kirchhof im Jahr 1544** oder natürlich der **Wasserturm auf dem Wachberg, erbaut 1914**. An der Zeitstrahl-Station für die „Schule am Kirchhof” könnte z.B. **ein Rückmarsdorfer ABC** entstehen.

## Wie der Zeitstrahl aber letztendlich aussehen wird, entscheiden alle, die den Zeitstrahl mit-/gestalten.

**Eingeladen sind alle, die am Rückmarsdorfer Zeitstrahl mit-/bauen möchten oder Ideen haben – von 0 bis 99 Jahren: die ganze Familie, solo, in Gruppe.** Es werden Bierbänke und Biertische da sein, wie auch Kaffee, Wasser, Saftschorle, Kekse und Kuchen. Und es werden viele nachhaltige Naturmaterialien genutzt wie Steine, Baumscheiben, Holz, Erden, Pflanzen, Erdfarben, Leinöl- und Quarkfarben, schnell trocknender Ton oder nicht mehr genutzte Gegenstände, alte Stoffe, Wolle und Stroh etc. … und für notwendige Vertiefungen haben wir *einen super Hand-Erdbohrer*. \| \| Teilnahme ist kostenlos

Im Rahmen des Gemeinschaftsprojektes des Soziokulturellen Zentrums Frauenkultur Leipzig und des Heimatvereins Rückmarsdorf „Text, Bild & Stein: Sie erzählen uns unsere Geschichte/n.“ im Rahmen des Themenjahres 2023 „Leipzig – Die ganze Stadt als Bühne”.

**Ort: An der Teichmühle 2, auf der Brach-Fläche vor dem Kindergarten am Heimatmuseum**