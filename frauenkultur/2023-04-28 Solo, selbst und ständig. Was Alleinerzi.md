---
id: aIIsSLwd
title: "Solo, selbst und ständig. Was Alleinerziehende wirklich brauchen "
start: 2023-04-28 18:00
end: 2023-04-28 19:15
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
LESUNG & GESPRÄCH \| Kösel-Verlag	

 zur Buchmesse 2023 *Leipzig liest* in der FraKu

Mit ANNE DITTMANN, Berlin

Sie ist eine der engagiertesten Stimmen zum Thema „Allein- und Getrennterziehend“. In ihrem Wut- und Mutmach-Buch liefert Anne Dittmann Tipps und Bestärkung, aber auch Ventile für Frust und Ängste. Sie holt Betroffene da ab, wo sie während oder nach einer Trennung stehen, bietet Empathie sowie erste Orientierung in einer neuen Lebensphase. Sie zeigt finanzielle, rechtliche und gesellschaftliche Benachteiligungen von Alleinerziehenden auf… und setzt all dem eine selbstbewusste Haltung entgegen.

Anne Dittmann, Autorin, Kolumnistin (solomuetter.de) & Journalistin, hat auf Instagram eine starke Community zu allein- und getrennt erziehende Mütter, lebt mit Kind zunächst allein-, mittlerweile getrennt erziehend im Wechselmodell. 

 Eintritt: nach Selbsteinschätzung