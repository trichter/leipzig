---
id: gmvQoEcI
title: Clara Zetkin - geliebt und gehasst. Eine umstrittene Sozialistin.
start: 2023-04-26 17:30
end: 2023-04-26 18:30
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
## Veranstaltungen zur Buchmesse 2023 *Leipzig liest*

LESUNG & GESPRÄCH \| Karl Dietz Verlag Berlin

 Mit der Autorin und Herausgeberin

 Dr. FLORENCE HERVÉ,

Die Spiegel-Edition 2022 über 75 “große Deutsche” bezeichnet Clara Zetkin als “eine der Wagemutigen und Wegbereiterinnen“. Als eine, die die Geschichte des Landes prägte oder bis heute inspiriert… und nicht nur als die, die den Internationalen Frauentag mit-erfunden hat. Vorgestellt wird eine biografische Annäherung an Clara Zetkin, unter besonderer Berücksichtigung ihres Wirkens und ihrer Rezeption in Frankreich und in Deutschland. Es geht unter anderem um Frauengeschichte, um das Recht auf Arbeit und Bildung, auf politische Betäti-gung und Selbstbestimmung, um Krieg und Frieden.

Dr. FLORENCE HERVÉ, Autorin, ist u.a. Herausgeberin von zwei Büchern mit biografischer Einführung zu und Texten von Clara Zetkin auf Französisch (2021, „Je veux me battre partout où il y a de la vie“) und auf Deutsch (4.Aufl. 2020, „Dort kämpfen, wo das Leben ist“). Sie erhielt neben anderen Auszeichnungen 2022 den Louise-Otto-Peters-Preis der Stadt Leipzig.

 Eintritt: nach Selbsteinschätzung