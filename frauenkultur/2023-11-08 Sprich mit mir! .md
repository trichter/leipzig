---
id: R8IWNCFp
title: "Sprich mit mir! "
start: 2023-11-08 15:00
end: 2023-11-08 17:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
SPRACHTANDEM

Ein Sprachtandem-Projekt der Frauenkultur Leipzig und des Museums der bildenden Künste Leipzig 

 für Frauen und Mädchen ab 14 Jahren.

Interessierte Frauen können an jedem 2. Mittwoch im Monat das Museum der bildenden Künste besuchen und im Sprachtandem dabei Sprache neu lernen. Eine deutsch-sprechende und eine deutsch-lernende Frau gehen gemeinsam durch das Museum – und erzählen einander, was sie sehen. Sie entdecken zusammen Bilder und lernen gemeinsam Deutsch oder auch eine andere Sprache.

## Heute: Vom Tochtersein

**Ort: Museum der Bildenden Künste Leipzig \| Katharinenstraße 10 \| 04109 Leipzig \| Die Teilnahme ist kostenlos.**