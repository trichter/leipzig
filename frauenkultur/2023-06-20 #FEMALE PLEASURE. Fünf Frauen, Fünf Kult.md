---
id: Kt7KGb9l
title: "#FEMALE PLEASURE. Fünf Frauen, Fünf Kulturen, Eine Geschichte"
start: 2023-06-20 19:00
end: 2023-06-20 21:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
FILM & GESPRÄCH

Preisgekrönter Film der Schweizer Regisseurin BARBARA MILLER; Ch. 2018

 Thematischer Input und Gespräch: 

 QUYNH ANH LE NGOC, Dolmetscherin, Übersetzerin & Moderatorin

> „Dieser Film hat mich dazu gebracht, Feministin zu werden.“

Fünf mutige, kluge und selbstbestimmte Frauen stehen im Zentrum von Barbara Millers Dokumentarfilm. Sie brechen das Tabu des Schweigens und der Scham, dass ihnen die Gesellschaft oder ihre religiösen Gemeinschaften mit ihren archaisch-patriarchalen Struktu-ren auferlegen. Mit einer unfassbaren positiven Energie und aller Kraft setzen sich Deborah Feldman, Leyla Hussein, Rokudenashiko, Doris Wagner und Vithika Yadav für sexuelle Aufklärung und Selbstbestimmung aller Frauen ein, hinweg über jedwede gesellschaftli-chen sowie religiösen Normen und Schranken. Dafür zahlen sie einen hohen Preis – sie werden öffentlich diffamiert, verfolgt, bedroht, von ihrem ehemaligen Umfeld werden sie verstoßen, von Religionsführern und fanatischen Gläubigen mit dem Tod bedroht.

\#FEMALEPLEASURE, ausgezeichnet u.a. 2019 mit dem Amnesty International Award, ist ein Film, der schildert, wie universell und alle kulturellen und religiösen Grenzen überschreitend die Mechanismen sind, die die Situation der Frau bis heute bestimmen. Gleichzeitig zeigen die fünf Protagonistinnen, wie man mit Mut, Kraft und Lebensfreude jede Struktur verändern kann. Wir möchten an diesem Abend ins Gespräch kommen zu dem benannten Unrecht und zu Möglichkeiten, dem entgegenzuwirken.

 Eintritt: frei