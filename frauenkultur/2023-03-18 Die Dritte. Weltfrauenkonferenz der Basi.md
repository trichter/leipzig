---
id: aWbjZKfF
title: Die Dritte. Weltfrauenkonferenz der Basisfrauen
start: 2023-03-18 16:00
end: 2023-03-18 17:30
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
VORTRAG & GESPRÄCH

450 Teilnehmerinnen aus 42 Ländern und vier Kontinenten debattierten vom 4.9.2022 bis 9.9.2022 in Tunis sechs Tage lang solidarisch, wissbegierig, streitbar und selbstbe-wusst in 31 Workshops und einem großen Abschlussplenum. Die Themen zentrierten sich um: Die Unterdrückung der Frauen weltweit, Frauenorganisationen, Frauen in Befreiungsbewegungen, Arbeiterinnen und ihre Kämpfe, Probleme und Kämpfe der Landfrauen, Frauen und Umwelt, Gewalt gegen Frauen in Kriegen sowie um junge Frauen.

Auch Leipzigerinnen waren dabei: Prof. Dr. GODULA KOSACK von TERRE DES FEMMES und ELKE PREETORIUS von COURAGE berichten von diesem einzigartigen Erleben des Empowerments von Frauen, die sich unter dem Motto versammelt hatten: 

 bq. „Lasst uns sein wie ein Olivenbaum: nach der Sonne greifend, stark, in der Erde fest verwurzelt: Keiner und nichts kann uns brechen.“

*Eine Veranstaltung, die explizit o.g. Themen im Kontext von Frauen aufzeigt.* 

 Eintritt: gegen Spende