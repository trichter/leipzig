---
id: rsSkq1xY
title: "Women at Work: „Leipzig braucht Dich!“ "
start: 2023-09-25 11:00
end: 2023-09-25 15:30
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
## 6\. FACHTAG von und mit Frauen mit Migrationsgeschichte 

Wie gelingt die Integration in den Arbeitsmarkt? Wer kann mich unterstützen, wenn es schwierig wird? Was ist das Erfolgsrezept von Frauen, die es geschafft haben? Wenn man nach Flucht oder Zuwanderung neu in Leipzig lebt, ist es oft schwer, sich zu orientieren und Wege in Arbeit oder Ausbildung zu finden. Beim Fachtag „women at work“ gibt es deshalb für Frauen viele Informationen über Bera-tungsangebote und Möglichkeiten, sich Unterstützung zu holen – unabhängig von Alter und Sprachraum.

Einzelne Frauen verschiedener Berufsgruppenerzählen davon, wie sie es in Leipzig geschafft haben, eine Ausbildung oder Arbeit zu finden. Sie wollen Mut machen, aktiv zu sein und den eigenen Weg zu gehen. In Workshop geht es um Weiterbildungsmöglichkeiten, die vom Jobcenter gefördert werden; um Berufsberatungsangebote und den Umgang mit Herausforderungen.

Der Besuch des Fachtages ist kostenlos. \| Mit Kinderbetreuung. \| Für Arabisch, Englisch, Farsi, Ukrainisch und Russisch wird bei Be-darf Sprachmittlung angeboten. \| Programm, Informationen, Anmeldung nicht erforderlich.

*Der Fachtag findet statt in Kooperation mit der Stadt Leipzig und zahlreichen Institutionen und Vereinen.* 

[https://www.leipzig.de/jugend-familie-und-soziales/auslaender-und-migranten/migration-und-integration/ausbildung-und-arbeit/women-at-work](<https://www.leipzig.de/jugend-familie-und-soziales/auslaender-und-migranten/migration-und-integration/ausbildung-und-arbeit/women-at-work>)

Ort: Werk2, Kochstr. 132, 04277 Leipzig