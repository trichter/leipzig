---
id: HkL3HwPN
title: War da was? Unterdrückung lesbischer Liebe.
start: 2023-10-19 18:00
end: 2023-10-19 19:30
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
VORTRAG & DISKUSSION innerhalb des 30. LeLe\*Tre’s 

 Diskriminierungserfahrungen gleichgeschlechtlich liebender Frauen in der BRD mit Dr. KIRSTEN PLÖTZ, Koblenz

Anders als Männer waren Frauen, die gleichgeschlechtlich liebten, im 20. Jahrhundert in der BRD nicht vom Strafrecht bedroht. Aber das heißt nicht, dass sie frei waren, lesbisch zu leben. Schlechte Berufsaussichten, Abwertung durch die Umwelt, fehlende Vorbilder sind nur einige der Stichworte. Wichtig war auch das Recht, das die Folgen einer Ehescheidung regelte – offen lesbisch liebende Mütter verloren bis in die 1990er Jahre hinein das Sorgerecht für ihre Kinder. Die bundesweit erste Studie darüber stellt die Historikerin Dr. Kirsten Plötz in der Veranstaltung vor. Auch andere Formen der Unterdrückung kommen zur Sprache.

Dr. KIRSTEN PLÖTZ studierte Geschichte und Politik an der Universität Hannover. Seit den 1990er Jahren arbeitet sie über Geschlech-tergeschichte, vor allem über lesbisches Leben. Darüber forscht sie als freie Historikerin in verschiedenen westdeutschen Bundeslän-dern. Mehr unter [https://die-andere-biografie.de](<https://die-andere-biografie.de>) 

 Eintritt: nach Selbsteinschätzung

**Das gesamte Programm des 30. LeLe\*Tre’s ->[siehe bitte hier](</angebote/aktuelle-projekte/leletre-leipziger-lesbentreffen/>)**