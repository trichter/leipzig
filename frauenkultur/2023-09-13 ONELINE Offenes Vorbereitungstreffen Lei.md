---
id: o9j16O4R
title: "ONLINE: Offenes Vorbereitungstreffen: Leipziger Frauen*/FLINTA*-Festival
  2024 "
start: 2023-09-13 17:00
end: 2023-09-13 19:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
**ONLINE-MEETING**

Die öffentlichen sexistischen Gewalttaten gegenüber Frauen\*/FLINTA\*s in den vergangenen Jahren weltweit (auch in Deutschland) sind erschütternd – und zeigen die Unabdingbarkeit von breiten Bündnissen feministisch & zivilgesellschaftlich humanistisch handelnden Grup-pen auf. Es braucht unterschiedlichste Formate und Mittel konsequenter Aktivitäten gegen jede Form von alltäglichem Sexismus, FLINTA\*- und Fremdenfeindlichkeiten.

Das Leipziger Frauen\*/FLINTA\*-Festival – **am 04. Mai 2024 \| 14 – 22 Uhr auf dem Leipziger Markplatz** – ist ein öffentliches Format, um Bündnisse zu vertiefen, positionierte Diskurse öffentlich partizipativ zu führen, unerlässliche Forderungen zu stellen, Perspektiven und Ideen zu teilen und neue gerichtete Formen des gemeinsamen feministischen Handelns zu entwickeln.

Dazu möchten wir feministischen Gruppen, Kollektive und interessierten Aktivistinnen\* einladen – gemeinsam das Sechste Leipziger Frauen\*/FLINTA\*-Festival inhaltlich mitzugestalten.

## Online-Teilnahme-Link: unter [www.frauenkultur-leipzig.de](<https://www.frauenkultur-leipzig.de>)

Ausführliche Infos *folgen* unter [www.leipzigerfrauenfestival.de](<https://www.leipzigerfrauenfestival.de>)