---
id: kCGw9onY
title: Fällt leider aus! Afro-Latin-Fitness mit Live Musik
start: 2023-05-20 10:00
end: 2023-05-20 12:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
## TANZ-WORKSHOP für Frauen\* … fällt leider aus!

mit CLAUDIA CASTAÑEDA; Leiterin der Tanz-Performancegruppe “Madamtamtam”

Der Empowerment-Workshop richtet sich an Frauen, besonders an Frauen mit Migrationsgeschichte, die sich gerne bewegen möchten zu westafrikanischer Tanz- und Trommelmusik. Sie sind alle herzlich eingeladen, mit Hilfe des Tanzes Stress abzubauen – und sich von Energie und Lebensfreude anstecken zu lassen. Anmeldung erwünscht. 

 Eintritt gegen Spende

**Nächster Workshop: 24.06.2023**