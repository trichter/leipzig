---
id: zWfhydjq
title: Die Schere im Kopf
start: 2023-09-07 19:00
end: 2023-09-07 21:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
AUSSTELLUNGSERÖFFNUNG

 Berührende Malerei der Künstlerin Dr. MIRETTE BAKIR

> „In meiner Kindheit in Kairo lagen oft die Burda-Modemagazine zum Selberschneidern zu Hause. Manchmal hatte sie jemand aus Europa mitgebracht, manchmal waren es die arabischen Ausgaben der Zeitschrift, die zwar die gleichen Kleider zeigten, aber völlig anders aussahen: Die Gesichter der Modelle, ihr Haar und alle nackten Körperstellen waren mit schwarzen Balken überklebt – die Frauen wirkten auf mich wie ihres Kopfes und ihrer Glieder beraubt. Was hatte die Schere aus ihnen gemacht? Sie war zu einem Zensor geworden, der alle Attribute von Weiblichkeit und eigenem Denkens beschnitten hatte. Was bedeutet Zensur für mich als Künstlerin zwischen verschiedenen Kulturen mit ihren ganz verschiedenen Wirklichkeiten und Tabus, wer setzt eigentlich *die Schere* an – kommt sie von außen mit öffentlichen Verboten, Regeln der Kontrolle, mit Überwachen und Strafen, oder sitzt sie im eigenen Kopf? Zensieren wir uns möglicher-weise nicht immer schon selbst mit allem, was wir sagen und (nicht) tun im vorauseilenden Gehorsam an die Erwartungen, die „man“ von außen an uns hat? Kunst ist für mich der Raum, um über die verschiedenen Dimensionen der Zensur – der Schere im Kopf – nachzudenken.“ (Mirette Bakir)

Dr. MIRETTE BAKIR (geb. 1980 in Kairo) studierte Kunst und Design an der Fakultät der angewandten Künste der Helwan-Universität Kairo und promovierte an der Bauhaus-Universität Weimar. In der Zeit zwischen 2003 und 2006 hatte sie einen Lehrstuhl für Kunst & Design an der Helwan-Universität Kairo inne. 2007 und 2008 studierte sie an der Hochschule für Grafik und Buchkunst (HGB) als Gaststudentin Grafik und Video Art. Sie lebt und arbeitet in Leipzig.

Mirette Bakir bewegt sich *zwischen* wie auch *in* zwei Kulturen. Sie hat ihren ganz eigenen Blick auf Kunst als Reflektion von vergangener wie auch gegenwärtiger Lebenswirklichkeit entwickelt. Sie beschäftigt sich mit den Beziehungen zwischen den verschiedenen Sichtweisen, den Ebenen der Wirklichkeit und der Ambivalenz unserer emotionalen Existenz. Mirette Bakir versucht, die Komplexität wie auch die Einfachheit unserer Welt durch ihre figurative Malerei zu zeigen. Mirette Bakir ist eine politische Künstlerin, die sich seit 2011 für die Umsetzung demokratischer Ziele der ägyptischen Revolution aktiv engagiert.

Musikalische Eröffnung der Ausstellung: SILVIA NEEDON, Percussion, Fiedel