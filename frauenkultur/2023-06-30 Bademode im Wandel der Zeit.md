---
id: La65RKul
title: Bademode im Wandel der Zeit
start: 2023-06-30 15:00
end: 2023-06-30 17:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
TEE & INTERKULTURELLES GESPRÄCH

 Diese Veranstaltungsreihe ist ein Angebot der soziokulturellen Begegnungen im Leipziger Osten. Bei einer Tasse Tee kommen Frauen aus verschiedenen Ländern zu ausgewählten Themen miteinander ins Gespräch.

## Heute: Bademode im Wandel der Zeit

Ein gemeinsamer Blick auf die Entstehung und Entwicklung der Bademode für Frauen. Welche Unterschiede gab und gibt es in ver-schiedenen Kulturen? Woher kommen Einflüsse und was kann Badebekleidung bewirken? Was gefällt uns und was sind wir gewohnt…?

Diese Veranstaltung der Volkshochschule Leipzig im Bereich der Politischen Bildung ist entgeltfrei – und findet statt in Kooperation mit dem Soziokulturellem Zentrum Frauenkultur \| Anmeldung erwünscht. 

 Eintritt. frei

**Ort: FiA, Konradstr. 62, 04315 Leipzig**