---
id: G5nvCP6L
title: Wir wollen gleichberechtigt sein! Das ist Fair-Sein!
start: 2023-11-17 15:00
end: 2023-11-17 16:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
## Demonstration *zu mehr Geschlechtergleichheit* von sehr jungen Akteuerinnen

Eine Gruppe von Schülerinnen einer Grundschule in Connewitz wird eine Demonstration zum Thema “Gleichberechtigung” durchführen. 

 Da wir im Vorfeld mit den Kindern der 3. und 4. Klasse einen kleinen Workshop zum Thema „Wie machen wir eine öffentliche Demo auf der Straße?“ durchgeführt haben – sind wir natürlich auch mit dabei :) Die Workshop-Anfrage kam von den jungen Schülerinnen selbst.

**Also, alle, die Lust und Zeit haben, diese Demo der “sehr jungen Aktivistinnen” zu unterstützen – sind ganz herzlich eingeladen, sie zu begleiten.**

Die Route ist nach einer kurzen Anfangskundgebung vom **Bayerischen Platz bis zum kleinen Wilhelm-Leuschner-Platz** (vorbei an der Stadtbibliothek) geplant.