---
id: dpcQ12Zp
title: Erzählen, Zu-/hören & Teilen ... Rückmarsdorf 1989
start: 2023-09-23 15:00
end: 2023-09-23 17:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
## EIN RUNDGANG *zu*

Dieser Rundgang ist wie ein Zeitensprung – in eine Zeit vor über 30 Jahren. **Es geht nicht um „Ostalgie oder darum einen Unrechtsstaat „zu verklären“ – sondern um all das, was heute für uns, für unsere Kinder, unsere Enkelkinder bewahrt werden sollte**.

Die Geschichte eines Landes sind immer auch die Geschichten der Menschen. Wenn man diese kennt, kann *man das Heute ein Stück besser* begreifen. Und darum geht es bei diesem Rundgang. Auf diesen Rundgang wollen wir uns an ausgewählten Rückmarsdorfer Orten gemeinsam erinnern und persönlich Erlebtes erzählen… von einer „Kinderkommission“, die zum Pfarrer ging, um einen Spielplatz zu erhalten / von Kamille, die an Feldern gesammelt wurde, die heute bebaut sind / von einem Sommerkleid aus selbst gefärbtem Baumwoll-Windeln, da es in den HO-Läden keine leichten Baumwoll-Kleider gab / vom Umgang der Menschen miteinander im DDR-Mangel-Alltag … und von anderem mehr. Und natürlich wollen wir auch einen Bezug zum Heute ziehen: Wie lebe ich heute? Was hat sich verändert? ..auch in Rückmarsdorf? Und was würde ich mir (noch) wünschen?

**Eingeladen sind alle interessierten Menschen! … die Älteren *mehr* zum Erzählen & Zu-/Hören… die Jüngeren *mehr* zum Zu-/Hören & Fragen.**

## Start \| Beginn:

**An der Kirche, \| Alte Dorfstraße 2 \| 04178 Leipzig**