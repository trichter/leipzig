---
id: aFk7Qbvz
title: 'Lese-Lust: Vorstellung von "Lieblingsbüchern" '
start: 2023-10-04 11:00
end: 2023-10-04 13:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
LITERARISCHER STAMMTISCH für Interessierte jeden Alters

Eingeladen sind alle, die Freude am Lesen haben und gerne darüber debattieren möchten. Einmal im Monat kann zugehört oder selbst etwas vorgestellt werden. Und mit Begeisterung gelesen wird alles: vom Historischen Roman über Biografien bis zu ultramoderner Literatur. 

 Leitung: S. SOMMER