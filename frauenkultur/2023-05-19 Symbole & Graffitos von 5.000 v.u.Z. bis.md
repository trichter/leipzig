---
id: 7JA4Btn6
title: Symbole & Graffitos von 5.000 v.u.Z. bis heute
start: 2023-05-19 14:00
end: 2023-05-19 17:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
## GRAFFITI-WORKSHOP für Jugendliche

Workshop-Leitung: 

 Künstler:innen des Graffitiverein Leipzig; [www.graffitiverein.de](<https://www.graffitiverein.de>)

Was ist eigentlich ein Graffiti? Zählen Stein- und Felszeichnungen früherer Jahrtausende auch dazu? Welches Symbol finden wir heute für unsere eigenen Anliegen und Ideen, für die wir aktiv werden wollen? Um dies zu entwickeln und auszuprobieren gibt es hier die Gelegenheit. Vermittelt werden verschiedene Graffiti-Techniken – und jede:r sprüht ein eigenes Kunstwerk, dass auch mit nach Hause genommen werden kann. Anmeldung erforderlich.

*Im Rahmen des Gemeinschaftsprojektes „Text, Bild & Stein: Sie erzählen uns unsere Geschichte. Rückmarsdorf 2023“ des Soziokulturellen Zentrums Frauenkultur Leipzig und des Heimatsvereins Rückmarsdorf*

Die Teilnahme ist kostenfrei.

## Ort: Ortsteilzentrum Rückmarsdorf, Ehrenberger Straße 5a