---
id: MBAStAdA
title: "Altes Handwerk wie im Mittelalter kennenlernen und ausprobieren.  "
start: 2023-09-10 15:00
end: 2023-09-10 18:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
## Weben, Korb-Flechten & Sandstein behauen.

**Weben**

 Das Weben gehört, nach Holz- und Steinbearbeitung, zu den ältesten Handwerken der Menschheit. In den Grabkammern des ägypti-schen Altertums sind Gewebereste von Gewändern nachgewiesen worden. Weben braucht relativ wenige Hilfsmittel. An diesem Sep-tember-Wochenende entsteht in einem großen Web-Rahmen aus alten Stoffresten ein gemeinsam gewebter bunter Flickenteppich. Und aus Stöcken, Fäden und Pflanzenteilen können in kleinen Rahmen ganz eigene künstlerische Web-Kreationen entstehen. **Mit den Künstlerinnen ASYA ALHAJI und CLAUDIA BERNIG**

**Korb-Flechten** 

 Flechten gehört wie Weben zu den ältesten handwerklichen Tätigkeiten der Menschheit und ist auf der ganzen Welt verbreitet. Wahr-scheinlich ist Flechten sogar älter als Weben, da es nur mit den Händen ausgeführt werden kann und von großem Nutz-Wert war. **Die Weidenfrau ANTJE HÖVEL** möchte altes Handwerk an diesem Wochenende in die neue Zeit bringen. Sie verbindet in ihrer Arbeit das Handwerk Flechttechnik mit kreativ künstlerischer Gestaltungsmöglichkeiten, z.B. für ein ganz besonderes geflochtenes Körbchen. \|\| [https://www.weidenfrau.de/](<https://www.weidenfrau.de/>)

**Arbeiten mit Sandstein**

 Sandstein entsteht durch natürliche Verfestigung von lockerem Sand und Trümmern verwitterter und abgetragener Gesteine – und braucht für die Entstehung zwischen wenigen Jahrzehnten und mehreren Millionen Jahren. **Die Bildhauerin KERSTIN KRIEG** baut an diesem Wochenende eine mobile Werkstatt auf dem Wachberg. Kinder und Erwachsene können sich allein und gemeinsam ausprobie-ren und dabei experimentell, handwerklich und erzählerisch Beschaffenheit, Eigenheit und Ausdruckskraft von Sandstein kennenlernen. \|\| [https://www.kerstinkriegsandsteine.com/](<https://www.kerstinkriegsandsteine.com/>)

**Ort 1:	An der Teichmühle 2, 04178 Leipzig- Rückmarsdorf auf der Brach-Fläche vor dem Kindergarten**

**Ort 2:	Sandstein behauen – am Wasserturm auf dem Wachberg nur am So.,10. 09. 2023 am Tag des offenen Denkmals**

*Veranstaltet in Kooperation mit dem Heimatverein Rückmarsdorf im Themenjahr 2023 „Leipzig – Die ganze Stadt als Bühne“.*

 Alle interessierten Menschen jeden Alters \| Familien sind herzlich eingeladen. \|\| Eintritt: frei