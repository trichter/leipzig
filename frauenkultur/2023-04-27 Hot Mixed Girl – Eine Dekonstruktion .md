---
id: ka28bzyn
title: "Hot Mixed Girl – Eine Dekonstruktion "
start: 2023-04-27 19:00
end: 2023-04-27 20:15
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
LESUNG & GESPRÄCH \| Unrast-Verlag

 zur Buchmesse 2023 *Leipzig liest* in der FraKu

Mit Dr. NAT ISABEL

Schön – exotisch – der Anfang vom Ende: in jedem Fall eine Provokation. Klar ist, dass ein Hot Mixed Girl die Gemüter erhitzt. Aber wer ist dieses Hot Mixed Girl eigentlich? Und woraus ist diese zeitgenössisch anmutende Projektionsfläche für jahrhundertealtes rassistisches Gedankengut und koloniale Denkmuster gerade in Deutschland historisch und politisch erwachsen? In einer systematischen Dekonstruktion komplexer Mechanismen untersucht Nat Isabel, welche Bedeutungen dem weiblichen afro-diasporischen Körper beigemessen werden – reflektiert die Auswirkungen auf dessen Seele. Mit Tiefgang und Schonungslosigkeit leitet sie zu radikalen Perspektivwechseln an – deckt Wege des Widerstands auf. Dabei zeigt sie – auch anhand ihrer eigenen Geschichte – wie eine Identitätsfindung in einem Drahtseilakt zwischen prekären Privilegien und gegensätzlichen und desorientierenden Fremdzuschreibungen dennoch gelingen kann.

NAT ISABEL ist gebürtige Kölnerin – mit deutschen, omani und kongolesischen Wurzeln – und hat an der University of Leicester im Bereich der Systemtheorie promoviert. 

 Eintritt: nach Selbsteinschätzung