---
id: lgozQluf
title: "Der Mitbring-Sonntagsbrunch "
start: 2023-10-22 11:00
end: 2023-10-22 13:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
SONNTAGSBRUNCH zum 30. LeLe\*Tre

Zum Festival- und LeLe\*Tre-Ausklang freuen wir uns auf Euch und alles, was eure Küche hergibt: Aufstriche, Salate, Kuchen… von süß bis herzhaft, von einfach bis raffiniert – alle Speisen sind willkommen! Sie müssen nur aus mindestens drei Zutaten bestehen (Gewürze zählen nicht…) Und wer fürs „Selber-Zubereiten“ nicht bereit ist, kann für einen kleinen Unkostenbeitrag dabei sein. Alle haben die Mög-lichkeit, mitabzustimmen für das beste herzhafte Gericht und die verführerischste Süßspeise. Gewinner:innen dürfen sich auf den „Scharfen Kuss“ oder das „Süße Hüft-Gold“ freuen. Genießt den Brunch! … zusammen schmeckt es einfach besser. Und für Kids gibt es wieder einen kleinen Spiele-Parcours! 

 Eintritt: Naturalien (siehe Text) oder nach Selbsteinschätzung

**Das gesamte Programm des 30. LeLe\*Tre’s ->[siehe bitte hier](</angebote/aktuelle-projekte/leletre-leipziger-lesbentreffen/>)**