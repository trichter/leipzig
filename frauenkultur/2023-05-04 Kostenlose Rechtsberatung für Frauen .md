---
id: AS6NKeY0
title: "Kostenlose Rechtsberatung für Frauen* "
start: 2023-05-04 18:00
end: 2023-05-04 20:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
mit der Rechtsanwältin Tanja Müller-Tegethoff

 Die Rechtsanwältin Tanja Müller-Tegethoff bietet Beratung für Frauen zu rechtlichen Problemen an. Die Beratung erfolgt vertraulich und kostenfrei.

 Wichtig: Vorherige Anmeldung unter 0341 – 2130030 ist notwendig.