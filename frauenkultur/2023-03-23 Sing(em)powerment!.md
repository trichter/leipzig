---
id: Feayowaz
title: Sing(em)powerment!
start: 2023-03-23 19:00
end: 2023-03-23 21:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
Chor-Workshop für Migrant:innen und BIPoC

 Leitung/ Workshoplead: SHIRA BITAN & GAL LEVY

*Ein Workshop des Vereins ZEOK im Rahmen der Leipziger Internationalen Wochen gegen Rassismus*

Chor-Beginners-Workshop von und für Migrants & BIPoC. Das Anliegen ist, einen Raum zur (Selbst-)Stärkung zu bieten. Einen Safer Space, in dem sich alle wohl und sicher genug fühlen, um ihre Stimme hören zu lassen und etwas Neues auszuprobieren. Singen ist Körper ist Emotion ist Technik ist Stimme ist Selbsterfahrung, kann Ventil, Energiequelle und Trost sein. Im Chor Singen braucht Verbundenheit und Achtsamkeit. Es bringt Freude, Mut und Gemeinschaft. Der Workshop beinhaltet Atmung, Stimmbildung, Gesangsübungen, mehrstimmiges Singen.

**Wer nach dem Workshop weitersingen möchte, ist bei ChorAlle, dem inter- und transkulturellen Chor-Projekt von ZEOK e.V. herzlich willkommen!**

Choir Beginners Workshop by and for Migrants & BIPoC. The intention is to provide a space for (self-)empowerment. A safe space where everyone feels comfortable and safe enough to let their voice be heard and try something new. Singing is body is emotion is technique is voice is self-awareness, can be outlet, source of energy and comfort. Singing in a choir requires connection and mindfulness. It brings joy, courage and community. Contents of the workshop are breathing, voice training, singing exercises, polyphonic singing.

**Those who would like to continue singing after the workshop are welcome to join ChorAlle, the inter- and transcultural choir project of ZEOK e.V.**

 Eintritt frei-Free entry \| Anmeldung-Registration via Email an i.herling@zeok.de