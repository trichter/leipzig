---
id: QvcxriQj
title: Lesbische Community in Leipzig – in den 80er und 90er Jahren
start: 2023-10-21 15:30
end: 2023-10-21 19:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
ZEITSPRÜNGE MIT KAFFEE & SELBSTGEBACKENEM KUCHEN innerhalb des 30. LeLe\*Tre’s

… zu diesem Thema fand am 04.02.2023 eine erste Diskussionsrunde in der Frauenkultur statt. Und es war klar: dieses Gespräch muss fortgesetzt werden. Vieles wurde gemeinsam zusammengetragen…einige Fragen blieben offen. Heute werden die Ergebnisse vorge-stellt – allen, die zu Beginn der 1990er Jahre bei der Gründung lesbischen Gruppen in Leipzig dabei waren, aber auch allen anderen am Thema Interessierten. Und es kann & soll diskutiert werden:

## Was hat sich geändert? Wie und wo treffen sich lesbische, queere Frauen heute in Leipzig? Wo sind Gemeinsamkeiten zwischen den Generationen, wo Unterschiede?

**Das gesamte Programm des 30. LeLe\*Tre’s ->[siehe bitte hier](</angebote/aktuelle-projekte/leletre-leipziger-lesbentreffen/>)**