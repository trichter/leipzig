---
id: t2gtgfkz
title: Kostenlose Rechtsberatung rund um das Thema „Grad der Behinderung“
start: 2023-11-07 16:30
end: 2023-11-07 17:30
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
KOSTENLOSE RECHTSBERATUNG. 

 mit der Rechtsanwältin ANNA VORWERG

Bei bestehenden Krankheiten und Behinderungen an der Teilhabe im Alltags- oder Erwerbsleben kann es sinnvoll sein, beim Versorgungsamt die Feststellung eines sogenannten „Grades der Behinderung“ zu beantragen. In der kostenfreien Beratungsstunde möchte Rechtsanwältin Anna Vorwerg Unsicherheiten bei der Antragsstellung nehmen und hilfreiche Tipps geben. **Am 07.11.2023 eine Stunde später: von 16.30 bis 17.30 Uhr**

**Jeden 1. Dienstag im Monat \| Anmeldung unbedingt erforderlich! (mindestens ein Tag vorher)**