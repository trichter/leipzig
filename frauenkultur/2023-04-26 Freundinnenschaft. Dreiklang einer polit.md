---
id: jZIQJMtm
title: Freund*innenschaft. Dreiklang einer politischen Praxis | Unrast-Verlag
start: 2023-04-26 19:00
end: 2023-04-26 20:30
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
LESUNG & DISKUSSION

 zur Buchmesse 2023 *Leipzig liest* in der FraKu

Mit MARÍA DO MAR CASTRO VARELA & BAHAR OGHALAI 

 Gerechtigkeit ist das Ziel, doch mit wem ist dieses zu erreichen? Es handelt sich dabei keineswegs um eine Randfrage, die neben den angeblich zentralen politischen Diskussionen gestellt wird, sondern um ein explizit politisches Thema, das im Zentrum einer jeden sozialen Bewegung steht. Es lohnt sich hier über einen anderen Begriff der politischen Praxis nachzudenken: Freund\*innenschaft, meist ins Private verdonnert und aus dem politischen Terrain verdammt. Dabei sind Freund\*innenschaften genuin politische Beziehungen, deren grundlegende Charakteristika unseren Blick auf die Welt transformieren. In Zeiten multipler Krisen und wachsender globaler Ungleichheiten, verfolgt dieser Essay bereits bestehende Theorien zur Freund\*innenschaft, um diese anschließend für einen Begriff der Freund\*innenschaft als politische Praxis produktiv zu machen.

MARÍA DO MAR CASTRO VARELA, Dipl.-Psychologin Dipl.-Pädagogin und promovierte Politikwissenschaftlerin. Sie ist Professorin für Allgemeine Pädagogik und Soziale Arbeit an der Alice Salomon Hochschule Berlin; Gründerin und Mitglied des bildungsLab\* und Vorsitzende des Instituts für kontrapunktische Gesellschaftsanalysen.

 BAHAR OGHALAI (M.A.) ist Sozialwissenschaftlerin. Seit 2019 forscht und lehrt sie an diversen Forschungs- und Universitätseinrichtun-gen, darunter dem Deutsche Zentrum für Integrations- und Migrationsforschung und der Alice-Salomon-Hochschule.

 Eintritt: nach Selbsteinschätzung