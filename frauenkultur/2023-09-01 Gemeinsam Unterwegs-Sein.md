---
id: yumniRcq
title: Gemeinsam "Unterwegs-Sein"
start: 2023-09-01 13:00
end: 2023-09-01 17:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
## TAG DER OFFENEN TÜR & PICKNICK im Rahmen der OSTLichter

**Ort: FiA & MiO, Konradstraße 62 & 64, 04315 Leipzig**

Heute öffnen die Frauen-Beratungsstelle FiA – Frauen in Arbeit und der Interkulturelle Offene Mädchentreff MiO ihre Türen und heißen alle interessierten Frauen\* und Mädchen\* willkommen, sie kennenzulernen. Denn das “gemeinsame Unterwegs-Sein” ist die beste Voraussetzung für ein wertschätzendes Miteinander. Ab 13 Uhr mit Tee, Gesprächen und Spieleangeboten auf der Konradstraße. Um 15 Uhr spazieren alle gemeinsam in den Mariannenpark, um dort zu picknicken. Essen & Trinken können gern mitgebracht werden.