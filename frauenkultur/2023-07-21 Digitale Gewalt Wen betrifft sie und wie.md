---
id: LO6Zqrof
title: "Verschoben! Digitale Gewalt: Wen betrifft sie und wie geht Politik damit um?"
start: 2023-07-21 19:00
end: 2023-07-21 20:30
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
## Update: Muss leider aus persönlichen Gründen der Referentin verschoben werden. Der neuen Veranstaltungstermin wird zeitnah hier bekannt gegeben werden.

VORTRAG & DISKUSSION

 mit ANNE ROTH

 Referentin für Netzpolitik der Linksfraktion im Bundestag

Häusliche und geschlechtsspezifische Gewalt finden durch die Digitalisierung unseres Alltags zunehmend auch ‘online’ statt. Social Media, Messenger Apps und Smart-Homes bieten neue Angriffsflächen für Täter und stellen damit eine Gefahr für Betroffene dar. Zu häufig wird diese Gewalt nicht ernst genommen oder als ein rein individuelles Problem verkannt. Wie die aktuelle Lage aussieht und was politische Lösungen sein könnten erläutert Anne Roth in ihrem Vortrag.

ANNE ist Referentin für Netzpolitik der Linksfraktion im Bundestag. Sie beschäftigt sich seit vielen Jahren mit digitaler Gewalt und dar-über hinaus mit Überwachung, Sicherheitsbehörden und feministischer Netzpolitik.

 Eintritt: nach Selbsteinschätzung