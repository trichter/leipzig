---
id: IgkHlaCV
title: Kostenlose Rechtsberatung rund um das Thema „Grad der Behinderung“
start: 2024-01-09 15:30
end: 2024-01-09 16:30
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
KOSTENLOSE RECHTSBERATUNG. 

 mit der Rechtsanwältin ANNA VORWERG

Bei bestehenden Krankheiten und Behinderungen an der Teilhabe im Alltags- oder Erwerbsleben kann es sinnvoll sein, beim Versorgungsamt die Feststellung eines sogenannten „Grades der Behinderung“ zu beantragen. In der kostenfreien Beratungsstunde möchte Rechtsanwältin Anna Vorwerg Unsicherheiten bei der Antragsstellung nehmen und hilfreiche Tipps geben. Anmeldung ist bitte bis **08\.01.2024** erforderlich!

**Jeden 1. Dienstag im Monat (außer Januar 2024\| Anmeldung unbedingt erforderlich! (mindestens ein Tag vorher)**