---
id: xqT4medK
title: Selbstverteidigung, Gesprächsrunde & gemeinsames improvisiertes Lied
start: 2023-10-21 11:00
end: 2023-10-21 15:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
EMPOWERMENT-WORKSHOP für Frauen\*/FLINTA\* mit Migrationserfahrung innerhalb des 30. LeLe\*Tre’s 

 Sprachen: Englisch und Spanisch

Dieser Workshop besteht aus zwei Teilen. In der ersten Phase werden wir einige grundlegende Tools der Selbstverteidigung aus der Perspektive des Karate vermitteln. Geleitet wird dieses Karate-/Selbstverteidigungstraining von NATALIA VERDEJO (Schwarzer Gürtel 2. Dan, Jundokan Okinawa). Es sind keine Karate-Vorkenntnisse erforderlich! Danach folgt eine offene Gesprächsrunde & gemeinsames Singen. Moderiert wird dieses Zusammen-Sein von MARIA CRISTINA GATICA, kulturelle Trainerin.

 —————————————————————————————————————————————————————————————-

EMPOWERMENT-WORKSHOP para Mujeres y disidencias (FLINTA) con experiencia migratoria

 Defensa personal, ronda de conversaciones y canción improvisación abierta.

 Idiomas: Inglés y Español

El presente Workshop contará con dos etapas. En la primera, entregaremos herramientas básicas de defensa personal desde la per-spectiva del karate, a través de una clase de Karate/Defensa Personal, impartida por NATALIA VERDEJO (Cinturón negro 2° dan, Jundokan Okinawa). No se necesita experiencia previa en Karate!

 Continuaremos con una ronda de conversación abierta, donde compartiremos experiencias, recomendaciones y dialogaremos libre-mente desde una perspectiva como mujeres, inmigrantes y lesbianas en Leipzig. Tendremos mate para compartir en la ronda! 

 Finalizaremos la ronda, cantando e improvisando una canción en conjunto. La segunda parte estará moderada por la facilitadora cul-tural MARÍA CRISTINA GATICA.

- Requisito: llevar ropa cómoda
- Trae tu instrumento/voz lista para la improvisación
- Abierto para todas las edades

 \*\*Se recibe aporte solidario

<!-- -->

—————————————————————————————————————————————————————————————-

EMPOWERMENT-WORKSHOP for women and LGBTQIA+ (FLINTA) with international background 

 Self-defense, circle of conversations and open improvisation song.

 Languages: English and Spanish

 The present Workshop will have two phases. In the first one, we will provide basic self-defense tools from the karate perspective, through a Karate/Self-Defense class, given by NATALIA VERDEJO (Black Belt 2nd dan, Jundokan Okinawa). No previous Karate expe-rience is required! We will continue with an open circle of conversation, where we will share experiences, recommendations and dia-logue openly from our perspective as women, immigrants and lesbians in Leipzig. We will have mate to share in the round! We will end the activity, singing and improvising a song together. The second part will be moderated by the cultural facilitator MARÍA CRISTINA GATICA. - Requirement: wear comfortable clothes
- Bring your instrument/voice ready for improvisation
- Open to all ages

 \*\*Donations welcome

<!-- -->

**Das gesamte Programm des 30. LeLe\*Tre’s ->[siehe bitte hier](</angebote/aktuelle-projekte/leletre-leipziger-lesbentreffen/>)**