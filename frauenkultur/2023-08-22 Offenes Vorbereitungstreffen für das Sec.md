---
id: atSEhrZp
title: "Offenes Vorbereitungstreffen: Leipziger Frauen*/FLINTA*-Festival 2024"
start: 2023-08-22 15:00
end: 2023-08-22 17:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
Die öffentlichen sexistischen Gewalttaten gegenüber Frauen\*/FLINTA\*s in den vergangenen Jahren weltweit (auch in Deutschland) sind erschütternd – und zeigen die Unabdingbarkeit von breiten Bündnissen feministisch & zivilgesellschaftlich humanistisch handelnden Gruppen auf. Es braucht unterschiedlichste Formate und Mittel konsequenter Aktivitäten gegen jede Form von alltäglichem Sexismus, FLINTA\*- und Fremdenfeindlichkeiten.

 Auch das Leipziger Frauen\*/FLINTA\*-Festival ist ein öffentliches Format, um - Bündnisse zu vertiefen,
- positionierte Diskurse öffentlich zu führen,
- unerlässliche Forderungen zu stellen,
- Perspektiven und Ideen zu teilen und
- neue gerichtete Formen des gemeinsamen feministischen Handelns zu entwickeln.

<!-- -->

Dazu möchten wir feministischen Gruppen, Kollektive und Aktivistinnen\* einladen – gemeinsam das Sechste Leipziger Frauen\*/FLINTA\*-Festival inhaltlich mitzugestalten.

## Termin: 04. Mai 2024 \| 14 – 22 Uhr \| Leipziger Markt.

**P.S.:** Das F in FLINTA\* steht für Frauen\*. Warum „Frauen\*/FLINTA\*“-Festival? 

 Warum *also* nicht konsequenterweise nur FLINTA\*-Festival?

Sprache entwickelt sich. Das ist immer ein Zeichen von weitreichenden gesellschaftlichen Bewegungen. Die Diskurse um diskriminierungsfreie Sprache sind z.Z. anders öffentlich als noch vor zwei bis drei Jahren. Und das Kürzel „FLINTA\*“ steht **für ein diskriminierungsfreies Miteinander. Und diskriminierungsfreies Miteinander ist eine unabdingbare Voraussetzung für die Zukunftsfähigkeit aller Generationen.**

In dem Prozess, diese Zukunftsvoraussetzung zu schaffen, befinden wir uns heute. Es ist ein Prozess, der auch bei den unterschiedlichsten feministischen Aktionen & Formaten **zielgruppenspezifisch mitgedacht werden sollte**.

In diesem Kontext also „Frauen\*/FLINTA\*-Festival“ – denn es gibt viele Menschen, die noch nicht wissen, was FLINTA\* bedeutet. Und da „Titel bzw. Überschriften“ visuell oft schnell erfasst werden, würde ein Teil unserer Zielgruppe sich nicht angesprochen fühlen bzw. nicht erreicht werden.

Ausführliche Infos *folgen* unter [www.leipzigerfrauenfestival.de](<https://www.leipzigerfrauenfestival.de>)