---
id: 60W5ERZv
title: Stark und aufrecht.
start: 2023-10-21 18:00
end: 2023-10-21 18:45
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
GOTTESDIENST für Frauen\* zum 30. LeLe\*Tre

Die befreiende, göttliche Kraft im Gottesdienst mit anderen Lesben und Frauen suchen, feiern und erfahren. Unser Thema in diesem Jahr ist die Zeit und der Moment, in dem etwas möglich ist. Verschiedene glaubende und suchende Frauen u.a. aus der Gruppe *Lesben und Kirche*, laden auch dieses Jahr wieder zum Gottesdienst ein und freuen sich auf alle, die kommen.

**Ort:** Taufkapelle der Peterskirche – Zugang von der Schletterstraße 

 Eintritt: frei

**Das gesamte Programm des 30. LeLe\*Tre’s ->[siehe bitte hier](</angebote/aktuelle-projekte/leletre-leipziger-lesbentreffen/>)**