---
id: M6EuGWM4
title: "Lesben*-Café "
start: 2024-01-12 19:00
end: 2024-01-12 22:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
Das Lesben\*Café ist ein Ort, um sich in freundlicher Atmosphäre kennenzulernen, auszutauschen und um gemeinsam Projekte und Unternehmungen zu planen; ein Ort positiver Energie, der immer wieder neugestaltet werden kann.