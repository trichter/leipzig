---
id: KDwdhOif
title: Patriarchale Gewalt im digitalen Raum. Dickpics, Stalking & Co. Was hilft?
start: 2023-06-01 18:00
end: 2023-06-01 21:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
WORKSHOP

von und mit der AG LINK

 Mit der Digitalisierung unseres Privatlebens findet geschlechtsbezogene und sexualisierte Gewalt zunehmend auch ‘online’ statt. Alltägli-che Beispiele sind Bedrohung, Belästigungen wie das unaufgeforderte Versenden von Penisbildern (engl. dickpics) und Stalking. Zu oft wird diese Gewalt bagatellisiert – und das Leid der Betroffenen nicht ernst genommen. Nach einem Einführungsvortrag werden wir gemein-sam mit euch passende Handlungsmöglichkeiten für verschiedene Formen digitaler Gewalt überlegen. Kein Vorwissen notwendig, offen für alle Gender.

Der Workshop wurde erarbeitet von der AG Link, den Kritischen Jurist\*innen Leipzig und weiteren Einzelpersonen. Weitere Infos: [https://ag-link.xyz/events](<https://ag-link.xyz/events>) \| Bist du betroffen von digitaler Gewalt? Wende dich an: [https://hateaid.org/meldeformular](<https://hateaid.org/meldeformular>) \| Hilfetelefon Gewalt gegen Frauen (24h): 08000 116 016