---
id: l5UbVtmb
title: Den Toten ehrendes Gedenken und Mahnung für heute
start: 2023-11-09 16:00
end: 2023-11-09 16:30
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
**MAHNWACHE an den Stolpersteinen**

Die Initiativgruppe “Mahnwache und Stolpersteine putzen” veranstaltet diese Aktion anlässlich der Pogromnacht und alljährlich beteiligt sich die Frauenkultur an den folgenden drei Standorten und lädt alle Interessierten ein, mit dazu zu kommen:

**Eisenbahnstr. 97: Sofie Schneider.** 

 Sie wurde im Januar 1942 nach Riga deportiert.

**Neustädter Str. 13: Fanny Chaja Mann.** 

 Sie stirbt während der „Polenaktion“ am 28. Oktober 1938 noch auf dem Leipziger Bahnhof.

**Kochstraße 56: Die Schwestern Erika, Genia und Renate Landwirth** 

 Sie sterben nach Abschiebung und Deportation ihrer Eltern in Krakau und Auschwitz im Alter von 2 bis 14 Jahren.