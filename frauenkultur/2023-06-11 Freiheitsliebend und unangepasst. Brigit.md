---
id: J6j0wFHS
title: Freiheitsliebend und unangepasst. Brigitte Reimann (1933-1973)
start: 2023-06-11 16:00
end: 2023-06-11 18:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
LITERATUR & GESPRÄCH … offen für alle Interessierten

Mit STEFFI BÖTTGER, Autorin und Schauspielerin & 

 Prof. ILSE NAGELSCHMIDT, Literaturwissenschaftlerin

Unangepasst, oft enttäuscht, vom Leben alles fordernd und viel bekommen – so lassen sich Lebenslinien von Brigitte Reimann be-schreiben. Dabei hat alles zunächst sehr unspektakulär angefangen. Ihre ersten Texte schrieb sie noch unter dem Einfluss der geforder-ten Methode des sozialistischen Realismus. Die erste größere Erzählung “Ankunft im Alltag” wurde unverzüglich mit dem Etikett der ‘Ankuftsliteratur’ versehen und stand in den Literaturlexika der DDR für all die Literatur, die unmittelbar nach 1961 geschrieben wurde.

Hinter diesen ersten Texten verbarg sich eine tief verunsicherte junge Frau, nicht ganz gesund und mit großen Plänen, diese ‘neue’ Gesellschaft mitzugestalten. Dabei ist sie bald an erste Grenzen gestoßen. Um diese auszuloten, hat sie Tagebücher geschrieben. Während die ersten ihren späteren Blicken nicht mehr standhalten konnten, liegen seit 1990 zwei große Tagebücher vor, die sie zum einen brauchte, sich all dessen zu vergewissern, was sie beunruhigte, bedrückte aber auch erfreute – und hier Material für einen der wohl inspirierendsten Roman der 1970er Jahre zu sammeln, der nicht vollenden werden konnte: Franziska Linkerhand.

In diesem Literaturtreff werden Steffi Böttger und Ilse Nagelschmidt vielfältige Blicke auf Leben und Werk der 1973 viel zu früh verstor-benen Autorin entwickeln und mit den Zuhörer:innen gemeinsam erkunden, welche der über 50Jahre alten Botschaften bis in die Ge-genwart ihre Wirkung erhalten haben.

*Veranstalter:in: Friedrich – Naumann-Stiftung für die Freiheit in Kooperation mit der Frauenkultur Leipzig* 

 Eintritt: frei