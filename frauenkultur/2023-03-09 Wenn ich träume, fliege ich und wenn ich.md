---
id: WHCmbTOP
title: Wenn ich träume, fliege ich und wenn ich fliege, bin ich frei.
start: 2023-03-09 19:00
end: 2023-03-09 20:30
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
AUSSTELLUNGSERÖFFNUNG

## Malerei von PAULA LIMA

Geboren 1966 in Luanda/Angola, aufgewachsen zwischen Angola und Portugal, lebt Paula Lima seit 1991 in Leipzig… mit Begeisterung. Bereits als Kind war sie fasziniert von jeglicher Art von Kunst. Diese Faszination hat sie ihr gesamtes Leben begleitet. Hinzu kam die Liebe, mit Kindern zu arbeiten und sie bei ihrer Entwicklung zu begleiten. Aus diesem Grund

> …gehe ich gegenwärtig mit Begeisterung und Spaß meinem Beruf als Erzieherin nach und lerne von den Kindern jeden Tag etwas Neues. In meiner Freizeit male und zeichne ich und bringe meine Träume, Ideen und alltägliche sowie gesellschaftliche Konflikte in meinen Bildern zum Ausdruck. Meine Bilder sind hauptsächlich mit Acryl auf Leinwänden gemalt und zeigen zudem typische Symbole aus meiner Heimat. Somit haben alle Werke einen hohen sentimentalen Wert. Meine Kunst entwickelt sich durch verschiedene Lebensabschnitte stetig mit mir, und ich probiere gerne neue Techniken und Methoden aus. Ich durfte bereits an einigen Ausstellungen mitwirken, wofür ich sehr dankbar bin, und möchte auch in Zukunft mehr von meiner Kunst zeigen.

Eintritt: frei

**Ausstellungsdauer: *bis 18. April 2023***