---
id: 0B4NFwRJ
title: "Tod im Cabaret Voltaire "
start: 2023-04-29 20:00
end: 2023-04-29 21:15
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
LESUNG & GESPRÄCH \| Zytglogge

 zur Buchmesse 2023 *Leipzig liest* in der FraKu

Mit MIRIAM VEYA

Zürich, im Oktober 1919: Die junge Witwe Josephine, deren soeben verstorbener Mann eine „Auskunftsstelle für vermisste Personen“ betrieben hat, in dem auch sie tätig war, steht vor dem Nichts. Als sie am Abend nach der Beerdigung im verwaisten Büro überlegt, dieses aufzulösen, stürmt eine Frau herein und beauftragt sie mit der Suche nach ihrer verschwundenen Freundin. Diese ist Tänzerin im Cabaret Voltaire, der Wiege der DADA-Bewegung, wo auch die Auftraggeberin als Künstlerin arbeitet. Eigentlich will Josephine den Auftrag ablehnen. Doch dann wird die Künstlerin auf der Bühne des Kleintheaters von einem herabstürzenden Kulissenteil erschlagen, und Josephine glaubt als Einzige nicht an einen Unfall. Sie beginnt, auf eigene Faust zu ermitteln. Dabei bringt sie nicht nur sich selbst in Gefahr, sondern muss sich auch gegen alle Widerstände den Weg freikämpfen, als alleinstehende Frau ein unabhängiges Leben führen zu können. Die Autorin erzählt einen spannenden historischen Kriminalfall, der durch seinen aktuellen Bezug überrascht. Gleichzeitig zeichnet sie ein authentisches und atmosphärisch dichtes Bild des Lebens in Zürich vor hundert Jahren.

MIRIAM VEYA hat Englische Linguistik und Literaturwissenschaft studiert und veröffentlicht im Frühjahr ihren historischen Krimi Tod im Cabaret Voltaire. Die Ermittlungen der modernen Protagonistin Josephine Wyss finden nach dem Ersten Weltkrieg im DADA-Umfeld statt. Die Moderation der Veranstaltung wird der Verlagsleiter Thomas Gierl übernehmen. 

 Eintritt: nach Selbsteinschätzung