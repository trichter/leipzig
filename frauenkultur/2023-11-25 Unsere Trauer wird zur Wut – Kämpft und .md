---
id: OZ4ax77x
title: Unsere Trauer wird zur Wut – Kämpft und wehrt euch!
start: 2023-11-25 13:00
end: 2023-11-25 20:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
AKTIONEN UND DEMONSTRATIONEN zum 25.11.2023

**Heraus zum internationalen Tag gegen Gewalt an Frauen.**

Alle aktuellen Infos auch unter [**https://agfrauenprojekte-leipzig.com/aktionen/**](<https://agfrauenprojekte-leipzig.com/aktionen/>)