---
id: BNikuuZT
title: Hast du auch eine Scheißangst, alles zu verlieren, wenn das Klima weiter
  destabilisiert wird?
start: 2023-05-11 19:00
end: 2023-05-11 20:30
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
Statt gegen die Hungersnöte, die Wüstenbildung, die Gletscherschmelze, die Flutkatastrophen vorzugehen, kippen die Verantwortlichen Öl ins Feuer. Schon bald wird es zu spät sein und die Gesellschaft verschließt die Augen. Wir haben einen Plan zivilen Widerstands, der hohe Opferbereitschaft erfordert, doch eine Chance hat, zu funktionieren. Hör ihn dir an! Eine Veranstaltung der *Letzten Generation Leipzig*

 Eintritt: frei

*Ein Kommentar (Deutschlandfunk), den die Frauenkultur in diesem Kontext teilen möchte:* **\-> [Kommentar zu Klimaprotesten](<https://www.deutschlandfunk.de/kommentar-zu-klimaprotesten-kaempfe-in-den-eigenen-reihen-schaden-klimabewegung-dlf-01a308f5-100.html>)**