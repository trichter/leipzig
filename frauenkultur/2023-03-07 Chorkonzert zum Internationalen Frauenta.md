---
id: NxiBo6pO
title: Chorkonzert zum Internationalen Frauentag
start: 2023-03-07 18:30
end: 2023-03-07 20:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
KONZERT 

*Organisiert vom Internationalen Frauenchor Leipzig*

**Moderation: Elisabeth Mücksch (Sängerin bei *Calmus* und *Sjaella*)**

 Die Chorszene Leipzigs ist eine der dynamischsten Chorszenen Europas. Aufgrund einer jahrhundertelangen Tradition finden sich trotzdem im Vordergrund dieser Szene vor allem Männer. **Der Internationale Frauenchor Leipzig** hatte daher die Idee, verschiedene Frauenchöre für ein Konzert zum Internationalen Frauentag zusammenzubringen. Besonders ist, dass diese Chöre auch von Frauen dirigiert werden. Damit feiern wir die Vielfalt der Frauenchorszene und wir unterstützen uns gegenseitig.

## Die Chöre:

**CKC und Cantares-Chor:**

 Leitung: ESMERALDA BOLAÑOS; zwei gemischte Chöre, am Konzert nehmen sie nur mit Frauen teil.

**INTERNATIONALER FRAUENCHOR LEIPZIG:** 

 eine Gruppe von Frauen verschiedener Nationalitäten und Sprachen. Probe 1x wöchentlich. Der Chor strebt danach, ein Raum für Begegnung, Interkulturalität und Integration zu sein. Ein offener Ort für begeisterte Frauen des Chorgesangs; eine eigenständige, enthusiastische und künstlerische Vereinigung, die sich momentan dem Chor Repertoire lateinamerikanischer Komponist:innen widmet. Er singt unter der Leitung der chilenischen Dirigentin DARSY ASTORGA FLORES

**FRÄULEIN A. KAPELLA:** 

 der Frauenchor wurde 2008 gegründet und bis 2020 von Elke Heiwolt und Jana Stefanek gemeinsam geleitet. Probe 1x wöchentlich; bis 01-2023 unter der Leitung von ELKE HEIWOLT; seit 02-2023 unter der Leitung von CONNY SCHÄFER. Das Repertoire umfasst Songs aus Rock, Pop, Jazz, Chansons und Volkslieder aus aller Welt. Traditionell geben die derzeit 24 Frauen ein Sommer- und ein Weihnachtskonzert. Fräulein A. Kapella singt, wie der Name schon sagt, ohne instrumentale Begleitung. Die Frauen stehen nicht in festen Stimmgruppen, so dass sich der Chorklang immer wieder neu zusammensetzt. Diese Besonderheit geht auf Delia Franke, die musikalische Leiterin des Primadonna Frauenmusik e.V. zurück, dem viele Sängerinnen von Fräulein A. Kapella von 1996 bis 2007 angehörten.

 Eintritt: gegen Selbsteinschätzung