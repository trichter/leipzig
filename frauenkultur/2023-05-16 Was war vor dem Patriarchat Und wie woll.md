---
id: m1FNeiSv
title: Was war vor dem Patriarchat? Und wie wollen wir heute leben?
start: 2023-05-16 17:00
end: 2023-05-16 19:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
## GESPRÄCHS-CAFÉ mit thematischem Input

Nun gibt es nicht wenige Menschen, die meinen, dass die patriarchale Herrschaft quasi gottgegeben ist und schon immer war. Auch „in der Forschung“ halten zumeist Forscher nahezu starr an dieser Sichtweise fest und negieren mit Vehemenz die wahrscheinliche Möglichkeit des gleichberechtigten Miteinanders der Menschen in der Ur- und Frühgeschichte. Und das trotz zahlreicher archäologischer Funde u.a. einer großen Fülle weiblicher Steinfiguren, die durchaus mit matriarchalen gemeinschaftlichen Lebensformen aus einer archäologisch soziologischen Wissenschaftsperspektive korrelieren.

In unserem offenen Gesprächscafé wollen wir uns über Arbeiten der Archäologin, Prähistorikerin und Anthropologin Dr. Marija Gimbutas mit dem Miteinander der Menschen in der Frühgeschichte und im Heute auseinandersetzen. Marija Gimbutas und andere Forschende zeigten auf, dass das gleichberechtigte Zusammenleben und Zusammenarbeiten von Frauen und Männern einer der wesentlichen Gründe für das Überleben/-Können der Menschen in der Ur- und Frühgeschichte war.

> Und „…wenn die Menschen früher in Frieden miteinander leben konnten, ist dies auch in der Zukunft möglich. …” (Marija Gimbutas)

Eintritt: nach Selbsteinschätzung