---
id: fUbKLjs9
title: Philosophi*A
start: 2023-11-09 19:00
end: 2023-11-09 21:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
AUSSTELLUNGSERÖFFNUNG

In der Vergangenheit wurden FLINTA [Frauen Lesben inter nonbinäre trans und agender-Personen] immer wieder aus dem philosophi-schen Diskurs verdrängt. Ihre Arbeiten wurden vergessen, gestohlen oder vernichtet. Wir haben uns zur Aufgabe gemacht, die Werke von Philosoph\*innen in den Mittelpunkt des Diskurses zu stellen – auf eine besondere Weise. Junge Wissenschaftler:innen aus unter-schiedlichen Fachrichtungen wurden gebeten, sich kreativ mit dem Werk jeweils eines\*r Philosoph:in auseinanderzusetzen. Hierfür wur-den ganz verschiedene Genres gewählt – Gemälde, Zeichnungen, Fotografien, Zines, Poetry Slam, Collagen, Webarbeiten u. a. 

 In ihnen wurden neue transdisziplinäre Perspektiven auf die philosophischen Texte entwickelt, indem sie mit aktuellen Fragen menschli-chen Handelns sowie Zukunftsperspektiven verbunden wurden.

## Die vielseitigen künstlerischen Arbeiten zeigen, wie inspirierend Philosophie heute sein kann. Lassen auch Sie sich faszinieren!

*Philosophi\*A* wird kuratiert von Prof. Dr. CARLA SCHRIEVER (Internationale Hochschule Hannover), Prof. Dr. BETTI HARTMANN (University College London) und Dr. VIOLA RÜHSE (Universität für Weiterbildung Krems) – die auch gemeinsam die Eröffnung gestalten.

Im Anschluss an die Eröffnung wird eingeladen zu Sekt und Saft und Häppchen.

Die Kunstinitiative steht in Zusammenhang mit der kürzlich im UNRAST Verlag erschienenen Publikation *Vordenkerinnen: Physikerinnen und Philosophinnen durch die Jahrhunderte* von Prof. Dr. Carla Schriever und Prof. Dr. Betti Hartmann.