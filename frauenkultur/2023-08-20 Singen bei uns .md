---
id: yVEBOhb3
title: "Singen bei uns "
start: 2023-08-20 15:00
end: 2023-08-20 17:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
OFFENES CHOR-Projekt Der Chor „Singen bei uns“ lädt ein zum gemeinsamen Singen… mit der Chorleiterin SANDRA HAVENSTEIN, Musikerin & Musikpädagogin. Das Chorprogramm spannt einen Bogen von berührenden Liedern bis zu mitreißenden Songs… Wir freuen uns über weitere interessierte Sängerinnen und Sänger, die Lust haben, mitzusingen.

[**Weitere Informationen hier**](</angebote/aktuelle-projekte/singen-bei-uns-ein-besonderes-chorprojekt/> "Singen bei uns. Ein besonderes Chorprojekt [27]")

Nächster Termin: **17\.09.2023**