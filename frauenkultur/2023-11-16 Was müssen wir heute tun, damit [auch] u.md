---
id: RD39iPwk
title: Was müssen wir heute tun, damit [auch] unsere Kinder für eine
  geschlechter-/gerechte Welt kämpfen werden?
start: 2023-11-16 18:00
end: 2023-11-16 20:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
P A N E L D I S K U S S I O N

In allen Gesellschaftsbereichen, besonders aber im Kinder- und Jugend-/Bildungsbereich, stehen aktuell Fragen nach Verantwortlichkeiten im Fokus – die durch den Generationenwechsel bedingt sind/sein werden. In diesem Kontext sind feministische Perspektiven in erster Priorität als zukunftsrelevant & unabdingbar einzustufen.

## Die Gleichstellung aller Geschlechter ist eine Grundvoraussetzung für eine Welt mit realer Zukunft – in der keine Generation mehr Angst haben muss, die „Letzte” zu sein.

Aber was müssen wir tun, damit Kinder/Jugendliche genau dies erleben werden können: Diese Angst nicht mehr zu haben? Welche Erfahrungen und Freiräume brauchen Kinder & Jugendliche, um ein eigenverantwortliches Selbstbewusstsein entwickeln zu können? Wie auch Selbst-/Reflexionsfähigkeit und eine Sensibilität für kompromissnotwenige Situationen.

Diese grundlegenden Aspekte für freie individuelle Entwicklungsmöglichkeiten der jüngeren Generationen, auch im Kontext mit feministischer Adultismuskritik, stehen im thematischen Mittelpunkt dieses Gesprächspodiums – **mit Redezeit für alle!**

## Speakerinnen:

**SOOKEE**, quing of berlin, Politik, Psyche & Partyzipation; sukini.musik

**CARINA FLORES**, Trainerin in der Politischen Bildung, Transformatives Lernen, Diversität und in der rassismuskritischen Organisationsberatung. Co -Autorin des Buches “Globales Lernen, Inspirationen für den Transformativen Unterricht

**JANINA BITTNER**, Abteilungsleiterin Jugendhilfe bei der Stadt Leipzig; zuständig u.a. für die Integrierte Kinder- und Jugendhilfeplanung (*angefragt*)

**FRANZISKA DEUTSCHMANN**, Vorstand Louise-Otto-Peters-Gesellschaft \| Gymnasiallehrerin Geschichte/Latein

Eingeladen sind alle Interessierten, besonders *Multiplikator:innen* [vorrangig] tätig in der Arbeit mit Kindern/Jugendlichen\*

 Teilnahme: nach Selbsteinschätzung