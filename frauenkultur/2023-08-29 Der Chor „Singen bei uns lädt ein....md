---
id: 7dCZI39m
title: Der Chor „Singen bei uns" lädt ein...
start: 2023-08-29 16:00
end: 2023-08-29 16:40
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
KLEINES SOMMER-CHOR-KONZERT im Garten des Klinger-Hauses

Der Chor „Singen bei uns” lädt ein… zum Mitsingen von bekannten Volksliedern, zum humorvollen Sommer-Quiz und zu poetischen Gedichten.

 Der Chor freut sich auf ein unbekümmertes gemeinsames Miteinander mit frohmachenden Begegnungen – die wir über ‘den Moment hinaus’ mitnehmen – und die wie ein kleiner Kraftquell wirken können.

## Ort: Garten des Klinger-Hauses, Richard-Lehmann-Straße 36, 04275 Leipzig