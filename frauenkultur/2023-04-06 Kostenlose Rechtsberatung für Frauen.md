---
id: ovm9VLI5
title: Kostenlose Rechtsberatung für Frauen*
start: 2023-04-06 18:00
end: 2023-04-06 20:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
Rechtsberatung

mit der Rechtsanwaltskanzlei TANJA MÜLLER-TEGETHOFF 

 Anmeldung ist bitte bis **05\.04.2023** erforderlich!