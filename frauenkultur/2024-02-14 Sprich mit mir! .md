---
id: G5XkKcEI
title: "Sprich mit mir! "
start: 2024-02-14 15:00
end: 2024-02-14 17:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
SPRACHTANDEM

 Ein Projekt der Frauenkultur Leipzig und des Museums der Bildenden Künste Leipzig.

Interessierte Frauen können an jedem 2. Mittwoch im Monat das Museum der Bildenden Künste besuchen und im Sprachtandem dabei Sprache neu lernen. Eine Deutsch-Sprechende und eine Deutsch-Lernende Frau besuchen gemeinsam das Museum – und erzählen einander, was sie sehen. Sie entdecken zusammen Bilder – und lernen gemeinsam Deutsch oder auch eine andere Sprache…

Für Frauen und Mädchen ab 14 Jahren. Die Teilnahme ist kostenlos.

 Alle Anfragen bitte: Frauenkultur Leipzig \| Tel. 0341 – 213 00 30 oder hallo@frauenkultur-leipzig.de