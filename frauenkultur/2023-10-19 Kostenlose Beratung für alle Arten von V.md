---
id: SggoPjeI
title: "Kostenlose Beratung für alle Arten von Versicherungen "
start: 2023-10-19 18:00
end: 2023-10-19 20:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
FAIR-SICHERUNGSBERATUNG für Frauen

Hier können eigene Versicherungspolicen überprüft oder Fragen zu neuen Abschlüssen gestellt werden. Und alles unverbindlich und unparteiisch! Mit ANTJE GOLDHORN, unabhängige Versicherungskauffrau. \| Wichtig: Vorherige Anmeldung unter 0341 – 2130030 ist notwendig.