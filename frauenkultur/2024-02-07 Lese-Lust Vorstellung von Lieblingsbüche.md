---
id: yx83W1nU
title: 'Lese-Lust: Vorstellung von "Lieblingsbüchern" '
start: 2024-02-07 11:00
end: 2024-02-07 13:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
LITERARISCHER STAMMTISCH für Interessierte jeden Alters

Eingeladen sind alle, die Freude am Lesen haben und gerne darüber debattieren möchten. Einmal im Monat kann zugehört oder selbst etwas vorgestellt werden. Und mit Begeisterung gelesen wird alles: vom Historischen Roman über Biografien bis zu ultramoderner Literatur. 

 Leitung: S. SOMMER