---
id: rQc7sq99
title: "Lesben*Café "
start: 2023-03-03 19:00
end: 2023-03-03 23:00
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
OFFENER TREFF

Das Lesben\*Café ist ein Ort, um sich in freundlicher Atmosphäre kennenzulernen, auszutauschen und um gemeinsam lesbische Projek-te und Unternehmungen zu planen