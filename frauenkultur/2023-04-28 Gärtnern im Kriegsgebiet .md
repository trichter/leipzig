---
id: 0YUK6sgy
title: "Gärtnern im Kriegsgebiet "
start: 2023-04-28 16:00
end: 2023-04-28 17:15
locationName: Soziokulturelles Zentrum Frauenkultur
address: Windscheidstrasse 51
link: https://www.frauenkultur-leipzig.de/programm/
isCrawled: true
---
LESUNG & GESPRÄCH \| Dagyeli Verlag eG (i.G.)

 zur Buchmesse 2023 *Leipzig liest* in der FraKu

Mit TAMRI FKHAKADZE

Erzählungen \| Aus dem Georgischen von Iunona Guruli

 In ihren Erzählungen setzt sich die georgische Autorin Tamri Fkhakadze u.a. mit weiblichen Rollenbildern und den Verwerfungen der postsowjetischen Gesellschaft auseinander. Viele ihrer Erzählungen wurden auf Theaterbühnen gebracht; die Verfilmung von „Gärtnern im Kriegsgebiet“ hatte 2022 Kinopremiere. 

 Eintritt: nach Selbsteinschätzung