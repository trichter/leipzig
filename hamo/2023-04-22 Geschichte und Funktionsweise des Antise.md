---
id: "980083643154156"
title: Geschichte und Funktionsweise des Antisemitismus - Workshop mit Olaf
  Kistenmacher
start: 2023-04-22 11:00
end: 2023-04-23 17:00
locationName: Handstand und Moral
address: Merseburger Straße 88 b, Leipzig
link: https://www.facebook.com/events/980083643154156/
image: 336919946_190744267009548_9127893547260961497_n.jpg
isCrawled: true
---
Workshop mit Dr. Olaf Kistenmacher

Ort: Handstand & Moral, Merseburger Str. 88b

Datum: 22. & 23. April

Uhrzeit: 11 - 17 Uhr


Das Seminar wird sich mit Judenfeindschaft in unterschiedlichen gesellschaftlichen Milieus und in unterschiedlichen Ausprägungen beschäftigen. Seit 1945 ist ein unterschwelliger Antisemitismus weit verbreitet, den es vor 1933 schon in liberalen und linken Milieus gab, die sich als vorurteilsfrei und als kritisch gegenüber Antisemitismus und Rassismus verstanden. Seit einigen Jahren bilden sich neue Bündnisse verschiedener politischer Strömungen, die Verschwörungsideologien anhängen, die einfache Antworten auf komplexe Probleme bieten, den Hass auf Wehrlose lenken und so Gemeinschaften schaffen. Seit einigen Jahren zeigen extreme Rechte in und um die AfD ihren Hass immer offener ...

Der Workshop wird anhand von aktuellen und historischen Beispielen die psychologischen und gesellschaftlichen Ursachen des Antisemitismus verdeutlichen und zeigen, was wir gegen Judenfeindschaft machen können.

Dr. Olaf Kistenmacher arbeitet seit über 20 Jahren als Bildungsreferent gegen Antisemitismus und Rassismus und veröffentlicht zur Judenfeindschaft von links wie von rechts. Weitere Informationen: https://forschungsforum.net/mitglieder/olaf-kistenmacher/ 

Für den Workshop ist eine Anmeldung erforderlich. Einfach per Mail an: jufo.leipzig@digev.de

Mit freundlicher und finanzieller Unterstützung der linksjugend Leipzig