---
id: "1227999744588768"
title: Aide! Fussball in Israel
start: 2023-05-22 19:00
locationName: Handstand und Moral
address: Merseburger Straße 88 b, Leipzig
link: https://www.facebook.com/events/1227999744588768/
image: 336920710_211486841567935_4421176583791832818_n.jpg
isCrawled: true
---
Aide! Fussball in Israel

Vortrag mit Oliver Vrankovic

Ort: Handstand & Moral, Merseburger Str. 88b

Datum: 22. Mai

Uhrzeit: 19 Uhr


 Warum hängt im Fanblock des Zweitligisten Maccabi Yafo eine Bulgarien Fahne und warum eine kurdische Fahne im Block des Viertligisten Beitar Nordia Jerusalem. Warum gibt es ein Derby zwischen Maccabi und Hapoel Ashdod in der dritten Liga, obwohl beide Vereine vor 20 Jahren zu MS Ashdod zusammengelegt wurden? Welche Bedeutung spielt der Viertligist Maccabi Sderot in der Hauptstadt der Bunker? Was treibt junge Männer in arabischen Städten an, Ultras ihrer lokalen unterklassigen Teams zu sein und wie kommen sie mit dem Ultras jüdischer Mannschaften aus? Welches ist die arabische Fußballhauptstadt und welche Bedeutung hat diese Frage für arabische Fussballfans? Was wird in Stadien arabischer Mannschaften gerufen und was passiert, wenn man sich dort auf Hebräisch unterhält? Wie spiegeln sich die innerisraelischen Verwerfungen vom Mai 2021 auf den Rängen? Wie spiegelt sich die Zerrissenheit der israelischen Gesellschaft auf den Rängen? Welche Rolle spielen Politik und Religion in den Stadien? Warum reicht die Rivalität von Maccabi Sha'arayim und Hapoel Marmorek bis in den Jemen zurück? Wie geht es in einem Facebook Forum zu, in dem sich die fanatischen Fans von Drittligisten austauschen und wie beeinflusst der Austausch die Stimmung in den Stadien?

Oliver Vrankovic hat sich seit Beginn der Saison 2018/19 mehr als 250 Fussballspiele in Israel angeschaut und dabei sehr viele Fans verschiedener unterklassiger Teams kennengelernt und dabei das Land, in dem er seit 16 Jahren lebt, neu entdeckt.