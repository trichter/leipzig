---
id: "1277705696177041"
title: Record Release "Arbeit oder Liebe"
start: 2023-05-06 17:00
end: 2023-05-06 18:00
locationName: Handstand und Moral
address: Merseburger Straße 88 b, Leipzig
link: https://www.facebook.com/events/1277705696177041/
image: 344804129_156678417124853_5999422603694115353_n.jpg
isCrawled: true
---
✬✬✬ Record Release ✬✬✬ 

„Arbeit oder Liebe“, ♡ der Titelsong des Albums, ist mir im Traum eingefallen (Stichwort 👽). Beim Aufwachen wusste ich: wo auch immer dieses Lied herkommt, es ist ein Geschenk. Eins, das ich mit diesem Album und dem Konzert gerne weitergeben möchte.

Die sieben Songs des Albums „Arbeit oder Liebe“ bewegen sich zwischen Auf und Ab, Pop und Bossa – und sind dennoch nicht für JedeN. Aber für alle, die Musik gern mit den Ohren hören =) –

Heute präsentiere ich die Songs zusammen mit Daniel am Kontrabass. Wir spielen im "Handstand und Moral". 📌  Drumherum ist das Georg-Schwarz-Straßenfest. 🍾 Eintritt ist frei, Spenden sind erbeten.