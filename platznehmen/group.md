---
name: Leipzig nimmt Platz
website: https://platznehmen.de
email: platznehmen@systemli.org
scrape:
  source: facebook
  options:
    page_id: platznehmen
---
Mission
1. Wir sind entschlossen, LEGIDA-Proteste und andere rassistische und Neonaziaufmärsche in Leipzig zu verhindern.
2. Neonazistische Einstellungen, Rassismus, Islamfeindlichkeit und andere Ideologien der Ungleichwertigkeit haben in Leipzig keinen Platz.
3. Wir sind solidarisch mit allen, die diese Ziele mit uns teilen.
4. Wir wollen das in gemeinsamen und gewaltfreien Aktionen erreichen.
5. Wir werden Rassist_innen, Neonazis und andere LEGIDAs mit Widersetz-Aktionen zeigen, dass wir sie weder in Leipzig noch anderswo dulden.