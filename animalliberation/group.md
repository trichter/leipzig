---
name: Animal Liberation Leipzig
website:  https://animalliberationleipzig.org/
email: info@animalliberationleipzig.org
scrape:
  source: facebook
  options:
    page_id: AnimalLiberationMarchLeipzig
---
Animal Liberation Leipzig ist ein Zusammenschluss verschiedener Aktivst*innen lokaler Tierrechts- & Tierbefreiungsgruppen. #ALLtogether