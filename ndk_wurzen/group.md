---
name: Netzwerk für Demokratische Kultur e.V.
website: https://www.ndk-wurzen.de/
email: team@ndk-wurzen.de
scrape:
  source: facebook
  options:
    page_id: 1517831911826353
---

Das NDK steht für eine gelebte demokratische Kultur, für das Einüben demokratischer Praktiken, für das gewaltfreie Aushandeln von Konflikten, für eine kritische und aktive Zivilgesellschaft.
