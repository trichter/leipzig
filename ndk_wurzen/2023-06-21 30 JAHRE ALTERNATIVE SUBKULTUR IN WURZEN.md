---
id: "6395672237217145"
title: 30 JAHRE ALTERNATIVE SUBKULTUR IN WURZEN
start: 2023-06-21 18:00
locationName: Netzwerk für Demokratische Kultur
address: Domplatz 5, Wurzen
link: https://www.facebook.com/events/6395672237217145/
image: 347240141_560978356191636_4255948307424327130_n.jpg
isCrawled: true
---
In unserem Archiv haben wir Videomaterial aus fast 30 Jahren Vereins- und Stadtgeschichte gefunden und digitalisiert. Konzerte, Interviews, Demovideos – Wurzens alternative Subkultur hat es immer wieder in die große Presse geschafft. Mit den Zeitdokumenten schauen wir zurück und diskutieren mit Menschen aus der Zivilgesellschaft über die Geschehnisse. Der Eintritt ist frei.