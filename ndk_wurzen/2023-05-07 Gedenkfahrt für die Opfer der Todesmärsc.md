---
id: "576070657295545"
title: Gedenkfahrt für die Opfer der Todesmärsche 1945 - Erinnnerungspolitische
  Wochen -
start: 2023-05-07 10:15
locationName: Heimatmuseum Borsdorf
link: https://www.facebook.com/events/576070657295545/
image: 339321228_1594149524416767_4607989814986787078_n.jpg
isCrawled: true
---
In Erinnerung an die Opfer der Todesmärsche 1945 . Um die Erinnerung an die vielen Opfer der Todesmärsche 1945, deren Qualen und deren Tod aufrecht zu erhalten und um ihrer zu gedenken, fahren wir mit dem Fahrrad von Borsdorf nach Wurzen.

Präsentiert werden an den jeweiligen Orten auch in einem Projekt mit Jugendlichen neu entstandene Gedenktafeln und eine ebenso neu entstandene Broschüre zu den Todesmärschen im Muldental.Zwischen Machern und Bennewitz werden wir eine kleine Pause einlegen - bringt euch bitte Verpflegung mit. 

An den jeweiligen Orten werden wir neue Gedenktafeln präsentieren. Diese wurden an Projekttagen von Jugendlichen und Schüler:innen und Schülern aus der Region erstellt. Außerdem veröffentlichen wir eine Broschüre, die genauer auf die einzelnen Stationen eingeht.

10.15 Uhr
Borsdorf, Heimatmuseum, Leipziger Straße
Begrüßung und Geleitworte mit dem Heimatverein und Bürgermeisterin Birgit Kaden

11.10 Uhr
Gerichshain, Kirche	
Gedenken und Geleitworte mit Bürgermeister Karsten Frosch und dem Chor der Ev.-luth. Kirchgemeinde

12.00 Uhr
Machern, Dorfstraße 1
Gedenken und Geleitworte mit Bürgermeister Karsten Frosch und Schülern des Gymnasiums Wurzen

13.15 Uhr
Bennewitz, Friedhof
Gedenken auf den Friedhof durch die Gemeindeverwaltung

13.30 Uhr
Bennewitz, Freizeithaus
Gedenken und Geleitworte mit Gemeinderätin Ina Adler

14.15 Uhr
Wurzen, Friedhof
Gedenken und Geleitworte mit Pfarrer Wieckowski und der AG Erinnerungskultur beim NDK