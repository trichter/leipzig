---
id: "575695564406927"
title: Wenceslaigassenfest
start: 2023-05-13 12:00
locationName: Wenceslaigasse, 04808 Wurzen, Deutschland
link: https://www.facebook.com/events/575695564406927/
image: 337547162_101723149573008_7375829080864159230_n.jpg
isCrawled: true
---
Wurzener Initiativen laden zum Wenceslaigassenfest

Die Fokusgruppe Nachhaltigkeit, ein Zusammenschluss verschiedener Wurzener Initiativen, lädt am 13. Mai zum Wenceslaigassenfest. Neben Musik, Essen und Mitmachaktionen wird es auch Informationen rund um lokalen und ökologischen Konsum geben.
 
„Wir wollen zeigen, wie schön unsere Stadt ist und wie vielfältig Nachhaltigkeit sein kann.“, so Janina Becker vom Kanthaus. „Deshalb laden wir gemeinsam mit vielen anderen zum Mitmachen, Informieren und Genießen ein.“

Der LADEN gestaltet und bepflanzt die neuen Hochbeete in der Wenceslaigasse mit Unterstützung des Hair Factory Teams. Hier sind viele helfende Hände willkommen. Mit dem Familienexpress und dem Freiraum beteiligen sich weitere Geschäftsleute aus der Wenceslaigasse mit bunten Angeboten am Fest. Aber auch Institutionen wie das Tierheim oder die Stadtwandler sind vertreten. Die Stadtbibliothek macht einen Bücherflohmarkt und die Firma KELL wird mit einem Verschenke- und Tauschstand vor Ort sein. Foodsharing Wurzen und die Solidarische Landwirtschaft Vegutopia stellen ihre Konzepte vor. Zudem wird es verschiedene Mitmachaktionen wie Upcycling, Siebdruck, Saatgutbomben und Pflanzaktionen geben.

„Das Fest ist auch für Familien interessant“, sagt Franziska Wittig von der Verkehrswendegruppe Bitte Wenden. „Kinder können sich auf der Hüpfburg des Familienexpress austoben. Wer ein Fahrrad oder Bobbycar dabei hat, wird sich über unsere Spielstraße freuen. Und auch die Jurte der Pfadfinder wird sicherlich ein Highlight.“
Diese laden zu Essen und Tee ans Lagerfeuer. Auch darüber hinaus sind einige Essensstände, wie etwa ein Bake-Sale von ProVeg Leipzig geplant. Dazu gibt es live Jazz von Felix Krause und Folkmusik aus Torgau.

„Wir haben bereits ein ziemlich buntes Programm zusammengestellt“, findet Heidi Bischof vom NDK e.V. „Aber ein bisschen Platz haben wir noch. Wer sich mit weiteren Angeboten beteiligen möchte, darf sich gerne bei uns melden.“

Das Straßenfest wird von 12 bis 18 Uhr gehen. In Kooperation dazu findet in der Wenceslaikirche die Kreativmesse der Stadt statt, die ebenfalls noch Kreative aller Art sucht, die sich beteiligen möchten. Zudem wird in fußläufiger Entfernung am selben Tag um 14 Uhr im Ringelnatzhaus die neue Dauerausstellung eröffnet.

_________________

Kontakt und weitere Informationen unter:

nachhaltigkeit@land-labor.de
bittewenden@disroot.org