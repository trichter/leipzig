---
id: "488572940124168"
title: Gedenken an die Bücherverbrennung 1933 - Erinnerungspolitische Wochen -
start: 2023-05-10 16:00
locationName: Marktplatz Wurzen
address: Markt, Wurzen
link: https://www.facebook.com/events/488572940124168/
image: 339149007_231981629313496_4614343069844323887_n.jpg
isCrawled: true
---
Werke, die die Nazis als „undeutsches Schrifttum" bezeichneten, wurden am 10. Mai 1933 in vielen deutschen Städten zu Scheiterhaufen aufgetürmt und verbrannt. Mit einer Lesung von Bürger:innen auf dem Wurzener Markt wollen wir daran erinnern.