---
id: "1226146381630331"
title: 'Tischgespräch: „Hier und jetzt: Solidarität statt Polizei" -
  Internationale Wochen gegen Rassismus-'
start: 2023-03-20 18:00
locationName: Netzwerk für Demokratische Kultur
address: Domplatz 5, Wurzen
link: https://www.facebook.com/events/1226146381630331/
isCrawled: true
---
In den USA gibt es seit etlichen Jahren das „Police Abolition Movement“ - die Bewegung zur Abschaffung der Polizei. Besonders seit dem Tod von Georg Floyd 2020 erhält sie großen Aufwind. Aber wie soll das gehen, eine Welt ohne Polizei? Darüber sprechen wir mit Sophie Perthus von der Goethe-Universität Frankfurt. Sie ist Expertin, wenn es um Konzepte zur Abschaffung oder Reformierung der Polizei geht.