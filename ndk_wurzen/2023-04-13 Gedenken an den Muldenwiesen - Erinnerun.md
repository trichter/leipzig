---
id: "1151713682198048"
title: "Gedenken an den Muldenwiesen - Erinnerungspolitische Wochen - "
start: 2023-04-13 17:00
locationName: Wurzener Muldenwiesen (Nähe Dreibrückenbad)
link: https://www.facebook.com/events/1151713682198048/
image: 339153730_1339519806625762_1015391384333682166_n.jpg
isCrawled: true
---
Gemeinsam wollen wir um 17:00 Uhr am Gedenkstein an den Wurzener Muldenwiesen der Opfer der Todesmärsche 1945 gedenken.