---
id: "1860782170928433"
title: 'Lesung: „Kinder von Hoy" Mit Autorin Grit Lemke - Internationale Wochen
  gegen Rassismus -'
start: 2023-03-23 19:00
locationName: Netzwerk für Demokratische Kultur
address: Domplatz 5, Wurzen
link: https://www.facebook.com/events/1860782170928433/
isCrawled: true
---
Durch die rassistischen Ausschreitungen gegen Vertragsarbeiter:innen aus Vietnam und Mosambik 1991 erlangten Hoyerswerda und „der Osten" traurige Berühmtheit. Grit Lemke gibt einen tiefen Einblick in das Leben ihrer Heimatstadt, erzählt vom Niedergang nach der Wende und arbeitet die Biographie ihrer komplexen Generation auf.