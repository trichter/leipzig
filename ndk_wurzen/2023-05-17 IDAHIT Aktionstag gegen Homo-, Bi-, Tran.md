---
id: "748021030010583"
title: "IDAHIT*: Aktionstag gegen Homo-, Bi-, Trans*-, Inter*feindlichkeit und
  Rassistische Strukturen"
start: 2023-05-17 15:30
locationName: Marktplatz Wurzen
address: Markt, Wurzen
link: https://www.facebook.com/events/748021030010583/
isCrawled: true
---
Der bundesweite Aktionstag ist seit 2017 auch in Wurzen eine feste Größe. Es gibt Infostände, Redebeitrage und Musik. Thematischer Schwerpunkt ist Intersektionalität (Gleichzeitigkeit von verschiedenen Diskriminierungsformen).