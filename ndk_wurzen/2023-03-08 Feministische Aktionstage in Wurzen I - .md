---
id: "1840410163018591"
title: Feministische Aktionstage in Wurzen I - Brunch / Feministisches Figurentheater
start: 2023-03-08 11:00
locationName: Netzwerk für Demokratische Kultur
address: Domplatz 5, Wurzen
link: https://www.facebook.com/events/1840410163018591/
isCrawled: true
---
Am 08. März starten wir um 11:00 mit einem Brunch im Kultur - und Bürger:innenzentrum D5. Anschließend verteilen wir Blumen in der Stadt. 19:00 öffnen wir die Türen zu einem Theaterstück des Feministischen Figurentheaters von Frieda Sünderhauf, welches sich der Frage widmet, wie es eigentlich zur Vormachtstellung des Mannes kommen konnte.

"Ich glaube nicht ans Schweigen" schreibt Clare Shaw in ihrem Gedichtband Head on. Diese Ansicht teilen wir. Seien es die Ungerechtigkeiten bei Bezahlung und Anerkennung, der Sexismus in Beziehungen und auf der Straße, aber auch die Nichtbeachtung von Frauen* und Mädchen* in wissenschaftlichen Studien – es gibt viele Gründe, um sich zu vernetzen und laut zu sein. Seit 2019 organisiert die Feministische Aktionsgruppe Wurzen deshalb rund um den 08. März verschiedenste Angebote und Veranstaltungen. So auch dieses Jahr! 