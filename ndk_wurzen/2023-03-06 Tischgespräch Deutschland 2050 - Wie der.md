---
id: "729010905163538"
title: "Tischgespräch: Deutschland 2050 - Wie der Klimawandel unser Leben
  verändern wird"
start: 2023-03-06 18:00
locationName: Netzwerk für Demokratische Kultur
address: Domplatz 5, Wurzen
link: https://www.facebook.com/events/729010905163538/
isCrawled: true
---
Der menschengemachte Klimawandel findet statt: hier und jetzt! Bereits heute steht fest: 2050 wird es bei uns im Durchschnitt mindestens zwei Grad Celsius wärmer sein. Doch welche konkreten Auswirkungen wird das auf unser Leben haben? Darüber sprechen wir mit Buchautor Toralf Staud.